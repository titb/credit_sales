Date.prototype.getAmPm = function () {
			if( this.getHours() >= 12 ) { return 1 }; // pm
			return 0; // am
		}

		var locale = {
			en: {
				month: ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September ', 'October', 'November', 'December'],
				ampm: [ 'am', 'pm' ]
			},
			km: {
				month: ['មករា', 'កុម្ភៈ', 'មីនា', 'មេសា', 'ឧសភា', 
'មិថុនា', 'កក្កដា', 'សីហា', 'កញ្ញា', 'តុលា', 'វិច្ឆិកា', 'ធ្នូ'],
				ampm: [ 'ព្រឹក', 'ល្ងាច' ]
			}
		};

		var toLocaleNumber = function( num, lang, zeroPadding ) {
			if( typeof num !== 'number' ) return null;

			var numInteger = parseInt( num );
			var numString = numInteger.toString();
			
			if( zeroPadding > 0 && numString.length < zeroPadding ) {
				numString = '0' + numString;
			}

			// support only khmer
			if( lang !== 'km' ) { return numString };

			var khmerNumber = '';
			var numbersKhmer = ['០', '១', '២', '៣', '៤', '៥', '៦', '៧', '៨', '៩'];

			for( var i=0; i < numString.length; i++ ) {
				khmerNumber += numbersKhmer[parseInt(numString[i])];
			}

			return khmerNumber;
		};

		var formatDate = function( date, lang ) {
			var formattedDate = null;
			var hours = d.getHours();
			if( hours > 12 ) { hours -= 12; }; 

			formattedDate = toLocaleNumber( d.getDate(), lang, 2 )
				+ '-'
				+ locale[lang]['month'][d.getMonth()]
				+ '-'
				+ toLocaleNumber( d.getFullYear(), lang )
				+ ' '
				+ toLocaleNumber( hours, lang, 2 )
				+ ':'
				+ toLocaleNumber( d.getMinutes(), lang, 2 )
				+ ' '
				+ locale[lang]['ampm'][d.getAmPm()];

			return formattedDate;
		};

		var d = new Date('2014-02-03');

		// for example: "28-May-2016 3:44 pm" "២៨-ឧសភា-២០១៦ ៣:៤៤ ល្ងាច"
		console.log( formatDate( d, 'en' ), formatDate( d, 'km' ) );