<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


// Route::get('/',['as'=>'/', 'uses'=>'HomeController@get_welcome']);
// Route::get('users',['as'=>'users', 'uses'=>'UserController@get_list_user']);
// Route::get('login',['as'=>'login', 'uses'=>'HomeController@get_login']);



// User Managemten
	// user list
		Route::get('users',array('as'=>'users','uses'=>'UserController@get_user_index','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
	//// create user
		Route::get('users/create',array('as'=>'users/create','uses'=>'UserController@get_user_create','middleware'=>'permissions','permissions'=>'create-user-groups'));
		Route::post('users/create',array('as'=>'users/create','uses'=>'UserController@post_user_create','middleware'=>'permissions','permissions'=>'create-user-groups'));
	//// edit user
		Route::get('users/{id}/edit',array('as'=>'users/{id}/edit' , 'uses'=>'UserController@get_user_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		Route::post('users/{id}/edit',array('as'=>'users/{id}/edit' , 'uses'=>'UserController@post_user_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
	// user stutus
		Route::get('users/{id}/active',array('as'=>'users/{id}/active' , 'uses'=>'UserController@show_user_status_active','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		Route::get('users/{id}/not_active',array('as'=>'users/{id}/active' , 'uses'=>'UserController@show_user_status_not_active','middleware'=>'permissions','permissions'=>'edit-user-groups'));
	//show user profile
		Route::get('users/profile',array('as'=>'users/profile','uses'=>'UserController@show_user_profile'));
		Route::get('users/{id}/show',array('as'=>'users/{id}/show','uses'=>'UserController@get_show_user'));
	//
	    Route::post('users/{id}/deleted',array('as'=>'users/{id}/deleted','uses'=>'UserController@post_user_deleted','middleware'=>'permissions','permissions'=>'delete-user-groups'));
	//group Users
		Route::get('users-groups',array('as'=>'users-groups','uses' =>'UserController@get_list_user_group','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		Route::get('personal-loads/bussiness-plan/{id}/create_plus',array('as'=>'personal-loads/bussiness-plan/{id}/create_plus','uses'=>'PBPLoadController@get_loan_bussiness_plan_create_pluss','middleware'=>'permissions','permissions'=>'create-p-loan'));
		Route::get('users-groups/create',array('as'=>'users-groups/create','uses'=>'UserController@get_create_user_group','middleware'=>'permissions','permissions'=>'create-user-groups'));
		Route::post('users-groups/create',array('as'=>'users-groups/create','uses'=>'UserController@post_create_user_group','middleware'=>'permissions','permissions'=>'create-user-groups'));
		Route::get('users-groups/{id}/edit',array('as'=>'users-groups/{id}/edit','uses'=>'UserController@get_edit_user_group','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		Route::post('users-groups/{id}/edit',array('as'=>'users-groups/{id}/edit','uses'=>'UserController@post_edit_user_group','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		Route::post('users-groups/{id}/deleted',array('as'=>'users-groups/{id}/deleted','uses'=>'UserController@post_user_group_deleted','middleware'=>'permissions','permissions'=>'delete-user-groups'));
	//Position
		//list
		Route::get('list_position',array('as'=>'list_position','uses'=>'UserController@list_position','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		//create
		Route::get('position/create',array('as'=>'position/create','uses'=>'UserController@position_create','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		Route::post('position/create',array('as'=>'position/create','uses'=>'UserController@post_position_create','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		//edit
		Route::get('position/{id}/edit',array('as'=>'position/{id}/edit' , 'uses'=>'UserController@position_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		Route::post('position/{id}/edit',array('as'=>'position/{id}/edit' , 'uses'=>'UserController@post_position_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		//delete
		Route::post('position/{id}/deleted',array('as'=>'position/{id}/deleted','uses'=>'UserController@post_position_deleted','middleware'=>'permissions','permissions'=>'delete-user-groups'));



//User Permission

  	Route::get('users-permission',array('as'=>'users-permission','uses' =>'UserController@get_list_user_permission','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
	Route::get('users-permission/create',array('as'=>'users-permission/create','uses'=>'UserController@get_create_user_permission','middleware'=>'permissions','permissions'=>'create-user-groups'));
	Route::post('users-permission/create',array('as'=>'users-permission/create','uses'=>'UserController@post_create_user_permission','middleware'=>'permissions','permissions'=>'create-user-groups'));
	Route::get('users-permission/{id}/edit',array('as'=>'users-permission/{id}/edit','uses'=>'UserController@get_edit_user_permission','middleware'=>'permissions','permissions'=>'edit-user-groups'));
	Route::post('users-permission/{id}/edit',array('as'=>'users-permission/{id}/edit','uses'=>'UserController@post_edit_user_permission','middleware'=>'permissions','permissions'=>'edit-user-groups'));
	Route::post('users-permission/{id}/deleted',array('as'=>'users-permission/{id}/deleted','uses'=>'UserController@post_user_group_permission_deleted','middleware'=>'permissions','permissions'=>'delete-user-groups'));

// User Change Password
  	Route::get('change_password_user/{id}/change',['us'=>'change_password_user/{id}/change','uses'=>'UserController@change_password_user']);
	Route::post('change_password_user/{id}/change',['us'=>'change_password_user/{id}/change','uses'=>'UserController@post_change_password_user']);
	Route::get('change_mypassword_user/change',['us'=>'change_mypassword_user/change','uses'=>'UserController@change_my_password_user']);
	Route::post('change_mypassword_user/change',['us'=>'change_mypassword_user/change','uses'=>'UserController@post_change_my_password_user']);
// Brand 
	Route::get('branch', array('as'=>'branch','uses'=>'BranchController@show_brand','middleware'=>'permissions','permissions'=>'list-brand'));
 	Route::get('branch/create', array('as'=>'branch/create','uses'=>'BranchController@show_brand_create','middleware'=>'permissions','permissions'=>'create-brand'));
 	Route::post('branch/create', array('as'=>'branch/create','uses'=>'BranchController@save_brand_create','middleware'=>'permissions','permissions'=>'create-brand'));
 	Route::get('branch/{id}/edit', array('as' => 'branch/{id}/edit','uses'=>'BranchController@show_brand_edit' ,'middleware'=>'permissions','permissions'=>'edit-brand'));
 	Route::post('branch/{id}/edit', array('as' => 'branch/{id}/edit','uses'=>'BranchController@brand_edit' ,'middleware'=>'permissions','permissions'=>'edit-brand'));
 	Route::get('branch/{id}/deleted', array('as' => 'branch/{id}/deleted','uses'=>'BranchController@deleted_brand' ,'middleware'=>'permissions','permissions'=>'deletd-brand'));
	
// Credit Sale Client 
	Route::get('accounts',array('as'=>'accounts','uses'=>'AccountController@get_list_client_accout','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('accounts/create',array('as'=>'accounts/create','uses'=>'AccountController@get_create_client_account','middleware'=>'permissions','permissions'=>'create-client'));
	Route::post('accounts/create' ,array('as'=>'accounts/create','uses'=>'AccountController@post_create_client_account','middleware'=>'permissions','permissions'=>'create-client'));
	Route::get('accounts_get_js',array('as'=>'accounts_get_js','uses'=>'AccountController@get_list_json_client_accout','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('accounts/{id}/show' ,array('as'=>'accounts/{id}/show','uses'=>'AccountController@get_show_client_accout','middleware'=>'permissions','permissions'=>'edit-client'));
	Route::get('accounts/edit' ,array('as'=>'accounts/edit','uses'=>'AccountController@get_edit_client_account','middleware'=>'permissions','permissions'=>'edit-client'));
	Route::get('accounts/{id}/edit' ,array('as'=>'accounts/{id}/edit','uses'=>'AccountController@get_json_edit_client_account','middleware'=>'permissions','permissions'=>'edit-client'));
	Route::post('accounts/{id}/edit' ,array('as'=>'accounts/{id}/edit','uses'=>'AccountController@post_edit_client_account','middleware'=>'permissions','permissions'=>'edit-client'));
	Route::post('accounts/{id}/delete' ,array('as'=>'accounts/{id}/delete','uses'=>'AccountController@post_delete_client_account','middleware'=>'permissions','permissions'=>'edit-client'));	

	// Sub Client
	Route::get('accounts/sub_client/create_sub_client',['as'=>'accounts/sub_client/create_sub_client','uses'=>'SubClientController@get_create_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::post('accounts/sub_client/create_sub_client',['as'=>'accounts/sub_client/create_sub_client','uses'=>'SubClientController@post_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::get('accounts/sub_client/show_sub_client',['as'=>'accounts/sub_client/show_sub_client','uses'=>'SubClientController@list_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	// Route::get('accounts/show_sub_client',['as'=>'accounts/show_sub_client','uses'=>'SubClientController@client_id','middleware'=>'permissions','permissions'=>'create-client']);
	Route::get('accounts/sub_client/show_sub_client/json',['as'=>'accounts/sub_client/show_sub_client/json','uses'=>'SubClientController@list_json_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::get('accounts/sub_client/{id}/sub_client',['as'=>'accounts/sub_client/{id}/sub_client','uses'=>'SubClientController@show_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::get('accounts/sub_client/edit_sub_client',['as'=>'accounts/sub_client/edit_sub_client','uses'=>'SubClientController@get_edit_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::get('accounts/sub_client/{id}/json_sub_client',['as'=>'accounts/sub_client/{id}/json_sub_client','uses'=>'SubClientController@get_json_edit_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::get('accounts/sub_client/{id}/edit_sub_client',['as'=>'accounts/sub_client/{id}/edit_sub_client','uses'=>'SubClientController@get_json_edit_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::post('accounts/sub_client/{id}/edit/sub_client',['as'=>'accounts/sub_client/{id}/edit/sub_client','uses'=>'SubClientController@post_edit_sub_client','middleware'=>'permissions','permissions'=>'create-client']);
	Route::post('accounts/sub_client/{id}/sub_delete' ,array('as'=>'accounts/sub_client/{id}/sub_delete','uses'=>'SubClientController@delete_sub_client','middleware'=>'permissions','permissions'=>'edit-client'));	
	Route::get('accounts/request_form/{id}',['as'=>'accounts/request_form/{id}','uses'=>'ClContractController@list_request_form','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/request_form/{client_id}/list_json',['as'=>'accounts/request_form/{client_id}/list_json','uses'=>'ClContractController@list_request_form_json','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/request_form/{client_id}/request_id/{id}',['as'=>'accounts/request_form/{client_id}/request_id/{id}','uses'=>'ClContractController@show_request_form','middleware'=>'permissions','permissions'=>'edit-client']);
		Route::get('accounts/request_form_json/{client_id}/request_id/{id}',['as'=>'accounts/request_form_json/{client_id}/request_id/{id}','uses'=>'ClContractController@show_request_form_json','middleware'=>'permissions','permissions'=>'edit-client']);
	
	Route::get('accounts/schedule/{client_id}',['as'=>'accounts/schedule/{client_id}','uses'=>'AccountController@list_client_scedule','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/show_client_schedule',['as'=>'accounts/show_client_schedule','uses'=>'AccountController@show_detail_client_schedule','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/client/list_schedule_json',['as'=>'accounts/client/list_schedule_json','uses'=>'AccountController@list_json_schedule']);
	Route::get('accounts/sub_client/{client_id}/schedule_show_json',['as'=>'accounts/sub_client/{$client_id}/schedule_show_json','uses'=>'AccountController@schedule_show_json','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/aprove_credit_sales/{id}/list_approve',['as'=>'accounts/aprove_credit_sales/{id}/list_approve','uses'=>'AccountController@list_aprove_credit_sales','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/aprove_credit_sales/{client_id}/show_approve/{id}',['as'=>'accounts/aprove_credit_sales/{client_id}/show_approve/{id}','uses'=>'AccountController@show_aprove_credit_sales','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/aprove_credit_sales/{id}/show_json',['as'=>'accounts/aprove_credit_sales/{id}/show_json','uses'=>'AccountController@show_aprove_credit_sales_json','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/aprove_credit_sales/{id}/show_detail_json',['as'=>'accounts/aprove_credit_sales/{id}/show_detail_json','uses'=>'AccountController@show_detail_aprove_credit_sales_json','middleware'=>'permissions','permissions'=>'edit-client']);
	Route::get('accounts/credit_sale/{id}/print_contract',array('as'=>'accounts/credit_sale/{id}/print_contract','uses'=>'AccountController@show_contract_credit_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	// Route::get('accounts/credit_sale/{id}/print_contract_form',array('as'=>'accounts/credit_sale/{id}/print_contract_form','uses'=>'AccountController@show_form_contract_credit_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('accounts/all_sub_client',array('as'=>'accounts/all_sub_client','uses'=>'SubClientController@all_sub_client','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('all_sub_client',array('as'=>'all_sub_client','uses'=>'SubClientController@all_sub_client_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	// History Payments
	Route::get('accounts/history_payment/sale_credit/{client_id}',array('as'=>'accounts/history_payment/sale_credit/{client_id}','uses'=>'AccountController@show_cred_history','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('accounts/history_payment/show_history_credit_sale',['as'=>'accounts/history_payment/show_history_credit_sale','uses'=>'AccountController@show_history_credit_sale']);
	Route::get('accounts/history_payment/show_history_credit_sale_json',['as'=>'accounts/history_payment/show_history_credit_sale_json','uses'=>'AccountController@show_history_credit_sale_json']);
	Route::get('accounts/history_payment/sale_credit_json/{client_id}',['as'=>'accounts/history_payment/sale_credit_json/{client_id}','uses'=>'AccountController@show_cred_history_json']);
	// Client Comment Finish
	Route::get('accounts/client/comment_finish/{client_id}',['as'=>'accounts/client/comment_finish/{client_id}','uses'=>'AccountController@index_client_comment']);
	Route::get('accounts/client/comment_finish/json/{client_id}',['as'=>'accounts/client/comment_finish/json/{client_id}','uses'=>'AccountController@client_comment_json']);


	//Product relatec
	//Brand
 
		Route::get('products/brands',array('as'=>'products/brands','uses'=>'RelateItemController@get_list_brands','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('products/brands_json',array('as'=>'products/brands_json','uses'=>'RelateItemController@get_create_json_brands','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('products/brands/create',array('as'=>'products/brands/create','uses'=>'RelateItemController@post_create_brands','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('products/brands/{id}/edit',array('as'=>'products/brands/{id}/edit','uses'=>'RelateItemController@post_edit_brands','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('products/brands/{id}/deleted',array('as'=>'products/brands/{id}/deleted','uses'=>'RelateItemController@get_deleted_brands','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	
	//Categories
		Route::get('products/categorys',array('as'=>'products/categorys','uses'=>'RelateItemController@get_list_categorys','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('products/categorys_json',array('as'=>'products/categorys_json','uses'=>'RelateItemController@get_create_json_categorys','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('products/categorys/create',array('as'=>'products/categorys/create','uses'=>'RelateItemController@post_create_categorys','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('products/categorys/{id}/edit',array('as'=>'products/categorys/{id}/edit','uses'=>'RelateItemController@post_edit_categorys','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('products/categorys/{id}/deleted',array('as'=>'products/categorys/{id}/deleted','uses'=>'RelateItemController@get_deleted_categorys','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

	//Item 
		Route::get('products/products',array('as'=>'products/products','uses'=>'ItemController@get_list_items','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('products/products_get_js',array('as'=>'products/products_get_js','uses'=>'ItemController@get_list_xjax_search_items','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('products/products/create',array('as'=>'products/products/create','uses'=>'ItemController@get_create_items','middleware'=>'permissions','permissions'=>'create-client'));
		Route::post('products/products/create' ,array('as'=>'products/products/create','uses'=>'ItemController@post_create_items','middleware'=>'permissions','permissions'=>'create-client'));
		Route::get('products/products/{id}/show' ,array('as'=>'products/products/{id}/show','uses'=>'ItemController@get_show_items','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('products/products/{id}/edit' ,array('as'=>'products/products/{id}/edit','uses'=>'ItemController@get_form_edit_items','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('products/products/{id}/json_edit' ,array('as'=>'products/products/{id}/json_edit','uses'=>'ItemController@get_json_edit_items','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::post('products/products/{id}/edit' ,array('as'=>'products/products/{id}/edit','uses'=>'ItemController@post_edit_items','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::post('products/products/{id}/deleted' ,array('as'=>'products/products/{id}/deleted','uses'=>'ItemController@post_delete_items','middleware'=>'permissions','permissions'=>'edit-client'));	
	// Item in Stock
		Route::get('products/products/show/{id}/stock' ,array('as'=>'products/products/show/{id}/stock','uses'=>'ItemController@get_show_items_stock','middleware'=>'permissions','permissions'=>'edit-client'));

	//Supplier
		Route::get('suppliers',array('as'=>'suppliers','uses'=>'SupplierController@get_list_suppliers','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('suppliers/create',array('as'=>'suppliers/create','uses'=>'SupplierController@get_form_create_suppliers','middleware'=>'permissions','permissions'=>'create-client'));
		Route::post('suppliers/create' ,array('as'=>'suppliers/create','uses'=>'SupplierController@post_create_suppliers','middleware'=>'permissions','permissions'=>'create-client'));
		Route::get('suppliers_get_js',array('as'=>'suppliers_get_js','uses'=>'SupplierController@get_list_json_suppliers','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('suppliers_get_ajax_serach',array('as'=>'suppliers_get_ajax_serach','uses'=>'SupplierController@suppliers_get_ajax_serach','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('suppliers/{id}/show' ,array('as'=>'suppliers/{id}/show','uses'=>'SupplierController@get_show_suppliers','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('suppliers/{id}/edit' ,array('as'=>'suppliers/{id}/edit','uses'=>'SupplierController@get_form_edit_suppliers','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('suppliers/{id}/json_edit' ,array('as'=>'suppliers/{id}/json_edit','uses'=>'SupplierController@get_json_edit_suppliers','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::post('suppliers/{id}/edit' ,array('as'=>'suppliers/{id}/edit','uses'=>'SupplierController@post_edit_suppliers','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::post('suppliers/{id}/deleted' ,array('as'=>'suppliers/{id}/deleted','uses'=>'SupplierController@post_delete_suppliers','middleware'=>'permissions','permissions'=>'edit-client'));	
	
    // Credit Sale Request Form and Sale 
		Route::get('accounts/{id}/request_list',array('as'=>'accounts/{id}/request_list','uses'=>'CreditSaleController@get_index_credit_sale_request','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('accounts/{id}/request_form_create',array('as'=>'accounts/{id}/request_form_create','uses'=>'CreditSaleController@get_create_credit_sale_request','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		
		//Credit Sale 
			Route::get('credit_sales',array('as'=>'credit_sales','uses'=>'CreditSaleController@get_create_credit_sales','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::get('search_item_ajax_search',array('as'=>'search_item_ajax_search','uses'=>'CreditSaleController@search_item_ajax_search','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::get('search_customer_name_ajax',array('as'=>'search_customer_name_ajax','uses'=>'CreditSaleController@search_customer_name_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('post_item_cart_ajax',array('as'=>'post_item_cart_ajax','uses'=>'CreditSaleController@post_item_cart_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('post_item_add_to_cart_ajax',array('as'=>'post_item_add_to_cart_ajax','uses'=>'CreditSaleController@post_item_add_to_cart_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('credit_sales/{id}/remove_card_by_id',array('as'=>'credit_sales/{id}/remove_card_by_id','uses'=>'CreditSaleController@post_remove_cart_with_item_id','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::get('post_cancel_all_cart_item',array('as'=>'post_cancel_all_cart_item','uses'=>'CreditSaleController@post_cancel_all_cart_item','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('post_cancel_all_cart_item_ajax',array('as'=>'post_cancel_all_cart_item_ajax','uses'=>'CreditSaleController@post_cancel_all_cart_item_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			
			
			Route::post('credit_sales/{id}/update_card_by_id',array('as'=>'credit_sales/{id}/update_card_by_id','uses'=>'CreditSaleController@post_item_update_to_cart_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('pos_add_all_session_credit_sale',array('as'=>'pos_add_all_session_credit_sale','uses'=>'CreditSaleController@pos_add_all_session_credit_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('get_add_all_session_credit_sale',array('as'=>'get_add_all_session_credit_sale','uses'=>'CreditSaleController@get_add_all_session_credit_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			//Route::post('credit_sales/{id}/remove_tran_cash',array('as'=>'credit_sales/{id}/remove_tran_cash','uses'=>'CreditSaleController@remove_tran_cash','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('credit_sales/{id}/remove_tran_cash',array('as'=>'credit_sales/{id}/remove_tran_cash','uses'=>'CreditSaleController@remove_tran_cash','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('completed_sale',array('as'=>'completed_sale','uses'=>'CreditSaleController@completed_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::get('credit_sales/{id}/invoice',array('as'=>'credit_sales/{id}/invoice','uses'=>'CreditSaleController@get_show_invoice','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::get('credit_sales/{id}/sale_invoice_json',array('as'=>'credit_sales/{id}/sale_invoice_json','uses'=>'CreditSaleController@get_show_sale_invoice_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			Route::post('credit_sales/{id}/delete_database',array('as'=>'credit_sales/{id}/delete_database','uses'=>'CreditSaleController@deleted_sale_from_database','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

			
	//
//End Product 
// Search Iterm
	Route::get('search_schedule_ajax_search_co',array('as'=>'search_schedule_ajax_search_co','uses'=>'SearchajaxController@search_schedule_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('search_Client_ajax',array('as'=>'search_Client_ajax','uses'=>'SearchajaxController@search_Client_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('search_schedule_give_to_co_ajax',array('as'=>'search_schedule_give_to_co_ajax','uses'=>'SearchajaxController@search_schedule_give_to_co_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('search_schedule_id_ajax',array('as'=>'search_schedule_id_ajax','uses'=>'SearchajaxController@search_schedule_id_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('schedule_search_all',array('as'=>'schedule_search_all','uses'=>'SearchajaxController@schedule_search_all','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	
// End Search Item

// Give Product To CO
	Route::get('given_products',array('as'=>'given_products','uses'=>'Given_ProductController@get_list_given_to_co','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('given_products/create',array('as'=>'given_products/create','uses'=>'Given_ProductController@get_create_gevin_to_co','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::post('given_products/create',array('as'=>'given_products/create','uses'=>'Given_ProductController@post_create_give_to_co','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('given_products/show',['as'=>'given_products/show','uses'=>'Given_ProductController@show_detail_give_product','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']]);
	
	Route::post('get_data_function/{id}',array('as'=>'get_data_function/{id}','uses'=>'Given_ProductController@get_data_function','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::post('show_give_product_to_co/{id}',array('as'=>'show_give_product_to_co/{id}','uses'=>'Given_ProductController@get_data_give_to_co_by_id','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

	Route::post('deleted_give_product_to_co/{id}',array('as'=>'deleted_give_product_to_co/{id}','uses'=>'Given_ProductController@deleted_give_product_to_co','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::post('deleted_from_database_give_product_to_co/{id}',array('as'=>'deleted_from_database_give_product_to_co/{id}','uses'=>'Given_ProductController@deleted_from_database_give_product_to_co','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::post('delete_data_give_to_client/{id}',array('as'=>'delete_data_give_to_client/{id}','uses'=>'Given_ProductController@delete_data_give_to_client','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	
	  
	
	
// Give Product To CO
	Route::get('given_products/client',array('as'=>'given_productsgiven_products/client','uses'=>'Given_ProductController@get_list_given_to_cl','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('given_products/client/create',array('as'=>'given_products/client/create','uses'=>'Given_ProductController@get_create_gevin_to_cl','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

// Approval Credit Sale 
	// View list Waiting credit sale 
		Route::get('get_sale_by_wating_credit_json',array('as'=>'get_sale_by_wating_credit_json','uses'=>'ApprovalCreditController@get_sale_by_wating_credit_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		// Route::get('get_sale_by_not_agree_credit_json',array('as'=>'get_sale_by_wating_credit_json','uses'=>'ApprovalCreditController@get_sale_by_wating_credit_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		// Route::get('get_sale_by_agree_credit_json',array('as'=>'get_sale_by_wating_credit_json','uses'=>'ApprovalCreditController@get_sale_by_wating_credit_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		// Route::get('get_sale_by_not_yet_agree_credit_json',array('as'=>'get_sale_by_wating_credit_json','uses'=>'ApprovalCreditController@get_sale_by_wating_credit_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		
		Route::get('aprove_credit_sales',array('as'=>'aprove_credit_sales','uses'=>'ApprovalCreditController@get_sale_by_credit','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('aprove_credit_sales/{id}/create',array('as'=>'aprove_credit_sales/{id}/create','uses'=>'ApprovalCreditController@get_aprove_credit_sales_create','middleware'=>'permissions','permissions'=>'create-client'));
		Route::post('aprove_credit_sales/{id}/create' ,array('as'=>'aprove_credit_sales/{id}/create','uses'=>'ApprovalCreditController@post_aprove_credit_sales_create','middleware'=>'permissions','permissions'=>'create-client'));
		Route::get('aprove_credit_sales/{id}/create_json' ,array('as'=>'aprove_credit_sales/{id}/create_json','uses'=>'ApprovalCreditController@get_sale_by_id_aprove_credit_sales_create','middleware'=>'permissions','permissions'=>'create-client'));
		Route::get('aprove_credit_sales/{id}/show' ,array('as'=>'aprove_credit_sales/{id}/show','uses'=>'ApprovalCreditController@get_aprove_credit_sales_show','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('aprove_credit_sales/{id}/show_json' ,array('as'=>'aprove_credit_sales/{id}/show_json','uses'=>'ApprovalCreditController@get_aprove_credit_sales_show_with_json','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('aprove_credit_sales/{id}/edit' ,array('as'=>'aprove_credit_sales/{id}/edit','uses'=>'ApprovalCreditController@get_aprove_credit_sales_edit','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::get('aprove_credit_sales/{id}/json_edit' ,array('as'=>'aprove_credit_sales/{id}/json_edit','uses'=>'ApprovalCreditController@get_json_aprove_credit_sales_edit','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::post('aprove_credit_sales/{id}/edit' ,array('as'=>'aprove_credit_sales/{id}/edit','uses'=>'ApprovalCreditController@post_aprove_credit_sales_edit','middleware'=>'permissions','permissions'=>'edit-client'));
		Route::post('aprove_credit_sales/{id}/deleted' ,array('as'=>'aprove_credit_sales/{id}/deleted','uses'=>'ApprovalCreditController@post_delete_aprove_credit_sales_edit','middleware'=>'permissions','permissions'=>'edit-client'));	
		
		// 
		Route::get('aprove_credit_sales/{id}/generate_credit_sale',array('as'=>'aprove_credit_sales/{id}/generate_credit_sale','uses'=>'ScheduleController@show_generate_schedule','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('aprove_credit_sales/{id}/generate_credit_sale',array('as'=>'aprove_credit_sales/{id}/generate_credit_sale','uses'=>'ScheduleController@generate_schedule','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('aprove_credit_sales/{id}/schedule_show_json',array('as'=>'aprove_credit_sales/{id}/schedule_show_json','uses'=>'ScheduleController@schedule_show_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

		Route::get('aprove_credit_sales/{id}/print_contract',array('as'=>'aprove_credit_sales/{id}/print_contract','uses'=>'ApprovalCreditController@get_contract_aprove_credit_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

//

// Comment Finish
	Route::get('comment_finish',['as'=>'comment_finish','uses'=>'CommentFinishController@index_comment_finish']);
	Route::get('comment_finish/json',['as'=>'comment_finish/json','uses'=>'CommentFinishController@index_comment_finish_json']);
	Route::get('comment_finish/create',['as'=>'comment_finish/show','uses'=>'CommentFinishController@create_comment_finish']);
	Route::get('comment_finish/create/json',['as'=>'comment_finish/show/json','uses'=>'CommentFinishController@create_comment_json']);
	Route::post('comment_finish/post/json',['as'=>'comment_finish/post/json','uses'=>'CommentFinishController@post_comment_finish_json']);
	Route::post('delete/comment/{id}',['as'=>'delete/comment/{id}','uses'=>'CommentFinishController@delete_comment']);
	/// ======> Comment Error <======
		Route::get('comment_error',['as'=>'comment_error','uses'=>'CommentFinishController@comment_error']);
		Route::get('comment_error/json',['as'=>'comment_error/json','uses'=>'CommentFinishController@comment_error_json']);

// Comment Finish
// Add Session 

		Route::post('add_session_new' ,array('as'=>'add_session_new','uses'=>'SessionController@add_session_new','middleware'=>'permissions','permissions'=>'edit-client'));	
		
//
// Go to Dashboard 	
  	Route::get('/',['as'=>'/', 'uses'=>'HomeController@get_welcome','middleware'=>'permissions']);
  	Route::get('/test',['as'=>'/test', 'uses'=>'HomeController@get_test','middleware'=>'permissions']);
//Login & Logout
	Route::get('login',array('as'=>'login','uses'=>'LoginController@getlogin'));
	Route::post('login',array('as'=>'login','uses'=>'LoginController@postlogin'));
	Route::get('logout',array('as'=>'logout','uses'=>'LoginController@getlogout'));
//History_log
	//combind
	Route::get('history_log',array('as'=>'history_log','uses'=>'HistorylogController@history_log'));

//Currency
//combind
Route::get('currency',['as'=>'currency','uses'=>'CurrencyController@get_list']);
Route::get('currency/create',['as'=>'currency/create','uses'=>'CurrencyController@get_create']);
Route::post('currency/create',['as'=>'currency/create','uses'=>'CurrencyController@post_create']);
Route::get('currency/{id}/edit',['as'=>'currency/{id}/edit','uses'=>'CurrencyController@get_edit']);
Route::post('currency/{id}/edit',['as'=>'currency/{id}/edit','uses'=>'CurrencyController@post_edit']);
Route::post('currency/{id}/deleted',['as'=>'currency/{id}/deleted','uses'=>'CurrencyController@post_delete']);

//rate_percent
Route::get('rate_percent',['as'=>'rate_percent','uses'=>'Rate_percentController@get_list']);
Route::get('rate_percent/create',['as'=>'rate_percent/create','uses'=>'Rate_percentController@get_create']);
Route::post('rate_percent/create',['as'=>'rate_percent/create','uses'=>'Rate_percentController@post_create']);
Route::get('rate_percent/{id}/edit',['as'=>'rate_percent/{id}/edit','uses'=>'Rate_percentController@get_edit']);
Route::post('rate_percent/{id}/edit',['as'=>'rate_percent/{id}/edit','uses'=>'Rate_percentController@post_edit']);
Route::post('rate_percent/{id}/deleted',['as'=>'currency/{id}/deleted','uses'=>'Rate_percentController@post_delete']);
// Configure
Route::get('configure',['as'=>'configure','uses'=>'ConfigureController@get_list']);
//Iterest Rate
	//Combind
	Route::get('interest_rate', array('as'=>'interest_rate','uses'=>'IterestController@get_interest_rate_list','middleware'=>'permissions','permissions'=>'create-interest_rate'));
	Route::get('interest_rate/create',array('as'=>'interest_rate/create','uses'=>'IterestController@get_iterest_rate_create','middleware'=>'permissions','permissions'=>'create-interest_rate'));
	Route::post('interest_rate/create',array('as'=>'interest_rate/create','uses'=>'IterestController@post_iterest_rate_create','middleware'=>'permissions','permissions'=>'create-interest_rate'));
	Route::get('interest_rate/{id}/edit',array('as'=>'interest_rate/{id}/edit','uses'=>'IterestController@get_iterest_rate_edit','middleware'=>'permissions','permissions'=>'edit-interest_rate'));
	Route::post('interest_rate/{id}/edit',array('as'=>'interest_rate/{id}/edit','uses'=>'IterestController@post_iterest_rate_edit','middleware'=>'permissions','permissions'=>'edit-interest_rate'));
//New Iterest Rate
	//Combaind
	Route::get('new_interest_rate', array('as'=>'new_interest_rate','uses'=>'IterestController@get_new_interest_rate_list','middleware'=>'permissions','permissions'=>'create-interest_rate'));
	Route::get('new_interest_rate/create',array('as'=>'new_interest_rate/create','uses'=>'IterestController@get_new_interest_rate_create','middleware'=>'permissions','permissions'=>'create-interest_rate'));
	Route::post('new_interest_rate/create',array('as'=>'new_interest_rate/create','uses'=>'IterestController@post_new_interest_rate_create','middleware'=>'permissions','permissions'=>'create-interest_rate'));
	Route::get('new_interest_rate/{id}/edit',array('as'=>'new_interest_rate/{id}/edit','uses'=>'IterestController@get_new_interest_rate_edit','middleware'=>'permissions','permissions'=>'edit-interest_rate'));
	Route::post('new_interest_rate/{id}/edit',array('as'=>'new_interest_rate/{id}/edit','uses'=>'IterestController@post_new_interest_rate_edit','middleware'=>'permissions','permissions'=>'edit-interest_rate'));
	Route::post('new_interest_rate/{id}/delete',array('as'=>'new_interest_rate/{id}/delete','uses'=>'IterestController@post_new_interest_rate_delete','middleware'=>'permissions','permissions'=>'edit-interest_rate'));
	//Public Holiday
//Combind
		Route::get('public_holiday',['us'=>'public_holiday','uses'=>'PublicholidayController@get_public_holiday','middleware'=>'permissions','permissions'=>'list-brand']);
	Route::get('public_holiday/create',['us'=>'public_holiday/create','uses'=>'PublicholidayController@get_public_holiday_create','middleware'=>'permissions','permissions'=>'create-brand']);
	Route::post('public_holiday/create',['us'=>'public_holiday/create','uses'=>'PublicholidayController@post_public_holiday_create','middleware'=>'permissions','permissions'=>'create-brand']);
	Route::get('public_holiday/{id}/edit',['us'=>'public_holiday/{id}/edit','uses'=>'PublicholidayController@get_public_holiday_edit','middleware'=>'permissions','permissions'=>'edit-brand']);
	Route::post('public_holiday/{id}/edit',['us'=>'public_holiday/{id}/edit','uses'=>'PublicholidayController@post_public_holiday_edit','middleware'=>'permissions','permissions'=>'edit-brand']);
	Route::get('public_holiday/{id}/deleted',['us'=>'public_holiday/{id}/deleted','uses'=>'PublicholidayController@get_public_holiday_deleted','middleware'=>'permissions','permissions'=>'deletd-brand']);
	//Exchange Rate
	//Combind
 	Route::get('exchange_rate', array('as'=>'exchange_rate','uses'=>'ExchangeRateController@show_list' ,'middleware'=>'permissions','permissions'=>'exchange-rate'));
 	Route::get('exchange_rate/{id}/edit', array('as'=>'exchange_rate/{id}/edit','uses'=>'ExchangeRateController@get_edit' ,'middleware'=>'permissions','permissions'=>'edit-exchange-rate'));
 	Route::post('exchange_rate/{id}/edit', array('as'=>'exchange_rate/{id}/edit','uses'=>'ExchangeRateController@post_edit' ,'middleware'=>'permissions','permissions'=>'edit-exchange-rate'));
 	Route::post('exchange_rate/{id}/delete', array('as'=>'exchange_rate/{id}/delete','uses'=>'ExchangeRateController@post_deleted' ,'middleware'=>'permissions','permissions'=>'Delete-exchange-rate'));
 	//Penaty date
 	 //Combind
	Route::get('penaty_date',['us'=>'penaty_date','uses'=>'PenatyController@index','middleware'=>'permissions','permissions'=>'list-brand']);
	Route::get('penaty_date/create',['us'=>'penaty_date/create','uses'=>'PenatyController@create','middleware'=>'permissions','permissions'=>'create-brand']);
	Route::post('penaty_date/create',['us'=>'penaty_date/create','uses'=>'PenatyController@store','middleware'=>'permissions','permissions'=>'create-brand']);
	Route::get('penaty_date/{id}/edit', ['us'=>'penaty_date/{id}/edit', 'uses'=>'PenatyController@edit','middleware'=>'permissions','permissions'=>'edit-brand']);
	Route::post('penaty_date/{id}/update', ['us'=>'penaty_date/{id}/update', 'uses'=>'PenatyController@update','middleware'=>'permissions','permissions'=>'edit-brand']);
	Route::get('penaty_date/{id}/delete', ['us'=>'penaty_date/{id}/delete', 'uses'=>'PenatyController@delete','middleware'=>'permissions','permissions'=>'deletd-brand']);

	//Position
		//list
		Route::get('list_position',array('as'=>'list_position','uses'=>'UserController@list_position','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		//create
		Route::get('position/create',array('as'=>'position/create','uses'=>'UserController@position_create','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		Route::post('position/create',array('as'=>'position/create','uses'=>'UserController@post_position_create','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		//edit
		Route::get('position/{id}/edit',array('as'=>'position/{id}/edit' , 'uses'=>'UserController@position_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		Route::post('position/{id}/edit',array('as'=>'position/{id}/edit' , 'uses'=>'UserController@post_position_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
		//delete
		Route::post('position/{id}/deleted',array('as'=>'position/{id}/deleted','uses'=>'UserController@post_position_deleted','middleware'=>'permissions','permissions'=>'delete-user-groups'));
		// Module Interest
	// list
	Route::get('module_interest',array('as'=>'module_interest','uses'=>'ModuleInterestController@view','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
	// create
	Route::get('module_interest/create',array('as'=>'module_interest/create','uses'=>'ModuleInterestController@get_module_create','middleware'=>'permissions','permissions'=>'create-user-groups'));
	Route::post('module_interest/create',array('as'=>'module_interest/create','uses'=>'ModuleInterestController@post_module_create','middleware'=>'permissions','permissions'=>'create-user-groups'));
	// edit
	Route::get('module_interest/{id}/edit',array('as'=>'module_interest/{id}/edit' , 'uses'=>'ModuleInterestController@get_module_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
	Route::post('module_interest/{id}/edit',array('as'=>'module_interest/{id}/edit' , 'uses'=>'ModuleInterestController@post_module_edit','middleware'=>'permissions','permissions'=>'edit-user-groups'));
	//delete
	Route::post('module_interest/{id}/deleted',array('as'=>'module_interest/{id}/deleted','uses'=>'ModuleInterestController@post_module_deleted','middleware'=>'permissions','permissions'=>'delete-user-groups'));
//End Product 
 
// Purchase Order  

		Route::get('credit_sales_purchas_order',array('as'=>'credit_sales_purchas_order','uses'=>'PurchaseOrderController@credit_sales_purchas_order','middleware'=>'permissions','permissions'=>['list-user-groups','create-user-groups','edit-user-groups','delete-user-groups']));
		Route::post('pos_add_all_session_purchas_order',array('as'=>'pos_add_all_session_purchas_order','uses'=>'PurchaseOrderController@pos_add_all_session_purchas_order','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('get_add_all_session_purchas_order',array('as'=>'get_add_all_session_purchas_order','uses'=>'PurchaseOrderController@get_add_all_session_purchas_order','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('post_item_cart_ajax_purchas_order',array('as'=>'post_item_cart_ajax_purchas_order','uses'=>'PurchaseOrderController@post_item_cart_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('post_item_add_to_cart_ajax_purchas_order',array('as'=>'post_item_add_to_cart_ajax_purchas_order','uses'=>'PurchaseOrderController@post_item_add_to_cart_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('credit_sales_purchas_order/{id}/remove_card_by_id',array('as'=>'credit_sales_purchas_order/{id}/remove_card_by_id','uses'=>'PurchaseOrderController@post_remove_cart_with_item_id','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('post_cancel_all_cart_item_purchas_order',array('as'=>'post_cancel_all_cart_item_purchas_order','uses'=>'PurchaseOrderController@post_cancel_all_cart_item','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('search_item_ajax_search_po',array('as'=>'search_item_ajax_search_po','uses'=>'PurchaseOrderController@search_item_ajax_search','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

		Route::post('credit_sales_purchas_order/{id}/update_card_by_id',array('as'=>'credit_sales_purchas_order/{id}/update_card_by_id','uses'=>'PurchaseOrderController@post_item_update_to_cart_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		//Route::post('credit_sales_purchas_order/{id}/remove_tran_cash',array('as'=>'credit_sales_purchas_order/{id}/remove_tran_cash','uses'=>'PurchaseOrderController@remove_tran_cash','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('credit_sales_purchas_order/{id}/remove_tran_cash',array('as'=>'credit_sales_purchas_order/{id}/remove_tran_cash','uses'=>'PurchaseOrderController@remove_tran_cash','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('completed_purchas_order',array('as'=>'completed_purchas_order','uses'=>'PurchaseOrderController@completed_purchas_order','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('credit_sales_purchas_order/{id}/invoice',array('as'=>'credit_sales_purchas_order/{id}/invoice','uses'=>'PurchaseOrderController@get_show_invoice','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('credit_sales_purchas_order/{id}/receive_invoice',array('as'=>'credit_sales_purchas_order/{id}/receive_invoice','uses'=>'PurchaseOrderController@get_show_receive_invoice','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('credit_sales_purchas_order/{id}/invoice_json',array('as'=>'credit_sales_purchas_order/{id}/invoice_json','uses'=>'PurchaseOrderController@get_show_invoice_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('credit_sales_purchas_order/{id}/delete_database',array('as'=>'credit_sales_purchas_order/{id}/delete_database','uses'=>'PurchaseOrderController@deleted_sale_from_database','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::post('post_cancel_all_purchas_order_ajax',array('as'=>'post_cancel_all_purchas_order_ajax','uses'=>'PurchaseOrderController@post_cancel_all_cart_item_ajax','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
			

		//End Purchase Order 
  
 
// Client Payment Back 
Route::get('payment_credit_back',array('as'=>'payment_credit_back','uses'=>'PaymentBackController@get_payment_back','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
Route::get('client_payment_back',['as'=>'client_payment_back','uses'=>'SearchajaxController@search_schedule_payment']);
Route::post('schedule_check_timesheet_pay/{id}',['as'=>'schedule_check_timesheet_pay/{id}','uses'=>'PaymentBackController@schedule_check_timesheet_pay','middleware'=>'permissions','permissions'=>['deleted-client']]);
Route::post('client_payment_back',['as'=>'client_payment_back','uses'=>'PaymentBackController@client_payment_back','middleware'=>'permissions','permissions'=>['deleted-client']]);
Route::get('list_payment_back',['as'=>'list_payment_back','uses'=>'PaymentBackController@list_payment_back','middleware'=>'permissions','permissions'=>['deleted-client']]);

Route::get('list_schedule_timesheet_pay_json',['as'=>'list_schedule_timesheet_pay_json','uses'=>'PaymentBackController@list_schedule_timesheet_pay_json','middleware'=>'permissions','permissions'=>['deleted-client']]);
Route::POST('delete_payment_timesheet/{id}',['as'=>'delete_payment_timesheet/{id}','uses'=>'PaymentBackController@delete_payment_timesheet','middleware'=>'permissions','permissions'=>['deleted-client']]);



//End Client Payment Back 

	//Account_type (Customer_type)
	//combind
Route::get('account_type',['as'=>'account_type','uses'=>'Account_typeController@get_list']);
Route::get('account_type/create',['as'=>'account_type/create','uses'=>'Account_typeController@get_create']);
Route::post('account_type/create',['as'=>'account_type/create','uses'=>'Account_typeController@post_create']);
Route::get('account_type/{id}/edit',['as'=>'account_type/{id}/edit','uses'=>'Account_typeController@get_edit']);
Route::post('account_type/{id}/edit',['as'=>'account_type/{id}/edit','uses'=>'Account_typeController@post_edit']);
Route::post('account_type/{id}/deleted',['as'=>'account_type/{id}/deleted','uses'=>'Account_typeController@post_delete']);

 
// Report 

	Route::get('report/report_receiving_product',array('as'=>'report/report_receiving_product','uses'=>'ReportReceivingController@report_receiving_product','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/report_receiving_product_json',array('as'=>'report/report_receiving_product_json','uses'=>'ReportReceivingController@report_receiving_product_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/report_po_product',array('as'=>'report/report_po_product','uses'=>'ReportReceivingController@report_po_product','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/report_po_product_json',array('as'=>'report/report_po_product_json','uses'=>'ReportReceivingController@report_po_product_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

	Route::get('report/report_schedules',array('as'=>'report/report_schedules','uses'=>'ReportScheduleController@get_report_schedule','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/report_schedules_json',array('as'=>'report/report_schedules_json','uses'=>'ReportScheduleController@get_report_schedule_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

	Route::get('report/report_sale',array('as'=>'report/report_sale','uses'=>'ReportSaleController@report_sale','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/report_sale_json',array('as'=>'report/report_sale_json','uses'=>'ReportSaleController@report_sale_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

	Route::get('report/sales/report_sale_by_credit',array('as'=>'report/sales/report_sale_by_credit','uses'=>'report\ReportSaleByCreditController@report_sale_by_credit','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/sales/report_sale_by_credit_json',array('as'=>'report/sales/report_sale_by_credit_json','uses'=>'report\ReportSaleByCreditController@report_sale_by_credit_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	
	Route::get('report/sales/report_direct_payment',array('as'=>'report/sales/report_direct_payment','uses'=>'report\ReportDirectPaymentController@report_direct_payment','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/sales/report_direct_payment_json',array('as'=>'report/sales/report_direct_payment_json','uses'=>'report\ReportDirectPaymentController@report_direct_payment_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	
	  
	
	Route::get('report/report_sale_by_credit',array('as'=>'report/report_sale_by_credit','uses'=>'ReportSaleController@report_sale_by_credit','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('report/report_sale_by_credit_json',array('as'=>'report/report_sale_by_credit_json','uses'=>'ReportSaleController@report_sale_by_credit_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	
	// Report Export to Excel
	Route::get('export_report',array('as'=>'export_report','uses'=>'ReportScheduleController@export_report_to_excel','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('export_report_receive',array('as'=>'export_report_receive','uses'=>'ReportReceivingController@export_report_receive_to_excel','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	Route::get('export_report_po',array('as'=>'export_report_po','uses'=>'ReportReceivingController@export_report_purches_to_excel','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		
   // Report Cleint 
   		Route::get('report/clients/report_client_list',array('as'=>'report/clients/report_client_list','uses'=>'report\ClientController@report_client_list','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		Route::get('report/clients/report_client_list_json',array('as'=>'report/clients/report_client_list_json','uses'=>'report\ClientController@report_client_list_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
		
		   
	 // End Report Client 
	 Route::get('report/item/report_item_list',array('as'=>'report/item/report_item_list','uses'=>'report\ItemController@report_item_list','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	 Route::get('report/item/report_item_list_json',array('as'=>'report/item/report_item_list_json','uses'=>'report\ItemController@report_item_list_json','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));

	 // Report Item
	 Route::get('report/payment_back/report_payment_back',array('as'=>'report/payment_back/report_payment_back','uses'=>'report\ReportRepayBackController@ShowData','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	 Route::get('report/payment_back/report_payment_back_json',array('as'=>'report/payment_back/report_payment_back_json','uses'=>'report\ReportRepayBackController@getDataJson','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	

	 // Report Repayment Back   

	 // Report Item
	 Route::get('report/payment_back/report_summery_payment_back',array('as'=>'report/payment_back/report_summery_payment_back','uses'=>'report\ReportSummeryPBController@ShowData','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	 Route::get('report/payment_back/report_payment_summery_back_json',array('as'=>'report/payment_back/report_payment_summery_back_json','uses'=>'report\ReportSummeryPBController@getDataJson','middleware'=>'permissions','permissions'=>['list-client','create-client','edit-client','deleted-client']));
	 
	 // Search Report
	 Route::get('report/search/item',['as'=>'report/search/item','uses'=>'report\SearchController@searchItem']);
	 Route::get('report/search/client',['as'=>'report/search/client','uses'=>'report\SearchController@searchClient']);
	 Route::get('report/search/sale_by_credit',['as'=>'report/search/sale_by_credit','uses'=>'report\SearchController@searchSaleCredit']);
	 Route::get('report/search/sale_by_cash',['as'=>'report/search/sale_by_cash','uses'=>'report\SearchController@searchDirectPay']);
	 Route::get('report/search/schedule',['as'=>'report/search/schedule','uses'=>'report\SearchController@searchSchedule']);
	 Route::get('report/search/sale',['as'=>'report/search/sale','uses'=>'report\SearchController@searchSale']);
	 Route::get('report/search/sale_on_credit',['as'=>'report/search/sale_on_credit','uses'=>'report\SearchController@searchSaleByCredit']);
	 Route::get('report/search/receiving_product',['as'=>'report/search/receiving_product','uses'=>'report\SearchController@searchReceiving']);
	 Route::get('report/search/purchase',['as'=>'report/search/purchase','uses'=>'report\SearchController@searchReceivingPO']);
	 Route::get('report/search/payment_back',['as'=>'report/search/payment_back','uses'=>'report\SearchController@searchPayBack']);

// End Report 
Route::get('locale/{locale}',function($locale){
	Session::put('locale', $locale);
	return Redirect()->back();
});
