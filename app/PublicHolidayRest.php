<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicHolidayRest extends Model
{
    //
    protected $table = "mfi_public_holiday_reset";

    public function publicholiday(){
        return $this->belongsTo('App\PublicHoliday','mfi_public_holiday_id');
    }
}
