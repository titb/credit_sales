<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Client extends Model
{
    protected $table = "cs_clients";
    
    public function sub_clients(){
    	return $this->belongsToMany('App\Sub_client','cs_client_andsub_clients','client_id','sub_client_id');
    }
    public function third_clients(){
        return $this->hasMany('App\ThirdClient','client_id');
    }

    public function client_type(){
    	return $this->belongsTo('App\ClientType','client_type_id');
    }

    public function cs_cds_request_form(){
       return $this->hasMany('App\Cs_Cds_Request_Form','client_id');
    }
    public function schedule(){
        return $this->hasMany('App\Cs_Schedule','client_id');
    }
    public function cs_sale(){
        return $this->hasMany('App\Cs_Sale','client_id');
    }
    public function branch(){
        return $this->belongsTo('App\Branch' ,'branch_id');
    }

    
}
