<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_co_give_item_client extends Model
{
    protected $table = "cs-co-give-item-client";
    public function schedule(){
        return $this->belongsTo('App\Cs_Schedule','schedule_id');
    }
    public function client(){
        return $this->belongsTo('App\Cs_Client','client_id'); 
    }
    
    public function user(){
        return $this->belongsTo('App\User','user_id'); 
    }
    public function staff(){
        return $this->belongsTo('App\User','staff_id'); 
    }
    public function staff_give(){
        return $this->belongsTo('App\User','staff_give_id'); 
    }
    public function approve_by(){
        return $this->belongsTo('App\User','approve_by'); 
    }
    public function give_by(){
        return $this->belongsTo('App\User','give_by'); 
    }
    
}
