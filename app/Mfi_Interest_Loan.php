<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfi_Interest_Loan extends Model
{
     protected $table = "mfi_interest_loan";

    public function module(){
        return $this->belongsTo('App\Module_Interest','module_interest_id');
    }    

    // public function mfi_credit_committy(){

    // 	return $this->hasOne('App\Mfi_Credit_Committy','mfi_interest_rate_id');

    // } 
    // public function mfi_planfor_bussiness(){
    	
    // 	return $this->hasOne('App\Mfi_Planfor_Bussiness','mfi_interest_rate_id');
    // } 
    // public function mfi_planfor_bussiness_group(){
    	
    // 	return $this->hasOne('App\Mfi_Planfor_Bussiness_Group','interest_rate_id');
    // } 
    //  public function mfi_credit_committies_groups(){
    	
    // 	return $this->hasOne('App\Mfi_Credit_Committies_Group','interest_rate_id');
    // } 
}
