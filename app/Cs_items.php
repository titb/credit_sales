<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Items extends Model
{
    //
    protected $table = "cs_items";
    
    public function inventorys(){
        return $this->hasMany('App\Cs_inventorys','item_id');
    }

    public function categorys(){
        return $this->belongsTo('App\Cs_categorys','category_id');
    }
    public function cs_suppliers(){
        return $this->belongsTo('App\Cs_suppliers','supplier_id');
    }
    public function cs_brand(){
        return $this->belongsTo('App\Cs_brand','brand_id');
    }
    public function cs_item_tax(){
        return $this->hasMany('App\Cs_item_tax','item_id');
    }

}
