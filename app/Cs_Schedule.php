<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
 
class Cs_Schedule extends Model
{
    // 
    const STATUS_ACTIVE = '1';
    const STATUS_INACTIVE = '0';

    /**
     * List of statuses.
     *
     * @var array
     */
    public static $statuses = [self::STATUS_ACTIVE, self::STATUS_INACTIVE];


    protected $table = "cs-schedules";

    public function cs_sale(){
        return $this->belongsTo('App\Cs_Sale','sale_id');
    }
    public function branch(){
        return $this->belongsTo('App\Branch','branch_id');
    }
    public function cs_client(){
        return $this->belongsTo('App\Cs_Client','client_id');
    }
    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    public function cs_staff(){
        return $this->belongsTo('App\User','staff_id');
    }

    public function cs_approvalcredit(){
        return $this->belongsTo('App\Cs_ApprovalCredit','approval_id');
    }
    public function cs_give_to_client(){
        return $this->hasOne('App\Cs_co_give_item_client','schedule_id');
    } 
    public function cs_schedule_timesheet(){
        return $this->hasMany('App\Cs_Schedule_Timesheet','cs_schedules_id');
    }

    public function cs_schedule_timesheet_pay(){
        return $this->hasMany('App\Cs_Schedule_Timesheet_Pay','cs_schedules_id');
    }

    public function currency(){
        return $this->belongsTo('App\Currency','currency_id');
    }


    public function scopeActive($query)
    {
        return $query->where('status', static::STATUS_ACTIVE);
    }

    public function scopeDeleted($query)
    {
        return $query->where('deleted', static::STATUS_ACTIVE);
    }

    public function comment_finish(){
        return $this->hasOne('App\Cs_CommentFinish', 'cs_schedules_id');
    }
    public function manager_id1(){
        return $this->belongsTo('App\User','manager_id1');
    }
    public function manager_id2(){
        return $this->belongsTo('App\User','manager_id2');
    }
    public function manager_id3(){
        return $this->belongsTo('App\User','manager_id3');
    }
}
