<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PublicHoliday extends Model
{
       protected $table="mfi_public_holiday";

       public function holiday_rest(){
           return $this->hasMany("App\PublicHolidayRest","mfi_public_holiday_id");
       }
}
