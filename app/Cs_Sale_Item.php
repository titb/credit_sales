<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Sale_Item extends Model
{
    //
    protected $table = "cs-sale-item";

    public function item(){
        return $this->belongsTo('App\Cs_Items','product_id');
    }
}
