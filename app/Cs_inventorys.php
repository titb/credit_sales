<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_inventorys extends Model
{
    //
    protected $table = "cs-inventorys";

    public function item(){
        return $this->belongsTo('App\Cs_Items','item_id');
    }
}
