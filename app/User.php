<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable
{
    use Notifiable;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
      'username', 'user_email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    // Group Model
    public function groups()
    {
        return $this->belongsToMany('App\Group','user_groups','user_id','group_id');
    }

// ACI
    public function hasAnyGroup($groups){

        if(is_array($groups)){
            foreach ($groups as  $group) {
                if($this->hasGroup($group)){
                    return true;
                }
            }

        }else{

            if($this->hasGroup($groups)){
                    return true;
                }
        }
        return false;
    }

    public function hasGroup($group){
        if($this->groups()->where('name',$group)->first()){
            return true;
        }
        return false;
    }

    public function branch(){

        return $this->belongsTo('App\Branch' ,'branch_id');
    }

    public function mfi_position(){

        return $this->belongsTo('App\Mfi_Position','position_id');

    }

    public function get_user(){
        return $this->where('user_status','=',1)->where('deteted','=',1)->get();
    }
}
