<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Sale extends Model
{
    //
    protected $table = "cs-sales";
    
    public function sale_payment(){
        return $this->hasOne('App\Cs_Sale_Payment','sale_id');
    }
    
    public function sale_transaction_pay(){
        return $this->hasMany('App\Cs_Sale_Payment_Tran','sale_id');
    }

    public function sale_item(){
        return $this->hasMany('App\Cs_Sale_Item','sale_id');
    }

    public function cs_request_form(){
        return $this->hasOne('App\Cs_Cds_Request_Form','sale_id');
    }
    public function cs_approval_credit_sale(){
        return $this->hasOne('App\Cs_ApprovalCredit','sale_id');
    }

    public function cs_client(){
        return $this->belongsTo('App\Cs_Client','client_id');
    }

    public function branch(){

        return $this->belongsTo('App\Branch' ,'branch_id');
    }

    public function user(){
        return $this->belongsTo('App\User','user_id');
    }
    public function cs_staff(){
        return $this->belongsTo('App\User','staff_id');
    }
    public function currency(){
        return $this->belongsTo('App\Currency','currency_id');
    }
}
