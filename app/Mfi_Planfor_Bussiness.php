<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfi_Planfor_Bussiness extends Model
{
     protected $table = "mfi_planfor_bussiness";

    

    public function mif_load_request()

    {

        return $this->belongsTo('App\Mfi_Load_Request' ,'mfi_load_request_id');

    }



    public function mfi_planuses_money(){



    	return $this->hasMany('App\Mfi_Planuses_Money' ,'mfi_planfor_bus_id');



    }

    public function mfi_plan_pawns(){



    	return $this->hasMany('App\Mfi_Plan_Pawn' ,'mfi_planfor_bus_id');



    }

    public function mfi_income_types(){



    	return $this->hasMany('App\Mfi_Income_Type' ,'mfi_planfor_bus_id');



    }



    public function mfi_expend_analbussineses(){



    	return $this->hasMany('App\Mfi_Expend_Analbussinese' ,'mfi_planfor_bus_id');



    }

    public function mfi_borrower_propertys(){



    	return $this->hasMany('App\Mfi_Borrower_Property' ,'mfi_planfor_bus_id');



    }



    public function mfi_planfor_bussiness_groups(){

        

        return $this->hasMany('App\Mfi_Planfor_Bussiness_Group','mfi_planfor_bus_id');

    }
    public function mfi_interest_loan(){

        return $this->belongsTo('App\Mfi_Interest_Loan','mfi_interest_rate_id');

    }

}
