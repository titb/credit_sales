<?php
namespace App\lib;
use App\lib\Session_Search;
use App\Cs_Schedule;
use App\Cs_Schedule_Timesheet_Pay;
use DB;
use Auth;
use Session;

class  Schedule {	
     
    protected $request;

    static function getData(){
        $data_query = Cs_Schedule::with(['cs_sale','branch','cs_client','cs_approvalcredit','cs_give_to_client','cs_schedule_timesheet','cs_schedule_timesheet_pay','branch','user','cs_staff','currency'])
                     ->where('deleted','=',1);
        return $data_query;
    }
    static function getDataAll($request){
        $query = self::getData();
        if($request->has('submit_search')){
            $data_search = Session_Search::search_form($request);
           
             $data_query =  $query->whereBetween('due_date', [$data_search['from_date'],$data_search['to_date']])
                     ->where('branch_id','LIKE',$data_search['brand_name'])
                     ->where('currency_id','LIKE',$data_search['currency'])
                     ->where('client_id','LIKE',$data_search['client_name'])  
                     ->where('sale_id','LIKE',$data_search['sale_id'])
                     ->where('staff_id','LIKE',$data_search['staff_name']);

         }else{
             $data_query = $query;
            
         }
         return  $data_query; 
    }   

    static function get_data_schedule_has_payment($request){
        $query = self::getDataAll($request);
        $data_query = $query->where('status_for_pay','<>',0);
        return $data_query;
    }
    
    static function get_data_by_schedule($schedule_id){
        $query = self::getData();
        $data_query = $query->where('id',$schedule_id);
        return $data_query;
    }
    
    static function get_data_schedule_by_id($schedule_id){
        $query = self::get_data_by_schedule($schedule_id);
        $data = $query->first();
        return $data;
    }

    static function getDatabyGet($request){
        $data_query = self::getDataAll($request);
        $data = $data_query->get();
        return $data;
    }

    static function getDatabyPagination($request,$num = 15){
        $data_query = self::getDataAll($request);
        $data = $data_query->paginate($num);
        return $data;
    }

    // Get Data By ID Schedule 
    static function get_total_available_total_pay_cost($schedule_id){
        $data = self::get_data_schedule_by_id($schedule_id);
        $available_total_pay_cost = $data->cs_schedule_timesheet_pay->sum('available_total_pay_cost');
        return $available_total_pay_cost;
    }

    static function get_total_available_total_pay_interest($schedule_id){
        $data = self::get_data_schedule_by_id($schedule_id);
        $available_total_pay_interest = $data->cs_schedule_timesheet_pay->sum('available_total_pay_interest');
        return $available_total_pay_interest;
    }
    static function get_total_available_total_payment($schedule_id){
        $data = self::get_data_schedule_by_id($schedule_id);
        $available_total_payment = $data->cs_schedule_timesheet_pay->sum('available_total_payment');
        return $available_total_payment;
    }

    static function check_if_schedule_has_payment($request){
        
    }

    static function get_total_payment_all_schedule($schedule_id){
        $data = self::get_data_schedule_by_id($schedule_id);
        $available_total_pay_cost = $data->cs_schedule_timesheet_pay->sum('available_total_pay_cost');
        $available_total_pay_interest = $data->cs_schedule_timesheet_pay->sum('available_total_pay_interest');
        $available_total_payment = $data->cs_schedule_timesheet_pay->sum('available_total_payment');
        $other_payment = $data->cs_schedule_timesheet_pay->sum('other_payment');
        $panalty = $data->cs_schedule_timesheet_pay->sum('panalty');
        $available_remain = $data->cs_schedule_timesheet_pay->sum('available_remain');
        $data_query = [
            'available_total_pay_cost' => $available_total_pay_cost,
            'available_total_pay_interest' => $available_total_pay_interest,
            'available_total_payment' => $available_total_payment,
            'other_payment' => $other_payment,
            'panalty' => $panalty,
            'available_remain' => $available_remain,
        ];
        return $data_query;
    }
    



}