<?php
namespace App\lib;
use App\lib\Session_Search;
use App\Cs_Client;
use App\Cs_Items;
use App\Cs_inventorys;
use DB;
use Auth;
use Session;

class  LibItem {	
     
    static function get_item_query(){
        $data = Cs_Items::where('deleted','=',1);
        return $data;
    }    

    static function get_item_info($is_paginate = false, $num_of_paginat = 10, $local = false, $user_local){
        $data_query = $this->get_item_query();
        // $user_local =  Auth::user()->branch_id;
        if($local){
            $data_query = $data_query->where('branch_id','=',$user_local);
        }

        if($is_paginate){
            $data_query = $data_query->paginate($num_of_paginat);
        }else{
            $data_query = $data_query->get();
        }

        return $data_query;
    }

    static function total_item_count($is_paginate = false, $num_of_paginat = 10, $local = false, $user_local){
        $data_query = $this->get_item_info();
        $data_count = count($data_query);
        return $data_count;
    }

}
