<?php
namespace App\lib;
use DB;
use Auth;
use Session;
class  Session_Search{	
        // search form 
        static function search_form($request){
                    // Search Start date To date
                    if($request->from_date != "" && $request->to_date != ""){
                    // from date
                    $from_date = date('Y-m-d',strtotime($request->from_date));
                    Session::put('from_date',$request->from_date);
                    // To date
                    $to_date = date('Y-m-d',strtotime($request->to_date));
                    Session::put('to_date',$request->to_date);
                }else{
                    // from date
                    $from_date = '1970-01-01';
                    Session::forget('from_date');
                    // To date
                    $to_date = date('Y-m-d');
                    Session::forget('to_date');
                }
                // Search Brand
                if($request->brand_name != ""){
                    $brand_name = $request->brand_name;
                    Session::put('brand_name',$request->brand_name);
                }else{
                    $brand_name = '%_%';
                    Session::forget('brand_name');
                }
                // Search Currency
                if($request->currency != ""){
                    $currency = $request->currency;
                    Session::put('currency',$request->currency);
                }else{
                    $currency = '%_%';
                    Session::forget('currency');
                }
                // Search Client Name
                if($request->client_name != ""){
                    $client_name = $request->client_name;
                    Session::put('client_name',$request->client_name);
                }else{
                    $client_name = '%_%';
                    Session::forget('client_name');
                }
                // Search Schedule ID
                if($request->sale_id != ""){
                    $sale_id = $request->sale_id;
                    Session::put('sale_id',$request->sale_id);
                }else{
                    $sale_id = '%_%';
                    Session::forget('sale_id');
                }
                // Search Staff Name
                if($request->staff_name != ""){
                    $staff_name = $request->staff_name;
                    Session::put('staff_name',$request->staff_name);
                }else{
                    $staff_name = '%_%';
                    Session::forget('staff_name');
                }
                
                $data = [
                    'from_date' => $from_date,
                    'to_date' => $to_date,
                    'brand_name' => $brand_name,
                    'currency' => $currency,
                    'client_name'=> $client_name,
                    'sale_id' => $sale_id,
                    'staff_name' => $staff_name
                ];
                return $data;
        }
    static function g_session(){
        if(Session::has('from_date')){
            $from_date  = Session::get('from_date');
        }else{
            $from_date  = '';
        }
    
        if(Session::has('to_date')){
            $to_date  = Session::get('to_date');
        }else{
            $to_date  = '';
        }
    
        if(Session::has('brand_name')){
            $brand_name  = Session::get('brand_name');
        }else{
            $brand_name  = '';
        }
    
        if(Session::has('currency')){
            $currency  = Session::get('currency');
        }else{
            $currency  = '';
        }
    
        if(Session::has('client_name')){
            $client_name  = Session::get('client_name');
        }else{
            $client_name  = '';
        }
    
        if(Session::has('sale_id')){
            $sale_id  = Session::get('sale_id');
        }else{
            $sale_id  = '';
        }
    
        if(Session::has('staff_name')){
            $staff_name  = Session::get('staff_name');
        }else{
            $staff_name  = '';
        }
    
    }

    static function clear_session_search(){
        Session::forget('from_date');
        Session::forget('to_date');
        Session::forget('brand_name');
        Session::forget('currency');
        Session::forget('client_name');
        Session::forget('sale_id');
        Session::forget('staff_name');
        return;
    }

}