<?php
namespace App\lib;
use App\lib\Session_Search;
use App\Cs_Client;
use App\Cs_Items;
use App\Cs_Sale;
use App\Cs_Sale_Payment;
use App\Purchase_Order;
use App\Cs_Schedule;
use App\Cs_Schedule_Timesheet_Pay;
use App\Cs_Schedule_Timesheet;
use App\Cs_inventorys;
use DB;
use Auth;
use Session;

class  Total_sale {	
    
        static function total_sale_item(){
            $data = Cs_Sale::where('deleted',1)->where('method','<>','sale_recieving');
            return $data;
        }
    // get data no search with function get()
    static function total_sale_item_all(){
            $data_query = self::total_sale_item();
            $data = $data_query->get();
            return $data;
        }
    // get data no search with function paginate()
    static function total_sale_item_paginate($num = 9){
            $data_query =  self::total_sale_item();
            $data = $data_query->paginate($num);
            return $data;
        }
    //

    // get total by localtion 
    static function total_sale_item_localtion($method = null, $local = false, $paginate = false , $numofpage = 10){
            $data_query =  self::total_sale_item();
            if($local){
                $data_query = $data_query->where('branch_id','=',$local);
            }
            if($method != null){
                $data_query = $data_query->where('method','=',$method);   
            }
            if($paginate){
                $data_query = $data_query->paginate($numofpage);
            }else{
                $data_query = $data_query->get();
            }

            return $data_query;
        }
    // get data sale by method 
        // public function total_sale_item_method($method = null, $local = false, $paginate = false , $numofpage = 10){
        //     $data_query =  self::total_sale_item();
        //     if($local){
        //         $data_query = $data_query->where('branch_id','=',$local);
        //     }
        //     if($method != null){
        //         $data_query = $data_query->where('method','=',$method);
        //     }
        //     if($paginate){
        //         $data_query = $data_query->paginate($numofpage);
        //     }else{
        //         $data_query = $data_query->get();
        //     }

        //     return $data_query;
        // }


    // get  total item sale 
    static function get_total_sale_item($method = null, $local = false, $paginate = false , $numofpage = 10){
             $data =  self::total_sale_item_localtion($method, $local, $paginate, $numofpage);
             $total_item = count($data);
             return $total_item;
        }
    // get toal sale in payment
    static function get_total_sale_item_payment($method = null, $local = false, $paginate = false , $numofpage = 10){
            $data = self::total_sale_item_localtion($method, $local, $paginate, $numofpage);
            //$total_payment_pay = 0; $total_payment_credit = 0;
            $total_payment = 0;
            if(!empty($data)){
                foreach($data as $pay){
                    if(!empty($pay->sale_payment)){
                        // $total_payment_pay +=  $pay->sale_payment->dute_payment_amount;
                        $total_payment += $pay->sale_payment->dute_payment_amount;
                    }
                    if(!empty($pay->cs_approval_credit_sale)){
                        // $total_payment_credit +=  $pay->cs_approval_credit_sale->prices_total_num;
                        $total_payment += $pay->cs_approval_credit_sale->prices_total_num;
                    }

                   
                }
            }
            
           // $total_payment = $total_payment_pay + $total_payment_credit;
            return $total_payment;
        }

       

}