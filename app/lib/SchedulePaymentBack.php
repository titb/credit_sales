<?php
namespace App\lib;
use App\lib\Session_Search;
use App\Cs_Schedule;
use App\Cs_Schedule_Timesheet_Pay;
use DB;
use Auth;
use Session;

class  SchedulePaymentBack {
    protected $request;
    static function getData(){
             $data_query = Cs_Schedule_Timesheet_Pay::with(['cs_schedule.cs_client','cs_schedule.currency','branch','user','cs_staff'])
                     ->where('deleted','=',1);
                     
         return  $data_query; 
    }

    // static function getDataPay(){
    //     $data_query = Cs_Schedule_Timesheet_Pay::join('')
    //      return  $data_query; 
    // }

    static function GetDataSearch($request){
        $query = self::getData();
        if($request->has('submit_search')){
          $data_search = Session_Search::search_form($request);    
          $data_query = $query->whereBetween('available_date_payment', [$data_search['from_date'],$data_search['to_date']])
                                ->where('branch_id','LIKE',$data_search['brand_name'])
                                ->where('client_id','LIKE',$data_search['client_name'])
                                ->where('cs_schedules_id','LIKE',$data_search['sale_id'])
                                ->where('currency_id','LIKE',$data_search['currency'])
                                ->where('staff_id','LIKE',$data_search['staff_name']);
        }else{
            $data_query = $query;
        }
        return $data_query;
    }
    
    static function getDataAll($request){
        $query = self::GetDataSearch($request);
        $data_query = $query->get();
        return $data_query;
    }
    static function getDatabyPagination($request,$num = 15){
        $data_query = self::GetDataSearch($request);
        $data = $data_query->paginate($num);
        return $data;
    }
// Group Bye 
    public function getDataGroupBySchedule(){
        $query = self::GetDataSearch($request);
        $data_query = $query->groupBy('cs_schedules_id')->get();
        return $data_query;
    }


    static function total_cost_own_of_client($data){
        
    }
}