<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Mfi_Repayment_Schedul extends Model
{
    protected $table = "mfi_repayment_scheduls";



    public function mfi_credit_committy(){

    	return $this->belongsTo('App\Mfi_Credit_Committy','mfi_credit_committies_id');

    }



    public function user(){

     	return $this->belongsTo('App\User');

    }
   public function staff(){

     	return $this->belongsTo('App\User','staff_id');

    }
     public function mfi_interest_loan(){

        return $this->belongsTo('App\Mfi_Interest_Loan','mfi_interest_rate_id');

    }
    public function mfi_repayment_scheduls_timesheets(){

    	return $this->hasMany('App\Mfi_Repayment_Scheduls_Timesheet','mfi_repayment_schedul_id');

    }
     public function mfi_load_request()

    {

        return $this->belongsTo('App\Mfi_Load_Request','mfi_load_request_id');

    }

    public function mfi_client()

    {

        return $this->belongsTo('App\Mfi_Client','mfi_client_id');

    }
    public function mfi_clients_group(){

        return $this->belongsTo('App\Mfi_Client_Group' ,'mfi_client_group_id');

    }
    public function mfi_repayment_sth(){
        return $this->hasMany('App\Mfi_Repayment_Scheduls_Timesheet_History','mfi_repayment_schedul_id');
    }

    public function mfi_credit_committies_group(){
        return $this->belongsTo('App\Mfi_Credit_Committies_Group','mfi_credit_committies_id');

    }
}
