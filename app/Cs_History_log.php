<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_History_log extends Model
{
   protected $table = "cs_history_log";
   public function user(){
     return $this->belongsTo('App\User','user_id');

   }
}
