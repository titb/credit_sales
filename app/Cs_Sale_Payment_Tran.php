<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Sale_Payment_Tran extends Model
{
    //
    protected $table ="cs_payments_transactions";

    public function currency(){
        return $this->belongsTo('App\Currency','currecy_type');
    }
}
