<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use App\Cs_Sale;
use App\Cs_Sale_Item;
use App\Cs_Sale_Payment;
use App\Cs_Sale_Payment_Tran;
use App\Cs_Cds_Request_Form;
use Auth;
use Session;
use DB;
use Cart;
class CreditSaleController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    // check sale type 
    public function saletype(Request $request){
        if($request->session()->has('sale_type')){
            $btn_sale = $request->session()->get('sale_type');
        }else{
            $btn_sale = 'sale_by_credit';
        }
        return $btn_sale; 
    }

    // check is has sale id 
    public function sale_id(Request $request){
        if($request->session()->has('sale_id')){    
            $sale_id_e = $request->session()->get('sale_id');
        }else{
            $sale_id_e = 0;
        }
        return $sale_id_e;
    }
// check redirect 
    // public function check_redirect_sale(Request $request){
        
    // }


    public function get_index_credit_sale_request(Request $request,$id){
        $title = "តារាងទម្រង់ស្នើសុំការទិញបង់រំលស់";
        $datas = Cs_Client::find($id);
        $client_id = $id;
        return view('credit_sale.credit_sale.index_rf_cre_sale_list',compact('title','datas','client_id'));
    }

    public function get_create_credit_sale_request(Request $request,$id){
        $title = "ការបង្កើតទម្រង់ស្នើសុំការទិញបង់រំលស់";
        $datas = Cs_Client::find($id);
        $client_id = $id;
        return view('credit_sale.credit_sale.create_rf_cre_sale_list',compact('title','datas','client_id'));
    }

    // add session 
    public function add_all_session(Request $request){
        if($request->has('customer_name')){
            $customer_id = $request->session()->put('customer_id', $request->customer_id);
            $customer_name = $request->session()->put('customer_name', $request->customer_name);
        }
        if($request->has('discount_globle')){
            $discount_globle = $request->session()->put('discount_globle', $request->discount_globle);
        }
        if($request->has('shipping_payment')){
            $shipping_payment = $request->session()->put('shipping_payment', $request->shipping_payment);
        }
        if($request->has('deposit_precent')){
            $deposit_precent = $request->session()->put('deposit_precent', $request->deposit_precent);
        }
        if($request->has('deposit_fixed')){
            $deposit_fixed = $request->session()->put('deposit_fixed', $request->deposit_fixed);
        }
        
        if($request->has('return_back_type')){
            $return_back_type = $request->session()->put('return_back_type', $request->return_back_type);
        }
        if($request->has('sale_type')){
            $sale_type = $request->session()->put('sale_type', $request->sale_type);
        }
        if($request->has('pay_type')){
            $pay_type = $request->session()->put('pay_type', $request->pay_type);
        }
        if($request->has('currency_type')){
            $currency_type = $request->session()->put('currency_type', $request->currency_type);
        }
        if($request->has('payment_num')){
            if($request->payment_num != 0){

                $request->session()->push('tran_cash', [
                    'id'=> time(),
                    'payment_num'=> $request->payment_num,
                    'pay_type_active'=> $request->pay_type_active,
                    'pay_active'=> $request->pay_active,
                ]);
            }

            $remaining_us = $request->session()->put('remaining_us', $request->remaining_us);
            $remaining_kh = $request->session()->put('remaining_kh', $request->remaining_kh);
            $payback_us = $request->session()->put('payback_us', $request->payback_us);
            $payback_kh = $request->session()->put('payback_kh', $request->payback_kh);
        }
        return;
    }
    public function pos_add_all_session_credit_sale(Request $request){
        $this->add_all_session($request);
        if($request->session()->has('return_back_type')){
            $return_back_type = $request->session()->get('return_back_type');
        }else{
            $return_back_type = 0;
        }
        $data = [
            'success'=>'seccess',
            'return_back_type' => $return_back_type,
            'shipping_payment' => $request->session()->get('shipping_payment'),
            'sale_type'=> $request->session()->get('sale_type'),
            'tran_cash'=>$request->session()->get('tran_cash'),
            'remaining_us'=>$request->session()->get('remaining_us'),
            'remaining_kh'=>$request->session()->get('remaining_kh'),
            'payback_us'=>$request->session()->get('payback_us'),
            'payback_kh'=>$request->session()->get('payback_kh') 
        ];

        return response()->json($data);
    }

// ការបង្កើតការទិញបង់រំលស់  
    public function get_create_credit_sales(Request $request){
        // $data = Cs_Client::get();
        
        // foreach($data as $test){
        //     if($test->schedule){    
        //         dd($test->kh_username);
        //     }
        // }
        $title = "ការបង្កើតការទិញបង់រំលស់";
        if($request->session()->has('sale_type')){
            $sale_type = $request->session()->get('sale_type');
        }else{
            $sale_type = "sale_by_credit";
        }
        // request client type
        if($request->from){
            $cli_type = $request->from;
        }else{
            $cli_type = 0;
        }
        return view('credit_sale.credit_sale.credit_sale',compact('title', 'cli_type' ,'sale_type'));
    }
// Search Item 
    public function search_item_ajax_search(Request $request){
        $iterm = $request->term;
        $btn_sale = $this->saletype($request);
        if($btn_sale == "sale_recieving"){
            $data = Cs_Sale::where('deleted','=',1)
            ->where('num_invoice','LIKE','%'.$iterm.'%')
            ->where('sale_status_for','<>','close')
            ->take(10)->get();

            $results = [] ;
            foreach($data as $key => $v){
                $results[] = ['id'=> $v->id, 'value' => $v->num_invoice ,'method'=>$btn_sale];
            }
        }else{
            $data = Cs_items::where('deleted','=',0)
                            ->where('name','LIKE','%'.$iterm.'%')
                            ->Orwhere('item_bacode','LIKE','%'.$iterm.'%')
                            ->Orwhere('add_number_id','LIKE','%'.$iterm.'%')
                            ->take(10)->get();

            $results = [] ;
            foreach($data as $key => $v){
                if($v->inventorys->sum('qty') >= 1){
                    $cur_qty =  $v->inventorys->sum('qty');
                }elseif($v->inventorys->sum('qty') <= 0) {
                    $cur_qty = 0;
                }else{
                    $cur_qty = "Out Of Stock";
                }
                $img_pro = "<img src='images/".$v->pro_image."' width=50px height=50px>";
                // if($v->inventorys->sum('qty') >= 1){
                    $results[] = ['id'=> $v->id, 'value' => $v->name , 'sell_price'=>$v->sell_price, 'current_qty'=> $cur_qty ,'image_g'=> $v->image_item ,'method'=>$btn_sale];
                // }
            }
        }    
        return response()->json($results);

  }
  // search customer 
  public function search_customer_name_ajax(Request $request){
    $iterm = $request->term;
    $data = Cs_Client::where('deleted','!=',1)
            ->where('kh_username','LIKE','%'.$iterm.'%')
            ->Orwhere('en_username','LIKE','%'.$iterm.'%')
            ->Orwhere('phone','LIKE','%'.$iterm.'%')
            ->Orwhere('email','LIKE','%'.$iterm.'%')
            ->Orwhere('identify_num','LIKE','%'.$iterm.'%')
            ->Orwhere('account_number','LIKE','%'.$iterm.'%')->take(10)->get();
    
            $results = [] ;
            foreach($data as $key => $v){
                // if($v->schedule){
                //     foreach($v->schedule as $k=>$sced){
                //         if($sced->status_for_pay == 1 && $sced->is_finish == 1){
                //             $results[] = ['id'=> $v->id, 'value' => $v->kh_username];
                //         }else{
                //             $results = "";
                //         }
                //     }
                // }else{
                    if($v->upload_relate_document){
                        $img = $v->upload_relate_document;
                    }else{
                        $img = 'avatar2.png';
                    }

                    $results[] = ['id'=> $v->id, 'value' => $v->kh_username ,'image_g'=>$img ];
                // }
                 
            }

            return response()->json($results);
 }


// add to card function 
    public function post_item_add_to_cart_ajax(Request $request){
        
        if($request->redirect == 'invoice' || $request->redirect == 'sale_recieving'){
                    $this->remove_cart_with_ajax($request);
                    $sale_id = $request->item_id;
                    $data_sale = Cs_Sale::find($sale_id);

                    if($request->redirect == 'invoice'){
                        if($data_sale->method){
                            $sale_type = $request->session()->put('sale_type', $data_sale->method);
                        }
                        $sign = 1;
                    }else{

                        $sign = -1;
                    }

                    foreach($data_sale->sale_item as $key => $d){
                        $qty = $sign * $d->qty;
                        $discount = $d->discount;
                        $this->add_item_to_card($request,$d->product_id, $qty , $discount);
                    }
                    if($request->item_id){
                        $request->session()->put('sale_id',  $sale_id);
                    }
                    if($data_sale->client_id){
                        $cust = Cs_Client::find($data_sale->client_id);
                        $customer_id = $request->session()->put('customer_id', $cust->id);
                        $customer_name = $request->session()->put('customer_name', $cust->kh_username);
                    } 

                if($request->redirect != 'invoice'){
                    $comment = $request->session()->put('comment', "Return to ".$data_sale->method." ID: ".$sale_id);
                }    
                if($data_sale->sale_payment){

                    if($data_sale->sale_payment->discount_payment){
                        $discount_globle = $request->session()->put('discount_globle', $data_sale->sale_payment->discount_payment);
                    }
                    if($data_sale->sale_payment->shipping_payment){
                        $shipping_payment = $request->session()->put('shipping_payment', $data_sale->sale_payment->shipping_payment);
                    }
                    if($request->redirect == 'invoice'){
                        if($data_sale->sale_payment->comment){
                            $comment = $request->session()->put('comment', $data_sale->sale_payment->comment);
                        }
                    }
                    
                    // if($data_sale->sale_transaction_pay){  
                    //     foreach($data_sale->sale_transaction_pay as $stp){
                    //             $request->session()->push('tran_cash', [
                    //                 'id'=> time(),
                    //                 'payment_num'=> $sign*$stp->account_mount,
                    //                 'pay_type_active'=> $stp->payment_pay,
                    //                 'pay_active'=> $stp->currecy_type,
                    //             ]);  
                    //         }
                        
                    // }
                    // $remaining_us = $request->session()->put('remaining_us', $data_sale->sale_payment->remaining_defualt);
                    // $remaining_kh = $request->session()->put('remaining_kh', $data_sale->sale_payment->remaining_change);
                    // $payback_us = $request->session()->put('payback_us', $data_sale->sale_payment->payback_defualt);
                    // $payback_kh = $request->session()->put('payback_kh', $data_sale->sale_payment->payback_change);
                }    
        }else{

            $item_id = $request->item_id;
            $qty = 1;
            $discount = 0;
            $this->add_item_to_card($request, $item_id , $qty, $discount);
        }
        return response()->json(['success'=>'That Success Card']);
    }


//add card  add_item_to_card
    public function add_item_to_card(Request $request,$id , $qty , $discount){
       
        // $id = $request->item_id;
        $deposit_setting = 0.3;
        $override_default_commission = 1;
        $defult_comission = 0;
        $is_defult_tax = 1 ;
        $is_include_defult_tax = 1; 
        $is_setting_defult_tax = 1 ;
        $item_tax = []; 
        $total_tax = 0 ;
        $userId = auth()->user()->id;

        $data_item = Cs_items::find($id);
        $price = $data_item->sell_price;
        $cost_price = $data_item->cost_price;
        $item_bacode = $data_item->item_bacode;
        // $this_day = date('Y-m-d');
        //                     if($data->promo_price){
        //                     if($this_day >= $data->start_date_promo && $this_day <= $data->end_date_promo){
        //                         $data_price_pro = $data->promo_price;
        //                     }else{
        //                             $data_price_pro = $data->unit_price;
        //                     }
        //                     $data_cost_price = $data->cost_price;
        //                     }else{
        //                     $data_price_pro = $data->unit_price;
        //                     $data_cost_price = $data->cost_price;
        //                     }
        //$promot_price = $data_item->promo_price;
        
 
        $cart = Cart::session($userId)->get($id);
        if(!empty($cart)){
            $dis = $cart->attributes->discount;
        }else{
            $dis = $discount;
        }

        if($data_item->override_default_commission ==  $override_default_commission){
            if($data_item->commission_fixed != 0){
                $comission = $data_item->commission_fixed;
            }elseif($data_item->commission_give != 0){
                $comission = number_format($cost_price * ($data_item->commission_give /100), 2);
            }else{
                $comission = 0;
            }
        }else{
            $comission = $defult_comission;
        }



        if($data_item->override_default_tax == $is_defult_tax){
            foreach($data_item->cs_item_tax as $key => $csit){
                $total_tax += $price*($csit->percent/100);
                $item_tax[] = [
                                    'id_tax'=> $csit->id, 
                                    'name_tax' => $csit->name ,
                                    'percent_tax'=>$csit->percent ,
                                    'tax'=>number_format($price*($csit->percent/100),2)
                              ];
            } 
        }
            $sale_type = $this->saletype($request);
        
        if($sale_type == "sale_by_credit"){
            $deposit_fixed = $data_item->sell_price * $deposit_setting;
            $credit_sale_cost = $data_item->sell_price - $deposit_fixed;
        }else{
            $deposit_setting = 0;
            $deposit_fixed = 0;
            $credit_sale_cost = 0;
        }
        
        $cur_qty =  $data_item->inventorys->sum('qty'); // Sum qty in Inventory

            $data_add = [
                'id'=>  $id,
                'name'=> $data_item->name,
                'quantity'=> $qty,
                'price'=> $data_item->sell_price,
                'attributes' => [ 
                    'tax'=>$item_tax,
                    'current_qty' => $cur_qty,
                    'deposit_percent' => $deposit_setting,
                    'deposit_fixed' => $deposit_fixed,
                    'credit_sale_cost' => $credit_sale_cost,
                    'discount'=> $dis,
                    'item_bacode'=> $item_bacode,
                    'cost_price'=> $cost_price,
                    'comission'=> $comission,
                    'total_tax'=> number_format($total_tax,2) 
                    ]
            ];
            $userId = auth()->user()->id; // or any string represents user identifier
            $data = Cart::session($userId)->add($data_add);
       
        return; 
    }
//update card
    public function post_item_update_to_cart_ajax(Request $request,$id){
        $deposit_setting = 0.3;
        $data_item = Cs_items::find($id);
        $price = $data_item->sell_price;
        $cost_price = $data_item->cost_price;
        $item_bacode = $data_item->item_bacode;

        $override_default_commission = 1;
        $defult_comission = 0;
        $is_defult_tax = 1 ;
        $is_include_defult_tax = 1; 
        $is_setting_defult_tax = 1 ;
        $item_tax = []; 
        $total_tax = 0 ;

        if($data_item->override_default_commission ==  $override_default_commission){
            if($data_item->commission_fixed != 0){
                $comission = $data_item->commission_fixed;
            }elseif($data_item->commission_give != 0){
                $comission = number_format($cost_price * ($data_item->commission_give /100), 2);
            }else{
                $comission = 0;
            }
        }else{
            $comission = $defult_comission;
        }

        if($data_item->override_default_tax == $is_defult_tax){
            foreach($data_item->cs_item_tax as $key => $csit){
                $total_tax += $price*($csit->percent/100);
                $item_tax[] = [
                                    'id_tax'=> $csit->id, 
                                    'name_tax' => $csit->name ,
                                    'percent_tax'=>$csit->percent ,
                                    'tax'=>number_format($price*($csit->percent/100),2)
                              ];
            } 
        }
        $sale_type = $this->saletype($request);
        if($sale_type == "sale_by_credit"){
            $deposit_fixed = $data_item->sell_price * $deposit_setting;
            $credit_sale_cost = $data_item->sell_price - $deposit_fixed;
            
        }else{
            $deposit_setting = 0;
            $deposit_fixed = 0;
            $credit_sale_cost = 0;
        }

        $cur_qty =  $data_item->inventorys->sum('qty'); // Sum qty in Inventory

        $data_add = [
                'quantity' => [
                    'relative' => false,
                    'value' => $request->qty
                ],
                'attributes' => [
                    'tax'=>$item_tax,
                    'current_qty' => $cur_qty,
                    'deposit_percent' => $deposit_setting,
                    'deposit_fixed' => $deposit_fixed,
                    'credit_sale_cost' => $credit_sale_cost,
                    'discount'=> $request->discount,
                    'item_bacode'=> $item_bacode,
                    'cost_price'=> $cost_price,
                    'comission'=> $comission,
                    'total_tax'=> number_format($total_tax,2) 
                ]

        ];
         $userId = auth()->user()->id; // or any string represents user identifier
         Cart::session($userId)->update($id,$data_add);
         $data = [
                'update_card'=> Cart::session($userId)->get($id)
                 ];
                 $request->session()->forget('tran_cash');
                 $request->session()->forget('currency_type');
                 $request->session()->forget('pay_type');
                 $request->session()->forget('remaining_us');
                 $request->session()->forget('remaining_kh');
                 $request->session()->forget('payback_us');
                 $request->session()->forget('payback_kh');
        return response()->json($data);
    }
    public function post_item_cart_ajax(Request $request){
        $userId = auth()->user()->id;
        $con_cart = Cart::session($userId)->getContent();
        $subtotal = Cart::session($userId)->getSubTotal();
        $carttotal = Cart::session($userId)->getTotal();

        if($request->session()->has('discount_globle')){
            $discount_globle = $request->session()->get('discount_globle');
        }else{
            $discount_globle = 0;
        }

        if($request->session()->has('shipping_payment')){
            $shipping_payment = $request->session()->get('shipping_payment');
        }else{
            $shipping_payment = 0;
        }

        if($request->session()->has('comment')){
            $comment = $request->session()->get('comment');
        }else{
            $comment = "";
        }
        $sale_type = $this->saletype($request);
        $data = [
            'shipping_payment'=> $shipping_payment,
            'con_cart'=> $con_cart ,
            'sale_type'=> $sale_type,
            'subtotal'=> $subtotal ,
            'comment' => $comment, 
            'carttotal'=> $carttotal,
            'discount_globle'=>$discount_globle,
        ]; 

        return response()->json($data);
    }
    // Remove cart by id item
    public function post_remove_cart_with_item_id(Request $request,$id){
        $userId = auth()->user()->id; // or any string represents user identifier
        Cart::session($userId)->remove($id);
        $request->session()->forget('tran_cash');
        $request->session()->forget('currency_type');
        $request->session()->forget('pay_type');
        $request->session()->forget('remaining_us');
        $request->session()->forget('remaining_kh');
        $request->session()->forget('payback_us');
        $request->session()->forget('payback_kh'); 
        $data = [
            'success'=> "Successfull Remove Item - ".$id
      ];
        return response()->json($data);
    }

    public function remove_cart_with_ajax(Request $request){
        $userId = auth()->user()->id;
        Cart::session($userId)->clear();
        $request->session()->forget('tran_cash');
        $request->session()->forget('discount_globle');
        $request->session()->forget('shipping_payment');
        $request->session()->forget('deposit_fixed');
        $request->session()->forget('deposit_precent');
        $request->session()->forget('return_back_type');
        $request->session()->forget('currency_type');
        $request->session()->forget('pay_type');
        $request->session()->forget('sale_id');
        $request->session()->forget('remaining_us');
        $request->session()->forget('remaining_kh');
        $request->session()->forget('payback_us');
        $request->session()->forget('payback_kh'); 
        $request->session()->forget('customer_id');
        $request->session()->forget('customer_name');
        $request->session()->forget('comment');
        
        return;   
    }
    // Remove cart in all item

    public function post_cancel_all_cart_item(Request $request){
        $this->remove_cart_with_ajax($request);
        return redirect()->to('credit_sales');
    }

    public function post_cancel_all_cart_item_ajax(Request $request){
        $this->remove_cart_with_ajax($request);
        $data = [
            'success'=> "Successfull Remove Item "
      ];
        return response()->json($data);
    }
    

    public function remove_tran_cash(Request $request ,$pay_id){
        if($request->session()->has('tran_cash')){
            $payment = collect($request->session()->get('tran_cash'));
            $payment->pull($pay_id);
          }
          $request->session()->put('tran_cash',$payment->all());

            $request->session()->forget('remaining_us');
            $request->session()->forget('remaining_kh');
            $request->session()->forget('payback_us');
            $request->session()->forget('payback_kh'); 
            $request->session()->forget('currency_type');
            $request->session()->forget('pay_type'); 
          $data = [
                'success'=>  "success remove transation"
          ];
         return response()->json($data);
        //return redirect()->to('credit_sales');
    }
     // get all  session 
     public function get_add_all_session_credit_sale(Request $request){
        if($request->session()->has('customer_name')){
            $customer_id = $request->session()->get('customer_id');
            $customer_name = $request->session()->get('customer_name');
        }else{
            $customer_id = 0;
            $customer_name = null;
        }
        if($request->session()->has('discount_globle')){
            $discount_globle = $request->session()->get('discount_globle');
        }else{
            $discount_globle = 0;
        }

        if($request->session()->has('shipping_payment')){
            $shipping_payment = $request->session()->get('shipping_payment');
        }else{
            $shipping_payment = 0;
        }
        if($request->session()->has('deposit_precent')){
            $deposit_precent = $request->session()->get('deposit_precent');
        }else{
            $deposit_precent = 0;
        }

        if($request->session()->has('deposit_fixed')){
            $deposit_fixed = $request->session()->get('deposit_fixed');
        }else{
            $deposit_fixed = 0;
        }
        
        $sale_type = $this->saletype($request);
        if($request->session()->has('comment')){
            $comment = $request->session()->get('comment');
        }else{
            $comment = "";
        }
        if($request->session()->has('pay_type')){
            $pay_type = $request->session()->get('pay_type');
        }else{
            $pay_type = "cash";
        }
        if($request->session()->has('currency_type')){
            $currency_type = $request->session()->get('currency_type');
        }else{
            $currency_type = 1;
        }

        if($request->session()->has('tran_cash')){
            $tran_cash = $request->session()->get('tran_cash');
        }else{
            $tran_cash = [];
        }
        if($request->session()->has('return_back_type')){
            $return_back_type = $request->session()->get('return_back_type');
        }else{
            $return_back_type = 0;
        }
        $data = [
            'customer_id'=>$customer_id,
            'customer_name'=>$customer_name,
            'deposit_precent' => $deposit_precent,
            'deposit_fixed' => $deposit_fixed,
            'discount_globle'=>$discount_globle,
            'return_back_type' => $return_back_type,
            'sale_type'=>$sale_type,
            'pay_type'=>$pay_type,
            'currency_type'=>$currency_type,
            'tran_cash' => $tran_cash,
            'comment' => $comment,
            'shipping_payment' => $shipping_payment,
            'remaining_us'=>$request->session()->get('remaining_us'),
            'remaining_kh'=>$request->session()->get('remaining_kh'),
            'payback_us'=>$request->session()->get('payback_us'),
            'payback_kh'=>$request->session()->get('payback_kh') 
        ];
        return response()->json($data);
    }
    

    public function completed_sale(Request $request){

        $defualt_required_request_form = 1;
        $userId = auth()->user()->id;
        $deposit_setting = 0.3;
        $defult_currency = 1;
        $interest_of_owne_precent = 0.112;
        
        $data_count = Cart::session($userId)->getContent();
        if($request->session()->has('customer_name')){
            $customer_id = $request->session()->get('customer_id');
            $customer_name = $request->session()->get('customer_name');
        }else{
            $customer_id = 0;
            $customer_name = null;
        }
        if($request->session()->has('discount_globle')){
            $discount_globle = $request->session()->get('discount_globle');
            $discount_reason = "Spacail Price";
        }else{
            $discount_globle = 0;
            $discount_reason =  null;
        }
        if($request->session()->has('shipping_payment')){
            $shipping_payment = $request->session()->get('shipping_payment');
        }else{
            $shipping_payment = 0;
        }
        $sale_type = $this->saletype($request);

        if($request->session()->has('pay_type')){
            $pay_type = $request->session()->get('pay_type');
        }else{
            $pay_type = "cash";
        }
        if($request->session()->has('currency_type')){
            $currency_type = $request->session()->get('currency_type');
           if($currency_type == 1){
                $currency = 1;
           }else{
                $currency =2;   
           }
        }else{
            $currency = 1;
            $currency_type = 1;
        }

        if( $sale_type != "sale_by_cash"){
            $is_oncredit = 1;
        }else{
            $is_oncredit = 0;
        }

        $total_qty = Cart::session($userId)->getTotalQuantity();
        // $total_due_amout = str_replace(',','',$request->total_due_amout);
        // $total_amount = str_replace(',','',$request->total_amount);
            
        $method = $sale_type;
        if($request->has('staff_id')){
            $staff_id = $request->staff_id;
        }else{
            $staff_id = Auth::user()->id;
        }
       if($request->has('due_date')){
            $due_date = $request->due_date;
        }else{
            $due_date = date('Y-m-d');
        }  
        if($sale_type != 'sale_recieving'){
            $sale_id_e = $this->sale_id($request);
        }else{
            $sale_id_e = 0;
        }    
        $remaining_defualt = $request->session()->get('remaining_us');
        $remaining_change = $request->session()->get('remaining_kh');
        $payback_defualt = $request->session()->get('payback_us');
        $payback_change = $request->session()->get('payback_kh'); 
        if($request->has('has_pay_tran')){
            $payment_amount = $request->has_pay_tran;
        }else{
            $payment_amount = 0;
        }
        if($request->has('total_payment')){
            $total_payment = $request->total_payment;
        }else{
            $total_payment = 0;
        }

        if($request->has('sub_payment_amount')){
            $sub_payment_amount = $request->sub_payment_amount;
        }else{
            $sub_payment_amount = 0;
        }

        if($request->has('dute_payment_amount')){
            $dute_payment_amount = $request->dute_payment_amount;
        }else{
            $dute_payment_amount = 0;
        }
        if($request->has('return_back_type')){
            $return_back_type = $request->return_back_type;
        }else{
            $return_back_type = 0;
        }
        if($request->has('payment_for_date')){
            $payment_for_date = date('Y-m-d h:m:s',strtotime($request->payment_for_date));
        }else{
            $payment_for_date = null;
        }

        if($request->has('payment_date')){
            $payment_date = date('Y-m-d h:m:s',strtotime($request->payment_date));
        }else{
            $payment_date = date('Y-m-d h:m:s');
        }

        if($request->has('date_create_request')){
            $date_create_request = date('Y-m-d',strtotime($request->date_create_request));
        }else{
            $date_create_request = null;
        }

        if($request->has('date_for_payments')){
            $date_for_payments = date('Y-m-d',strtotime($request->date_for_payments));
        }else{
            $date_for_payments = null;
        }
        if($request->deposit_precent){
            $deposit_precent = number_format(($request->deposit_precent / 100) ,2 );
        }else{
            $deposit_precent = $deposit_setting;
        }
    if($sale_type != "sale_by_cash"){   
        if($request->has('remaining')){
            $remaining = $request->remaining;
        }else{
            $remaining = "pay_for_owned";
        }
    }else{
        $remaining = "pay_on_direct_sale";
    }    

        if($request->has('penalty')){
            $penalty = $request->penalty;
        }else{
            $penalty = 0;
        }
        if($request->has('total_tax')){
            $total_tax = $request->total_tax;
        }else{
            $total_tax = 0;
        }
        if($request->has('comment')){
            $comment = $request->comment;
        }else{
            $comment = null;
        }
       
        if(count($data_count) > 0){
            $data_in_sale = [
                'client_id' => $customer_id,
                'user_id' => Auth::user()->id,
                'staff_id' =>$staff_id,
                'currency_id' => $currency,
                'branch_id' =>Auth::user()->branch_id,
                'sale_status_for' =>"padding", 
                'due_date' =>$due_date,
                'is_oncredit' =>$is_oncredit,
                'status' =>1,
                'active' =>1,
                'method' =>$method,
                'description' =>null,
                'deleted' =>1,
            ];

           
            if($sale_id_e == 0 ){
                $sale_id = Cs_Sale::insertGetId($data_in_sale);
                $numinvo = date('Y')."".date('m')."".date('d')."".$sale_id;
                Cs_Sale::where('id','=',$sale_id)->update(['num_invoice'=> $numinvo,'created_at' =>date('Y-m-d h:m:s')]);
            }else{
                $sale_id = $sale_id_e; 
                Cs_Sale::where('id','=',$sale_id)->update($data_in_sale);
                Cs_Cds_Request_Form::where('id','=',$sale_id)->delete();
                Cs_Sale_Payment::where('id','=',$sale_id)->delete();
                Cs_Sale_Payment_Tran::where('sale_id','=',$sale_id)->delete();   
                Cs_Sale_Item::where('sale_id','=',$sale_id)->delete(); 
                Cs_inventorys::where('spacifies','=',$sale_id)->delete();   
            }
            if($sale_id != 0){
        // Payment Type  are Payment direct or Payment Type is Required from  Defualt Setting 1
         if($sale_type == 'sale_by_credit'){
            // Request Form Payment 
                if($defualt_required_request_form == 1){
                    $data_request_form = [
                        'id'=> $sale_id,
                        'client_id'=>$customer_id, 
                        'sale_id'=> $sale_id,
                        'user_id' => Auth::user()->id,
                        'staff_id' =>$staff_id,
                        'branch_id' =>Auth::user()->branch_id,
                        'place_for_pay'=>1,
                        'prices_total_num'=> $request->prices_total_num,
                        'total_payment'=>$total_payment,
                        'sub_payment_amount'=>$sub_payment_amount,
                        'prices_totalword'=> $request->prices_totalword,
                        'payment_shipping' =>$shipping_payment,
                        'deposit_precent'=> $deposit_precent,
                        'deposit_fixed'=> $request->deposit_fixed,
                        'deposit_fixed_word'=> $request->deposit_fixed_word,
                        'money_owne'=> $request->money_owne,
                        'money_owne_word'=> $request->money_owne_word,
                        'interest_of_owne_precent'=> $interest_of_owne_precent,
                        'currency'=> $currency,
                        'method'=> $method,
                        'duration_pay_money'=> $request->duration_pay_money,
                        'duration_pay_money_type'=> $request->duration_pay_money_type,
                        'date_for_payments'=> $date_for_payments,
                        'date_create_request'=> $date_create_request,
                        'status'=> 1,
                        'discounted' =>$discount_globle,
                        'discount_reason' => $discount_reason,
                        'total_qty'=> $total_qty,
                        'is_agree'=> 0,
                        'is_give'=> 0,
                        'is_break'=> 0,
                        'deleted'=> 1,
                        'created_at'=> date('Y-m-d h:m:s'),
                        'updated_at'=> date('Y-m-d h:m:s'),

                    ];

                    $cs_request_form_id = Cs_Cds_Request_Form::insertGetId($data_request_form);
                } 
        }       
        // Payment aren't Credit Sale        
        if($sale_type != "sale_by_credit"){           
                // Sale payment 
                $data_payment = [
                    'id'=> $sale_id,
                    'sale_id'=> $sale_id,  
                    'place_for_pay'=>1,
                    'payment_amount'=>$payment_amount,
                    'total_payment'=>$total_payment,    
                    'payment_shipping' =>$shipping_payment,
                    'return_back_type' => $return_back_type,
                    'discount_payment' =>$discount_globle,
                    'discount_reason' => $discount_reason,
                    'sub_payment_amount'=>$sub_payment_amount,
                    'dute_payment_amount'=>$dute_payment_amount,
                    'remaining_defualt'=>$remaining_defualt,
                    'remaining_change'=>$remaining_change,
                    'payback_defualt'=>$payback_defualt,
                    'payback_change'=>$payback_change,
                    'payment_date'=>$payment_date,
                    'payment_for_date'=>$payment_for_date,
                    'remaining'=>$remaining,
                    'penalty'=>$penalty,
                    'currency'=>$defult_currency,
                    'comment'=>$comment,
                    'total_tax' => $total_tax,
                    'total_qty' => $total_qty,
                    'status'=>1
                ];
               $id_data_payment =  Cs_Sale_Payment::insertGetId($data_payment);    

                if($request->session()->has('tran_cash')){
                    $tran_cash = $request->session()->get('tran_cash');
                    foreach($tran_cash as $key => $v){
                        if($v['payment_num'] != 0){
                            $data_payment_tran = [
                                'sale_id' =>$sale_id,
                                'payment_id' =>$id_data_payment,
                                'account_mount' =>$v['payment_num'],
                                'currecy_type' =>$v['pay_active'],
                                'payment_pay' =>$v['pay_type_active'],
                            ];
                            Cs_Sale_Payment_Tran::insert($data_payment_tran);     
                        }
                        
                    }         
                } 
            }        

                // Product Sale 
                foreach($data_count as $key => $dc){
                    if($dc->quantity > 0){
                        $disqty = number_format(($dc->price * $dc->quantity)/100,2);
                    }else{
                        $disqty = 0;
                    }
                   $data = [ 
                        'sale_id'=> $sale_id,
                        'barcode'=>$dc->attributes->item_bacode,
                        'product_id'=>$dc->id,
                        'total_price'=>$dc->price * $dc->quantity,
                        'total_of_deposit'=>0,
                        'total_price_payment'=>$dc->price - $disqty,
                        'qty'=>$dc->quantity,
                        'sell_price'=>$dc->price,
                        'cost_price'=>$dc->attributes->cost_price,
                        'discount'=>$dc->attributes->discount,
                        'total_tax'=>$dc->attributes->total_tax * $dc->quantity,
                        'commission'=>$dc->attributes->comission * $dc->quantity,
                        'other_price'=>0,
                        'coment_on_item'=> null,
                   ]; 
                   Cs_Sale_Item::insert($data);

                  $data_in = [
                    'item_id' =>$dc->id,
                    'branch_id' => Auth::user()->branch_id,
                    'user_id' => Auth::user()->id,
                    'spacifies' => $sale_id,
                    'status_transation' => "Sale",
                    'qty'=>-($dc->quantity),
                  ];
                  Cs_inventorys::insert($data_in);
            
                } 
  
            }
            
            if($sale_type == "sale_recieving"){
                Cs_Sale::where('id','=',$sale_id)->update(['sale_status_for'=> 'close']);
                Cs_Sale::where('id','=',$this->sale_id($request))->update(['sale_status_for'=> 'close']);
            }

            $this->remove_cart_with_ajax($request);
            $resules = [
                'sale_id' => $sale_id
            ] ;
        }else{
            $resules = ['sataus'=>'sale is not complete'];
        }

        
        return response()->json($data);
    }
    


    public function get_show_invoice(Request $request,$id)
    {
        $title = "Show Invoice Sale";
        $sale_id = $id;
        return view('credit_sale.credit_sale.credit_sale_invoice', compact('title','sale_id'));   
    }

    public function get_show_sale_invoice_json(Request $request, $id)
    {
        $cs_sale = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item.item','cs_request_form','cs_client'])->where('id','=',$id)->first();
        $data = [
            'cs_sale' => $cs_sale
        ];
        return response()->json($data);
    }


    // Deleted Sale from database  

    public function deleted_sale_from_database(Request $request,$id){
        
        Cs_Cds_Request_Form::where('sale_id','=',$id)->delete();
        Cs_Sale_Payment::where('sale_id','=',$id)->delete();
        Cs_Sale_Payment_Tran::where('sale_id','=',$id)->delete();     
        Cs_Sale_Item::where('sale_id','=',$id)->delete();
        Cs_Sale::where('id','=',$id)->delete();
        Cs_inventorys::where('spacifies','=',$id)->delete();
        return response()->json(['msg_show'=>'deleted success']); 
    }

}
