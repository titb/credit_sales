<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use App\Cs_Purchase_Order;
use App\Cs_Purchase_Order_Item;
use App\Cs_Purchase_Order_Payment;
use Auth;
use Session;
use DB;
use Cart;
class PurchaseOrderController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    
    public function credit_sales_purchas_order(Request $request){
        $title = "ការ​បញ្ជា​ទិញ";
        if($request->session()->has('sale_type')){
            $sale_type = $request->session()->get('sale_type');
        }else{
            $sale_type = "purchas_order";
        } 
        return view('credit_sale.purchase_order.index_po',compact('sale_type','title'));
    }
   
    // search pos with ajax
    public function search_item_ajax_search(Request $request){
        if($request->session()->has('sale_type_po')){
            $sale_type = $request->session()->get('sale_type_po');
        }else{
            $sale_type = "purchas_order";
        } 
        
        $iterm = $request->term;
            $data = Cs_items::where('deleted','=',0)
            ->where('name','LIKE','%'.$iterm.'%')
            ->Orwhere('item_bacode','=',$iterm)
            ->Orwhere('add_number_id','=',$iterm)
            ->take(5)->get();
        
        if($sale_type == "receiving"){    
            $data1 = Cs_Purchase_Order::where('deleted','=',1)
            ->where('status_payment','=','purchas_order')
            ->where('invoice_num','LIKE','%'.$iterm.'%')
            ->take(5)->get();
            $results = [] ;

            foreach($data1 as $key => $v){
                $cur_qty = "No Qty";
                $sell_price = "No Price";
                $image_item = "No Image";
                $results[] = ['id'=> $v->invoice_num, 'value' => $v->invoice_num , 'sell_price'=>$sell_price, 'current_qty'=> $cur_qty ,'image_g'=> $image_item];
            }
        }    
            foreach($data as $key => $v){
                if($v->inventorys->sum('qty') > 0){
                $cur_qty =  $v->inventorys->sum('qty');
                }else {
                    $cur_qty = "Out Of Stock";
                }
                $img_pro = "<img src='images/".$v->pro_image."' width=50px height=50px>";

                $results[] = ['id'=> $v->id, 'value' => $v->name , 'sell_price'=>$v->sell_price, 'current_qty'=> $cur_qty ,'image_g'=> $v->image_item];
            }

            return response()->json($results);
    }
    //add card  
    public function post_item_add_to_cart_ajax(Request $request){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);
        $id = $request->item_id;
        $deposit_setting = 0.3;
        $override_default_commission = 1;
        $defult_comission = 0;
        $is_defult_tax = 1 ;
        $is_include_defult_tax = 1; 
        $is_setting_defult_tax = 1 ;
        $item_tax = []; 
        $total_tax = 0 ;

        // if($request->session()->has('sale_type_po')){
        //     $sale_type = $request->session()->get('sale_type_po');
        // }else{
        //     $sale_type = "purchas_order";
        // } 
        
        if($request->session()->has('sale_type_po')){
            $sale_type_po = $request->session()->get('sale_type_po');
        }else{
            $sale_type_po = "purchas_order";
        }

    if($sale_type_po == "purchas_order"){    
        $data_item = Cs_items::find($id);
        $price = $data_item->sell_price;
        $cost_price = $data_item->cost_price;
        $item_bacode = $data_item->item_bacode;
        $userId = auth()->user()->id;
        $cart = $purchase_order->get($id);
        if(count($cart) > 0){
            $dis = $cart->attributes->discount;
        }else{
            $dis = 0;
        }
        
            $data_add = [
                'id'=>  $id,
                'name'=> $data_item->name,
                'quantity'=> 1,
                'price'=> $cost_price,
                'attributes' => [ 
                    'status' =>'add_new', 
                    'discount'=> $dis,
                    'item_bacode'=> $item_bacode,
                    'sell_price'=> $data_item->sell_price
                    ]
            ];
            $userId = auth()->user()->id; // or any string represents user identifier
            $get_cart = $purchase_order->add($data_add);
        }else{
            $data_item = Cs_items::find($id);
                if(count($data_item) > 0){
                    $price = $data_item->sell_price;
                    $cost_price = $data_item->cost_price;
                    $item_bacode = $data_item->item_bacode;
                    
                    $cart = $purchase_order->get($id);
                    if(count($cart) > 0){
                        $dis = $cart->attributes->discount;
                        $qty_free = $cart->attributes->qty_free;
                    }else{
                        $dis = 0;
                        $qty_free = 0;
                    }
                    if(count($cart) > 0){
                        $po = $cart->attributes->status;
                    }else{
                        $po = "add_new";
                    }
                    
                        $data_add = [
                            'id'=>  $id,
                            'name'=> $data_item->name,
                            'quantity'=> 1,
                            'price'=> $cost_price,
                            'attributes' => [ 
                                'qty_free'=>$qty_free,
                                'status'=> $po,
                                'discount'=> $dis,
                                'item_bacode'=> $item_bacode,
                                'sell_price'=> $data_item->sell_price
                                ]
                        ];
                        $userId = auth()->user()->id; // or any string represents user identifier
                        $get_cart = $purchase_order->add($data_add);
            
                      
                }

                $data_po = Cs_Purchase_Order::where('invoice_num','=',$id)->first();
                if(count($data_po) > 0){       
                    foreach($data_po->cs_purchase_order_item as $pi){
                        $data_item1 = Cs_items::find($pi->item_id);
                        $data_add_purchase = [
                            'id'=>  $data_item1->id,
                            'name'=> $data_item1->name,
                            'quantity'=> $pi->qty_order,
                            'price'=> $pi->cost_price,
                            'attributes' => [ 
                                'qty_free'=>$pi->qty_free,
                                'status'=> 'recieving',
                                'discount'=> $pi->discount,
                                'item_bacode'=> $data_item1->item_bacode,
                                'sell_price'=> $data_item1->sell_price
                                ]
                        ];
                        $userId = auth()->user()->id; // or any string represents user identifier
                        $get_cart = $purchase_order->add($data_add_purchase);
                    }

                    $add_po_id = $request->session()->put('purchase_order_id',$data_po->id);
                    if($data_po->supplier_id){
                        $supp = Cs_suppliers::find($data_po->supplier_id); 
                        $request->session()->put('supplier_id',$data_po->supplier_id); 
                        $request->session()->put('supplier_name',$supp->name);   
                    }
                    if(count($data_po->cs_purchase_order_payment) > 0){
                        foreach($data_po->cs_purchase_order_payment as $key => $k){
                          if($k->currency == 1){
                              $cr = "us";
                          }else{
                              $cr = "kh";
                          }
                            $request->session()->push('tran_cash_po', [
                                'id'=> time(),
                                'payment_num'=> $k->total_payment_price,
                                'pay_type_active'=> $k->payment_type,
                                'status'=> $k->status,
                                'status_add' => 'receiving',
                                'pay_active'=> $cr,
                            ]);
                        }
                    }
                    if($data_po->discount_percent){
                         $dis_count = $request->session()->put('discount_globle_po',$data_po->discount_percent);
                    }
                    // if($data_po->deposit_price){
                    //     $request->session()->put('deposit_price',$data_po->deposit_price);
                    // }
                    if($data_po->remain_price){
                        $request->session()->put('remain_price',$data_po->remain_price);
                    }
                    
                    if($data_po->receiving_date){
                        $request->session()->put('receiving_date',$data_po->receiving_date);
                    }
 
                   
                }

        }
        $data =  $purchase_order->getContent();   
        return response()->json($data);
    }
    
    //update card
    public function post_item_update_to_cart_ajax(Request $request,$id){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);

        $deposit_setting = 0.3;
        $data_item = Cs_items::find($id);
        $price = $data_item->sell_price;
        $cost_price = $data_item->cost_price;
        $item_bacode = $data_item->item_bacode;

        $override_default_commission = 1;
        $defult_comission = 0;
        $is_defult_tax = 1 ;
        $is_include_defult_tax = 1; 
        $is_setting_defult_tax = 1 ;
        $item_tax = []; 
        $total_tax = 0 ;

        $cart = $purchase_order->get($id);
        if(count($cart) > 0){
            $po = $cart->attributes->status;
        }else{
            $po = "add_new";
        }
        $data_add = [
                'price'=> $request->price,
                'quantity' => [
                    'relative' => false,
                    'value' => $request->qty
                ],
                'attributes' => [
                    'qty_free' => $request->qty_free,
                    'status'=> $po,
                    'discount'=> $request->discount,
                    'item_bacode'=> $item_bacode,
                    'sell_price'=> $data_item->sell_price
                ]

        ];
         $userId = auth()->user()->id; // or any string represents user identifier
         $purchase_order->update($id,$data_add);
         $data = [
                'update_card'=> $purchase_order->get($id)
                 ];
                //  $request->session()->forget('tran_cash_po');
                //  $request->session()->forget('currency_type_po');
                //  $request->session()->forget('pay_type_po');
        return response()->json($data);
    }


    public function post_item_cart_ajax(Request $request){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);
        $con_cart = $purchase_order->getContent();
        $subtotal = $purchase_order->getSubTotal();
        $carttotal = $purchase_order->getTotal();

        if($request->session()->has('discount_globle_po')){
            $discount_globle_po = $request->session()->get('discount_globle_po');
        }else{
            $discount_globle_po = 0;
        }
        if($request->session()->has('sale_type_po')){
            $sale_type_po = $request->session()->get('sale_type_po');
        }else{
            $sale_type_po = "purchas_order";
        }
        $data = [
            'con_cart'=> $con_cart ,
            'sale_type_po'=> $sale_type_po,
            'subtotal'=> $subtotal ,
            'carttotal'=> $carttotal,
            'discount_globle_po'=>$discount_globle_po,
        ]; 

        return response()->json($data);
    }
    // Remove cart by id item
    public function post_remove_cart_with_item_id(Request $request,$id){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);
        $purchase_order->remove($id);
        $request->session()->forget('tran_cash_po');
        $request->session()->forget('currency_type_po');
        $request->session()->forget('pay_type_po'); 
        $data = [
            'success'=> "Successfull Remove Item - ".$id
      ];
        return response()->json($data);
    }
    // Remove cart in all item
    public function post_cancel_all_cart_item(Request $request){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);
        $purchase_order->clear();
        $request->session()->forget('tran_cash_po');
        $request->session()->forget('discount_globle_po');
        $request->session()->forget('currency_type_po');
        $request->session()->forget('pay_type_po'); 
        $request->session()->forget('supplier_id');
        $request->session()->forget('supplier_name');
        $request->session()->forget('receiving_date');
        
        $request->session()->forget('purchase_order_id');
        $request->session()->forget('remain_price');
        $request->session()->forget('deposit_price');
        return redirect()->to('credit_sales_purchas_order');
    }

    // remove session with ajax
    public function post_cancel_all_cart_item_ajax(Request $request){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);
        $purchase_order->clear();
        $request->session()->forget('tran_cash_po');
        $request->session()->forget('discount_globle_po');
        $request->session()->forget('currency_type_po');
        $request->session()->forget('pay_type_po'); 
        $request->session()->forget('purchase_order_id');
        
        // $request->session()->forget('supplier_id');
        // $request->session()->forget('supplier_name');
        $request->session()->forget('receiving_date');
        $request->session()->forget('remain_price');
        $request->session()->forget('deposit_price');
            $data = [
                'success'=> "Successfull "
        ];
            return response()->json($data);
    }


    // add session 
    public function pos_add_all_session_purchas_order(Request $request){
        if($request->has('supplier_name')){
            $supplier_id = $request->session()->put('supplier_id', $request->supplier_id);
            $supplier_name = $request->session()->put('supplier_name', $request->supplier_name);
        }
        if($request->has('discount_globle_po')){
            $discount_globle_po = $request->session()->put('discount_globle_po', $request->discount_globle_po);
        }
        if($request->has('sale_type_po')){
            $sale_type_po = $request->session()->put('sale_type_po', $request->sale_type_po);
        }
        if($request->has('pay_type_po')){
            $pay_type_po = $request->session()->put('pay_type_po', $request->pay_type_po);
        }
        if($request->has('deposit_price')){
            $deposit_price = $request->session()->put('deposit_price', $request->deposit_price);
        }
        if($request->has('currency_type')){
            $currency_type = $request->session()->put('currency_type_po', $request->currency_type);
        }
        if($request->session()->has('sale_type_po')){
            $sale_type_po = $request->session()->get('sale_type_po');
        }else{
            $sale_type_po = "purchas_order";
        }
        if($request->has('payment_num')){
            if($request->payment_num != 0){

                $request->session()->push('tran_cash_po', [
                    'id'=> time(),
                    'payment_num'=> $request->payment_num,
                    'pay_type_active'=> $request->pay_type_active,
                    'status'=>'pay',
                    'status_add' => $sale_type_po,
                    'pay_active'=> $request->pay_active,
                ]);
            }

                 }
        $data = [
            'success'=>'seccess',
            'sale_type_po'=> $request->session()->get('sale_type_po'),
            'tran_cash_po'=>$request->session()->get('tran_cash_po')
           
        ];

        return response()->json($data);
    }
    public function remove_tran_cash(Request $request ,$pay_id){
        if($request->session()->has('tran_cash_po')){
            $payment = collect($request->session()->get('tran_cash_po'));
            $payment->pull($pay_id);
          }
          $request->session()->put('tran_cash_po',$payment->all());

           
            $request->session()->forget('currency_type_po');
            $request->session()->forget('pay_type_po'); 
          $data = [
                'success'=>  "success remove transation"
          ];
         return response()->json($data);
        //return redirect()->to('credit_sales');
    }
     // get all  session 
     public function get_add_all_session_purchas_order(Request $request){
        if($request->session()->has('supplier_name')){
            $supplier_id = $request->session()->get('supplier_id');
            $supplier_name = $request->session()->get('supplier_name');
        }else{
            $supplier_id = 0;
            $supplier_name = null;
        }
        if($request->session()->has('discount_globle_po')){
            $discount_globle_po = $request->session()->get('discount_globle_po');
        }else{
            $discount_globle_po = 0;
        }
        if($request->session()->has('sale_type_po')){
            $sale_type_po = $request->session()->get('sale_type_po');
        }else{
            $sale_type_po = "purchas_order";
        }
        if($request->session()->has('pay_type_po')){
            $pay_type_po = $request->session()->get('pay_type_po');
        }else{
            $pay_type_po = "cash";
        }
        if($request->session()->has('deposit_price')){
            $deposit_price = $request->session()->get('deposit_price');
        }else{
            $deposit_price = 0;
        }
        if($request->session()->has('currency_type_po')){
            $currency_type_po = $request->session()->get('currency_type_po');
        }else{
            $currency_type_po = "us";
        }

        if($request->session()->has('tran_cash_po')){
            $tran_cash_po = $request->session()->get('tran_cash_po');
        }else{
            $tran_cash_po = [];
        }
        if($request->session()->has('deposit_price')){
            $deposit_price = $request->session()->get('deposit_price');
        }else{
            $deposit_price = 0;
        }
        if($request->session()->has('remain_price')){
            $remain_price = $request->session()->get('remain_price');
        }else{
            $remain_price = 0;
        }

        if($request->session()->has('receiving_date')){
            $receiving_date = $request->session()->get('receiving_date');
        }else{
            $receiving_date = null;
        }
        if($request->session()->has('purchase_order_id')){
            $purchase_order_id = $request->session()->get('purchase_order_id');
        }else{
            $purchase_order_id = null;
        }
      
        $data = [
            'purchase_order_id'=> $purchase_order_id,
            'receiving_date'=> $receiving_date,
            'remain_price' => $remain_price,
            'deposit_price'=> $deposit_price,
            'supplier_id'=>$supplier_id,
            'supplier_name'=>$supplier_name,
            'discount_globle_po'=>$discount_globle_po,
            'sale_type_po'=>$sale_type_po,
            'pay_type_po'=>$pay_type_po,
            'currency_type_po'=>$currency_type_po,
            'tran_cash_po' => $tran_cash_po,
         
        ];
        return response()->json($data);
    }
    

    public function completed_purchas_order(Request $request){
        $pur_orders = app('purchase_order');
        $userId = auth()->user()->id; 
        $purchase_order = $pur_orders->session($userId);
        $defualt_required_request_form = 1;
      
        $deposit_setting = 0.3;
        $defult_currency = 1;
        $interest_of_owne_precent = 0.112;


        $data_count = $purchase_order->getContent();
        if($request->session()->has('supplier_name')){
            $supplier_id = $request->session()->get('supplier_id');
        }else{
            $supplier_id = 0;
        }
        if($request->session()->has('discount_globle_po')){
            $discount_globle_po = $request->session()->get('discount_globle_po');
            $discount_reason = "Spacail Price";
        }else{
            $discount_globle_po = 0;
            $discount_reason =  null;
        }
        if($request->session()->has('sale_type_po')){
            $sale_type_po = $request->session()->get('sale_type_po');
        }else{
            $sale_type_po = "purchas_order";
        }
        if($request->session()->has('pay_type_po')){
            $pay_type_po = $request->session()->get('pay_type_po');
        }else{
            $pay_type_po = "cash";
        }
        if($request->session()->has('currency_type_po')){
            $currency_type = $request->session()->get('currency_type_po');
           if($currency_type_po == "us"){
                $currency = 1;
           }else{
                $currency =2;   
           }
            $currency_type_po = $request->session()->get('currency_type_po'); 
        }else{
            $currency = 1;
            $currency_type_po = "us";
        }

        
        // $total_due_amout = str_replace(',','',$request->total_due_amout);
        // $total_amount = str_replace(',','',$request->total_amount);
            
        $method = $sale_type_po;
        if($request->has('staff_id')){
            $staff_id = $request->staff_id;
        }else{
            $staff_id = Auth::user()->id;
        }
      

        if($request->has('has_pay_tran')){
            $payment_amount = $request->has_pay_tran;
        }else{
            $payment_amount = 0;
        }
        if($request->has('total_payment')){
            $total_payment = $request->total_payment;
        }else{
            $total_payment = 0;
        }

        if($request->has('sub_payment_amount')){
            $sub_payment_amount = $request->sub_payment_amount;
        }else{
            $sub_payment_amount = 0;
        }

        if($request->has('dute_payment_amount')){
            $dute_payment_amount = $request->dute_payment_amount;
        }else{
            $dute_payment_amount = 0;
        }
        if($request->has('deposit_price')){
            $deposit_price = $request->deposit_price;
        }else{
            $deposit_price = 0;
        }
        
        // if($request->has('prices_total_num')){
        //     $prices_total_num = $request->prices_total_num;
        // }else{
        //     $prices_total_num = 0;
        // }
 

        if($sale_type_po == "purchas_order"){
            $total_qty =$purchase_order->getTotalQuantity();
            $status_for_purchase = "pending";
            $qty_order = $total_qty ;
            if($request->date_for_receiving != ''){

                $order_modify_date = date('Y-m-d h:m:s',strtotime($request->date_for_receiving));
                $order_date = date('Y-m-d h:m:s',strtotime($request->date_for_receiving));
            }else{
                $order_modify_date = date('Y-m-d h:m:s');
                $order_date = date('Y-m-d h:m:s');
               
            }
        }else{

        }

        if($request->has('comment')){
            $comment = $request->comment;
        }else{
            $comment = null;
        }
        $remain_price = $dute_payment_amount - $deposit_price;
        $branch_id = Auth::user()->branch_id;
        $user_id = Auth::user()->id;
        if($sale_type_po == "purchas_order"){
                $data_po = [
                    'supplier_id' => $supplier_id,
                    'branch_id'=> $branch_id,
                    'user_id'=> $user_id,
                    'stuff_order_id'=>$staff_id,
                    'status'=> 1,
                    'active'=> 1,
                    'qty_order'=> $qty_order,
                    'deposit_price'=> $deposit_price,
                    'remain_price'=> $remain_price,
                    'order_date'=> $order_date,
                    'order_modify_date'=> $order_modify_date,
                    'total_price'=> $total_payment,
                    'sup_total_price'=> $sub_payment_amount,
                    'dute_total_price'=> $dute_payment_amount,
                    'currency'=> $currency,
                    'discount_percent'=> $discount_globle_po,
                    'status_payment'=> $sale_type_po,
                    'status_for_purchase'=> $status_for_purchase,
                    'command_purchase'=> $comment,
                    'supspend'=> 1,
                    'deleted'=> 1,
                    'updated_at'=> date('Y-m-d h:m:s'),
                    'created_at'=> date('Y-m-d h:m:s')
                ];
    
            $cs_purchase_order_id = Cs_Purchase_Order::insertGetId($data_po);
            $numinvo = "PO".date('Y')."".date('m')."".date('d')."".$cs_purchase_order_id;
            Cs_Purchase_Order::where('id','=',$cs_purchase_order_id)->update(['invoice_num'=> $numinvo]);
            
        }elseif($sale_type_po == "receiving"){
            $total_qty =$purchase_order->getTotalQuantity();
            $status_for_purchase = "done";
            $qty_receiving =  $total_qty;
            if($request->date_for_receiving != ''){
                $receiving_date = date('Y-m-d h:m:s',strtotime($request->date_for_receiving));
                $order_modify_date = date('Y-m-d h:m:s',strtotime($request->date_for_receiving));
            }else{
                $receiving_date = date('Y-m-d h:m:s');   
                $order_modify_date = date('Y-m-d h:m:s');  
            }
            if($request->session()->has('purchase_order_id')){
                $id = $request->session()->get('purchase_order_id');
                $data_pro = Cs_Purchase_Order::find($id);
            
                if(count($data_pro) > 0 ){
                        $data_po = [
                            'supplier_id' => $supplier_id,
                            'branch_id'=> $branch_id,
                            'user_id'=> $user_id,
                            'stuff_receiving_id'=>$staff_id,
                            'qty_receiving'=> $qty_receiving,
                            'deposit_price'=> $deposit_price,
                            'remain_price'=> $remain_price,
                            'receiving_date' => $receiving_date,
                            'order_modify_date'=> $order_modify_date,
                            'total_price'=> $total_payment,
                            'sup_total_price'=> $sub_payment_amount,
                            'dute_total_price'=> $dute_payment_amount,
                            'currency'=> $currency,
                            'is_receiving'=>1,
                            'supspend'=> 1,
                            'deleted'=> 1,
                            'discount_percent'=> $discount_globle_po,
                            'status_payment'=> $sale_type_po,
                            'status_for_purchase'=> $status_for_purchase,
                            'receiving_command'=> $comment,
                            'updated_at'=> date('Y-m-d h:m:s')
                        ];

                            Cs_Purchase_Order::where('id','=',$id)->update($data_po);
                            $cs_purchase_order_id = $id;
                    }else{
                        $data_po = [
                            'supplier_id' => $supplier_id,
                            'branch_id'=> $branch_id,
                            'user_id'=> $user_id,
                            'stuff_receiving_id'=>$staff_id,
                            'qty_receiving'=> $qty_receiving,
                            'deposit_price'=> $deposit_price,
                            'remain_price'=> $remain_price,
                            'receiving_date' => $receiving_date,
                            'order_modify_date'=> $order_modify_date,
                            'total_price'=> $total_payment,
                            'sup_total_price'=> $sub_payment_amount,
                            'dute_total_price'=> $dute_payment_amount,
                            'currency'=> $currency,
                            'is_receiving'=>1,
                            'supspend'=> 1,
                            'deleted'=> 1,
                            'discount_percent'=> $discount_globle_po,
                            'status_payment'=> $sale_type_po,
                            'status_for_purchase'=> $status_for_purchase,
                            'receiving_command'=> $comment,
                            'updated_at'=> date('Y-m-d h:m:s')
                        ];
        
                        $cs_purchase_order_id = Cs_Purchase_Order::insertGetId($data_po);
                        $numinvo = "PO".date('Y')."".date('m')."".date('d')."".$cs_purchase_order_id;
                        Cs_Purchase_Order::where('id','=',$cs_purchase_order_id)->update(['invoice_num'=> $numinvo]);
                    }    
            }else{
                $data_po = [
                    'supplier_id' => $supplier_id,
                    'branch_id'=> $branch_id,
                    'user_id'=> $user_id,
                    'stuff_receiving_id'=>$staff_id,
                    'qty_receiving'=> $qty_receiving,
                    'deposit_price'=> $deposit_price,
                    'remain_price'=> $remain_price,
                    'receiving_date' => $receiving_date,
                    'order_modify_date'=> $order_modify_date,
                    'total_price'=> $total_payment,
                    'sup_total_price'=> $sub_payment_amount,
                    'dute_total_price'=> $dute_payment_amount,
                    'currency'=> $currency,
                    'is_receiving'=>1,
                    'is_receiving'=>1,
                    'supspend'=> 1,
                    'deleted'=> 1,
                    'discount_percent'=> $discount_globle_po,
                    'status_payment'=> $sale_type_po,
                    'status_for_purchase'=> $status_for_purchase,
                    'receiving_command'=> $comment,
                    'updated_at'=> date('Y-m-d h:m:s')
                ];

                $cs_purchase_order_id = Cs_Purchase_Order::insertGetId($data_po);
                $numinvo = "PO".date('Y')."".date('m')."".date('d')."".$cs_purchase_order_id;
                Cs_Purchase_Order::where('id','=',$cs_purchase_order_id)->update(['invoice_num'=> $numinvo]);
                
            }   
        }
        if($cs_purchase_order_id != 0){
          
            if($sale_type_po == "purchas_order"){
              
                
                if(count($data_count) > 0){
                        foreach($data_count as $dc){
                            if($dc->quantity > 0){
                                $disqty = number_format(($dc->price * $dc->quantity)/100,2);
                            }else{
                                $disqty = 0;
                            }

                            $data_po_payment = [
                                'item_id'=>$dc->id,
                                'cs_purchase_order_id'=> $cs_purchase_order_id,
                                'qty_order'=> $dc->quantity,
                                'cost_price'=> $dc->price,
                                'sub_total'=> $dc->price * $dc->quantity, 
                                'total'=> $dc->price - $disqty,
                                'discount'=> $dc->attributes->discount  
                            ];
                            Cs_Purchase_Order_Item::insert($data_po_payment);
                        }
                    }
                     // add pay tran session 
                     if($request->has('prices_total_num')){
                        if($request->prices_total_num != 0){

                            $request->session()->push('tran_cash_po', [
                                'id'=> time(),
                                'payment_num'=> $request->prices_total_num,
                                'pay_type_active'=>'store_account',
                                'status' =>'pay',
                                'pay_active'=> $currency_type_po,
                            ]);
                        }
                    }
                    if($request->has('deposit_price')){
                        if($request->deposit_price != 0){

                            $request->session()->push('tran_cash_po', [
                                'id'=> time(),
                                'payment_num'=> $request->deposit_price,
                                'pay_type_active'=>'cash',
                                'status'=>'desposit',
                                'pay_active'=> $currency_type_po,
                            ]);
                        }
                    }
                        if($request->session()->has('tran_cash_po')){
                        $tran_cash = $request->session()->get('tran_cash_po');
                       
                        foreach($tran_cash as $key => $v){
                            if($v['pay_active'] == "us" ){
                                $current = 1;
                            }else{
                                $current = 2;
                            }
                            if($v['payment_num'] != 0){
                                $data_payment_tran = [
                                    'cs_purchase_order_id'=>$cs_purchase_order_id,
                                    'total_payment_price' =>$v['payment_num'],
                                    'currency' =>$current,
                                    'payment_type' =>$v['pay_type_active'],
                                    'status' => $v['status'],
                                    'status_payment'=> $sale_type_po
                                ];
                                Cs_Purchase_Order_Payment::insert($data_payment_tran);     
                            }
                            
                        } 
                    } 
            }else{
                    if(count($data_count) > 0){
                        foreach($data_count as $dc){
                            if($dc->attributes->discount > 0){
                                $disqty = number_format(($dc->price * $dc->quantity) * ($dc->attributes->discount /100),2);
                            }else{
                                $disqty = 0;
                            }
                            if($dc->attributes->qty_free >0 ){
                               
                                $sub_total_qty = $dc->price * $dc->quantity;
                                $total = $sub_total_qty - $disqty;
                                $sutotal_qty_free1 = $dc->price * $dc->attributes->qty_free;
                                $sutotal_qty_free = $sub_total_qty - $sutotal_qty_free1;
                                $total_qty_free = $total - $sutotal_qty_free1;
                            }else{
                                $sub_total_qty = $dc->price * $dc->quantity;
                                $total = $sub_total_qty - $disqty;
                                $sutotal_qty_free = $dc->price * $dc->quantity;
                                $total_qty_free = $total;
                            }
                            if($dc->attributes->status == "add_new"){ 
                                $data_po_payment = [
                                    'item_id'=>$dc->id,
                                    'cs_purchase_order_id'=> $cs_purchase_order_id,
                                    'qty_receiving'=> $dc->quantity,
                                    'qty_free' => $dc->attributes->qty_free,
                                    'cost_price'=> $dc->price,
                                    'sub_total'=> $sutotal_qty_free, 
                                    'total'=> $total_qty_free,
                                    'discount'=> $dc->attributes->discount  
                                ];
                                Cs_Purchase_Order_Item::insert($data_po_payment);
                            }else{
                                $data_po_payment = [
                                    'item_id'=>$dc->id,
                                    'cs_purchase_order_id'=> $cs_purchase_order_id,
                                    'qty_receiving'=> $dc->quantity,
                                    'qty_free' => $dc->attributes->qty_free,
                                    'cost_price'=> $dc->price,
                                    'sub_total'=> $sutotal_qty_free, 
                                    'total'=> $total_qty_free,
                                    'discount'=> $dc->attributes->discount  
                                ];
                                Cs_Purchase_Order_Item::where('cs_purchase_order_id','=',$cs_purchase_order_id)->where('item_id','=',$dc->id)->update($data_po_payment);
                            }

                            $data_inventory = [
                                    'item_id'=>$dc->id,
                                    'branch_id' => $branch_id,
                                    'user_id'=> $user_id,
                                    'spacifies' => $cs_purchase_order_id,
                                    'status_transation' => 'receiving',
                                    'qty' => $dc->quantity,
                                    'qty_free' => $dc->attributes->qty_free,
                                    'updated_at'=> date('Y-m-d h:m:s'),
                                    'created_at'=> date('Y-m-d h:m:s')

                            ];
                            Cs_inventorys::insert($data_inventory);
                        }


                    }
                     // add pay tran session 
                     if($request->has('prices_total_num')){
                        if($request->prices_total_num != 0){

                            $request->session()->push('tran_cash_po', [
                                'id'=> time(),
                                'payment_num'=> $request->prices_total_num,
                                'pay_type_active'=>'store_account',
                                'status' =>'pay',
                                'pay_active'=> $currency_type_po,
                            ]);
                        }
                    }
                    if($request->has('deposit_price')){
                        if($request->deposit_price != 0){

                            $request->session()->push('tran_cash_po', [
                                'id'=> time(),
                                'payment_num'=> $request->deposit_price,
                                'pay_type_active'=>'cash',
                                'status'=>'desposite',
                                'pay_active'=> $currency_type_po,
                            ]);
                        }
                    }
                        if($request->session()->has('tran_cash_po')){
                        $tran_cash = $request->session()->get('tran_cash_po');
                        Cs_Purchase_Order_Payment::where('cs_purchase_order_id','=',$cs_purchase_order_id)->delete();
                        foreach($tran_cash as $key => $v){
                            if($v['pay_active'] == "us"){
                                $current = 1;
                            }else{
                                $current = 2;
                            }
                            if($v['payment_num'] != 0){
                                $data_payment_tran = [
                                    'cs_purchase_order_id'=>$cs_purchase_order_id,
                                    'total_payment_price' =>$v['payment_num'],
                                    'currency' =>$current ,
                                    'payment_type' =>$v['pay_type_active'],
                                    'status' => $v['status'],
                                    'status_payment'=> $sale_type_po
                                ];
                                Cs_Purchase_Order_Payment::insert($data_payment_tran);     
                            }
                            
                        } 
                    } 

                

            }
                      
  
                $data = Cs_Purchase_Order::find($cs_purchase_order_id);      
        }else{
            $data = [];
        }    
     
          
          
            $purchase_order->clear();
            $request->session()->forget('tran_cash_po');
            $request->session()->forget('discount_globle_po');
            $request->session()->forget('purchase_order_id');
            $request->session()->forget('currency_type_po');
            $request->session()->forget('pay_type_po'); 
            $request->session()->forget('supplier_id');
            $request->session()->forget('supplier_name');
            $request->session()->forget('receiving_date');
            $request->session()->forget('remain_price');
            $request->session()->forget('deposit_price'); 

    
          
       return response()->json($data);
    }
     


    public function get_show_invoice_json(Request $request,$id){
        $cs_purchase_order = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers'])->where('id','=',$id)->first();
       
        $data = [
            'cs_purchase_order' => $cs_purchase_order
            
        ];
        return response()->json($data);
        
    }

    public function get_show_invoice(Request $request,$id){

        $title = "Show Invoice Purchase Order";
       $po_id = $id;
        return view('credit_sale.purchase_order.po_invoice',compact('title','po_id'));
    }
    public function get_show_receive_invoice(Request $request, $id){
        $title = "Show Invoice Receive Order";
        $po_id = $id;
        return view('credit_sale.purchase_order.receive_invoice',compact('title','po_id'));
    }

    // Deleted Sale from database  

    public function deleted_sale_from_database(Request $request,$id){
        
        Cs_Cds_Request_Form::where('sale_id','=',$id)->delete();
        Cs_Sale_Payment::where('sale_id','=',$id)->delete();
        Cs_Sale_Payment_Tran::where('sale_id','=',$id)->delete();     
        Cs_Sale_Item::where('sale_id','=',$id)->delete();
        Cs_Sale::where('id','=',$id)->delete();
        Cs_inventorys::where('spacifies','=',$id)->delete();
        return response()->json(['msg_show'=>'deleted success']); 
    }



}
