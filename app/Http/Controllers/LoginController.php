<?php



namespace App\Http\Controllers;



use Illuminate\Http\Request;

use Illuminate\Support\Facades\Auth;

use App\Http\Requests;

use App\Http\Controllers\Controller;

use DB;

class LoginController extends Controller

{

   public function getlogin(){

     return view('credit_sale.login');

   }

   public function postlogin(Request $request){

      $this->validate($request, [

        'username' => 'required',

        'password'  => 'required'

    ]);
      if (Auth::attempt(['username' => $request->username, 'password' => $request->password, 'user_status' => 1])) {

           $iduser = Auth::id();

            DB::table('users')->where('id','=',$iduser)->update(array('user_last_login'=> date('Y-m-d h:m:s')));
              return redirect()->intended('/')->with('success','Welcome to Credit Sale System');
            $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "Log In",
                'user_id'=> Auth::user()->id,
                'status'=> '1',
                'create_date' => date('Y-m-d h:m:s'),
                'date_login' =>  date('Y-m-d h:m:s')
            ];
          DB::table('cs-history-logs')->insert($us_gr);
      }else{
           return redirect()->to('login')->withErrors('Please Check You Username and Password Again Is Not Correct');
      }

    }



    public function getlogout(Request $request){
       $iduser = Auth::id();
        $us_gr = [
                'ip_log'=> $request->ip(),
                'active'=> "Logout",
                'user_id'=> Auth::user()->id,
                'status'=> '1',
                'create_date' => date('Y-m-d h:m:s'),
                'date_logout' =>  date('Y-m-d h:m:s')
            ];
       DB::table('users')->where('id','=',$iduser)->update(['user_last_logout'=> date('Y-m-d h:m:s')]);
   
       Auth::logout();
       return redirect()->to('login');

    }



}
