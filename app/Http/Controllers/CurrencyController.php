<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use App\Currency;

class CurrencyController extends Controller
{
     public function get_list(){
    	$data = Currency::all();
    	return view('credit_sale.currency.index', compact('data'));
    }
    public function get_create(){

        $title = "Create Currency";
    
        return view('credit_sale.currency.create')->with('title',$title);

    }
    public function post_create(Request $request)
    {
    	$data = [
            'name'=>$request->name,
    		'value_option'=>$request->value_option,
    		'note'=>$request->note
    	];
    	Currency::insert($data);
    	return Redirect()->to('currency');
    }
      public function get_edit($id)
    {
    	$data = Currency::find($id);
    	return view('credit_sale.currency.edit', compact('data'));
    }
      public function post_edit(Request $request,$id)
    {
    	$data = [
            'name'=>$request->name,
    		'value_option'=>$request->value_option,
    		'note'=>$request->note,
    	];

    	Currency::where('id','=',$id)->update($data);
    	return Redirect()->to('currency');
    }
      

   public function post_delete(Request $edit, $id)
    {
        $data = Currency::find($id);
        $data->delete();
        return redirect()->to('currency');
    }
}
