<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Sale;
use Session;

class ReportSaleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth'); 
    }

    // Report Sale By Cash
    public function report_sale(Request $request)
    {
        $title = "Report Sale By Cash";
        $codes = Cs_Sale::orderBy('num_invoice','asc')
                        ->where('deleted','=',1)
                        ->where('method','=','sale_by_cash')
                        ->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->num_invoice]);
        }
        return view('credit_sale.report.report_sale', compact('title','code'));
    }

    public function report_sale_json(Request $request)
    {
        if($request->has('submit_search')){
            // Search Start date To date
            if($request->from_date != "" && $request->to_date != ""){
                // from date
                $from_date = date('Y-m-d',strtotime($request->from_date));
                Session::flash('from_date',$request->from_date);
                // To date
                $to_date = date('Y-m-d',strtotime($request->to_date));
                Session::flash('to_date',$request->to_date);
            }else{
                // from date
                $from_date = '1970-01-01';
                Session::forget('from_date');
                // To date
                $to_date = date('Y-m-d');
                Session::forget('to_date');
            }
            // Search Brand
            if($request->brand_name != ""){
                $brand_name = $request->brand_name;
                Session::flash('brand_name',$request->brand_name);
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            // Search Currency
            if($request->currency != ""){
                $currency = $request->currency;
                Session::flash('currency',$request->currency);
            }else{
                $currency = '%_%';
                Session::forget('currency');
            }
            // Search Client Name
            if($request->client_name != ""){
                $client_name = $request->client_name;
                Session::flash('client_name',$request->client_name);
            }else{
                $client_name = '%_%';
                Session::forget('client_name');
            }
            // Search Schedule ID
            if($request->sale_id != ""){
                $sale_id = $request->sale_id;
                Session::flash('sale_id',$request->sale_id);
            }else{
                $sale_id = '%_%';
                Session::forget('sale_id');
            }
            // Search Staff Name
            if($request->staff_name != ""){
                $staff_name = $request->staff_name;
                Session::flash('staff_name',$request->staff_name);
            }else{
                $staff_name = '%_%';
                Session::forget('staff_name');
            }

            $data = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
            ->where('deleted','=',1)
            ->where('method','=','sale_by_cash')
            ->whereBetween('due_date', [$from_date,$to_date])
            ->where('branch_id','LIKE',$brand_name)
            ->where('currency_id','LIKE',$currency)
            ->where('client_id','LIKE',$client_name)
            ->where('num_invoice','LIKE',$sale_id)
            ->where('user_id','LIKE',$staff_name)
            ->paginate(15);
        }else{
            $data = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
            ->where('deleted','=',1)
            ->where('method','=','sale_by_cash')
            ->paginate(15);
        }

        return response()->json(['data'=>$data]);
    }

    // Report Sale By Credit
    public function report_sale_by_credit(Request $request)
    {
        $title = "Report Sale By Credit";
        $codes = Cs_Sale::orderBy('num_invoice','asc')
                        ->where('deleted','=',1)
                        ->where('method','=','sale_by_credit')
                        ->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->num_invoice]);
        }
        return view('credit_sale.report.report_sale_on_credit', compact('title','code'));
    }

    public function report_sale_by_credit_json(Request $request)
    {

        if($request->has('submit_search')){
            // Search Start date To date
            if($request->from_date != "" && $request->to_date != ""){
                // from date
                $from_date = date('Y-m-d',strtotime($request->from_date));
                Session::flash('from_date',$request->from_date);
                // To date
                $to_date = date('Y-m-d',strtotime($request->to_date));
                Session::flash('to_date',$request->to_date);
            }else{
                // from date
                $from_date = '1970-01-01';
                Session::forget('from_date');
                // To date
                $to_date = date('Y-m-d');
                Session::forget('to_date');
            }
            // Search Brand
            if($request->brand_name != ""){
                $brand_name = $request->brand_name;
                Session::flash('brand_name',$request->brand_name);
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            // Search Currency
            if($request->currency != ""){
                $currency = $request->currency;
                Session::flash('currency',$request->currency);
            }else{
                $currency = '%_%';
                Session::forget('currency');
            }
            // Search Client Name
            if($request->client_name != ""){
                $client_name = $request->client_name;
                Session::flash('client_name',$request->client_name);
            }else{
                $client_name = '%_%';
                Session::forget('client_name');
            }
            // Search Schedule ID
            if($request->sale_id != ""){
                $sale_id = $request->sale_id;
                Session::flash('sale_id',$request->sale_id);
            }else{
                $sale_id = '%_%';
                Session::forget('sale_id');
            }
            // Search Staff Name
            if($request->staff_name != ""){
                $staff_name = $request->staff_name;
                Session::flash('staff_name',$request->staff_name);
            }else{
                $staff_name = '%_%';
                Session::forget('staff_name');
            }

            $data = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
            ->where('deleted','=',1)
            ->where('method','=','sale_by_credit')
            ->whereBetween('due_date', [$from_date,$to_date])
            ->where('branch_id','LIKE',$brand_name)
            ->where('currency_id','LIKE',$currency)
            ->where('client_id','LIKE',$client_name)
            ->where('num_invoice','LIKE',$sale_id)
            ->where('user_id','LIKE',$staff_name)
            ->paginate(15);
        }else{
            $data = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
            ->where('deleted','=',1)
            ->where('method','=','sale_by_credit')
            ->paginate(15);
        }

        return response()->json(['data'=>$data]);
    }
}
