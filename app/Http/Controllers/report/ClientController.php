<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\lib\Session_Search;
use App\Cs_Client;
use DB;
use Auth;
use Session;
use DateTime;
use Excel;
class ClientController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    // Report Client List 
    public function report_client_list(Request $request)
    {
        $title = "របាយការណ៍លម្អិតពីអតិថិជន";
        if($request->has('reset')){
            Session_Search::clear_session_search();
            return redirect()->to('report/clients/report_client_list');
        }
        if($request->has('export')){
        
            $data_query = $this->get_data_cient($request); 
            $data_all =  $data_query->get();  
            $day_export = date('d-m-Y');
            Excel::create('របាយការណ៍លម្អិតពីអតិថិជន('.$day_export.')',function($excel) use ($data_all){
                    $excel->sheet('របាយការណ៍លម្អិតពីអតិថិជន',function($sheet) use ($data_all){

                        // Font family
                    $sheet->setFontFamily('Battambang');
                    // Sets all borders0815
                    $sheet->setAllBorders('thin');

                        $sheet->rows(array(
                            array('','','', '', 'របាយការណ៍លម្អិតពីអតិថិជន','','',''),
                             array('ល.រ','លេខកូដអតិថិជន','គោត្តនាម និង នាម','អាសយដ្ឋាន', 'ភេទ', 'លេខទូរសព្ទ',
                                'លេខសម្គាល់អត្តសញ្ញាណ','ថ្ងៃខែឆ្នាំកំណើត')
                         ));
                         $row_total = count($data_all) + 5;
                        foreach($data_all as $key1 => $dat){  
                            $address; $hn; $gn; $st; $vl; $cm; $dis; $pro; $gd; $sts; $dob;
                                if($dat->home_num !== null){
                                    $hn = $dat->home_num .",";
                                }else{
                                    $hn = "";
                                }
                                if($dat->group_num !== null){
                                    $gn = $dat->group_num .",";
                                }else{
                                    $gn = "   ";
                                }
                                if($dat->street_num !== null){
                                    $st = $dat->street_num .",";
                                }else{
                                    $st = "  ";
                                }
                                if($dat->vilige !== null){
                                    $vl = $dat->vilige .",";
                                }else{
                                    $vl = "";
                                }
                                if($dat->commune !== null){
                                    $cm = $dat->commune .",";
                                }else{
                                    $cm = "";
                                }
                                if($dat->district !== null){
                                    $dis = $dat->district .",";
                                }else{
                                    $dis = "";
                                }
                                if($dat->province !== null){
                                    $pro = $dat->province .",";
                                }else{
                                    $pro = "";
                                }
                                
                                $address = $hn ." ". $gn ." ".  $st ."  ".  $vl  ."  ".  $cm ."  ".  $dis ."  ".  $pro ; 
                            if($dat->gender !== 'F'){
                                  $gd = "ប្រុស"; 
                            }else{
                                  $gd = "ស្រី"; 
                            }
                            $client_code = $dat->client_code;   
                            $client_name = $dat->kh_username;
                            $client_phone = $dat->phone;
                            $identify_num =  $dat->identify_num;
                            $dob =  date('d-m-Y',strtotime($dat->dob));
                            //================================================
                            $sheet->rows(array(
                                 array($key1+1, $client_code, $client_name, $address, $gd, $client_phone, $identify_num, $dob )
                             ));

                            //=============================================
                        }
                        //Background Color
                        $sheet->row(2, function ($row) {
                              $row->setBackground('#ffc1c1');
                           });

                        //Align Center
                        $sheet->cells('A1:H'.$row_total, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            });
                    });
                })->export('xlsx');
        }
        $code = [];
        return view('credit_sale.report.clients.report_client_list', compact('title', 'code'));
    }
    
    public function get_data_cient(Request $request){
        if($request->has('submit_search')){
            $data_search = Session_Search::search_form($request);
             $data_query = Cs_Client::with(['cs_sale.sale_item'])
                     ->where('deleted',0)
                     ->whereBetween('created_at', [$data_search['from_date'],$data_search['to_date']])
                     ->where('branch_id','LIKE',$data_search['brand_name'])
                     ->where('id','LIKE',$data_search['client_name'])
                     ->where('user_id','LIKE',$data_search['staff_name']);

         }else{
             $data_query = Cs_Client::with(['cs_sale.sale_item'])->where('deleted',0);
            
         }

         return  $data_query; 
    }
    public function report_client_list_json(Request $request)
    {
        $data_query = $this->get_data_cient($request);
        $data = $data_query->paginate(15); 
        $data_all =  $data_query->get();  
        return response()->json(['data'=>$data]);
    }

}
