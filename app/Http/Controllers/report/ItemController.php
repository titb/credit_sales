<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cs_Items;
use DB;
use Auth;
use Session;
use DateTime;
use Excel;
class ItemController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    // search form 
    public function search_form(Request $request){
             // Search Start date To date
             if($request->from_date != "" && $request->to_date != ""){
                // from date
                $from_date = date('Y-m-d',strtotime($request->from_date));
                Session::put('from_date',$request->from_date);
                // To date
                $to_date = date('Y-m-d',strtotime($request->to_date));
                Session::put('to_date',$request->to_date);
            }else{
                // from date
                $from_date = '1970-01-01';
                Session::forget('from_date');
                // To date
                $to_date = date('Y-m-d');
                Session::forget('to_date');
            }
            // Search Brand
            if($request->brand_name != ""){
                $brand_name = $request->brand_name;
                Session::put('brand_name',$request->brand_name);
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            // Search Currency
            if($request->currency != ""){
                $currency = $request->currency;
                Session::put('currency',$request->currency);
            }else{
                $currency = '%_%';
                Session::forget('currency');
            }
            // Search Client Name
            if($request->client_name != ""){
                $client_name = $request->client_name;
                Session::put('client_name',$request->client_name);
            }else{
                $client_name = '%_%';
                Session::forget('client_name');
            }
            // Search Schedule ID
            if($request->sale_id != ""){
                $sale_id = $request->sale_id;
                Session::put('sale_id',$request->sale_id);
            }else{
                $sale_id = '%_%';
                Session::forget('sale_id');
            }
            // Search Staff Name
            if($request->staff_name != ""){
                $staff_name = $request->staff_name;
                Session::put('staff_name',$request->staff_name);
            }else{
                $staff_name = '%_%';
                Session::forget('staff_name');
            }
            
            $data = [
                'from_date' => $from_date,
                'to_date' => $to_date,
                'brand_name' => $brand_name,
                'currency' => $currency,
                'client_name'=> $client_name,
                'sale_id' => $sale_id,
                'staff_name' => $staff_name
            ];
            return $data;
    }
    public function clear_session_search(){
        Session::forget('from_date');
        Session::forget('to_date');
        Session::forget('brand_name');
        Session::forget('currency');
        Session::forget('client_name');
        Session::forget('sale_id');
        Session::forget('staff_name');
        return;
    }
    // Report Client List 
    public function report_item_list(Request $request)
    {
        $title = "របាយការណ៍លម្អិតពីឥវ៉ាន់";

        if($request->has('export')){
        
            $data_query = $this->get_data_item($request); 
            $data_all =  $data_query->get();  
            $day_export = date('d-m-Y');
            Excel::create('របាយការណ៍លម្អិតពីឥវ៉ាន់('.$day_export.')',function($excel) use ($data_all){
                    $excel->sheet('របាយការណ៍លម្អិតពីឥវ៉ាន់',function($sheet) use ($data_all){

                        // Font family
                    $sheet->setFontFamily('Battambang');
                    // Sets all borders0815
                    $sheet->setAllBorders('thin');

                        $sheet->rows(array(
                            array('','','', '', 'របាយការណ៍លម្អិតពីឥវ៉ាន់','','',''),
                             array('ល.រ','លេខកូដឥវ៉ាន់','ឈ្មោះ','ម៉ាក', 'ប្រភេទ', 'ចំនួន',
                                'តម្លៃដើម','តម្លៃលក់')
                         ));
                         $row_total = count($data_all) + 5;
                        foreach($data_all as $key1 => $dat){  
                            $address; $hn; $gn; $st; $vl; $cm; $dis; $pro; $gd; $sts; $dob;
                                if($dat->item_bacode !== null){
                                    $hn = $dat->item_bacode;
                                }else{
                                    $hn = "";
                                }
                                if($dat->name !== null){
                                    $gn = $dat->name;
                                }else{
                                    $gn = "   ";
                                }
                                if($dat->brand_id !== null){
                                    $st = $dat->cs_brand->name;
                                }else{
                                    $st = "  ";
                                }
                                if($dat->category_id !== null){
                                    $vl = $dat->categorys->name;
                                }else{
                                    $vl = "";
                                }
                                if($dat->commune !== null){
                                    $cm = $dat->commune;
                                }else{
                                    $cm = "";
                                }
                                if($dat->cost_price !== null){
                                    $dis = $dat->cost_price;
                                }else{
                                    $dis = "";
                                }
                                if($dat->sell_price !== null){
                                    $pro = $dat->sell_price;
                                }else{
                                    $pro = "";
                                }
                            //================================================
                            $sheet->rows(array(
                                 array($key1+1,$hn, $gn, $st, $vl, $cm, $dis, $pro )
                             ));

                            //=============================================
                        }
                        //Background Color
                        $sheet->row(2, function ($row) {
                              $row->setBackground('#ffc1c1');
                           });

                        //Align Center
                        $sheet->cells('A1:H'.$row_total, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            });
                    });
                })->export('xlsx');
        }
        $codes = Cs_Items::orderBy('item_bacode','asc')->where('deleted',0)->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->item_bacode]);
        }
        return view('credit_sale.report.item.report_item', compact('title','code'));
    }
    
    public function get_data_item(Request $request){
        if($request->has('submit_search')){
            $data_search = $this->search_form($request);
             $data_query = Cs_Items::with(['cs_item_tax','cs_suppliers','cs_brand','inventorys','categorys'])
                     ->where('deleted',0)
                     ->whereBetween('created_at', [$data_search['from_date'],$data_search['to_date']])
                     ->where('branch_id','LIKE',$data_search['brand_name'])
                     ->where('id','LIKE',$data_search['client_name'])
                     ->where('user_id','LIKE',$data_search['staff_name']);

         }else{
             $data_query = Cs_Items::with(['cs_item_tax','cs_suppliers','cs_brand','inventorys','categorys'])->where('deleted',0);
             $this->clear_session_search();
         }

         return  $data_query; 
    }
    public function report_item_list_json(Request $request)
    {
        $data_query = $this->get_data_item($request);
        $data = $data_query->paginate(15); 
        $data_all =  $data_query->get();  
        return response()->json(['data'=>$data]);
    }
    
}
