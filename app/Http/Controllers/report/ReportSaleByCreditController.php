<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\lib\Session_Search;
use App\Cs_Sale;
use DB;
use Auth;
use Session;
use DateTime;
use Excel;
class ReportSaleByCreditController extends Controller
{
    //
    public function report_sale_by_credit(Request $request)
    {
        $title = "របាយការណ៍លម្អិតពីការលក់ដោយឥណទាន ";
        if($request->has('reset')){
            Session_Search::clear_session_search();
            return redirect()->to('report/sales/report_sale_by_credit');
        }
        if($request->has('export')){
        
            $data_query = $this->get_data_sales_by_credit($request); 
            $data_all =  $data_query->get();  
            $day_export = date('d-m-Y');
            Excel::create(' របាយការណ៍លម្អិតពីការលក់ដោយឥណទាន ('.$day_export.')',function($excel) use ($data_all){
                    $excel->sheet(' លម្អិតពីការលក់ដោយឥណទាន ',function($sheet) use ($data_all){

                        // Font family
                    $sheet->setFontFamily('Battambang');
                    // Sets all borders0815
                    $sheet->setAllBorders('thin');

                        $sheet->rows(array(
                            array('','','', '','', ' របាយការណ៍លម្អិតពីការលក់ដោយឥណទាន ','','','','',''),
                             array('ល.រ','លេខកូដ','ឈ្មោះអតិថិជន','ចំនួនទឹកប្រាក់', 'ប្រាក់បង់តំបូង៣០%', '	ប្រាក់ដែលនៅជំពាក់',
                                'បរិមាណទំនិញ','រយៈពេលសង','កាលបរិច្ឆេទបង្កើត','បុគ្គលិកប្រមូលប្រាក់','រូបិយប័ណ្ណ','ស្ថានភាព')
                         ));
                         $row_total = count($data_all) + 5;
                        foreach($data_all as $key1 => $dat){  
                            $num_invoice = $dat->num_invoice;
                            $client_name = $dat->cs_client->name;
                            $prices_total_num = $dat->cs_request_form->prices_total_num;
                            $deposit_fixed = $dat->cs_request_form->deposit_fixed;
                            $money_owne = $dat->cs_request_form->money_owne;
                            $total_qty = $dat->cs_request_form->total_qty;
                            $duration_pay_money = $dat->cs_request_form->duration_pay_money;
                            if( $dat->cs_request_form->duration_pay_money_type == "month"){
                                $duration_pay_money_type = "ខែ"; 
                            }elseif( $dat->cs_request_form->duration_pay_money_type == "2week"){
                                $duration_pay_money_type = "២ សប្តាហ៍";
                            }elseif( $dat->cs_request_form->duration_pay_money_type == "week"){
                                $duration_pay_money_type = "សប្តាហ៍";
                            }elseif( $dat->cs_request_form->duration_pay_money_type == "day"){
                                $duration_pay_money_type = "ថ្ងៃ";
                            }
                            $dura = $duration_pay_money."  ".$duration_pay_money_type;
                            $staff_name = $dat->cs_staff->kh_name; 
                            if($dat->cs_approval_credit_sale){
                                   if($dat->cs_approval_credit_sale->is_agree == 1){
                                        $cs_app = "អនុម័ត";
                                   }elseif($dat->cs_approval_credit_sale->is_agree == 2){
                                        $cs_app = "មិនទាន់អនុម័ត";
                                   }elseif($dat->cs_approval_credit_sale->is_agree == 3){
                                        $cs_app = "មិនអនុម័ត";
                                   }     
                            }else{
                                $cs_app = "រង់ចាំការអនុម័ត";
                            }
                            
                            $currency = $dat->currency->name_kh;

                            $dat_create = date('d-m-Y',strtotime($dat->cs_request_form->date_create_request));
                            //================================================
                            $sheet->rows(array(
                                 array($key1+1, $num_invoice, $client_name,$prices_total_num , $deposit_fixed, $money_owne, $total_qty, $dura , $dat_create, $staff_name, $currency, $cs_app  )
                             ));

                            //=============================================
                        }
                        //Background Color
                        $sheet->row(2, function ($row) {
                              $row->setBackground('#ffc1c1');
                           });

                        //Align Center
                        $sheet->cells('A1:L'.$row_total, function($cells) {
                            $cells->setAlignment('center');
                            $cells->setValignment('center');
                            });
                    });
                })->export('xlsx');
        }
        $codes = Cs_Sale::orderBy('num_invoice', 'asc')->where('deleted','=',1)->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->num_invoice]);
        }
        return view('credit_sale.report.credit_payment.report_sale_by_credit', compact('title', 'code'));
    }
    
    public function get_data_sales_by_credit(Request $request){
        if($request->has('submit_search')){
            $data_search = Session_Search::search_form($request);
             $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
                     ->where('deleted','=',1)
                     ->where('method','=','sale_by_credit')
                     ->whereBetween('due_date', [$data_search['from_date'],$data_search['to_date']])
                     ->where('branch_id','LIKE',$data_search['brand_name'])
                     ->where('currency_id','LIKE',$data_search['currency'])
                     ->where('client_id','LIKE',$data_search['client_name'])
                     ->where('num_invoice','LIKE',$data_search['sale_id'])
                     ->where('user_id','LIKE',$data_search['staff_name']);

         }else{
             $data_query =  Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
             ->where('deleted','=',1)
             ->where('method','=','sale_by_credit');
            
         }

         return  $data_query; 
    }
    public function report_sale_by_credit_json(Request $request)
    {
        $data_query = $this->get_data_sales_by_credit($request);
        $data = $data_query->paginate(15); 
        $data_all =  $data_query->get();  
        return response()->json(['data'=>$data]);
    }


}
