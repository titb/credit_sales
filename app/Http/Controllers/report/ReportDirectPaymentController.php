<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\lib\Session_Search;
use App\Cs_Sale;
use DB;
use Auth;
use Session;
use DateTime;
use Excel;
class ReportDirectPaymentController extends Controller
{
     //
     public function report_direct_payment(Request $request)
     {
         $title = "របាយការណ៍លម្អិតពីការលក់ដោយរការទូទាត់ដោយផ្ទាល់ ";
         if($request->has('reset')){
             Session_Search::clear_session_search();
             return redirect()->to('report/sales/report_direct_payment');
         }
         if($request->has('export')){
         
             $data_query = $this->get_data_direct_payment($request); 
             $data_all =  $data_query->get();  
             $day_export = date('d-m-Y');
             Excel::create(' របាយការណ៍លម្អិតពីការលក់ដោយរការទូទាត់ដោយផ្ទាល់ ('.$day_export.')',function($excel) use ($data_all){
                     $excel->sheet(' ការលក់ដោយរការទូទាត់ដោយផ្ទាល់ ',function($sheet) use ($data_all){
 
                         // Font family
                     $sheet->setFontFamily('Battambang');
                     // Sets all borders0815
                     $sheet->setAllBorders('thin');
 
                         $sheet->rows(array(
                             array('','','', '','', ' របាយការណ៍លម្អិតអំពីការលក់ដោយរការទូទាត់ដោយផ្ទាល់ ','','','','','',''),
                              array('ល.រ','លេខកូដ','ឈ្មោះអតិថិជន','បរិមាណទំនិញ', 'ការទូទាត់សរុបទាំងអស់', 'បញ្ចុះតម្លៃ','ការទូទាត់សរុប',
                                 'តម្លៃនៃទំនិញដែលបានលក់','ប្រាក់ចំណេញ','ប្រភេទបង់ប្រាក់','មូលហេតុបញ្ចុះតម្លៃ','បុគ្គលិក','កាលបរិច្ឆេទបង្កើត')
                          ));
                          $row_total = count($data_all) + 5;
                         foreach($data_all as $key1 => $dat){  
                             $num_invoice = $dat->num_invoice;
                             if($dat->cs_client){
                                $client_name = $dat->cs_client->kh_username;
                             }else{
                                $client_name = "None";
                             }
                             
                             $currency = $dat->currency->name_kh;
                             $sub_total_payment = $dat->sale_payment->sub_payment_amount;
                             $total_payment = $dat->sale_payment->total_payment; 
                             $dute_payment_amount = $dat->sale_payment->dute_payment_amount; 
                             $discount_payment = $dat->sale_payment->discount_payment; 
                             $discount_reason = $dat->sale_payment->discount_reason;   
                             $total_qty = $dat->sale_payment->total_qty; 
                             $payment_shipping = $dat->sale_payment->payment_shipping;  
                             $pay_type = '';
                             foreach($dat->sale_transaction_pay as $key => $p){ 
                                 if($key == 0){
                                        $sy = "";
                                 }else{
                                    $sy = " || ";
                                 }
                                $pay_type .= $sy." ". $p->payment_pay." : ".$p->account_mount ."  ".$p->currency->name_kh; 
                             }
                             $profit = 0;
                             $dat_create = date('d-m-Y',strtotime($dat->sale_payment->payment_date));
                             $staff_name = $dat->cs_staff->kh_name;
                             //================================================
                             $sheet->rows(array(
                                  array($key1+1, $num_invoice, $client_name, $total_qty, $sub_total_payment, $discount_payment, $total_payment, $dute_payment_amount, $profit, $pay_type,$discount_reason, $staff_name, $dat_create)
                              ));
 
                             //=============================================
                         }
                         //Background Color
                         $sheet->row(2, function ($row) {
                               $row->setBackground('#ffc1c1');
                            });
 
                         //Align Center
                         $sheet->cells('A1:L'.$row_total, function($cells) {
                             $cells->setAlignment('center');
                             $cells->setValignment('center');
                             });
                     });
                 })->export('xlsx');
         }
        $codes = Cs_Sale::orderBy('num_invoice','asc')
                        ->where('deleted','=',1)
                        ->where('method','=','sale_by_cash')
                        ->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->num_invoice]);
        }
         return view('credit_sale.report.direct_payment.report_direct_payment_detail', compact('title', 'code'));
     }
     
     public function get_data_direct_payment(Request $request){
         if($request->has('submit_search')){
             $data_search = Session_Search::search_form($request);
              $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay.currency','sale_item','cs_request_form','cs_client','branch','user','cs_staff','currency'])
                      ->where('deleted','=',1)
                      ->where('method','=','sale_by_cash')
                      ->whereBetween('due_date', [$data_search['from_date'],$data_search['to_date']])
                      ->where('branch_id','LIKE',$data_search['brand_name'])
                      ->where('currency_id','LIKE',$data_search['currency'])
                      ->where('client_id','LIKE',$data_search['client_name'])
                      ->where('num_invoice','LIKE',$data_search['sale_id'])
                      ->where('user_id','LIKE',$data_search['staff_name']);
 
          }else{
              $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay.currency','sale_item','cs_request_form','cs_client','branch','user','cs_staff','currency'])
              ->where('deleted','=',1)
              ->where('method','=','sale_by_cash');
             
          }
 
          return  $data_query; 
     }
     public function report_direct_payment_json(Request $request)
     {
         $data_query = $this->get_data_direct_payment($request);
         $data = $data_query->paginate(15); 
         $data_all =  $data_query->get();  
         return response()->json(['data'=>$data]);
     }

}
