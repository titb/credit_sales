<?php

namespace App\Http\Controllers\report;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Cs_Items;
use App\Cs_Client;
use App\Cs_Sale;
use App\Cs_Schedule_Timesheet_Pay;
use App\Cs_Schedule;
use App\Cs_Purchase_Order;

class SearchController extends Controller
{
    public function searchItem(Request $request){
        $term = ['date'=>'created_at', 'branch'=>'branch_id', 'code'=>'item_bacode', 'staff_name'=>'user_id','currency'=>'created_at','client'=>'created_at'];
        $data_query = Cs_Items::with(['cs_item_tax','cs_suppliers','cs_brand','inventorys','categorys'])->where('deleted',0);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchClient(Request $request){
        $term = ['date'=>'created_at', 'branch'=>'branch_id', 'code'=>'created_at', 'staff_name'=>'user_id','currency'=>'created_at','client'=>'id'];
        $data_query = Cs_Client::with(['cs_sale.sale_item'])->where('deleted',0);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchSaleCredit(Request $request){
        $term = ['date'=>'due_date', 'branch'=>'branch_id', 'code'=>'num_invoice', 'staff_name'=>'user_id','currency'=>'currency_id','client'=>'client_id'];
        $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
                     ->where('deleted','=',1);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchDirectPay(Request $request){
        $term = ['date'=>'due_date', 'branch'=>'branch_id', 'code'=>'num_invoice', 'staff_name'=>'user_id','currency'=>'currency_id','client'=>'client_id'];
        $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay.currency','sale_item','cs_request_form','cs_client','branch','user','cs_staff','currency'])
                      ->where('deleted','=',1)
                      ->where('method','=','sale_by_cash');
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchPayBack(Request $request){
        $term = ['date'=>'available_date_payment', 'branch'=>'branch_id', 'code'=>'cs_schedules_id', 'staff_name'=>'staff_id','currency'=>'available_date_payment','client'=>'available_date_payment'];
        $data_query = Cs_Schedule_Timesheet_Pay::with(['cs_schedule.cs_client','cs_schedule.currency','branch','user','cs_staff'])
                         ->where('deleted','=',1);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchSchedule(Request $request){
        $term = ['date'=>'created_at', 'branch'=>'branch_id', 'code'=>'id', 'staff_name'=>'staff_id','currency'=>'currency_id','client'=>'client_id'];
        $data_query = Cs_Schedule::with(['branch','cs_client','user','cs_staff','cs_approvalcredit','cs_schedule_timesheet','cs_schedule_timesheet_pay','currency'])
                        ->where('deleted','=',1);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchSale(Request $request){
        $term = ['date'=>'due_date', 'branch'=>'branch_id', 'code'=>'num_invoice', 'staff_name'=>'user_id','currency'=>'currency_id','client'=>'client_id'];
        $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
                        ->where('deleted','=',1)
                        ->where('method','=','sale_by_cash');
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);
    }
    public function searchSalebyCredit(Request $request){
        $term = ['date'=>'due_date', 'branch'=>'branch_id', 'code'=>'num_invoice', 'staff_name'=>'user_id','currency'=>'currency_id','client'=>'client_id'];
        $data_query = Cs_Sale::with(['sale_payment','sale_transaction_pay','sale_item','cs_request_form','cs_approval_credit_sale','cs_client','branch','user','cs_staff','currency'])
                        ->where('deleted','=',1)
                        ->where('method','=','sale_by_credit');
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);;
    }
    public function searchReceiving(Request $request){
        $term = ['date'=>'receiving_date', 'branch'=>'branch_id', 'code'=>'invoice_num', 'staff_name'=>'stuff_receiving_id','currency'=>'currency','client'=>'receiving_date'];
        $data_query = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers','staff_receive','users','staff_order'])
            ->where('is_receiving','=',1)
            ->where('supspend','=',1)
            ->where('deleted','=',1);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);;
    }
    public function searchReceivingPO(Request $request){
        $term = ['date'=>'order_date', 'branch'=>'branch_id', 'code'=>'invoice_num', 'staff_name'=>'stuff_receiving_id','currency'=>'currency','client'=>'receiving_date'];
        $data_query = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers','staff_receive','users','staff_order'])
            ->where('is_receiving','=',0)
            ->where('supspend','=',1)
            ->where('deleted','=',1);
        $data_all = $this->searchData($request, $data_query, $term);                        
        return response()->json($data_all);;
    }

    public function searchData(Request $request, $data_query, $term){
        if($request->time != 'null'){
            $data_query = $this->searchTimer($request, $data_query, $term, $request->time);
        }
        if($request->branch_name != 'null'){
            $data_query = $data_query->where($term['branch'],'=',$request->branch_name);
        }
        if($request->currency != 'null'){
            $data_query = $data_query->where($term['currency'],'=',$request->currency);
        }
        if($request->sale_id != 'null'){
            $data_query = $data_query->where($term['code'],'=',$request->sale_id);
        }
        if($request->client_name != 'null'){
            $data_query = $data_query->where($term['client'],'=',$request->client_name);
        }
        if($request->staff_name != 'null'){
            $data_query = $data_query->where($term['staff_name'],'=',$request->staff_name);
        }

        $data_query = $data_query->paginate(10);
        return $data_query;
    }
    public function searchTimer(Request $request, $data_query, $term, $timer){
    
            if($timer == 'today'){
                $time = '%'.date('Y-m-d').'%';
                $data_query = $data_query->where($term['date'], 'LIKE', $time);
            }
            if($timer == 'yesterday'){
                $time = '%'.date('Y-m-d', strtotime( '-1 days' )).'%';
                $data_query = $data_query->where($term['date'], 'LIKE', $time);
            }
            if($timer == 'l7day'){
                $start = date('Y-m-d', strtotime( '-7 days' ));
                $end = date('Y-m-d');
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'l30day'){
                $start = date('Y-m-d', strtotime( '-30 days' ));
                $end = date('Y-m-d');
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'week'){
                $start = date('Y-m-d', strtotime( 'This Week' ));
                $end = date('Y-m-d', strtotime( 'This Sunday + 1 days' ));
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'lweek'){
                $start = date('Y-m-d', strtotime( 'Last Week' ));
                $end = date('Y-m-d', strtotime( 'Last Sunday + 1 days' ));
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'month'){
                $start = date('Y-m-01');
                $end = date('Y-m-01', strtotime( '+ 1 months' ));
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'lmonth'){
                $start = date('Y-m-01', strtotime( 'Last Month' ));
                $end = date('Y-m-01');
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'quarter'){
                $cur_q = ceil(date('m')/4) * 3 + 1;
                $cur_q = strlen($cur_q)==1?'0'.$cur_q:$cur_q;
                $start = date('Y-'.$cur_q.'-01');
                $end = date('Y-'.($cur_q+3).'-01');
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'lquarter'){
                $cur_q = (ceil(date('m')/4) - 1) * 3 + 1;
                $cur_q = strlen($cur_q)==1?'0'.$cur_q:$cur_q;
                $start = date('Y-'.$cur_q.'-01');
                $end = date('Y-'.($cur_q+3).'-01');
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'year'){
                $start = date('Y-01-01');
                $end = date('Y-01-01', strtotime( '+ 1 years' ));
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'lyear'){
                $start = date('Y-01-01', strtotime( '- 1 years' ));
                $end = date('Y-01-01');
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
            if($timer == 'all'){
                $data_query = $data_query->where('created_at', 'LIKE', '%');
            }
            if($timer == 'custom'){
                $from_date =  date('Y-m-d',strtotime($request->from));
                $to_dates = date('Y-m-d', strtotime($request->to_date));
                $to_dates = date('d', strtotime($to_dates)) + 1;
                $to_date =  date('Y-m-'.$to_dates);
                $start = $from_date;
                $end = $to_date;
                $data_query = $data_query->whereBetween('created_at', [$start, $end]);
            }
        return $data_query;
    }
}
