<?php

namespace App\Http\Controllers\report;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Cs_Schedule_Timesheet_Pay;
use App\Cs_Schedule;
use App\lib\Session_Search;
use App\Cs_Sale;
use DB;
use Auth;
use Session;
use DateTime;
use Excel; 
class ReportRepayBackController extends Controller
{
    protected $sch_time;
    protected $schedule;
    // protected $total_default = [
    //     'subtotal'            => 0,
	// 	'subtotal_tax'        => 0,
	// 	'shipping_total'      => 0,
	// 	'shipping_tax'        => 0,
	// 	'shipping_taxes'      => array(),
	// 	'discount_total'      => 0,
	// 	'discount_tax'        => 0,
	// 	'cart_contents_total' => 0,
	// 	'cart_contents_tax'   => 0,
	// 	'cart_contents_taxes' => array(),
	// 	'fee_total'           => 0,
	// 	'fee_tax'             => 0,
	// 	'fee_taxes'           => array(),
	// 	'total'               => 0,
	// 	'total_tax'           => 0,
    // ];




    public function __construct()
    {
        $this->sch_time = new Cs_Schedule_Timesheet_Pay;
        $this->schedule = new Cs_Schedule;
        $this->middleware('auth');
    }
        //
        public function ShowData(Request $request)
        {
            $title = "របាយការណ៍លម្អិតពីការសងប្រាក់ត្រលប់";
            if($request->has('reset')){
                Session_Search::clear_session_search();
                return redirect()->to('report/payment_back/report_payment_back');
            }
            if($request->has('export')){
            
                $data_query = $this->getData($request); 
                $data_all =  $data_query->get();  
                $day_export = date('d-m-Y h:m:s');
                Excel::create(' របាយការណ៍លម្អិតពីការសងប្រាក់ត្រលប់ ('.$day_export.')',function($excel) use ($data_all){
                        $excel->sheet(' ការសងប្រាក់ត្រលប់ ',function($sheet) use ($data_all){
    
                            // Font family
                        $sheet->setFontFamily('Battambang');
                        // Sets all borders0815
                        $sheet->setAllBorders('thin');
    
                            $sheet->rows(array(
                                array('','','', '','', ' របាយការណ៍លម្អិតពីការសងប្រាក់ត្រលប់ ','','','',''),
                                 array('ល.រ','លេខកូដ','ឈ្មោះអតិថិជន','ប្រាក់ដើមបានសង់សរុប', 'ប្រាក់ការបានសង់សរុប', '	ប្រាក់ពិន័យសរុប',
                                    'ប្រាក់ផ្សេងៗ','ចំនួនទឹកសង់ប្រាក់សរុប','កាលបរិច្ឆេទបង់ប្រាក់','បុគ្គលិកប្រមូលប្រាក់','រូបិយប័ណ្ណ')
                             ));
                             $row_total = count($data_all) + 5;
                            foreach($data_all as $key1 => $dat){  
                                $num_invoice = $dat->cs_schedule->schedule_number;
                                $client_name = $dat->cs_client->kh_username;
                                $available_total_pay_cost = $dat->available_total_pay_cost;
                                $available_total_pay_interest = $dat->available_total_pay_interest;
                                $panalty = $dat->panalty;
                                $other_payment = $dat->other_payment;
                                $available_total_payment = $dat->available_total_payment;

                                $available_date_payment = date('d-m-Y',strtotime($dat->available_date_payment));
                                $staff_name = $dat->cs_staff->name_kh;
                                $currency = $dat->currency->name_kh;
    
                                //================================================
                                $sheet->rows(array(
                                     array($key1+1, $num_invoice, $client_name,$available_total_pay_cost , $available_total_pay_interest, $panalty, $other_payment, $available_total_payment , $available_date_payment, $staff_name, $currency )
                                 ));
    
                                //========================== ===================
                            }
                            //Background Color
                            $sheet->row(2, function ($row) {
                                  $row->setBackground('#ffc1c1');
                               });
    
                            //Align Center
                            $sheet->cells('A1:L'.$row_total, function($cells) {
                                $cells->setAlignment('center');
                                $cells->setValignment('center');
                                });
                        });
                    })->export('xlsx');
            }
            $codes = Cs_Schedule_Timesheet_Pay::orderBy('cs_schedules_id','asc')->where('deleted','=',1)->get();
            $code = [];
            foreach($codes as $val){
                array_push($code, ['id'=>$val->id, 'code'=>$val->cs_schedules_id]);
            }
            dd($code);
            return view('credit_sale.report.payment_back.report_payment_back', compact('title', 'code'));
        }
        
        public function getData(Request $request){
            if($request->has('submit_search')){
                $data_search = Session_Search::search_form($request);
                 $data_query = Cs_Schedule_Timesheet_Pay::with(['cs_schedule.cs_client','cs_schedule.currency','branch','user','cs_staff'])
                         ->where('deleted','=',1)
                         ->whereBetween('available_date_payment', [$data_search['from_date'],$data_search['to_date']])
                         ->where('branch_id','LIKE',$data_search['brand_name'])
                         ->where('client_id','LIKE',$data_search['client_name'])
                         ->where('cs_schedules_id','LIKE',$data_search['sale_id'])
                         ->where('currency_id','LIKE',$data_search['currency'])
                         ->where('staff_id','LIKE',$data_search['staff_name']);
    
             }else{
                 $data_query =  Cs_Schedule_Timesheet_Pay::with(['cs_schedule.cs_client','cs_schedule.currency','branch','user','cs_staff'])
                 ->where('deleted','=',1);
             }
    
             return  $data_query; 
        }

        public function getTotalRow(){
            
        }

        public function getDataJson(Request $request)
        {
            $data_query = $this->getData($request);
            $data = $data_query->paginate(15); 
            $data_all =  $data_query->get();  
            return response()->json(['data'=>$data]);
        }



    
}
