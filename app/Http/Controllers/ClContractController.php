<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Cs_Client;
use App\Cs_ApprovalCredit;
use App\Cs_Cds_Request_Form;

class ClContractController extends Controller
{
    public function show_contract(Request $request, $id){
        $title = "ការបង្ហាញការអនុម័តឥណទាន";
        $data_id = $id;
        $client_id = $id;
        $cli_data = Cs_Client::where('id',$id)->first();
        $cl_data = Cs_ApprovalCredit::with(['approval_item.item','cs_sales.branch','approval_item.item.categorys','cs_sales.cs_client','schedule'])
                    ->where('client_id','=',$client_id)
                    ->where('is_agree','=',1)
                    ->where('is_finish','=',0)
                    ->first();
            if(count($cli_data->sub_clients) > 0){
                // dd($cl_data);
                return view('credit_sale.client_management.form_credit_sale',compact('title','data_id','client_id','cl_data'));
            }else{
                $title = "Create Sub Client";
                return Redirect('accounts/sub_client/create_sub_client'.'?client_id='.$client_id);
            }
    }
    public function list_request_form(Request $request, $client_id){
        $title = "ការបង្ហាញការអនុម័តឥណទាន";
        return view("credit_sale.client_management.list_request_form", compact('title','client_id'));
    }
    public function list_request_form_json(Request $request, $client_id){
        $request_data = Cs_Cds_Request_Form::where('client_id',$client_id)->get();
        return response()->json($request_data);
    }
    public function show_request_form(Request $request, $client_id, $id){
        $title = "ពត័មានទម្រង់ស្នើរសុំ";
        $data_id = $id;
        $request_data = Cs_Cds_Request_Form::where('id',$id)->first();
        // dd($cl_data);
        return view('credit_sale.client_management.show_request_form',compact('data_id','client_id','request_data','title'));
    }
    public function show_request_form_json($client_id, $id){
        $request_data = Cs_Cds_Request_Form::with(['cs_clients', 'cs_sales', 'cs_sales.sale_item', 'cs_sales.sale_item.item', 'cs_sales.sale_item.item.cs_brand'])
                                            ->where('id',$id)
                                            ->first();
        return response()->json($request_data);
    }
}
