<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Account;

class Account_typeController extends Controller
{
   public function get_list(){
        $data = Account::all();
        return view('credit_sale.account_type.index', compact('data'));
    }
    public function get_create(){

        $title = "Create Account Management";
    
        return view('credit_sale.account_type.create')->with('title',$title);

    }
    public function post_create(Request $request)
    {
    	$data = [
            'name'=>$request->name,
    		'description'=>$request->description
    	];
    	Account::insert($data);
    	return Redirect()->to('account_type');
    }
      public function get_edit($id)
    {
    	$data = Account::find($id);
    	return view('credit_sale.account_type.edit', compact('data'));
    }
      public function post_edit(Request $request,$id)
    {
    	$data = [
            'name'=>$request->name,
    		'description'=>$request->description
    	];

    	Account::where('id','=',$id)->update($data);
    	return Redirect()->to('account_type');
    }
      

   public function post_delete(Request $edit, $id)
    {
        $data = Account::find($id);
        $data->delete();
        return redirect()->to('account_type');
    }
}
