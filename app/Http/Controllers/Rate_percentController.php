<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Rate_percent;

class Rate_percentController extends Controller
{
     public function get_list(){
    	$data = Rate_percent::all();
    	return view('credit_sale.rate_percent.index', compact('data'));
    }
    public function get_create(){

        $title = "Create Rate_percent";
    
        return view('credit_sale.rate_percent.create')->with('title',$title);

    }
    public function post_create(Request $request)
    {
    	$data = [
            'meta_key'=>$request->meta_key,
    		'value_option'=>$request->value_option
    	];
    	Rate_percent::insert($data);
    	return Redirect()->to('rate_percent');
    }
      public function get_edit($id)
    {
    	$data = Rate_percent::find($id);
    	return view('credit_sale.rate_percent.edit', compact('data'));
    }
      public function post_edit(Request $request,$id)
    {
    	$data = [
            'meta_key'=>$request->meta_key,
    		'value_option'=>$request->value_option
    	];

    	Rate_percent::where('id','=',$id)->update($data);
    	return Redirect()->to('rate_percent');
    }
      

   public function post_delete(Request $edit, $id)
    {
        $data = Rate_percent::find($id);
        $data->delete();
        return redirect()->to('rate_percent');
    }
}
