<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use App\Cs_Purchase_Order; 
use App\Cs_Purchase_Order_Item;
use App\Cs_Purchase_Order_Payment;
use Auth;
use Session;
use DB;
use Excel;

class ReportReceivingController extends Controller
{
   
    //
    public function __construct()
    {
        $this->middleware('auth'); 
    }
    
    public function report_receiving_product(Request $request){
        $title = "Report Receiving Product";
        $codes = Cs_Purchase_Order::orderBy('invoice_num','asc')
                                    ->where('is_receiving','=',1)
                                    ->where('supspend','=',1)
                                    ->where('deleted','=',1)
                                    ->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->invoice_num]);
        }
        return view('credit_sale.report.report_receiving_product',compact('title','code'));
    }

    public function report_receiving_product_json(Request $request){

        if($request->has('submit_search')){
            // Search Start date To date
            if($request->from_date != "" && $request->to_date != ""){
                // from date
                $from_date = date('Y-m-d',strtotime($request->from_date));
                Session::flash('from_date',$request->from_date);
                // To date
                $to_date = date('Y-m-d',strtotime($request->to_date));
                Session::flash('to_date',$request->to_date);
            }else{
                // from date
                $from_date = '1970-01-01';
                Session::forget('from_date');
                // To date
                $to_date = date('Y-m-d');
                Session::forget('to_date');
            }
            // Search Brand
            if($request->brand_name != ""){
                $brand_name = $request->brand_name;
                Session::flash('brand_name',$request->brand_name);
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            // Search Currency
            if($request->currency != ""){
                $currency = $request->currency;
                Session::flash('currency',$request->currency);
            }else{
                $currency = '%_%';
                Session::forget('currency');
            }
            // Search Supplier Name
            if($request->supplier_name != ""){
                $supplier_name = $request->supplier_name;
                Session::flash('supplier_name',$request->supplier_name);
            }else{
                $supplier_name = '%_%';
                Session::forget('supplier_name');
            }
            // Search Purches Order ID
            if($request->purches_id != ""){
                $purches_id = $request->purches_id;
                Session::flash('purches_id',$request->purches_id);
            }else{
                $purches_id = '%_%';
                Session::forget('purches_id');
            }
            // Search Staff Name
            if($request->staff_name != ""){
                $staff_name = $request->staff_name;
                Session::flash('staff_name',$request->staff_name);
            }else{
                $staff_name = '%_%';
                Session::forget('staff_name');
            }

            $data = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers','staff_receive','users','staff_order'])
            ->where('is_receiving','=',1)
            ->where('supspend','=',1)
            ->where('deleted','=',1)
            ->whereBetween('receiving_date', [$from_date,$to_date])
            ->where('branch_id','LIKE',$brand_name)
            ->where('currency','LIKE',$currency)
            ->where('supplier_id','LIKE',$supplier_name)
            ->where('invoice_num','LIKE',$purches_id)
            ->where('stuff_receiving_id','LIKE',$staff_name)
            ->paginate(15);
        
        }else{
            $data = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers','staff_receive','users','staff_order'])
            ->where('is_receiving','=',1)
            ->where('supspend','=',1)
            ->where('deleted','=',1)
            ->paginate(15);
        }
        return response()->json(['data'=>$data]);
    }

    public function export_report_receive_to_excel(Request $request){
        
        Excel::create('Report Receiving', function($excel) {

            $excel->sheet('Report Receiving', function($sheet) {
                $data = Cs_Purchase_Order::get();
                $sheet->row(1, array(
                    'Report Receiving Product',
                ));
                $sheet->row(2, array(
                    '#', 'លេខកូដ','ឈ្មោះអ្នកផ្គត់ផ្គង់','ចំនួនផលិតផលPO','ចំនួនផលិតផលRE','ភាគរយបញ្ចុះតម្លៃ','ប្រាក់កក់','ប្រាក់ដែលនៅជំពាក់','ចំនួនប្រាក់សរុប','ថ្ងៃទទួលផលិតផល','បុគ្គលិកទទួល'
                ));

                $i = 3;

                // $data_count = count($data);
                foreach($data as $key=>$da){
                    
                    $sheet->row($key+$i, array(
                        $key+1, $da->invoice_num, $da->suppliers->name, $da->qty_order, $da->qty_receiving, $da->discount_percent, $da->deposit_price, $da->remain_price, $da->dute_total_price, $da->receiving_date, $da->users->name_kh
                    ));
                    $sheet->row($key+$i, function($row){
                        $row->setFontFamily('Battambang');
                    });
                    
                }

                // Style
                $sheet->setBorder('A1:K10', 'thin');
                $sheet->mergeCells('A1:K1');
                $sheet->setHeight(1,70);
                $sheet->setHeight(2,40);

                
                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Battambang',
                        'size'      =>  10,
                    )
                ));

                $sheet->row(1, function($row){
                    $row->setBackground('#d74a4a');
                    $row->setFontFamily('Battambang');
                
                });
                $sheet->row(2, function($row){
                    $row->setBackground('#fab8b8');
                    $row->setFontFamily('Battambang');
                
                });
                
                $sheet->cell('A1:K1', function($cell){
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    $cell->setFont(array(
                        'family' => 'Battambang',
                        'size' => '12',
                        'bold' => true
                    ));
                });

                $sheet->cell('A2:K2', function($cell){
                    $cell->setFont(array(
                        'size' => '12',
                        'bold' => true
                    ));
                });

                $sheet->cell('A1:K1', function($cells){
                    $cells->setFont(array(
                        'size' => '15',
                    ));
                });

            });
        })->export('xlsx');
    }

    public function report_po_product(Request $request){
        $title = "Report Receiving Product";
        $codes = Cs_Purchase_Order::orderBy('invoice_num','asc')
                                    ->where('is_receiving','=',0)
                                    ->where('supspend','=',1)
                                    ->where('deleted','=',1)
                                    ->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->invoice_num]);
        }
        return view('credit_sale.report.report_purchase_order',compact('title','code'));
    }

    public function report_po_product_json(Request $request){

        if($request->has('submit_search')){
            // Search Start date To date
            if($request->from_date != "" && $request->to_date != ""){
                // from date
                $from_date = date('Y-m-d',strtotime($request->from_date));
                Session::flash('from_date',$request->from_date);
                // To date
                $to_date = date('Y-m-d',strtotime($request->to_date));
                Session::flash('to_date',$request->to_date);
            }else{
                // from date
                $from_date = '1970-01-01';
                Session::forget('from_date');
                // To date
                $to_date = date('Y-m-d');
                Session::forget('to_date');
            }
            // Search Brand
            if($request->brand_name != ""){
                $brand_name = $request->brand_name;
                Session::flash('brand_name',$request->brand_name);
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            // Search Currency
            if($request->currency != ""){
                $currency = $request->currency;
                Session::flash('currency',$request->currency);
            }else{
                $currency = '%_%';
                Session::forget('currency');
            }
            // Search Supplier Name
            if($request->supplier_name != ""){
                $supplier_name = $request->supplier_name;
                Session::flash('supplier_name',$request->supplier_name);
            }else{
                $supplier_name = '%_%';
                Session::forget('supplier_name');
            }
            // Search Purches Order ID
            if($request->purches_id != ""){
                $purches_id = $request->purches_id;
                Session::flash('purches_id',$request->purches_id);
            }else{
                $purches_id = '%_%';
                Session::forget('purches_id');
            }
            // Search Staff Name
            if($request->staff_name != ""){
                $staff_name = $request->staff_name;
                Session::flash('staff_name',$request->staff_name);
            }else{
                $staff_name = '%_%';
                Session::forget('staff_name');
            }

            $data = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers','staff_receive','users','staff_order'])
            ->where('is_receiving','=',0)
            ->where('supspend','=',1)
            ->where('deleted','=',1)
            ->whereBetween('order_date', [$from_date,$to_date])
            ->where('branch_id','LIKE',$brand_name)
            ->where('currency','LIKE',$currency)
            ->where('supplier_id','LIKE',$supplier_name)
            ->where('invoice_num','LIKE',$purches_id)
            ->where('stuff_order_id','LIKE',$staff_name)
            ->paginate(15);
        
        }else{
            $data = Cs_Purchase_Order::with(['cs_purchase_order_payment','cs_purchase_order_item.item','suppliers','staff_receive','users','staff_order'])
            ->where('is_receiving','=',0)
            ->where('supspend','=',1)
            ->where('deleted','=',1)
            ->paginate(15);
        }

        return response()->json(['data'=>$data]);
    }

    public function export_report_purches_to_excel(Request $request){
        
        Excel::create('Report Purches', function($excel) {

            $excel->sheet('Report Purches', function($sheet) {
                $data = Cs_Purchase_Order::get();
                $sheet->row(1, array(
                    'Report Purchase Order Product',
                ));
                $sheet->row(2, array(
                    '#', 'លេខកូដ','ឈ្មោះអ្នកផ្គត់ផ្គង់','ចំនួនផលិតផលPO','ប្រាក់កក់','ប្រាក់ដែលនៅជំពាក់','ចំនួនប្រាក់សរុប','ថ្ងៃបញ្ជាទិញ','បុគ្គលិកទទួល'
                ));

                $i = 3;

                // $data_count = count($data);
                foreach($data as $key=>$da){
                    
                    $sheet->row($key+$i, array(
                        $key+1, $da->invoice_num, $da->suppliers->name, $da->qty_order, $da->deposit_price, $da->remain_price, $da->dute_total_price, $da->order_date, $da->users->name_kh
                    ));
                    $sheet->row($key+$i, function($row){
                        $row->setFontFamily('Battambang');
                    });
                    
                }

                // Style
                $sheet->setBorder('A1:I10', 'thin');
                $sheet->mergeCells('A1:I1');
                $sheet->setHeight(1,70);
                $sheet->setHeight(2,40);

                
                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Battambang',
                        'size'      =>  10,
                    )
                ));

                $sheet->row(1, function($row){
                    $row->setBackground('#d74a4a');
                    $row->setFontFamily('Battambang');
                
                });
                $sheet->row(2, function($row){
                    $row->setBackground('#fab8b8');
                    $row->setFontFamily('Battambang');
                
                });
                
                $sheet->cell('A1:I1', function($cell){
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    $cell->setFont(array(
                        'family' => 'Battambang',
                        'size' => '12',
                        'bold' => true
                    ));
                });

                $sheet->cell('A2:I2', function($cell){
                    $cell->setFont(array(
                        'size' => '12',
                        'bold' => true
                    ));
                });

                $sheet->cell('A1:I1', function($cells){
                    $cells->setFont(array(
                        'size' => '15',
                    ));
                });

            });
        })->export('xlsx');
    }
}
 