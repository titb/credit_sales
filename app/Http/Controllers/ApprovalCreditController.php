<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use App\Cs_Sale;
use App\Cs_Sale_Item;
use App\Cs_Sale_Payment;
use App\Cs_Sale_Payment_Tran;
use App\Cs_Cds_Request_Form;
use App\Cs_ApprovalCredit;
use App\Cs_Approval_Item;
use App\Mfi_Interest_Loan;
use Auth;
use Session;
use DB;
use Cart;
class ApprovalCreditController extends Controller
{

    //
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function get_sale_by_credit(Request $request){
        $title = "ការអនុម័តឥណទាន";
        if($request->session()->has('tab_approve')){
            $tab_approve = $request->session()->get('tab_approve');   
        }else{
            $tab_approve = "waiting"; 
        }
        return view('credit_sale.approval_credit_sale.list_approval_credit_sale',compact('title','tab_approve'));
    }
    public function get_search_by_ajax_sale_by_credit(Request $request){
        $title = "ការអនុម័តឥណទាន";
        if($request->session()->has('tab_approve')){
            $tab_approve = $request->session()->get('tab_approve');   
        }else{
            $tab_approve = "waiting"; 
        }
        return view('credit_sale.approval_credit_sale.list_approval_credit_sale',compact('title','tab_approve'));
    }
  // Sale waiting approval 
    public function get_sale_by_wating_credit_json(Request $request){
        if($request->has('tab_approve')){
            $tab_approve = $request->tab_approve;   
        }else{
            $tab_approve = "waiting"; 
        }

        if( $tab_approve == "waiting"){
            $pro = 0;
        }elseif($tab_approve == "agree"){
            $pro = 1;
        }elseif($tab_approve == "not_yet_agree"){
            $pro = 2;
        }elseif($tab_approve == "not_agree"){
            $pro = 3;
        }

        if($request->has('submit_search')){ 
            $item_search = $request->item_search;
            $datas = Cs_Sale::join('cs_clients as mc','cs-sales.client_id','=','mc.id')
                    ->join('cs_request_form as csrf','csrf.sale_id','=','cs-sales.id')       
                    ->with(['cs_client','cs_request_form','sale_payment'])
                    ->where('cs-sales.is_approve','=',$pro)
                    ->where('cs-sales.deleted','=',1)
                    ->where('cs-sales.method','=','sale_by_credit')
                    ->where(function ($query) use ($item_search){
                        $query->where('mc.kh_username','LIKE','%'.$item_search.'%')
                              ->Orwhere('mc.en_username','LIKE','%'.$item_search.'%');
                      })
                    ->Orwhere('cs-sales.id','LIKE','%'.$request->item_search.'%')
                    ->paginate(10);  
            $datas->setPath('get_sale_by_wating_credit_json?item_search='.$request->item_search.'&submit_search=b_search');
        }else{
            $datas = Cs_Sale::with(['sale_payment', 'cs_request_form','sale_transaction_pay','sale_item','sale_item.item','cs_approval_credit_sale','cs_client'])
                    ->where('deleted','=',1)
                    ->where('is_approve','=',$pro)
                    ->where('method','=','sale_by_credit')
                    ->paginate(10); 
        }
             return response()->json($datas);
    }


    // Create  Get View Form Approval Credit Sale
    public function get_aprove_credit_sales_create(Request $request ,$id){
        $title = "ការបង្កើតការអនុម័តឥណទាន";
        $userdata = User::where('deteted','=',1)->where('user_status','=',1)->get();
        $interest = Mfi_Interest_Loan::where('status','=',1)->groupBy('module_interest_id')->get();
        $data_id = $id;
        return view('credit_sale.approval_credit_sale.create_approval_credit_sale',compact('interest','title','data_id','userdata'));
    } 

      // Create  Do Action Approval Credit Sale
    public function get_sale_by_id_aprove_credit_sales_create(Request $request ,$id){

        $datas = Cs_Sale::with(['sale_payment', 'cs_request_form','sale_transaction_pay','sale_item','sale_item.item','sale_item.item.categorys','cs_approval_credit_sale','cs_client'])
                            ->where('deleted','=',1)
                            ->where('method','=','sale_by_credit')
                            ->where('id','=',$id)
                            ->first();
                            return response()->json($datas);
    } 

    public function get_interest_data($data){
        $money_ount = str_replace(',','',$data['money_owne']);
            if($data['duration_pay_money_type'] != null && $data['place_for_pay'] != 0  ){
                $interest  = Mfi_Interest_Loan::where('module_interest_id','=',$data['interest_of_owne_precent'])
                                ->where('size_money_from','<',$money_ount)
                                ->where('size_money_to','>=',$money_ount)->first();
                $it_no_money  = Mfi_Interest_Loan::where('module_interest_id','=',$data['interest_of_owne_precent'])->first();
                if(count($interest) > 0){
                    $it = $interest;
                }else{
                    $it = $it_no_money;
                }
              // print_r($it );
                //$interest_data = 0;
                        if($data['duration_pay_money_type'] == 'day'){
                            if($data['place_for_pay']== 1){
                                $interest_data = $it->day_rate_brand;
                            }else{
                                $interest_data = $it->day_rate_villige;
                            }
                        }elseif($data['duration_pay_money_type'] == 'week'){
                            if($data['place_for_pay']== 1){
                                $interest_data = $it->weekly_rate_brand;
                            }else{
                                $interest_data = $it->weekly_rate_villige;
                            }
                        }elseif($data['duration_pay_money_type'] == '2week'){
                                if($data['place_for_pay']== 1){
                                    $interest_data = $it->two_weekly_brand;
                                }else{
                                    $interest_data = $it->two_weekly_rate_villige;
                                }
                        }elseif($data['duration_pay_money_type'] == 'month'){
                            if($data['place_for_pay']== 1){
                                $interest_data = $it->monthly_rate_brand;
                            }else{
                                $interest_data = $it->monthly_rate_villige;
                            }
                        }
                return $interest_data;
            }else{
                return false;
            }
    }
    public function post_aprove_credit_sales_create(Request $request ,$id){
        try {
            DB::beginTransaction();  
               $data_all = $request->all();
                $currency = 1;
                $status = 1;
                $deleted = 1;
                $data = Cs_Sale::find($id);
                //Staff ID
                $method = $data->method;
                if($data->cs_request_form->staff_id){
                    $staff_id = $data->staff_id;
                }else{
                    $staff_id = $data->user_id;
                }
                if($request->place_for_pay){
                    $place_for_pay = $request->place_for_pay;
                }else{
                    $place_for_pay = $data->place_for_pay;
                }
                // 
            if($request->is_agree){
                $is_agree = $request->is_agree;
            }else{
                    $is_agree =  1;
            }

             // interst 
              // interst 
              if($request->interest_of_owne_precent != 'special'){
                    $interest_cds_percent = $this->get_interest_data($data_all);
                }else{
                    $interest_cds_percent = $request->interest_of_owne_precent_special;
                }
                 $interest_cd = $interest_cds_percent /100;
                
                if($interest_cds_percent > 0){
                    $interest_of_owne_precent = $interest_cd;
                }else{
                    $interest_of_owne_precent = 0;
                }
            // Comment Mananger 
                if($request->comment_manager){
                    $comment_manager = $request->comment_manager; 
                }else{
                    $comment_manager = "We Agree for Credit Sale "; 
                }
            
                // Formula 
                    if($request->ftype_of_fomula){
                        $type_of_fomula = $request->type_of_fomula;
                    }else{
                        $type_of_fomula = 1;
                    }

                // សុំសងនៅថ្ងៃទី
                if($request->date_for_payments){
                        $date_for_payments = date('Y-m-d',strtotime($request->date_for_payments));
                }else{
                        $date_for_payments = date('Y-m-d',strtotime($data->date_for_payments));
                } 
                //កាលបរិច្ឆេទបើកផលិតផល
                    if($request->date_give_product){
                            $date_give_product = date('Y-m-d',strtotime($request->date_give_product));
                    }else{
                            $date_give_product = null;
                    }
                //កាលបរិច្ឆេទអនុម័តឥណទាន
                    if($request->date_approval){
                        $date_approval = date('Y-m-d',strtotime($request->date_approval));
                    }else{
                        $date_approval = null;
                    }
                // Deposit of Credit Sale 
                $deposit_pro_percent_div = number_format($request->deposit_pro_percent/100 ,2);
                $total_price_of_pro = $data->cs_request_form->prices_total_num;
                $deposit_pro_num =  $total_price_of_pro * $deposit_pro_percent_div; 


                $data_insert = [
                    'id'=> $id,
                    'sale_id' => $id,
                    'staff_id' => $staff_id,
                    'user_id' => Auth::user()->id,
                    'client_id' => $data->client_id,
                    'cds_request_id' => $data->cs_request_form->id,
                    'interest_of_owne_precent' => $interest_of_owne_precent,
                    'interest_type' => $request->interest_of_owne_precent,
                    'is_agree' => $is_agree,
                    'deposit_precent' => $request->deposit_precent,
                    'deposit_fixed' => str_replace(',','',$request->deposit_fixed),
                    'deposit_fixed_word' => $request->deposit_fixed_word,
                    'prices_total_num' => str_replace(',','',$request->prices_total_num),
                    'prices_totalword' => $request->prices_totalword,
                    'money_owne' => str_replace(',','',$request->money_owne),
                    'money_owne_word' => $request->money_owne_word,
                    'currency' => $currency,
                    'duration_pay_money' => $request->duration_pay_money,
                    'duration_pay_money_type' =>  $request->duration_pay_money_type,
                    'date_for_payments' => $date_for_payments,
                    'date_approval' => $date_approval,
                    'date_give_product' => $date_give_product,
                    'type_of_fomula' => $type_of_fomula,
                    'type_of_payment' => $request->type_of_payment,
                    'place_for_pay' => $place_for_pay,
                    'status' => $status,
                    'method' => $method,
                    'comment_manager' => $comment_manager,
                    'manager_id1' => $request->manager_id1,
                    'manager_id2' => $request->manager_id2,
                    'manager_id3' => $request->manager_id3,
                    'description' => $request->description,
                    'deleted' => $deleted,
                    'created_at' => date('Y-m-d h:m:s'),
                    'updated_at' =>  date('Y-m-d h:m:s'),

                ];

                $get_approval_id = Cs_ApprovalCredit::insertGetId($data_insert);

                        foreach($request->item_id as $key => $value){
                            if($request->item_id[$key]){
                                $sale_item = [
                                    'cs_approval_id' =>$id,
                                    'product_id' => $request->item_id[$key],
                                    'sell_item_id' => $request->sale_item_id[$key],
                                    'total_price' => $request->total_tax[$key],
                                    'qty' => $request->qty[$key],
                                    'sell_price' => $request->sell_price[$key],
                                    'cost_price' => $request->cost_price[$key],
                                    'total_tax' => $request->total_tax[$key],
                                    'commission' => $request->commission[$key],
                                    'other_price' => $request->other_price[$key],
                                    'discount' => $request->discount[$key],
                                    'total_price_payment' => $request->total_price_payment[$key]
                                    
                                ];
                            
                                Cs_Approval_Item::insert($sale_item);
                            }
                        }   
                    Cs_Cds_Request_Form::where('sale_id','=',$id)->update(['is_agree'=>$is_agree]);
                    Cs_Sale::where('id','=',$id)->update(['is_approve'=>$is_agree]);
                 DB::commit();    

                return response()->json($data_insert);
            } catch (\Exception $e) {
                DB::rollBack();

                $data = [
                    'message'    => 'Error Edit  Please Check: '.$e->getMessage(),
                    'alert-type' => 'error',
                ];
                return response()->json($data);

        }        
    }
    
    public function get_aprove_credit_sales_show(Request $request ,$id){
        $title = "ការបង្ហាញការអនុម័តឥណទាន";
        $data_id = $id;
        return view('credit_sale.approval_credit_sale.show_approval_credit_sale',compact('title','data_id'));
    }

    public function get_aprove_credit_sales_show_with_json(Request $request ,$id){
        $datas = Cs_ApprovalCredit::with(['approval_item.item','module_interest','cs_client',
        'cs_client.sub_clients','cs_sales.branch',
        'approval_item.item.categorys','approval_item.item.cs_brand',
        'cs_sales.cs_client','schedule','schedule.cs_schedule_timesheet',
        'manager_id1','manager_id2','manager_id3'])->where('id','=',$id)->first();
        return response()->json($datas);
    }
// Post Edit  Approve Credit Sale 
    public function get_aprove_credit_sales_edit(Request $request ,$id){
        $title = "កិច្ចសន្យាទិញបង់រំលស់";
        $userdata = User::where('deteted','=',1)->where('user_status','=',1)->get();
        $interest = Mfi_Interest_Loan::groupBy('module_interest_id')->get();
        $data_id = $id;
        return view('credit_sale.approval_credit_sale.edit_approval_credit_sale',compact('interest','title','data_id','userdata'));
    }

    public function get_json_aprove_credit_sales_edit(){
        
    }

    // Post Edit  Approve Credit Sale 
    public function post_aprove_credit_sales_edit(Request $request ,$id){
        try {
            DB::beginTransaction();   
                $data_all = $request->all();
                $currency = 1;
                $status = 1;
                $deleted = 1;
                $data = Cs_ApprovalCredit::find($id);
                $data_sale = Cs_Sale::find($id);
                //Staff ID
                $method = $data->method;
                if($data->staff_id){
                    $staff_id = $data->staff_id;
                }else{
                    $data_sale = $data->user_id;
                }
                if($request->place_for_pay){
                    $place_for_pay = $request->place_for_pay;
                }else{
                    $place_for_pay = $data->place_for_pay;
                }
                // 
            if($request->is_agree){
                $is_agree = $request->is_agree;
            }else{
                    $is_agree =  1;
            }

           // interst 
            if($request->interest_of_owne_precent != 'special'){
                $interest_cds_percent = $this->get_interest_data($data_all);
            }else{
                $interest_cds_percent = $request->interest_of_owne_precent_special;
            }
             $interest_cd = number_format($interest_cds_percent /100,3);
             
             if($interest_cds_percent > 0){
                 $interest_of_owne_precent = $interest_cd;
             }else{
                 $interest_of_owne_precent = 0;
             }
            
            // Comment Mananger 
                if($request->comment_manager){
                    $comment_manager = $request->comment_manager; 
                }else{
                    $comment_manager = "We Agree for Credit Sale "; 
                }
            
                // Formula 
                    if($request->ftype_of_fomula){
                        $type_of_fomula = $request->type_of_fomula;
                    }else{
                        $type_of_fomula = 1;
                    }

                // សុំសងនៅថ្ងៃទី
                if($request->date_for_payments){
                        $date_for_payments = date('Y-m-d',strtotime($request->date_for_payments));
                }else{
                        $date_for_payments = date('Y-m-d',strtotime($data->date_for_payments));
                } 
                //កាលបរិច្ឆេទបើកផលិតផល
                    if($request->date_give_product){
                            $date_give_product = date('Y-m-d',strtotime($request->date_give_product));
                    }else{
                            $date_give_product = null;
                    }
                //កាលបរិច្ឆេទអនុម័តឥណទាន
                    if($request->date_approval){
                        $date_approval = date('Y-m-d',strtotime($request->date_approval));
                    }else{
                        $date_approval = null;
                    }
                // Deposit of Credit Sale 
                $deposit_pro_percent_div = number_format($request->deposit_pro_percent/100 ,2);
                $total_price_of_pro = $data->prices_total_num;
                $deposit_pro_num =  $total_price_of_pro * $deposit_pro_percent_div; 


                $data_insert = [
                    'id'=> $id,
                    'sale_id' => $id,
                    'staff_id' => $staff_id,
                    'user_id' => Auth::user()->id,
                    'client_id' => $data->client_id,
                    'cds_request_id' => $data->cds_request_id,
                    'interest_of_owne_precent' => $interest_of_owne_precent,
                    'interest_type' => $request->interest_of_owne_precent,
                    'is_agree' => $is_agree,
                    'deposit_precent' => $request->deposit_precent,
                    'deposit_fixed' => str_replace(',','',$request->deposit_fixed),
                    'deposit_fixed_word' => $request->deposit_fixed_word,
                    'prices_total_num' => str_replace(',','',$request->prices_total_num),
                    'prices_totalword' => $request->prices_totalword,
                    'money_owne' => str_replace(',','',$request->money_owne),
                    'money_owne_word' => $request->money_owne_word,
                    'currency' => $currency,
                    'duration_pay_money' => $request->duration_pay_money,
                    'duration_pay_money_type' =>  $request->duration_pay_money_type,
                    'date_for_payments' => $date_for_payments,
                    'date_approval' => $date_approval,
                    'date_give_product' => $date_give_product,
                    'type_of_fomula' => $type_of_fomula,
                    'type_of_payment' => $request->type_of_payment,
                    'place_for_pay' => $place_for_pay,
                    'status' => $status,
                    'method' => $method,
                    'comment_manager' => $comment_manager,
                    'manager_id1' => $request->manager_id1,
                    'manager_id2' => $request->manager_id2,
                    'manager_id3' => $request->manager_id3,
                    'description' => $request->description,
                    'deleted' => $deleted,
                    'updated_at' =>  date('Y-m-d h:m:s'),

                ];

                $get_approval_id = Cs_ApprovalCredit::where('id','=',$id)->update($data_insert);
                        Cs_Approval_Item::where('cs_approval_id','=',$id)->delete();
                        foreach($request->item_id as $key => $value){
                            if($request->item_id[$key]){
                                $sale_item = [
                                    'cs_approval_id' =>$id,
                                    'product_id' => $request->item_id[$key],
                                    'sell_item_id' => $request->sale_item_id[$key],
                                    'total_price' => $request->total_tax[$key],
                                    'qty' => $request->qty[$key],
                                    'sell_price' => $request->sell_price[$key],
                                    'cost_price' => $request->cost_price[$key],
                                    'total_tax' => $request->total_tax[$key],
                                    'commission' => $request->commission[$key],
                                    'other_price' => $request->other_price[$key],
                                    'discount' => $request->discount[$key],
                                    'total_price_payment' => $request->total_price_payment[$key]
                                    
                                ];
                            
                                Cs_Approval_Item::insert($sale_item);
                            }
                        }   
                    Cs_Cds_Request_Form::where('sale_id','=',$id)->update(['is_agree'=>$is_agree]);
                    Cs_Sale::where('id','=',$id)->update(['is_approve'=>$is_agree]);
                DB::commit();    
            return response()->json($data_insert);   
            } catch (\Exception $e) {
                DB::rollBack();
    
                $data = [
                    'message'    => 'Error Edit  Please Check: '.$e->getMessage(),
                    'alert-type' => 'error',
                ];
                return response()->json($data);
    
        }
        
         
    }

    //Show contract  Approve Credit Sale   for Database
    public function get_contract_aprove_credit_sale(Request $request ,$id){
        $title = "ការបង្ហាញការអនុម័តឥណទាន";
        $data_id = $id;
        $appr_data = Cs_ApprovalCredit::with(['approval_item.item','approval_item.item.cs_brand',
                                          'cs_client.sub_clients','cs_sales.branch','approval_item.item.categorys',
                                          'cs_sales.cs_client','schedule','schedule.cs_schedule_timesheet'])
                                        ->where('cds_request_id','=',$id)
                                        ->first();
        // dd($appr_data);
        return view('credit_sale.approval_credit_sale.contract_credit_sale',compact('title','data_id','appr_data'));
    }    
    // Deleted  Approve Credit Sale   for Database
    public function post_delete_aprove_credit_sales_edit(Request $request ,$id){
        Cs_Approval_Item::where('cs_approval_id','=',$id)->delete();
        Cs_ApprovalCredit::where('id','=',$id)->delete();
        Cs_Cds_Request_Form::where('sale_id','=',$id)->update(['is_agree'=>0]);
        Cs_Sale::where('id','=',$id)->update(['is_approve'=>0]);
        return response()->json(['msg_show'=>'deleted success']);  
    }
    //Show contract  Approve Credit Sale   for Database
    public function get_print_contract_sale_approval(Request $request ,$id){

    }


}
