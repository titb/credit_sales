<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_Schedule;
use Auth;
use DB;
use Session;
use Excel;

class ReportScheduleController extends Controller
{
    //
    public function __construct()
    {
        $this->middleware('auth');
    }
    public function get_report_schedule(Request $request){
        $title = "Report Schedule Back";
        $codes = Cs_Schedule::orderBy('id','asc')->where('deleted','=',1)->get();
        $code = [];
        foreach($codes as $val){
            array_push($code, ['id'=>$val->id, 'code'=>$val->id]);
        }
        return view('credit_sale.report.report_schedule',compact('title','code'));
    }

    public function get_report_schedule_json(Request $request){
        
        if($request->has('submit_search')){
            // Search Start date To date
            if($request->from_date != "" && $request->to_date != ""){
                // from date
                $from_date = date('Y-m-d',strtotime($request->from_date));
                Session::flash('from_date',$request->from_date);
                // To date
                $to_date = date('Y-m-d',strtotime($request->to_date));
                Session::flash('to_date',$request->to_date);
            }else{
                // from date
                $from_date = '1970-01-01';
                Session::forget('from_date');
                // To date
                $to_date = date('Y-m-d');
                Session::forget('to_date');
            }
            // Search Brand
            if($request->brand_name != ""){
                $brand_name = $request->brand_name;
                Session::flash('brand_name',$request->brand_name);
            }else{
                $brand_name = '%_%';
                Session::forget('brand_name');
            }
            // Search Currency
            if($request->currency != ""){
                $currency = $request->currency;
                Session::flash('currency',$request->currency);
            }else{
                $currency = '%_%';
                Session::forget('currency');
            }
            // Search Client Name
            if($request->client_name != ""){
                $client_name = $request->client_name;
                Session::flash('client_name',$request->client_name);
            }else{
                $client_name = '%_%';
                Session::forget('client_name');
            }
            // Search Schedule ID
            if($request->schedule_id != ""){
                $schedule_id = $request->schedule_id;
                Session::flash('schedule_id',$request->schedule_id);
            }else{
                $schedule_id = '%_%';
                Session::forget('schedule_id');
            }
            // Search Staff Name
            if($request->staff_name != ""){
                $staff_name = $request->staff_name;
                Session::flash('staff_name',$request->staff_name);
            }else{
                $staff_name = '%_%';
                Session::forget('staff_name');
            }

            $data = Cs_Schedule::with(['branch','cs_client','user','cs_staff','cs_approvalcredit','cs_schedule_timesheet','cs_schedule_timesheet_pay','currency'])
            ->where('deleted','=',1)
            ->whereBetween('date_give_to_client', [$from_date,$to_date])
            ->where('branch_id','LIKE',$brand_name)
            ->where('currency_id','LIKE',$currency)
            ->where('client_id','LIKE',$client_name)
            ->where('id','LIKE',$schedule_id)
            ->where('staff_id','LIKE',$staff_name)
            ->paginate(15);
            
            

            // $result = [];
            // foreach($data as $key => $v){
            //         $result[] = ['id'=>$v->id,'value'=>	$v->id];
            // }
        }else{
            $data = Cs_Schedule::with(['branch','cs_client','user','cs_staff','cs_approvalcredit','cs_schedule_timesheet','cs_schedule_timesheet_pay','currency'])
            ->where('deleted','=',1)
            ->paginate(15);
        }
        
        return response()->json(['data'=>$data]);
    }

    public function export_report_to_excel(Request $request){
        
        Excel::create('schedule report', function($excel) {

            $excel->sheet('schedule report', function($sheet) {
                $data = Cs_Schedule::get();
                $sheet->row(1, array(
                    'Report Repayment Back',
                ));
                $sheet->row(2, array(
                    '#', 'លេខកូដតារា','ឈ្មោះអតិថិជន','ចំនួនទឹកប្រាក់','ប្រាក់បង់ដំបូង៣០%','ប្រាក់ដែលនៅជំពាក់','រយៈពេលសង','ថ្ងៃទទួលទំនិញ','បុគ្គលិកប្រមូលប្រាក់','រូបិយប័ណ្ណ'
                ));
                $i = 3;
                $data_count = count($data);
                foreach($data as $key=>$d){
                    
                    $sheet->row($key+$i, array(
                        $key+1, $d->id, $d->cs_client->kh_username, $d->money_owne_total_pay, $d->cs_approvalcredit->money_owne, $d->money_owne_interest, $d->cs_approvalcredit->date_give_product, $d->cs_approvalcredit->date_approval, $d->cs_staff->name_kh, $d->currency->name
                    ));
                    $sheet->row($key+$i, function($row){
                        $row->setFontFamily('Battambang');
                    });
                    
                }

                // Style
                $sheet->setBorder('A1:J10', 'thin');
                $sheet->mergeCells('A1:J1');
                $sheet->setHeight(1,70);
                $sheet->setHeight(2,40);

                
                $sheet->setStyle(array(
                    'font' => array(
                        'name'      =>  'Battambang',
                        'size'      =>  10,
                    )
                ));

                $sheet->row(1, function($row){
                    $row->setBackground('#d74a4a');
                    $row->setFontFamily('Battambang');
                
                });
                $sheet->row(2, function($row){
                    $row->setBackground('#fab8b8');
                    $row->setFontFamily('Battambang');
                
                });
                
                $sheet->cell('A1:J'.$data_count.'', function($cell){
                    $cell->setAlignment('center');
                    $cell->setValignment('center');
                    $cell->setFont(array(
                        'family' => 'Battambang',
                        'size' => '12',
                        'bold' => true
                    ));
                });

                $sheet->cell('A1:J1', function($cells){
                    $cells->setFont(array(
                        'size' => '15',
                    ));
                });

            });
        })->export('xlsx');
    }
    
}