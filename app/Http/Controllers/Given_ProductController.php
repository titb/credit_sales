<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_ApprovalCredit;
use App\Cs_co_give_item_client;
use App\Cs_Schedule;
use App\Cs_Client;
use App\User;
use Auth;
use DB;
use Session;

class Given_ProductController extends Controller
{

    

    public function __construct()
    {
        $this->middleware('auth');
    }
    // validate give product to co 
    public function validate_product_give_to_co(Request $request){
        return $this->validate($request,[
                    'schedule_id' => 'required',
                    'client_id' => 'required',
                    'currency_id' => 'required',
                    'approve_by' => 'required',
                    'staff_id' => 'required',
                    'give_by' => 'required',
                    'comment' => 'required',
                ]);
        
    }
    // data give product to co 
    public function data_give_product_to_co(Request $request){
        if($request->date_give){
            $date_give = date('Y-m-d',strtotime($request->date_give));
        }else{
            $date_give = date('Y-m-d');
        }
        if($request->date_approval){
            $date_approval = date('Y-m-d',strtotime($request->date_approval));
        }else{
            $date_approval = date('Y-m-d');
        }


        $this->validate_product_give_to_co($request);
        $data = [
            'user_id' => Auth::user()->id,
            'client_id' => $request->client_id,
            'schedule_id' => $request->schedule_id,
            'currency_id' =>  $request->currency_id,
            'approve_by' =>  $request->approve_by,
            'staff_id' => $request->staff_id,
            'give_by' =>  $request->give_by,
            'branch_id' => Auth::user()->branch_id,
            'date_approval' => $date_approval,
            'date_give' => $date_give,
            'comment' => $request->comment,
            'status' => $request->status,
            'active' =>1,
            'deleted' =>1,
        ];
        return $data;
    }
    // validate give product to  Client 
    public function validate_give_product_client(Request $request){
        return $this->validate($request,[
            'schedule_id' => 'required',
            'give_to_client_id' => 'required',
            'num_give_to_client' => 'required',
            'date_give_to_client' => 'required',
            'staff_give_id' => 'required',
            'comment_give_to_client' => 'required',
        ]);
    }

    // data give product to client 
    public function data_give_product_client(Request $request){

        $this->validate_give_product_client($request);
        
        $data = [
            'user_id' => Auth::user()->id,
            'branch_id' => Auth::user()->branch_id,
            'num_give_to_client' => $request->num_give_to_client,
            'date_give_to_client' => date('Y-m-d',strtotime($request->date_give_to_client)),
            'staff_give_id' => $request->staff_give_id,
            'comment_give_to_client' => $request->comment_give_to_client,
            'status' => 1,
            
        ];
        return $data;
    }

    // List 
    public function get_list_given_to_co(){
        $title = "ការគ្រប់លើការបើកផលិតផល";  
        $datas = Cs_co_give_item_client::where('deleted','=',1)->orderBy('id','desc')->paginate(30);
        return view('credit_sale.product_given.list_pro_give_co', compact('title','datas'));
    }

    // get data schedule 
    public function get_data_function(Request $request , $id){
        $data = Cs_Schedule::with(['currency','cs_client','cs_approvalcredit.approval_item.item.categorys','cs_give_to_client'])
                ->where('id','=',$id)->first();
        return response()->json($data);
    }
    // get data Give Product to  co 
    public function get_data_give_to_co_by_id(Request $request ,$id){
        $datas = Cs_co_give_item_client::with('schedule.cs_approvalcredit.approval_item.item.categorys','schedule.currency','client','staff_give','staff','approve_by','give_by')
        ->where('id','=',$id)->where('deleted','=',1)->first();
        return response()->json($datas);
    }

    // Create
    public function get_create_gevin_to_co(Request $request){

       
       $user = User::where('deteted','=',1)->get();
       $user_login_id = Auth::user()->id;
       $item_id = $request->item_id;
       if($request->redirect == "give_product_to_co"){
             $title = "ការបើកផលិតផលអោយទៅមន្រ្តីឥណទាន";
             return view('credit_sale.product_given.create_pro_give_co', compact('title','user','user_login_id','item_id'));
       }elseif($request->redirect == "give_product_to_client"){
            $title = "ការបើកផលិតផលអោយអតិថិជន";
            return view('credit_sale.product_given.create_pro_give_client', compact('title','user','user_login_id','item_id'));

       }
    }
    // POS Create 
    public function post_create_give_to_co(Request $request){
        // $this->validate_product_give($request->all());
        if($request->redirect == 'give_product_to_co'){
            $data = $this->data_give_product_to_co($request);
        
            if($request->item_id != 0){
                Cs_co_give_item_client::where('id','=',$request->item_id)->update($data);
                Cs_ApprovalCredit::where('id',$request->schedule_id)->update(['date_give_product'=>$data['date_give']]);
            }else{
                $data_it = Cs_co_give_item_client::insertGetid(array_add($data,'created_at',date('Y-m-d h:m:s')));
                Cs_Schedule::where('id',$request->schedule_id)->update(['is_give'=>2]);
            }
        }elseif($request->redirect == 'give_product_to_client'){
            $data = $this->data_give_product_client($request);
            Cs_co_give_item_client::where('id','=',$request->give_to_client_id)->update($data);
            if($request->item_id == 0){
                Cs_Schedule::where('id',$request->schedule_id)->update(['is_give'=>1]);    
            } 
        }else{
          $data = "error url";  
        }
        return response()->json(['data'=>$data]);
    }

    //  Give Product to client
    public function deleted_give_product_to_co(Request $request, $id){
        Cs_co_give_item_client::where('id',$id)->update(['deleted'=>0]);
        return response()->json(['data'=>'Successful deleted from Database']);        
    }
    public function deleted_from_database_give_product_to_co(Request $request, $id){
        $data = Cs_co_give_item_client::find($id);
        Cs_Schedule::where('id',$data->schedule_id)->update(['is_give'=>0]);  
        $data->delete();  
        return response()->json(['data'=>'Successful deleted from Database']);
    }
    

    // List 
    public function get_list_given_to_cl(){
        $title = "ការគ្រប់ការបើកផលិតផលអោយអតិថិជន";
        $datas = Cs_co_give_item_client::where('deleted','=',1)->where('status','=',1)->orderBy('id','desc')->paginate(30);
        return view('credit_sale.product_given.list_pro_give_client', compact('title','datas'));
    }

    public function delete_data_give_to_client(Request $request,$id){
        $data_1 = Cs_co_give_item_client::find($id);
        $data = [
            'user_id' => Auth::user()->id,
            'branch_id' => Auth::user()->branch_id,
            'num_give_to_client' => null,
            'date_give_to_client' => null,
            'staff_give_id' => 0,
            'comment_give_to_client' => null,
            'status' => 2,
        ];
        Cs_co_give_item_client::where('id','=',$id)->update($data);
        Cs_Schedule::where('id',$data_1->schedule_id)->update(['is_give'=>2]); 
        return response()->json(['data'=>'Successful deleted Data Give To Client']); 
    }

    // update give product client 
    
    public function show_detail_give_product(Request $request){
        $redirect = $request->redirect;
        if($redirect == 'give_product_to_co'){
            $title = "ការបើកផលិតផលអោយមន្រ្តីឥទាន";
        }else{
            $title = "ការបើកផលិតផលអោយអតិថិជន";
        }
                   
            $item_id = $request->item_id;
            
            return view('credit_sale.product_given.show_detail',compact('title','item_id','redirect'));
    }
}
