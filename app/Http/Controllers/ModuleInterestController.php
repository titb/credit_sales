<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Module_Interest;
use Auth;
use DB;
use Session;
class ModuleInterestController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function view(){
    	$title = "Module Interest";
    	$data = Module_Interest::where('active','=',1)->orderBy('id','asc')->paginate(30);
    	return view('credit_sale.module_interest.view',compact('title','data'));
    }

    public function get_module_create(){
    	$title = "Create New Module Interest";
    	return view('credit_sale.module_interest.create',compact('title'));
    }

    public function post_module_create(Request $request){
    	$data = [
    			'user_id' => Auth::user()->id,
    			'name' => $request->name,
    			'display_name_eng' => $request->display_name_eng,
    			'display_name_kh' => $request->display_name_kh,
				'active' => $request->active,
				'loan_type'=> $request->loan_type,
    			'description' => $request->description,
    			'created_at' => date('Y-m-d')
    			];
    	DB::table('module_interest')->insert($data);
    	Session::flash('success','Data insert successful!');
    	return redirect('module_interest');
    }

    public function get_module_edit($id){
    	$title = "Update Module Interest";
    	$data = Module_Interest::find($id);
    	return view('credit_sale.module_interest.update',compact('title','data'));
    }

    public function post_module_edit(Request $request, $id){
    	$data = [
    			'user_id' => Auth::user()->id,
    			'name' => $request->name,
    			'display_name_eng' => $request->display_name_eng,
    			'display_name_kh' => $request->display_name_kh,
				'active' => $request->active,
				'loan_type'=> $request->loan_type,
    			'description' => $request->description,
    			'updated_at' => date('Y-m-d')
    			];
    	DB::table('module_interest')->where('id','=',$id)->update($data);
    	Session::flash('success','Data update successful!');
    	return redirect('module_interest');
    }

    public function post_module_deleted($id){
    	DB::table('module_interest')->where('id','=',$id)->delete();
    	Session::flash('success','Data delete successful!');
    	return redirect('module_interest');
    }
}
