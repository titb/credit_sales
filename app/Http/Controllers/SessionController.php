<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SessionController extends Controller
{   
    protected $name;
    public function __construct(){

    }
    public function add_session_new(Request $request){
        
        if($request->has('tab_approve')){
            $tab_approve = $request->session()->put('tab_approve', $request->tab_approve);
        }
        $data = [
            'tab_approve'=>$request->session()->get('tab_approve') 
        ];

        return response()->json($data);
    }

    
}
