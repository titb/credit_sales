<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use Auth;
use DB;
use Session;
class ItemController extends Controller
{
    public function __construct()
    {
        $this->middleware('auth');
    }
    //index
    public function get_list_items(Request $request){

        $title = "កាគ្រប់គ្រង់ផលិតផល";
        return view('credit_sale.items.index_item',compact('title'));

    }
    
    // Ajax autocomplet 
    public function get_list_xjax_search_items(Request $request){
        if($request->has('submit_search')){   
            if($request->item_search != ""){
                $item_search = $request->item_search;
                Session::flash('item_search',$request->item_search);
              }else{
                $item_search = '%_%';
                Session::forget('item_search');
              }
              if($request->category_search != ""){
                $category_search = $request->category_search;
                Session::flash('category_search',$request->category_search);
              }else{
                $category_search = '%_%';
                Session::forget('category_search');
              }
            $data = Cs_items::join('cs_categorys as csc','csc.id','=','cs_items.category_id')
                    // ->join('cs_suppliers as css','css.id','=','cs_items.supplier_id')
                    // ->join('cs_brand as csb','csb.id','=','cs_items.brand_id')
                    // ->rightjoin('cs-inventorys as csin','csin.item_id','=','cs_items.id')
                    ->with('inventorys')
                    ->select('cs_items.*','csc.name as csc_name','cs_items.qty')
                    ->where('cs_items.deleted','=',0)->where('cs_items.status','=',1)
                    ->where('cs_items.name','LIKE','%'.$item_search.'%')
                    ->where('cs_items.category_id','LIKE','%'.$category_search.'%')
                    ->paginate(10);
            $data->setPath('products/products_get_js?item_search='.$request->item_search.'&category_search='.$request->category_search.'&submit_search=b_search');
        }else{
            $data = Cs_items::join('cs_categorys as csc','csc.id','=','cs_items.category_id')
            // ->join('cs_suppliers as css','css.id','=','cs_items.supplier_id')
            // ->join('cs_brand as csb','csb.id','=','cs_items.brand_id')
            //->leftjoin('cs-inventorys as csin','cs_items.id','=','csin.item_id')
            ->with('inventorys')
           // ->select('cs_items.*','csc.name as csc_name','css.name as css_name','csb.name as csb_name','cs_items.qty')
           ->select('cs_items.*','csc.name as csc_name','cs_items.qty')
            ->where('cs_items.deleted','=',0)->where('cs_items.status','=',1)
            ->paginate(10);
        } 
         
        return response()->json($data);
    }

    // create form Item 

    public function get_create_items(Request $request){
        $title = "ការបង្កើតផលិតផលថ្មី";
        return view('credit_sale.items.form_item',compact('title'));
    }

    public function post_create_items1(Request $request){
         $mess = $request->all();
         return response()->json($mess);
    }
    // Insert data to database 
    public function post_create_items(Request $request){
          //  $mess = $request->all();
            if($request->hasFile('images')){
            
                $destinationPath = "Account/images";
                $im_cl_up = $request->file('images');
                $fileName = $im_cl_up->getClientOriginalName();
                $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
                $adimage = $request->get('Account/images',$fileName);
                
            }else{

                $adimage = null;
            }

            // Commission 
                if($request->has('override_default_commission')){
                    if($request->commission_type == "fixed_amount"){
                        $commission_fixed = $request->commission;
                    }else{
                        $commission_fixed = 0;
                    }
                    if($request->commission_type == "precent_amount"){
                        $commission_give = $request->commission;
                    }else{
                        $commission_give =0;
                    }
                }else{
                    $commission_fixed = 0;
                    $commission_give =0;
                }
                if($request->start_date != null && $request->end_date != null){
                    $stare_date = date('Y-m-d',strtotime($request->start_date));
                    $end_date = date('Y-m-d',strtotime($request->end_date));
                }else{
                    $stare_date = null;
                    $end_date = null;

                }
            // Commission 
            $data = [
                        'item_bacode'=> $request->item_bacode,
                        'name'=> $request->name,
                        'add_number_id'=> $request->add_number_id,
                        'brand_id'=> $request->brand_id,
                        'category_id'=> $request->category_id,
                        'supplier_id'=> $request->supplier_id,
                        'size'=> $request->size,
                        'cost_price'=> $request->cost_price,
                        'sell_price'=> $request->sell_price,
                        'promo_price'=> $request->promo_price,
                        'start_date'=> $stare_date,
                        'end_date'=> $end_date,
                        'pro_image'=> $adimage,
                        'recode_level' => $request->recode_level,
                        'description' => $request->description,
                        'is_service' => $request->is_service,
                        'tax_include' => $request->tax_include,
                        'override_default_tax' => $request->override_default_tax,                        
                        'override_default_commission' => $request->override_default_commission,
                        'commission_fixed' => $commission_fixed,
                        'commission_give' => $commission_give,
                        'discount' => $request->discount,
                        'user_id' => Auth::user()->id,
                        'branch_id' => Auth::user()->branch_id,
                        'status'=> 1,
                        'created_at' => date('Y-m-d h:m:s')
                    ];

                $cs_item_id = Cs_items::insertGetId($data);
                $num_of_borcode = 10;        
                       
               
            if($cs_item_id){

                    if($request->item_bacode == null){
                        $barcode = str_pad($cs_item_id,$num_of_borcode,"0",STR_PAD_LEFT);
                        Cs_items::where('id','=',$cs_item_id)->update(['item_bacode'=>$barcode ,'add_number_id'=>$cs_item_id]);
                    }
                    if($request->has('override_default_tax')){
                        if($request->has('tax')){
                            foreach ($request->tax as $key => $tax){
                                if($request->tax[$key] && $request->tax_num[$key]){
                                        $data_tax = [
                                                    'item_id' => $cs_item_id,
                                                    'name'=>$request->tax[$key],
                                                    'percent'=> $request->tax_num[$key]
                                                ];
                                        DB::table('cs_items_tax')->insert($data_tax);
                                }                
                            }
                        }
                    }
                    if ($request->has('is_service') == false) {    
                        if($request->qty){
                            $data_inv = [
                                'item_id' => $cs_item_id,
                                'branch_id' => Auth::user()->branch_id,
                                'user_id'=> Auth::user()->id,
                                'status_transation' => "add new",
                                'qty' => $request->qty,
                                'size' => $request->size,
                                'created_at' => $request->created_at
                            ];
                            Cs_inventorys::insert($data_inv);
                        }
                    }    
                        $us_gr = [
                            'ip_log'=> $request->ip(),
                            'active'=> "ការបង្កើតផលិតផលថ្មី",
                            'user_id'=> Auth::user()->id,
                            'status'=> '2',
                            'what_id' => $cs_item_id,
                            'method' => 'items',
                            'create_date' => date('Y-m-d h:m:s')
                        ];
                    DB::table('cs-history-logs')->insert($us_gr);
                    $datas = Cs_items:: find($cs_item_id);    
                    $mess = [
                        'datas' =>$datas,
                        'redirect' => $request->redirect,
                        'msg_show' => '<div class="alert alert-success"><p>Success! Create Supplier</p></div>'
                    ];
                }else{
                    $mess = [
                        'datas' =>$datas,
                        'redirect' => $request->redirect,
                        'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                    ];
                }
        
            return response()->json($mess);
     
           
    } 

    // Show Item
    public function get_show_items(Request $request ,$id){  
        $title = "ការបង្ហាញព័ត៌មានផលិតផល";
        $form = $request->form;
        $datas = Cs_items::find($id);
        $data_id = $id;
        return view('credit_sale.items.show_item',compact('title','data_id','datas','form'));
    }
    public function get_show_items_stock(Request $request ,$id){  
        $title = "ការបង្ហាញព័ត៌មានផលិតផល";
        $form = $request->form;
        $datas = Cs_items::find($id);
        $data_id = $id;
        return view('credit_sale.items.show_item_stock',compact('title','data_id','datas','form'));
    }
    public function get_form_edit_items(Request $request,$id){
        $title = "ការកែប្រែរព័ត៌មានផលិតផល";
        $data_id = $id;
        return view('credit_sale.items.form_item',compact('title','data_id'));
        
    }
    public function get_json_edit_items(Request $request,$id){
        $data1 = Cs_items::with(['categorys','cs_suppliers','cs_brand','cs_item_tax'])->where('id','=',$id)->first();
       
        $qty = [
            'qty' => $data1->inventorys->sum('qty'),
            'tax_over_item' => $data1->cs_item_tax()->get()
        ];
        $data = array_add($data1,
                    'qty_re',$qty    
                );
         
         
        return response()->json($data);
    }
     
        // EDIT data to database 
    public function post_edit_items(Request $request ,$id){
    //      $mess = $request->all();
    //     return response()->json($mess);
    //  dd();
        $data1 = Cs_items::find($id);
              if($request->hasFile('images')){
              
                  $destinationPath = "Account/images";
                  $im_cl_up = $request->file('images');
                  $fileName = $im_cl_up->getClientOriginalName();
                  $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
                  $adimage = $request->get('Account/images',$fileName);
                  
              }else{
  
                  $adimage =$data1->pro_image;
              }
  
              // Commission 
                  if($request->has('override_default_commission')){
                      if($request->commission_type == "fixed_amount"){
                          $commission_fixed = $request->commission;
                      }else{
                          $commission_fixed = 0;
                      }
                      if($request->commission_type == "precent_amount"){
                          $commission_give = $request->commission;
                      }else{
                          $commission_give =0;
                      }
                  }else{
                      $commission_fixed = 0;
                      $commission_give =0;
                  }
                  if($request->start_date != null && $request->end_date != null){
                      $stare_date = date('Y-m-d',strtotime($request->start_date));
                      $end_date = date('Y-m-d',strtotime($request->end_date));
                  }else{
                      $stare_date = null;
                      $end_date = null;
                  }
              // Commission 
              $data = [
                          'item_bacode'=> $request->item_bacode,
                          'name'=> $request->name,
                          'add_number_id'=> $request->add_number_id,
                          'brand_id'=> $request->brand_id,
                          'category_id'=> $request->category_id,
                          'supplier_id'=> $request->supplier_id,
                          'size'=> $request->size,
                          'cost_price'=> $request->cost_price,
                          'sell_price'=> $request->sell_price,
                          'promo_price'=> $request->promo_price,
                          'start_date'=> $stare_date,
                          'end_date'=> $end_date,
                          'pro_image'=> $adimage,
                          'recode_level' => $request->recode_level,
                          'description' => $request->description,
                          'is_service' => $request->is_service,
                          'tax_include' => $request->tax_include,
                          'override_default_tax' => $request->override_default_tax,                        
                          'override_default_commission' => $request->override_default_commission,
                          'commission_fixed' => $commission_fixed,
                          'commission_give' => $commission_give,
                          'discount' => $request->discount,
                          'user_id' => Auth::user()->id,
                          'branch_id' => Auth::user()->branch_id,
                          'status'=> 1,
                          'created_at' => date('Y-m-d h:m:s')
                      ];
  
                  $cs_item_id = Cs_items::where('id','=',$id)->update($data);
                  $num_of_borcode = 10;        
                         
  
              if($cs_item_id){
  
                      if($request->item_bacode == null){
                          $barcode = str_pad($id,$num_of_borcode,"0",STR_PAD_LEFT);
                          Cs_items::where('id','=',$id)->update(['item_bacode'=>$barcode ,'add_number_id'=>$id]);
                      }
                      if($request->has('override_default_tax')){

                          if($request->has('tax')){
                            DB::table('cs_items_tax')->where('item_id','=',$id)->delete();
                              foreach ($request->tax as $key => $tax){
                                if($request->tax[$key] && $request->tax_num[$key]){
                                    $data_tax = [
                                    'item_id' => $id,
                                    'name'=>$request->tax[$key],
                                    'percent'=> $request->tax_num[$key]
                                    ];
                                  DB::table('cs_items_tax')->insert($data_tax);
                                }   
                              }
                          }
                      }else{
                        DB::table('cs_items_tax')->where('item_id','=',$id)->delete();
                      }
                          
                          if($request->qty != 0){
                              $data_inv = [
                                  'item_id' => $id,
                                  'branch_id' => Auth::user()->branch_id,
                                  'user_id'=> Auth::user()->id,
                                  'status_transation' => "add new",
                                  'qty' => $request->qty,
                                  'size' => $request->size,
                              ];
                            if(count($data1->inventorys) > 0){
                                Cs_inventorys::where('item_id','=',$id)->update($data_inv);
                            }else{
                                Cs_inventorys::insert($data_inv);
                            }  
                             
                          }
                     
                          $us_gr = [
                              'ip_log'=> $request->ip(),
                              'active'=> "ការកែប្រែរព័ត៌មានផលិតផល",
                              'user_id'=> Auth::user()->id,
                              'status'=> '2',
                              'what_id' => $id,
                              'method' => 'items',
                              'create_date' => date('Y-m-d h:m:s')
                          ];
                      DB::table('cs-history-logs')->insert($us_gr);
  
                      $mess = [
                          'msg_show' => '<div class="alert alert-success"><p>Success! Create Supplier</p></div>'
                      ];
                  }else{
                      $mess = [
                          'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                      ];
                  }
  
              return response()->json($mess);
    }
    

    // Deleted  Supplier 
 
    public function post_delete_items(Request $request,$id){
        Cs_inventorys::where('item_id','=',$id)->delete();
        DB::table('cs_items_tax')->where('item_id','=',$id)->delete();
        $cs_item_id = Cs_items::where('id','=',$id)->delete();
            if($cs_item_id){
                    $us_gr = [
                        'ip_log'=> $request->ip(),
                        'active'=> "លុបព័ត៌មានផលិតផល",
                        'user_id'=> Auth::user()->id,
                        'status'=> '2',
                        'what_id' => $id,
                        'method' => 'item',
                        'create_date' => date('Y-m-d h:m:s')
                    ];
                DB::table('cs-history-logs')->insert($us_gr);

                $mess = [
                    'msg_show' => '<div class="alert alert-success"><p>Success! Delete Item '.$id.'</p></div>'
                ];
            }else{
                $mess = [
                    'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                ];
            }
        return response()->json($mess);
    }
}
 