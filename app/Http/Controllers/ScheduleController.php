<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\User;
use App\Cs_Client;
use App\Cs_Image_Data;
use App\Cs_brand;
use App\Cs_categorys;
use App\Cs_suppliers;
use App\Cs_items;
use App\Cs_inventorys;
use App\Cs_Sale;
use App\Cs_Sale_Item;
use App\Cs_Sale_Payment;
use App\Cs_Sale_Payment_Tran;
use App\Cs_Cds_Request_Form;
use App\Cs_ApprovalCredit;
use App\Cs_Approval_Item;
use App\PublicHoliday;
use App\Mfi_Interest_Loan;
use App\Mfi_New_Interest_Rate;
use App\Cs_Schedule_Timesheet_Pay;
use App\Cs_Schedule_Timesheet;
use App\Cs_Schedule;
use Auth;
use Session;
use DateTime;
use DB;
use Cart;
class ScheduleController extends Controller
{
    //

    public function __construct()
    {
        $this->middleware('auth');
    }
    public function date_count_date($stare_date = null ,$end_date = null){
		$dat_payf = new DateTime(date("Y-m-d",strtotime($stare_date)));
		$date_generat = new DateTime(date("Y-m-d",strtotime($end_date)));
		$dat_pay1 = $date_generat->getTimestamp() - $dat_payf->getTimestamp();

		$date_generat1 = $dat_pay1 / (60 * 60 *24);
		return $date_generat1;
    } 
    
    // function check_array_holidy($daypay){
    //     $pub_hol1 = PublicHolidayRest::all(); 
    //     $arr = [];
    //     foreach($pub_hol1 as $p){
    //         $arr[] =$p->date_for_holiday;
    //     }
        
    //     if(is_array($daypay , $arr)){
    //         $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($datpay)));

    //     }else{
    //         $day_pay1 = $datpay ; 
    //     }
    //     return $day_pay1;
    // }


    public function show_generate_schedule(Request $request,$id){
        $title = "កាលវិភាគសងប្រាក់អតិថិជន";
        $data_id = $id;
        return view('credit_sale.approval_credit_sale.generate_schedule',compact('title','data_id'));
    }
    public function schedule_show_json(Request $request,$id){
        $data = Cs_Schedule::with(['cs_approvalcredit.approval_item.item','cs_schedule_timesheet','cs_client','user','cs_staff'])
        ->where('id','=',$id)
        ->first();   
        return response()->json($data);
    }
    // Generate Schedule 
    public function generate_schedule(Request $request,$id){
        try {
            DB::beginTransaction(); 
            $change_to_kh = 4100;
            $schedule_type_setting = 1;
            $currency_setting = 1;
            $data = Cs_ApprovalCredit::find($id);
            //if Data has Schedule 
            if(count($data->schedule) >0){
                    Cs_Schedule_Timesheet::where('cs_schedules_id','=',$id)->delete();
                    Cs_Schedule_Timesheet_Pay::where('cs_schedules_id','=',$id)->delete();
                    Cs_ApprovalCredit::where('id','=',$id)->update(['is_finish'=>0]);
            }
    
            // End if Data has Schedule 
            $interest_of_owne_precent = $data->interest_of_owne_precent;
            $money_owne = $data->money_owne;
            $currency = $data->currency;
            $dura = $data->duration_pay_money;
            $type_of_payment = $data->type_of_payment;
            $date_for_open = $data->date_give_product;
            $date_for_payments = $data->date_for_payments;
            $dura_type = $data->duration_pay_money_type;
            $pay_month = date('m',strtotime($data->date_for_payments));
            $pat_year = date('Y',strtotime($data->date_for_payments));
            $count_day =  cal_days_in_month(CAL_GREGORIAN,$pay_month,$pat_year);
            
            if($dura_type === "month"){
                $dura_loop = 1;
            }elseif($dura_type === "2week"){
                $dura_loop = 2;
            }elseif($dura_type === "week"){
                $dura_loop = 4;
            }elseif($dura_type ==="day"){
                $dura_loop = $count_day;
            }
                
                $dura_real = $dura * $dura_loop;
                $money_owne_1 = $money_owne / $dura_real;
                $owncoustpay = $money_owne;
            //  echo "<table border=1 >";
                $places = -2;
                $mult = pow(10, abs($places));
                $pay_total_all = 0;
                $total_interest_pay = 0;
                $total_cost_pay = 0;

            //holiday array 
                    $sp = DB::table('mfi_public_holiday_reset')->select('date_for_holiday')->orderBy('date_for_holiday','desc')->get();
                    $to = array();
                    foreach ($sp as $key => $value) {
                    
                        $to[] = $value->date_for_holiday;
                    }
                    $da = $to;
            
            // Holiday array 
        if( $data->deposit_fixed > 0 ){
                $data_sc_timesheet = [
                        'cs_schedules_id' => $id,
                        'total_pay_cost' => $data->deposit_fixed,
                        'total_pay_interest' => 0,
                        'total_payment' => $data->deposit_fixed,
                        'total_pay_cost_owe' => $data->money_owne,
                        'date_payment' => $date_for_open,
                        'status' => 0,
                        'status_for_pay' =>0,
                        'active' => 1,
                        'is_finish' => 0,
                        'description' => "first_payment_30"
                ];
            
            Cs_Schedule_Timesheet::insert($data_sc_timesheet);
        }    
            for($i = 0; $i < $dura_real ; $i++){

        // add in date 06/05/2019
                    if($i == 0){
                        $datpay =  date('Y-m-d',strtotime($date_for_open));
                    }elseif($i == 1){
                        $datpay =  date('Y-m-d',strtotime($data->date_for_payments));
                    }else{
                        if($dura_type === "month"){
                            $datpay =  date('Y-m-d',strtotime('+'. $i .' month',strtotime($data->date_for_payments)));
                        }elseif($dura_type === "2week"){
                            $datpay =  date('Y-m-d',strtotime('+'. $i+2 .' week',strtotime($data->date_for_payments)));
                        }elseif($dura_type === "week"){
                            $datpay =  date('Y-m-d',strtotime('+'. $i .' week',strtotime($data->date_for_payments)));
                        }elseif($dura_type === "day"){
                            $datpay =  date('Y-m-d',strtotime('+'. $i .' day',strtotime($data->date_for_payments)));
                        }
                    
                    }        
        // close in date 06/05/2019 
                    // if($i == 0){
   
                    //     $datpay =  date('Y-m-d',strtotime($data->date_for_payments));
                    // }else{
                    //     if($dura_type == "month"){
                    //         $datpay =  date('Y-m-d',strtotime('+'. $i .' month',strtotime($data->date_for_payments)));
                    //     }elseif($dura_type == "2week"){
                    //         $datpay =  date('Y-m-d',strtotime('+'. $i+2 .' week',strtotime($data->date_for_payments)));
                    //     }elseif($dura_type == "week"){
                    //         $datpay =  date('Y-m-d',strtotime('+'. $i .' week',strtotime($data->date_for_payments)));
                    //     }elseif($dura_type == "day"){
                    //         $datpay =  date('Y-m-d',strtotime('+'. $i .' day',strtotime($data->date_for_payments)));
                    //     }
                    
                    // }
                if (in_array($datpay, $da)){
                    $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($datpay)));
                        if(in_array($day_pay1, $da)){
                            $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                            if(in_array($day_pay1, $da)){
                                $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                                if(in_array($day_pay1, $da)){
                                    $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                                    if(in_array($day_pay1, $da)){
                                        $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                                        if(in_array($day_pay1, $da)){
                                            $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                                            if(in_array($day_pay1, $da)){
                                                $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                                                if(in_array($day_pay1, $da)){
                                                    $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                                                    
                                                }
                                            }
                                        }
                                    }
                                }
                            }
                        }
                }else{
                    $day_pay1 = $datpay ; 
                }
            
    // If Date Payment is Sunday 
                $pubi = date('l',strtotime($day_pay1));
                if($pubi == "Sunday"){
                    
                    $day_pay1 =  date('Y-m-d',strtotime('+ 1 day',strtotime($day_pay1)));
                }else{
                    $day_pay1 =  date('Y-m-d',strtotime($day_pay1));
                }

    // If End Date Payment is Sunday 
                $mw_cost =  $money_owne_1;
                if($type_of_payment == 2){
                    $mwr = $money_owne * $interest_of_owne_precent;
                }else{
                    $mwr = $owncoustpay * $interest_of_owne_precent;
                }
               
                // if($data->mfi_repayment_schedul_type_id == 2){	
                //     $totat_pay = $money_cost * $interest * ((30 * 12)/360);
                // }else{
                //     $totat_pay = $owncoustpay * $interest * ((30 * 12)/360);
                // }

                if($i == 0){
                    $dat_payf = new DateTime($date_for_open);
                    $date_generat = new DateTime($date_for_payments);
                    $dat_pay_d = $date_generat->getTimestamp() - $dat_payf->getTimestamp();
                    $date_generat1 = $dat_pay_d / (60 * 60 *24);
                        if($dura_type == 'day'){
                            $mwr_in = $mwr *  ( $date_generat1 /30);
                        }elseif($dura_type == 'week'){
                            $mwr_in = $mwr *  ( $date_generat1 /7);
                        }elseif($dura_type == '2week'){
                            $mwr_in = $mwr *  ( $date_generat1 /14);
                        }else{
                            $mwr_in= $mwr ;
                        }
                        $request->session()->put('num_of_date',$date_generat1);
                        $request->session()->put('day_last',$day_pay1);
                }else{
                    $get_last_num = $request->session()->get('num_of_date');
                    $get_last_day = $request->session()->get('day_last');
                    
                    $dat_payf = new DateTime($get_last_day);
                    $date_generat = new DateTime($day_pay1);
                // $date_generat = new DateTime($datpay);
                    $dat_pay_d = $date_generat->getTimestamp() - $dat_payf->getTimestamp();
                    $date_generat1 = $dat_pay_d / (60 * 60 *24);

                        if($dura_type == 'day'){
                            $mwr_in = $mwr *  ( $date_generat1 /30);
                        }elseif($dura_type == 'week'){
                            $mwr_in = $mwr *  ( $date_generat1 /7);
                        }elseif($dura_type == '2week'){
                            $mwr_in = $mwr *  ( $date_generat1 /14);
                        }else{
                            $mwr_in= $mwr ;
                        }
                        $request->session()->put('num_of_date',$date_generat1);
                        $request->session()->put('day_last',$day_pay1);
                }

                $owncoustpay -= $mw_cost;
                if($dura_real -1 == $i){
                    if($owncoustpay > 0){
                        $mw_cost = $mw_cost + $owncoustpay;
                        $owncoustpay = 0 ;
                    }else{
                        $mw_cost = $mw_cost - trim($owncoustpay,'-');
                        $owncoustpay = 0 ;
                    }
                }else{
                   
                    if($owncoustpay > 0){
                        $mw_cost = $mw_cost;
                        $owncoustpay = $owncoustpay;
                    }else{
                        $mw_cost = $mw_cost - trim($owncoustpay,'-');
                        $owncoustpay = 0 ;
                    }
                }
                $payment_total = $mwr_in + $mw_cost;
                $pay_total_all += $payment_total;
                $total_interest_pay += $mwr_in;
                $total_cost_pay += $mw_cost;
                $day_1 = date('l',strtotime($day_pay1));
                $places = -2;
                $mult = pow(10, abs($places));
                if($currency == 2){
                    $mw_cost =  ceil(trim($mw_cost,'-')/$mult)*$mult;
                    $mwr_in =  ceil(trim($mwr_in,'-')/$mult)*$mult;
                    $payment_total =  ceil(trim($payment_total,'-')/$mult)*$mult;
                    $owncoustpay =  ceil(trim($owncoustpay,'-')/$mult)*$mult;
                }else{
                    $mw_cost = round(trim($mw_cost,'-'),2); 
                    $mwr_in = round(trim($mwr_in,'-'),2); 
                    $payment_total = round(trim($payment_total,'-'),2); 
                    $owncoustpay = round(trim($owncoustpay,'-'),2); 
                }
                // echo "<tr>";
                // echo "<td>".$day_1."</td>";
                // echo "<td>".$get_last_day."</td>";
                // echo "<td>".$mw_cost."</td>";
                // echo "<td>".$mwr_in."</td>";
                // echo "<td>".$payment_total."</td>";
                // echo "<td>".$owncoustpay."</td>";
                // echo "</tr>";
                $data_sc_timesheet = [
                        'cs_schedules_id' => $id,
                        'total_pay_cost' => $mw_cost,
                        'total_pay_interest' => $mwr_in,
                        'total_payment' => $payment_total,
                        'total_pay_cost_owe' => $owncoustpay,
                        'date_payment' => $day_pay1,
                        'num_date_use' => $date_generat1,
                        'status' => 0,
                        'status_for_pay' =>0,
                        'active' => 1,
                        'is_finish' => 0,
                        'description' => null
                ];
                
                Cs_Schedule_Timesheet::insert($data_sc_timesheet);
            
            }

            // echo "<tr>";
            // echo "<td colspan=2>Total</td>";
            // echo "<td >".$total_cost_pay."</td>";
            // echo "<td>".$total_interest_pay."</td>";
            // echo "<td>".$pay_total_all."</td>";
            // echo "<td></td>";
            // echo "</tr>"; 
            // echo "</table>"; 
            $request->session()->forget('num_of_date');
            $request->session()->forget('day_last');   
            $schedule_number =  Auth::user()->branch->brand_name_short."".date('Y')."".date('m')."".date('d')."".$id;
            $data_schedule = [
                                'id'=> $id,
                                'client_id' => $data->client_id,
                                'schedule_number' => $schedule_number,
                                'sale_id' => $data->sale_id,
                                'branch_id' => Auth::user()->branch_id,
                                'approval_id' => $data->id,
                                'user_id' => Auth::user()->id,
                                'staff_id' => $data->staff_id,
                                'is_finish' => 0,
                                'currency_id' => $data->currency,
                                'money_owne_cost' => round($total_cost_pay,2),
                                'money_owne_interest' => round($total_interest_pay,2),
                                'money_owne_total_pay' => round($pay_total_all,2),
                                'status' => 1,
                                'status_for_pay' => 0,
                                'active' => 1,
                                'deleted' => 1,
                                'created_at' => date('Y-m-d h:m:s'),
                                'updated_at' => date('Y-m-d h:m:s')
                            
                            ];
               if(count($data->schedule) >0){            
                    Cs_Schedule::where('id','=',$id)->update($data_schedule);
                    $schedule_id = $id;
               }else{
                    $schedule_id = Cs_Schedule::insertGetId($data_schedule);
               }
               
              DB::commit();   
                if($schedule_id != 0){
                    $message = [
                        'msg_show'=> "Successful Generate Schedule"
                    ];
                }else{

                    $message = [
                        'msg_show'=> "Error Generate Schedule"
                    ];   
                }
                return response()->json($message);
            } catch (\Exception $e) {
                DB::rollBack();

                $data = [
                    'message'    => 'Error Edit  Please Check: '.$e->getMessage(),
                    'alert-type' => 'error',
                ];
                return response()->json($data);

        }   
    }


}
