<?php

namespace App\Http\Controllers;
use Auth;
use DB;
use Session;
use Illuminate\Http\Request;
use App\Http\Requests\ValidRequest;
use App\Cs_Client;
use App\Sub_client;
use App\ThirdClient;
use Validator;
use App;

class SubClientController extends Controller
{
    public function list_sub_client(Request $request){
        $title = "ការគ្រប់គ្រងអ្នកធានា";
        $client_id = $request->client_id;
        $cli_dat = Cs_Client::where('id',$client_id)->with('sub_clients')->get();
        foreach($cli_dat as $cc){
            if(count($cc->sub_clients) > 0){
                return view('credit_sale.client_management.sub_client_list',compact('title','client_id'));
            }else{
                $title = "Create Sub Client";
                return Redirect('accounts/sub_client/create_sub_client'.'?client_id='.$client_id);
            }
        }
    }
    public function list_json_sub_client(Request $request){
        
        $client_id = $request->client_id;
        if(Auth::user()->groups->first()->hasPermission(['create-user-groups'])){
            $cli_data = Cs_Client::where('id','=',$client_id)
                                ->with('sub_clients')
                                ->paginate(2);
        }else{
            $cli_data = Cs_Client::where('id','=',$client_id)
                                ->with('sub_clients')
                                ->paginate(2);
        }
        return Response()->json($cli_data);
        
    }

   public function get_create_sub_client(Request $request){
     $title = "Create Sub Client";
     $client_id = $request->client_id;
     $cli_type = Cs_Client::where('id',$client_id)->first();
     $item = $request->item;
     $sub_client_id = $request->sub_client_id;
    return view('credit_sale.client_management.create_sub_client',compact('title','cli_type','client_id','item','sub_client_id'));
  }
  public function ValidationForm(){
    $messages = [
        'client_first_name_kh.required'  => 'The First Name is required.',
        'client_second_name_kh.required' => 'The Second Name is required.',
        'client_gender.required'         => 'Please Select Gender.',
        'day.required'                   => 'Please Select Date of Birth',
        'client_nationality1.required'   => 'Nationality is required.',
        'client_type_idcard1.required'   => 'Please select ID card type.',
        'client_phone.required'                 => 'Phone is required.',
        'client_phone.unique'                 => 'Phone number is chose, Please make other.',
        'client_idcard_no.required'          => 'ID number is required.',
        'client_idcard_no.unique'          => 'ID number is already taken.',
    ];
    $messages_kh = [
        'client_first_name_kh.required'  => 'សូមបញ្ចូលត្រកូលភាសាខ្មែរ',
        'client_second_name_kh.required' => 'សូមបញ្ចូលឈ្មោះភាសាខ្មែរ',
        'client_gender.required'         => 'សូមជ្រើសរើសភេទ',
        'day.required'                   => 'សូមជ្រើសរើសថ្ងៃខែឆ្នាំកំណើត',
        'client_nationality1.required'   => 'សូមជ្រើសរើសសញ្ជាតិ',
        'client_type_idcard1.required'   => 'សូមជ្រើសរើសប្រភេទអត្តសញ្ញាណប័ណ្ណ',
        'client_phone.required'          => 'សូមបញ្ចូលលេខទូរស័ព្ទ',
        'client_phone.unique'                 => 'លេខទូរស័ព្ទ ត្រូវបានជ្រើសរើសរួចហើយ',
        'client_idcard_no.required'          => 'សូមបញ្ចូលលេខកូដអត្តសញ្ញាណប័ណ្ណ',
        'client_idcard_no.unique'          => 'លេខកូដអត្តសញ្ញាណប័ណ្ណ ត្រូវបានជ្រើសរើសរួចហើយ',
    ];
    $locale = App::getLocale();
    $validator = Validator::make(Request()->all(), [
                                    'client_first_name_kh' => 'required',
                                    'client_second_name_kh' => 'required',
                                    'client_gender'  => 'required',
                                    'day' => 'required', 
                                    'client_nationality1' => 'required',
                                    'client_type_idcard1' => 'required',
                                    'client_phone'  => 'required|unique:cs_sub_clients,client_phone|max:60',
                                    'client_idcard_no' => 'required|unique:cs_sub_clients,client_idcard_no|max:60',
                                ], $locale=='en'?$messages:$messages_kh);
    return $validator;
  }
   public function post_sub_client(Request $request) {
    
        DB::beginTransaction();
        
        $validator = $this->ValidationForm();
        
        if($request->hasFile('client_upload_image')){
        
            $destinationPath = "Account/images";
            $im_cl_up = $request->file('client_upload_image');
            $fileName = $im_cl_up->getClientOriginalName();
            $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
            $adimage = $request->get('Account/images',$fileName);
            
        }else{
            $up_cl_im =" ";
            $adimage ="";
        }
        $address = $adimage;
        $result['longitude'] = $request->client_longitidute;
        $result['latitude'] = $request->client_lutidued;

        $client_first_name_en = strtolower($request->client_first_name_en);
        $client_second_name_en = strtolower($request->client_second_name_en);
        if ($request->client_nationality1 == 1) {
            $cnation = $request->client_nationality1;
        }elseif ($request->client_nationality1 == 2) {
            $cnation = $request->client_nationality2;
        }

        if ($request->client_type_idcard1 < 6) {
            $ctype_idcard = $request->client_type_idcard1;
        }elseif ($request->client_type_idcard1 == 6) {
            $ctype_idcard = $request->client_type_idcard2;
        }

        if ($request->client_aprovel_idcard_by1 == 1) {
            $caprovel_idcard_by = $request->client_aprovel_idcard_by1;
        }elseif ($request->client_aprovel_idcard_by1 == 2) {
            $caprovel_idcard_by = $request->client_aprovel_idcard_by2;
        }
        
        //date dob
        $day = $request->day;
        $month = $request->month;
        $year = $request->year;
        $date_dob = $year.'-'.$month.'-'.$day ;
        
        // district other 
        if($request->client_district == "ផ្សេងៗ"){

            $client_district = $request->other_district;
        }else{
            $client_district = $request->client_district;
        }

        if($request->client_commune == "ផ្សេងៗ"){
            $client_commune = $request->other_commune;
        }else{
            $client_commune = $request->client_commune;
        }

        if($request->client_village == "ផ្សេងៗ"){
            $client_village = $request->other_village;
        }else{
            $client_village = $request->client_village;
        }
        if($request->client_create_date_idcard){
            $client_create_date = date('Y-m-d' ,strtotime($request->client_create_date_idcard));
        }else{
            $client_create_date = " ";
        }

        $identify_day = $request->identify_day;
        $identify_month = $request->identify_month;
        $identify_year = $request->identify_year;
        $identify_date = $identify_year.'-'.$identify_month.'-'.$identify_day ;
        if($validator->failed()){
            $id_num = "";
            $phone = "";
            
        }else{
            $phone = $request->client_phone;
            $id_num = $request->client_idcard_no;
        }
        $acount_p = [
            'branch_id' => Auth::user()->branch_id,
            'client_first_name_kh' => $request->client_first_name_kh,
            'client_second_name_kh' => $request->client_second_name_kh,
            'client_first_name_en' => ucwords($client_first_name_en),
            'client_second_name_en' => ucwords($client_second_name_en),
            'client_name_kh' => $request->client_first_name_kh.' '.$request->client_second_name_kh,
            'client_name_en' => ucwords($client_first_name_en).' '.ucwords($client_second_name_en),
            'client_gender'  => $request->client_gender,
            'client_dob'  => $date_dob,
            'client_nationality' => $cnation,
            'client_idcard_no' => '',
            'client_type_idcard' => $ctype_idcard,
            'client_identify_date' => $identify_date,
            'client_aprovel_idcard_by' => $caprovel_idcard_by,
            'client_identify_by' => $request->approve_by_name,
            'client_phone'    => $phone,
            'client_house_num'=> $request->client_house_num,
            'client_group_num'=> $request->client_group_num,
            'client_st_num'=> $request->client_st_num, 
            'client_village'=> $client_village,
            'client_commune' => $client_commune,
            'client_district' => $client_district,
            'client_province' => $request->client_province,
            'client_job' => $request->client_job,
            'client_address_job' => $request->client_address_job,
            'client_upload_image' => $adimage,
            'client_note' => $request->client_note,
            'b_status_live' => 0,
            'client_lavel' => 0,
            'client_lutidued' =>  $result['latitude'] ,
            'client_longitidute' => $result['longitude'],
            'user_id'  => Auth::id(),
            'created_at' => date('Y-m-d h:m:s'),
        ];

        $acc_pp = Sub_client::insertGetId($acount_p);

        $acc_cl = [
            'client_id' => $request->client_id,
            'sub_client_id' => $acc_pp,
        ];
        DB::table('cs_client_andsub_clients')->insert($acc_cl);

        $acount_p = [
            'ip_log'=> $request->ip(),
            'active'=> "បង្កើតអ្នកធានា",
            'user_id'=> Auth::user()->id,
            'status'=> '2',
            'what_id' => $acc_pp,
            'method' => 'customer',
            'create_date' => date('Y-m-d h:m:s')
        ];
        DB::table('cs-history-logs')->insert($acount_p); 
        if($request->hasFile('image_uploade_id_familly')){
            $imagp = $request->file('image_uploade_id_familly');
            foreach($imagp as $key => $value) {
                $fileName = $value->getClientOriginalName();  
                $uplades = $value->move($destinationPath, $fileName);
                $dat_image = [
                'image_value' =>$request->get('Account/images',$fileName),
                'meta_value' => 'cs_sub_client_id',
                'dis' => $acc_pp
            ];
            
                DB::table('cs_image_data')->insert($dat_image);
            }
        }

        if($request->third_id == 1){
            $third_client_first_name_en = strtolower($request->third_client_first_name_en);
            $third_client_second_name_en = strtolower($request->third_client_second_name_en);
            if ($request->third_client_nationality1 == 1) {
                $third_cnation = $request->third_client_nationality1;
            }elseif ($request->third_client_nationality1 == 2) {
                $third_cnation = $request->third_client_nationality2;
            }

            if ($request->third_client_type_idcard1 < 6) {
                $third_ctype_idcard = $request->third_client_type_idcard1;
            }elseif ($request->third_client_type_idcard1 == 6) {
                $third_ctype_idcard = $request->third_client_type_idcard2;
            }

            if ($request->third_client_aprovel_idcard_by1 == 1) {
                $third_caprovel_idcard_by = $request->third_client_aprovel_idcard_by1;
            }elseif ($request->third_client_aprovel_idcard_by1 == 2) {
                $third_caprovel_idcard_by = $request->third_client_aprovel_idcard_by2;
            }
            // Third DOB
            $third_day = $request->third_day;
            $third_month = $request->third_month;
            $third_year = $request->third_year;
            $third_date_dob = $third_year.'-'.$third_month.'-'.$third_day ;
            // Third Identify Date
            $third_identify_day = $request->third_identify_day;
            $third_identify_month = $request->third_identify_month;
            $third_identify_year = $request->third_identify_year;
            $third_identify_date = $third_identify_year.'-'.$third_identify_month.'-'.$third_identify_day ;
            $third_data = [
                            'client_id' => $request->client_id,
                            'sub_client_id' => $acc_pp,
                            'third_kh_name_first' => $request->third_client_first_name_kh,
                            'third_kh_name_last' => $request->third_client_second_name_kh,
                            'third_en_name_first' => ucwords($third_client_first_name_en),
                            'third_en_name_last' => ucwords($third_client_second_name_en),
                            'third_kh_username' => ucwords($third_client_first_name_en).' '.ucwords($third_client_second_name_en),
                            'third_en_username' => $request->third_client_first_name_kh.' '.$request->third_client_second_name_kh,
                            'third_gender' => $request->third_client_gender,
                            'third_dob' => $third_date_dob,
                            'third_identify_date' => $third_identify_date,
                            'third_nationality' => $third_cnation,
                            'third_identify_num' => $request->third_identify_num,
                            'third_identify_type' => $third_ctype_idcard,
                            'third_identify_by' => $third_caprovel_idcard_by,
                            'third_home_num' => $request->third_client_house_num,
                            'third_group_num' => $request->third_client_group_num,
                            'third_street_num' => $request->third_client_st_num,
                            'third_phone' => $request->third_phone,
                            'third_job' => $request->third_client_job,
                            'third_place_job' => $request->third_client_address_job,
                            
                            'third_village'=> $client_village,
                            'third_commune' => $client_commune,
                            'third_district' => $client_district,
                            'third_province' => $request->client_province,
                            'user_id'  => Auth::id(),
                        ];
            ThirdClient::insert($third_data);
        }
        
        if ($validator->fails()) {
            DB::rollBack();
            return response()->json(['error'=>$validator->errors()->all()]);
        }
        
        $mess = [
                'msg_show' => '<div class="alert alert-success"><p>Success! Create Client</p></div>'
            ];
        DB::commit();
        return response()->json($mess);
    }
    public function show_sub_client($id, Request $request) {
        $client_id = $request->client_id;
        $title = "ការបង្ហាញព័ត៌មានអ្នកធានា";
        $datas = Sub_client::find($id);
        $subclient_id = $id;
        return view('credit_sale.client_management.show_sub_client',compact('title','subclient_id','datas','client_id','all_sub_cl'));
    }
    public function get_edit_sub_client(Request $request){
        $title = "Edit Sub Client";
        $client_id = $request->client_id;
        $item = $request->item;
        $sub_client_id = $request->sub_client_id;
       return view('credit_sale.client_management.edit_sub_client',compact('title','client_id','item','sub_client_id'));
    }
    public function get_json_edit_sub_client(Request $request ,$id){

        $data = Sub_client::with(['third_clients'])->find($id);
        $relate_file = DB::table('cs_image_data')->where('dis','=',$id)->where('meta_value','=','cs_sub_client_id')->get();
          $data_source = [
            'sub_client'=> $data,
            'relate_file' => $relate_file
          ];  
        return response()->json($data_source);
    }
    public function post_edit_sub_client(Request $request, $id) {
        $image = Sub_client::find($id);
        // try {
           DB::beginTransaction();
        $validator = $this->ValidationForm($request);
               
                   if($request->hasFile('client_upload_image')){
                   
                       $destinationPath = "Account/images";
                       $im_cl_up = $request->file('client_upload_image');
                       $fileName = $im_cl_up->getClientOriginalName();
                       $up_cl_im = $im_cl_up->move($destinationPath,$fileName);
                       $adimage = $request->get('Account/images',$fileName);
                       
                   }else{
                       $up_cl_im =" ";
                       $adimage = $image->client_upload_image;
                   }
                   $address = $adimage;
                   $result['longitude'] = $request->client_longitidute;
                   $result['latitude'] = $request->client_lutidued;
       
                   $client_first_name_en = strtolower($request->client_first_name_en);
                   $client_second_name_en = strtolower($request->client_second_name_en);
                   if ($request->client_nationality1 == 1) {
                       $cnation = $request->client_nationality1;
                   }elseif ($request->client_nationality1 == 2) {
                       $cnation = $request->client_nationality2;
                   }
       
                   if ($request->client_type_idcard1 < 6) {
                       $ctype_idcard = $request->client_type_idcard1;
                   }elseif ($request->client_type_idcard1 == 6) {
                       $ctype_idcard = $request->client_type_idcard2;
                   }
       
                   if ($request->client_aprovel_idcard_by1 == 1) {
                       $caprovel_idcard_by = $request->client_aprovel_idcard_by1;
                   }elseif ($request->client_aprovel_idcard_by1 == 2) {
                       $caprovel_idcard_by = $request->client_aprovel_idcard_by2;
                   }
                   
                   //date dob
                   $day = $request->day;
                   $month = $request->month;
                   $year = $request->year;
                   $date_dob = $year.'-'.$month.'-'.$day ;
                   
                   // district other 
                   if($request->client_district == "ផ្សេងៗ"){
       
                       $client_district = $request->other_district;
                   }else{
                       $client_district = $request->client_district;
                   }
       
                   if($request->client_commune == "ផ្សេងៗ"){
                       $client_commune = $request->other_commune;
                   }else{
                       $client_commune = $request->client_commune;
                   }
       
                   if($request->client_village == "ផ្សេងៗ"){
                       $client_village = $request->other_village;
                   }else{
                       $client_village = $request->client_village;
                   }
                   if($request->client_create_date_idcard){
                       $client_create_date = date('Y-m-d' ,strtotime($request->client_create_date_idcard));
                   }else{
                       $client_create_date = " ";
                   }

                    $identify_day = $request->identify_day;
                    $identify_month = $request->identify_month;
                    $identify_year = $request->identify_year;
                    $identify_date = $identify_year.'-'.$identify_month.'-'.$identify_day ;
                   $acount_p = [
                       'branch_id' => Auth::user()->branch_id,
                       'client_first_name_kh' => $request->client_first_name_kh,
                       'client_second_name_kh' => $request->client_second_name_kh,
                       'client_first_name_en' => ucwords($client_first_name_en),
                       'client_second_name_en' => ucwords($client_second_name_en),
                       'client_name_kh' => $request->client_first_name_kh.' '.$request->client_second_name_kh,
                       'client_name_en' => ucwords($client_first_name_en).' '.ucwords($client_second_name_en),
                       'client_gender'  => $request->client_gender,
                       'client_dob'  => $date_dob,
                       'client_nationality' => $cnation,
                       'client_idcard_no' => $request->identify_num,
                       'client_identify_date' => $identify_date,
                       'client_type_idcard' => $ctype_idcard,
                       'client_aprovel_idcard_by' => $caprovel_idcard_by,
                       'client_identify_by' => $request->approve_by_name,
                       'client_phone'    => $request->phone,
                       'client_house_num'=> $request->client_house_num,
                       'client_group_num'=> $request->client_group_num,
                       'client_st_num'=> $request->client_st_num, 
                       'client_village'=> $client_village,
                       'client_commune' => $client_commune,
                       'client_district' => $client_district,
                       'client_province' => $request->client_province,
                       'client_job' => $request->client_job,
                       'client_address_job' => $request->client_address_job,
                       'client_upload_image' => $adimage,
                       'client_note' => $request->client_note,
                       'b_status_live' => 0,
                       'client_lavel' => 0,
                       'client_lutidued' =>  $result['latitude'] ,
                       'client_longitidute' => $result['longitude'],
                       'user_id'  => Auth::id(),
                       'created_at' => date('Y-m-d h:m:s'),
                   ];
       
                   $acc_pp = Sub_client::where('id',$id)->update($acount_p);
       
                //    $acc_cl = [
                //        'client_id' => $request->client_id,
                //        'sub_client_id' => $acc_pp,
                //    ];
                //    DB::table('cs_client_andsub_clients')->insert($acc_cl);
       
                   $acount_p = [
                       'ip_log'=> $request->ip(),
                       'active'=> "បង្កើតអ្នកធានា",
                       'user_id'=> Auth::user()->id,
                       'status'=> '2',
                       'what_id' => $acc_pp,
                       'method' => 'customer',
                       'create_date' => date('Y-m-d h:m:s')
                   ];
                   DB::table('cs-history-logs')->insert($acount_p);
                   $destinationPath = "Account/images";
                   if($request->hasFile('image_uploade_id_familly')){
                    
                    $imagp = $request->file('image_uploade_id_familly');
                    foreach($imagp as $key => $value) {
                        $fileName = $value->getClientOriginalName();
                          
                        $uplades = $value->move($destinationPath, $fileName);
                        $dat_image = [
                          'image_value' =>$request->get('Account/images',$fileName),
                          'meta_value' => 'cs_sub_client_id',
                          'dis' => $id,
                      ];
                     
                        DB::table('cs_image_data')->insert($dat_image);
                    }
                  }

                // Third Client Update
                //   if($request->third_id == 1){
                    $third_client_first_name_en = strtolower($request->third_client_first_name_en);
                    $third_client_second_name_en = strtolower($request->third_client_second_name_en);
                    if ($request->third_client_nationality1 == 1) {
                        $third_cnation = $request->third_client_nationality1;
                    }elseif ($request->third_client_nationality1 == 2) {
                        $third_cnation = $request->third_client_nationality2;
                    }
        
                    if ($request->third_client_type_idcard1 < 6) {
                        $third_ctype_idcard = $request->third_client_type_idcard1;
                    }elseif ($request->third_client_type_idcard1 == 6) {
                        $third_ctype_idcard = $request->third_client_type_idcard2;
                    }
        
                    if ($request->third_client_aprovel_idcard_by1 == 1) {
                        $third_caprovel_idcard_by = $request->third_client_aprovel_idcard_by1;
                    }elseif ($request->third_client_aprovel_idcard_by1 == 2) {
                        $third_caprovel_idcard_by = $request->third_client_aprovel_idcard_by2;
                    }
                    $third_day = $request->third_day;
                    $third_month = $request->third_month;
                    $third_year = $request->third_year;
                    $third_date_dob = $third_year.'-'.$third_month.'-'.$third_day ;
                    // Third Identify Date
                    $third_identify_day = $request->third_identify_day;
                    $third_identify_month = $request->third_identify_month;
                    $third_identify_year = $request->third_identify_year;
                    $third_identify_date = $third_identify_year.'-'.$third_identify_month.'-'.$third_identify_day;
                    $third_data = [
                                    // 'client_id' => $request->client_id,
                                    // 'sub_client_id' => $acc_pp,
                                    'third_kh_name_first' => $request->third_client_first_name_kh,
                                    'third_kh_name_last' => $request->third_client_second_name_kh,
                                    'third_en_name_first' => ucwords($third_client_first_name_en),
                                    'third_en_name_last' => ucwords($third_client_second_name_en),
                                    'third_kh_username' => ucwords($third_client_first_name_en).' '.ucwords($third_client_second_name_en),
                                    'third_en_username' => $request->third_client_first_name_kh.' '.$request->third_client_second_name_kh,
                                    'third_gender' => $request->third_client_gender,
                                    'third_dob' => $third_date_dob,
                                    'third_nationality' => $third_cnation,
                                    'third_identify_num' => $request->third_identify_num,
                                    'third_identify_date' => $third_identify_date,
                                    'third_identify_type' => $third_ctype_idcard,
                                    'third_identify_by' => $third_caprovel_idcard_by,
                                    'third_home_num' => $request->third_client_house_num,
                                    'third_group_num' => $request->third_client_group_num,
                                    'third_street_num' => $request->third_client_st_num,
                                    'third_phone' => $request->third_phone,
                                    'third_job' => $request->third_client_job,
                                    'third_place_job' => $request->third_client_address_job,
                                    
                                    'third_village'=> $client_village,
                                    'third_commune' => $client_commune,
                                    'third_district' => $client_district,
                                    'third_province' => $request->client_province,
                                    // 'user_id'  => Auth::id(),
                                ];
                    if($image->third_clients){
                        ThirdClient::where("id",$request->third_id)->update($third_data);
                    }else{
                        ThirdClient::insert($third_data + ['client_id' => $request->client_id,'sub_client_id' => $id,'user_id'  => Auth::id()]);
                    }

                    if ($validator->fails()) {
                        DB::rollBack();
                        return response()->json(['error'=>$validator->errors()->all()]);
                    }
                // }
                //    
                   $mess = [
                       'msg_show' => '<div class="alert alert-success"><p>Success! Create Client</p></div>'
                   ];
                   DB::commit();
                   return response()->json($mess);
               
                // } catch (\Exception $e) {
                //    DB::rollBack();
       
                //    return Redirect()->to('accounts/sub_client/show_sub_client')->with([
                //        'message'    => 'Error creating new relationship: '.$e->getMessage(),
                //        'alert-type' => 'error',
                //    ]);
            //    }
        }
        public function delete_sub_client(Request $request, $id){
            $acc_pp = Sub_client::where('id','=',$id)->delete();
            if($acc_pp ){
                 DB::table('cs_image_data')->where('meta_value','=','cs_sub_client_id')->where('dis','=',$id)->delete();
                 DB::table('cs_client_andsub_clients')->where('sub_client_id','=',$id)->delete();
                 DB::table('cs_third_client')->where('sub_client_id',$id)->delete();
                  $acount_p = [
                    'ip_log'=> $request->ip(),
                    'active'=> "កែរប្រែរអ្នកធានា",
                    'user_id'=> Auth::user()->id,
                    'status'=> '2',
                    'what_id' => $id,
                    'method' => 'customer personal',
                    'create_date' => date('Y-m-d h:m:s')
                ];
                DB::table('cs-history-logs')->insert($acount_p); 
    
                $mess = [
                  'msg_show' => '<div class="alert alert-success"><p>Success! Deleted Client . Client ID '.$id.' has delete.</p></div>'
                ];
            }else{
                $mess = [
                  'msg_show' => '<div class="alert alert-danger"><p>Error! Please check input again</p></div>'
                ];
            }
    
            return response()->json($mess);
       }
    public function all_sub_client() {
        $title = "អ្នកធានា";
        return view('credit_sale.client_management.all_sub_client');
    }
    public function all_sub_client_json(Request $request){
        $all_scl = Sub_client::all();
        return response()->json($all_scl);
    }
}
