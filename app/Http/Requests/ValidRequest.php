<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class ValidRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'client_first_name_kh' => 'required',
            'client_second_name_kh' => 'required',
            'client_gender'  => 'required',
            'day' => 'required', 
            'client_nationality1' => 'required',
            'client_type_idcard1' => 'required',
            'phone'  => 'required|unique:cs_clients|max:60',
            'identify_num' => 'required|unique:cs_clients|max:60',
        ];
    }
}
