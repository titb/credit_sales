<?php

namespace App\Http\Middleware;

use Closure;

class GroupMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
     public function handle($request, Closure $next)

     {

        if($request->user() === null){

         return back()->with('keyerror','You dont have Permission');

        }

        $actions =$request->route()->getAction();

        $groups = isset($actions['groups']) ? $actions['groups'] : null;



        if($request->user()->hasAnyGroup($groups) || !$groups){

             return $next($request);

        }

      return back()->with('keyerror','You dont have Permission');

  }
}
