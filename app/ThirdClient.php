<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ThirdClient extends Model
{
    protected $table = "cs_third_client";
    public function clients(){
        return $this->belongsTo('App\Cs_Client','client_id');
    }
    public function sub_clients(){
        return $this->belongsTo('App\Sub_client', 'sub_client_id');
    }
}
