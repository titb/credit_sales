<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Cs_Purchase_Order_Item extends Model
{
    protected $table = "cs_purchase_order_item"; 

    public function item(){
        return $this->belongsTo('App\Cs_Items','item_id');
    }

}
