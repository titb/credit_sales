<?php
return [
    'product' => 'Product',
    'supplier' => 'Supplier',
    'productmanagement' => 'Product Management',
    'brand' => 'Brand',
    'product-type' => 'Product Type'
];