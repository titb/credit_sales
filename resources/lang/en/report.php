<?php
return [
    'report' => 'Report',
    'product-report' => 'Product Report',
    'client-report' => 'Client Report',
    'sale-credit-report' => 'Sale by Credit Report',
    'direct-payment-report' => 'Direct Payment Report',
    'payment-back-report' => 'Payment Back Report',
    'total-payment-back-report' => 'Total Payment Back Report',
    'schedule-report' => 'Payment Schedule Report',
    'sale-cash-report' => 'Sale by Cash Report',
    'credit-sale-report' => 'Credit Sale Report',
    'receiving-product-report' => 'Receiving Product Report',
    'purchase-order-report' => 'Purchase Order Report',
];