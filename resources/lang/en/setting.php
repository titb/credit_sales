<?php
return [
    'setting' => 'Setting',
    'language' => 'Language',
    'user' => 'User',
    'user-management' => 'User Management',
    'permission' => 'Permission',
    'branch' => 'Branch',
    'interest-rate' => 'Interest Rate',
    'new-interest-rate' => 'New Interest Rate',
    'holiday' => 'Holiday',
    'exchange-rate' => 'Exchange Rate',
    'penalty' => 'Penalty',
    'position' => 'Position',
    'module-interest' => 'Module Interest',
    'history-log' => 'User History Log',
    'currency' => 'Currency',
    'rate-percent' => 'Rate Percent',
    'configure' => 'Configure',
];