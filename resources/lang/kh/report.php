<?php
return [
    'report' => 'របាយការណ៍',
    'product-report' => 'របាយការណ៍លម្អិតពីទំនិញ',
    'client-report' => 'របាយការណ៍លម្អិតពីអតិថិជន',
    'sale-credit-report' => 'របាយការណ៍លម្អិតពីការលក់ដោយឥណទាន',
    'direct-payment-report' => 'របាយការណ៍លម្អិតពីការលក់ដោយរការទូទាត់ដោយផ្ទាល់',
    'payment-back-report' => 'របាយការណ៍លម្អិតពីសង់ប្រាក់ត្រលប់',
    'total-payment-back-report' => 'របាយការណ៍សរុបពីសង់ប្រាក់ត្រលប់',
    'schedule-report' => 'របាយការណ៍លម្អិតពីកាលវិភាគសងប្រាក់',
    'sale-cash-report' => 'របាយការណ៍លម្អិតពីការទិញជាសាច់ប្រាក់',
    'credit-sale-report' => 'របាយការណ៍លម្អិតពីការទិញបង់រំលស់',
    'receiving-product-report' => 'របាយការណ៍លម្អិតពីការទទួលទំនិញ',
    'purchase-order-report' => 'របាយការណ៍លម្អិតពីការបញ្ជារទិញ',
];