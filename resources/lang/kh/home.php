<?php
return [
    'Home' => 'ទំព័រដំបូង',
    'currency' => 'រូបីយ​ប័ណ្ណ',
    'user-profile' => 'ព័ត៌មានអ្នកប្រើប្រាស់',
    'change-pass' => 'ប្តូរលេខកូដសំងាត់',
    'logout' => 'ចាក់ចេញ',
];