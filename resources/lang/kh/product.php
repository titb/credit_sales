<?php
return [
    'product' => 'ផលិតផល',
    'supplier' => 'អ្នកផ្គត់ផ្គង់',
    'productmanagement' => 'កាគ្រប់គ្រង់ផលិតផល',
    'brand' => 'ម៉ាកផលិតផល',
    'product-type' => 'ប្រភេទផលិតផល'
];