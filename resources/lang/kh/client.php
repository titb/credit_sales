<?php
return [
    'client-management' => 'ការគ្រប់គ្រងអតិថិជន',
    'client-type-management' => 'ការគ្រប់គ្រងប្រភេទអតិថិជន',
    'guaranteed-management' => 'ការគ្រប់គ្រងអ្នកធានា',
    // =========> Start editing
    'personal-client' => 'អតិថិជនជាលក្ខណៈបុគ្គល',
    // Search
    'search-name' => 'ឈ្មោះ ខ្មែរ/អង់គ្លេស',
    'search-phone' => 'លេខទូរស័ព្ទ',
    'search-id' => 'អត្តសញ្ញាណប័ណ្ណ',
    'search-dob' => 'ថ្ងៃខែឆ្នាំកំណើត',
    'search-gender' => 'ជ្រើស ភេទ',
    'search-boy' => 'ប្រុស',
    'search-girl' => 'ស្រី',
    'search-branch' => 'ជ្រើស សាខា',
    'search' => 'ស្វែងរក',
    'per-client-mgm' => 'ការគ្រប់គ្រងអតិថិជនជាលក្ខណៈបុគ្គល',
    'add-new' => 'បង្កើតថ្មី',
    'no' =>'ល.រ',
    'first-name' => 'គោត្តនាម',
    'last-name' => 'នាម',
    'address' => 'អាសយដ្ឋាន',
    'gender' => 'ភេទ',
    'phone' => 'លេខទូរសព្ទ',
    'id-number' => 'លេខសម្គាល់អត្តសញ្ញាណ',
    'dob' => 'ថ្ងៃខែឆ្នាំកំណើត',
    'status' => 'ស្ថានភាព',
    'action' => 'សកម្មភាព',
    'total-client' => 'អតិថិជនសរុប:',
    'btn-show' => 'លម្អិត',
    'btn-edit' => 'កែប្រែ',
    'btn-delete' => 'លុប',
    // Create Client
    'account' => 'គណនី',
    'add-account-new' => 'បង្កើតគណនីថ្មី',
    'client-type' => 'ប្រភេទអតិថិជន',
    'choose-branch' => 'ជ្រើសរើសសាខា',
    'create-first-name' => 'គោត្តនាម',
    'create-last-name' => 'នាម',
    'create-first-name-en' => 'First Name (EN)',
    'create-last-name-en' => 'Last Name (EN)',
    'create-gender' => 'ភេទ',
    'create-boy' => 'ប្រុស',
    'create-girl' => 'ស្រី',
    'create-dob' => 'ថ្ងៃខែឆ្នាំកំណើត',
    'create-date' => 'ថ្ងៃ',
    'create-month' => 'ខែ',
    'jan' => 'មករា',
    'feb' => 'កុម្ភៈ',
    'mar' => 'មីនា',
    'apr' => 'មេសា',
    'may' => 'ឧសភា',
    'jun' => 'មិថុនា',
    'jul' => 'កក្កដា',
    'aug' => 'សីហា',
    'sep' => 'កញ្ញា',
    'oct' => 'តុលា',
    'nov' => 'វិចិ្ឆកា',
    'dec' => 'ធ្នូ',
    'create-year' => 'ឆ្នាំ',
    'create-nationality' => 'សញ្ជាតិ',
    'check-kh' => 'ខ្មែរ',
    'check-other' => 'ផ្សេងៗ',
    'create-id' => 'ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ​',
    'create-id-type' => 'ប្រភេទប័ណ្ណ',
    'id-type-id' => 'អត្តសញ្ញាណប័ណ្ណ',
    'id-type-passport' => 'លិខិនឆ្លង់ដែន',
    'id-type-birth' => 'សំបុត្របញ្ជាក់កំណើត',
    'id-type-stay' => 'សៀវភៅសា្នក់នៅ',
    'id-type-family' => 'សៀវភៅគ្រួសារ',
    'id-type-other' => 'ផ្សេងៗ',
    'id-approve-by' => 'ចេញដោយ',
    'id-appr-interior' => 'ក្រសួងមហាផ្ទៃ',
    'id-appr-other' => 'ផ្សេងៗ',
    'id-appr-date' => 'ថ្ងៃចេញអត្តសញ្ញាណប័ណ្ណ',
    'id-appr-name' => 'ចេញដោយឈ្មោះ',
    'home-num' => 'អាសយដ្ឋានផ្ទះលេខ',
    'group-num' => 'ក្រុមទី',
    'street-mum' => 'ផ្លូវលេខ',
    'phone' => 'លេខទូរសព្ទទំនាក់ទំនង',
    'job' => 'មុខរបរ',
    'work-place' => 'ទីកន្លែងប្រកបមុខរបរ',
    'coordinate' => 'កូអរដោនេ',
    'image-upload' => 'ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ',
    'family-image' => 'ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ',
    'note' => 'កំណត់ចំណាំ',
    'submit' => 'បញ្ចូល',
    'back' => 'ត្រលប់',
];