@extends('credit_sale.layout.master')
@section('contend')



   

                    @if ($message = Session::get('success'))

                        <div class="alert alert-success">

                            <p>{{ $message }}</p>

                        </div>

                    @endif

                     @if ($message = Session::get('keyerror'))

                                <div class="alert alert-danger">

                                    <p>{{ $message }}</p>

                                </div>

                     @endif



                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                    
                            </div>

                        <div class="block-content collapse in">
                            <form action="{{ url('exchange_rate/'.$data->id.'/edit') }}" method="post" enctype="multipart/form-data">

                                    {{ csrf_field() }}
                                <div class="span12">
                                    <div class="span5">
                                        <label>ប្រាក់​ (គិតជា រៀល)</label>
                                        <input type="text" name="real" class="span12 m-wrap" value="{{number_format($data->real,2)}}" onkeypress="return forceNumber(event);" onkeyup="this.value=numberWithCommas(this.value);">
                                    </div>
                                    <div class="span5">
                                        <label>ប្រាក់​ (គិតជា ដុល្លា)</label>
                                        <input type="text" readonly name="us" class="span12 m-wrap" value="{{$data->us}}">
                                    </div>

                                    <div class="span2">
                                            <button type="submit" class="btn btn-primary" style="margin-top:23px;">កែប្រែ</button>
                                            <a href="{{url('exchange_rate')}}" class="btn btn-danger" style="margin-top:23px;">បោះបង់</a>
                                    </div>
                                </div>
                            </div>

                            </form>
                        </div>

                        <!-- /block -->

                    </div>

<script>
    //Can't type word
        function forceNumber(e) {
            var keyCode = e.keyCode ? e.keyCode : e.which;
            if((keyCode < 48 || keyCode > 58) && keyCode != 188) {
                return false;
            }
            return true;
        }

        function numberWithCommas(n){
            var n = n.replace(/,/g, "");
            var s= n.split('.')[1];
            (s) ? s="."+s : s="";
            n=n.split('.')[0];
            while(n.length>3){
                s=","+n.substr(n.length-3,3)+s;
                n=n.substr(0,n.length-3);
            }
            return n+s;
        }
</script>


@endsection