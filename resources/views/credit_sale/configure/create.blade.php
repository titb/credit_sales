@extends('credit_sale.layout.master')
@section('contend')  
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                            </div>
                            <div class="block-content collapse in">
                            @if (count($errors) > 0)
                                  <div class="alert alert-danger">
                                    <strong>Whoops!</strong> There were some problems with your input.<br><br>
                                    <ul>
                                      @foreach ($errors->all() as $error)
                                        <li>{{ $error }}</li>
                                      @endforeach
                                    </ul>
                                  </div>
                            @endif
                            @if ($message = Session::get('success'))
                                <div class="alert alert-success">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                             @if($message = Session::get('keyerror'))
                                <div class="alert alert-danger">
                                    <p>{{ $message }}</p>
                                </div>
                            @endif
                            <div class="span12">
                                <h3 class="cen_title"> Create Company Information </h3>
                                <legend></legend>
                            </div>
                                    <div class="span5">
                                        <label class="control-label">Company Logo</label>

                                        <div class="controls">
                                            <input type="text" name="name" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <input type="file" id="myFile">
                                    </div>

                                    <div class="span">
                                       Delete Logo: <input type="checkbox" id="myCheck">
                                    </div>

                                    <div class="span5">
                                        <label class="control-label" style="color: red;">Company Name</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>

                                    <div class="span5">
                                        <label class="control-label">Website</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                <!-- Taxes -->
                           <div class="span12">
                                <h3 class="cen_title">Taxes</h3>
                                <legend></legend>
                            </div>
                                    <div class="span">
                                       Price Include Tax: <input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Charge Tax on Receivings: <input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Use Tax Values At All: <input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                        <label class="control-label">Location</label>
                                    </div>
                                    <div class="span">
                                        <table id="myTable">
                                        <tbody class="Taxx">
                                          <tr>
                                            <td class="tax_name">Name</td>
                                            <td>Tax Name</td>
                                            <td>Tax Percent</td>
                                            <td>Cumulative</td>
                                            <td>Default</td>
                                            <td>Delete</td>
                                            <td>Add</td>
                                          </tr>
                                          </tbody>
                                        </table>
                                       
                                    </div>
                                       <div class="span5">
                                        <button onclick="myFunction()">Add tax group</button>
                                       </div>
                                        <div class="span12">
                                <h3 class="cen_title">Currency</h3>
                                <legend></legend>
                            </div>
                                    <div class="span5">
                                        <label class="control-label">Currency Symbol</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Currency Code</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                      <div class="span">
                                        <label class="control-label">Exchange Rate:</label>
                                           <table>
                                            <tbody class="exchange">
                                                <td>Payment</td>
                                                <td>Currency</td>
                                                <td>Currency Symbol</td>
                                                <td>Number of</td>
                                                <td>Thousands</td>
                                                <td>Decimal</td>
                                                <td>Exchange</td>
                                              </tbody>
                                            </table>
                                          
                                           <!--  <div class="line3"></div> -->
                                        <div class="span">
                                             <button onclick="myFunction2()">Add Currency Exchange Rate</button>
                                          </div>
                                            <div class="span5">
                                        <label class="control-label">Currency Symbol Location</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Number of Decimals</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Thousands Separator</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Decimals Point</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                </div>
                                
                                        
                                     <div class="span">
                                        <label class="control-label">Currency Denomination:</label>

                                        <table id="myTable">
                                        <tbody class="Currency">
                                          <tr>
                                            <td>Denomination</td>
                                            <td>Currency Value</td>
                                            <td>Delete</td>
                                          </tr>
                                          </tbody>
                                        </table>
                                          <!--   <div class="line4" style="margin-bottom: 20px;"></div> -->
                                  
                                            <div class="span">
                                            <button onclick="myFunction1()">Add Currency denomenation</button>
                                            </div>
                                        </div>
                                <!-- Payment Type -->
                                          <div class="span12">
                                            <h3 class="cen_title">Payment Type</h3>
                                            <legend></legend>
                                          </div>
                                         <div class="span">
                                          <button>Cash</button>
                                          <button>Check</button>
                                          <button>Gift Card</button>
                                          <button>Debit Card</button>
                                          <button>Credit Card</button>
                                         </div>
                                      <div class="span5">
             
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap" style="margin-top: 25px;" />
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Thousands Separator</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span">
                                       Enable EBY Payment: <input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span5">
                                       Enable WC: <input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Prompt for CCV when swiping card : <input type="checkbox" id="myCheck">
                                    </div>
                                <!-- Price Role -->
                            <div class="span12">
                                <h3 class="cen_title">Price Role</h3>
                                <legend></legend>
                            </div>
                                    
                                    <div class="span">
                                       Disble Price Roles Dialog: <input type="checkbox" id="myCheck">
                                    </div>

                              <div class="span12">
                                <h3 class="cen_title">Order and Diliveries</h3>
                                <legend></legend>
                            </div>
                            <div class="span">
                                       Do NOT tax service Items: <input type="checkbox" id="myCheck">
                            </div>
                            <div class="span">
                                <div class="span">
                                        <label class="control-label">Shipping Providers:</label>
                                    </div> 
                                <ul class="configure2">
                                                <li>Currency</li>
                                                <li>Symbol</li>
                                                <li>Location</li>
                                                <li>Decimals</li>
                                                <li>Separator</li>
                                                <li>Point</li>
                                                <li>Rate</li>
                                                <li>Delete</li>
                                </ul><hr>
                                            <div class="span">
                                                <button>Add Shipping Provider</button>
                                            </div>
                                                <div class="span">
                                        <label class="control-label">Shipping Providers:</label>
                                    </div> 
                                <ul class="configure2">
                                                <li>Currency</li>
                                                <li>Symbol</li>
                                                <li>Location</li>
                                                <li>Decimals</li>
                                                <li>Separator</li>
                                                <li>Point</li>
                                                <li>Rate</li>
                                                <li>Delete</li>
                                </ul><hr>
                                <div class="span">
                                    <button>Add Shipping Provider</button>
                                </div>

                                </div>
                                <!-- Sale -->
                                      <div class="span12">
                                    <h3 class="cen_title">Sale</h3>
                                <legend></legend>
                                </div>
                                    <div class="span5">
                                        <label class="control-label">Sale ID Prefix</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Item ID to show on sales</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Interface</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                     <div class="span">
                                       Auto Focus on Item Field when using Sales/Recievings Interfaces: <input type="checkbox" id="myCheck">
                                     </div>
                                     <div class="span">
                                       Capture Signature For all Sales: <input type="checkbox" id="myCheck">
                                     </div>
                                     <div class="span5">
                                        <label class="control-label">Number of Recent sale By Customer to show:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                     <div class="span">
                                       Hide Recent Sale For Customer: <input type="checkbox" id="myCheck">
                                     </div>
                                      <div class="span">
                                       Disable Sale Quick Complete: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Calculate Average Cost Price From Recivings: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Hide Subspended Recievings in Report: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Track cash In Register: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Do Not Show Expected Closing Amount when Closing Register: <input type="checkbox" id="myCheck">
                                     </div>
                                      <div class="span">
                                       Disable Giftcard Detection: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Always show Item Grid: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Hide out stock Items In Grid: <input type="checkbox" id="myCheck">
                                     </div>
                                      
                                    <div class="span5">
                                        <label class="control-label"> Defualt type for grid:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    </div>
                                    <div class="span">
                                       Require Customer for Sale: <input type="checkbox" id="myCheck">
                                     </div>
                                       <div class="span">
                                       Select Sale Person During Sale: <input type="checkbox" id="myCheck">
                                     </div>
                                      <div class="span5">
                                        <label class="control-label"> Defualt Sales Person:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    <!-- <div class="span5">
                                       Disable Sale Notification: <input type="checkbox" id="myCheck">
                                     </div> -->
                                    </div>

                              <div class="input-append" style="width: 93%;">
                               <label class="control-label" style="padding-left: 29px;"> Commission Default Rate</label>
                                 <input type="text" class="span12 m-wrap" name="commission" data-required="1" value="{{ old('commission') }}" onkeyup="this.value=numberWithCommas(this.value);" style="width: 464px; margin-left: 29px;" />
                                                <span class="add-on" style="    height: 20px;line-height: 24px;">%</span></div>
                                 <div class="span5">
                                        <label class="control-label"> Commission Percent Caculate Method:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    <!-- <div class="span5">
                                       Disable Sale Notification: <input type="checkbox" id="myCheck">
                                     </div> -->
                                    </div>
                                    <div class="span">
                                       Disable tell Notification: <input type="checkbox" id="myCheck">
                                     </div> 
                                     <div class="span">
                                       Comfirm Error Message Using Model Dialogs: <input type="checkbox" id="myCheck">
                                     </div> 
                                     <div class="span">
                                       Change Sale Date for New Sale: <input type="checkbox" id="myCheck">
                                     </div> 
                                     <div class="span">
                                       Do not group Item that are The same: <input type="checkbox" id="myCheck">
                                     </div> 
                                     <div class="span">
                                       Do not Alow Item To be Sold below Cost Price: <input type="checkbox" id="myCheck">
                                     </div> 
                                     <div class="span">
                                       Do not Alow Out of stock Item to be Sold: <input type="checkbox" id="myCheck">
                                     </div>
                                     <div class="span">
                                       Do not Allow Variation Items To be Sold Without selecting Variation: <input type="checkbox" id="myCheck">
                                     </div>  
                                      <div class="span">
                                       Edit Item Price If 0 After adding to sale: <input type="checkbox" id="myCheck">
                                     </div>
                                     <div class="span">
                                       Remind Employee to Open Customer Facing Display<input type="checkbox" id="myCheck">
                                     </div>  
                                     <!-- Suspend -->
                                <div class="span12">
                                    <h3 class="cen_title">Suspended Sales/Layaways</h3>
                                       <legend></legend>
                                </div>
                                <div class="span">
                                 <ul class="configure2">
                                                <li>Additional suspended Sale</li>
                                               <!--  <li>Types:</li> -->
                                                <li>Sort</li>
                                                <li class="sort">ID</li>
                                                <li class="sort">Suspend sale type</li>
                                                <li class="delete">Delete</li>
                                </ul>
                                    <!-- Types:<div class="line1"></div> -->
                                    <div class="span">
                                        <button>Add Susspended sale type</button>
                                    </div>
                                </div>
                                
                                 <div class="span">
                                       Require Customer for Suspended Sale : <input type="checkbox" id="myCheck">
                                 </div>
                                  <div class="span5">
                                        <label class="control-label">Override Layaway Name:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                 </div>
                                 <div class="span">
                                       Hide Layaways in Reports : <input type="checkbox" id="myCheck">
                                 </div>
                                  <div class="span">
                                       Change Sale Date When Suspending Sale : <input type="checkbox" id="myCheck">
                                 </div>
                                  <div class="span">
                                       Change Sale Date When Completing Suspending Sale : <input type="checkbox" id="myCheck">
                                 </div>
                                  <div class="span">
                                       Show Receipt Aafter Suspending Sale : <input type="checkbox" id="myCheck">
                                 </div>
                                  <div class="span12">
                                            <h3 class="cen_title">Receipt</h3>
                                            <legend></legend>
                                          </div>
                                       <div class="span5">
                                        <label class="control-label">Override Receipt Title:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                 </div>   
                                 <div class="span5">
                                        <label class="control-label">Email Receipt Subject:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                 </div>
                                    <div class="span">
                                       Show Item ID on Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                      <div class="span5">
                                        <label class="control-label">Number of Decimals For Quantity On Receipt:</label>
                                        <div class="controls">
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                      </div>
                                        <div class="span">
                                       Indicate Taxable On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Hide Description On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Print Receipt After Sale: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Wide Printer Receipt Format: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Print Receipt After Receiving: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Hide Signature: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Remove Customer Name From Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Remove Employee Name From Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Remove Customer Company Name From Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Remove Customer Contact Info From Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Automatically Email Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Automatically Print Duplicate Receipt For Credit Card Transactions: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Always Print Duplicate Receipt For All: <input type="checkbox" id="myCheck">
                                    </div>

                                    <div class="span">
                                       Automatically Show Comments On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Hide Barcode On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                        <div class="span">
                                       Group All Taxes On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                       Redirect To Sale Or Receving Screen After Printing Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span5">
                                        <label class="control-label">Receipt Text Size:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                   
                                      <div class="span">
                                       Hide Store Account Balance On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                     Round To Nearest .05 On Receipt: <input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                        <label class="control-label">Return Policy:
                                            <textarea style="height: 100px; width: 600px;"></textarea>
                                    </div>
                                     <div class="span">
                                        <label class="control-label">Announcements/Specials:
                                            <textarea style="height: 100px; width: 600px;"></textarea>
                                    </div>
                                      <div class="span12">
                                            <h3 class="cen_title">Profit Calculation</h3>
                                        <legend></legend>
                                     </div>
                                    <div class="span5">
                                        <label class="control-label">Calculate Gift Card Profit when:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    
                                    <div class="span">
                                       Remove Commission From Profit In Reports:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                     Remove Points Redemption From Profit:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span12">
                                        <h3 class="cen_title">Barcodes</h3>
                                        <legend></legend>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">ID to Show on Barcode:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span">
                                       Include Tax on Barcodes:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Hide Price on Barcode:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Hide Name On Barcode:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Show Company Name On Barcode:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Enable Scale:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Scale Barcode Format:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Scale Price Divide By:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                     <div class="span12">
                                            <h3 class="cen_title">Customer Loyalty</h3>
                                        <legend></legend>
                                     </div>
                                     <div class="span5">
                                       Enable Customer Loyalty System:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span12">
                                            <h3 class="cen_title">Price Tiers</h3>
                                        <legend></legend>
                                     <ul class="configure2">
                                                <li>Price Tiers:</li>
                                                <li>Sort</li>
                                                <li class="tier">Tier Name</li>
                                                <li class="tier">Default Percent Off</li>
                                                <li class="tier">Default Cost Plus Percent</li>
                                                <li class="tier">Default Cost Plus Fixed Amount</li>
                                                <li class="tier">Delete</li>
                                         </ul>
                                            <!--  <div class="line1"></div> -->
                                        <div class="span5">
                                        <label class="control-label">Override Tier Name On Receipt:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Default Tier Percent Type For Excel Import:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Default Tier Fixed Amount For Excel Import:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                     <div class="span5">
                                       Round Tier Price To 2 Decimals:<input type="checkbox" id="myCheck">
                                    </div>
                                     </div>
                                     <div class="span12">
                                            <h3 class="cen_title">ID Number</h3>
                                        <legend></legend>
                                        <div class="alert1">
                                            <strong>note:</strong> You can only increas Auto Increment Values Updating them will note affect IDs for Items,item kits, sale or receivings that already exist 
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Item ID Auto Increment Starting Value:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Item Kit ID Auto Increment Starting Value:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Sale ID Auto Increment Starting Value:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Receiving ID Auto Increment Starting Value:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                     </div>
                                     
                                     <div class="span12">
                                            <h3 class="cen_title">Items</h3>
                                        <legend></legend>
                                        <div>manage categories</div>
                                        <div>Number of Items Per page in Grid</div>
                                     </div>
                                        <div class="span5">
                                        <label class="control-label">Number Of Items Per Page:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                        <div class="span5">
                                        <label class="control-label">Number Of Items Per Page In Grid:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                    </div>
                                        <div class="span5">
                                        <label class="control-label">Default Recorder Level When Creating Items:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                     <div class="span">
                                       Default New Items As Service Items:<input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                       Highlight Low Inventory Items In Items Module:<input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                       Limit Manual Price Adjustments And Discounts:<input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                       Enable Mark Up Calculator:<input type="checkbox" id="myCheck">
                                    </div>
                                     <div class="span">
                                       Verify Age For Products:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span12">
                                            <h3 class="cen_title">Employee</h3>
                                        <legend></legend>
                                        <div class="span">
                                       Enable Time Clock:<input type="checkbox" id="myCheck">
                                    </div>
                                    <div class="span">
                                       Log out Automatically When Clocking Out:<input type="checkbox" id="myCheck">
                                    </div>  
                                    <div class="span">
                                       Enable Fast User Switching(Password Not Required):<input type="checkbox" id="myCheck">
                                    </div>  
                                    <div class="span">
                                       Require Employee Login Before Each Sale:<input type="checkbox" id="myCheck">
                                    </div>  
                                    <div class="span">
                                       Reset Location When Switching Employee:<input type="checkbox" id="myCheck">
                                    </div>  
                                     </div>
                                      <div class="span12">
                                            <h3 class="cen_title">Store Account</h3>
                                        <legend></legend>
                                       <div class="span">
                                         Customers Store Accounts:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                            Suppliers Store Accounts:<input type="checkbox" id="myCheck">
                                        </div>
                                    <div class="span">
                                       Disable Store Account When Over Credit Limit:<input type="checkbox" id="myCheck">
                                        </div>
                                    
                                      <div class="span">
                                        <label class="control-label">Store Account Statement Message:
                                            <textarea style="height: 100px; width: 600px;"></textarea>
                                       </div>
                                       <div class="span">
                                       Hide Store Account Payments In Reports:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Hide Store Account Payments From Report Totals:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">PayPal.Me Username:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                </div>
                                <div class="span12">
                                            <h3 class="cen_title">Application Setting</h3>
                                        <legend></legend>
                                        <div class="span5">
                                        <label class="control-label">Language:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Date Format:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Time Format:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        
                                        <div class="input-append" style="width: 93%;">
                                           <label class="control-label" style="padding-left: 29px;">Store Opening Time</label>
                                             <span class="add-on" style=" height: 20px;line-height: 24px;margin-left: 32px;"><i class="fas fa-clock"></i></span>
                                             <input type="text" name="commission" data-required="1" value="{{ old('commission') }}" onkeyup="this.value=numberWithCommas(this.value);" style="width: 446px;
                                                margin-left: 0px; border-left: none;" />
                                </div>
                                         <div class="input-append" style="width: 93%;">
                                           <label class="control-label" style="padding-left: 29px;">Store Closing Time</label>
                                             <span class="add-on" style=" height: 20px;line-height: 24px;margin-left: 32px;"><i class="fas fa-clock"></i></span>
                                             <input type="text" name="commission" data-required="1" value="{{ old('commission') }}" onkeyup="this.value=numberWithCommas(this.value);" style="width: 446px;
                                                margin-left: 0px; border-left: none;" />
                                </div>
                                        <div class="span">
                                       Require Https For Program:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Do Not Force HTTP When Needed For EMV Credit Care Processing:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Test Mode (Sales NOT Saved):<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Disable Test Mode:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       sHide Item Description In Reports:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Enable Sounds For Status Messages :<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Virtual Keyboard(On\Off):
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span">
                                       Show Languange Switcher :<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Show Clock In Header:<input type="checkbox" id="myCheck">
                                        </div>
                                            <div class="span">    
                                             <ul>
                                                <li class="tax_group">This is visible only on wide screens</li>
                                            </ul>
                                            </div>
                                        <div class="span">
                                       Legecy Detailed Report Excel Export :<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Overwrite Existing Items On Excel Import :<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Report Sort Order:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span">
                                       Speed Up Search Queries ?(Only Recommend If You Have More Than 10,000 Items Or Customers):<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Enable Quick Edit On Manage Pages:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Enhanced Search Method :<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span">
                                       Include Child Categories When Searching Or Reporting:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Spreadsheet Format:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Mailing Labels Format:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Session Expiration:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span">
                                       Always Minimize Left Side Bar Menu :<input type="checkbox" id="myCheck">
                                        </div>
                                </div>
                                    <div class="span">
                                    <table class="w3-table-alll" border="1px" width="700px" style="border-color: lightgrey;
                                        margin-left: 25px;border: none;">
                                      
                                        <tr>
                                          <td>Item ID</td>
                                        </tr>
                                        <tr>
                                          <td>Item Number</td>
                                        </tr>
                                        <tr>
                                          <td>Product ID</td>
                                        </tr>
                                        <tr>
                                        
                                          <td>Additional Item Numbers</td>
                                        </tr>
                                         <tr>
                                        
                                          <td>Serial Numbers</td>
                                        </tr>
                                         <tr>
                                        
                                          <td>Item Vaiation Item Number</td>
                                        </tr>
                                      </table>
                                    </div>
                                     <div class="span12">
                                            <h3 class="cen_title">Ecommerce Platform</h3>
                                        <legend></legend>
                                        <div class="span5">
                                        <label class="control-label">Select Platform:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span">
                                       New Items Are E-Commerce By Default:<input type="checkbox" id="myCheck">
                                        </div>
                                        <div class="span5">
                                        <label class="control-label">Store Location:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                        <div class="span">   
                                        <label>Ecoomerce Sync Operations:</label>
                                        </div>
                                        <div class="span">
                                    <table class="w3-table-all" border="1px" width="700px" style="
                                        border-color: lightgrey;border: none;">
                                      
                                       
                                        <tr>
                                          <td><i class="fa fa-external-link" aria-hidden="true"></i>  Import tags into phppos</td>
                                        </tr>
                                        <tr>
                                          <td><i class="fa fa-external-link" aria-hidden="true"></i>Import categories into phppos</td>
                                        </tr>
                                        <tr>
                                          <td><i class="fa fa-external-link" aria-hidden="true"></i>Import attributes into phppos</td>
                                        </tr>
                                        <tr>
                                            <td><i class="fa fa-external-link" aria-hidden="true"></i>Sync inventory changes</td>
                                        </tr>
                                        <tr>
                                        
                                          <td><i class="fa fa-external-link" aria-hidden="true"></i>Imort items into phppos</td>
                                        </tr>
                                         <tr>
                                        
                                          <td><i class="fa fa-external-link" aria-hidden="true"></i>import orders into phppos</td>
                                        </tr>
                                         <tr>
                                        
                                          <td><i class="fa fa-external-link" aria-hidden="true"></i>Export tags to ecommerce</td>
                                        </tr>
                                        <tr>
                                            <td><i class="fa fa-external-link" aria-hidden="true"></i>Export categories to ecommerce</td>
                                        </tr>
                                        <tr>
                                            <td>
                                                <i class="fa fa-external-link" aria-hidden="true"></i>Export items to ecommerce
                                            </td>
                                        </tr>
                                      </table>
                                    </div>
                                    <div class="span">   
                                    <label style="margin-top: 13px;">E-Commerce Syncing Logs:</label>
                                    </div>
                                    <div class="span5">
                                        <label class="control-label">Last Sync Date:
                                            <input type="text" name="display_name_kh" data-required="1" class="span12 m-wrap"/>
                                        </div>
                                    <div class="span">
                                    <label>Reset E-Commerce:</label>
                                    <button style="background-color: #d13535;">Reset E-Commerce</button>
                                    </div>
                                     </div>
                                     <div class="span12">
                                    <h3 class="cen_title">API Setting</h3>
                                       <legend></legend>
                                        <ul class="configure2">
                                                <li>Price Tiers:</li>
                                                <li>Description</li>
                                                <li class="tier">API key Ending In</li>
                                                <li class="tier">Permissions</li>
                                                <li class="tier">Delete</li>
                                        </ul>
                                             <div class="line1"></div>
                                    </div>
                                </div>
                                
                                <div class="span12">
                                        <button type="submit" class="btn btn-success">Submit</button>
                                        <a href="{{ url('module_interest') }}" class="btn btn-danger">Back</a>
                                </div>
                                </form>
                            </div>

                        </div>
                        <!-- /block -->
                    </div>
                     <!-- /validation -->

                </div>
            </div>  
            <script>
                    function myFunction() {
                        var x = document.getElementById("myFile");
                        x.disabled = true;
                    }
            </script>
            <script>
            function myFunction() {
                var x = document.getElementById("myCheck");
                x.checked = true;
            }
            </script>
<script>
        function myFunction() {
           var row;
              row += "<tr>";
              row += "<td><input type='text' name=''></td>";
              row += "<td></td>";
              row += "<td></td>";
              row += "<td></td>";
              row += "<td>4</td>";
              row += "<td>5</td>";
              row += "<td>6</td>";
              row += "</tr>";
            $(".Taxx").append(row);    
        }
        function myFunction1(){
            var row;
              row += "<tr>";
              row += "<td><input type='text' name=''></td>";
              row += "<td><input type='text' name=''></td>";
              row += "<td>Delete</td>";
               row += "</tr>";
              $(".Currency").append(row); 
        }
         function myFunction2(){
            var row;
              row += "<tr>";
              row += "<td>Currency</td>";
              row += "<td>Symbol</td>";
              row += "<td>Location</td>";
              row += "<td>Decimals</td>";
              row += "<td>Point</td>";
              row += "<td>Rate</td>";
              row += "<td>Delete</td>";
              row += "</tr>";
              $(".exchange").append(row); 
        }

</script>
@stop()

