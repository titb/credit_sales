@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

            <div class="row-fluid">



                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <!-- <div class="muted pull-left"><a href="{!! url('users') !!}">អ្នកប្រើប្រាស់</a> <span class="divider">/</span> បង្កើតអ្នកប្រើប្រាស់ថ្មី</div> -->

                            	<!-- <div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('users/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> New</a></div> -->

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="span12">

                            		<h3 class="cen_title"> កែប្រែមុខតំណែងថ្មី </h3>

                            	<legend></legend>

                            </div>

                            	<form action="{{ url('position/'.$position->id.'/edit') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="">

										<div class="control-group">

											<div class="span6">
				  								<label class="control-label">ឈ្មោះមុខតំណែងថ្មី<span class="required">*</span></label>

				  								<div class="controls">

				  									<input type="text" name="name" value="{{ $position->name }}" data-required="1" class="span12 m-wrap"/>

				  								</div>
			  								</div>

			  								<div class="span2" style="margin-top:25px;">
											  <button type="submit" class="btn btn-success">កែប្រែ</button>

											  <a href="{{ url('list_position') }}" class="btn btn-danger">ត្រលប់</a>
											</div>
										</div>

									</div>


								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>







@stop()
