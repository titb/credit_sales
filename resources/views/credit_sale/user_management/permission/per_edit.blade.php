@extends('credit_sale.layout.master')

@section('contend')
<div class="container-fluid">
            <div class="row-fluid">

                     <!-- validation -->
                    <div class="row-fluid">
                      <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="{!! url('users-permission') !!}">សិទ្ធរបស់អ្នកប្រើប្រាស់</a> <span class="divider">/</span> កែប្រែសិទ្ធរបស់អ្នកប្រើប្រាស់</div>
                            </div>
                            <div class="block-content collapse in">
                            @if (count($errors) > 0)
						          <div class="alert alert-danger">
						            <strong>Whoops!</strong> There were some problems with your input.<br><br>
						            <ul>
						              @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						              @endforeach
						            </ul>
						          </div>
						    @endif
                            @if ($message = Session::get('success'))
	                        <div class="alert alert-success">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
		                   @if($message = Session::get('keyerror'))
		                        <div class="alert alert-danger">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
                            <div class="12">
                            	<h3 class="cen_title">កែប្រែសិទ្ធរបស់អ្នកប្រើប្រាស់</h3>
                            	<legend></legend>
                            </div>
                            	<form action="{{ url('users-permission/'.$data->id.'/edit') }}" method="post" enctype="multipart/form-data">
	                                {{ csrf_field() }}
	                                <div class="span12" style="padding-left: 50px;">
										  <div class="control-group">
			  								<label class="control-label">ឈ្មោះមុខងារ<span class="required" placeholder="Ex:  title-name">*</span></label>
			  								<div class="controls">
			  									<input type="text" name="name" data-required="1" value="{{ $data->name }}" class="span9 m-wrap"/>
			  								</div>
			  								<label class="control-label">ឈ្មោះបង្ហាញ<span class="required" placeholder="Ex: Title Name">*</span></label>
			  								<div class="controls">
			  									<input type="text" name="display_name" value="{{ $data->display_name }}" data-required="1" class="span9 m-wrap"/>
			  								</div>
                                            <label class="control-label">Module<span class="required" placeholder="Ex: Module">*</span></label>
			  								<div class="controls">
			  									<input type="text" name="module" data-required="1" value="{{ $data->module }}" class="span9 m-wrap"/>
			  								</div>
			  								<label class="control-label">បរិយាយ</label>
			  								<div class="controls">
			  									<input type="text" name="description" data-required="1" value="{{ $data->description }}" class="span9 m-wrap"/>
			  								</div>
	  									  </div>
									</div>
									<div class="span12">

										<center>

											<button type="submit" class="btn btn-success">កែប្រែ</button>

										</center>

									</div>

								</form>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>
            </div>
<script type="text/javascript">
	$(document).ready(function() {
    $('#selecctall').click(function(event) {  //on click
        if(this.checked) { // check select status
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = true;  //select all checkboxes with class "checkbox1"
            });
        }else{
            $('.checkbox1').each(function() { //loop through each checkbox
                this.checked = false; //deselect all checkboxes with class "checkbox1"
            });
        }
    });
});
</script>
@stop()
