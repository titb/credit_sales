@extends('credit_sale.layout.master')
@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

                

                <!--/span-->

                	<div class="row-fluid">

		                	<div class="navbar">

		                    	<div class="navbar-inner">

		                            <ul class="breadcrumb">

		                            	<li class="active"> <a href="{{ url('users-groups') }}" class="btn btn-defualt pull-right"><i class="icon-eye-open"></i>  មើល</a></li>

		                                <li class="active"> <a href="{{ url('users-groups/create') }}" class="btn btn-success pull-right"><i class="icon-pencil icon-white"></i>  បង្កើតថ្មី</a></li>

		                            

		                            </ul>



		                    	</div>

		                	</div>

		                

		            </div>

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('users-groups') !!}">អ្នកប្រើប្រាស់</a> <span class="divider">/</span> កែប្រែអ្នកប្រើប្រាស់</div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                   @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="12">

                            	

                            	<h3 class="cen_title">កែប្រែអ្នកប្រើប្រាស់</h3>

                            	<legend></legend>

                            	

                            </div>

                            	<form action="{{ url('users-groups/'.$data->id.'/edit') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">

	             

										  <div class="control-group">

			  								<!-- <label class="control-label">Name Function<span class="required" placeholder="Ex:  title-name">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="name" data-required="1" value="{{ $data->name }}" class="span9 m-wrap"/>

			  								</div> -->

			  								<label class="control-label">Name Display<span class="required" placeholder="Ex: Title Name">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="display_name" value="{{ $data->display_name }}" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">Discription<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="description" data-required="1" value="{{ $data->description }}" class="span9 m-wrap"/>

			  								</div>

			  								

	  									  </div>	

								

									</div>

									<div class="span12">

										<div class="controls">

			  									

													<input type="checkbox" id="selecctall"/> ជ្រើសរើសទាំងអស់<br/><br/>

												 @foreach($groups as $key => $groper)
												<div>

														<div class="span12" style="margin-left: 0px !important;">

															<b style="color:#08c; font-size: 14px; text-transform: capitalize;"> {{ $key }} </b>

														</div>

													   @foreach($groper as $permiss)
											                 	<div class="span3" style="margin-left: 0px !important;">

												                	<label>
																						<input name="permission_id[]" type="checkbox" class="ace permission_id" value="{{ $permiss->id }}" @foreach($data->permissions as $per)@if($per->id == $permiss->id) checked  @endif @endforeach/>
												 										<span class="lbl">  {{ $permiss->display_name}} </span>
																					</label>

											                	</div>

									                @endforeach

									            </div><br/>

									            @endforeach			

			  								</div>

									</div>

									<div class="span12">

										<center>

											<button type="submit" class="btn btn-success">កែប្រែ</button>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	





<script type="text/javascript">



	$(document).ready(function() {

    $('#selecctall').click(function(event) {  //on click 

        if(this.checked) { // check select status

            $('.checkbox1').each(function() { //loop through each checkbox

                this.checked = true;  //select all checkboxes with class "checkbox1"               

            });

        }else{

            $('.checkbox1').each(function() { //loop through each checkbox

                this.checked = false; //deselect all checkboxes with class "checkbox1"                       

            });         

        }

    });

    

    

});

</script>



@stop()
