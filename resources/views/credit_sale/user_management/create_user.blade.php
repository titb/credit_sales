@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

            <div class="row-fluid">



                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('users') !!}">អ្នកប្រើប្រាស់</a> <span class="divider">/</span> បង្កើតអ្នកប្រើប្រាស់ថ្មី</div>

                            	<!-- <div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('users/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> New</a></div> -->

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="span12">

                            		<h3 class="cen_title"> បង្កើតអ្នកប្រើប្រាស់ថ្មី </h3>

                            	<legend></legend>

                            </div>

                            	<form action="{{ url('users/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">



										  <div class="control-group">

										  	<label class="control-label">ជ្រើសរើសសាខា<span class="required">*</span></label>

			  								<div class="controls">
			  									<?php $branch = DB::table('mfi_branch')->get(); ?>
			  									<select class="span9 m-wrap" name="branch_id">
			  										@foreach($branch as $b)
			  										<option value="{{ $b->id }}">{{ $b->brand_name }}</option>
			  										@endforeach
			  									</select>

			  								</div>

			  								<label class="control-label">ឈ្មោះអ្នកប្រើប្រាស់<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="username" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">ឈ្មោះជាភាសាខ្មែរ<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="name_kh" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">ភេទ<span class="required">*</span></label>

			  								<div class="controls">

			  									<select id="selectError"​ name="user_sex" class="span9 m-wrap">

												  <option value="M">ប្រុស</option>

												  <option value="F">ស្រី</option>



												</select>

			  								</div>



			  								<label class="control-label">លេខទូរសព្ទ<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="user_phone" data-required="1" class="telnumber span9 m-wrap"/>

			  								</div>
			  								<script type="text/javascript">
											   $('.telnumber').keyup(function() {
												foo = $(this).val().split("-").join(""); // remove hyphens
												foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");
											        $(this).val(foo);
											   });
											</script>

			  								<label class="control-label">សារអេឡិចត្រូនិច<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="user_email" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">កំណត់សិទ្ធិអ្នកប្រើប្រាស់<span class="required">*</span></label>

			  								<div class="controls">

			  									<select id="selectError"​ name="group_id" class="span9 m-wrap">

												  <option value="">ជ្រើសរើសសិទ្ធ </option>

												  @foreach($groups as $g)

												     <option value="{{ $g->id }}"> {{ $g->display_name }} </option>

												  @endforeach

												</select>

			  								</div>

			  								<label class="control-label">លេខសម្ងាត់<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="password" name="password" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">បំពេញលេខសម្ងាត់ម្តងទៀត<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="password" name="confirm-password" data-required="1" class="span9 m-wrap"/>

			  								</div>
			  								<label class="control-label">មុខតំណែង<span class="required">*</span></label>
			  									<?php $position = DB::table('mfi_positions')->where('deleted','=',0)->get(); ?>
			  									<select name="position_id" class="span9 m-wrap">
			  										<option>-- ជ្រើសរើស --</option>
			  										@foreach($position as $p)
			  										<option value="{{ $p->id }}">{{ $p->name }}</option>
			  										@endforeach
			  									</select>

			  								<label class="control-label">រូបថត</label>
			  								<div class="controls">
			  									<input type="file" name="photo" class="input-file uniform_on">
			  								</div>

	  									  </div>
									</div>



									<div class="span12">

										<center>

											<button type="submit" class="btn btn-success">បញ្ចូល</button>

											<a href="{{ url('users') }}" class="btn btn-danger">ត្រលប់</a>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>







@stop()
