@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

		                    <div class="row-fluid">

		                        <!-- block -->

		                        <div class="block">

		                            <div class="navbar navbar-inner block-header">

		                            	<a href="{{ url('users') }}" class="btn btn-danger pull-right" style="margin-top:15px;">ត្រលប់</a>

		                                <h3 class="text-center" style="color:#307ec2;"><b>ព័ត៌មានអ្នកប្រើប្រាស់</b> <!-- {{ $data->username }} --></h3>

		                            </div>

		                              @if($message = Session::get('keyerror'))

					                        <div class="alert alert-danger">

					                            <p>{{ $message }}</p>

					                        </div>

					                   @endif

		                           <div class="block-content collapse in">


								                <div class="span3 " align="left">
								                	<img alt="User Pic" src="<?php if($data->photo > 0){ echo "../../User/images/".$data->photo; }else{echo "../../User/images/icon-user.png";} ?>" class="img-circle img-responsive">
								                	<p style="margin-left:50px;">
								                	<b>លេខសម្គាល់:
								                		@if($data->brand_id)
								                			{{$data->mfi_brand->brand_name_short}}
									                	@else
									                		NO
									                	@endif
									                  	-
									                  	<?php
												                $invID = $data->id;
												                $invID = str_pad($invID, 6, '0', STR_PAD_LEFT);
												                echo $invID;
														?>
													</b>
								                	</p>
								                </div>
								                <div class="span9 ">

								                  <table class="table" style="margin-top:30px;">
								                    <tbody>
								                      <tr>
								                        <td>ឈ្មោះបុគ្គលិក:</td>
								                        <td>{{ $data->username }}</td>
								                      </tr>
								                      <tr>
								                        <td>ឈ្មោះជាភាសាខ្មែរ:</td>
								                        <td>{{ $data->name_kh }}</td>
								                      </tr>
								                      <tr>
								                        <td>អីុម៉ែល:</td>
								                        <td>{{ $data->user_email }}</td>
								                      </tr>
								                      <tr>
								                        <td>លេខទូរស័ព្ទ:</td>
								                        <td>
								                        	<?php
															$length = strlen($data->user_phone);
															  if ($length == 9) {
															    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})$/", "$1-$2-$3", $data->user_phone);
															  }
															  else{
															    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})(\d{1})$/", "$1-$2-$3-$4", $data->user_phone);
															  }
															?>
														</td>
								                      </tr>

								                         <tr>
								                             <tr>
								                        <td>ភេទ:</td>
								                        <td>
								                        	@if($data->user_gender == "F")
											                    ស្រី
											                @elseif($data->user_gender == "M")
											                    ប្រុស
											                @endif
											            </td>
								                      </tr>
								                      <tr>
								                        <td>មុខតំណែង:</td>
								                        <td>
								                        	@if($data->mfi_position->name)
								                        	{{ $data->mfi_position->name }}
								                        	@else
								                        	NO
								                        	@endif
								                        </td>
								                      </tr>
								                      <tr>
								                        <td>សិទ្ធិ:</td>
								                        <td>{{ $data->groups->first()->name }}</td>
								                      </tr>

								                    </tbody>
								                  </table>
								                </div>
		                          </div>
		                        <!-- /block -->

		                    	</div>

		                </div>
</div>

@endsection
