@extends('credit_sale.layout.master')

@section('contend')



<div class="container-fluid">

            <div class="row-fluid">



                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <!-- <div class="muted pull-left"><a href="{!! url('users') !!}">អ្នកប្រើប្រាស់</a> </div> -->

                            		<form class="form-search" style="margin-bottom:2px;" action="{{URL::to('users')}}" method="GET">

											<?php $user = DB::table('users')->get();?>

											<input type="text" name="search" placeholder="ឈ្មោះ" data-required="1" class="span3 m-wrap" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($user as $row)"{{$row->name_kh}}",@endforeach""]'>

											<button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> ស្វែងរក</button>

                            		</form>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                             	<center>

			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រង់អ្នកប្រើប្រាស់</h3>

			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><a href="{{ url('users/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

			                    </center>

                            	<legend></legend>



                            <style>



                            </style>





							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>លេខសម្គាល់</th>

							            <th>ឈ្មោះអ្នកប្រើប្រាស់</th>

							            <th>ឈ្មោះជាភាសាខ្មែរ</th>

							            <th>សារអេឡិចត្រូនិច </th>

							            <th>លេខទូរសព្ទ</th>

							            <th>ភេទ</th>

							            <th>មុខតំណែង</th>

							            <th>សិទ្ធិប្រើប្រាស់</th>

							            <th>ស្ថានភាព</th>

										<th>ប្តូរលេខសម្ងាត់</th>

							            <th>សកម្មភាព</th>

							        </tr>

							      </thead>

							      <tbody>

							        	@foreach($data as $key => $d)

								                <tr>

								                <td>
								                	@if($d->branch_id)
														{{$d->branch->brand_name_short}}
								                	@else
								                		NO
								                	@endif
								                  	-
								                  	<?php
											                $invID = $d->id;
											                $invID = str_pad($invID, 6, '0', STR_PAD_LEFT);
											                echo $invID;
													?>
												</td>

								                  <td><a href="{!! url('users/'.$d->id.'/edit') !!}" >{!! $d->username !!} </a></td>

								                  <td>
								                  	@if($d->name_kh)
								                  		{{ $d->name_kh }}
								                  	@else
								                  		NO
								                  	@endif
								                  </td>

								                  <td> {{ $d->user_email }} </td>

								                  <td>
								                  	<?php
													$length = strlen($d->user_phone);
													  if ($length == 9) {
													    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})$/", "$1-$2-$3", $d->user_phone);
													  }
													  else{
													    echo preg_replace("/^1?(\d{3})(\d{3})(\d{3})(\d{1})$/", "$1-$2-$3-$4", $d->user_phone);
													  }
													?>
												</td>

								                  <td>



								                  		@if($d->user_gender == "F")

								                  			ស្រី

								                  		@else

								                  			ប្រុស

								                  		@endif



								                   </td>
								                   <td>
								                   	@if($d->position_id)
								                   		{{ $d->mfi_position->name }}
								                   	@else
								                   		NO
								                   	@endif
								                   </td>

								                  <td>

									                 	<span class="badge badge-success">



									                 	@if(count($d->groups) > 0)

									                 		{!! $d->groups->first()->name  !!}

									                 	@endif

									                 	</span>

								                  </td>

								                  <td>



						                  					@if($d->user_status == 1)

						                  					  <a href="{!! url('users/'.$d->id.'/not_active') !!}"><span class="badge badge-success"> Active </span></a>

						                  					@else

						                  					  <a href="{!! url('users/'.$d->id.'/active') !!}"><span class="badge badge-important"> Not Active </span></a>

						                  					@endif



								                  </td>
                                                 <td><a href="{!! url('change_password_user/'.$d->id.'/change') !!}" >ប្តូរលេខសម្ងាត់</a></td>

								                  <td>
								                  		<a href="{!! url('users/'.$d->id.'/show') !!}" class="btn btn-info">លម្អិត</a>

								                  		<a href="{!! url('users/'.$d->id.'/edit') !!}" class="btn btn-primary">កែប្រែ</a>

								                  		{!! Form::open(['method' => 'post','url' => 'users/'.$d->id.'/deleted','style'=>'display:inline']) !!}

											            {!! Form::submit('លុប', ['class' => 'btn btn-danger']) !!}

											        	{!! Form::close() !!}



								                  </td>

								                </tr>

								        @endforeach

							      </tbody>

							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">{!! $data->render() !!}</div>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>





@stop()
