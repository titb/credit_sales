@extends('credit_sale.layout.master')

@section('contend')
<div class="container-fluid">
            <div class="row-fluid">

                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="{!! url('users-permission') !!}">អ្នកប្រើប្រាស់</a> <span class="divider">/</span>ប្តូរលេខកូដសម្ងាត់</div>
                            </div>
                            <div class="block-content collapse in">
                            @if (count($errors) > 0)
						          <div class="alert alert-danger">
						            <strong>Whoops!</strong> There were some problems with your input.<br><br>
						            <ul>
						              @foreach ($errors->all() as $error)
						                <li>{{ $error }}</li>
						              @endforeach
						            </ul>
						          </div>
						    @endif
                            @if ($message = Session::get('success'))
		                        <div class="alert alert-success">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="12">



                            	<h3 class="cen_title">  </h3>

                            	<legend>ប្តូរលេខកូដសម្ងាត់</legend>



                            </div>

                            	<form action="{{ url('change_password_user/'.$data->id.'/change') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">



										  <div class="control-group">

			  								<label class="control-label">New Password<span class="required" placeholder="Ex:  New Password">*</span></label>

			  								<div class="controls">

			  									<input type="password" name="password" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">Confirm Password<span class="required" placeholder="Ex: Confirm Password">*</span></label>

			  								<div class="controls">
			  									<input type="password" name="confirm-password" data-required="1" class="span9 m-wrap"/>
			  								</div>

									</div>

									<div class="span12">
										<center>
											<button type="submit" class="btn btn-success">Save</button>
										</center>
									</div>
								</form>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>
            </div>

@stop()
