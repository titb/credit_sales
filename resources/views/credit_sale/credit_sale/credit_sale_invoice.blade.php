@extends('credit_sale.layout.master')
@section('contend')
<link rel="stylesheet" href="{{ URL::to('assets/jquery-ui.css')}}">
<script src="{{ URL::to('assets/jquery-1.12.4.js')}}"></script>
<script src="{{ URL::to('assets/jquery-ui.js')}}"></script>

<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<style>
    #content {
        margin-left: 15%;
        width: 70%;
    }
    .header2 {
        height: 60px;
    }
    .table-bordered {
        border: none;
    }
    .table-bordered th, .table-bordered td{
        border-left: none;
    }
</style>
<!-- container-fluid  -->
<div class="container-fluid content_top_mar" >
        <!-- row-fluid -->
            <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                       <a href="{{url('credit_sales')}}" class="btn btn-sm btn-info pull-right" style="margin-left:10px; margin-right:10px"><i class="icon-arrow-left icon-white"></i> New Sale </a>    
                                       <button class="btn btn-sm btn-primary edit_sale" style="margin-left:10px; margin-right:10px" value="{{$sale_id}}"> Edit Sale </button>    
                                        
                            </div>
                            <!-- block-content collapse in     -->
                            <div class="block-content collapse in">
                               @include('errors.error')	
                               <!-- <div class="up_all"> -->
									<div class="header1">
										<div class="title">
											<h3>TITB Sale By Cash</h3>
										</div>
									</div>
									<div class="border-bottom"></div>
									<div class="header2">
										<div class="row">
                                            <div class="span4">
												<p><b>Invoice #:</b><span class="invoice_num"> </span></p>
												<!-- <p><b>Invoice Date:</b><span class="invoice_date"> </span></p> -->
											</div>
											<div class="span4" style="float: right;">
												<!-- <p><b>Invoice #:</b><span class="invoice_num"> </span></p> -->
												<p><b>Invoice Date:</b><span class="invoice_date"> </span></p>
											</div>
										</div>
									</div>
									<div class="header3">
										<div class="row">
                                            <div class="span4">
												<p>Staff: <span class="staff">{{Auth::User()->username}} </span></p>
                                                <p>Phone: <span class="phone1">{{Auth::User()->branch->brand_phone}}</span></p>
                                                <p>Email : <span class="email1"> {{Auth::User()->branch->brand_email}} </span></p>
                                                <p>Website : <span class="website"> {{Auth::User()->branch->website}} </span></p>
                                                <p>Address: <span class="address "> {{ Auth::User()->branch->brand_address }} </span></p>
											</div>
											<!-- <div class="span4"> -->
												
												<!-- <p>F: <span class="fax1">Fax Number</span></p> -->
											<!-- </div> -->
											<!-- <div class="span4">
												
											</div> -->
											<div class="span4" style="float: right;">
												<p><b>Customer Name:</b>	<span class="customer_name"> </span></p>
                                                <p><b>Phone:</b> <span class="phone"> </span></p>
                                                <p><b>Email :</b> <span class="email"> </span></p>
                                                <p><b>Address:</b> <span class="address1"> </span></p>
											</div>
											<!-- <div class="span4">
												
											</div> -->
											
										</div>
                                    </div>
                                    <div class="border-bottom"></div>
									<div class="table_show">
										<table class="table table-bordered table-striped">
											<thead ><!-- style="background-color: #f4433642;" -->
												<tr>
													<th>Item</th>
													<th>Description</th>
													<th>Order Qty</th>
													<th>Sell Price</th>
													<th style="text-align: right;">Total</th>
												</tr>
											</thead>

											<tbody class="list_item" id="list_item">
												
                                            </tbody>
                                            <table class="table table-bordered table-striped" style="width: 31%; float: left; clear: right;">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tr>
													<td colspan="5" class="tfoot-right-a">Payback Defualt</td>
													<td class="right">$ <span class="payback_defualt"></span></td>
												</tr>
                                                <tr>
													<td colspan="5" class="tfoot-right-a">Payback Change</td>
													<td class="right"><span class="payback_change"></span>៛</td>
												</tr>
												<tr>
													<td colspan="5" class="tfoot-right-a">Tax Rate</td>
													<td class="right"><span class="tax"></span></td>
												</tr>
												
												<tr>
													<td colspan="5" class="tfoot-right-a">Deposit Received</td>
													<td class="right"><span class="deposit_price"> </span></td>
												</tr>
												<tr>
													<td colspan="5" class="tfoot-right-a">Comment</td>
													<td class="right"><span class="comment"> </span></td>
												</tr>
                                            </table>
											<table class="table table-bordered table-striped" style="width: 31%; float: right; clear: right;">
                                                <thead>
                                                    <tr>
                                                        <th></th>
                                                    </tr>
                                                </thead>
                                                <tr>
													<td colspan="5" class="tfoot-right-a">Invoice Subtotal</td>
													<td class="right">$ <span class="sup_total_price"> 0.00 </span></td>
												</tr>
												<tr>
													<td colspan="5" class="tfoot-right-a">Discount</td>
													<td class="right"><span class="discount_payment"> 0 </span>(%)</td>
												</tr>
                                                <tr>
													<td colspan="5" class="tfoot-right-a">Discount Reason</td>
													<td class="right"><span class="discount_reason"> </span></td>
												</tr>
                                                <tr>
													<td colspan="5" class="tfoot-right-a">Dute Payment Amount</td>
													<td class="right">$ <span class="dute_payment_amount"> 0.00 </span></td>
												</tr>
                                                <!-- <tr>
                                                    <td colspan="6" class="tfoot-right">
                                                        <table class="table table-bordered table-striped">
                                                            <tbody id="list" class="list_item">
                                                
                                                            </tbody>
                                                        </table>
                                                    </td>
												</tr> -->
											</table>
										</table>

                                    </div>
                                
							   <!-- </div> -->
                            </div>
                        </div>
            </div>
</div>

<script type="text/javascript">

	$(document).ready(function(){
                    $.ajaxSetup({
							headers: {
								'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
							}
						})
						var c_id ="{{$sale_id}}";
						
						var url_edit = "{{route('credit_sales')}}/"+c_id+"/sale_invoice_json";

						// alert(url_edit);
						var client;
						var forData = {};
							var out = "";
							$.ajax({
									type: "GET",
									url: url_edit, 
									dataType: "json",
									data: forData,
									success: function(result ,xhr){
										console.log(result);
                                        var myJSON = JSON.stringify(result);

                                    // Get from Client
                                        if(result.cs_sale.cs_client.en_username){
                                            $('.customer_name').html(result.cs_sale.cs_client.en_username);
                                        }
                                        if(result.cs_sale.cs_client.phone){
                                            $('.phone').html(result.cs_sale.cs_client.phone);
                                        }
                                        if(result.cs_sale.num_invoice){
                                            $('.invoice_num').html(result.cs_sale.num_invoice);
                                        }
                                        
                                        var address, home_n, street_n, vilige, group_n, district, commune, pro, dob;
                                        
                                        if(result.cs_sale.cs_client.home_num !== null){
                                            home_n = result.cs_sale.cs_client.home_num +",";
                                        }else{
                                            home_n = " ";
                                        }
                                        if(result.cs_sale.cs_client.street_num !== null){
                                            street_n = result.cs_sale.cs_client.street_num +",";
                                        }else{
                                            street_n = " ";
                                        }
                                        if(result.cs_sale.cs_client.group_num !== null){
                                            group_n = result.cs_sale.cs_client.group_num +",";
                                        }else{
                                            group_n = " ";
                                        }
                                        if(result.cs_sale.cs_client.vilige !== null){
                                            vilige = result.cs_sale.cs_client.vilige +",";
                                        }else{
                                            vilige = " ";
                                        }
                                        if(result.cs_sale.cs_client.district !== null){
                                            district = result.cs_sale.cs_client.district +",";
                                        }else{
                                            district = " ";
                                        }
                                        if(result.cs_sale.cs_client.commune !== null){
                                            commune = result.cs_sale.cs_client.commune +",";
                                        }else{
                                            commune = " ";
                                        }
                                        if(result.cs_sale.cs_client.province !== null){
                                            pro = result.cs_sale.cs_client.province +",";
                                        }else{
                                            pro = " ";
                                        }
                                        if(result.cs_sale.cs_client.dob !== null){
                                            dob = result.cs_sale.cs_client.dob +",";
                                        }else{
                                            dob = " ";
                                        }

                                        address = home_n +" "+ street_n +" "+ group_n +" "+ vilige +" "+ district +" "+ commune +" "+ pro;
                                        $('.address1').html(address);

                                        if(result.cs_sale.cs_client.email){
                                            $('.email').html(result.cs_sale.cs_client.email);
                                        }
                                        if(result.cs_sale.due_date){
                                            $('.invoice_date').html(result.cs_sale.due_date);
                                        }

                                        // Get Data from Item
                                        text = "";
                                        if(result.cs_sale.sale_item){
                                            $.each(result.cs_sale.sale_item, function(k1, sale){
                                                text +="<tr>";
                                                    text +="<td>"+sale.item.item_bacode+"</td>";
                                                    text +="<td>"+sale.item.name+"</td>";
                                                    text +="<td>"+sale.qty+"</td>";
                                                    text +="<td>"+accounting.formatMoney(sale.item.sell_price)+"</td>";
                                                    text +="<td style='text-align: right;'>"+accounting.formatMoney(sale.total_price)+"</td>";
                                                text +="</tr>";
                                            });
                                        }
                                        $('#list_item').html(text);
                                        

                                        if(result.cs_sale.sale_payment.total_payment){
                                           $('.sup_total_price').html(accounting.formatMoney(result.cs_sale.sale_payment.total_payment));
                                        }
                                        if(result.cs_sale.sale_payment.discount_payment){
                                           $('.discount_payment').html(result.cs_sale.sale_payment.discount_payment);
                                        }
                                        if(result.cs_sale.sale_payment.discount_reason){
                                           $('.discount_reason').html(result.cs_sale.sale_payment.discount_reason);
                                        }
                                        if(result.cs_sale.sale_payment.dute_payment_amount){
                                           $('.dute_payment_amount').html(accounting.formatMoney(result.cs_sale.sale_payment.dute_payment_amount));
                                        }
                                        
                                        // Transaction Pay
                                        text1 ="";
                                        if(result.cs_sale.sale_transaction_pay){
                                            $.each(result.cs_sale.sale_transaction_pay, function(k2, tran){
                                                if(tran.currecy_type ==1){
                                                        var ct = "US";
                                                }else{
                                                        var ct = "KHM";
                                                }
                                                text1 +="<tr>";
                                                    text1 +="<td>"+accounting.formatMoney(tran.account_mount)+"</td>";
                                                    text1 +="<td>"+ct+"</td>";
                                                    text1 +="<td>"+tran.payment_pay+"</td>";
                                                text1 +="</tr>";
                                            });
                                        }
                                        $('#list').html(text1);

                                        if(result.cs_sale.sale_payment.payback_defualt){
                                           $('.payback_defualt').html(accounting.formatMoney(result.cs_sale.sale_payment.payback_defualt));
                                        }
                                        if(result.cs_sale.sale_payment.payback_change){
                                           $('.payback_change').html(result.cs_sale.sale_payment.payback_change);
                                        }
                                        if(result.cs_sale.sale_payment.total_tax){
                                           $('.tax').html(result.cs_sale.sale_payment.total_tax);
                                        }
                                        if(result.cs_sale.cs_request_form.deposit_fixed){
                                           $('.deposit_price').html(result.cs_sale.cs_request_form.deposit_fixed);
                                        }
                                        if(result.cs_sale.sale_payment.comment){
                                           $('.comment').html(result.cs_sale.sale_payment.comment);
                                        }
                                        
										
                                    },
									error: function (result ) {
										console.log(result.stutus);
									}

							});
				      
				
    });
    $(".edit_sale").click(function(){
            var item_id = $(this).val();
            var formData = {
                    item_id: item_id,
                    redirect: 'invoice',
            };
            add_to_cart_item(formData);

       });
		    // add card to table by on click item
       function add_to_cart_item(formData){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
                }
            });
           // alert(formData.item_id);
            var url_now = "{{url('post_item_add_to_cart_ajax')}}";
            $.ajax({
                type: "POST",
                url: url_now,
                dataType: 'json',
                data: formData,
                success: function(result){
                    window.location = "{{url('credit_sales?')}}redirect="+formData.redirect+"&sales="+formData.item_id;
                    return false;
                },
                error: function(){} 	        
            });

    }
</script>
@endsection
