@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('products/products') !!}"> ការគ្រប់គ្រង់ផលិតផល</a> <span class="divider">|</span> បង្កើតផលិតផលថ្មី </div>					
                            </div>

                            <div class="block-content collapse in">
                                     
                               <hr/>
                               
                                 <center>
			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រង់ផលិតផល</h3>
			                    </center>
                            	<legend></legend>
                                <div class="span12" style="margin-left:0px;" >
                                            <?php $items = DB::table('cs_items')->where('status','=',1)->where('deleted','=',0)->get();?>
                                            <div class="span3">
                                                <lable>&nbsp;</lable>
                                                <input type="text" name="item_search" placeholder="ឈ្មោះ" data-required="1" class="span12 m-wrap item_search"id="typeahead" data-provide="typeahead" data-source='[@foreach($items as $row)"{{$row->name}}",@endforeach""]' autocomplete="off">
                                            </div>
                                            
                                            <div class="span3">
                                                <lable>ប្រភេទ</lable>
                                                <?php $cates = DB::table('cs_categorys')->where('status','=',1)->where('deleted','=',0)->get();?>
                                                <select class="span12 m-wrap category_search" name="category_search" id="category_search">
                                                    <option value="">ទាំងអស់</option>
                                                    @foreach($cates as $ca)
                                                    <option value="{{$ca->id}}">{{$ca->name}}</option>
                                                    @endforeach
                                                </select>
                                                
                                            </div>
                                            <div class="span2">
                                                <lable>&nbsp;</lable><br/>
                                                <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search">ស្វែងរក</button>
                                            </div>
                                            <div class="muted span3 pull-right" style="margin-top: 18px;"><a href="{{ url('products/products/create?redirect=create') }}" class="btn btn-success btn_edit pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
                                 </div>  
                            
                            @include('errors.error')	
                             	
                           
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>
							            <th>លេខបាកូដ</th>
							            <th>ឈ្មេាះ</th>
							            <th>Category</th>
                                        <th>Cost Price</th>
                                        <th>Selling Price</th>
                                        <th>Quantity</th>

                                        <th>សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="item_list">
                                       
                                  </tbody>
                                    <tr>
                                        <td colspan="7">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right"></div>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>

<script>
$(document).ready(function(){
        var numpage = 1;
		var url_index1 = "{{route('products/products_get_js')}}";
		get_page(url_index1,numpage);
            $(document).ajaxComplete(function(){    
                    $(".b_search").click(function(){
                        var submit_search = $(this).val();
                        var n = 1;
                        
                        var url_index2 = submit_search; 		
                        get_page(url_index2,numpage = n);
                    });
                    
                    $(".pag").click(function(){
                        var numpage = $(this).text();   
                        get_page(url_index1,numpage);
                    });

                    $(".pre").click(function(){
                        var numpage = $(this).find(".pre_in").val();
                        get_page(url_index1,numpage);
                    });

                    $(".btn_deleted").click(function(e){
                        
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                            var item_id = $(this).val();
                            var url1 = "{{ route('products/products') }}";
                            var url_index = url1+"/"+item_id+"/deleted";
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: url_index, 
                                dataType: "json",
                                success: function(result){
                                    $('.msg_show').html(result.msg_show); 
                                            var numpage = 1 ;
                                            var url_index1 = "{{ route('products/products_get_js') }}";
                                            get_page(url_index1,numpage);
                                },
                                error: function (result ,status, xhr) {
                                    console.log(result.responseText);
                                }                   
                            });
                    });

                });
        function get_page(url,n){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                var item_search = $(".item_search").val(); 
                //alert(item_search);
                if(url === "b_search" || item_search !== ""){
                    var url_i = "{{route('products/products_get_js')}}";
                    var forData = {
                                    item_search: $(".item_search").val(),
                                    category_search: $(".category_search").val(),
                                    submit_search: $(".b_search").val()
                                }                
                    var url_index = url_i+"?item_search="+item_search+"&category_search="+category_search+"&submit_search=b_search&page="+n;     
                }else{
                    var forData = {};
                    var url_index = url+"?page="+n;
                }
			    var client;
                    $.ajax({
                            type: "GET",
                            url: url_index, 
                            dataType: "json",
                            data: forData,
                            // async: false,
                            success: function(result){
                                console.log(result);
                                
                               // var tatal_count = result.total
                               // if(tatal_count != 0 ){
                                    // var non = '<div class="alert alert-success"><p>No Item Found </p></div>';
                                    // $('.msg_show').html(non); 
                                    // window.location="{{url('products/products')}}";        
                                //}else{
                                    $("#total_all").text(result.total);
                               
                                        $.each(result.data, function(i, field){
                                                var il = result.from  + i;  
                                                var url_edit = "{{url('products/products')}}/"+field.id+"/edit"; 
                                                var url_show = "{{url('products/products')}}/"+field.id+"/show"; 
                                                //var  
                                                var in_qty = 0;
                                            $.each(field.inventorys,function(x,fi) {
                                                in_qty += fi.qty;
                                            })                                    
                                                client += "<tr>";
                                                    client += "<td>"+ il +"</td>";
                                                    client += "<td>"+ field.item_bacode +"</td>";
                                                    client += "<td>"+ field.name +"</td>";
                                                    client += "<td> "+ field.csc_name +"</td>";
                                                    client += "<td> "+ field.cost_price +"</td>";
                                                    client += "<td> "+ field.sell_price +"</td>";
                                                    client += "<td> "+ in_qty +"</td>";
                                                    client += "<td>";
                                                    client += "<a href='"+url_show+"' class='btn btn-info btn_show' id='btn_show'>លម្អិត</a>  ";
                                                    client += "<a href='"+url_edit+"' class='btn btn-primary btn_edit' id='btn_edit'>កែប្រែ</a>  ";
                                                    client += "<button  class='btn btn-danger btn_deleted'  value='"+ field.id +"'>លុប</button>";
                                                    client += "</td>";
                                                client += "</tr>";
                                        });
                                    $(".item_list").html(client);	
                                    var page = "";
                                    if(result.prev_page_url === null){
                                        var pr_url = result.current_page;
                                    }else{
                                        var pr_url = result.current_page -1;
                                    }
                                    page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                    for(var x = 1; x <= result.last_page; x ++  ) {
                                        if(result.current_page === x){
                                            page += "<a class='pag active' >"+x+"</a>";
                                        }else{
                                            page += "<a class='pag' >"+x+"</a>";
                                        }
                                    }
                                    if(result.next_page_url === null){
                                        var ne_url = result.current_page;
                                    }else{
                                        var ne_url = result.current_page +1;
                                    }
                                    page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                    $(".pagination").html(page );
                          //  }           
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		}    
        
 
 });       
</script>            
@stop()
