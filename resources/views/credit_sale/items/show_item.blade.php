@extends('credit_sale.layout.master')
@section('contend')	
<div class="container-fluid">

    <div class="row-fluid">

        <!-- block -->

      <div class="block">

          <div class="navbar navbar-inner block-header">
            @if($form != 'report')
              <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span> {{$title}} </div>
            @else
              <div class="muted pull-left"><a href="{!! url('report/item/report_item_list') !!}"> របាយការណ៍ទំនិញ</a> <span class="divider">|</span> {{$title}} </div>
            @endif
          </div>

          <div class="block-content collapse in">
            <ul class="nav nav-tabs">
                <li class="<?php echo(Request::segment(3) == $data_id?'active':'');  ?>"><a href="{{ url('products/products/'.$data_id.'/show') }}">ព័ត៌មានផលិតផល</a></li>
                <li class="<?php echo(last(Request::segments()) == 'stock'?'active':'');  ?>"><a href="{{url('products/products/show/'.$data_id.'/stock')}}">ក្នុងស្តុក</a></li>
            </ul>
							@include('errors.error')
										<div class="span8"  style="margin: 0 auto !important;float: none;">
											<center>

												<h3 class="cen_title"> ការបង្ហាញព័ត៌មានផលិតផល </h3>

											</center>

										</div>

                    <div class="span8" style="margin: 0 auto !important;float: none;">
                          <div class="span3"  style="float: right;">
                            <a href="#" class="btn btn-primary">កែប្រែរ</a>
                          </div>
                          <table class="table table-bordered table-striped">
                            <thead style="background: rgb(251, 205, 205);">
                              <tr>
                              <th></th>
                              <th>បរិយាយ</th>
                              </tr>
                            </thead>
                            <tbody>
                              <tr>
                                <td><b>UPC/EAN/ISBN OR BARCODE ITEM</b></td>
                                <td><p class="item_bacode"></p></td>
                              </tr>
                              <tr>
                                <td><b>Item Number</b></td>
                                <td><p class="add_number_id"></p></td>
                              </tr>
                              <tr>
                                <td><b>ឈ្មោះ</b></td>
                                <td><p class="name"></p></td>
                              </tr>
                              <tr>
                                <td><b>ប្រភេទផលិតផល</b></td>
                                <td>
                                    <p class="cat_name"></p>
                                </td>
                              </tr>
                              <tr>
                                <td><b>ម៉ាកផលិតផល</b></td>
                                <td>
                                    <p class="brand_name"></p>
                                </td>
                              </tr>
                              <tr>
                                <td><b>អ្នកផ្គត់ផ្គង់</b></td>
                                <td> <p class="supp_name"></p></td>
                              </tr>
                             
                              <tr>
                                <td> <b>ទំហំ</b></td>
                                <td> <p class="size"></p></td>
                              </tr>
                              <tr>
                                <td> <b>តម្លៃដើម</b></td>
                                <td> <p class="cost_price"></p></td>
                              </tr>
                              <tr>
                                <td> <b>តម្លៃលក់</b></td>
                                <td> <p class="sell_price"></p></td>
                              </tr>
                              <tr>
                                <td> <b>បញ្ចុះតម្លៃ</b></td>
                                <td> <p class="promo_price"></p></td>
                              </tr>
                              <tr>
                                <td> <b>កាលបរិច្ឆេទចាប់ផ្តើមនៃការបញ្ចុះតម្លៃ</b></td>
                                <td> <p class="start_date"></p></td>
                              </tr>
                              <tr>
                                <td> <b>កាលបរិច្ឆេទបញ្ចប់នៃការបញ្ចុះតម្លៃ</b></td>
                                <td> <p class="end_date"></p></td>
                              </tr>
                              <tr>
                                <td> <b>បរិមាណ</b></td>
                                <td> <p class="quality"></p></td>
                              </tr>
                              <tr>
                                <td> <b>Service</b></td>
                                <td> <p class="is_service"></p></td>
                              </tr>
                              <tr>
                                <td> <b>Default Tax</b></td>
                                <td> <p class="tax_include"></p></td>
                              </tr>
                              <tr>
                                <td> <b>Commission</b></td>
                                <td> <p class="override_default_commission"></p></td>
                              </tr>
                              <tr>
                                <td> <b>Override Default Tax</b></td>
                                <td> <p class="override_default_tax"></p></td>
                              </tr>
                              <tr>
                                <td> <b>សេចក្ដីពិពណ៌នា</b></td>
                                <td> <p class="description"></p></td>
                              </tr>
                              <tr>
                                  <td><b>រូបភាព</b></td>
                                  <td>
                                      <p class="pro_image"></p><br/>
                                  </td>
                              </tr>
                              
                            </tbody>
                            </table>
                    	</div>
					</div>
					
				</div>

                     	<!-- /block -->
    </div>
		    	
	</div>

</div>
<meta name="_token" content="{{ csrf_token() }}" />
	
<script type="text/javascript">

	$(document).ready(function(){
        var sup_id ="{{$data_id}}";
		    var url = "{{ url('products/products') }}"; 
            var url_index1 = url+"/"+sup_id+"/json_edit";
                $.ajax({
                        type: "GET",
                        url: url_index1, 
                        dataType: "json",
                       // data: forData,
                        // async: false,
                        success: function(result){
                            console.log(result);
                            if(result.item_bacode){
                              $('.item_bacode').html(result.item_bacode);
                            }
                            if(result.item_bacode){
                              $('.add_number_id').html(result.add_number_id);
                            }
                            if(result.name){
                              $('.name').html(result.name);
                            }
                            if(result.categorys.name){
                              $('.cat_name').html(result.categorys.name);
                            }
                            if(result.cs_brand.name){
                              $('.brand_name').html(result.cs_brand.name);
                            }
                            if(result.cs_suppliers.name){
                              $('.supp_name').html(result.cs_suppliers.name);
                            }
                            if(result.size){
                              $('.size').html(result.size);
                            }
                            if(result.cost_price){
                              $('.cost_price').html(result.cost_price);
                            }
                            if(result.sell_price){
                              $('.sell_price').html(result.sell_price);
                            }
                            if(result.promo_price){
                              $('.promo_price').html(result.promo_price);
                            }
                            if(result.start_date){
                              $('.start_date').html(result.start_date);
                            }
                            if(result.end_date){
                              $('.end_date').html(result.end_date);
                            }

                            var qty = 0;
                            $.each(result.inventorys, function(k, val){
                              qty += val.qty;
                            });  
                            $('.quality').html(qty);

                            if(result.is_service){
                              $('.is_service').html(result.is_service);
                            }
                            if(result.tax_include){
                              $('.tax_include').html(result.tax_include);
                            }
                            if(result.override_default_commission){
                              $('.override_default_commission').html(result.override_default_commission);
                            }
                            if(result.override_default_tax){
                              $('.override_default_tax').html(result.override_default_tax);
                            }
                            if(result.description){
                              $('.description').html(result.description);
                            }

                            // image
                            var url_image = "{{url('/')}}";
                            if(result.pro_image !== null){
                              $('.pro_image').html("<br/><img class='pro_image' > ");
                              $(".pro_image").attr("src",url_image+"/Account/images/"+result.pro_image);
                            }


                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                });
	});
</script>

@endsection

