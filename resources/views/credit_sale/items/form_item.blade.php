@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('products/products') !!}"> ការគ្រប់គ្រង់ផលិតផល</a> <span class="divider">|</span> បង្កើតផលិតផលថ្មី </div>					
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center>
			                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
			                    </center>
                            	<legend></legend>
                                <form role="form" id="form_insert_items" name="form_insert_items" method="POST"  enctype="multipart/form-data">
                                        {{ csrf_field() }}  
                                            <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">

                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ UPC/EAN/ISBN OR BARCODE ITEM: </label>
                                                        <div class="controls">
                                                            <input type="text" id="item_bacode" class="span12 m-wrap item_bacode" name="item_bacode" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ITEM NUMBER : </label>
                                                        <div class="controls">
                                                            <input type="text" id="add_number_id" class="span12 m-wrap add_number_id" name="add_number_id" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ ឈ្មេាះ <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="name" class="span12 m-wrap name" name="name" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" >​ ប្រភេទផលិតផល: <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <!-- <input type="text" id="category_id" class="span12 m-wrap category_id" name="category_id" data-required="1" autocomplete="off"/> -->
                                                            <select id="select01" class="chzn-select span12 m-wrap category_id"  name="category_id">
                                                                <?php $cat = DB::table('cs_categorys')->where('deleted','=',0)->get();?>
                                                                <option value="">ជ្រើសរើសប្រភេទផលិតផល</option>
                                                                @foreach($cat as $ca)    
                                                                     <option value="{{$ca->id}}">{{$ca->name}}</option>
                                                                @endforeach
                                                                <option value="new_category">New Category</option>
                                                            </select>
                                                        </div><br/>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ម៉ាកផលិតផល​ : </label>
                                                        <div class="controls">
                                                            <!-- <input type="text" id="brand_id" class="span12 m-wrap brand_id" name="brand_id" data-required="1" autocomplete="off"/> -->
                                                            <select id="select01" class="chzn-select span12 m-wrap brand_id"  name="brand_id">
                                                                <?php $brands = DB::table('cs_brand')->where('deleted','=',0)->get();?>
                                                                <option value="">ជ្រើសរើសម៉ាកផលិតផល​</option>
                                                                @foreach($brands as $bra)    
                                                                     <option value="{{$bra->id}}">{{$bra->name}}</option>
                                                                @endforeach
                                                                <option value="new_brand">New Brand</option>
                                                            </select>
                                                        </div><br/>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ អ្នកផ្កត់ផ្កង់ : </label>
                                                        <div class="controls">
                                                            <!-- <input type="text" id="supplier_id" class="span12 m-wrap supplier_id" name="supplier_id" data-required="1" autocomplete="off"/> -->
                                                            <select id="select01" class="chzn-select span12 m-wrap supplier_id"  name="supplier_id">
                                                                <?php $sub_pli = DB::table('cs_suppliers')->where('deleted','=',0)->get();?>
                                                                <option value="">ជ្រើសរើសអ្នកផ្កត់ផ្កង់​</option>
                                                                @foreach($sub_pli as $sub)    
                                                                     <option value="{{$sub->id}}">{{$sub->company_name}}</option>
                                                                @endforeach
                                                                <option value="new_supplier">New Supplier</option>
                                                            </select>
                                                        </div><br/>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ទំហំ : </label>
                                                        <div class="controls">
                                                            <input type="text" id="size" class="span12 m-wrap size" name="size" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ តម្លៃដើម : <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="cost_price" class="span12 m-wrap cost_price" name="cost_price" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ តម្លៃលក់ : <span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <input type="text" id="sell_price" class="span12 m-wrap sell_price" name="sell_price" data-required="1" autocomplete="off"/>
                                                        </div>
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ បញ្ចុះតម្លៃ: </label>
                                                        <div class="controls">
                                                            <input type="text" id="promo_price" class="span12 m-wrap promo_price" name="promo_price" data-required="1" autocomplete="off"/>
                                                        </div>  
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​  កាលបរិច្ឆេទចាប់ផ្តើមនៃបញ្ចុះតម្លៃ: </label>
                                                        <div class="controls">
                                                            <input type="text" id="start_date" class="input-xlarge datepicker span12 m-wrap start_date" name="start_date" data-required="1" autocomplete="off"/>
                                                        </div>  
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ កាលបរិច្ឆេទបញ្ចប់នៃ បញ្ចុះតម្លៃ: </label>
                                                        <div class="controls">
                                                            <input type="text" id="end_date" class="input-xlarge datepicker span12 m-wrap end_date" name="end_date" data-required="1" autocomplete="off"/>
                                                        </div> <br/> 
                                                        <div class="controls">
                                                            <lable class="span4 m-wrap ">Is Service: </lable>
                                                           
                                                            <input class="is_service"  type="checkbox"  name="is_service" id="optionsCheckbox" value="1">
                                                          
                                                        </div><br/>
                                                        <div class="controls">
                                                            <lable class="span4 m-wrap "> Include Default Tax:</lable>
                                                           
                                                            <input class="tax_include" type="checkbox"  name="tax_include" id="optionsCheckbox" value="1">
                                                           
                                                        </div><br/>
                                                        <div class="controls">
                                                            <lable class="span4 m-wrap "> Override Default Commission:</lable>
                                                            
                                                            <input class="override_default_commission" type="checkbox" name="override_default_commission"  id="optionsCheckbox" value="1" >
                                                            
                                                                <div class="comission_is">
                                                                <br/>
                                                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Commission : </label>
                                                                    <div class="controls">
                                                                        <input type="text" id="commission" class="span12 m-wrap commission" name="commission" data-required="1" autocomplete="off"/>
                                                                    </div>
                                                                    <select class="span3 m-wrap commission_type" name="commission_type"> 
                                                                        <option value="fixed_amount"> Fixed Amount</option>
                                                                        <option value="precent_amount"> Precent Amount</option>
                                                                    </select>
                                                                </div>
                                                        </div>
                                                        <br/>
                                                       
                                                        
                                                        <div class="controls">
                                                            <lable class="span4 m-wrap "> Override Default  Tax:</lable>
                                                            
                                                            <input class="override_default_tax" type="checkbox" id="optionsCheckbox" value="1" name="override_default_tax">
                                                            <br/>
                                                            <br/>
                                                            
                                                             <div class="over_tax_is">
                                                                    <div class="span6">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax Name1 : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax1" class="span12 m-wrap tax1" name="tax[]" value="Tax 1" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="span6">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax1 (%)  : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax1" class="span12 m-wrap tax_price1" name="tax_num[]" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="span6" style="margin-left:0px">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax Name2 : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax2" class="span12 m-wrap tax2" name="tax[]" value="Tax 2"  data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="span6">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax2(%)  : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax_price2" class="span12 m-wrap tax_price2" name="tax_num[]" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="span6" style="margin-left:0px">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax Name3 : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax3" class="span12 m-wrap tax3" name="tax[]" value="Tax 3" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="span6">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax3(%)  : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax_price3" class="span12 m-wrap tax_price3" name="tax_num[]" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="span6" style="margin-left:0px">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax Name4 : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax4" class="span12 m-wrap tax4" name="tax[]" value="Tax 4" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="span6">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax4(%)  : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax_price4" class="span12 m-wrap tax_price4" name="tax_num[]" data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div>
                                                                    <div class="span6" style="margin-left:0px">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax Name5 : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax5" class="span12 m-wrap tax5" name="tax[]" data-required="1" value="Tax 5" autocomplete="off"/>
                                                                        </div>
                                                                    </div> 
                                                                    <div class="span6">
                                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ Tax5(%)  : </label>
                                                                        <div class="controls">
                                                                            <input type="text" id="tax_price5" class="span12 m-wrap tax_price5" name="tax_num[]"  data-required="1" autocomplete="off"/>
                                                                        </div>
                                                                    </div>  
                                                                </div>
                                                           
                                                        </div> <br/>

                                                        <label class="control-label w"> ​ ជ្រើសរើសរូបភាព: </label>
                                                        <div class="controls">
                                                            <input type="file" name="images" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត"/>
                                                        
                                                            <p id="images_get"></p>
                                                        <br/>
                                                        <div class="qty_ser">
                                                            <label class="control-label" >​Quantity: </label>
                                                            <div class="controls">
                                                                <input type="text" id="qty" class="span12 m-wrap qty" name="qty" data-required="1" autocomplete="off"/>  
                                                            </div>    
                                                        </div>

                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" >​ សេចក្ដីពិពណ៌នា:</label>
                                                        <div class="controls">
                                                            <textarea name="description"  data-required="1" rows="5" class="span12 m-wrap description"></textarea>
                                                        </div>
                                        
                                                    </div>
                                                    <?php
                                                     if(isset($_REQUEST["redirect"]) ){
                                                        
                                                        echo ' <input type="hidden" name="redirect" id="redirect"  value="'.$_REQUEST["redirect"].'" />';
                                                    }
                                                    if(isset($_REQUEST["sales"]) ){
                                                        echo ' <input type="hidden" name="sales" id="sales" value="'.$_REQUEST["redirect"].'" />';
                                                    }
                                                    ?>
                                                   
                                                    <div class="span12" style="margin-left:0;">
                                                        <br/>
                                                            <center>
                                                                    <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">Submit</button>
                                                                    <input type="hidden" id="item_id" name="item_id" value="0">
                                                                    <button  class="btn btn-danger get_back">Back</button>
                                                            </center>  
                                                        <br/>
                                                    
                                                    </div>
                                            </div>
                                        </form>
			    			</div>
                        <!-- Edit Supplier -->

						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
             // Check Is Service 
             var is_service = $('.is_service');
                    is_service.attr('checked');
                    if(is_service.prop('checked') == true){
                        $(".qty_ser").css("display","none");
                    }else{
                        $(".qty_ser").removeAttr("style");
                    }
                    $('.is_service').click(function(){
                        if(is_service.prop('checked') == true){
                            $(".qty_ser").css("display","none");
                        }else{
                            $(".qty_ser").removeAttr("style");
                        }
                    });
            // Override Default Commission 
                    var is_com = $('.override_default_commission');
                    //is_com.attr('checked');
                    
                    if(is_com.is(':checked') == true){
                        $(".comission_is").removeAttr("style");
                    }else{
                    
                        $(".comission_is").css("display","none");
                    }

                    $('.override_default_commission').click(function(){
                        if(is_com.is(':checked') == true){
                            $(".comission_is").removeAttr("style");
                        }else{
                            $(".comission_is").css("display","none");
                        }
                    });   
            // Override Default Tax 
            var is_tax = $('.override_default_tax');
                // is_tax.attr('checked');
                        if(is_tax.is(':checked') == true){
                            $(".over_tax_is").removeAttr("style");
                        }else{
                            $(".over_tax_is").css("display","none");
                        }
                    $('.override_default_tax').click(function(){
                        if(is_tax.is(':checked') == true){
                            $(".over_tax_is").removeAttr("style");
                        }else{
                            $(".over_tax_is").css("display","none");
                        }
                    });


$(document).ready(function(){

        @if(Request::is('products/products/create'))
		    var url = "{{ route('products/products/create') }}";
		@else
            var sup_id ="{{$data_id}}";
		    var url = "{{ url('products/products') }}"; 
            var url_index1 = url+"/"+sup_id+"/json_edit";
                $.ajax({
                        type: "GET",
                        url: url_index1, 
                        dataType: "json",
                       // data: forData,
                        // async: false,
                        success: function(result){
                            console.log(result);

                           // Category 
                            $('.category_id').find("[value='"+result.category_id+"']").attr("selected","selected");
                            var name_cate =  $('.category_id').find("[value='"+result.category_id+"']").text();
                                if(result.category_id !== null){
                                    $(".category_id").next().find("span").text(name_cate);
                                    $('.category_id').next().find("a").addClass("chzn-single");
                                    $('.category_id').next().find("a").removeClass("chzn-default");
                                }	
                            //Supplier
                            $('.supplier_id').find("[value='"+result.supplier_id+"']").attr("selected","selected");
                            var name_sub =  $('.supplier_id').find("[value='"+result.supplier_id+"']").text();
                                if(result.supplier_id !== null){
                                    $(".supplier_id").next().find("span").text(name_sub);
                                    $('.brasupplier_idnd_id').next().find("a").addClass("chzn-single");
                                    $('.supplier_id').next().find("a").removeClass("chzn-default");
                                }    
                            // Brand    
                            $('.brand_id').find("[value='"+result.brand_id+"']").attr("selected","selected");
                            var name_bran =  $('.brand_id').find("[value='"+result.brand_id+"']").text();
                                if(result.brand_id !== null){
                                    $(".brand_id").next().find("span").text(name_bran);
                                    $('.brand_id').next().find("a").addClass("chzn-single");
                                    $('.brand_id').next().find("a").removeClass("chzn-default");
                                }     
                            
                            $(".item_bacode").val(result.item_bacode); 
                            $(".name").val(result.name);
                            $(".add_number_id").val(result.add_number_id);    
                            $(".size").val(result.size);
                            $(".cost_price").val(result.cost_price);
                            $(".sell_price").val(result.sell_price);
                            $(".promo_price").val(result.promo_price);
                            
                            $(".start_date").val(result.start_date);
                            $(".end_date").val(result.end_date);
                        if(result.is_service != null){           
                            $(".is_service").val(result.is_service);
                            $(".is_service").attr('checked','checked');
                            $(".qty_ser").css("display","none");
                        } 
                        if(result.tax_include != null){           
                            $(".tax_include").val(result.tax_include);
                            $(".tax_include").attr('checked','checked');
                        }
                        if(result.override_default_tax != null){           
                            $(".override_default_tax").val(result.override_default_tax);
                            $(".override_default_tax").attr('checked','checked');
                            $(".over_tax_is").removeAttr("style");

                            $.each(result.qty_re.tax_over_item, function (i ,field){
                                var i_k = i + 1;
                                $('.tax'+i_k).val(field.name);
                                $('.tax_price'+i_k).val(field.percent);
                               
                            });
                        } 
                        if(result.override_default_commission != null){           
                            $(".override_default_commission").val(result.override_default_commission);
                            $(".override_default_commission").attr('checked','checked');
                            $(".comission_is").removeAttr("style");
                            if(result.commission_fixed != 0){
                                $(".commission").val(result.commission_fixed);
                                $('.commission_type').find("[value='fixed_amount']").attr("selected","selected");
                            }else if(result.commission_give != 0){
                                $(".commission").val(result.commission_give);
                                $('.commission_type').find("[value='precent_amount']").attr("selected","selected");
                            }
                        }   
                            $(".description").val(result.description);
                            $(".qty").val(result.qty_re.qty);

                          var im = result.pro_image;
                          var url_image = "{{url('/')}}";	
                            if(im !== null){
                            
                                $('#images_get').html("<br/><img class='client_upload_image_respon' > ");
								$(".client_upload_image_respon").attr("src",url_image+"/Account/images/"+im);
						    } 

                             $('#item_id').val(result.id);
                             $('#btn-save').val("edit");
                            
														     
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		@endif 
        $('#btn-save').click(function(e){
            
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    })
                    e.preventDefault();
                    var btn_id = $(this).val();
                    //var url_index = "{{ route('products/products/create') }}";  
                   if(btn_id == "edit"){
                       var item_id = $('#item_id').val();
                       var url = "{{ url('products/products') }}";
                        var url_index = url+"/"+item_id+"/edit";
                   }else{
                       var url_index = "{{ route('products/products/create') }}"; 
                       var item_id = 0;
                   }
                  
                    var form = document.forms.namedItem("form_insert_items"); 
                    var formData = new FormData(form); 
                 
                    $.ajax({
                        type:'POST',   
                        url: url_index,
                        dataType: 'json', 
                        contentType: false,
                        data: formData,
                        processData: false,
                        success: function(result){
                            console.log(result);
                               
                                if(result.redirect == "purchase_order"){
                                        var post_url = "{{url('post_item_add_to_cart_ajax_purchas_order')}}"; 
                                        var reditrect_url = "{{url('credit_sales_purchas_order')}}";
                                        var done = function() {
                                            window.location.href = reditrect_url;
                                       }; 
                                       var formdata = {
                                            item_id: result.datas.id,
                                            name: result.datas.name,
                                            sell_price: result.datas.cost_price
                                        };
                                        $.post(post_url, formdata , done);      
                                }else if(result.redirect == "credit_sales"){
                                     var sales = $("#sales").val();
                                    var post_url = "{{url('post_item_add_to_cart_ajax')}}"; 
                                        var reditrect_url = "{{url('credit_sales')}}?redirect=credit_sales&sales="+sales;
                                        var done = function() {
                                            window.location.href = reditrect_url;
                                       }; 
                                       var formdata = {
                                            item_id: result.datas.id,
                                            name: result.datas.name,
                                            redirect: 'sale'
                                        };
                                        $.post(post_url, formdata , done);   
                                }else{
                                    if(btn_id == "add"){

                                        // window.scrollTo(0, 0);
                                        // $("#item_id").val(0);
                                        // $("#btn-save").val("add");
                                        // $('#form_insert_items').trigger("reset");
                                        // $('.msg_show').html(result.msg_show); 
                                        // $('.filename').text("No file selected");
                                        // $(".qty_ser").removeAttr("style");
                                        // $(".comission_is").css("display","none");
                                        // $(".over_tax_is").css("display","none");
                                        // $('.is_service').parents("span").removeClass('checked');
                                        // $('.tax_include').parents("span").removeClass('checked');
                                        // $('.override_default_commission').parents("span").removeClass('checked');
                                        // $('.override_default_tax').parents("span").removeClass('checked');
                                        // $(".category_id").next().find("span").text("ជ្រើសរើសប្រភេទផលិតផល");
                                        // $(".brand_id").next().find("span").text("ជ្រើសរើសម៉ាកផលិតផល​");
                                        // $(".supplier_id").next().find("span").text("ជ្រើសរើសអ្នកផ្កត់ផ្កង់​");
                                        // //$('.override_default_tax').parent();
                                             window.location="{{url('products/products')}}";   
                                    }else{
                                        window.location="{{url('products/products')}}"; 
                                    }
                                        
                                }
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }                   
                    });
            });  

 }); 
      
</script>            
@stop()
