@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
    <div class="row-fluid">
            <!-- validation -->
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                        <div class="muted pull-left"><a href="{!! url('comment_finish') !!}"> ការបញ្ចប់ឥណទាន</a> <span class="divider">|</span> ការបង្កើតការបញ្ចប់ឥណទាន </div>					
                </div>

                <div class="block-content collapse in">
                @include('errors.error')	
                    <center>
                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
                    </center>
                    <legend></legend>
                    <form id="" name="" method="POST"  enctype="multipart/form-data">
                        {{ csrf_field() }} 
                            <div class="span8"  style="margin: 0 auto !important;float: none;">
                                <!-- <div class="span12" style="margin-left:0;">
                                    <table class="table table-bordered">
                                    <thead>
                                        <tr>
                                        <th>ឈ្មោះទំនិញ</th>
                                        <th>លេខ​កូដ​</th>
                                        <th>ម៉ាក</th>
                                        <th>ចំនួន</th>
                                        <th>តម្លៃ​ឯកតា</th>
                                        <th>តម្លៃសរុប</th>
                                        </tr>
                                    </thead>
                                    <tbody class="item_list"> -->
                                        
                                    <!-- </tbody>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"> តម្លៃសរុប </td>
                                                <td> <b class="prices_total_num_text"></b> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"> ប្រភេទនៃការទិញ </td>
                                                <td> <b class="method"></b> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="text-align: right;"> ប្រាក់កក </td>
                                                <td> <b class="deposit_fixed_text"></b> </td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="text-align: right;">  រយៈពេលបង់ប្រាក់</td>
                                                <td> <b class="duration_pay_money_text"></b>  / <b class="duration_pay_money_type_text"></b></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="text-align: right;">  សុំបង់ថ្ងៃទី</td>
                                                <td> <b class="date_for_payments_text"></b></td>
                                            </tr>
                                            <tr>
                                                <td colspan="5" style="text-align: right;">ចំនួនទឹកប្រាក់</td>
                                                <td> <b class="money_owne_text"></b> </td>
                                            </tr>

                                    </table>  
                                    <br/>
                            <legend></legend>  -->
                                <!-- </div> -->
                                <div class="span12" style="margin-left:0;">
                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ សេចក្តីបញ្ជាក់របស់គណៈកម្មការឥណទាន<span class="required" title="This place you must be put data">*</span></label>
                                    <div class="controls">
                                        <textarea name="command_finish" data-required="1" rows="3" id="command_finish" class="span12 m-wrap command_finish"></textarea>
                                    </div>
                                </div>
                                <label class="control-label">កាលបរិច្ឆេទបង់ប្រាក់ចុងក្រោយ<span class="required">*</span></label>
                                    <div class="controls">
                                        <input type="text" autocomplete="off" name="date_last_payment" class="span12 m-wrap input-xlarge datepicker last_date_payment" value="" id="date01"/>
                                    </div>
                                <label class="control-label">ការបញ្ចប់ឥណទាន<span class="required">*</span></label>
                                <select name="status_of_finish" class="span12 m-wrap status_of_finish">
                                        <option value="1">ល្អ</option>
                                        <option value="2">ល្អបង្គួរ</option>
                                        <option value="3">មធ្យម</option>
                                        <option value="4">មិនល្អ</option>                                                                     
                                </select>
                                <div class="span12" style="margin-left:0;">
                                    <label class="control-label" >​ សេចក្ដីពិពណ៌នា:</label>
                                    <div class="controls">
                                        <textarea name="note"  data-required="1" rows="5" class="span12 m-wrap description"></textarea>
                                    </div>
                                    
                                </div>
                                <div class="span12" style="margin-left:0;">
                                    <br/>
                                        <center>
                                                <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="{{$comment_id}}">បញ្ចូល</button>
                                                <input type="hidden" id="comment_id" name="comment_id" value="0">
                                                <button  class="btn btn-danger get_back">ត្រលប់</button>
                                        </center>  
                                    <br/>
                                
                                </div>
                            </div>
                        </form>
                    </div>
                <!-- Edit Supplier -->

                </div>
                <!-- /block -->
            </div>
                <!-- /validation -->
        </div>
    </div>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
    function day_format_show(date_format){
        var d = new Date(date_format);

        var year_n = d.getFullYear();
        var month_n = d.getMonth() + 1;
        var day_n = d.getDate();
        if(month_n > 10){
            month_n = month_n;
        }else{
            month_n = "0"+month_n; 
        }
        
        if(day_n > 10){
            day_n = day_n;
        }else{
            day_n = "0"+day_n; 
        }

        return  day_n +"-"+month_n+"-"+year_n;
    }
    var comment_id = "?request_id={{$comment_id}}";
    var url = "{{url('comment_finish/create/json/')}}";
    $.ajax({
        url: url + comment_id,
        success: function(result){
            console.log(result);
            $.each(result, function(k, v){
                // v.comment_finish !== null ? $("#comment").val("edit") : $("#comment").val("create");
                if(v.comment_finish !== null){
                    $("#comment_id").val(v.comment_finish.id);
                    $("#command_finish").val(v.comment_finish.command_finish);
                    $(".last_date_payment").val(v.comment_finish.date_last_payment);
                    $(".status_of_finish option[value=" + v.comment_finish.status_of_finish + "]").attr('selected', 'selected');
                    $(".description").val(v.comment_finish.note);
                }
                $.each(v.cs_schedule_timesheet_pay, function(c, p){
                    if(v.cs_schedule_timesheet_pay.length == (c + 1)){
                        $(".last_date_payment").val(day_format_show(p.date_payment));
                    }
                });
            });
        },
    });

    var url_post = "{{url('comment_finish/post/json')}}";
    $("#btn-save").click(function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault();
        $.ajax({
            url: url_post + comment_id,
            type: 'post',
            data: {
                'comment_id' : $("input[name=comment_id]").val(),
                'command_finish' : $("textarea[name=command_finish]").val(),
                'date_last_payment' : $("input[name=date_last_payment]").val(),
                'status_of_finish' : $("select[name=status_of_finish]").val(),
                'note' : $("textarea[name=note]").val(),
            },
            
            success: function(result,status,xhr){
                location.href = "{{url('comment_finish')}}";
            },
            error: function(error){
                console.log(error.responseText);
            }
        });
    });
</script>            
@stop()