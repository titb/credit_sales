@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
    <div class="row-fluid">
            <!-- validation -->
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <center>
                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
                    </center>
                </div>

                <div class="block-content collapse in">
                    <div class="span12" style="margin: 0 auto !important;float: none;">								
                        <ul class="nav nav-tabs">
                            <li class="<?php if(Request::segment(1) == 'comment_finish'){echo "active";} ?>"><a href="{{url('comment_finish')}}">ការបញ្ចប់ឥណទាន</a></li>
                            <li class="<?php if(Request::segment(1) == 'comment_error'){echo "active";} ?>"><a href="{{url('comment_error')}}">ការបញ្ចប់ឥណទានមិនត្រឹមត្រូវ</a></li>
                        </ul>								
                    </div>

                    @include('errors.error')
                    <table class="table table-bordered">

                        <thead style="background: rgb(251, 205, 205);">

                            <tr class="header">

                                <th>ល.រ</th>
                                <th>សំណើ​រ ល.រ</th>
                                <th>ឈ្មេាះអតិថិជន</th>
                                <th>ចំនួនទំនិញ</th>
                                <th>តម្លៃសរុប</th>
                                <th>ការបរិច្ឆេតយល់ព្រម</th>
                                <!-- <th>ប្រភេទនៃការបញ្ចប់</th> -->
                                <th>សកម្មភាព</th>

                            </tr>
                        </thead>
                        <tbody class="item_list">
                               
                        </tbody>
                            <tr>
                                <td colspan="6">
                                    <b class="pull-right">សរុប:</b>
                                </td>
                                <td>
                                    <b id="total_all"> </b>
                                </td>
                            </tr>
                        </table>
                        <!-- Pagination -->
                        <div class="pagination text-right"></div>
                    </div>
                </div>
                <!-- /block -->
            </div>
            <!-- /validation -->
        </div>
    </div>
</div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<script>

    $.ajax({
        url: "{{url('comment_error/json')}}",
        type: 'get',
        success: function(result){
            // console.log(result);
            var txt;
            var key = 0;
            $.each(result, function(k,v){
                if(v.comment_finish !== null && v.comment_finish.status_of_finish == 1){
                    console.log(v);
                    txt += '<tr>';
                    txt += '<td>' + (k+1) + '</td>';
                    txt += '<td>' + v.approval_id + '</td>';
                    txt += '<td>' + v.cs_client.kh_username + '</td>';
                    var qtys = 0;
                    $.each(v.cs_approvalcredit.approval_item, function(kq,q){
                        qtys += q.qty;
                    });
                    txt += '<td>' + qtys + '</td>';
                    txt += '<td>' + v.money_owne_total_pay + '</td>';
                    txt += '<td>' + v.cs_approvalcredit.date_approval + '</td>';
                    txt += '<td>'
                                + '<a href="" class="btn btn-info">លម្អិត</a>' +
                            '</td>';
                    // txt += '<td>' + v.approval_id + '</td>';
                    txt += '</tr>';
                    key += (k-k+1);
                }else{
                    $(".item_list").html("<tr><td>មិនមានទិន្នន័យ</td></tr>");
                }
            });
            $(".item_list").append(txt);
            $("#total_all").html(key);
        },
        error: function(error){
            console.log(error.responseText);
        }
    });
    
</script>          
@stop()
