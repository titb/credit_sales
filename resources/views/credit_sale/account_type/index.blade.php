@extends('credit_sale.layout.master')

@section('contend')

<?php

header('Cache-Control: no-store, no-cache, must-revalidate, post-check=0, pre-check=0, max-age=0');

?>

<div class="container-fluid">

    <div class="row-fluid">



                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                            		<form class="form-search" style="margin-bottom:2px;" action="{{URL::to('account_type')}}" method="GET">

											<?php $account_type = DB::table('account_type')->where('deleted','=',0)->get(); ?>

											<input type="text" name="search" data-required="1" placeholder="ឈ្មោះ" class="span3 m-wrap" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($account_type as $row)"{{$row->name}}",@endforeach""]'>

											<button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> ស្វែងរក</button>

											<div class="muted span3 pull-right" style="padding-top: 0px;"><a href="{{ url('account_type/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>

                            		</form>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if ($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            	<div class="12">



	                            	<center>

	                            		<h3 class="cen_title khmer_Moul"> ការគ្រប់គ្រងប្រភេទអតិថិជន</h3>



	                            	</center>

	                            	<legend></legend>

	                            	</div>

	                            <style>

	                            	table.table.table-bordered thead tr th{

	                            		text-align: center;

	                            	}

	                            	table.table.table-bordered tbody tr td{

	                            		text-align: center;

	                            	}

	                            </style>

                            	<table class="table table-bordered">

						              <thead style="background: rgb(251, 205, 205);">

						                <tr>

						                  <th>លេខសម្គាល់</th>

						                  <th>ឈ្មោះ</th>

						                  <th>ការកំណត់សម្គាល់</th>

						                    <th>សកម្មភាព</th>

						                </tr>

						              </thead>

						              <tbody>

						        @foreach($data as $key => $d)

						                <tr>



						                  <td>
						                  	<?php
								                $invID = $d->id;
								                $invID = str_pad($invID, 6, '0', STR_PAD_LEFT);
								                echo $invID;
											?>
										  </td>

						                   <td>{{ $d->name }}</td>
						                  <td>{{ $d->description}}</td>


						                  <td>

						                  		<a href="{!! url('account_type/'.$d->id.'/edit') !!}" class="btn btn-primary">កែប្រែ</a>

						                  	   <form action="{{ url('account_type/'.$d->id.'/deleted') }}" method="post" style="display: inline-block;">
													<input type="hidden" name="_token" value="{{ csrf_token() }}">
													<button type="submit" class="btn btn-danger">លុប</button>
												</form>


						                  </td>

						                </tr>

						        @endforeach

						              </tbody>

						            </table>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

	</div>

</div>





@endsection
