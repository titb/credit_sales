@extends('credit_sale.layout.master')
@section('contend')
<div class="container-fluid">
    <div class="row-fluid">
                         <!-- block -->

                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="{!! url('account_type') !!}">សាខា</a></div>
                               <!--  <div class="muted pull-right"style="padding-top: 0px;"><a href="{{ url('accounts/create') }}" class="btn btn-success"><i class="icon-pencil icon-white"></i> New</a></div> -->
                           </div>
                            <div class="block-content collapse in">
                              @if (count($errors) > 0)
						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>
						            <ul>
						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if ($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            	<div class="span12">

	                            	<center>

	                            		<h3 class="cen_title"> បង្កើតការគ្រប់គ្រងប្រភេទអតិថិជន</h3>

	                            	</center>

                            	</div>



                            <div class="span12" style="margin-left:0;">

                            	<form action="{{ url('account_type/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}


									<div class="span6">

										<label class="control-label">ឈ្មោះ<span class="required">*</span></label>

					  					<div class="controls">

					  						<input type="text" id="name" class="span12 m-wrap" name="name" data-required="1" value="{{ old('name') }}"/>

					  					</div>
									</div>

									<div class="span6" style="margin-left:0;">
										<label class="control-label">បរិយាយ<span class="required">*</span></label>
					  					<div class="controls">
					  						<input type="text" id="description" class="span12 m-wrap" name="description" data-required="1" value="{{ old('description') }}"/>

					  					</div>

									</div>

				  					<br/>

									<div class="span12" style="margin-left:0; margin-top:15px;">

										<center>

											<button type="submit" class="btn btn-success">បញ្ចូល</button>

										</center>

									</div>

								</form>

							</div>



			    			</div>

						</div>
                     	<!-- /block -->
		    		</div>
	</div>

</div>



<script type="text/javascript">

	/*$(document).ready(function(){

		$('#add').click(function(){

			var img = '<div class="controls" id="addimage">' +

				  	'<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

				  	'</div>';

			$('#addimage').append(img);

		});

	});*/

</script>



@endsection
