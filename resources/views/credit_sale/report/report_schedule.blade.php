@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >
    
<!-- validation -->
<div class="row-fluid">
    
    <!-- block -->
    <div class="block">

        @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 3px;">
                <div class="muted pull-left" style="padding-top: 5px;">Report Repayment Back</div>
                <a href="{{url('export_report')}}" class="btn btn-primary b_search pull-right"  value="b_search">បញ្ចូលទៅកាន់ Excel</a>
                <a href="{{url('export_report')}}" class="btn btn-danger pull-right"  value="b_search" style="margin-right: 4px;">ព្រីន</a>
            </div>
            
            
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = "{{url('report/report_schedules')}}";
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12" style="margin-left: 0px;">
                    

                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដតារា</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>ចំនួនទឹកប្រាក់</th>
                            <th>ប្រាក់បង់តំបូង៣០%</th>
                            <th>ប្រាក់ដែលនៅជំពាក់</th>
                            <th>រយៈពេលសង</th>
                            <th>ថ្ងៃទទួលទំនិញ</th>
                            <th>បុគ្គលិកប្រមូលប្រាក់</th>
                            <th>រូបិយប័ណ្ណ</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                    <div class="pagination text-right"></div>
                </div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

    $(document).ready(function(){
        var search_url = "{{url('report/search/schedule')}}";
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
	});
    function appendData(result){
        var text = "";
            $.each(result.data, function(k,sche){
                var sche_id = sche.id;
                var url_sch = "{{ url('aprove_credit_sales') }}/"+sche_id+"/generate_credit_sale";
                var cur = result.current_page * 10 - 10;
                var il = (1 + k) + cur;
                text +="<tr>";
                    text +="<td>"+il+"</td>";
                    text +="<td><a href='"+url_sch+"'>"+sche.id+"</a></td>";
                    text +="<td>"+sche.cs_client.kh_username+"</td>";
                    text +="<td>"+sche.money_owne_total_pay+"</td>";
                    text +="<td>"+sche.cs_approvalcredit.money_owne+"</td>";
                    text +="<td>"+sche.money_owne_interest+"</td>";
                    text +="<td>"+day_format_show(sche.cs_approvalcredit.date_give_product)+"</td>";
                    text +="<td>"+day_format_show(sche.cs_approvalcredit.date_approval)+"</td>";
                    text +="<td>"+sche.cs_staff.name_kh+"</td>";
                    text +="<td>"+sche.currency.value_option+"</td>";
                text +="</tr>";
            });
            $('#list_item').html(text); 
            var page = "";
            if(result.prev_page_url === null){
                var pr_url = result.current_page;
            }else{
                var pr_url = result.current_page -1;
            }
            page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
            for(var x = 1; x <= result.last_page; x ++  ) {
                if(result.current_page === x){
                    page += "<a class='pag active' >"+x+"</a>";
                }else{
                    page += "<a class='pag' >"+x+"</a>";
                }
            }
            if(result.next_page_url === null){
                var ne_url = result.current_page; 
            }else{
                var ne_url = result.current_page +1;
            }
            page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
            $(".pagination").html(page );
        }
        
</script>
@endsection
