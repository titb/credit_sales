@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = url('report/sales/report_direct_payment?reset=reset');
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12"  style="margin-left: 0px;" id="report-incom"> 
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        }

                        table {

                        width: 100%;

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            font-size: 10px;

                        }

                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                    <table class="table table-bordered table-print"  cellpadding="0" cellspacing="0" >
                        <thead  style="background: rgb(251, 205, 205);">
                        <tr class="header"> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>បរិមាណទំនិញ</th>
                            <th>ការទូទាត់សរុបទាំងអស់</th>
                            <th>បញ្ចុះតម្លៃ</th>
                            <th>ការទូទាត់សរុប</th>
                            <th>តម្លៃនៃទំនិញដែលបានលក់</th>
                            <th>ប្រាក់ចំណេញ</th>   
                            <th>ប្រភេទបង់ប្រាក់</th>
                            <th>មូលហេតុបញ្ចុះតម្លៃ</th>
                            <th>បុគ្គលិកប្រមូលប្រាក់</th>
                            <th>កាលបរិច្ឆេទបង្កើត</th>
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                </div>
                <div class="pagination text-right"></div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">
	$(document).ready(function(){
        var search_url = "{{url('report/search/sale_by_cash')}}";
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
	});
    function appendData(result){
        console.log(result);
        
        var text = '';
        var out = "";
        
        $.each(result.data, function(k,sale){
            var cur = result.current_page * 10 - 10;
            var il = (1 + k) + cur;
            var st_p = ''; 
            if(sale.sale_transaction_pay){
                $.each(sale.sale_transaction_pay, function(i,b){
                    st_p += " <b style='text-transform: capitalize;'>"+b.payment_pay+"</b> : "+b.account_mount+"  "+b.currency.name_kh+"<br/>";
                });
            }
            var cl = "";
            if(sale.cs_client){
                cl = sale.cs_client.kh_username;
            }else{
                cl = "No Customer";
            }
            var profit = 0;
            text +="<tr>";
                text +="<td>"+il+"</td>";
                text +="<td>"+sale.num_invoice+"</td>";
                text +="<td>"+cl+"</td>";
                var total_qty = sale.sale_payment!==null?sale.sale_payment.total_qty:'';
                text +="<td>"+total_qty+"</td>";
                text +="<td>"+accounting.formatMoney(sale.sale_payment.sub_payment_amount)+"</td>";
                text +="<td>"+accounting.formatMoney(sale.sale_payment.discount_payment)+"</td>";
                text +="<td>"+accounting.formatMoney(sale.sale_payment.total_payment)+"</td>";
                text +="<td>"+accounting.formatMoney(sale.sale_payment.dute_payment_amount)+"</td>";
                text +="<td>"+profit+"</td>";
                text +="<td>"+st_p+"</td>";
                text +="<td>"+sale.sale_payment.discount_reason+"</td>";
                text +="<td>"+sale.user.name_kh+"</td>";
                text +="<td>"+day_format_show(sale.sale_payment.payment_date)+"</td>";
            text +="</td>";
        });
        $('#list_item').html(text);
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }
function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }


</script>

@endsection
