
<div class="span12">
    <div class="span2">
        <lable>&nbsp;</lable>
        <select class="span12 m-wrap currency" name="time" id="times">
            <option value="null">ជ្រើសរើសរយៈពេល</option>
            <option value="today">Today</option>
            <option value="yesterday">Yesterday</option>
            <option value="l7day">Last 7 Days</option>
            <option value="l30day">Last 30 Days</option>
            <option value="week">This Week</option>
            <option value="lweek">Last Week</option>
            <option value="month">This Month</option>
            <option value="lmonth">Last Month</option>
            <option value="quarter">This Quarter</option>
            <option value="lquarter">Last Quarter</option>
            <option value="year">This Year</option>
            <option value="lyear">Last Year</option>
            <option value="all">All Time</option>
            <option value="custom">Custom Date Range</option>
        </select>
    </div>
    
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $brand_id = App\Branch::where('status','=',1)->where('deleted','=',0)->get(); ?>
            <select id="brand_name" class="span12 m-wrap brand_name" name="brand_name" >
            <option value="null">ជ្រើសរើសសាខា</option>
            @foreach($brand_id as $br)
            <option value="{{$br->id}}">{{$br->brand_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $currency1 = App\Currency::where('deleted','=',1)->get(); ?>
        <select id="currency" class="span12 m-wrap currency" name="currency" >
            <option value="null">ជ្រើសរើសរូបិយប័ណ្ណ</option>
            @foreach($currency1 as $br)
            <option value="{{$br->id}}">{{$br->name_kh}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $client_name1 = App\Cs_Client::where('deleted','=',0)->get(); ?>
        <select id="client_name" class="span12 m-wrap client_name" name="client_name" >
            <option value="null">ជ្រើសរើសអតិថិជន</option>
            @foreach($client_name1 as $br)
            <option value="{{$br->id}}">{{$br->kh_username}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php 
            $sale_id1 = App\Cs_Schedule::where('deleted','=',1)->get(); 
            // if(Request()->url('*/report_item_list'))$sale_id1 = App\Cs_Items::where('deleted','=',0)->get();
        ?>
        <select id="sale_id" class="span12 m-wrap sale_id" name="sale_id" >
            <option value="null">ជ្រើសរើសលេខកូដ</option>
            @foreach($code as $co)
            <option value="{{$co['code']}}">{{$co['code']}}</option>
            @endforeach
        </select>
    </div>
        <!-- <div class="span10"  style="margin-left: 0px;"> -->
            <div class="span2"> <!-- style="margin-left: 0px; width: 200px;" -->
                <lable>&nbsp;</lable>
                <?php $staff_name1 = App\User::where('deteted','=',1)->get(); ?>
                <select id="staff_name" class="span12 m-wrap staff_name" name="staff_name" >
                    <option value="null">ជ្រើសរើសបុគ្គលិក</option>
                    @foreach($staff_name1 as $br)
                    <option value="{{$br->id}}">{{$br->name_kh}}</option>
                    @endforeach
                </select>
            </div>
        <!-- </div> -->
        <div class="span2 custom_duration" style="display: none;margin-left: 0px;">
        <lable>&nbsp;</lable>
        <input type="text" name="from_date" id="from_date" placeholder="ពីថ្ងៃទី" data-required="1" class="input-xlarge datepicker span12 m-wrap from_date" autocomplete="off">
    </div>
    <div class="span2 custom_duration" style="display: none;">
        <lable>&nbsp;</lable>
        <input type="text" name="to_date" id="to_date" placeholder="ដល់ថ្ងៃទី" data-required="1" class="input-xlarge datepicker span12 m-wrap to_date" autocomplete="off">
    </div>
        <div class="span2 pull-right" style="padding-bottom: 5px;"><!--  -->
            <lable>&nbsp;</lable><br/>
            <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search">ស្វែងរក</button>
            <a href="" class="btn btn-danger" >Reset</a>
        </div>
    <!-- </div> -->
</div>
<script>
    $("#times").change(function(){
        if($(this).val() == 'custom'){
            $(".custom_duration").css('display', 'block');
        }
        
    });
    function searchItem(search_url){
        var time = $("#times").val();
        var branch_name = $("#brand_name").val();
        var currency = $("#currency").val();
        var client_name = $("#client_name").val();
        var sale_id = $("#sale_id").val();
        var staff_name = $("#staff_name").val();
        var from = $("#from_date").val();
        var to_date = $("#to_date").val();
        var all_search = time+branch_name+currency+client_name+sale_id+staff_name;
        var re_search = all_search.split('null').join('0');
        
        if(re_search == '000000'){alert("Null");return false;};
        var search_url = search_url + "&time="+time+"&from="+from+"&to_date="+to_date+"&branch_name="+branch_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name;
        // console.log(search_url);
        $.ajax({
            url: search_url,
            success: function(data){
                appendData(data);               
            },
            error: function(error){
                console.log(error.responseText);
            }
        });
    }
</script>