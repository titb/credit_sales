@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >
    
<!-- validation -->
<div class="row-fluid">
    
    <!-- block -->
    <div class="block">

        @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 3px;">
                <div class="muted pull-left" style="padding-top: 5px;">Report Sale By Cash</div>
                <a href="{{url('export_report')}}" class="btn btn-primary b_search pull-right"  value="b_search">Export to Report</a>
                <a href="{{url('export_report')}}" class="btn btn-danger pull-right"  value="b_search" style="margin-right: 4px;">Print</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = "{{url('report/report_sale')}}";
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12" style="margin-left: 0px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>ចំនួនទឹកប្រាក់</th>
                            <th>បញ្ចុះតម្លៃ</th>
                            <!-- <th>ប្រាក់ដែលនៅជំពាក់</th> -->
                            <th>ចំនួនផលិតផល</th>
                            <th>ប្រភេទបង់ប្រាក់</th>
                            <th>បុគ្គលិកប្រមូលប្រាក់</th>
                            <th>រូបិយប័ណ្ណ</th>
                            <th>ផ្ដល់មតិ</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                    <div class="pagination text-right"></div>
                </div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }
        var search_url = "{{url('report/search/sale')}}";
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
    });
    function appendData(result){
        // console.log(result);
        var text = "";
        $.each(result.data, function(k, sale){
            var cur = result.current_page * 10 - 10;
            var il = (1 + k) + cur;
            var url_show_in = "{{url('credit_sales')}}/"+sale.id+"/invoice";
            text +="<tr>";
                text +="<td>"+il+"</td>";
                text +="<td> <a href='#' class='print_sale_by_cash'>Print</a>  | <a href='"+url_show_in+"' class='show_sale_by_cash'>Show</a> |<a href='#' class='deleted_sale_by_cash'>Delete</a> | "+sale.num_invoice+"</td>";
                    var name_kh = sale.cs_client!==null?sale.cs_client.kh_username:'';
                text +="<td>"+name_kh+"</td>";
                text +="<td>"+accounting.formatMoney(sale.sale_payment.dute_payment_amount)+"</td>";
                text +="<td>"+sale.sale_payment.discount_payment+"</td>";
                text +="<td>"+sale.sale_payment.total_qty+"</td>";
                text +="<td>"+sale.method+"</td>";
                text +="<td>"+sale.user.name_kh+"</td>";
                text +="<td>"+sale.currency.value_option+"</td>";
                text +="<td>"+sale.sale_payment.comment+"</td>";
            text +="</tr>";
        });
        $('#list_item').html(text); 
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }
         
</script>
@endsection
