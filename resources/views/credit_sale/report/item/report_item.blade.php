@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
@include('credit_sale.report.get_session_search')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<?php  
    if(Session::has('from_date')){
        $from_date  = Session::get('from_date');
    }else{
        $from_date  = '';
    }

    if(Session::has('to_date')){
        $to_date  = Session::get('to_date');
    }else{
        $to_date  = '';
    }

    if(Session::has('brand_name')){
        $brand_name  = Session::get('brand_name');
    }else{
        $brand_name  = '';
    }

    if(Session::has('currency')){
        $currency  = Session::get('currency');
    }else{
        $currency  = '';
    }

    if(Session::has('client_name')){
        $client_name  = Session::get('client_name');
    }else{
        $client_name  = '';
    }

    if(Session::has('sale_id')){
        $sale_id  = Session::get('sale_id');
    }else{
        $sale_id  = '';
    }

    if(Session::has('staff_name')){
        $staff_name  = Session::get('staff_name');
    }else{
        $staff_name  = '';
    }

?>
										
                                       
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >
    
<!-- validation -->
<div class="row-fluid">
    
    <!-- block -->
    <div class="block">

        @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <!-- <a href="{{ route('report/clients/report_client_list',['submit_search'=>'b_search','from_date'=>$from_date,'to_date'=>$to_date,'brand_name'=>$brand_name ,'currency'=>$currency,'sale_id'=>$sale_id, 'staff_name'=>$staff_name,'export'=>'excel']) }}"  class="btn btn-info pull-right" style="margin-top:-5px;">Export to Excel</a> -->
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in" >
        <?php 
            $url = "{{url('report/clients/report_client_list_json')}}";
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12" style="margin-left: 0px;"  id="report-incom">
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            /*background-color:rgba(0, 136, 204, 0.55);*/
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        /*font-size: 11px;*/

                        }

                        table {

                        width: 100%;

                        /*border: 1px solid #000;*/

                        /*border-collapse: separate;*/

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        /*tr{

                            page-break-inside: avoid;

                        }*/

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            /*border-top: 1px solid #000;*/

                            font-size: 10px;

                        }



                        /*thead {

                            display: table-header-group;

                            vertical-align: middle;

                            border-color: inherit;

                        }*/
                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                        <table class="table table-bordered table-print" cellpadding="0" cellspacing="0" >
                            <thead style="background: rgb(251, 205, 205);">
                            <tr class="header">
                                <th>ល.រ</th>
                                <th>លេខកូដទំនិញ</th>
                                <th>ឈ្មោះ</th>
                                <th>ម៉ាក</th>
                                <th>ប្រភេទ</th>
                                <th>ចំនួនក្នុងស្តុក</th>
                                <!-- <th>ចំនួន</th> -->
                                <th>ចំនួនលក់</th>
                                <th>ចំនួនទំនិញត្រឡប់</th>
                                <th>តម្លៃដើម</th>
                                <th>តម្លៃលក់</th>
                            </tr>
                            </thead>
                            <tbody id="list_item" class="list_item">

                                
                                    
                            </tbody>
                                <tr>
                                    <td colspan="9">
                                        <b class="pull-right">ទំនិញសរុប:</b>
                                    </td>
                                    <td>
                                        <b id="total_all"></b>
                                    </td>
                                </tr>
                        </table>
                
									       
                </div>
                <!-- Pagination -->
                <div class="pagination text-right"></div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript">
	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }

        // var numpage = 1;
        // var url_edit = "{{route('report/item/report_item_list_json')}}";
        // get_page(url_edit,numpage);
        // $(document).ajaxComplete(function(){
            // $(".b_search").click(function(){
            //     var submit_search = $(this).val();
            //     var n = 1;
                
            //     var url_index2 = submit_search; 		
            //     get_page(url_index2,numpage = n);
            // });
            // $(document).on('click','.pag',function(){
            //     var numpage = $(this).text();   
            //     get_page(url_edit,numpage);
            // });

            // $(".pre").click(function(){
            //     var numpage = $(this).find(".pre_in").val();
            //     get_page(url_edit,numpage);
            // });
        // });
        
        function get_page(url,n){
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            
            var from_date = $(".from_date").val();
            var to_date = $(".to_date").val(); 
            var brand_name = $(".brand_name").val(); 
            var currency = $(".currency").val(); 
            var client_name = $(".client_name").val(); 
            var sale_id = $(".sale_id").val(); 
            var staff_name = $(".staff_name").val();  

            //alert(item_search);
            if((url === "b_search"|| url === "export_to_excel") || (from_date !== "" && to_date !== "") || brand_name !== "" || currency !== "" || client_name !== "" || sale_id !== "" || staff_name !== ""){
                var url_i = "{{route('report/item/report_item_list_json')}}";
                
                var forData = {
                    from_date: from_date,
                    to_date: to_date,
                    brand_name: brand_name,
                    currency: currency,
                    client_name: client_name,
                    staff_name: staff_name,
                    submit_search: url
                }                
                var url_index = url_i+"?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search="+url+"&page="+n;     
                var url_excel = "{{route('report/item/report_item_list')}}?from_date="+from_date+"&to_date="+to_date+"&brand_name="+brand_name+"&currency="+currency+"&client_name="+client_name+"&sale_id="+sale_id+"&staff_name="+staff_name+"&submit_search="+url+"&export=excel";  
            }else{
                var forData = {};
                var url_index = url_edit+"?page="+n;
                var url_excel = "{{route('report/item/report_item_list')}}?export=excel";  

            }
        
            var client;
            // var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_index, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        if(url != "export_to_excel"){
                                console.log(result);
                                $("#total_all").text(result.data.total);
                                $(".export_excel").attr('href',url_excel);
                                if(result.data.data){
                                $.each(result.data.data, function(i, field){ 
                                    var qty_of_buy = 0;   
                                    var total_price = 0; 
                                    // $.each(field.cs_sale, function(f, x){
                                    //         $.each(x.sale_item, function(b, n){ 
                                    //             qty_of_buy += n.qty;
                                    //             total_price += n.total_price;
                                    //         });  
                                        
                                    // });

                                        var il = i + 1;
                                        if(field.brand_id !== null){
                                            var brand = field.cs_brand.name;
                                        }else{
                                            var brand = "";
                                        }
                                        var qty = 0;
                                        var qty_sell = 0;
                                        var return_qty = 0;
                                        var net_qty = 0;
                                        $.each(field.inventorys, function(k, val){
                                            qty += val.qty;
                                            if(val.status_transation == 'add new'){
                                                net_qty += val.qty;
                                            }
                                            
                                            if(val.status_transation == 'Sale'){
                                                qty_sell += (0 - val.qty);
                                                // var qty_sells = qty_sell;
                                            }
                                            if(val.status_transation == 'receiving'){
                                                return_qty += val.qty;
                                            }
                                        });
                                        if(qty < net_qty){
                                            var all_qty = "out";
                                        }else{
                                            var all_qty = "In";
                                        }
                                        // var qtys = Number(qty);
                                            // if(sts !== 1 ){
                                            //     sts = "<span class='label label-success'>Active</span>";
                                            // }else{
                                            //     sts = "<span class='label label-important'>Inactive</span>";
                                            // }	
                                        var url = "{{ url('products/products') }}";
                                        var show_url = url + "/"+field.id+"/show?form=report";
                                        // var edit_url = url + "/"+field.id+"/edit";
                                        // var qty_sells = qty_sell.replace("-","");
                                            client += "<tr>";
                                                client += "<td>"+ il +"</td>";
                                                client += "<td><a href='"+show_url+"'>"+field.item_bacode+"</a></td>";
                                                client += "<td>"+ field.name +"</td>";
                                                client += "<td>"+ brand +"</td>";
                                                client += "<td>"+ field.categorys.name +"</td>";
                                                // client += "<td>"+field. +"</td>";
                                                client += "<td>"+ qty +"</td>";
                                                client += "<td>"+ qty_sell +"</td>";
                                                client += "<td>"+ return_qty +"</td>";
                                                client += "<td>"+ field.cost_price +"</td>";
                                                client += "<td>"+ field.sell_price +"</td>";
                                            client += "</tr>";
                                    });
                                }
                                $('#list_item').html(client);
                                var page = "";
                                if(result.data.prev_page_url === null){
                                    var pr_url = result.data.current_page;
                                }else{
                                    var pr_url = result.data.current_page -1;
                                }
                                page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                for(var x = 1; x <= result.data.last_page; x ++  ) {
                                    if(result.data.current_page === x){
                                        page += "<a class='pag active' >"+x+"</a>";
                                    }else{
                                        page += "<a class='pag' >"+x+"</a>";
                                    }
                                }
                                if(result.data.next_page_url === null){
                                    var ne_url = result.data.current_page; 
                                }else{
                                    var ne_url = result.data.current_page +1;
                                }
                                page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                $(".pagination").html(page );
                        }else{
                            window.location = "{{url('report/item/report_item_list')}}";
                        }            
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
        }
        var search_url = "{{url('report/search/item')}}"
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
    });
    
    function appendData(result){
        console.log(result);
        var client="";
        $.each(result.data, function(i, field){ 
            var cur = result.current_page * 10 - 10;
            var il = (1 + i) + cur;
            var qty = 0;
            var qty_sell = 0;
            var return_qty = 0;
            var net_qty = 0;
            $.each(field.inventorys, function(k, val){
                qty += val.qty;
                if(val.status_transation == 'add new'){
                    net_qty += val.qty;
                }
                
                if(val.status_transation == 'Sale'){
                    qty_sell += (0 - val.qty);
                    // var qty_sells = qty_sell;
                }
                if(val.status_transation == 'receiving'){
                    return_qty += val.qty;
                }
            });
            if(qty < net_qty){
                var all_qty = "out";
            }else{
                var all_qty = "In";
            }
            var url = "{{ url('products/products') }}";
            var show_url = url + "/"+field.id+"/show?form=report";
            client += "<tr>";
                client += "<td>"+ il +"</td>";
                client += "<td><a href='"+show_url+"'>"+field.item_bacode+"</a></td>";
                client += "<td>"+ field.name +"</td>";
                    var brand = field.cs_brand?field.cs_brand.name:''
                client += "<td>"+ brand +"</td>";
                    var category = field.categorys?field.categorys.name:'';
                client += "<td>"+ category +"</td>";
                client += "<td>"+ qty +"</td>";
                client += "<td>"+ qty_sell +"</td>";
                client += "<td>"+ return_qty +"</td>";
                client += "<td>"+ field.cost_price +"</td>";
                client += "<td>"+ field.sell_price +"</td>";
            client += "</tr>";
        });
        
        $('#list_item').html(client);
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }
function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }

</script>
    
@endsection
