@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')

<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
                                
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >
    
<!-- validation -->
<div class="row-fluid">
    
    <!-- block -->
    <div class="block">

        @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in" >
        <?php 
            $url = route('report/clients/report_client_list',['reset'=>'reset']);
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12" style="margin-left: 0px;"  id="report-incom">
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        }

                        table {

                        width: 100%;

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            font-size: 10px;

                        }

                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                        <table class="table table-bordered table-print" cellpadding="0" cellspacing="0" >
                            <thead style="background: rgb(251, 205, 205);">
                            <tr class="header">
                                <th>ល.រ</th>
                                <th>លេខកូដអតិថិជន</th>
                                <th>គោត្តនាម និង នាម</th>
                                <th>បរិមាណនៃការទិញដោយជំពាក់</th>
                                <th>ចំនួនទឹកប្រាក់នៃការទិញដោយជំពាក់</th>
                                <th>បរិមាណនៃការទិញដោយផ្ទាល</th>
                                <th>ចំនួនទឹកប្រាក់នៃការទិញដោយផ្ទាល</th>
                                <th>បរិមាណនៃការសង់ត្រលប់</th>
                                <th>ចំនួនទឹកប្រាក់នៃការសង់ត្រលប់</th>
                                <th>សរុបបរិមាណនៃការទិញ</th>
                                <th>សរុបចំនួនទឹកប្រាក់នៃការទិញ</th>
                            </tr>
                            </thead>
                            <tbody id="list_item" class="list_item">

                                
                                    
                            </tbody>
                                <tr>
                                    <td colspan="10">
                                        <b class="pull-right">អតិថិជនសរុប:</b>
                                    </td>
                                    <td>
                                        <b id="total_all"></b>
                                    </td>
                                </tr>
                        </table>
                
									       
                </div>
                <!-- Pagination -->
                <div class="pagination text-right"></div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }

        // var numpage = 1;
        // var url_edit = "{{route('report/clients/report_client_list_json')}}";
        // get_page(url_edit,numpage);
        // // $(document).ajaxComplete(function(){
        //     $(".b_search").click(function(){
        //         var submit_search = $(this).val();
        //         var n = 1;
                
        //         var url_index2 = submit_search; 		
        //         get_page(url_index2,numpage = n);
        //     });
        //     $(document).on('click','.pag',function(){
        //         var numpage = $(this).text();   
        //         get_page(url_edit,numpage);
        //     });

        //     $(".pre").click(function(){
        //         var numpage = $(this).find(".pre_in").val();
        //         get_page(url_edit,numpage);
        //     });
        // // });
        var search_url = "{{url('report/search/client')}}";
        $("#b_search").click(function(){
            
            searchItem(search_url+"?page=1");
        });
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
    });
        
    function appendData(result){
        console.log(result);
        // $("#total_all").text(result.data.total);
        // $(".export_excel").attr('href',url_excel);
        var client='';
        $.each(result.data, function(i, field){ 
            var qty_of_buy = 0;   
            var total_price = 0; 
            var qty_of_buy_cash = 0;
            var total_price_cash = 0;
            var qty_of_receiving = 0;
            var total_price_of_receving = 0 ;
            var qty_of_buy_credit  = 0;
            var total_price_credit = 0;
            $.each(field.cs_sale, function(f, x){
                $.each(x.sale_item, function(b, n){ 
                        qty_of_buy += n.qty;
                        total_price += n.total_price;
                });  
                if(x.method == 'sale_by_cash'){
                    $.each(x.sale_item, function(b, n){ 
                        qty_of_buy_cash += n.qty;
                        total_price_cash += n.total_price;
                    });   
                }else if(x.method == 'sale_by_credit'){
                    $.each(x.sale_item, function(b, n){ 
                        qty_of_buy_credit += n.qty;
                        total_price_credit += n.total_price;
                    });  
                }else{
                    $.each(x.sale_item, function(b, n){ 
                        qty_of_receiving += n.qty;
                        total_price_of_receving += n.total_price;
                    });  
                }
            });

            var cur = result.current_page * 10 - 10;
            var il = (1 + i) + cur;
            var url = "{{ url('accounts') }}";
            var show_url = url + "/"+field.id+"/show";
                client += "<tr>";
                    client += "<td>"+ il +"</td>";
                    client += "<td><a href='"+show_url+"'>"+field.client_code+"</a></td>";
                    client += "<td>"+ field.kh_name_first +" &nbsp; "+ field.kh_name_last +"</td>";
                    client += "<td>"+ qty_of_buy_credit + "</td>";
                    client += "<td>"+ accounting.formatMoney(total_price_credit) + "</td>";
                    client += "<td>"+ qty_of_buy_cash + "</td>";
                    client += "<td>"+ accounting.formatMoney(total_price_cash) + "</td>";
                    client += "<td>"+ qty_of_receiving + "</td>";
                    client += "<td>"+ accounting.formatMoney(total_price_of_receving) + "</td>";
                    client += "<td>"+ qty_of_buy + "</td>";
                    client += "<td>"+ accounting.formatMoney(total_price) + "</td>";
                client += "</tr>";
        });
        $('#list_item').html(client);     
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }    

function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }



</script>
    
@endsection
