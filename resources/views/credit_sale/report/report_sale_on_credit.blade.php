@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = "{{url('report/sales/report_sale_by_credit?reset=reset')}}";
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12"  style="margin-left: 0px;">

                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>ចំនួនទឹកប្រាក់</th>
                            <th>ប្រាក់បង់តំបូង៣០%</th>
                            <th>ប្រាក់ដែលនៅជំពាក់</th>
                            <th>រយៈពេលសង</th>
                            <th>ថ្ងៃបញ្ចេញទំនិញ</th>
                            <th>ប្រភេទបង់ប្រាក់</th>
                            <th>បុគ្គលិកប្រមូលប្រាក់</th>
                            <th>រូបិយប័ណ្ណ</th>
                            <th>ស្ថានភាព</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                </div>
                <div class="pagination text-right"></div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }
        var search_url = "{{url('report/search/sale_on_credit')}}";
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
	});
    function appendData(result){   
        var text = "";
        $.each(result.data, function(k,sale){
            var cur = result.current_page * 10 - 10;
            var il = (1 + k) + cur;
            text +="<tr>";
                text +="<td>"+il+"</td>";
                text +="<td>"+sale.num_invoice+"</td>";
                text +="<td>"+sale.cs_client.kh_username+"</td>";
                text +="<td>"+accounting.formatMoney(sale.cs_request_form.prices_total_num)+"</td>";
                text +="<td>"+accounting.formatMoney(sale.cs_request_form.money_owne)+"</td>";
                text +="<td>"+accounting.formatMoney(sale.cs_request_form.deposit_fixed)+"</td>";
                text +="<td>"+day_format_show(sale.cs_request_form.date_for_payments)+"</td>";
                text +="<td>"+day_format_show(sale.cs_request_form.date_create_request)+"</td>";
                text +="<td>"+sale.method+"</td>";
                text +="<td>"+sale.user.name_kh+"</td>";
                text +="<td>"+sale.currency.name+"</td>";
                if(sale.is_approve == 0){
                    text +="<td>"+"មិនយល់ព្រម"+"</td>";
                }else if(sale.is_oncredit == 1){
                    text +="<td>"+"យល់ព្រម"+"</td>";
                }
            text +="</td>";
        });
        $('#list_item').html(text);
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }
</script>
</script>
@endsection
