@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 10px;">
                <div class="muted pull-left" style="padding-top: 5px;"><span> {{$title}} </span></div>
                <a  class="btn btn-info export_excel pull-right" style="margin-top:-5px;">បញ្ចូលទៅកាន់ Excel</a>
                <a href="#" class="btn btn-danger print_excel pull-right" onclick="myFunction('report-incom')" style="margin-right: 4px; margin-top:-5px;" >ព្រីន</a>
            </div>    
        </div>
        <div class="block-content collapse in">
        <?php 
            $url = url('report/sales/report_sale_by_credit?reset=reset');
        ?>
            @include('credit_sale.report.search.search')
                <div class="span12"  style="margin-left: 0px;" id="report-incom"> 
                <style type="text/css">
                            body {
                                -webkit-print-color-adjust: exact;
                            }
                        @media print
                        {    
                            .no-print, .no-print *{display: none !important;}
                            .color{color:red !important;}
                            tr th.row-background{background:#438eb9 !important;}
                        }
                        #co{
                            text-align: center;
                        }
                        .no_show{
                            display: none;
                        }
                    </style>
                    <style type="text/css" media="print">

                        @page { size: landscape; margin: 10px; margin-top: 10px;margin-bottom: 20px;}

                        .table-print{

                        width: 100%;

                        font-family: 'Battambang';

                        }

                        table {

                        width: 100%;

                        border-left: 0;

                        -webkit-border-radius: 4px;

                        -moz-border-radius: 4px;

                        border-radius: 4px;

                        }

                        table tr th, table tr td {

                            border: 1px solid #000;

                            padding: 5px;

                            line-height: 20px;

                            vertical-align: center;

                            font-size: 10px;

                        }

                        .no_print{
                                display: none;
                        }

                        .title_print{
                            font-family: 'Moul';
                        }

                        a{
                            text-decoration: none;
                            color: #000;
                        }
                        .font-weight{
                            font-weight: bold;
                        }
                        .print_show{
                            display: block;
                        }

                    </style>
                    <table class="table table-bordered table-print"  cellpadding="0" cellspacing="0" >
                        <thead  style="background: rgb(251, 205, 205);">
                        <tr class="header"> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអតិថិជន</th>
                            <th>ចំនួនទឹកប្រាក់</th>
                            <th>ប្រាក់បង់តំបូង៣០%</th>
                            <th>ប្រាក់ដែលនៅជំពាក់</th>
                            <th>បរិមាណទំនិញ</th>
                            <th>រយៈពេលសង</th>
                            <th>កាលបរិច្ឆេទបង្កើត</th>
                            <th>បុគ្គលិកប្រមូលប្រាក់</th>
                            <th>រូបិយប័ណ្ណ</th>
                            <th>ស្ថានភាព</th>
                        </tr>
                        </thead>
                        <tbody id="list_item" class="list_item">
                            
                        </tbody>
                    </table>
                </div>
                <div class="pagination text-right"></div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>

<script type="text/javascript">

	$(document).ready(function(){

        var search_url = "{{url('report/search/sale_by_credit')}}";
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });

	});
    function appendData(result){
        console.log(result);
        var text = '';
        $.each(result.data, function(k,sale){
            var cur = result.current_page * 10 - 10;
            var il = (1 + k) + cur;
            var status;
            if(sale.cs_approval_credit_sale){
                if(sale.is_approve == 1){
                    status = 'អនុម័ត';     
                }else if(sale.is_approve == 2) {
                    status = 'មិនទាន់អនុម័ត';   
                }else if(sale.is_approve == 3) {
                    status = 'មិនអនុម័ត';   
                }
            }else {
                status = 'រង់ចាំការអនុម័ត';   
            }
            var dpm = sale.cs_request_form!==null?sale.cs_request_form.duration_pay_money:'';
            var dpmt = sale.cs_request_form!==null?sale.cs_request_form.duration_pay_money_type:'';
            var ptn = sale.cs_request_form!==null?sale.cs_request_form.prices_total_num:'';
            var mo = sale.cs_request_form!==null?sale.cs_request_form.money_owne:'';
            var df = sale.cs_request_form!==null?sale.cs_request_form.deposit_fixed:'';
            var total_q = sale.cs_request_form!==null?sale.cs_request_form.total_qty:'';
            var dcr = sale.cs_request_form!==null?sale.cs_request_form.date_create_request:'';

            if(dpmt == "month"){
                dpmt = "ខែ";
            }else if(dpmt == "2week"){
                dpmt = "២ សប្តាហ៍";
            }else if(dpmt == "week"){
                dpmt = "សប្តាហ៍";
            }else if(dpmt == "day"){
                dpmt = "ថ្ងៃ";
            }  
            var dpm_d = dpm+"  "+dpmt;

            text +="<tr>";
                text +="<td>"+il+"</td>";
                text +="<td>"+sale.num_invoice+"</td>";
                text +="<td>"+sale.cs_client.kh_username+"</td>";
                text +="<td>"+accounting.formatMoney(ptn)+"</td>";
                text +="<td>"+accounting.formatMoney(mo)+"</td>";
                text +="<td>"+accounting.formatMoney(df)+"</td>";
                text +="<td>"+total_q+"</td>";
                text +="<td>"+dpm_d+"</td>";
                text +="<td>"+day_format_show(dcr)+"</td>";
                text +="<td>"+sale.user.name_kh+"</td>";
                text +="<td>"+sale.currency.name_kh+"</td>";
                text +="<td>"+status+"</td>";
            text +="</td>";
        });
        $('#list_item').html(text);
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }
function myFunction(printpage)

    {

    var headstr = "<html><head><title></title></head><body>";

    var footstr = "</body>";

    var newstr = document.all.item(printpage).innerHTML;

    var oldstr = document.body.innerHTML;

    document.body.innerHTML = headstr+newstr+footstr;

    window.print();

    document.body.innerHTML = oldstr;

    return false;

    }


</script>

@endsection
