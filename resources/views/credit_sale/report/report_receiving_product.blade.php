@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
    p#payment_back {
        color: #08c;
        margin-top: 10px;
    }

</style>
<meta name="csrf-token" content="{{ csrf_token() }}">
<script src="{{ URL::to('assets/accounting.min.js')}}"></script>
<!-- container-fluid  --> 	
<div class="container-fluid content_top_mar" >

<!-- validation -->
<div class="row-fluid">
    <!-- block -->
    <div class="block">
    @include('errors.error')
        <div class="navbar navbar-inner block-header">
            <div class="span12" style="margin-bottom: 3px;">
                <div class="muted pull-left" style="padding-top: 5px;">Report Receiving Product</div>
                <a href="{{url('export_report_receive')}}" class="btn btn-primary b_search pull-right"  value="b_search">បញ្ចូលទៅកាន់ Excel</a>
                <a href="{{url('export_report')}}" class="btn btn-danger pull-right"  value="b_search" style="margin-right: 4px;">ព្រីន</a>
            </div>
        </div>
        <div class="block-content collapse in">
            <?php 
                $url = "{{url('report/report_receiving_product')}}";
            
            ?>
            @include('credit_sale.report.search.search')
                <div class="span12"  style="margin-left: 0px;">
                    <table class="table table-bordered">
                        <thead>
                        <tr> 
                            <th>#</th>
                            <th>លេខកូដ</th>
                            <th>ឈ្មោះអ្នកផ្គត់ផ្គង់</th>
                            <th>ចំនួនផលិតផលPO</th>
                            <th>ចំនួនផលិតផលRE</th>
                            <th>ភាគរយបញ្ចុះតម្លៃ</th>
                            <th>ប្រាក់កក់</th>
                            <th>ប្រាក់ដែលនៅជំពាក់</th>
                            <th>ចំនួនប្រាក់សរុប</th>
                            <th>ថ្ងៃទទួលផលិតផល</th>
                            <th>បុគ្គលិកទទួល</th>  
                        </tr>
                        </thead>
                        <tbody id="list_item">
                            
                        </tbody>
                    </table>
                    <div class="pagination text-right"></div>
                </div>
            </div>

        </div>
    <!-- /block -->
</div>
<!-- /validation -->

</div>
<script type="text/javascript">

	$(document).ready(function(){
        function day_format_show(date_format){
            var d = new Date(date_format);

            var year_n = d.getFullYear();
            var month_n = d.getMonth() + 1;
            var day_n = d.getDate();
            if(month_n > 10){
                month_n = month_n;
            }else{
                month_n = "0"+month_n; 
            }
            
            if(day_n > 10){
                day_n = day_n;
            }else{
                day_n = "0"+day_n; 
            }

            return  day_n +"-"+month_n+"-"+year_n;
        }
        var search_url = "{{url('report/search/receiving_product')}}"
        $("#b_search").click(function(){
            searchItem(search_url+"?page=1");
        });
        
        $(document).on('click','.pag',function(){
            var numpage = $(this).text();
            searchItem(search_url+"?page="+numpage);
        });
        $(document).on('click','.pre',function(){
            var numpage = $(this).find(".pre_in").val();
            searchItem(search_url+"?page="+numpage);
        });
    });
            
    function appendData(result){
        // console.log(result);
        var text ="";
        $.each(result.data , function(index, field){
            var cur = result.current_page * 10 - 10;
            var il = (1 + index) + cur;
            var url_show_re = "{{url('credit_sales_purchas_order')}}/"+field.id+"/receive_invoice";
            text +=  "<tr>";
            text += " <td>"+ il +"</td>";
            text += " <td> <a href='"+url_show_re+"'>"+ field.invoice_num +"</a></td>";
            var supplier_name = field.suppliers!==null?field.suppliers.name:'';
            text += " <td>"+  +"</td>";
            text += " <td>"+ field.qty_order +"</td>";
            text += " <td>"+ field.qty_receiving +"</td>"; 
            text += " <td>"+ field.discount_percent +" ​(%) </td>";
            text += " <td>"+ accounting.formatMoney(field.deposit_price) +"</td>";
            text += " <td>"+ accounting.formatMoney(field.remain_price) +"</td>";
            text += " <td>"+ accounting.formatMoney(field.dute_total_price) +"</td>";
            text += " <td>"+ day_format_show(field.receiving_date) +"</td>";
            text += " <td>"+ field.staff_receive.name_kh +"</td>";
            text += "</tr>";
        });
        $("#list_item").html(text);
        var page = "";
        if(result.prev_page_url === null){
            var pr_url = result.current_page;
        }else{
            var pr_url = result.current_page -1;
        }
        page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
        for(var x = 1; x <= result.last_page; x ++  ) {
            if(result.current_page === x){
                page += "<a class='pag active' >"+x+"</a>";
            }else{
                page += "<a class='pag' >"+x+"</a>";
            }
        }
        if(result.next_page_url === null){
            var ne_url = result.current_page; 
        }else{
            var ne_url = result.current_page +1;
        }
        page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
        $(".pagination").html(page );
    }	

</script>
@endsection
