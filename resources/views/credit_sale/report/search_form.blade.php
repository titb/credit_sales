<?php 
    if(Session::has('from_date')){
        $from_date  = Session::get('from_date');
    }else{
        $from_date  = '';
    }

    if(Session::has('to_date')){
        $to_date  = Session::get('to_date');
    }else{
        $to_date  = '';
    }

    if(Session::has('brand_name')){
        $brand_name  = Session::get('brand_name');
    }else{
        $brand_name  = '';
    }

    if(Session::has('currency')){
        $currency  = Session::get('currency');
    }else{
        $currency  = '';
    }

    if(Session::has('client_name')){
        $client_name  = Session::get('client_name');
    }else{
        $client_name  = '';
    }

    if(Session::has('sale_id')){
        $sale_id  = Session::get('sale_id');
    }else{
        $sale_id  = '';
    }

    if(Session::has('staff_name')){
        $staff_name  = Session::get('staff_name');
    }else{
        $staff_name  = '';
    }


?>
<div class="span12">
        
    <div class="span2">
        <lable>&nbsp;</lable>
        <input type="text" name="from_date" placeholder="ពីថ្ងៃទី" data-required="1" class="input-xlarge datepicker span12 m-wrap from_date" autocomplete="off">
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <input type="text" name="to_date" placeholder="ដល់ថ្ងៃទី" data-required="1" class="input-xlarge datepicker span12 m-wrap to_date" autocomplete="off">
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $brand_id = App\Branch::where('status','=',1)->where('deleted','=',0)->get(); ?>
            <select id="select01" class="span12 m-wrap brand_name" name="brand_name" >
            <option value="">ជ្រើសរើសសាខា</option>
            @foreach($brand_id as $br)
            <option value="{{$br->id}}"  <?php if($br->id == $brand_name){echo "selected";}  ?> >{{$br->brand_name}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $currency1 = App\Currency::where('deleted','=',1)->get(); ?>
        <select id="select01" class="span12 m-wrap currency" name="currency" >
            <option value="">ជ្រើសរើសរូបិយប័ណ្ណ</option>
            @foreach($currency1 as $br)
            <option value="{{$br->id}}" <?php if($br->id == $currency){echo "selected";}?> >{{$br->name_kh}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $client_name1 = App\Cs_Client::where('deleted','=',0)->get(); ?>
        <select id="select01" class="span12 m-wrap client_name" name="client_name" >
            <option value="">ជ្រើសរើសអតិថិជន</option>
            @foreach($client_name1 as $br)
            <option value="{{$br->id}}" <?php if($br->id == $client_name){echo "selected";}?> >{{$br->kh_username}}</option>
            @endforeach
        </select>
    </div>
    <div class="span2">
        <lable>&nbsp;</lable>
        <?php $sale_id1 = App\Cs_Schedule::where('deleted','=',1)->get(); ?>
        <select id="select01" class="span12 m-wrap sale_id" name="sale_id" >
            <option value="">ជ្រើសរើសលេខកូដ</option>
            @foreach($sale_id1 as $br)
            <option value="{{$br->id}}"  <?php if($br->id == $sale_id){echo "selected";}?>  >{{$br->id}}</option>
            @endforeach
        </select>
    </div>
        <div class="span10"  style="margin-left: 0px;">
            <div class="span2"  style="margin-left: 0px; width: 200px;">
                <lable>&nbsp;</lable>
                <?php $staff_name1 = App\User::where('deteted','=',1)->get(); ?>
                <select id="select01" class="span12 m-wrap staff_name" name="staff_name" >
                    <option value="">ជ្រើសរើសបុគ្គលិក</option>
                    @foreach($staff_name1 as $br)
                    <option value="{{$br->id}}" <?php if($br->id == $staff_name){echo "selected";}?> >{{$br->name_kh}}</option>
                    @endforeach
                </select>
            </div>
        </div>
        <div class="span2 pull-right">
            <lable>&nbsp;</lable><br/>
            <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search">ស្វែងរក</button>
            <a href="{{$url}}" class="btn btn-danger" >Reset</a>
        </div>
    <!-- </div> -->
</div>
    