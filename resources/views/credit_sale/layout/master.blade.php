<!DOCTYPE html>

<html class="no-js">

    <head>

        <title>MANAGEMANT
</title>

        <!-- Bootstrap -->
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta name="csrf-token" content="{{ csrf_token() }}">
        <!-- <link href="https://fonts.googleapis.com/css?family=Battambang|Bokor|Moul|Siemreap" rel="stylesheet"> -->
       <link href="{{ URL::to('bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" media="screen">
        <link href=" {{ URL::to('bootstrap/css/bootstrap-responsive.min.css') }}" rel="stylesheet" media="screen">

        
        <link rel="stylesheet" href="{{ URL::to('assets/colorpicker/bootstrap-colorpicker.min.css') }}" rel="stylesheet" media="screen">
        <link href="{{ URL::to('vendors/easypiechart/jquery.easy-pie-chart.css') }}" rel="stylesheet" media="screen">

        <link href="{{ URL::to('assets/styles.css') }}" rel="stylesheet" media="screen">
        <link href="{{ url('css/inv_style.css')}}" rel="stylesheet" media="screen">
        <!-- <link href="{{ url('vendors/datepicker.css')}}" rel="stylesheet" media="screen"> -->
        <!-- <link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-datepicker/1.8.0/css/bootstrap-datepicker.css" rel="stylesheet" media="screen"> -->
         <link href="{{ url('css/bootstrap-datepicker.css')}}" rel="stylesheet" media="screen">
        <link href="{{ url('vendors/uniform.default.css') }}" rel="stylesheet" media="screen">

        <link href="{{ url('vendors/chosen.min.css') }}" rel="stylesheet" media="screen">

        <link href="{{ url('assets/DT_bootstrap.css') }}" rel="stylesheet" media="screen">
        <link href="{{ url('css/dashboard-respon.css') }}" rel="stylesheet" media="screen">
        <link href="{{ url('css/dasboard.css') }}" rel="stylesheet" media="screen">
        
        <script src="{{ URL::to('vendors/modernizr-2.6.2-respond-1.1.0.min.js') }}"></script>

            <script src="{{ url('assets/jquery.min.js')}}"></script>
            <link href="https://fonts.googleapis.com/css?family=Moul" rel="stylesheet">
            <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Hanuman">
<!-- Model -->
     <!--  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css"> -->
      <!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script> -->
 <!--  Model -->
 <script src="{{ URL::to('assets/accounting.min.js')}}"></script>
 <script src="{{ URL::to('assets/credit_sale_script.js')}}"></script>
<style type="text/css">
    .khmer_Moul
    { 
        font-family: 'Moul' !important, 'Times New Roman'; 
        font-size: 20px !important;
    }
    body, input, button, select, textarea, label, span, .chzn-container
    { 
        font-family:  'Hanuman'  !important; 
        font-size: 13px !important;
    }
    body{
        font-family: 'Hanuman' !important; 
    }
    .btn{ font-size: 14px !important; }
    a{
        text-decoration: none !important;  
    }
.pagination a {
  color: black;
  cursor: pointer;
  padding: 8px 16px;
  text-decoration: none;
  transition: background-color .3s;
}

.pagination a.active {
  background-color: dodgerblue;
  color: white;
}

.pagination a:hover:not(.active) {background-color: #ddd;}
.modal-body{
  max-height: -webkit-fill-available;
  max-height: fit-content;
  overflow: unset;
}
.modal {
	position: absolute;
  top: 2%;
  left: 7%;
  width: 85%;
  margin-left: auto;	
}
.chzn-container{
	width: 100% !important;
}
.content_top_mar {
    margin-top: 10px;
}
.span_upercase {
    text-transform: capitalize;
    padding-left: 10px;
}
.loading_file {
    position: fixed;
    top: 0;
    right: 0;
    bottom: 0;
    left: 0;
    z-index: 1050;
    text-align: center;
    background-color: rgba(0, 0, 0, 0.6);
}

.lds-spinner {
 vertical-align: middle;
  color: official;
  display: inline-block;
  position: relative;
  width: 64px;
  height: 64px;
  margin: 15% 0 0;
}

.lds-spinner div {
  transform-origin: 32px 32px;
  animation: lds-spinner 1.2s linear infinite;
}
.lds-spinner div:after {
  content: " ";
  display: block;
  position: absolute;
  top: 3px;
  left: 29px;
  width: 5px;
  height: 14px;
  border-radius: 20%;
  background: #fff;
}
.loading_file.active {
    display:none;
}
.lds-spinner div:nth-child(1) {
  transform: rotate(0deg);
  animation-delay: -1.1s;
}
.lds-spinner div:nth-child(2) {
  transform: rotate(30deg);
  animation-delay: -1s;
}
.lds-spinner div:nth-child(3) {
  transform: rotate(60deg);
  animation-delay: -0.9s;
}
.lds-spinner div:nth-child(4) {
  transform: rotate(90deg);
  animation-delay: -0.8s;
}
.lds-spinner div:nth-child(5) {
  transform: rotate(120deg);
  animation-delay: -0.7s;
}
.lds-spinner div:nth-child(6) {
  transform: rotate(150deg);
  animation-delay: -0.6s;
}
.lds-spinner div:nth-child(7) {
  transform: rotate(180deg);
  animation-delay: -0.5s;
}
.lds-spinner div:nth-child(8) {
  transform: rotate(210deg);
  animation-delay: -0.4s;
}
.lds-spinner div:nth-child(9) {
  transform: rotate(240deg);
  animation-delay: -0.3s;
}
.lds-spinner div:nth-child(10) {
  transform: rotate(270deg);
  animation-delay: -0.2s;
}
.lds-spinner div:nth-child(11) {
  transform: rotate(300deg);
  animation-delay: -0.1s;
}
.lds-spinner div:nth-child(12) {
  transform: rotate(330deg);
  animation-delay: 0s;
}
@keyframes lds-spinner {
  0% {
    opacity: 1;
  }
  100% {
    opacity: 0;
  }
}

.no_left{
    margin-left: 0px !important
}
a.btn i {
    margin-top: 3px !important;
    padding-right: 0px;
}

@media screen and (max-width: 1366px) {
  .container-fluid .row-fluid .block {
    margin-top: 60px !important;
  }
}
</style>
    </head>

    <script type="text/javascript">

            // (function() {

            //     var link_element = document.createElement("link"),

            //         s = document.getElementsByTagName("script")[0];

                // if (window.location.protocol !== "http:" && window.location.protocol !== "https:") {

                //     link_element.href = "http:";

                // }

            //     link_element.href += "";

            //     link_element.rel = "stylesheet";

            //     link_element.type = "text/css";

            //     s.parentNode.insertBefore(link_element, s);

            // })();
            $.ajaxSetup ({
                cache: false,
                headers: { "cache-control": "no-cache" }
            });   
        </script>

    <body>

        @include('credit_sale.layout.header')

       

        <div class="container-fluid top_n"  style="margin-top:10px;">

            <div class="row-fluid">

                 

                <section class="span12" id="content">     

                     @yield('contend') 

                </section>

            </div>

        </div>

        @include('credit_sale.layout.footer')

        <!--/.fluid-container-->
     <div class="loading_file active">   
        <div class="lds-spinner"><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div><div></div></div>     
    </div>


        <link rel="stylesheet" href="{{ url('vendors/morris/morris.css')}}">
        <script src="{{ URL::to('vendors/morris/morris.min.js') }}"></script>

        <script src="{{ URL::to('vendors/jquery-1.9.1.min.js') }}"></script>

        <script src="{{ URL::to('bootstrap/js/bootstrap.min.js') }}"></script>

        <script src="{{ URL::to('vendors/easypiechart/jquery.easy-pie-chart.js')}}"></script>

        <script src="{{ url('vendors/ckeditor/ckeditor.js') }}"></script>

        <script src=" {{ url('vendors/ckeditor/adapters/jquery.js') }}"></script>

        <script src="{{ url('vendors/jquery.uniform.min.js') }}"></script>

        <script src="{{ url('vendors/chosen.jquery.min.js') }}"></script>

        <script src="{{ url('vendors/bootstrap-datepicker.js') }}"></script>

        <script src="{{ url('vendors/wizard/jquery.bootstrap.wizard.min.js') }}"></script>

        <script src="{{ url('vendors/datatables/js/jquery.dataTables.min.js') }}"></script>

        <script type="text/javascript" src="{{ url('vendors/jquery-validation/dist/jquery.validate.min.js') }}"></script>

        <script src="{{ url('assets/form-validation.js') }}"></script>

        <script src="{{ url('assets/DT_bootstrap.js')  }}"></script>

        <script src="{{ URL::to('assets/scripts.js')}}"></script>
        
        <script src="{{ URL::to('assets/colorpicker/bootstrap-colorpicker.min.js') }}"></script>
        <script>
        //var op = $(".top_n").offset();
       
        $(function () {
            //Colorpicker
            $(".my-colorpicker1").colorpicker();
            //color picker with addon
            $(".my-colorpicker2").colorpicker();
          });    


        $(function() {

            // Easy pie charts

            $('.chart').easyPieChart({animate: 1000});

        });

        </script>

    <!-- datepicker validation chozen -->

       <script>

            jQuery(document).ready(function() {   

               FormValidation.init();

            });

            

        $(function() {

            $(".datepicker").datepicker({
                format: "dd-mm-yyyy",
                todayHighlight: true,
                autoclose: true
                
            });

            $(".uniform_on").uniform();

            $(".chzn-select").chosen();

            // $('.textarea').wysihtml5();



            $('#rootwizard').bootstrapWizard({onTabShow: function(tab, navigation, index) {

                var $total = navigation.find('li').length;

                var $current = index+1;

                var $percent = ($current/$total) * 100;

                $('#rootwizard').find('.bar').css({width:$percent+'%'});

                // If it's the last tab then hide the last button and show the finish instead

                if($current >= $total) {

                    $('#rootwizard').find('.pager .next').hide();

                    $('#rootwizard').find('.pager .finish').show();

                    $('#rootwizard').find('.pager .finish').removeClass('disabled');

                } else {

                    $('#rootwizard').find('.pager .next').show();

                    $('#rootwizard').find('.pager .finish').hide();

                }

            }});

            $('#rootwizard .finish').click(function() {

                alert('Finished!, Starting over!');

                $('#rootwizard').find("a[href*='tab1']").trigger('click');

            });

        });

        </script>



        <script>

        $(function() {



            // Ckeditor standard

            $( 'textarea#ckeditor_standard' ).ckeditor({width:'98%', height: '150px', toolbar: [

                { name: 'document', items: [ 'Source', '-', 'NewPage', 'Preview', '-', 'Templates' ] }, // Defines toolbar group with name (used to create voice label) and items in 3 subgroups.

                [ 'Cut', 'Copy', 'Paste', 'PasteText', 'PasteFromWord', '-', 'Undo', 'Redo' ],          // Defines toolbar group without name.

                { name: 'basicstyles', items: [ 'Bold', 'Italic' ] }

            ]});

            $( 'textarea#ckeditor_full' ).ckeditor({width:'98%', height: '150px'});

        });



        </script>

        

    </body>



</html>