<div class="navbar navbar-fixed-top">
            <div class="navbar-inner">
                <div class="container-fluid">
                    <a class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse"> <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                     <span class="icon-bar"></span>
                    </a>
                    <a class="brand" href="{{ URL::to('/home')}}"><img src="{{url('images/logo-no-bg.png')}}" width="50" style="margin-top: -10px;padding: 5px;"></a>
                    <div class="nav-collapse collapse" style="margin-top: 10px;">
                        <ul class="nav pull-right">
                            <li><a>@lang('home.currency') <b>US</b></a></li>
                            <li class="dropdown">
                                <a href="#" role="button" class="dropdown-toggle" data-toggle="dropdown"> <i class="icon-user"></i>@if(Auth::user()->name_kh) {{Auth::user()->name_kh}}@else {{Auth::user()->username}} @endif <i class="caret"></i>

                                </a>    
                                <ul class="dropdown-menu">
                                    <li>
                                        <a tabindex="-1" href="{{ url('users/profile') }}"><i class="icon-list"></i> @lang('home.user-profile')</a>
                                    </li>
                                    <li>
                                        <a tabindex="-1" href="{{ url('change_mypassword_user/change') }}"><i class="icon-wrench"></i> @lang('home.change-pass')</a>
                                    </li>
                                    <li class="divider"></li>
                                    <li>
                                        <a tabindex="-1" href="{{ url('logout') }}"><i class="icon-share"></i> @lang('home.logout')</a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                        <ul class="nav">

                                <li  class="{{ Request::segment(1) == '/' ? 'active' : null }}">
                                        <a href="{{ URL::to('/') }}"><b>@lang('home.Home')</b></a>
                                </li>
                                 <li class="dropdown {{Request::segment(1) == 'accounts'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>@lang('client.client-management')</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::segment(2) == 'accounts' ? 'active' : null }}">
                                                <a href="{{ url('accounts') }}">@lang('client.client-management')</a>
                                            </li> 
                                                <li class="{{ Request::segment(1) == 'account_type' ? 'active' : null }}">
                                                <a href="{{ url('account_type') }}">​@lang('client.client-type-management')</a>
                                            </li>
                                            </li> 
                                                <li class="{{ Request::segment(2) == 'all_sub_client' ? 'active' : null }}">
                                                <a href="{{ url('accounts/all_sub_client') }}">@lang('client.guaranteed-management')</a>
                                            </li>
                                        </ul>
                                </li>
                                <!-- <li  class="{{ Request::segment(1) == 'products' ? 'active' : null }}">
                                        <a href="{{ URL::to('products') }}"><b>ផលិតផល</b></a>
                                </li> -->
                                <li  class="{{ Request::segment(1) == 'credit_sales_purchas_order' ? 'active' : null }}">
                                        <a href="{{ URL::to('credit_sales_purchas_order') }}"><b>@lang('purchase.purchase-order')</b></a>
                                </li>

                                <li class="dropdown {{Request::segment(1) == 'products'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>@lang('product.product')</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                                <li  class="{{ Request::segment(1) == 'suppliers' ? 'active' : null }}">
                                                        <a href="{{ URL::to('suppliers') }}"><b>@lang('product.supplier')</b></a>
                                                </li> 
                                                <li class="{{ Request::segment(2) == 'products' ? 'active' : null }}">
                                                    <a href="{{ url('products/products') }}">@lang('product.productmanagement')</a>
                                                </li> 
                                                 <li class="{{ Request::segment(2) == 'brands' ? 'active' : null }}">
                                                    <a href="{{ url('products/brands') }}">@lang('product.brand')</a>
                                                </li> 
                                                <li class="{{ Request::segment(2) == 'categorys' ? 'active' : null }}">
                                                    <a href="{{ url('products/categorys') }}">@lang('product.product-type')</a>
                                                </li>     
                                        </ul>
                                </li>
                                
                                <li  class="{{ Request::segment(1) == 'credit_sales' ? 'active' : null }}">
                                        <a href="{{ URL::to('credit_sales') }}?redirect=credit_sales&sales=0"><b>@lang('creditsale.credit-sale')</b></a>
                                </li>
                               
                                <li  class="{{ Request::segment(1) == 'aprove_credit_sales' ? 'active' : null }}">
                                        <a href="{{ URL::to('aprove_credit_sales') }}"><b>@lang('approve.approve-credit')</b></a>
                                </li>

                                <li  class="{{ Request::segment(1) == 'comment_finish' ? 'active' : null }}">
                                        <a href="{{ URL::to('comment_finish') }}"><b>@lang('comment.comment-finish')</b></a>
                                </li>

                                <li class="dropdown {{Request::segment(1) == 'given_products'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>@lang('giveproduct.give-product')</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                                <li class="{{ Request::segment(2) == 'given_products' ? 'active' : null }}">
                                                    <a href="{{ url('given_products') }}">@lang('giveproduct.give-to-co') </a>
                                                </li> 
                                                <li class="{{ Request::segment(2) == 'given_products/client' ? 'active' : null }}">
                                                    <a href="{{ url('given_products/client') }}">@lang('giveproduct.give-to-client') </a>
                                                </li> 
                                        </ul>
                                </li>

                                @if(Auth::user()->groups->first()->hasPermission(['developer']))
                                <li class="{{ Request::segment(1) == 'payment_credit_back'   ? 'active' : null }}">
                                    <a href="{{URL::to('payment_credit_back')}}"><b>@lang('schedule.schedule')</b></a> <!--Payment Back-->
                                </li>
                                 @endif

                                @if(Auth::user()->groups->first()->hasPermission(['developer']))
                                <li class="dropdown {{Request::segment(1) == 'report'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>@lang('report.report')</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="{{ Request::segment(2) == 'item' ? 'active' : null }}">
                                                <a href="{{ url('report/item/report_item_list') }}"> @lang('report.product-report') </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'clients' ? 'active' : null }}">
                                                <a href="{{ url('report/clients/report_client_list') }}"> @lang('report.client-report') </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report_sale_by_credit' ? 'active' : null }}">
                                                <a href="{{ url('report/sales/report_sale_by_credit') }}"> @lang('report.sale-credit-report') </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report_direct_payment' ? 'active' : null }}">
                                                <a href="{{ url('report/sales/report_direct_payment') }}"> @lang('report.direct-payment-report')  </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report_payment_back' ? 'active' : null }}">
                                                <a href="{{ url('report/payment_back/report_payment_back') }}"> @lang('report.payment-back-report')  </a>
                                            </li>
                                            <li class="{{ Request::segment(3) == 'report/payment_back/report_summery_payment_back' ? 'active' : null }}">
                                                <a href="{{ url('report/payment_back/report_summery_payment_back') }}"> @lang('report.total-payment-back-report')  </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_schedules' ? 'active' : null }}">
                                                <a href="{{ url('report/report_schedules') }}">@lang('report.schedule-report') </a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_sale' ? 'active' : null }}">
                                                <a href="{{ url('report/report_sale') }}">@lang('report.sale-cash-report')</a>
                                            </li> 
                                            <li class="{{ Request::segment(2) == 'report_sale_by_credit' ? 'active' : null }}">
                                                <a href="{{ url('report/report_sale_by_credit') }}">@lang('report.credit-sale-report')</a>
                                            </li>
                                            <li class="{{ Request::segment(2) == 'report_receiving_product' ? 'active' : null }}">
                                                <a href="{{ url('report/report_receiving_product') }}">@lang('report.receiving-product-report') </a>
                                            </li> 
                                            <li class="{{ Request::segment(2) == 'report_po_product' ? 'active' : null }}">
                                                <a href="{{ url('report/report_po_product') }}">@lang('report.purchase-order-report') </a>
                                            </li>
                                            
                                        </ul>
                                        
                                        
                                </li>
                                 @endif
                                <li class="dropdown {{Request::segment(1) == 'history_log' || Request::segment(1) == 'users' || Request::segment(1) == 'users-permission' || Request::segment(1) == 'interest_rate'||Request::segment(1) == 'location' || Request::segment(1) == 'public_holiday'  ? 'active' : null }}" >
                                    <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>@lang('setting.setting')</b> <b class="caret"></b></a>
                                        <ul class="dropdown-menu">
                                            <li class="dropdown-submenu">
                                                <a href="#" data-toggle="dropdown" class="dropdown-toggle"><b>@lang('setting.language')</b></a>
                                                    <ul class="dropdown-menu">
                                                        <li>
                                                            <a href="locale/kh"><img src="{{asset('images/kh.jpg')}}" width="20px" height="20px">​ ខ្មែរ</a>
                                                        </li>
                                                        <li>
                                                            <a href="locale/en"><img src="{{asset('images/en.jpg')}}" width="20px" height="20px"> English</a>
                                                        </li>
                                                    </ul>
                                                </a>  
                                            </li>  
                                            @if(Auth::user()->groups->first()->hasPermission(['create-user-groups']))
                                                @if(Auth::user()->groups->first()->hasPermission(['list-user-groups']))
                                                    <li class="dropdown-submenu">
                                                        <a href="{{ url('#') }}">  @lang('setting.user')</a>
                                                        <ul class="dropdown-menu">
                                                            <li>
                                                                <a tabindex="-1" href="{{ URL::to('users')}}">@lang('setting.user')</a>
                                                            </li>
                                                            <li>
                                                                <a tabindex="-1" href="{{ URL::to('users-groups')}}">@lang('setting.user-management')</a>
                                                            </li>
                                                            @if(Auth::user()->groups->first()->hasPermission(['developer']))   
                                                                <li>
                                                                    <a tabindex="-1" href="{{ URL::to('users-permission')}}">@lang('setting.permission')</a>
                                                                </li>
                                                            @endif
                                                        </ul>
                                                    </li>
                                                @endif
                                                <li class="{{ Request::segment(1) == 'branch' ? 'active' : null }}">
                                                    <a href="{{ url('branch') }}">@lang('setting.branch')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'interest_rate' ? 'active' : null }}">
                                                    <a href="{{ url('interest_rate') }}">@lang('setting.interest-rate')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'new_interest_rate' ? 'active' : null || Request::segment(1) == 'service_interest_rate' ? 'active' : null }}">
                                                    <a href="{{ url('new_interest_rate') }}">@lang('setting.new-interest-rate')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'public_holiday' ? 'active' : null }}">
                                                    <a href="{{ url('public_holiday') }}">@lang('setting.holiday')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'exchange_rate' ? 'active' : null }}">
                                                    <a href="{{ url('exchange_rate') }}">@lang('setting.exchange-rate')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'penaty_date' ? 'active' : null }}">
                                                    <a href="{{ url('penaty_date') }}">@lang('setting.penalty')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'list_position' ? 'active' : null }}">
                                                    <a href="{{ url('list_position') }}">@lang('setting.position')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'module_interest' ? 'active' : null }}">
                                                    <a href="{{ url('module_interest') }}">@lang('setting.module-interest')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'history_log' ? 'active' : null }}">
                                                    <a href="{{ url('history_log') }}">@lang('setting.history-log')</a>
                                                </li>
                                                 <li class="{{ Request::segment(1) == 'currency' ? 'active' : null }}">
                                                    <a href="{{url('currency')}}">@lang('setting.currency')</a>
                                                </li>
                                                <li class="{{ Request::segment(1) == 'rate_percent' ? 'active' : null }}">
                                                    <a href="{{url('rate_percent')}}">@lang('setting.rate-percent')</a>
                                                </li>
                                                  <li class="{{ Request::segment(1) == 'configure' ? 'active' : null }}">
                                                    <a href="{{url('configure')}}">@lang('setting.configure')</a>
                                                </li>
                                            @endif
                                    </ul>
                            </li>
                            <li class="dropdown">
                                <a href="#" data-toggle="dropdown" class="dropdown-toggle">La<b class="caret"></b></a>
                                    <ul class="dropdown-menu">
                                        <li>
                                            <a href="locale/kh"><img src="{{asset('images/kh.jpg')}}" width="20px" height="20px">​ ខ្មែរ</a>
                                        </li>
                                        <li>
                                            <a href="locale/en"><img src="{{asset('images/en.jpg')}}" width="20px" height="20px"> English</a>
                                        </li>
                                    </ul>
                                </a>  
                            </li>  
                        </ul>
                    </div>
                    <!--/.nav-collapse -->
                </div>
            </div>
          
    </div>