@extends('credit_sale.layout.master')
@section('contend')	
    <!-- Model Edit -->
    <div class="container-fluid">
    <div class="row-fluid">
    <div class="block">
        <div class="navbar navbar-inner block-header">
            <div class="muted pull-left">
                <a href="{{url('accounts')}}"> គណនី</a> 
                <span class="divider">|</span> កែប្រែ​​គណនីអតិថិជន
            </div>
        </div>
        <div class="block-content collapse in">
        @include('errors.error')
            <div class="span8"  style="margin: 0 auto !important;float: none;">
                <center>
                    <h3 class="cen_title"> កែប្រែ​​គណនីអតិថិជន</h3>
                </center>
            </div>
            <form role="form" id="form_insert_client" name="form_insert_client" method="POST"  enctype="multipart/form-data">
                <div class="span8"  style="margin: 0 auto !important;float: none;">

                    {{ csrf_field() }}

                        <div class="control-group">
                                <div class="span12" style="margin-left:0;">
                                    <div class="span6">
                                        <label class="control-label" title="ប្រភេទកម្ចី">ប្រភេទអតិថិជន<span class="required" title="This place you must be select data">*</span></label>
                                        <select name="client_type" class="span12 m-wrap" id="client_type">
                                        <?php $client_type  = App\ClientType::get(); ?>
                                        @foreach($client_type as  $ct)
                                            <option value="{{ $ct->id }}">{{ $ct->dispay_name }}</option>
                                        @endforeach 
                                            <!-- <option value="2">ក្រុម</option> -->
                                        </select>
                                    </div>

                                    <div class="span6">
                                        <label class="control-label" title="ជ្រើសរើសសាខារបស់អតិថិជន ដែលគាត់មកខ្ចីប្រាក់, ចាំបាច់ត្រូវតែមាន">ជ្រើសរើសសាខា <span class="required" title="This place you must be select data">*</span></label>
                                        <?php $branch = DB::table('mfi_branch')->where('deleted','=',0)->get(); ?>

                                        <select name="branch_id" class="span12 m-wrap" id="branch_id">
                                                <!--<option value="0">-- Please Select Brand --</option>-->

                                            @foreach($branch as $b)

                                                <option value="{{ $b->id }}">{{ $b->brand_name }}</option>

                                            @endforeach

                                        </select>

                                    </div>
                                </div>
                            
                            <div class="span12" style="margin-left:0;">

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាម <span class="required" title="This place you must be put data">*</span></label>

                                    <div class="controls">

                                        <input type="text" id="client_first_name_kh" class="span12 m-wrap kh_name_first" name="client_first_name_kh" data-required="1" value="{{ old('client_first_name_kh') }}"/>

                                    </div>

                                    <p class="alert-danger">{{$errors->first('client_first_name_kh')}}</p>

                                </div>

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាម <span class="required" title="This place you must be put data">*</span></label>

                                    <div class="controls">

                                        <input type="text" id="client_second_name_kh" class="span12 m-wrap kh_name_last" name="client_second_name_kh" data-required="1" value="{{ old('client_second_name_kh') }}"/>

                                    </div>

                                    <p class="alert-danger">{{$errors->first('client_second_name_kh')}}</p>

                                </div>

                            </div>



                            <div class="span12" style="margin-left:0;">

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាមជាអក្សរឡាតាំង (First Name)</label>

                                    <div class="controls">

                                        <input type="text" autocapitalize="word" id="client_first_name_en" name="client_first_name_en" data-required="1" value="{{ old('client_first_name_en') }}" class="span12 m-wrap en_name_first"/>

                                    </div>

                                </div>

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាមជាអក្សរឡាតាំង (Last Name)</label>

                                    <div class="controls">

                                        <input type="text" autocapitalize="word" id="client_second_name_en" class="span12 m-wrap en_name_last" name="client_second_name_en" data-required="1" value="{{ old('client_second_name_en') }}"/>

                                    </div>
                                    
                                </div>
                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ភេទ <span class="required" title="This place you must be put data">*</span></label>

                                    <div class="controls">

                                        <select id="selectError"​ class="span12 m-wrap client_gender" name="client_gender">

                                        <option value="M">ប្រុស</option>

                                        <option value="F">ស្រី</option>

                                        </select>

                                    </div>

                                    <p class="alert-danger">{{$errors->first('client_gender')}}</p>

                            </div>
                            <div class="span12" style="margin-left:0px;">
                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ថ្ងៃខែឆ្នាំកំណើត <span class="required" title="This place you must be put data">*</span></label>
                                <div class="controls">
                                    <div class="span4">
                                        <label>ថ្ងៃ</label>
                                        <select name="day" id="select01" class="chzn-select get_date_dob span12 m-wrap"></select>
                                            <p class="alert-danger">{{$errors->first('day')}}</p>
                                    </div>
                                    <div class="span4">
                                        <label>ខែ</label>
                                        <select name="month" id="select01" class="chzn-select get_month_dob span12 m-wrap">
                                        <option value=""></option>
                                        <option value="01">មករា</option>
                                        <option value="02">កុម្ភៈ</option>
                                        <option value="03">មីនា</option>
                                        <option value="04">មេសា</option>
                                        <option value="05">ឧសភា</option>
                                        <option value="06">មិថុនា</option>
                                        <option value="07">កក្កដា</option>
                                        <option value="08">សីហា</option>
                                        <option value="09">កញ្ញា</option>
                                        <option value="10">តុលា</option>
                                        <option value="11">វិចិ្ឆកា</option>
                                        <option value="12">ធ្នូ</option>
                                        </select>
                                    </div>
                                    <div class="span4">
                                        <label>ឆ្នាំ</label>
                                        <select name="year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
                                    </div>
                                </div>
                            </div>
                        <div class="span12" style="margin-left:0px;">
                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">សញ្ជាតិ <span class="required" title="This place you must be put data">*</span></label>

                            <div class="controls">

                                <label class="checkbox-inline" style="margin-top:10px;">

                                    <input type="radio" id="default" class="check_me" checked name="client_nationality1" value="1"> ខ្មែរ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" id="other" class="check_me" name="client_nationality1" value="2"> ផ្សេងៗ

                                </label>

                                <input type="text" id="show_other" name="client_nationality2" placeholder="Input other" data-required="1" value="{{ old('client_nationality') }}" class="span12 m-wrap"/>

                            </div>

                            <p class="alert-danger">{{$errors->first('client_nationality')}}</p>

                            <script type="text/javascript">

                            $(document).ready(function(){

                                $('#show_other').hide();



                                $('#default').click(function (){

                                    $('#show_other').hide();

                                });

                                $('#other').click(function (){

                                    $('#show_other').show();

                                });

                            });

                            </script>

                            <br/>
                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ​ ( <b><span id="characters"><span></b> ) <span class="required" title="This place you must be put data">*</span></label>

                            <div class="controls">

                                <input type="text" id="count-number" name="identify_num" onkeypress="return forceNumber(event);" data-required="1" value="{{ old('identify_num') }}"  class="span12 m-wrap identify_num"/>

                            </div>

                            <p class="alert-danger">{{$errors->first('identify_num')}}</p>

                            <script type="text/javascript">

                                $('#count-number').keyup(updateCount);

                                $('#count-number').keydown(updateCount);



                                function updateCount() {

                                    var cs = $(this).val().length;

                                    $('#characters').text(cs);

                                }
                                //can type English only
                                function forceNumber(e) {
                                    var keyCode = e.keyCode ? e.keyCode : e.which;
                                        if(keyCode == 32)
                                            return true;
                                        if(48 <= keyCode && keyCode <= 57)
                                            return true;
                                        if(65 <= keyCode && keyCode <= 90)
                                            return true;
                                        if(97 <= keyCode && keyCode <= 122)
                                            return true;
                                        return false;
                                }

                            </script>

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប្រភេទប័ណ្ណ <span class="required" title="This place you must be put data">*</span></label>

                            <div class="controls">

                                <label class="checkbox-inline" style="margin-top:10px;">

                                    <input type="radio" class="default client_type_idcard1" checked name="client_type_idcard1" value="1"> អត្តសញ្ញាណប័ណ្ណ

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" class="default client_type_idcard2" name="client_type_idcard1" value="2"> លិខិនឆ្លង់ដែន

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" class="default client_type_idcard3" name="client_type_idcard1" value="3"> សំបុត្របញ្ជាក់កំណើត

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" class="default client_type_idcard4" name="client_type_idcard1" value="4"> សៀវភៅសា្នក់នៅ

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" class="default client_type_idcard5" name="client_type_idcard1" value="5"> សៀវភៅគ្រួសារ

                                    &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" id="other1" name="client_type_idcard1" value="6"> ផ្សេងៗ

                                </label>

                                <input type="text" id="show_other1" name="client_type_idcard2" placeholder="Input other" data-required="1" value="{{ old('client_type_idcard') }}" class="span12 m-wrap"/>

                            </div>

                            <p class="alert-danger">{{$errors->first('client_type_idcard')}}</p>

                            <script type="text/javascript">

                                $(document).ready(function(){

                                    $('#show_other1').hide();



                                    $('.default').click(function (){

                                        $('#show_other1').hide();

                                    });

                                    $('#other1').click(function (){

                                        $('#show_other1').show();

                                    });

                                });

                            </script>

                            <br/>

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ចេញដោយ</label>

                            <div class="controls">

                                <label class="checkbox-inline" style="margin-top:10px;">

                                    <input type="radio" id="default2" checked name="client_aprovel_idcard_by1" value="1"> ក្រសួងមហាផ្ទៃ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                    <input type="radio" id="other2" name="client_aprovel_idcard_by1" value="2"> ផ្សេងៗ

                                </label>

                                <input type="text" id="show_other2" name="client_aprovel_idcard_by2" placeholder="Input other" data-required="1" value="{{ old('client_aprovel_idcard_by') }}" class="span12 m-wrap"/>

                            </div>
                            <br/>
                            <script type="text/javascript">

                            $(document).ready(function(){

                                $('#show_other2').hide();



                                $('#default2').click(function (){

                                    $('#show_other2').hide();

                                });

                                $('#other2').click(function (){

                                    $('#show_other2').show();

                                });

                            });

                            </script>			  	

                        </div>
                        
                    </div>	


                    <div class="span12" style="margin-left:0px;">
                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ថ្ងៃចេញអត្តសញ្ញាណប័ណ្ណ <!-- <span class="required" title="This place you must be put data">*</span> --></label>
                        <div class="controls">
                            <div class="span4">
                                <label>ថ្ងៃ</label>
                                <select name="identify_day" id="select01" class="chzn-select identify_day span12 m-wrap"></select>
                                    <p class="alert-danger">{{$errors->first('day')}}</p>
                            </div>
                            <div class="span4">
                                <label>ខែ</label>
                                <select name="identify_month" id="identify_month" class="chzn-select span12 m-wrap">
                                <option value=""></option>
                                <option value="01">មករា</option>
                                <option value="02">កុម្ភៈ</option>
                                <option value="03">មីនា</option>
                                <option value="04">មេសា</option>
                                <option value="05">ឧសភា</option>
                                <option value="06">មិថុនា</option>
                                <option value="07">កក្កដា</option>
                                <option value="08">សីហា</option>
                                <option value="09">កញ្ញា</option>
                                <option value="10">តុលា</option>
                                <option value="11">វិចិ្ឆកា</option>
                                <option value="12">ធ្នូ</option>
                                </select>
                            </div>
                            <div class="span4">
                                <label>ឆ្នាំ</label>
                                <select name="identify_year" id="select01" class="chzn-select identify_year span12 m-wrap"></select>
                            </div>
                        </div>
                    </div>

                    <div class="span12" style="margin-left:0;">

                        <div class="span12">

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន"> ចេញដោយឈ្មោះ</label>

                            <div class="controls">

                                <input type="text" autocapitalize="word" id="approve_by_name" name="approve_by_name" data-required="1" value="{{ old('approve_by_name') }}" class="span12 m-wrap"/>

                            </div>

                        </div>
                    </div>
                            
                        <div class="span12" style="margin-left:0px;">
                        
                            <div class="span4">
                                <label class="control-label">អាសយដ្ឋានផ្ទះលេខ</label>

                                <div class="controls">

                                    <input type="text" name="client_house_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap home_num"/>

                                </div>
                            </div>
                            <div class="span4">
                                <label class="control-label">ក្រុមទី</label>

                                <div class="controls">

                                    <input type="text" name="client_group_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap group_num"/>

                                </div>
                            </div>
                            <div class="span4">
                                <label class="control-label">ផ្លូវលេខ</label>

                                <div class="controls">

                                    <input type="text" name="client_st_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap street_num"/>

                                </div>
                            </div>
                        </div>
                        <div class="span12" style="margin-left:0px;">		
                        
                                @include('credit_sale.client_management.p_location')
                        </div>	
                            <br/>

                            <label class="control-label">លេខទូរសព្ទទំនាក់ទំនង <span class="required" title="This place you must be put data">*</span></label>

                            <div class="controls">

                                <input type="text" name="phone" data-required="1" value="{{ old('phone') }}" class="span12 m-wrap telnumber phone" maxlength="12"/>

                            </div>

                            <p class="alert-danger">{{$errors->first('phone')}}</p>
                            <script type="text/javascript">
                                $('.telnumber').keyup(function() {
                                    foo = $(this).val().split("-").join(""); // remove hyphens
                                
                                        foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");

                                        $(this).val(foo);
                                
                                    });
                            </script>



                            <label class="control-label">មុខរបរ</label>

                            <div class="controls">

                                <input type="text" name="client_job" data-required="1" value="{{ old('client_job') }}" class="span12 m-wrap job"/>

                            </div>

                            <label class="control-label">ទីកន្លែងប្រកបមុខរបរ</label>

                            <div class="controls">

                                <input type="text" name="client_address_job" data-required="1" value="{{ old('client_address_job') }}" class="span12 m-wrap place_job"/>

                            </div>

                        
                            <br/>
                            <label>កូអរដោនេ</label>
                            X: <input type="text" name="client_lutidued" class="span5 m-wrap client_lutidued" style="width: 14%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                            Y: <input type="text" name="client_longitidute" class="span5 m-wrap client_longitidute" style="width: 14%;">

                            <br/>
                            <label class="control-label">ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ <span class="required" title="This place you must be select data">*</span></label>

                            <div class="controls">

                                <input type="file" name="client_upload_image" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត" />

                            </div>
                            <p class="client_upload_image"></p>		
                            <p class="alert-danger">{{$errors->first('client_upload_image')}}</p>

                            <br/>

                            <a href="#" id="add" class="btn btn-danger pull-right">+ បន្ថែមទៀត</a>

                                <label class="control-label">ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ <span class="required" title="This place you must be select data">*</span></label>

                                <div class="controls" id="addimage">

                                    <input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវមានទំហំសមល្មម"/>

                                </div>

                                <p class="alert-danger">{{$errors->first('image_uploade_id_familly')}}</p>
                                <p class="image_uploade_id_familly"></p>	
                        </div>

                    </div>

                    

                    <div class="span8"  style="margin: 0 auto !important;float: none;">

                        <br/>

                            <label class="control-label">កំណត់ចំណាំ</label>
                            
                            <div class="controls">
                                <textarea name="client_note"  data-required="1" rows="5" class="span12 m-wrap client_note">{{ old('client_job_profit') }}</textarea>

                    </div>

                        <br/>

                        <center>
                                <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="Edit">បញ្ចូល</button>
                                <input type="hidden" id="client_id" name="client_id" value="0">
                            <a href="{{url('accounts')}}" class="btn btn-danger">ត្រលប់</a>

                        </center>

                    </div>
                <br/><br/>
                </form>										


        </div>
        <!-- <div class="modal-footer">
            <a data-dismiss="modal" class="btn btn-primary" href="#">Confirm</a>
            <button data-dismiss="modal" class="btn btn-primary">Su</button>
            <a data-dismiss="modal" class="btn" href="#">Cancel</a>
        </div> -->
    </div>
    </div>
    </div>

    <!-- Edit Model Edit -->
    <script>
        $('#add').click(function(event){
				event.preventDefault();
			var img = '<div class="controls" id="addimage">' +

				  	'<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

				  	'</div>';
			$('#addimage').append(img);
		});
		var age_get = 18;
		var d = new Date();
		var year_d = d.getFullYear() - age_get;
		var i;
		var year_dob ="";
		for(i = 1940 ; i <= year_d ; i ++){
			year_dob += "<option value=''></option>";
			year_dob += "<option value='"+i+"'>"+i+"</option>";
		}

		$(".get_year_dob").html(year_dob);
        $(".identify_year").html(year_dob);	

		var lastday = function(y,m){
				return  new Date(y, m +1, 0).getDate();
		}
		var ldfm = lastday(d.getFullYear(),d.getMonth());
        var date_of = "";
		for (var m = 1 ; m <= ldfm ; m ++ ){
			date_of += "<option value=''></option>";
			date_of += "<option value='"+m+"'>"+m+"</option>";
		}
		$(".get_date_dob").html(date_of);
        $(".identify_day").html(date_of);	
		//Edit Client  Show Form  Edit 
		var url_e = "{{ route('accounts') }}";	
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            $(".tbn_add").val("update");
            var url_edit = url_e+"/"+"{{$client_id}}"+"/edit";
            // alert(url_edit);
            var client;
            var forData = {};
                var out = "";
                $.ajax({
                        type: "GET",
                        url: url_edit, 
                        dataType: "json",
                        data: forData,
                        success: function(result ,xhr){
                            console.log(result);
                            $.each(result ,function(i, field){
                                
                                if(i == "client"){
                                    if(field.description !== null){
                                        $(".client_note").val(field.description);
                                    }
                                    if(field.latitude !== null){
                                        $(".client_lutidued").val(field.latitude);
                                    }
                                    if(field.longitude !== null){
                                        $(".client_longitidute").val(field.longitude);
                                    }
                                    if(field.kh_name_first !== null){
                                        $(".kh_name_first").val(field.kh_name_first);
                                    }
                                    if(field.kh_name_last !== null){
                                        $(".kh_name_last").val(field.kh_name_last);
                                    }
                                    if(field.en_name_first !== null){
                                        $(".en_name_first").val(field.en_name_first);
                                    }
                                    if(field.en_name_last !== null){
                                        $(".en_name_last").val(field.en_name_last);
                                    }
                                    if(field.job !== null){
                                        $(".job").val(field.job);
                                    }
                                    if(field.place_job !== null){
                                        $(".place_job").val(field.place_job);
                                    }
                                    if(field.street_num !== null){
                                        $(".street_num").val(field.street_num);
                                    }
                                    if(field.home_num !== null){
                                        $(".home_num").val(field.home_num);
                                    }
                                    if(field.group_num !== null){
                                        $(".group_num").val(field.group_num);
                                    }		
                                    if(field.identify_num !== null){
                                        $(".identify_num").val(field.identify_num);
                                        var idn = field.identify_num;
                                        $("#characters").text(idn.length); 
                                    }
                                    if(field.phone !== null){
                                        $(".phone").val(field.phone);
                                    }


                            //conditional 		
                                
                                    $('#client_type').find("[value='"+field.client_type_id+"']").attr("selected","selected");
                                    $('#branch_id').find("[value='"+field.branch_id+"']").attr("selected","selected");
                                    $('.client_gender').find("[value='"+field.gender+"']").attr("selected","selected");
                        // DOB		
                            if(field.dob !== null){		
                                    var dob1 = new Date(field.dob);
                                    var day1 = dob1.getDate();
                                    var month2 = dob1.getMonth() + 1;
                                    if(month2 < 10){
                                        var month1 = "0"+month2;
                                    }else{
                                        var month1 = month2;
                                    }
                                    var year1 = dob1.getFullYear();
                                    
                                    $('.get_date_dob').find("[value='"+day1+"']").attr("selected","selected");
                                    $('.get_month_dob').find("[value='"+month1+"']").attr("selected","selected");
                                    $('.get_year_dob').find("[value='"+year1+"']").attr("selected","selected");
                                    $('#client_province').find("[value='"+field.province+"']").attr("selected","selected");
                                    $('#client_district').find("[value='"+field.district+"']").attr("selected","selected");
                                    $('#client_commune').find("[value='"+field.commune+"']").attr("selected","selected");
                                    $('#client_village').find("[value='"+field.vilige+"']").attr("selected","selected");
                                    
                                    //$('#client_village').next().find("[data-option-array-index='"+day1+"']").addClass("result-selected");
                                    // Day 	
                                        $('.get_date_dob').next().find("span").text(day1);
                                        $('.get_date_dob').next().find("a").addClass("chzn-single");
                                        $('.get_date_dob').next().find("a").removeClass("chzn-default");
                                    // Month 
                                        var month3 = $('.get_month_dob').find("[value='"+month1+"']").text();
                                        $('.get_month_dob').next().find("span").text(month3);
                                        $('.get_month_dob').next().find("a").addClass("chzn-single");
                                        $('.get_month_dob').next().find("a").removeClass("chzn-default");
                                    // Year  
                                        $('.get_year_dob').next().find("span").text(year1);
                                        $('.get_year_dob').next().find("a").addClass("chzn-single");
                                        $('.get_year_dob').next().find("a").removeClass("chzn-default");
                                }
                            
                                // Province 
                                    if(field.province !== null){
                                        $("#client_province").next().find("span").text(field.province);
                                        $('#client_province').next().find("a").addClass("chzn-single");
                                        $('#client_province').next().find("a").removeClass("chzn-default");	
                                    }
                                
                                // District 
                                    if(field.district !== null){
                                        $("#client_district").next().find("span").text(field.district);
                                        $('#client_district').next().find("a").addClass("chzn-single");
                                        $('#client_district').next().find("a").removeClass("chzn-default");		
                                    }
                                // Comnunce 
                                    if(field.commune !== null){
                                        $("#client_commune").next().find("span").text(field.commune);
                                        $('#client_commune').next().find("a").addClass("chzn-single");
                                        $('#client_commune').next().find("a").removeClass("chzn-default");
                                    }
                                // Villeg 	
                                    if(field.vilige !== null){
                                        $("#client_village").next().find("span").text(field.vilige);
                                        $('#client_village').next().find("a").addClass("chzn-single");
                                        $('#client_village').next().find("a").removeClass("chzn-default");
                                    }
                                //Check Nationality
                                    //$(".check_me").removeAttr("checked");
                                    if(field.nationality ==1){
                                        $("#default").attr("checked","checked");
                                    }else{
                                        $("#other").attr("checked","checked");
                                            $("#show_other").css({"display":"block"});
                                            $("#show_other").val(field.nationality);
                                    }
                                //ប្រភេទប័ណ្ណ 	
                                    if(field.identify_type ==="1"){
                                        $('.client_type_idcard1').attr("checked","checked");
                                    }else if(field.identify_type ==="2"){
                                        $('.client_type_idcard2').attr("checked","checked");
                                    }else if(field.identify_type ==="3"){
                                        $('.client_type_idcard3').attr("checked","checked");
                                    }else if(field.identify_type ==="4"){
                                        $('.client_type_idcard4').attr("checked","checked");
                                    }else if(field.identify_type ==="5"){
                                        $('.client_type_idcard5').attr("checked","checked");
                                    }else{
                                        $("#other1").attr("checked","checked");
                                            $("#show_other1").css({"display":"block"});
                                            $("#show_other1").val(field.identify_type);
                                    }
                                // ថ្ងៃចេញប័ណ្ណ
                                    if(field.identify_date !== null){
                                        var date = new Date(field.identify_date);
                                        var day = date.getDate();
                                        var month = date.getMonth() + 1;
                                        if(month < 10){
                                            var month1 = "0"+month;
                                        }else{
                                            var month1 = month;
                                        }
                                        var year = date.getFullYear();

                                        $('.identify_day').find("[value='"+day+"']").attr("selected","selected");
                                        $('#identify_month').find("[value='"+month1+"']").attr("selected","selected");
                                        $('.identify_year').find("[value='"+year+"']").attr("selected","selected");
                                    }

                                //	ប្រភេទប័ណ្ណចេញដោយ	
                                    if(field.identify_by ==1){
                                        $("#default2").attr("checked","checked");
                                    }else{
                                        $("#other2").attr("checked","checked");
                                            $("#show_other2").css({"display":"block"});
                                            $("#show_other2").val(field.nationality);
                                    }

                                    if(field.approve_by !== null){
                                        $("#approve_by_name").val(field.approve_by);
                                    }
                                //ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ
                                    var url_image = "{{url('/')}}";			
                                            if(field.upload_relate_document !== null){
                                                $(".client_upload_image").html("<br/><img class='client_upload_image_respon' > ");
                                                $(".client_upload_image_respon").attr("src",url_image+"/Account/images/"+field.upload_relate_document);
                                            }
                                    }
                                
                            });
                            //ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ
                            var l =	(result.relate_file).length;
                            if(l > 0){
                                var im_up_id = "";
                                    $.each(result.relate_file, function(i, field){
                                                var url_image = "{{url('/')}}";			
                                                if(field.image_value !== null){
                                                    im_up_id += "<img class='client_upload_image_respon' id='image_uploade_id_familly"+i+"' src='"+url_image+"/Account/images/"+field.image_value+"'>"; 
                                                }

                                    });
                                    $(".image_uploade_id_familly").html(im_up_id);
                            }
                            
                        },
                        error: function (result ) {
                            console.log(result.stutus);
                        }

                });

                // insert  add client  to database
	var url = "{{ url('accounts/'.$client_id.'/edit') }}";
      $("#btn-save").click(function(e){
      		  $.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			        }
			    })
		     e.preventDefault();
            var state = $('#btn-save').val();
		    var client_id =  $('#client_id').val();
		    // high importance!, here you need change "yourformname" with the name of your form
		    var form = document.forms.namedItem("form_insert_client"); 
		    var formData = new FormData(form); 

		    $.ajax({    
		        type:'POST',
		        url: url, 
		        dataType: 'json',
		       contentType: false,
		        data: formData,
		       processData: false,
		        success: function (data,status, xhr) {
		          console.log(data);
		       
				if(data.redirect == 'credit_sales'){
					var post_url = "{{url('pos_add_all_session_credit_sale')}}";
					var done = function() {
							window.location.href = "{{url('credit_sales')}}?redirect=credit_sales&sales="+data.item;
					}; 
					
					   var formdata = {
							customer_id: data.datas.id,
							customer_name: data.datas.kh_username,    
						};  
					$.post(post_url, formdata , done);
				}else if(data.redirect == 'accounts'){
					window.scrollTo(0, 0);  
					$('.msg_show').html(data.msg_show);
					$('#form_insert_client').trigger("reset");
					$('#client_id').val("0");
					$('#btn-save').val("add");
					url = "{{ route('accounts/create') }}";
				}else{
					window.location="{{url('accounts')}}";
				}
		           
		       
		       },
		        error: function (data ,status, xhr) {
		            console.log(data.responseText);
		        }
		    });

		});
    </script>
    @endsection