@extends('credit_sale.layout.master')
@section('contend')
<div class="container-fluid">

    <div class="row-fluid">

        <!-- block -->

        <div class="block">

            <div class="navbar navbar-inner block-header">

                <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span>
                    បង្កើតគណនីថ្មី </div>

            </div>

            <div class="block-content collapse in">
            @include('credit_sale.client_management.menu_client')
                @include('errors.error')
                
                <div class="span8" style="margin: 0 auto !important;float: none;">

                    <center>

                        <h3 class="cen_title"> បង្កើតគណនីអ្នកធានាថ្មី </h3>

                    </center>
                    <div class="errors"></div>
                </div>
                <form role="form" id="form_insert_sub_client" name="form_insert_sub_client" method="POST" enctype="multipart/form-data">
                    <div class="span8" style="margin: 0 auto !important;float: none;">
                    {{ csrf_field() }}
                        <div class="control-group">
                        
                            <div class="span12" style="margin-left:0;">
                                
                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាម <span class="required"
                                            title="This place you must be put data">*</span></label>

                                    <div class="controls">

                                        <input type="text" id="client_first_name_kh" class="span12 m-wrap" name="client_first_name_kh"
                                            data-required="1" value="{{ old('client_first_name_kh') }}" />
                                            <input type="hidden" name="client_id" value="{{ $client_id }}">

                                    </div>

                                    <p class="alert-danger">{{$errors->first('client_first_name_kh')}}</p>

                                </div>

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាម <span class="required"
                                            title="This place you must be put data">*</span></label>

                                    <div class="controls">

                                        <input type="text" id="client_second_name_kh" class="span12 m-wrap" name="client_second_name_kh"
                                            data-required="1" value="{{ old('client_second_name_kh') }}" />

                                    </div>

                                    <p class="alert-danger">{{$errors->first('client_second_name_kh')}}</p>

                                </div>

                            </div>



                            <div class="span12" style="margin-left:0;">

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាមជាអក្សរឡាតាំង (First
                                        Name)</label>

                                    <div class="controls">

                                        <input type="text" autocapitalize="word" id="client_first_name_en" name="client_first_name_en"
                                            data-required="1" value="{{ old('client_first_name_en') }}" class="span12 m-wrap" />

                                    </div>

                                </div>

                                <div class="span6">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាមជាអក្សរឡាតាំង​ (Last Name)</label>

                                    <div class="controls">

                                        <input type="text" autocapitalize="word" id="client_second_name_en" class="span12 m-wrap"
                                            name="client_second_name_en" data-required="1" value="{{ old('client_second_name_en') }}" />

                                    </div>

                                </div>
                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ភេទ <span class="required" title="This place you must be put data">*</span></label>

                                <div class="controls">

                                    <select id="selectError" ​ class="span12 m-wrap" name="client_gender">

                                        <option value="M">ប្រុស</option>

                                        <option value="F">ស្រី</option>

                                    </select>

                                </div>

                                <p class="alert-danger">{{$errors->first('client_gender')}}</p>

                            </div>
                            <div class="span12" style="margin-left:0px;">
                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ថ្ងៃខែឆ្នាំកំណើត <span class="required"
                                        title="This place you must be put data">*</span></label>
                                <div class="controls">
                                    <div class="span4">
                                        <label>ថ្ងៃ</label>
                                        <select name="day" id="select01" class="chzn-select get_date_dob span12 m-wrap"></select>
                                        <p class="alert-danger">{{$errors->first('day')}}</p>
                                    </div>
                                    <div class="span4">
                                        <label>ខែ</label>
                                        <select name="month" id="select01" class="chzn-select span12 m-wrap" placeholder="ជ្រើសរើស">
                                            <option value=""></option>
                                            <option value="01">មករា</option>
                                            <option value="02">កុម្ភៈ</option>
                                            <option value="03">មីនា</option>
                                            <option value="04">មេសា</option>
                                            <option value="05">ឧសភា</option>
                                            <option value="06">មិថុនា</option>
                                            <option value="07">កក្កដា</option>
                                            <option value="08">សីហា</option>
                                            <option value="09">កញ្ញា</option>
                                            <option value="10">តុលា</option>
                                            <option value="11">វិចិ្ឆកា</option>
                                            <option value="12">ធ្នូ</option>
                                        </select>
                                    </div>
                                    <div class="span4">
                                        <label>ឆ្នាំ</label>
                                        <select name="year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
                                    </div>
                                </div>
                            </div>
                            <div class="span12" style="margin-left:0px;">
                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">សញ្ជាតិ <span class="required"
                                        title="This place you must be put data">*</span></label>

                                <div class="controls">

                                    <label class="checkbox-inline" style="margin-top:10px;">

                                        <input type="radio" id="default" checked name="client_nationality1" value="1">
                                        ខ្មែរ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" id="other" name="client_nationality1" value="2"> ផ្សេងៗ

                                    </label>

                                    <input type="text" id="show_other" name="client_nationality2" placeholder="Input other"
                                        data-required="1" value="{{ old('client_nationality') }}" class="span12 m-wrap" />

                                </div>

                                <p class="alert-danger">{{$errors->first('client_nationality')}}</p>

                                <script type="text/javascript">
                                    $(document).ready(function () {

                                        $('#show_other').hide();



                                        $('#default').click(function () {

                                            $('#show_other').hide();

                                        });

                                        $('#other').click(function () {

                                            $('#show_other').show();

                                        });

                                    });
                                </script>

                                <br />
                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ​ ( <b><span
                                            id="characters"><span></b> ) <span class="required" title="This place you must be put data">*</span></label>

                                <div class="controls">

                                    <input type="text" id="count-number" name="client_idcard_no" onkeypress="return forceNumber(event);"
                                        data-required="1" value="{{ old('client_idcard_no') }}" class="span12 m-wrap" />

                                </div>

                                <p class="alert-danger">{{$errors->first('client_idcard_no')}}</p>

                                <script type="text/javascript">
                                    $('#count-number').keyup(updateCount);

                                    $('#count-number').keydown(updateCount);



                                    function updateCount() {

                                        var cs = $(this).val().length;

                                        $('#characters').text(cs);

                                    }
                                    //can type English only
                                    function forceNumber(e) {
                                        var keyCode = e.keyCode ? e.keyCode : e.which;
                                        if (keyCode == 32)
                                            return true;
                                        if (48 <= keyCode && keyCode <= 57)
                                            return true;
                                        if (65 <= keyCode && keyCode <= 90)
                                            return true;
                                        if (97 <= keyCode && keyCode <= 122)
                                            return true;
                                        return false;
                                    }
                                </script>

                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប្រភេទប័ណ្ណ <span class="required"
                                        title="This place you must be put data">*</span></label>

                                <div class="controls">

                                    <label class="checkbox-inline" style="margin-top:10px;">

                                        <input type="radio" class="default" checked name="client_type_idcard1" value="1">
                                        អត្តសញ្ញាណប័ណ្ណ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" class="default" name="client_type_idcard1" value="2">
                                        លិខិនឆ្លង់ដែន

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" class="default" name="client_type_idcard1" value="3">
                                        សំបុត្របញ្ជាក់កំណើត

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" class="default" name="client_type_idcard1" value="4">
                                        សៀវភៅសា្នក់នៅ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" class="default" name="client_type_idcard1" value="5">
                                        សៀវភៅគ្រួសារ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" id="other1" name="client_type_idcard1" value="6"> ផ្សេងៗ

                                    </label>

                                    <input type="text" id="show_other1" name="client_type_idcard2" placeholder="Input other"
                                        data-required="1" value="{{ old('client_type_idcard') }}" class="span12 m-wrap" />

                                </div>

                                <p class="alert-danger">{{$errors->first('client_type_idcard')}}</p>

                                <script type="text/javascript">
                                    $(document).ready(function () {

                                        $('#show_other1').hide();



                                        $('.default').click(function () {

                                            $('#show_other1').hide();

                                        });

                                        $('#other1').click(function () {

                                            $('#show_other1').show();

                                        });

                                    });
                                </script>

                                <br />

                                <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ចេញដោយ</label>

                                <div class="controls">

                                    <label class="checkbox-inline" style="margin-top:10px;">

                                        <input type="radio" id="default2" checked name="client_aprovel_idcard_by1"
                                            value="1"> ក្រសួងមហាផ្ទៃ

                                        &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                        <input type="radio" id="other2" name="client_aprovel_idcard_by1" value="2">
                                        ផ្សេងៗ

                                    </label>

                                    <input type="text" id="show_other2" name="client_aprovel_idcard_by2" placeholder="Input other"
                                        data-required="1" value="{{ old('client_aprovel_idcard_by') }}" class="span12 m-wrap" />

                                </div>
                                <br />
                                <script type="text/javascript">
                                    $(document).ready(function () {

                                        $('#show_other2').hide();



                                        $('#default2').click(function () {

                                            $('#show_other2').hide();

                                        });

                                        $('#other2').click(function () {

                                            $('#show_other2').show();

                                        });

                                    });
                                </script>

                            </div>

                        </div>

                        <div class="span12" style="margin-left:0px;">
								<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ថ្ងៃចេញអត្តសញ្ញាណប័ណ្ណ <!-- <span class="required" title="This place you must be put data">*</span> --></label>
								<div class="controls">
									<div class="span4">
										<label>ថ្ងៃ</label>
										<select name="identify_day" id="select01" class="chzn-select get_date_dob span12 m-wrap"></select>
											<p class="alert-danger">{{$errors->first('day')}}</p>
									</div>
									<div class="span4">
										<label>ខែ</label>
										<select name="identify_month" id="identify_month" class="chzn-select span12 m-wrap">
										<option value=""></option>
										<option value="01">មករា</option>
										<option value="02">កុម្ភៈ</option>
										<option value="03">មីនា</option>
										<option value="04">មេសា</option>
										<option value="05">ឧសភា</option>
										<option value="06">មិថុនា</option>
										<option value="07">កក្កដា</option>
										<option value="08">សីហា</option>
										<option value="09">កញ្ញា</option>
										<option value="10">តុលា</option>
										<option value="11">វិចិ្ឆកា</option>
										<option value="12">ធ្នូ</option>
										</select>
									</div>
									<div class="span4">
										<label>ឆ្នាំ</label>
										<select name="identify_year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
									</div>
								</div>
							</div>

                            <div class="span12" style="margin-left:0;">

                                <div class="span12">

                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន"> ចេញដោយឈ្មោះ</label>

                                    <div class="controls">

                                        <input type="text" autocapitalize="word" id="approve_by_name" name="approve_by_name" data-required="1" value="{{ old('client_identify_by') }}" class="span12 m-wrap"/>

                                    </div>

                                </div>
                            </div>

                        <div class="span12" style="margin-left:0px;">

                            <div class="span4">
                                <label class="control-label">អាសយដ្ឋានផ្ទះលេខ</label>

                                <div class="controls">

                                    <input type="text" name="client_house_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}"
                                        class="span12 m-wrap" />

                                </div>
                            </div>
                            <div class="span4">
                                <label class="control-label">ក្រុមទី</label>

                                <div class="controls">

                                    <input type="text" name="client_group_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}"
                                        class="span12 m-wrap" />

                                </div>
                            </div>
                            <div class="span4">
                                <label class="control-label">ផ្លូវលេខ</label>

                                <div class="controls">

                                    <input type="text" name="client_st_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}"
                                        class="span12 m-wrap" />

                                </div>
                            </div>
                        </div>
                        <div class="span12" style="margin-left:0px;">

                            @include('credit_sale.client_management.p_location')
                        </div>
                        <br />

                        <label class="control-label">លេខទូរសព្ទទំនាក់ទំនង <span class="required" title="This place you must be put data">*</span></label>

                        <div class="controls">

                            <input type="text" name="client_phone" data-required="1" value="{{ old('client_phone') }}" class="span12 m-wrap telnumber"
                                maxlength="12" />

                        </div>

                        <p class="alert-danger">{{$errors->first('client_phone')}}</p>
                        <script type="text/javascript">
                            $('.telnumber').keyup(function () {
                                foo = $(this).val().split("-").join(""); // remove hyphens

                                foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");

                                $(this).val(foo);

                            });
                        </script>



                        <label class="control-label">មុខរបរ</label>

                        <div class="controls">

                            <input type="text" name="client_job" data-required="1" value="{{ old('client_job') }}"
                                class="span12 m-wrap" />

                        </div>

                        <label class="control-label">ទីកន្លែងប្រកបមុខរបរ</label>

                        <div class="controls">

                            <input type="text" name="client_address_job" data-required="1" value="{{ old('client_address_job') }}"
                                class="span12 m-wrap" />

                        </div>


                        <br />
                        <label>កូអរដោនេ</label>
                        X: <input type="text" name="client_lutidued" class="span5 m-wrap" style="width: 14%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
                        Y: <input type="text" name="client_longitidute" class="span5 m-wrap" style="width: 14%;">

                        <br />
                        <label class="control-label">ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ <span class="required"
                                title="This place you must be select data">*</span></label>

                        <div class="controls">

                            <input type="file" name="client_upload_image" class="input-file uniform_on" id="fileInput"
                                multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត" />

                        </div>

                        <p class="alert-danger">{{$errors->first('client_upload_image')}}</p>

                        <br />

                        <a href="#" id="add" class="btn btn-danger pull-right">+ បន្ថែមទៀត</a>

                        <label class="control-label">ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ <span
                                class="required" title="This place you must be select data">*</span></label>

                        <div class="controls" id="addimage">

                            <input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput"
                                multiple title="រូបភាពត្រូវមានទំហំសមល្មម" />

                        </div>

                        <p class="alert-danger">{{$errors->first('image_uploade_id_familly')}}</p>

                    </div>

            </div>



            <div class="span8" style="margin: 0 auto !important;float: none;">

                <br />

                <label class="control-label">កំណត់ចំណាំ</label>

                <div class="controls">
                    <textarea name="client_note" data-required="1" rows="5" class="span12 m-wrap">{{ old('client_job_profit') }}</textarea>

                </div>

                <br />

                <!-- <center>
                    <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">បញ្ជូល</button>

                    <a href="{{url('accounts')}}" class="btn btn-danger">ត្រលប់</a>

                </center> -->
                
                <style>
                    .b-icon {
                        border: none;
                        border-radius: 0;
                    }
                    .b-icon-hr {
                        border-top: 1px solid #ee5f5b;
                        margin: 0 0 20px;
                    }
                    i.b-icon:after {
                            content: '\002B';
                            color: white;
                            font-weight: bold;
                            float: right;
                            margin-left: 5px;
                    }
                    i.b-icon.active:after {
                        content: "\2212";
                    }
                    .hide {
                        display: none;
                    }
                </style>
                <span><i id="b_icon" class="b-icon btn btn-danger">បន្ថែមអ្នកចូលរួម</i><hr class="b-icon-hr"></span>
                <input type="hidden" id="third_id" name="third_id" value="0">
            
            </div>
            
            <!-- <br /><br /> -->
            
            <!-- Third Client Form -->

            <div class="span8 hide toggle-form" style="margin: 0 auto !important;float: none;">
                <div class="control-group">
                    <div class="span12" style="margin-left:0;">
                        <div class="span12">
                            <label class="control-label" title="ប្រភេទកម្ចី">ប្រភេទអ្នកចូលរួម <span class="required"
                                    title="This place you must be put data">*</span></label>
                            <select name="client_type" class="span12 m-wrap">
                                <option value="1">អ្នកចូលរួមធានា</option>
                                <option value="2">អ្នកចូលរួមខ្ចី</option>
                            </select>
                        </div>
                    </div>

                    <div class="span12" style="margin-left:0;">
                        
                        <div class="span6">

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាម <span class="required"
                                    title="This place you must be put data">*</span></label>

                            <div class="controls">

                                <input type="text" id="third_client_first_name_kh" class="span12 m-wrap" name="third_client_first_name_kh"
                                    data-required="1" value="{{ old('third_client_first_name_kh') }}" />

                            </div>

                            <p class="alert-danger">{{$errors->first('third_client_first_name_kh')}}</p>

                        </div>

                        <div class="span6">

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាម <span class="required"
                                    title="This place you must be put data">*</span></label>

                            <div class="controls">

                                <input type="text" id="third_client_second_name_kh" class="span12 m-wrap" name="third_client_second_name_kh"
                                    data-required="1" value="{{ old('third_client_second_name_kh') }}" />

                            </div>

                            <p class="alert-danger">{{$errors->first('third_client_second_name_kh')}}</p>

                        </div>

                    </div>



                    <div class="span12" style="margin-left:0;">

                        <div class="span6">

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាមជាអក្សរឡាតាំង (First
                                Name)</label>

                            <div class="controls">

                                <input type="text" autocapitalize="word" id="third_client_first_name_en" name="third_client_first_name_en"
                                    data-required="1" value="{{ old('third_client_first_name_en') }}" class="span12 m-wrap" />

                            </div>

                        </div>

                        <div class="span6">

                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាមជាអក្សរឡាតាំង​ (Last Name)</label>

                            <div class="controls">

                                <input type="text" autocapitalize="word" id="third_client_second_name_en" class="span12 m-wrap"
                                    name="third_client_second_name_en" data-required="1" value="{{ old('third_client_second_name_en') }}" />

                            </div>

                        </div>
                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ភេទ <span class="required" title="This place you must be put data">*</span></label>

                        <div class="controls">

                            <select id="third_selectError" ​ class="span12 m-wrap" name="third_client_gender">

                                <option value="M">ប្រុស</option>

                                <option value="F">ស្រី</option>

                            </select>

                        </div>

                        <p class="alert-danger">{{$errors->first('third_client_gender')}}</p>

                    </div>
                    <div class="span12" style="margin-left:0px;">
                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ថ្ងៃខែឆ្នាំកំណើត <span class="required"
                                title="This place you must be put data">*</span></label>
                        <div class="controls">
                            <div class="span4">
                                <label>ថ្ងៃ</label>
                                <select name="third_day" id="third_select01" class="chzn-select get_date_dob span12 m-wrap"></select>
                                <p class="alert-danger">{{$errors->first('third_day')}}</p>
                            </div>
                            <div class="span4">
                                <label>ខែ</label>
                                <select name="third_month" id="select01" class="chzn-select span12 m-wrap" placeholder="ជ្រើសរើស">
                                    <option value=""></option>
                                    <option value="01">មករា</option>
                                    <option value="02">កុម្ភៈ</option>
                                    <option value="03">មីនា</option>
                                    <option value="04">មេសា</option>
                                    <option value="05">ឧសភា</option>
                                    <option value="06">មិថុនា</option>
                                    <option value="07">កក្កដា</option>
                                    <option value="08">សីហា</option>
                                    <option value="09">កញ្ញា</option>
                                    <option value="10">តុលា</option>
                                    <option value="11">វិចិ្ឆកា</option>
                                    <option value="12">ធ្នូ</option>
                                </select>
                            </div>
                            <div class="span4">
                                <label>ឆ្នាំ</label>
                                <select name="third_year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
                            </div>
                        </div>
                    </div>
                    <div class="span12" style="margin-left:0px;">
                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">សញ្ជាតិ <span class="required"
                                title="This place you must be put data">*</span></label>

                        <div class="controls">

                            <label class="checkbox-inline" style="margin-top:10px;">

                                <input type="radio" id="third_default" checked name="third_client_nationality1" value="1">
                                ខ្មែរ

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" id="third_other" name="third_client_nationality1" value="2"> ផ្សេងៗ

                            </label>

                            <input type="text" id="third_show_other" name="third_client_nationality2" placeholder="Input other"
                                data-required="1" value="{{ old('third_client_nationality') }}" class="span12 m-wrap" />

                        </div>

                        <p class="alert-danger">{{$errors->first('third_client_nationality')}}</p>

                        <script type="text/javascript">
                            $(document).ready(function () {

                                $('#third_show_other').hide();



                                $('#third_default').click(function () {

                                    $('#third_show_other').hide();

                                });

                                $('#third_other').click(function () {

                                    $('#third_show_other').show();

                                });

                            });
                        </script>

                        <br />
                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ​ ( <b><span
                                    id="third_characters"><span></b> ) <span class="required" title="This place you must be put data">*</span></label>

                        <div class="controls">

                            <input type="text" id="third-count-number" name="third_identify_num" onkeypress="return third_forceNumber(event);"
                                data-required="1" value="{{ old('third_identify_num') }}" class="span12 m-wrap" />

                        </div>

                        <p class="alert-danger">{{$errors->first('third_identify_num')}}</p>

                        <script type="text/javascript">
                            $('#third-count-number').keyup(third_updateCount);

                            $('#third-count-number').keydown(third_updateCount);



                            function third_updateCount() {

                                var cs = $(this).val().length;

                                $('#third_characters').text(cs);

                            }
                            //can type English only
                            function third_forceNumber(e) {
                                var keyCode = e.keyCode ? e.keyCode : e.which;
                                if (keyCode == 32)
                                    return true;
                                if (48 <= keyCode && keyCode <= 57)
                                    return true;
                                if (65 <= keyCode && keyCode <= 90)
                                    return true;
                                if (97 <= keyCode && keyCode <= 122)
                                    return true;
                                return false;
                            }
                        </script>

                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប្រភេទប័ណ្ណ <span class="required"
                                title="This place you must be put data">*</span></label>

                        <div class="controls">

                            <label class="checkbox-inline" style="margin-top:10px;">

                                <input type="radio" class="third_default" checked name="third_client_type_idcard1" value="1">
                                អត្តសញ្ញាណប័ណ្ណ

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" class="third_default" name="third_client_type_idcard1" value="2">
                                លិខិនឆ្លង់ដែន

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" class="third_default" name="third_client_type_idcard1" value="3">
                                សំបុត្របញ្ជាក់កំណើត

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" class="third_default" name="third_client_type_idcard1" value="4">
                                សៀវភៅសា្នក់នៅ

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" class="third_default" name="third_client_type_idcard1" value="5">
                                សៀវភៅគ្រួសារ

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" id="third_other1" name="third_client_type_idcard1" value="6"> ផ្សេងៗ

                            </label>

                            <input type="text" id="third_show_other1" name="third_client_type_idcard2" placeholder="Input other"
                                data-required="1" value="{{ old('third_client_type_idcard') }}" class="span12 m-wrap" />

                        </div>

                        <p class="alert-danger">{{$errors->first('third_client_type_idcard')}}</p>

                        <script type="text/javascript">
                            $(document).ready(function () {

                                $('#third_show_other1').hide();



                                $('.third_default').click(function () {

                                    $('#third_show_other1').hide();

                                });

                                $('#third_other1').click(function () {

                                    $('#third_show_other1').show();

                                });

                            });
                        </script>

                        <br />

                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ចេញដោយ</label>

                        <div class="controls">

                            <label class="checkbox-inline" style="margin-top:10px;">

                                <input type="radio" id="third_default2" checked name="third_client_aprovel_idcard_by1"
                                    value="1"> ក្រសួងមហាផ្ទៃ

                                &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

                                <input type="radio" id="third_other2" name="third_client_aprovel_idcard_by1" value="2">
                                ផ្សេងៗ

                            </label>

                            <input type="text" id="third_show_other2" name="third_client_aprovel_idcard_by2" placeholder="Input other"
                                data-required="1" value="{{ old('third_client_aprovel_idcard_by') }}" class="span12 m-wrap" />

                        </div>
                        <br />
                        <script type="text/javascript">
                            $(document).ready(function () {

                                $('#third_show_other2').hide();



                                $('#third_default2').click(function () {

                                    $('#third_show_other2').hide();

                                });

                                $('#third_other2').click(function () {

                                    $('#third_show_other2').show();

                                });

                            });
                        </script>

                    </div>

                </div>

                <div class="span12" style="margin-left:0px;">

                    <div class="span4">
                        <label class="control-label">អាសយដ្ឋានផ្ទះលេខ</label>

                        <div class="controls">

                            <input type="text" name="third_client_house_num" data-required="1" value="{{ old('third_client_num_st_ho_gr') }}"
                                class="span12 m-wrap" />

                        </div>
                    </div>
                    <div class="span4">
                        <label class="control-label">ក្រុមទី</label>

                        <div class="controls">

                            <input type="text" name="third_client_group_num" data-required="1" value="{{ old('third_client_num_st_ho_gr') }}"
                                class="span12 m-wrap" />

                        </div>
                    </div>
                    <div class="span4">
                        <label class="control-label">ផ្លូវលេខ</label>

                        <div class="controls">

                            <input type="text" name="third_client_st_num" data-required="1" value="{{ old('third_client_num_st_ho_gr') }}"
                                class="span12 m-wrap" />

                        </div>
                    </div>
                </div>
                
                <br />

                <label class="control-label">លេខទូរសព្ទទំនាក់ទំនង <span class="required" title="This place you must be put data">*</span></label>

                <div class="controls">

                    <input type="text" name="third_phone" data-required="1" value="{{ old('third_phone') }}" class="span12 m-wrap third_telnumber"
                        maxlength="12" />

                </div>

                <p class="alert-danger">{{$errors->first('phone')}}</p>
                <script type="text/javascript">
                    $('.third_telnumber').keyup(function () {
                        foo = $(this).val().split("-").join(""); // remove hyphens

                        foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");

                        $(this).val(foo);

                    });
                </script>



                <label class="control-label">មុខរបរ</label>

                <div class="controls">

                    <input type="text" name="third_client_job" data-required="1" value="{{ old('third_client_job') }}"
                        class="span12 m-wrap" />

                </div>

                <label class="control-label">ទីកន្លែងប្រកបមុខរបរ</label>

                <div class="controls">

                    <input type="text" name="third_client_address_job" data-required="1" value="{{ old('third_client_address_job') }}"
                        class="span12 m-wrap" />

                </div>
            </div>
            <br /><br />
                <center>
                    <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">បញ្ជូល</button>
                    <input type="hidden" id="client_type">
                    <a href="{{url('accounts')}}" class="btn btn-danger">ត្រលប់</a>

                </center>
                <br /><br />
            <!-- End Form Third Sub Client -->
            
            </form>

        </div>

    </div>

    <!-- /block -->
</div>

</div>

</div>
<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
    $(document).ready(function () {
        $(".b-icon").click(function(event){ 
            event.preventDefault(); 
            $(".toggle-form").toggle(1,function(){ 
                if($(this).is(':hidden')) { 
                    $(".b-icon").removeClass("active"); 
                    $("#third_id").val("0"); 
                } else { 
                    $(".b-icon").addClass("active"); 
                    $("#third_id").val("1"); 
                } 
            }); 
        });

        $('#add').click(function (event) {
            event.preventDefault();
            var img = '<div class="controls" id="addimage">' +

                '<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

                '</div>';
            $('#addimage').append(img);
        });
        var age_get = 18;
        var d = new Date();
        var year_d = d.getFullYear() - age_get;
        var i;
        var year_dob = "";
        for (i = 1940; i <= year_d; i++) {
            year_dob += "<option value=''></option>";
            year_dob += "<option value='" + i + "'>" + i + "</option>";
        }

        $(".get_year_dob").html(year_dob);

        var lastday = function (y, m) {
            return new Date(y, m + 1, 0).getDate();
        }
        var ldfm = lastday(d.getFullYear(), d.getMonth());
        var date_of = "";
        for (var m = 1; m <= ldfm; m++) {
            date_of += "<option value=''></option>";
            date_of += "<option value='" + m + "'>" + m + "</option>";
        }
        $(".get_date_dob").html(date_of);

        // insert  add client  to database   
        @if(Request::is('accounts/sub_client/create_sub_client'))
        var url = "{{ route('accounts/sub_client/create_sub_client') }}";
        @else
        var url = "{{ url('accounts/sub_client/edit') }}";
        @endif
        $("#btn-save").click(function (e) {
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            })
            e.preventDefault();
            var url_create = "{{ url('accounts/sub_client/create_sub_client') }}";
            var url_r = "?client_id={{$client_id}}&sub_client_id=0&item=create";
            var url_all = url_create + url_r;
            var url_index = "{{url('accounts/sub_client/show_sub_client')}}";
            var state = $('#btn-save').val();
            var client_id = $('#client_id').val();
            // high importance!, here you need change "yourformname" with the name of your form
            var form = document.forms.namedItem("form_insert_sub_client");
            var formData = new FormData(form);
            $.ajax({
                type: 'POST',
                url: url_all,
                dataType: 'json',
                contentType: false,
                data: formData,
                processData: false,
                success: function (data, status, xhr) {
                    // console.log(data);
                    if(data.error){
                        var err = "";
                        $.each(data.error, function(key, val){
                            err += '<li>'+val+'</li>';
                        })
	                	$(".errors").append('<div class = "alert alert-danger"><ul>'+err+'</ul></div></br>');
                        window.scrollTo(0, 0);
                        $(".alert").delay(4000).slideUp(1000, function() {
                            $(this).alert('close');
                        });
	                }else{
	                	@if(Request::is('accounts/sub_client/create_sub_client'))
                        var cli_type = "{{$cli_type->client_type_id}}";
                        // location.href = "{{url('credit_sales')}}"+"?from="+cli_type+"&redirect=credit_sales&sales=0";
                            window.scrollTo(0, 0);
                            $('.msg_show').html(data.msg_show);
                            $('#form_insert_sub_client').trigger("reset");
                            $('#client_id').val("0");
                            $('#btn-save').val("add");
                            url = "{{ route('accounts/sub_client/create_sub_client') }}";
                        @else
                            location.href = url_all;
                        @endif
	                }
                    
                    
                },
                error: function (data,xhr) {
                    console.log(data.responseText);
                    //     $(".cen_title").after('<div class = "alert alert-danger"><ul><li>'+data.responseText+'</li></ul></div>');                   
                   
                }
            });

        });

    });
    
</script>

@endsection