@extends('credit_sale.layout.master')
@section('contend')	
<div class="container-fluid">

    <div class="row-fluid">

            <!-- block -->

        <div class="block">

            <div class="navbar navbar-inner block-header">
            <?php
                if($client_id == 0){
                    $t = "អ្នកធានាទាំងអស់";
                    $url_o = url('accounts/all_sub_client');
                }else{
                    $t = "គណនី";
                    $url_o = url('accounts');
                }
            ?>
                <div class="muted pull-left"><a href="{!! $url_o !!}"> {{$t}}</a> <span class="divider">|</span> {{$title}} </div>

            </div>
            
            <div class="block-content collapse in">
            @if($client_id != 0)
                @include('credit_sale.client_management.menu_client')
            @endif
            <div class="span12"  style="margin: 0 auto !important;float: none;">	
            
            </div>
            
            @include('errors.error')
                        <div class="span8"  style="margin: 0 auto !important;float: none;">
                            <center>

                                <h3 class="cen_title"> ការបង្ហាញព័ត៌មានអ្នកធានា </h3>

                            </center>

                        </div>

    <div class="span8" style="margin: 0 auto !important;float: none;">
                                                        
        <h4>ព័ត៌មានរបស់អ្នកធានា <b class="kh_username"></b> </h4>
            <table class="table table-bordered table-striped">
            <thead style="background: rgb(251, 205, 205);">
                <tr>
                <th></th>
                <th>បរិយាយ</th>
                </tr>
            </thead>
            <tbody>
                <tr>
                <td><b>ឈ្មោះជាភាសាខ្មែរ</b></td>
                <td><p class="kh_username"></p></td>
                </tr>
                <tr>
                <td><b>ឈ្មោះជាអក្សរឡាតាំង</b></td>
                <td><p class="en_username"></p></td>
                </tr>
                <tr>
                <td><b>ភេទ</b></td>
                <td>
                    <p class="gender"></p>
                </td>
                </tr>
                <tr>
                <td><b>ថ្ងែខែឆ្នាំកំណើត</b></td>
                <td>
                    <p class="dob"></p>
                </td>
                </tr>
                <tr>
                <td><b>សញ្ជាតិ</b></td>
                <td> <p class="nationality"></p></td>
                </tr>
                
                <tr>
                <td> <b>កាន់ប័ណ្ណសម្គាល់អត្តសញ្ញាណ</b></td>
                <td><p class="identify_num"></p>
                    <b>ប្រភេទប័ណ្ណ : </b>	
                    <p class="identify_type"> </p>
                        <b>ចេញដោយ : </b>	
                    <p class="identify_by"> </p>
                </td>
                </tr>
                <tr>
                <td> <b>អាសយដ្ឋាន</b></td>
                <td> <p class="address"></p> </td>
                </tr>
                <tr>
                <td><b>លេខទូរសព្ទទំនាក់ទំនង</b></td>
                <td><p class="phone"></p> </td>
                </tr>
                <tr>
                <td><b>មុខរបរ</b></td>
                <td>
                    <p class="job"></p>
                
                    <!-- <b>ទីកន្លែងមុខរបរ</b>  
                        <p class="place_job"></p> -->
                </td>
                </tr>
                <tr>
                <td><b>ព័ត៌មានលំអិត</b></td>
                <td><p class="client_note"></p></td>
                </tr>
                <tr>
                <td><b>ស្ថានភាព</b></td>
                <td><p class="status"></p> </td>
                </tr>
                <tr>
                    <td><b>ឯកសារដែលបានថតចំលង</b></td>
                    <td>
                        <p class="client_upload_image"></p><br/>
                        <p class="image_uploade_id_familly"></p>
                    </td>
                </tr>
                <input type="hidden" id="cli_id" name="client_id" value="{{$client_id}}">
            </tbody>
            </table>
        </div>
    </div>

    </div>

        <!-- /block -->
    </div>

    </div>

    </div>
    <meta name="_token" content="{{ csrf_token() }}" />

    <script type="text/javascript">

    $(document).ready(function(){
    $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        var url_e = "{{ url('accounts/sub_client') }}";
        var c_id ="{{$subclient_id}}";
        
        var url_edit = url_e+"/"+c_id+"/json_sub_client";
        // alert(url_edit);
        var client;
        var forData = {};
            var out = "";
            $.ajax({
                    type: "GET",
                    url: url_edit, 
                    dataType: "json",
                    data: forData,
                    success: function(result ,xhr){
                        console.log(result);
                        $.each(result ,function(i, field){
                            
                            if(i == "client"){
                                if(field.client_note !== null){
                                    $(".client_note").html(field.client_note);
                                }
                                
                                if(field.client_name_kh !== null){
                                    $(".kh_username").html(field.client_name_kh);
                                }
                                if(field.client_name_en !== null){
                                    $(".en_username").html(field.client_name_en);
                                }
                                
                                if(field.client_job !== null){
                                    $(".job").html(field.client_job);
                                }
                            var id_num , nation , id_ty , id_by ,identify;	
                                if(field.client_idcard_no !== null){
                                        
                                        id_num = field.client_idcard_no;
                                        $('.identify_num').html(id_num);
                                    //Check Nationality
                                        if(field.client_nationality ==1){
                                            nation = "ខ្មែរ";
                                        }else{
                                                nation = field.client_nationality;
                                        }
                                        $('.nationality').html(nation);
                                    //ប្រភេទប័ណ្ណ 	
                                        if(field.client_type_idcard ==="1"){
                                            id_ty = "អត្តសញ្ញាណប័ណ្ណ";
                                        }else if(field.client_type_idcard ==="2"){
                                            id_ty = "លិខិនឆ្លង់ដែន";
                                        }else if(field.client_type_idcard ==="3"){
                                            id_ty = "សំបុត្របញ្ជាក់កំណើត";
                                        }else if(field.client_type_idcard ==="4"){
                                            id_ty = "សៀវភៅសា្នក់នៅ";
                                        }else if(field.client_type_idcard ==="5"){
                                            id_ty = "សៀវភៅគ្រួសារ";
                                        }else{
                                            id_ty = field.client_type_idcard;
                                        }
                                        $('.identify_type').html(id_ty);

                                    //	ប្រភេទប័ណ្ណចេញដោយ	
                                        if(field.client_aprovel_idcard_by ==1){
                                            id_by = "ក្រសួងមហាផ្ទៃ";
                                        }else{
                                            id_by = field.client_aprovel_idcard_by;
                                        }
                                        $('.identify_by').html(id_by);
                                        
                                }

                        // Phone		
                                if(field.client_phone !== null){
                                    $(".phone").html(field.client_phone);
                                }
                        // Address
                                var address , hn ,gn ,st, vl , cm , dis,pro ,gd ,sts , dob;
                                if(field.client_house_num !== null){
                                    hn = field.client_house_num +",";
                                }else{
                                    hn = "";
                                }
                                if(field.client_group_num !== null){
                                    gn = field.client_group_num +",";
                                }else{
                                    gn = "   ";
                                }
                                if(field.client_st_num !== null){
                                    st = field.client_st_num +",";
                                }else{
                                    st = "  ";
                                }
                                if(field.client_village !== null){
                                    vl = field.client_village +",";
                                }else{
                                    vl = "";
                                }
                                if(field.client_commune !== null){
                                    cm = field.client_commune +",";
                                }else{
                                    cm = "";
                                }
                                if(field.client_district !== null){
                                    dis = field.client_district +",";
                                }else{
                                    dis = "";
                                }
                                if(field.client_province !== null){
                                    pro = field.client_province +",";
                                }else{
                                    pro = "";
                                }
                                
                                address = hn +"  "+  gn +" "+  st +"  "+  vl  +"  "+  cm +"  "+  dis +"  "+  pro ; 
                                $('.address').html(address);
                        // Gender		
                                if(field.client_gender !== "F"){
                                    gd = "ប្រុស";
                                }else{
                                    gd = "ស្រី";
                                }
                                $('.gender').html(gd);	
                        // DOB			
                            if(field.client_dob !== null){	
                                dob = new Date(field.client_dob);
                                var dd = dob.getDate();
                                var mm = dob.getMonth()+1; //January is 0!

                                var yyyy = dob.getFullYear();
                                if(dd<10){
                                    dd='0'+dd;
                                } 
                                if(mm<10){
                                    mm='0'+mm;
                                } 
                                dob = dd+'-'+mm+'-'+yyyy;
                            }else{
                                dob = "not yet input";
                            }
                                $('.dob').html(dob);	

                            // Status   
                                if(sts !== 1 ){
                                    sts = "<span class='label label-success'>Active</span>";
                                }else{
                                    sts = "<span class='label label-important'>Inactive</span>";
                                }


                            $('.status').html(sts);		
                            
                            //ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ
                                var url_image = "{{url('/')}}";			
                                        if(field.upload_relate_document !== null){
                                            $(".client_upload_image").html("<br/><img class='client_upload_image_respon' > ");
                                            $(".client_upload_image_respon").attr("src",url_image+"/Account/images/"+field.client_upload_image);
                                        }
                                }
                            
                        });
                        //ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ
                        var l =	(result.relate_file).length;
                        if(l > 0){
                            var im_up_id = "";
                                $.each(result.relate_file, function(i, field){
                                            var url_image = "{{url('/')}}";	
                                            if(field.image_value !== null){
                                                
                                                    im_up_id += "<img class='client_upload_image_respon' id='image_uploade_id_familly"+i+"' src='"+url_image+"/Account/images/"+field.image_value+"'>    "; 
                                                
                                            }

                                });
                                $(".image_uploade_id_familly").html(im_up_id);
                        }
                        
                    },
                    error: function (result ) {
                        console.log(result.stutus);
                    }

            });
						      
				
	});
</script>

@endsection

