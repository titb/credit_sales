<ul class="nav nav-tabs">
    <li class="<?php if(Request::segment(2) == $client_id){echo "active";} ?>"><a href="{!! url('accounts/'.$client_id.'/show') !!}">ការបង្ហាញព័ត៌មានអតិថិជន</a></li>
    <li class="<?php if(Request::segment(2) == 'sub_client'){echo "active";} ?>">
        <a href="{!! url('accounts/sub_client/show_sub_client?client_id='.$client_id.'&sub_client_id=0&item=create') !!}">អ្នកធានា</a></li>
    <li class="<?php if(Request::segment(2) == 'request_form'){echo "active";} ?>">
        <a href="{{url('accounts/request_form/'.$client_id)}}">ទម្រង់ស្នើសុំ</a>
    </li>
    <li class="<?php if(Request::segment(2) == 'aprove_credit_sales'){echo "active";} ?>">
        <a href="{{url('accounts/aprove_credit_sales/'.$client_id.'/list_approve')}}">កិច្ចសន្យាទិញបង់រំលស់</a>
    </li>
    <li class="<?php if(Request::segment(2) == 'schedule' || Request::segment(2) == 'show_client_schedule'){echo "active";} ?>">
        <a href="{{url('accounts/schedule/'.$client_id)}}">កាលវិភាគសងប្រាក់</a>
    </li>
    <li class="<?php if(Request::segment(2) == 'history_payment'){echo "active";} ?>">
        <a href="{{url('accounts/history_payment/sale_credit/'.$client_id)}}">ប្រវត្តិនៃការទិញបង់រំលស់</a>
    </li>
    <li class="<?php if(Request::segment(3) == 'comment_finish'){echo "active";} ?>">
        <a href="{{url('accounts/client/comment_finish/'.$client_id)}}">ការបញ្ចប់ឥណទាន</a>
    </li>
</ul>	