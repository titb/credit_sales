@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
    <div class="row-fluid">
            <!-- validation -->
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <center>
                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
                    </center>
                </div>

                <div class="block-content collapse in">
                    <div class="span12" style="margin: 0 auto !important;float: none;">								
                        @include('credit_sale.client_management.menu_client')					
                    </div>

                    @include('errors.error')
                    <table class="table table-bordered">

                        <thead style="background: rgb(251, 205, 205);">

                            <tr class="header">
                                <th>ល.រ</th>
                                <th>សំណើ​រ ល.រ</th>
                                <!-- <th>ឈ្មេាះអតិថិជន</th> -->
                                <th>ចំនួនទំនិញ</th>
                                <th>តម្លៃសរុប</th>
                                <th>ការបរិច្ឆេតបង់ប្រាក់ចុងក្រោយ</th>
                                <th>កម្រិតនៃការបញ្ចប់</th>
                                <th>មតិបញ្ចប់</th>
                                <!-- <th>សកម្មភាព</th> -->
                            </tr>
                        </thead>
                        <tbody class="item_list">

                        </tbody>
                            <tr>
                                <td colspan="6">
                                    <b class="pull-right">សរុប:</b>
                                </td>
                                <td>
                                    <b id="total_all"></b>
                                </td>
                            </tr>
                        </table>
                        <!-- Pagination -->
                        <div class="pagination text-right"></div>
                    </div>
                </div>
                <!-- /block -->
            </div>
            <!-- /validation -->
        </div>
    </div>
</div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<script>
    var url = "{{ url('accounts/client/comment_finish/json/'.$client_id) }}";
    $.get(url, function(data, status, xhr){
        console.log(data);
        var txt;
        $.each(data, function(k, val){
            var qtys = 0;
            var status;
            $.each(val.cs_approvalcredit.approval_item, function(kq,q){
                qtys += q.qty;
            });
            if(val.comment_finish !== null){
                if(val.comment_finish.status_of_finish == 1){
                    status = "ល្អ";
                }else if(val.comment_finish.status_of_finish == 2){
                    status = 'ល្អបង្គួរ';
                }else if(val.comment_finish.status_of_finish == 3){
                    status = "មធ្យម";
                }else if(val.comment_finish.status_of_finish == 4){
                    status = "មិនល្អ";
                }else{
                    status = "មិនមាន";
                }
                var dlp = val.comment_finish.date_last_payment;
                var com_fin = val.comment_finish.command_finish;
                txt += '<tr>';
                txt += '<td>'+(k+1)+'</td>';
                txt += '<td>'+ val.approval_id +'</td>';
                // txt += '<td>'+ val.cs_client.kh_username +'</td>';
                txt += '<td>'+ qtys +'</td>';
                txt += '<td>'+ val.money_owne_total_pay +'</td>';
                txt += '<td>'+ dlp +'</td>';
                txt += '<td>'+ status +'</td>';
                txt += '<td>'+ com_fin +'</td>';
                txt += '</tr>';
            }else{
                $(".item_list").html("<tr><td>មិនមានទិន្នន័យ</td></tr>"); 
            }
        });
        $(".item_list").append(txt);   
        $("#total_all").html(data.length); 
    }).fail(function(error){
        console.log(error.responseText);
    });
    
    
</script>          
@stop()
