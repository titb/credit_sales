@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('aprove_credit_sales') !!}"> ការអនុម័តឥណទាន</a> <span class="divider">|</span> ការបង្ហាញការអនុម័តឥណទាន </div>		
                                    <button id="back_to_prev" class="btn btn-danger pull-right back_to_prev" style="font-family: 'Hanuman' !important;" onclick="back_to_prev()"><i class=" icon-arrow-left icon-white"></i> back </button>
                            </div>

                            <div class="block-content collapse in">
                            @include('credit_sale.client_management.menu_client')
                            <button class="btn btn-success pull-left" id="print_hide" style=" margin-right: 10px; font-family: 'Hanuman' !important;" onclick="with_print()"><i class="icon-print icon-white"></i> ព្រីនកិច្ចសន្យាទិញបង់រំលស់ </button>   
                            <button id="generate_schedule" class="btn btn-info pull-left generate_schedule" style="margin-right: 10px;font-family: 'Hanuman' !important;"  name="generate_schedule"><i class="icon-th-list icon-white"></i> ការបង្កើតកាលវិភាគសងប្រាក់អតិថិជន </button>
                            <a href="{{url('aprove_credit_sales/'.$data_id.'/edit')}}" class="btn btn-primary pull-right"> កែប្រែ</a>
                            @include('errors.error')	       
                             	
			                        <h3 class="cen_title text-center khmer_Moul ">{{$title}}</h3>
			                      
                            	<legend></legend>
                                
                                 
                                <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>ឈ្មោះទំនិញ</th>
                                                                <th>លេខ​កូដ​</th>
                                                                <th>ម៉ាក</th>
                                                                <th>ចំនួន</th>
                                                                <th>តម្លៃ​ឯកតា</th>
                                                                <th>តម្លៃសរុប</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="item_list">
                                                           
                                                        </tbody>
                                                                
                                                        </table>  
                                                        <br/>
                                                        
                                                <legend></legend> 
                                                <table class="table table-bordered">
                                                    <tbody>
                                                                <tr>
                                                                    <td style="text-align: right;">  ការអនុម័តឥណទាន</td>
                                                                    <td> <b class="is_agree"></b> </b></td>
                                                                    <td style="text-align: right;"> ឥណទានបញ្ចប់</td>
                                                                    <td> <b class="is_finish"></b></td>
                                                                   
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;">  មតិរបស់គណៈកម្មការឥណទាន</td>
                                                                    <td colspan="3"> <b class="comment_manager"></b>  </td>
                                                                   
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;"> តម្លៃសរុប </td>
                                                                    <td> <b class="prices_total_num_text"></b> </td>
                                                                    <td style="text-align: right;"> តម្លៃសរុបជាអក្សរ </td>
                                                                    <td> <b class="prices_total_text_text"></b> </td>
                                                                    
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;"> ចំនួនប្រាក់កក់សរុបជាលេខ </td>
                                                                    <td> <b class="money_owne_text"></b> </td>
                                                                    <td style="text-align: right;"> ចំនួនប្រាក់កក់សរុបជាអក្សរ </td>
                                                                    <td> <b class="money_owne_text_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;"> ចំនួនប្រាក់ដែលនៅជំពាក់សរុបជាលេខ </td>
                                                                    <td> <b class="deposit_fixed_text"></b> </td>
                                                                    <td style="text-align: right;"> ចំនួនប្រាក់ដែលនៅជំពាក់សរុបជាលេខ </td>
                                                                    <td> <b class="deposit_fixed_text_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;"> ប្រភេទនៃការទិញ </td>
                                                                    <td> <b class="method"></b> </td>
                                                                    <td style="text-align: right;"> ប្រភេទនៃការបង់ប្រាក </td>
                                                                    <td> <b class="type_of_payment"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;"> អត្រាការប្រាក់ </td>
                                                                    <td> <b class="interest_of_owne_precent"></b> </td>
                                                                    <td style="text-align: right;"> ប្រភេទអត្រាការប្រាក់ </td>
                                                                    <td> <b class="interest_type"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;">អត្រាការប្រាក់ជាភាគរយ</td>
                                                                    <td> <b class="deposit_precent"></b> </td>
                                                                    <td style="text-align: right;"> បង់នៅ </td>
                                                                    <td> <b class="place_for_pay"></b> </td>
                                                                </tr>
                                                               
                                                                <tr>
                                                                    <td style="text-align: right;">  រយៈពេលបង់ប្រាក់</td>
                                                                    <td> <b class="duration_pay_money_text"></b>  / <b class="duration_pay_money_type_text"></b></td>
                                                                    <td style="text-align: right;">  សុំបង់ថ្ងៃទី</td>
                                                                    <td> <b class="date_for_payments_text"></b></td>
                                                                   
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;">  កាលបរិច្ឆេទអនុម័តឥណទាន</td>
                                                                    <td> <b class="date_approval"></b> </td>
                                                                    <td style="text-align: right;">  កាលបរិច្ឆេទបើកផលិតផល</td>
                                                                    <td> <b class="date_give_product"></b></td>
                                                                   
                                                                </tr>

                                                                <tr>
                                                                    <td style="text-align: right;">  គណៈកម្មការឥណទាន</td>
                                                                    <td colspan="3"> <b class="approval_by"></b> </td>
                                                                   
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: right;">  សេចក្ដីពិពណ៌នា</td>
                                                                    <td colspan="3"> <b class="description"></b> </td>
                                                                   
                                                                </tr>
                                                                
                                                                
                                                    </tbody>
                                                </table>
                                            </div>
                                         </div>
			    			</div>
                        <!-- Edit Approval -->
                        
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>
        <script>

            function with_print(){
                window.location= "{{ url('accounts/credit_sale/'.$data_id.'/print_contract') }}";
                
            }
            function back_to_prev(){
                window.history.back();
                
            }
        </script>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
$(document).ready(function(){
    $(window).load(function(){
        get_sale_credit_sale_show();
    });
        function get_sale_credit_sale_show(){
            var url_json =  "{{ url('accounts/aprove_credit_sales/'.$data_id.'/show_detail_json') }}";
                    $.ajax({
                        type: "GET",
                        url: url_json, 
                        dataType: "json",
                        success: function(result){
                            var url_link = "{{ url('accounts/schedule/'.$client_id) }}";
                            if(result.schedule != null){
                                var btn_chachge = " <a href='"+url_link+"' id='show_schedule' class='btn btn-info pull-left show_schedule'  style='font-family: Hanuman !important;' rel='generate_schedule'><i class='icon-eye-open icon-white'></i> ការបង្ហាញកាលវិភាគសងប្រាក់អតិថិជន </a>";
                                $("#generate_schedule").after(btn_chachge);
                                $("#generate_schedule").remove();
                            }
                            console.log(result);
                            var text = "";
                            $.each(result.approval_item,function(i,da){
                                text += "<tr> " ;
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+(da.item.categorys !== null?da.item.categorys.name:"")+" </td>"
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                               
                            });

                            $(".item_list").html(text); 
                            if(result.is_agree == 1){
                                $(".is_agree").text("អនុម័ត");
                            }else if(result.is_agree == 2){
                                $(".is_agree").text("មិនទាន់អនុម័ត");
                            }else{
                                
                                $(".is_agree").text("មិនអនុម័ត");
                            }
                            $('.comment_manager').text(result.comment_manager);
                            $('.description').text(result.comment_manager);
                            var interest = result.interest_of_owne_precent * 100;
                            $(".interest_of_owne_precent").text(interest.toFixed(2)+"%");   
                            $('.deposit_precent').text(result.deposit_precent+"%");
                            $('.date_approval').text(day_format_show(result.date_approval));
                            var manager_id ="";
                            if(result.manager_id1){
                                manager_id += "<p>ប្រធាន :  "+result.manager_id1.name_kh+"</p>";
                            }else{
                                manager_id += "";
                            }
                            if(result.manager_id2){
                                manager_id += "<p>សមាជិក :  "+result.manager_id2.name_kh+"</p>";
                            }else{
                                manager_id += "";
                            }
                            if(result.manager_id3){
                                manager_id += "<p> សមាជិក :  "+result.manager_id3.name_kh+"</p>";
                            }else{
                                manager_id += "";
                            }   
                            $(".approval_by").html(manager_id);
                            if(result.is_finish == 1){
                                $(".is_finish").text("បញ្ចប់");
                            }else{
                                $(".is_finish").text("មិនទានបញ្ចប់");
                            }
                            if(result.method == "sale_by_credit"){
                                var mothod = "បង់រំលស់";
                            }else{
                                var mothod = "បង់ជាសាច់ប្រាក់";   
                            }
                            var  dpm = result.date_for_payments;
                            var d = new Date(dpm);
                            var year_n = d.getFullYear();
                            var month_n = d.getMonth() + 1;
                            var day_n = d.getDate();
                            if(month_n > 10){
                                month_n = month_n;
                            }else{
                                month_n = "0"+month_n; 
                            }
                            if(day_n > 10){
                                day_n = day_n;
                            }else{
                                day_n = "0"+day_n; 
                            }
                           var  date_for_payments = day_n +"-"+month_n+"-"+year_n;
                           var dpmt = result.duration_pay_money_type;
                            if(dpmt == "month"){
                                dpmt = "ខែ";
                            }else if(dpmt == "2week"){
                                dpmt = "២ សប្តាហ៍";
                            }else if(dpmt == "week"){
                                dpmt = "សប្តាហ៍";
                            }else if(dpmt == "day"){
                                dpmt = "ថ្ងៃ";
                            }  
                            if(result.type_of_payment == 1){
                                $(".type_of_payment").text("រំលស់ថយ"); 
                            }else{
                                $(".type_of_payment").text("រំលស់ថេរ"); 
                            }
                            if(result.interest_type === "special"){
                                $('.interest_type').text("អត្រាការប្រាក់ពិសេស់");
                            }else{
                                $('.interest_type').text(result.module_interest.display_name_kh);
                            }
                            if(result.place_for_pay == 1){
                                $(".place_for_pay").text("សាខា");     
                            }else{
                                $(".place_for_pay").text("ភូមិ"); 
                            }
                            $(".method").text(mothod); 
                            $(".prices_total_num_text").text(accounting.formatMoney(result.prices_total_num));
                            $(".deposit_fixed_text").text(accounting.formatMoney(result.deposit_fixed));
                            $(".duration_pay_money_text").text(result.duration_pay_money);
                            $(".duration_pay_money_type_text").text(dpmt);
                            $(".date_for_payments_text").text(date_for_payments );
                            $(".money_owne_text").text(accounting.formatMoney(result.money_owne));   
                            $(".money_owne_text_text").text(result.money_owne_word); 
                            $(".deposit_fixed_text_text").text(result.deposit_fixed_word);
                            $(".prices_total_text_text").text(result.prices_totalword);
                            $('.date_give_product').text(day_format_show(result.date_give_product));
                            
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
        }
   $(".generate_schedule").one("click", function(e){ 
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            e.preventDefault();
            var url_sc_gen = "{{ url('aprove_credit_sales/'.$data_id.'/generate_credit_sale') }}"  ;    
            $.ajax({
                type: "POST",
                url: url_sc_gen, 
                dataType: "json",
                success: function(data){
                    console.log(data);
                    $('.msg_show').html(data.msg_show); 
                    window.location="{{ url('accounts/schedule/'.$data_id) }}";        
                },
                error: function (data ,status, xhr) {
                    console.log(data.responseText);
                }                   
            });
    });
 });       
</script>            
@stop()
