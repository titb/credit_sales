@extends('credit_sale.layout.master')
@section('contend')	

<div class="container-fluid">
    <div class="row-fluid">
					
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span> {{$title}} </div>
                </div>
                
            <!-- <ul class="nav nav-tabs" style="margin-top: 1em;">
                <li ><a href="{!! url('accounts/'.$client_id.'/show') !!}">ការបង្ហាញព័ត៌មានអតិថិជន</a></li>
                <li class="<?php if(Request::segment(2) == 'show_sub_client'){echo "active";} ?>"><a href="{!! url('accounts/show_sub_client?client_id='.$client_id.'&sub_client_id=0&item=create') !!}">អ្នកធានា</a></li>
                <li>
                    <a href="">ទម្រង់ស្នើសុំការទិញបង់រំលស់</a></li>
            </ul>	 -->
                <div class="block-content collapse in change_content">
                @include('credit_sale.client_management.menu_client')
                    @include('errors.error')	

                    <div class="span12" style="margin-left: 0;margin-top: 10px;">

                        <center>
                            <h3 class="cen_title" style="font-family: 'Moul', 'Times New Roman';"> ពត័មានកិច្ចសន្យាទិញបង់រំលស់</h3>
                            <div class="muted span3 pull-right" style="margin-bottom:5px;">
                                
                                <input type="hidden" id="cli_id" name="cli_id" value="{{ $client_id }}">
                                <!-- <a href="{{ url('get_two_name_storn') }}" class="btn btn-info pull-right" target="_blank" style="margin-left: 5px;"> &nbsp;អតិថិជនស្ទួន</a> &nbsp; -->
                                <!-- <a href="{{ url('accounts/sub_client/create_sub_client?client_id='.$client_id.'&sub_client_id=0&item=create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី -->
                                </a>
                                
                            </div>
                        </center>
                        <legend></legend>
                        
                    </div>
                        
                            <table class="table table-bordered">
                                <thead style="background: rgb(251, 205, 205);">
                                <tr class="header">
                                
                                    <th>ល.រ</th>
                                    <th>លេខសំណើរ</th>
                                    <th>ឈ្មោះអតិថិជន</th>
                                    <th>ប្រាក់សរុប</th>
                                    <th>ប្រាក់បង់មុន</th>
                                    <th>ប្រាប់នៅសល់</th>
                                    <th>រយៈពេលបង់</th>
                                    <th>ថ្ងៃអនុម័ត</th>
                                    <th>មតិគណៈកម្មការ</th>
                                    <th>សកម្មភាព</th>
                                </tr>
                                </thead>
                                <tbody class="appr_list">
                                    
                                        
                                </tbody>
                                    <tr>
                                        <td colspan="9">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
                            </table>
                            <!-- Pagination -->
                    <div class="pagination text-right"></div>
                </div>
            </div>
            <!-- /block -->
        </div>
</div>
</div>
<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
    function day_format_show(date_format){
                var d = new Date(date_format);

                var year_n = d.getFullYear();
                var month_n = d.getMonth() + 1;
                var day_n = d.getDate();
                if(month_n > 10){
                    month_n = month_n;
                }else{
                    month_n = "0"+month_n; 
                }
               
                if(day_n > 10){
                    day_n = day_n;
                }else{
                    day_n = "0"+day_n; 
                }

        return  day_n +"-"+month_n+"-"+year_n;
    }
    $.ajax({
        type: "GET",
        url: "{{ url('accounts/aprove_credit_sales/'.$data_id.'/show_json') }}", 
        dataType: "json",
        success: function(result){
            console.log(result);
            var url_show_appr = "{{url('accounts/aprove_credit_sales')}}";
            var appr;
            
            $.each(result, function(k, v){
                appr += '<tr>';
                appr += '<td>'+ (k+1) +'</td>';
                appr += '<td>'+ v.cds_request_id +'</td>';
                appr += '<td>'+ v.cs_sales.cs_client.kh_username +'</td>';
                appr += '<td>'+ v.prices_total_num +'</td>';
                appr += '<td>'+ v.deposit_fixed +'</td>';
                appr += '<td>'+ v.money_owne +'</td>';
                appr += '<td>'+ v.duration_pay_money +'</td>';
                appr += '<td>'+ day_format_show(v.date_approval) +'</td>';
                appr += '<td>'+ v.comment_manager +'</td>';
                appr += '<td><a href="'+ url_show_appr + '/' + v.client_id + '/show_approve/'+ v.cds_request_id+'"​ class="btn btn-info btn_show">លម្អិត</a></td>';
                appr += '</tr>';
            });
            $(".appr_list").append(appr);
        },
        error: function(jqXHR, status){
            console.log(status.responseText);
        }
    });

$(document).ajaxComplete(function(){
    // Deleted data Client
    $(".btn_deleted").click(function(e){
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
            }
        })
        e.preventDefault();
        var url_cr = "{{ url('accounts/sub_client') }}";
        var client_id = $(this).val();
        var url_dell = url_cr+"/"+client_id+"/sub_delete";

        $.ajax({
            type: "POST",
            url: url_dell,
            dataType:"json",
            data : {client_id:client_id} ,
            success: function(data){
                console.log(data);
                
                window.scrollTo(0, 0);  
                $('.msg_show').html(data.msg_show);
                var numpage = 1 ;
                var url_index = "{{url('accounts/sub_client/show_sub_client')}}";
                var cl_id = $("#cli_id").val();
                var url_last = '?client_id='+cl_id+'&sub_client_id=0&item=create';
                url_index1 = url_index + url_last;
                window.location = url_index1;
            }		
        });
    }); 
});
		
</script>
@endsection