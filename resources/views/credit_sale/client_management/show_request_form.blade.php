@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('aprove_credit_sales') !!}"> គណនី</a> <span class="divider">|</span> ការបង្ហាញទម្រង់ស្នើសុំ </div>		
                                    <!-- <button id="back_to_prev" class="btn btn-danger pull-right back_to_prev" style="font-family: 'Hanuman' !important;" onclick="back_to_prev()"><i class=" icon-arrow-left icon-white"></i> back </button> -->
                            </div>

                            <div class="block-content collapse in">
                            @include('credit_sale.client_management.menu_client')
                            <!-- <button class="btn btn-success pull-left" id="print_hide" style=" margin-right: 10px; font-family: 'Hanuman' !important;" onclick="with_print()"><i class="icon-print icon-white"></i> ព្រីនកិច្ចសន្យាទិញបង់រំលស់ </button>   
                            <button id="generate_schedule" class="btn btn-info pull-left generate_schedule" style="margin-right: 10px;font-family: 'Hanuman' !important;"  name="generate_schedule"><i class="icon-th-list icon-white"></i> ការបង្កើតកាលវិភាគសងប្រាក់អតិថិជន </button> -->
                            <!-- <a href="{{url('aprove_credit_sales/'.$data_id.'/edit')}}" class="btn btn-primary pull-right"> កែប្រែ</a> -->
                            @include('errors.error')	       
                             	
			                        <h3 class="cen_title text-center khmer_Moul ">{{$title}}</h3>
			                      
                            	<legend></legend>
                                
                                 
                                <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th>ឈ្មោះទំនិញ</th>
                                                                <th>លេខ​កូដ​</th>
                                                                <th>ម៉ាក</th>
                                                                <th>ចំនួន</th>
                                                                <th>តម្លៃ​ឯកតា</th>
                                                                <th>តម្លៃសរុប</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="item_list">
                                                           
                                                        </tbody>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> តម្លៃសរុប </td>
                                                                    <td> <b class="prices_total_num_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រភេទនៃការទិញ </td>
                                                                    <td> <b class="method"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រាក់កក់ </td>
                                                                    <td> <b class="deposit_fixed_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> អត្រាការប្រាក់ </td>
                                                                    <td> <b class="interest_of_owne_precent"></b> </td>
                                                                </tr>
                                                                <!-- <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រភេទនៃការបង់ប្រាក់ </td>
                                                                    <td> <b class="type_of_payment"></b> </td>
                                                                </tr> -->
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">  រយៈពេលបង់ប្រាក់</td>
                                                                    <td> <b class="duration_pay_money_text"></b>  / <b class="duration_pay_money_type_text"></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">  សុំបង់ថ្ងៃទី</td>
                                                                    <td> <b class="date_for_payments_text"></b></td>
                                                                </tr>
                                                                <!-- <tr>
                                                                    <td colspan="5" style="text-align: right;">ចំនួនទឹកប្រាក់</td>
                                                                    <td> <b class="money_owne_text"></b> </td>
                                                                </tr> -->
                                                            
                                                        </table>  
                                                        <br/>
                                                <legend></legend> 
                                            </div>
                                         </div>
			    			</div>
                        <!-- Edit Approval -->
                        
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>
        <script>

            function with_print(){
                window.location= "{{ url('aprove_credit_sales/'.$data_id.'/print_contract') }}";
                
            }
            function back_to_prev(){
                window.history.back();
                
            }
        </script>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
    function day_format_show(date_format){
        var d = new Date(date_format);

        var year_n = d.getFullYear();
        var month_n = d.getMonth() + 1;
        var day_n = d.getDate();
        if(month_n > 10){
            month_n = month_n;
        }else{
            month_n = "0"+month_n; 
        }

        if(day_n >= 10){
            day_n = day_n;
        }else{
            day_n = "0"+day_n; 
        }

        return  day_n +"-"+month_n+"-"+year_n;
    }
    $(document).ready(function(){ 
        $(window).load(function(){
            get_request_show();
        });
        function get_request_show(){
            var url_json =  "{{ url('accounts/request_form_json') }}";
            $.ajax({
                type: "GET",
                url: url_json + "/{{$client_id}}/request_id/{{$data_id}}", 
                dataType: "json",
                success: function(result){
                    console.log(result);
                    var txt = "";
                    $.each(result.cs_sales.sale_item, function(k, v){
                        txt = '<tr>'+ 
                                '<td>' + v.item.name + '</td>' + 
                                '<td>' + v.item.item_bacode + '</td>' +
                                '<td>' + (v.item.cs_brand!==null?v.item.cs_brand.name:'') + '</td>' +
                                '<td>' + v.qty + '</td>' +
                                '<td>' + v.item.sell_price + "$" + '</td>' +
                                '<td>' + v.total_price + "$" + '</td>' +
                              '</tr>';
                    });
                    $(".item_list").append(txt);
                    $(".prices_total_num_text").text(result.total_payment + "$");
                    $(".method").text(result.method == "sale_by_credit"?"បង់រំលស់":"ទិញជាសាច់ប្រាក់");
                    $(".deposit_fixed_text").text(result.deposit_fixed + "$");
                    $(".interest_of_owne_precent").text(result.deposit_precent + " %");
                    $(".duration_pay_money_text").text(result.duration_pay_money);
                    if(result.duration_pay_money_type == "month"){
                        var dpmt = "ខែ";
                    }else if(result.duration_pay_money_type == "2week"){
                        var dpmt = "២សប្តាហ៍";
                    }else{
                        var dpmt = "សប្តាហ៍";
                    }
                    $(".duration_pay_money_type_text").text(dpmt);
                    $(".date_for_payments_text").text(day_format_show(result.date_for_payments));
                },
                error: function (result ,status, xhr) {
                    console.log(result.responseText);
                }
            
            });
        }
    });       
</script>            
@stop()
