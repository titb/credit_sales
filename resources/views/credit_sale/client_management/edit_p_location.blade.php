<!-- (Provine) -->
<script>
function myclient_province() {
    var x = document.getElementById("client_province").value;
    if( x == "រាជធានីភ្នំពេញ" ){
        document.getElementById("iclient_district").innerHTML ="<select class='chzn-select span12 m-wrap'  name='client_district' id='client_district' onchange='myclient_district()'>"+
        '<option>ជ្រើសរើស​ ស្រុក/ខណ្ឌ</option>'+
        '<option value="ចំការមន">ចំការមន</option>'+ 
        '<option value="ដូនពេញ">ដូនពេញ</option>'+
        '<option value="៧មករា">៧មករា</option>'+
        '<option value="ទួលគោក">ទួលគោក</option>'+
    '<option value="ដង្កោ">ដង្កោ</option>'+
    '<option value="មានជ័យ">មានជ័យ</option>'+
        '<option value="ឫស្សីកែវ">ឫស្សីកែវ</option>'+
        '<option value="សែនសុខ">សែនសុខ</option>'+
        '<option value="ពោធិ៍សែនជ័យ">ពោធិ៍សែនជ័យ</option>'+
        '<option value="ជ្រោយចង្វារ">ជ្រោយចង្វារ</option>'+
        '<option value="ព្រែកព្នៅ">ព្រែកព្នៅ</option>'+
        '<option value="ច្បារអំពៅ">ច្បារអំពៅ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if(x == "ខេត្តកណ្ដាល" ){
        document.getElementById("iclient_district").innerHTML ="<select class='chzn-select span12 m-wrap'  name='client_district' id='client_district' onchange='myclient_district()'>"+
        '<option value="កណ្ដាលស្ទឹង">កណ្ដាលស្ទឹង</option>'+
        '<option value="កៀនស្វាយ">កៀនស្វាយ</option>'+
        '<option value="ខ្សាច់កណ្ដាល">ខ្សាច់កណ្ដាល</option>'+
        '<option value="កោះធំ">កោះធំ</option>'+
        '<option value="លើកដែក">លើកដែក</option>'+
        '<option value="ល្វាឯម">ល្វាឯម</option>'+
        '<option value="មុខកំពូល">មុខកំពូល</option>'+
        '<option value="អង្គស្នួល">អង្គស្នួល</option>'+
        '<option value="ពញាឮ">ពញាឮ</option>'+
        '<option value="ស្អាង">ស្អាង</option>'+
        '<option value="ក្រុងតាខ្មៅ">ក្រុងតាខ្មៅ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if(x == "ខេត្តតាកែវ" ){
        document.getElementById("iclient_district").innerHTML ="<select class='chzn-select span12 m-wrap'  name='client_district' id='client_district' onchange='myclient_district()'>"+
        '<option value="អង្គរបូរី">អង្គរបូរី</option>'+
        '<option value="បាទី">បាទី</option>'+
        '<option value="បូរីជលសារ">បូរីជលសារ</option>'+
        '<option value="គីរីវង់">គីរីវង់</option>'+
        '<option value="កោះអណ្ដែត">កោះអណ្ដែត</option>'+
        '<option value="ព្រៃកប្បាស">ព្រៃកប្បាស</option>'+
        '<option value="សំរោង">សំរោង</option>'+
        '<option value="ក្រុងដូនកែវ">ក្រុងដូនកែវ</option>'+
        '<option value="ត្រាំកក់">ត្រាំកក់</option>'+
        '<option value="ទ្រាំង">ទ្រាំង</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if(x == "ខេត្តកំពត" ){
        document.getElementById("iclient_district").innerHTML ="<select class='chzn-select span12 m-wrap'  name='client_district' id='client_district' onchange='myclient_district()'>"+
        '<option value="អង្គរជ័យ">អង្គរជ័យ</option>'+
        '<option value="បន្ទាយមាស">បន្ទាយមាស</option>'+
        '<option value="ឈូក">ឈូក</option>'+
        '<option value="ជុំគិរី">ជុំគិរី</option>'+
        '<option value="ដងទង់">ដងទង់</option>'+
        '<option value="កំពង់ត្រាច">កំពង់ត្រាច</option>'+
        '<option value="ទឹកឈូ">ទឹកឈូ</option>'+
        '<option value="ក្រុងកំពត">ក្រុងកំពត</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if(x == "ខេត្តកំពង់ស្ពឺ" ){
        document.getElementById("iclient_district").innerHTML ="<select class='chzn-select span12 m-wrap'  name='client_district' id='client_district' onchange='myclient_district()'>"+
        '<option value="បរសេដ្ឋ">បរសេដ្ឋ</option>'+
        '<option value="ក្រុងច្បារមន">ក្រុងច្បារមន</option>'+
        '<option value="គងពិសី">គងពិសី</option>'+
        '<option value="ឱរ៉ាល់">ឱរ៉ាល់</option>'+
        '<option value="ឧដុង្គ">ឧដុង្គ</option>'+
        '<option value="ភ្នំស្រួច">ភ្នំស្រួច</option>'+
        '<option value="សំរោងទង">សំរោងទង</option>'+
        '<option value="ថ្ពង">ថ្ពង</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }
    
}
function myclient_district() {
    var x = document.getElementById("client_district").value;
 // Phnom Penh  
    if( x == "ចំការមន" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
    '<option value="ទន្លេបាសាក់">ទន្លេបាសាក់</option>'+
        '<option value="បឹងកេងកងទី ១">បឹងកេងកងទី ១</option>'+
        '<option value="បឹងកេងកងទី ២">បឹងកេងកងទី ២</option>'+
        '<option value="បឹងកេងកងទី ៣">បឹងកេងកងទី ៣</option>'+
        '<option value="អូឡាំពិក">អូឡាំពិក</option>'+
        '<option value="ទួលស្វាយព្រៃទី ១">ទួលស្វាយព្រៃទី ១</option>'+
        '<option value="ទួលស្វាយព្រៃទី ២">ទួលស្វាយព្រៃទី ២</option>'+
        '<option value="ទំនប់ទឹក">ទំនប់ទឹក</option>'+
        '<option value="ទួលទំពូងទី ២">ទួលទំពូងទី ២</option>'+
        '<option value="ទួលទំពូងទី ១">ទួលទំពូងទី ១</option>'+
        '<option value="បឹងត្របែក">បឹងត្របែក</option>'+
        '<option value="ផ្សារដើមថ្កូវ">ផ្សារដើមថ្កូវ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ដូនពេញ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ផ្សារថ្មីទី ១">ផ្សារថ្មីទី ១</option>'+
        '<option value="ផ្សារថ្មីទី ២">ផ្សារថ្មីទី ២</option>'+
        '<option value="ផ្សារថ្មីទី ៣">ផ្សារថ្មីទី ៣</option>'+
        '<option value="បឹងរាំង">បឹងរាំង</option>'+
        '<option value="ផ្សារកណ្ដាលទី១">ផ្សារកណ្ដាលទី១</option>'+
        '<option value="ផ្សារកណ្ដាលទី២">ផ្សារកណ្ដាលទី២</option>'+
       ' <option value="ចតុមុខ">ចតុមុខ</option>'+
        '<option value="ជ័យជំនះ">ជ័យជំនះ</option>'+
        '<option value="ផ្សារចាស់">ផ្សារចាស់</option>'+
        '<option value="ស្រះចក">ស្រះចក</option>'+
       ' <option value="វត្ដភ្នំ">វត្ដភ្នំ</option>'+
       '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "៧មករា" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="អូរឫស្សីទី ១">អូរឫស្សីទី ១</option>'+
        '<option value="អូរឫស្សីទី ២">អូរឫស្សីទី ២</option>'+
        '<option value="អូរឫស្សីទី ៣">អូរឫស្សីទី ៣</option>'+
        '<option value="អូរឫស្សីទី ៤">អូរឫស្សីទី ៤</option>'+
        '<option value="មនោរម្យ">មនោរម្យ</option>'+
        '<option value="មិត្ដភាព">មិត្ដភាព</option>'+
        '<option value="វាលវង់">វាលវង់</option>'+
        '<option value="បឹងព្រលឹត">បឹងព្រលឹត</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ទួលគោក" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ផ្សារដេប៉ូទី ១">ផ្សារដេប៉ូទី ១</option>'+
        '<option value="ផ្សារដេប៉ូទី ២">ផ្សារដេប៉ូទី ២</option>'+
        '<option value="ផ្សារដេប៉ូទី ៣">ផ្សារដេប៉ូទី ៣</option>'+
        '<option value="ទឹកល្អក់ទី ១">ទឹកល្អក់ទី ១</option>'+
        '<option value="ទឹកល្អក់ទី ២">ទឹកល្អក់ទី ២</option>'+
        '<option value="ទឹកល្អក់ទី ៣">ទឹកល្អក់ទី ៣</option>'+
        '<option value="បឹងកក់ទី ១">បឹងកក់ទី ១</option>'+
        '<option value="បឹងកក់ទី ២">បឹងកក់ទី ២</option>'+
        '<option value="ផ្សារដើមគរ">ផ្សារដើមគរ</option>'+
        '<option value="បឹងសាឡាង">បឹងសាឡាង</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ដង្កោ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ដង្កោ">ដង្កោ</option>'+
        '<option value="ពងទឹក">ពងទឹក</option>'+
        '<option value="ព្រៃវែង">ព្រៃវែង</option>'+
        '<option value="ព្រៃស">ព្រៃស</option>'+
        '<option value="ក្រាំងពង្រ">ក្រាំងពង្រ</option>'+
        '<option value="ប្រទះឡាង">ប្រទះឡាង</option>'+
        '<option value="សាក់សំពៅ">សាក់សំពៅ</option>'+
        '<option value="ជើងឯក">ជើងឯក</option>'+
        '<option value="គងនយ">គងនយ</option>'+
        '<option value="ព្រែកកំពឹស">ព្រែកកំពឹស</option>'+
        '<option value="រលួស">រលួស</option>'+
        '<option value="ស្ពានថ្ម">ស្ពានថ្ម</option>'+
        '<option value="ទៀន">ទៀន</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "មានជ័យ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ស្ទឹងមានជ័យ">ស្ទឹងមានជ័យ</option>'+
        '<option value="បឹងទំពុន">បឹងទំពុន</option>'+
        '<option value="ចាក់អង្រែលើ">ចាក់អង្រែលើ</option>'+
        '<option value="ចាក់អង្រែក្រោម">ចាក់អង្រែក្រោម</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ឫស្សីកែវ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ទួលសង្កែ">ទួលសង្កែ</option>'+
        '<option value="ស្វាយប៉ាក">ស្វាយប៉ាក</option>'+
        '<option value="គីឡូម៉ែត្រលេខ៦">គីឡូម៉ែត្រលេខ៦</option>'+
        '<option value="ឫស្សីកែវ">ឫស្សីកែវ</option>'+
        '<option value="ច្រាំងចំរេះទី ១">ច្រាំងចំរេះទី ១</option>'+
        '<option value="ច្រាំងចំរេះទី ២">ច្រាំងចំរេះទី ២</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "សែនសុខ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ភ្នំពេញថ្មី">ភ្នំពេញថ្មី</option>'+
        '<option value="ទឹកថ្លា">ទឹកថ្លា</option>'+
        '<option value="ឃ្មួញ">ឃ្មួញ</option>'+
        '<option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ពោធិ៍សែនជ័យ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>'+
        '<option value="ភ្លើងឆេះរទេះ">ភ្លើងឆេះរទេះ</option>'+
        '<option value="ចោមចៅ">ចោមចៅ</option>'+
        '<option value="កាកាប">កាកាប</option>'+
        '<option value="សំរោងក្រោម">សំរោងក្រោម</option>'+
        '<option value="បឹងធំ">បឹងធំ</option>'+
        '<option value="កំបូល">កំបូល</option>'+
        '<option value="កន្ទោក">កន្ទោក</option>'+
        '<option value="ឪឡោក">ឪឡោក</option>'+
        '<option value="ស្នោរ">ស្នោរ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ជ្រោយចង្វារ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ជ្រោយចង្វារ">ជ្រោយចង្វារ</option>'+
        '<option value="ព្រែកលៀប">ព្រែកលៀប</option>'+
        '<option value="ព្រែកតាសេក">ព្រែកតាសេក</option>'+
        '<option value="កោះដាច់">កោះដាច់</option>'+
        '<option value="បាក់ខែង">បាក់ខែង</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ព្រែកព្នៅ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ព្រែកព្នៅ">ព្រែកព្នៅ</option>'+
        '<option value="ពញាពន់">ពញាពន់</option>'+
        '<option value="សំរោង">សំរោង</option>'+
        '<option value="គោករកា">គោករកា</option>'+
        '<option value="ពន្សាំង">ពន្សាំង</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ច្បារអំពៅ" ){
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ច្បារអំពៅទី ១">ច្បារអំពៅទី ១</option>'+
        '<option value="ច្បារអំពៅទី ២">ច្បារអំពៅទី ២</option>'+
        '<option value="និរោធ">និរោធ</option>'+
        '<option value="ព្រែកប្រា">ព្រែកប្រា</option>'+
        '<option value="វាលស្បូវ">វាលស្បូវ</option>'+
        '<option value="ព្រែកឯង">ព្រែកឯង</option>'+
        '<option value="ក្បាលកោះ">ក្បាលកោះ</option>'+
        '<option value="ព្រែកថ្មី">ព្រែកថ្មី</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
  // End Phnom Penh
  // Takeo
    }else if ( x == "អង្គរបូរី") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="អង្គរបូរី">អង្គរបូរី</option>'+
        '<option value="បាស្រែ">បាស្រែ</option>'+
        '<option value="គោកធ្លក">គោកធ្លក</option>'+
        '<option value="ពន្លៃ">ពន្លៃ</option>'+
        '<option value="ព្រែកផ្ទោល">ព្រែកផ្ទោល</option>'+
        '<option value="ព្រៃផ្គាំ">ព្រៃផ្គាំ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';

    }else if ( x == "បាទី") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ចំបក់">ចំបក់</option>'+
        '<option value="ចំប៉ី">ចំប៉ី</option>'+
        '<option value="ដូង">ដូង</option>'+
        '<option value="កណ្ដឹង">កណ្ដឹង</option>'+
        '<option value="កុមាររាជា">កុមាររាជា</option>'+
        '<option value="ក្រាំងលាវ">ក្រាំងលាវ</option>'+
        '<option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>'+
        '<option value="លំពង់">លំពង់</option>'+
        '<option value="ពារាម">ពារាម</option>'+
        '<option value="ពត់សរ">ពត់សរ</option>'+
        '<option value="សូរភី">សូរភី</option>'+
        '<option value="តាំងដូង">តាំងដូង</option>'+
        '<option value="ត្នោត">ត្នោត</option>'+
        '<option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>'+
        '<option value="ត្រពាំងសាប">ត្រពាំងសាប</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';

    }else if ( x == "បូរីជលសារ") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="បូរីជលសារ">បូរីជលសារ</option>'+
        '<option value="ជ័យជោគ">ជ័យជោគ</option>'+
        '<option value="ដូងខ្ពស់">ដូងខ្ពស់</option>'+
        '<option value="កំពង់ក្រសាំង">កំពង់ក្រសាំង</option>'+
        '<option value="គោកពោធិ៍">គោកពោធិ៍</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "គីរីវង់") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="អង្គប្រាសាទ">អង្គប្រាសាទ</option>'+
        '<option value="ព្រះបាទជាន់ជុំ">ព្រះបាទជាន់ជុំ</option>'+
        '<option value="កំណប់">កំណប់</option>'+
        '<option value="កំពែង">កំពែង</option>'+
        '<option value="គីរីចុងកោះ">គីរីចុងកោះ</option>'+
        '<option value="គោកព្រេច">គោកព្រេច</option>'+
        '<option value="ភ្នំដិន">ភ្នំដិន</option>'+
        '<option value="ព្រៃអំពក">ព្រៃអំពក</option>'+
        '<option value="ព្រៃរំដេង">ព្រៃរំដេង</option>'+
        '<option value="រាមអណ្ដើក">រាមអណ្ដើក</option>'+
        '<option value="សោម">សោម</option>'+
        '<option value="តាអូរ">តាអូរ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "កោះអណ្ដែត") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="ក្រពុំឈូក">ក្រពុំឈូក</option>'+
        '<option value="ពេជសារ">ពេជសារ</option>'+
        '<option value="ព្រៃខ្លា">ព្រៃខ្លា</option>'+
        '<option value="ព្រៃយុថ្កា">ព្រៃយុថ្កា</option>'+
        '<option value="រមេញ">រមេញ</option>'+
        '<option value="ធ្លាប្រជុំ">ធ្លាប្រជុំ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "ព្រៃកប្បាស") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()" >'+
        '<option value="អង្កាញ់">អង្កាញ់</option>'+
        '<option value="បានកាម">បានកាម</option>'+
        '<option value="ចំប៉ា">ចំប៉ា</option>'+
        '<option value="ចារ">ចារ</option>'+
        '<option value="កំពែង">កំពែង</option>'+
        '<option value="កំពង់រាប">កំពង់រាប</option>'+
        '<option value="ក្ដាញ់">ក្ដាញ់</option>'+
        '<option value="ពោធិ៍រំចាក">ពោធិ៍រំចាក</option>'+
        '<option value="ព្រៃកប្បាស">ព្រៃកប្បាស</option>'+
        '<option value="ព្រៃល្វា">ព្រៃល្វា</option>'+
        '<option value="ព្រៃផ្ដៅ">ព្រៃផ្ដៅ</option>'+
        '<option value="ស្នោ">ស្នោ</option>'+
        '<option value="តាំងយ៉ាប">តាំងយ៉ាប</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "សំរោង") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="បឹងត្រាញ់ខាងជើង">បឹងត្រាញ់ខាងជើង</option>'+
        '<option value="បឹងត្រាញ់ខាងត្បូង">បឹងត្រាញ់ខាងត្បូង</option>'+
        '<option value="ជើងគួន">ជើងគួន</option>'+
        '<option value="ជំរះពេន">ជំរះពេន</option>'+
        '<option value="ខ្វាវ">ខ្វាវ</option>'+
        '<option value="លំចង់">លំចង់</option>'+
        '<option value="រវៀង">រវៀង</option>'+
        '<option value="សំរោង">សំរោង</option>'+
        '<option value="សឹង្ហ">សឹង្ហ</option>'+
        '<option value="ស្លា">ស្លា</option>'+
        '<option value="ទ្រា">ទ្រា</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "ក្រុងដូនកែវ") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="បារាយណ៍">បារាយណ៍</option>'+
        '<option value="រកាក្នុង">រកាក្នុង</option>'+
        '<option value="រកាក្រៅ">រកាក្រៅ</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "ត្រាំកក់") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="អង្គតាសោម">អង្គតាសោម</option>'+
        '<option value="ជាងទង">ជាងទង</option>'+
        '<option value="គុស">គុស</option>'+
        '<option value="លាយបូរ">លាយបូរ</option>'+
        '<option value="ញ៉ែងញ៉ង">ញ៉ែងញ៉ង</option>'+
        '<option value="អូរសារាយ">អូរសារាយ</option>'+
        '<option value="ត្រពាំងក្រញូង">ត្រពាំងក្រញូង</option>'+
        '<option value="ឧត្ដមសុរិយា">ឧត្ដមសុរិយា</option>'+
        '<option value="ពពេល">ពពេល</option>'+
        '<option value="សំរោង">សំរោង</option>'+
        '<option value="ស្រែរនោង">ស្រែរនោង</option>'+
        '<option value="តាភេម">តាភេម</option>'+
        '<option value="ត្រាំកក់">ត្រាំកក់</option>'+
        '<option value="ត្រពាំងធំខាងជើង">ត្រពាំងធំខាងជើង</option>'+
        '<option value="ត្រពាំងធំខាងត្បូង">ត្រពាំងធំខាងត្បូង</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if ( x == "ត្រាំកក់") {
        document.getElementById("iclient_commune").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_commune" id="client_commune" onchange="myclient_commune()">'+
        '<option value="អង្កាញ់">អង្កាញ់</option>'+
        '<option value="ទ្រាំង">ទ្រាំង</option>'+
        '<option value="ជីខ្មា">ជីខ្មា</option>'+
        '<option value="ខ្វាវ">ខ្វាវ</option>'+
        '<option value="ប្រាំបីមុំ">ប្រាំបីមុំ</option>'+
        '<option value="អង្គកែវ">អង្គកែវ</option>'+
        '<option value="ព្រៃស្លឹក">ព្រៃស្លឹក</option>'+
        '<option value="រនាម">រនាម</option>'+
        '<option value="សំបួរ">សំបួរ</option>'+
        '<option value="សន្លុង">សន្លុង</option>'+
        '<option value="ស្មោង">ស្មោង</option>'+
        '<option value="ស្រង៉ែ">ស្រង៉ែ</option>'+
        '<option value="ធ្លក">ធ្លក</option>'+
        '<option value="ត្រឡាច">ត្រឡាច</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if( x == "ផ្សេងៗ" ){
              document.getElementById("other_district").innerHTML =   '<div class="control-group"><div class="controls"><input type="text" class="span12 m-wrap" name="other_district"></div></div>';
        }
}

function myclient_commune() {
    var x = document.getElementById("client_commune").value;
    if ( x == "ទន្លេបាសាក់") {
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ភូមិ ៦">ភូមិ ៦</option>'+
        '<option value="ភូមិ ៧">ភូមិ ៧</option>'+
        '<option value="ភូមិ ៨">ភូមិ ៨</option>'+
        '<option value="ភូមិ ៩">ភូមិ ៩</option>'+
        '<option value="ភូមិ ១០">ភូមិ ១០</option>'+
        '<option value="ភូមិ ១១">ភូមិ ១១</option>'+
        '<option value="ភូមិ ១២">ភូមិ ១២</option>'+
        '<option value="ភូមិ ១៣">ភូមិ ១៣</option>'+
        '<option value="ភូមិ ១៤">ភូមិ ១៤</option>'+
        '<option value="ភូមិ ១៥">ភូមិ ១៥</option>'+
        '<option value="ភូមិ ១៦">ភូមិ ១៦</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if(x == "បឹងកេងកងទី ១"){
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ភូមិ ៦">ភូមិ ៦</option>'+
        '<option value="ភូមិ ៧">ភូមិ ៧</option>'+
        '<option value="ភូមិ ៨">ភូមិ ៨</option>'+
        '<option value="ភូមិ ៩">ភូមិ ៩</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if(x == "អូឡាំពិក"){
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
       '</select>';
    }else if(x == "ទួលស្វាយព្រៃទី ១"){
    document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ភូមិ ៦">ភូមិ ៦</option>'+
        '<option value="ភូមិ ៧">ភូមិ ៧</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
      '</select>';
    }else if(x == "ទួលស្វាយព្រៃទី ២"){
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ភូមិ ៦">ភូមិ ៦</option>'+
        '<option value="ភូមិ ៧">ភូមិ ៧</option>'+
        '<option value="ភូមិ ៨">ភូមិ ៨</option>'+
        '<option value="ភូមិ ៩">ភូមិ ៩</option>'+
        '<option value="ភូមិ ១០">ភូមិ ១០</option>'+
        '<option value="ភូមិ ១១">ភូមិ ១១</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>';
    }else if (x == "ទំនប់ទឹក") {
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>'
    }else if (x == "ទួលទំពូងទី ២") {
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>'
    }else if (x == "ទួលទំពូងទី ១") {
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>'
    }else if (x == "បឹងត្របែក") {
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ភូមិ ៦">ភូមិ ៦</option>'+
        '<option value="ភូមិ ៧">ភូមិ ៧</option>'+
        '<option value="ភូមិ ៨">ភូមិ ៨</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>'
    }else if (x == "ផ្សារដើមថ្កូវ") {
        document.getElementById("iclient_village").innerHTML ='<select class="chzn-select span12 m-wrap"  name="client_village" id="client_village">'+
        '<option value="ភូមិ ១">ភូមិ ១</option>'+
        '<option value="ភូមិ ២">ភូមិ ២</option>'+
        '<option value="ភូមិ ៣">ភូមិ ៣</option>'+
        '<option value="ភូមិ ៤">ភូមិ ៤</option>'+
        '<option value="ភូមិ ៥">ភូមិ ៥</option>'+
        '<option value="ភូមិ ៦">ភូមិ ៦</option>'+
        '<option value="ភូមិ ៧">ភូមិ ៧</option>'+
        '<option value="ផ្សេងៗ">ផ្សេងៗ</option>'+
        '</select>'
    }else if( x == "ផ្សេងៗ" ){
              document.getElementById("other_commune").innerHTML = '<div class="control-group"><div class="controls"><input type="text" class="span12 m-wrap" name="other_commune"></div></div>';
        }

}

function myclient_village(){
        var x = document.getElementById("client_village").value;
    if( x == "ផ្សេងៗ" ){
              document.getElementById("other_village").innerHTML = '<div class="control-group"><div class="controls"><input type="text" class="span12 m-wrap" name="other_village"></div></div>';
     }    
}
</script>

<label class="control-label">ខេត្តក្រុង</label>

<div class="controls">

<select class="chzn-select span12 m-wrap " name="client_province" id="client_province" onchange="myclient_province()">
    @if($data->client_province)
        <option value="{{ $data->client_province }}">{{ $data->client_province }}</option>
        <option>ជ្រើសរើរស​ ខេត្តក្រុង</option>
    @else
        <option>ជ្រើសរើរស​ ខេត្តក្រុង</option>
    @endif
    <option value="រាជធានីភ្នំពេញ">រាជធានីភ្នំពេញ</option>
    <option value="ខេត្តតាកែវ">តាកែវ</option>
    <option value="ខេត្តកណ្ដាល">កណ្ដាល</option>
    <option value="ខេត្តកំពត">កំពត</option>
    <option value="ខេត្តកំពង់ស្ពឺ">កំពង់ស្ពឺ</option>
    <option value="ខេត្តបន្ទាយមានជ័យ">បន្ទាយមានជ័យ</option>
    <option value="ខេត្តបាត់ដំបង">បាត់ដំបង</option>
    <option value="ខេត្តកំពង់ចាម">កំពង់ចាម</option>
    <option value="ខេត្តកំពង់ឆ្នាំង">កំពង់ឆ្នាំង</option>

    <option value="ខេត្តកំពង់ធំ">កំពង់ធំ</option>
    <option value="ខេត្តកោះកុង">    កោះកុង</option>
    <option value="ខេត្តក្រចេះ">ក្រចេះ</option>
    <option value="ខេត្តមណ្ឌលគិរី">មណ្ឌលគិរី</option>
    <option value="ខេត្តព្រះវិហារ">ព្រះវិហារ</option>
    <option value="ខេត្តព្រៃវែង">ព្រៃវែង</option>
    <option value="ខេត្តពោធិ៍សាត់">ពោធិ៍សាត់</option>
    <option value="ខេត្តរតនគិរី">រតនគិរី</option>
    <option value="ខេត្តសៀមរាប">សៀមរាប</option>
    <option value="ខេត្តព្រះសីហនុ">ព្រះសីហនុ</option>
    <option value="ខេត្តស្ទឹងត្រែង">ស្ទឹងត្រែង</option>
    <option value="ខេត្តស្វាយរៀង">ស្វាយរៀង</option> <option value="ខេត្តឧត្ដរមានជ័យ">ឧត្ដរមានជ័យ</option>
    <option value="ខេត្តកែប">កែប</option>
    <option value="ខេត្តប៉ៃលិន">ប៉ៃលិន</option>
    <option value="ខេត្តត្បូងឃ្មុំ">ត្បូងឃ្មុំ</option>
</select>

</div>

<br/>

<label class="control-label">ស្រុក/ខណ្ឌ</label>

<div class="controls">
<p id="iclient_district">
    <!-- District -->
<select class="span12 m-wrap​​​ chzn-select" name="client_district" id="client_district" onchange="myclient_district()" >
<!--        Phnom Penh-->
    @if($data->client_district)
        <option value="{{ $data->client_district }}">{{ $data->client_district }}</option>
        <option>ជ្រើសរើរស​ ស្រុក/ខណ្ឌ</option>
    @else
        <option>ជ្រើសរើរស​ ស្រុក/ខណ្ឌ</option>
    @endif
        <option value="ចំការមន">ចំការមន</option>
        <option value="ដូនពេញ">ដូនពេញ</option>
        <option value="៧មករា">៧មករា</option>
        <option value="ទួលគោក">ទួលគោក</option>
        <option value="ដង្កោ">ដង្កោ</option>
        <option value="មានជ័យ">មានជ័យ</option>
        <option value="ឫស្សីកែវ">ឫស្សីកែវ</option>
        <option value="សែនសុខ">សែនសុខ</option>
        <option value="ពោធិ៍សែនជ័យ">ពោធិ៍សែនជ័យ</option>
        <option value="ជ្រោយចង្វារ">ជ្រោយចង្វារ</option>
        <option value="ព្រែកព្នៅ">ព្រែកព្នៅ</option>
        <option value="ច្បារអំពៅ">ច្បារអំពៅ</option>
    
<!--Kandal-->
        <option value="កណ្ដាលស្ទឹង">កណ្ដាលស្ទឹង</option>
        <option value="កៀនស្វាយ">កៀនស្វាយ</option>
        <option value="ខ្សាច់កណ្ដាល">ខ្សាច់កណ្ដាល</option>
        <option value="កោះធំ">កោះធំ</option>
        <option value="លើកដែក">លើកដែក</option>
        <option value="ល្វាឯម">ល្វាឯម</option>
        <option value="មុខកំពូល">មុខកំពូល</option>
        <option value="អង្គស្នួល">អង្គស្នួល</option>
        <option value="ពញាឮ">ពញាឮ</option>
        <option value="ស្អាង">ស្អាង</option>
        <option value="ក្រុងតាខ្មៅ">ក្រុងតាខ្មៅ</option>
<!--    Takeo-->
        <option value="អង្គរបូរី">អង្គរបូរី</option>
        <option value="បាទី">បាទី</option>
        <option value="បូរីជលសារ">បូរីជលសារ</option>
        <option value="គីរីវង់">គីរីវង់</option>
        <option value="កោះអណ្ដែត">កោះអណ្ដែត</option>
        <option value="ព្រៃកប្បាស">ព្រៃកប្បាស</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ក្រុងដូនកែវ">ក្រុងដូនកែវ</option>
        <option value="ត្រាំកក់">ត្រាំកក់</option>
        <option value="ទ្រាំង">ទ្រាំង</option>
<!--    Kampot-->
        <option value="អង្គរជ័យ">អង្គរជ័យ</option>
        <option value="បន្ទាយមាស">បន្ទាយមាស</option>
        <option value="ឈូក">ឈូក</option>
        <option value="ជុំគិរី">ជុំគិរី</option>
        <option value="ដងទង់">ដងទង់</option>
        <option value="កំពង់ត្រាច">កំពង់ត្រាច</option>
        <option value="ទឹកឈូ">ទឹកឈូ</option>
        <option value="ក្រុងកំពត">ក្រុងកំពត</option>
<!--    Kampong Sue-->
        <option value="បរសេដ្ឋ">បរសេដ្ឋ</option>
        <option value="ក្រុងច្បារមន">ក្រុងច្បារមន</option>
        <option value="គងពិសី">គងពិសី</option>
        <option value="ឱរ៉ាល់">ឱរ៉ាល់</option>
        <option value="ឧដុង្គ">ឧដុង្គ</option>
        <option value="ភ្នំស្រួច">ភ្នំស្រួច</option>
        <option value="សំរោងទង">សំរោងទង</option>
        <option value="ថ្ពង">ថ្ពង</option>
        <option value="ផ្សេងៗ">ផ្សេងៗ</option>
</select>  
</p>
 <p id="other_district" ></p>
</div>

<label class="control-label">ឃុំ/សង្កាត់</label>

<div class="controls">
<p id="iclient_commune">
    <select class="span12 m-wrap chzn-select" name="client_commune" id="client_commune" onchange="myclient_commune()" ​​​​​​​​>
<!--    Phnom Penh-->
    <!--    ខណ្ឌចំការមន-->
    @if($data->client_commune)
        <option value="{{ $data->client_commune }}">{{ $data->client_commune }}</option>
        <option>ជ្រើសរើរស​ ឃុំ/សង្កាត់</option>
    @else
        <option>ជ្រើសរើរស​ ឃុំ/សង្កាត់</option>
    @endif
        <option value="ទន្លេបាសាក់">ទន្លេបាសាក់</option>
        <option value="បឹងកេងកងទី ១">បឹងកេងកងទី ១</option>
        <option value="បឹងកេងកងទី ២">បឹងកេងកងទី ២</option>
        <option value="បឹងកេងកងទី ៣">បឹងកេងកងទី ៣</option>
        <option value="អូឡាំពិក">អូឡាំពិក</option>
        <option value="ទួលស្វាយព្រៃទី ១">ទួលស្វាយព្រៃទី ១</option>
        <option value="ទួលស្វាយព្រៃទី ២">ទួលស្វាយព្រៃទី ២</option>
        <option value="ទំនប់ទឹក">ទំនប់ទឹក</option>
        <option value="ទួលទំពូងទី ២">ទួលទំពូងទី ២</option>
        <option value="ទួលទំពូងទី ១">ទួលទំពូងទី ១</option>
        <option value="បឹងត្របែក">បឹងត្របែក</option>
        <option value="ផ្សារដើមថ្កូវ">ផ្សារដើមថ្កូវ</option>
    <!--    ដូនពេញ-->
        <option value="ផ្សារថ្មីទី ១">ផ្សារថ្មីទី ១</option>
        <option value="ផ្សារថ្មីទី ២">ផ្សារថ្មីទី ២</option>
        <option value="ផ្សារថ្មីទី ៣">ផ្សារថ្មីទី ៣</option>
        <option value="បឹងរាំង">បឹងរាំង</option>
        <option value="ផ្សារកណ្ដាលទី១">ផ្សារកណ្ដាលទី១</option>
        <option value="ផ្សារកណ្ដាលទី២">ផ្សារកណ្ដាលទី២</option>
        <option value="ចតុមុខ">ចតុមុខ</option>
        <option value="ជ័យជំនះ">ជ័យជំនះ</option>
        <option value="ផ្សារចាស់">ផ្សារចាស់</option>
        <option value="ស្រះចក">ស្រះចក</option>
        <option value="វត្ដភ្នំ">វត្ដភ្នំ</option>
<!--        ៧មករា-->
        <option value="អូរឫស្សីទី ១">អូរឫស្សីទី ១</option>
        <option value="អូរឫស្សីទី ២">អូរឫស្សីទី ២</option>
        <option value="អូរឫស្សីទី ៣">អូរឫស្សីទី ៣</option>
        <option value="អូរឫស្សីទី ៤">អូរឫស្សីទី ៤</option>
        <option value="មនោរម្យ">មនោរម្យ</option>
        <option value="មិត្ដភាព">មិត្ដភាព</option>
        <option value="វាលវង់">វាលវង់</option>
        <option value="បឹងព្រលឹត">បឹងព្រលឹត</option>
<!--        ទួលគោក-->
        <option value="ផ្សារដេប៉ូទី ១">ផ្សារដេប៉ូទី ១</option>
        <option value="ផ្សារដេប៉ូទី ២">ផ្សារដេប៉ូទី ២</option>
        <option value="ផ្សារដេប៉ូទី ៣">ផ្សារដេប៉ូទី ៣</option>
        <option value="ទឹកល្អក់ទី ១">ទឹកល្អក់ទី ១</option>
        <option value="ទឹកល្អក់ទី ២">ទឹកល្អក់ទី ២</option>
        <option value="ទឹកល្អក់ទី ៣">ទឹកល្អក់ទី ៣</option>
        <option value="បឹងកក់ទី ១">បឹងកក់ទី ១</option>
        <option value="បឹងកក់ទី ២">បឹងកក់ទី ២</option>
        <option value="ផ្សារដើមគរ">ផ្សារដើមគរ</option>
        <option value="បឹងសាឡាង">បឹងសាឡាង</option>
<!--        ដង្កោ-->
        <option value="ដង្កោ">ដង្កោ</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ព្រៃវែង">ព្រៃវែង</option>
        <option value="ព្រៃស">ព្រៃស</option>
        <option value="ក្រាំងពង្រ">ក្រាំងពង្រ</option>
        <option value="ប្រទះឡាង">ប្រទះឡាង</option>
        <option value="សាក់សំពៅ">សាក់សំពៅ</option>
        <option value="ជើងឯក">ជើងឯក</option>
        <option value="គងនយ">គងនយ</option>
        <option value="ព្រែកកំពឹស">ព្រែកកំពឹស</option>
        <option value="រលួស">រលួស</option>
        <option value="ស្ពានថ្ម">ស្ពានថ្ម</option>
        <option value="ទៀន">ទៀន</option>
<!--        មានជ័យ-->
        <option value="ស្ទឹងមានជ័យ">ស្ទឹងមានជ័យ</option>
        <option value="បឹងទំពុន">បឹងទំពុន</option>
        <option value="ចាក់អង្រែលើ">ចាក់អង្រែលើ</option>
        <option value="ចាក់អង្រែក្រោម">ចាក់អង្រែក្រោម</option>
<!--        ឫស្សីកែវ-->
        <option value="ទួលសង្កែ">ទួលសង្កែ</option>
        <option value="ស្វាយប៉ាក">ស្វាយប៉ាក</option>
        <option value="គីឡូម៉ែត្រលេខ៦">គីឡូម៉ែត្រលេខ៦</option>
        <option value="ឫស្សីកែវ">ឫស្សីកែវ</option>
        <option value="ច្រាំងចំរេះទី ១">ច្រាំងចំរេះទី ១</option>
        <option value="ច្រាំងចំរេះទី ២">ច្រាំងចំរេះទី ២</option>
<!--        សែនសុខ-->
        <option value="ភ្នំពេញថ្មី">ភ្នំពេញថ្មី</option>
        <option value="ទឹកថ្លា">ទឹកថ្លា</option>
        <option value="ឃ្មួញ">ឃ្មួញ</option>
        <option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>
<!--        ពោធិ៍សែនជ័យ-->
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ភ្លើងឆេះរទេះ">ភ្លើងឆេះរទេះ</option>
        <option value="ចោមចៅ">ចោមចៅ</option>
        <option value="កាកាប">កាកាប</option>
        <option value="សំរោងក្រោម">សំរោងក្រោម</option>
        <option value="បឹងធំ">បឹងធំ</option>
        <option value="កំបូល">កំបូល</option>
        <option value="កន្ទោក">កន្ទោក</option>
        <option value="ឪឡោក">ឪឡោក</option>
        <option value="ស្នោរ">ស្នោរ</option>
<!--        ជ្រោយចង្វារ-->
        <option value="ជ្រោយចង្វារ">ជ្រោយចង្វារ</option>
        <option value="ព្រែកលៀប">ព្រែកលៀប</option>
        <option value="ព្រែកតាសេក">ព្រែកតាសេក</option>
        <option value="កោះដាច់">កោះដាច់</option>
        <option value="បាក់ខែង">បាក់ខែង</option>
<!--        ព្រែកព្នៅ-->
        <option value="ព្រែកព្នៅ">ព្រែកព្នៅ</option>
        <option value="ពញាពន់">ពញាពន់</option>
        <option value="សំរោង">សំរោង</option>
        <option value="គោករកា">គោករកា</option>
        <option value="ពន្សាំង">ពន្សាំង</option>
<!--        ច្បារអំពៅ -->
        <option value="ច្បារអំពៅទី ១">ច្បារអំពៅទី ១</option>
        <option value="ច្បារអំពៅទី ២">ច្បារអំពៅទី ២</option>
        <option value="និរោធ">និរោធ</option>
        <option value="ព្រែកប្រា">ព្រែកប្រា</option>
        <option value="វាលស្បូវ">វាលស្បូវ</option>
        <option value="ព្រែកឯង">ព្រែកឯង</option>
        <option value="ក្បាលកោះ">ក្បាលកោះ</option>
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
    <!--Kandal-->
<!--        កណ្ដាលស្ទឹង-->
        <option value="អំពៅព្រៃ">អំពៅព្រៃ</option>
        <option value="អន្លង់រមៀត">អន្លង់រមៀត</option>
        <option value="បារគូ">បារគូ</option>
        <option value="បឹងខ្យាង">បឹងខ្យាង</option>
        <option value="ជើងកើប">ជើងកើប</option>
        <option value="ដើមឫស">ដើមឫស</option>
        <option value="កណ្ដោក">កណ្ដោក</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="គោកត្រប់">គោកត្រប់</option>
        <option value="ព្រះពុទ្ធ">ព្រះពុទ្ធ</option>
        <option value="ព្រែករកា">ព្រែករកា</option>
        <option value="ព្រែកស្លែង">ព្រែកស្លែង</option>
        <option value="រកា">រកា</option>
        <option value="រលាំងកែន">រលាំងកែន</option>
        <option value="សៀមរាប">សៀមរាប</option>
        <option value="ត្បែង">ត្បែង</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ទ្រា">ទ្រា</option>
<!--        កៀនស្វាយ-->
        <option value="បន្ទាយដែក">បន្ទាយដែក</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="ដីឥដ្ឋ">ដីឥដ្ឋ</option>
        <option value="កំពង់ស្វាយ">កំពង់ស្វាយ</option>
        <option value="គគីរ">គគីរ</option>
        <option value="គគីរធំ">គគីរធំ</option>
        <option value="ភូមិធំ">ភូមិធំ</option>
        <option value="សំរោងធំ">សំរោងធំ</option>
<!--        ខ្សាច់កណ្ដាល-->
        <option value="បាក់ដាវ">បាក់ដាវ</option>
        <option value="ជ័យធំ">ជ័យធំ</option>
        <option value="កំពង់ចំលង">កំពង់ចំលង</option>
        <option value="កោះចូរ៉ាម">កោះចូរ៉ាម</option>
        <option value="កោះឧកញ៉ាតី">កោះឧកញ៉ាតី</option>
        <option value="ព្រះប្រសប់">ព្រះប្រសប់</option>
        <option value="ព្រែកអំពិល">ព្រែកអំពិល</option>
        <option value="ព្រែកលួង">ព្រែកលួង</option>
        <option value="ព្រែកតាកូវ">ព្រែកតាកូវ</option>
        <option value="ព្រែកតាមាក់">ព្រែកតាមាក់</option>
        <option value="ពុកឫស្សី">ពុកឫស្សី</option>
        <option value="រកាជន្លឹង">រកាជន្លឹង</option>
        <option value="សន្លុង">សន្លុង</option>
        <option value="ស៊ីធរ">ស៊ីធរ</option>
        <option value="ស្វាយជ្រំ">ស្វាយជ្រំ</option>
        <option value="ស្វាយរមៀត">ស្វាយរមៀត</option>
        <option value="តាឯក">តាឯក</option>
        <option value="វិហារសួគ៌">វិហារសួគ៌</option>
<!--        កោះធំ-->
        <option value="ឈើខ្មៅ">ឈើខ្មៅ</option>
        <option value="ជ្រោយតាកែវ">ជ្រោយតាកែវ</option>
        <option value="កំពង់កុង">កំពង់កុង</option>
        <option value="កោះធំ ‹ក›">កោះធំ ‹ក›</option>
        <option value="កោះធំ ‹ខ›">កោះធំ ‹ខ›</option>
        <option value="លើកដែក">លើកដែក</option>
        <option value="ពោធិ៍បាន">ពោធិ៍បាន</option>
        <option value="ព្រែកជ្រៃ">ព្រែកជ្រៃ</option>
        <option value="ព្រែកស្ដី">ព្រែកស្ដី</option>
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="សំពៅពូន">សំពៅពូន</option>
<!--        លើកដែក-->
        <option value="កំពង់ភ្នំ">កំពង់ភ្នំ</option>
        <option value="ក្អមសំណរ">ក្អមសំណរ</option>
        <option value="ខ្ពបអាទាវ">ខ្ពបអាទាវ</option>
        <option value="ពាមរាំង">ពាមរាំង</option>
        <option value="ព្រែកដាច់">ព្រែកដាច់</option>
        <option value="ព្រែកទន្លាប់">ព្រែកទន្លាប់</option>
        <option value="សណ្ដារ">សណ្ដារ</option>
<!--        ល្វាឯម-->
        <option value="អរិយក្សត្រ">អរិយក្សត្រ</option>
        <option value="បារុង">បារុង</option>
        <option value="បឹងគ្រំ">បឹងគ្រំ</option>
        <option value="កោះកែវ">កោះកែវ</option>
        <option value="កោះរះ">កោះរះ</option>
        <option value="ល្វាសរ">ល្វាសរ</option>
        <option value="ពាមឧកញ៉ាអុង">ពាមឧកញ៉ាអុង</option>
        <option value="ភូមិធំ">ភូមិធំ</option>
        <option value="ព្រែកក្មេង">ព្រែកក្មេង</option>
        <option value="ព្រែករៃ">ព្រែករៃ</option>
        <option value="ព្រែកឫស្សី">ព្រែកឫស្សី</option>
        <option value="សំបួរ">សំបួរ</option>
        <option value="សារិកាកែវ">សារិកាកែវ</option>
        <option value="ថ្មគរ">ថ្មគរ</option>
        <option value="ទឹកឃ្លាំង">ទឹកឃ្លាំង</option>
<!--        មុខកំពូល-->
        <option value="ព្រែកអញ្ចាញ">ព្រែកអញ្ចាញ</option>
        <option value="ព្រែកដំបង">ព្រែកដំបង</option>
        <option value="រកាកោង ទី ១">រកាកោង ទី ១</option>
        <option value="រកាកោង ទី ២">រកាកោង ទី ២</option>
        <option value="ឫស្សីជ្រោយ">ឫស្សីជ្រោយ</option>
        <option value="សំបួរមាស">សំបួរមាស</option>
        <option value="ស្វាយអំពារ">ស្វាយអំពារ</option>
<!--        អង្គស្នួល -->
        <option value="បែកចាន">បែកចាន</option>
        <option value="ឆក់ឈើនាង">ឆក់ឈើនាង</option>
        <option value="ដំណាក់អំពិល">ដំណាក់អំពិល</option>
        <option value="ក្រាំងម្កាក់">ក្រាំងម្កាក់</option>
        <option value="លំហាច">លំហាច</option>
        <option value="ម្កាក់">ម្កាក់</option>
        <option value="ពើក">ពើក</option>
        <option value="ព្រៃពួច">ព្រៃពួច</option>
        <option value="សំរោងលើ">សំរោងលើ</option>
        <option value="ទួលព្រេជ">ទួលព្រេជ</option>
<!--        ពញាឮ-->
        <option value="ឈ្វាំង">ឈ្វាំង</option>
        <option value="ជ្រៃលាស់">ជ្រៃលាស់</option>
        <option value="កំពង់ហ្លួង">កំពង់ហ្លួង</option>
        <option value="កំពង់អុស">កំពង់អុស</option>
        <option value="កោះចិន">កោះចិន</option>
        <option value="ភ្នំបាត">ភ្នំបាត</option>
        <option value="ពញាឮ">ពញាឮ</option>
        <option value="ព្រែកតាទែន">ព្រែកតាទែន</option>
        <option value="ផ្សារដែក">ផ្សារដែក</option>
        <option value="ទំនប់ធំ">ទំនប់ធំ</option>
        <option value="វិហារហ្លួង">វិហារហ្លួង</option>
<!--        ស្អាង-->
        <option value="ខ្ពប">ខ្ពប</option>
        <option value="កោះអន្លង់ចិន">កោះអន្លង់ចិន</option>
        <option value="កោះខែល">កោះខែល</option>
        <option value="កោះខ្សាច់ទន្លា">កោះខ្សាច់ទន្លា</option>
        <option value="ក្រាំងយ៉ូវ">ក្រាំងយ៉ូវ</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="ព្រែកអំបិល">ព្រែកអំបិល</option>
        <option value="ព្រែកគយ">ព្រែកគយ</option>
        <option value="រកាខ្ពស់">រកាខ្ពស់</option>
        <option value="ស្អាងភ្នំ">ស្អាងភ្នំ</option>
        <option value="សិត្បូ">សិត្បូ</option>
        <option value="ស្វាយប្រទាល">ស្វាយប្រទាល</option>
        <option value="ស្វាយរលំ">ស្វាយរលំ</option>
        <option value="តាលន់">តាលន់</option>
        <option value="ត្រើយស្លា">ត្រើយស្លា</option>
        <option value="ទឹកវិល">ទឹកវិល</option>
<!--        ក្រុងតាខ្មៅ-->
        <option value="តាក្ដុល">តាក្ដុល</option>
        <option value="ង្កាត់ព្រែកឫស្សី">ង្កាត់ព្រែកឫស្សី</option>
        <option value="ដើមមៀន">ដើមមៀន</option>
        <option value="តាខ្មៅ">តាខ្មៅ</option>
        <option value="ព្រែកហូរ">ព្រែកហូរ</option>
        <option value="កំពង់សំណាញ់">កំពង់សំណាញ់</option>
<!--    Takeo-->
<!--        អង្គរបូរី-->
        <option value="អង្គរបូរី">អង្គរបូរី</option>
        <option value="បាស្រែ">បាស្រែ</option>
        <option value="គោកធ្លក">គោកធ្លក</option>
        <option value="ពន្លៃ">ពន្លៃ</option>
        <option value="ព្រែកផ្ទោល">ព្រែកផ្ទោល</option>
        <option value="ព្រៃផ្គាំ">ព្រៃផ្គាំ</option>
<!--        បាទី-->
        <option value="ចំបក់">ចំបក់</option>
        <option value="ចំប៉ី">ចំប៉ី</option>
        <option value="ដូង">ដូង</option>
        <option value="កណ្ដឹង">កណ្ដឹង</option>
        <option value="កុមាររាជា">កុមាររាជា</option>
        <option value="ក្រាំងលាវ">ក្រាំងលាវ</option>
        <option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>
        <option value="លំពង់">លំពង់</option>
        <option value="ពារាម">ពារាម</option>
        <option value="ពត់សរ">ពត់សរ</option>
        <option value="សូរភី">សូរភី</option>
        <option value="តាំងដូង">តាំងដូង</option>
        <option value="ត្នោត">ត្នោត</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ត្រពាំងសាប">ត្រពាំងសាប</option>
<!--        បូរីជលសារ-->
        <option value="បូរីជលសារ">បូរីជលសារ</option>
        <option value="ជ័យជោគ">ជ័យជោគ</option>
        <option value="ដូងខ្ពស់">ដូងខ្ពស់</option>
        <option value="កំពង់ក្រសាំង">កំពង់ក្រសាំង</option>
        <option value="គោកពោធិ៍">គោកពោធិ៍</option>
<!--        គីរីវង់-->
        <option value="អង្គប្រាសាទ">អង្គប្រាសាទ</option>
        <option value="ព្រះបាទជាន់ជុំ">ព្រះបាទជាន់ជុំ</option>
        <option value="កំណប់">កំណប់</option>
        <option value="កំពែង">កំពែង</option>
        <option value="គីរីចុងកោះ">គីរីចុងកោះ</option>
        <option value="គោកព្រេច">គោកព្រេច</option>
        <option value="ភ្នំដិន">ភ្នំដិន</option>
        <option value="ព្រៃអំពក">ព្រៃអំពក</option>
        <option value="ព្រៃរំដេង">ព្រៃរំដេង</option>
        <option value="រាមអណ្ដើក">រាមអណ្ដើក</option>
        <option value="សោម">សោម</option>
        <option value="តាអូរ">តាអូរ</option>
<!--        កោះអណ្ដែត-->
        <option value="ក្រពុំឈូក">ក្រពុំឈូក</option>
        <option value="ពេជសារ">ពេជសារ</option>
        <option value="ព្រៃខ្លា">ព្រៃខ្លា</option>
        <option value="ព្រៃយុថ្កា">ព្រៃយុថ្កា</option>
        <option value="រមេញ">រមេញ</option>
        <option value="ធ្លាប្រជុំ">ធ្លាប្រជុំ</option>
<!--        ព្រៃកប្បាស-->
        <option value="អង្កាញ់">អង្កាញ់</option>
        <option value="បានកាម">បានកាម</option>
        <option value="ចំប៉ា">ចំប៉ា</option>
        <option value="ចារ">ចារ</option>
        <option value="កំពែង">កំពែង</option>
        <option value="កំពង់រាប">កំពង់រាប</option>
        <option value="ក្ដាញ់">ក្ដាញ់</option>
        <option value="ពោធិ៍រំចាក">ពោធិ៍រំចាក</option>
        <option value="ព្រៃកប្បាស">ព្រៃកប្បាស</option>
        <option value="ព្រៃល្វា">ព្រៃល្វា</option>
        <option value="ព្រៃផ្ដៅ">ព្រៃផ្ដៅ</option>
        <option value="ស្នោ">ស្នោ</option>
        <option value="តាំងយ៉ាប">តាំងយ៉ាប</option>
<!--        សំរោង-->
        <option value="បឹងត្រាញ់ខាងជើង">បឹងត្រាញ់ខាងជើង</option>
        <option value="បឹងត្រាញ់ខាងត្បូង">បឹងត្រាញ់ខាងត្បូង</option>
        <option value="ជើងគួន">ជើងគួន</option>
        <option value="ជំរះពេន">ជំរះពេន</option>
        <option value="ខ្វាវ">ខ្វាវ</option>
        <option value="លំចង់">លំចង់</option>
        <option value="រវៀង">រវៀង</option>
        <option value="សំរោង">សំរោង</option>
        <option value="សឹង្ហ">សឹង្ហ</option>
        <option value="ស្លា">ស្លា</option>
        <option value="ទ្រា">ទ្រា</option>
<!--        ក្រុងដូនកែវ -->
        <option value="បារាយណ៍">បារាយណ៍</option>
        <option value="រកាក្នុង">រកាក្នុង</option>
        <option value="រកាក្រៅ">រកាក្រៅ</option>
<!--        ត្រាំកក់ -->
        <option value="អង្គតាសោម">អង្គតាសោម</option>
        <option value="ជាងទង">ជាងទង</option>
        <option value="គុស">គុស</option>
        <option value="លាយបូរ">លាយបូរ</option>
        <option value="ញ៉ែងញ៉ង">ញ៉ែងញ៉ង</option>
        <option value="អូរសារាយ">អូរសារាយ</option>
        <option value="ត្រពាំងក្រញូង">ត្រពាំងក្រញូង</option>
        <option value="ឧត្ដមសុរិយា">ឧត្ដមសុរិយា</option>
        <option value="ពពេល">ពពេល</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ស្រែរនោង">ស្រែរនោង</option>
        <option value="តាភេម">តាភេម</option>
        <option value="ត្រាំកក់">ត្រាំកក់</option>
        <option value="ត្រពាំងធំខាងជើង">ត្រពាំងធំខាងជើង</option>
        <option value="ត្រពាំងធំខាងត្បូង">ត្រពាំងធំខាងត្បូង</option>
<!--        ទ្រាំង-->
        <option value="អង្កាញ់">អង្កាញ់</option>
        <option value="ទ្រាំង">ទ្រាំង</option>
        <option value="ជីខ្មា">ជីខ្មា</option>
        <option value="ខ្វាវ">ខ្វាវ</option>
        <option value="ប្រាំបីមុំ">ប្រាំបីមុំ</option>
        <option value="អង្គកែវ">អង្គកែវ</option>
        <option value="ព្រៃស្លឹក">ព្រៃស្លឹក</option>
        <option value="រនាម">រនាម</option>
        <option value="សំបួរ">សំបួរ</option>
        <option value="សន្លុង">សន្លុង</option>
        <option value="ស្មោង">ស្មោង</option>
        <option value="ស្រង៉ែ">ស្រង៉ែ</option>
        <option value="ធ្លក">ធ្លក</option>
        <option value="ត្រឡាច">ត្រឡាច</option>
<!--    Kampot-->
<!--        អង្គរជ័យ-->
        <option value="អង្គភ្នំតូច">អង្គភ្នំតូច</option>
        <option value="អង្គរជ័យ">អង្គរជ័យ</option>
        <option value="ចំប៉ី">ចំប៉ី</option>
        <option value="ដំបូកខ្ពស់">ដំបូកខ្ពស់</option>
        <option value="ដានគោម">ដានគោម</option>
        <option value="ដើមដូង">ដើមដូង</option>
        <option value="ម្រោម">ម្រោម</option>
        <option value="ភ្នំកុង">ភ្នំកុង</option>
        <option value="ប្រភ្នំ">ប្រភ្នំ</option>
        <option value="សំឡាញ">សំឡាញ</option>
        <option value="តានី">តានី</option>
<!--         បន្ទាយមាស-->
        <option value="បន្ទាយមាសខាងកើត">បន្ទាយមាសខាងកើត</option>
        <option value="បន្ទាយមាសខាងលិច">បន្ទាយមាសខាងលិច</option>
        <option value="ព្រៃទន្លេ">ព្រៃទន្លេ</option>
        <option value="សំរោងក្រោម">សំរោងក្រោម</option>
        <option value="សំរោងលើ">សំរោងលើ</option>
        <option value="ស្ដេចគង់ខាងជើង">ស្ដេចគង់ខាងជើង</option>
        <option value="ស្ដេចគង់ខាងលិច">ស្ដេចគង់ខាងលិច</option>
        <option value="ស្ដេចគង់ខាងត្បូង">ស្ដេចគង់ខាងត្បូង</option>
        <option value="ត្នោតចុងស្រង់">ត្នោតចុងស្រង់</option>
        <option value="ត្រពាំងសាលាខាងកើត">ត្រពាំងសាលាខាងកើត</option>
        <option value="ត្រពាំងសាលាខាងលិច">ត្រពាំងសាលាខាងលិច</option>
        <option value="ទូកមាសខាងកើត">ទូកមាសខាងកើត</option>
        <option value="ទូកមាសខាងលិច">ទូកមាសខាងលិច</option>
        <option value="វត្ដអង្គខាងជើង">វត្ដអង្គខាងជើង</option>
        <option value="វត្ដអង្គខាងត្បូង">វត្ដអង្គខាងត្បូង</option>
<!--        ឈូក-->
        <option value="បានៀវ">បានៀវ</option>
        <option value="តាកែន">តាកែន</option>
        <option value="បឹងនិមល">បឹងនិមល</option>
        <option value="ឈូក">ឈូក</option>
        <option value="ដូនយ៉យ">ដូនយ៉យ</option>
        <option value="ក្រាំងស្បូវ">ក្រាំងស្បូវ</option>
        <option value="ក្រាំងស្នាយ">ក្រាំងស្នាយ</option>
        <option value="ល្បើក">ល្បើក</option>
        <option value="ត្រពាំងភ្លាំង">ត្រពាំងភ្លាំង</option>
        <option value="មានជ័យ">មានជ័យ</option>
        <option value="នារាយណ៍">នារាយណ៍</option>
        <option value="សត្វពង">សត្វពង</option>
        <option value="ត្រពាំងបី">ត្រពាំងបី</option>
        <option value="ត្រមែង">ត្រមែង</option>
        <option value="តេជោអភិវឌ្ឍន៍">តេជោអភិវឌ្ឍន៍</option>
<!--        ជុំគិរី-->
        <option value="ច្រេស">ច្រេស</option>
        <option value="ជំពូវន្ដ">ជំពូវន្ដ</option>
        <option value="ស្នាយអញ្ជិត">ស្នាយអញ្ជិត</option>
        <option value="ស្រែចែង">ស្រែចែង</option>
        <option value="ស្រែក្នុង">ស្រែក្នុង</option>
        <option value="ស្រែសំរោង">ស្រែសំរោង</option>
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
<!--        ដងទង់-->
        <option value="ដំណាក់សុក្រំ">ដំណាក់សុក្រំ</option>
        <option value="ដងទង់">ដងទង់</option>
        <option value="ឃ្ជាយខាងជើង">ឃ្ជាយខាងជើង</option>
        <option value="ខ្ជាយខាងត្បូង">ខ្ជាយខាងត្បូង</option>
        <option value="មានរិទ្ធិ">មានរិទ្ធិ</option>
        <option value="ស្រែជាខាងជើង">ស្រែជាខាងជើង</option>
        <option value="ស្រែជាខាងត្បូង">ស្រែជាខាងត្បូង</option>
        <option value="ទទុង">ទទុង</option>
        <option value="អង្គ រមាស">អង្គ រមាស</option>
        <option value="ល្អាង">ល្អាង</option>
<!--        កំពង់ត្រាច-->
        <option value="បឹងសាលាខាងជើង">បឹងសាលាខាងជើង</option>
        <option value="បឹងសាលាខាងត្បូង">បឹងសាលាខាងត្បូង</option>
        <option value="ដំណាក់កន្ទួតខាងជើង">ដំណាក់កន្ទួតខាងជើង</option>
        <option value="ដំណាក់កន្ទួតខាងត្បូង">ដំណាក់កន្ទួតខាងត្បូង</option>
        <option value="កំពង់ត្រាចខាងកើត">កំពង់ត្រាចខាងកើត</option>
        <option value="កំពង់ត្រាចខាងលិច">កំពង់ត្រាចខាងលិច</option>
        <option value="ប្រាសាទភ្នំខ្យង">ប្រាសាទភ្នំខ្យង</option>
        <option value="ភ្នំប្រាសាទ">ភ្នំប្រាសាទ</option>
        <option value="អង្គសុរភី">អង្គសុរភី</option>
        <option value="ព្រែកក្រឹស">ព្រែកក្រឹស</option>  
        <option value="ឫស្សីខាងកើត">ឫស្សីខាងកើត</option>
        <option value="ឫស្សីខាងលិច">ឫស្សីខាងលិច</option>
        <option value="ស្វាយទងខាងជើង">ស្វាយទងខាងជើង</option>
        <option value="ស្វាយទងខាងត្បូង">ស្វាយទងខាងត្បូង</option>
<!--         ទឹកឈូ-->
        <option value="បឹងទូក">បឹងទូក</option>
        <option value="ជុំគ្រៀល">ជុំគ្រៀល</option>
        <option value="កំពង់ក្រែង">កំពង់ក្រែង</option>
        <option value="កំពង់សំរោង">កំពង់សំរោង</option>
        <option value="កណ្ដោល">កណ្ដោល</option>
        <option value="កោះតូច">កោះតូច</option>
        <option value="កូនសត្វ">កូនសត្វ</option>
        <option value="ម៉ាក់ប្រាង្គ">ម៉ាក់ប្រាង្គ</option>
        <option value="ព្រែកត្នោត">ព្រែកត្នោត</option>
        <option value="ព្រៃឃ្មុំ">ព្រៃឃ្មុំ</option>
        <option value="ព្រៃថ្នង">ព្រៃថ្នង</option>  
        <option value="ស្ទឹងកែវ">ស្ទឹងកែវ</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="ត្រពាំងសង្កែ">ត្រពាំងសង្កែ</option>
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
<!--        ក្រុងកំពត-->
        <option value="កំពង់កណ្ដាល">កំពង់កណ្ដាល</option>
        <option value="ក្រាំងអំពិល">ក្រាំងអំពិល</option>
        <option value="កំពង់បាយ">កំពង់បាយ</option>
        <option value="អណ្ដូងខ្មែរ">អណ្ដូងខ្មែរ</option>
        <option value="ត្រើយកោះ">ត្រើយកោះ</option>
<!--        បរសេដ្ឋ-->
        <option value="បរសេដ្ឋ">បរសេដ្ឋ</option>
        <option value="កាត់ភ្លុក">កាត់ភ្លុក</option>
        <option value="និទាន">និទាន</option>
        <option value="ភក្ដី">ភក្ដី</option>
        <option value="ភារីមានជ័យ">ភារីមានជ័យ</option>
        <option value="ផុង">ផុង</option>
        <option value="ពោធិអង្ក្រង">ពោធិអង្ក្រង</option>
        <option value="ពោធិ៍ចំរើន">ពោធិ៍ចំរើន</option>
        <option value="ពោធិ៍ម្រាល">ពោធិ៍ម្រាល</option>
        <option value="ស្វាយចចិប">ស្វាយចចិប</option>
        <option value="ទួលអំពិល">ទួលអំពិល</option>
        <option value="ទួលសាលា">ទួលសាលា</option>
        <option value="កក់">កក់</option>
        <option value="ស្វាយរំពារ">ស្វាយរំពារ</option>
        <option value="ព្រះខែ">ព្រះខែ</option>
<!--        ក្រុងច្បារមន-->
        <option value="ច្បារមន">ច្បារមន</option>
        <option value="កណ្ដោលដុំ">កណ្ដោលដុំ</option>
        <option value="រការធំ">រការធំ</option>
        <option value="សុព័រទេព">សុព័រទេព</option>
        <option value="ស្វាយក្រវ៉ាន់">ស្វាយក្រវ៉ាន់</option>
<!--        គងពិសី -->
        <option value="អង្គពពេល">អង្គពពេល</option>
        <option value="ជង្រុក">ជង្រុក</option>
        <option value="មហាឫស្សី">មហាឫស្សី</option>
        <option value="ពេជ្រមុនី">ពេជ្រមុនី</option>
        <option value="ព្រះនិព្វាន">ព្រះនិព្វាន</option>
        <option value="ព្រៃញាតិ">ព្រៃញាតិ</option>
        <option value="ព្រៃវិហារ">ព្រៃវិហារ</option>
        <option value="រកាកោះ">រកាកោះ</option>
        <option value="ស្ដុក">ស្ដុក</option>
        <option value="ស្នំក្រពើ">ស្នំក្រពើ</option>
        <option value="ស្រង់">ស្រង់</option>
        <option value="ទឹកល្អក់">ទឹកល្អក់</option>
        <option value="វាល">វាល</option>
<!--        ឱរ៉ាល់-->
        <option value="ហោងសំណំ">ហោងសំណំ</option>
        <option value="រស្មីសាមគ្គី">រស្មីសាមគ្គី</option>
        <option value="ត្រពាំងជោ">ត្រពាំងជោ</option>
        <option value="សង្កែសាទប">សង្កែសាទប</option>
        <option value="តាសាល">តាសាល</option>
<!--        ឧដុង្គ-->
        <option value="ចាន់សែន">ចាន់សែន</option>
        <option value="ជើងរាស់">ជើងរាស់</option>
        <option value="ជំពូព្រឹក្ស">ជំពូព្រឹក្ស</option>
        <option value="ក្សេមក្សាន្ដ">ក្សេមក្សាន្ដ</option>
        <option value="ក្រាំងចេក">ក្រាំងចេក</option>
        <option value="មានជ័យ">មានជ័យ</option>
        <option value="ព្រះស្រែ">ព្រះស្រែ</option>
        <option value="ព្រៃក្រសាំង">ព្រៃក្រសាំង</option>
        <option value="ត្រាចទង">ត្រាចទង</option>
        <option value="វាលពង់">វាលពង់</option>
        <option value="វាំងចាស់">វាំងចាស់</option>
        <option value="យុទ្ធសាមគ្">យុទ្ធសាមគ្</option>
        <option value="ដំណាក់រាំង">ដំណាក់រាំង</option>
        <option value="ពាំងល្វា">ពាំងល្វា</option>
        <option value="ភ្នំតូច">ភ្នំតូច</option>
<!--        ភ្នំស្រួច-->
        <option value="ចំបក់">ចំបក់</option>
        <option value="ជាំសង្កែ">ជាំសង្កែ</option>
        <option value="ដំបូករូង">ដំបូករូង</option>
        <option value="គិរីវន្ដ">គិរីវន្ដ</option>
        <option value="ក្រាំងដីវ៉ាយ">ក្រាំងដីវ៉ាយ</option>
        <option value="មហាសាំង">មហាសាំង</option>
        <option value="អូរ">អូរ</option>
        <option value="ព្រៃរំដួល">ព្រៃរំដួល</option>
        <option value="ព្រៃក្មេង">ព្រៃក្មេង</option>
        <option value="តាំងសំរោង">តាំងសំរោង</option>
        <option value="តាំងស្យា">តាំងស្យា</option>
        <option value="ត្រែងត្រយឹង">ត្រែងត្រយឹង</option>
<!--        សំរោងទង-->
        <option value="រលាំងចក">រលាំងចក</option>
        <option value="កាហែង">កាហែង</option>
        <option value="ខ្ទុំក្រាំង">ខ្ទុំក្រាំង</option>
        <option value="ក្រាំងអំពិល">ក្រាំងអំពិល</option>
        <option value="ព្នាយ">ព្នាយ</option>
        <option value="រលាំងគ្រើល">រលាំងគ្រើល</option>
        <option value="សំរោងទង">សំរោងទង</option>
        <option value="សំបូរ">សំបូរ</option>
        <option value="សែងដី">សែងដី</option>
        <option value="ស្គុះ">ស្គុះ</option>
        <option value="តាំងក្រូច">តាំងក្រូច</option>
        <option value="ធម្មតាអរ">ធម្មតាអរ</option>
        <option value="ត្រពាំងគង">ត្រពាំងគង</option>
        <option value="ទំព័រមាស">ទំព័រមាស</option>
        <option value="វល្លិសរ">វល្លិសរ</option>
<!--        ថ្ពង-->
        <option value="អមលាំង">អមលាំង</option>
        <option value="មនោរម្យ">មនោរម្យ</option>
        <option value="ប្រាំបីមុម">ប្រាំបីមុម</option>
        <option value="រុងរឿង">រុងរឿង</option>
        <option value="ទ័ពមាន">ទ័ពមាន</option>
        <option value="វាលពន់">វាលពន់</option>
        <option value="យាអង្គ">យាអង្គ</option>
        <option value="ផ្សេងៗ">ផ្សេងៗ</option>
</select>
</p>
<p​​ id="other_commune"></p>
</div>



<label class="control-label">ភូមិ</label>

<div class="controls">
<p id="iclient_village">
    <select class="span12 m-wrap chzn-select" name="client_village" id="client_village" onchange="myclient_village()"​> 

<!--            សង្កាត់ស្រះចក-->
  @if($data->client_village)
    <option value="{{ $data->client_village }}">{{ $data->client_village }}</option>
    <option>ជ្រើសរើរស​ ភូមិ</option>
  @else
    <option>ជ្រើសរើរស​ ភូមិ</option>
  @endif

        <option value="ភូមិ ១">ភូមិ ១</option>
        <option value="ភូមិ ២">ភូមិ ២</option>
        <option value="ភូមិ ៣">ភូមិ ៣</option>
        <option value="ភូមិ ៤">ភូមិ ៤</option>
        <option value="ភូមិ ៥">ភូមិ ៥</option>
        <option value="ភូមិ ៦">ភូមិ ៦</option>
        <option value="ភូមិ ៧">ភូមិ ៧</option>
        <option value="ភូមិ ៨">ភូមិ ៨</option>
        <option value="ភូមិ ៩">ភូមិ ៩</option>
        <option value="ភូមិ ១០">ភូមិ ១០</option>
        <option value="ភូមិ ១១">ភូមិ ១១</option>
        <option value="ភូមិ ១២">ភូមិ ១២</option>
        <option value="ភូមិ ១៣">ភូមិ ១៣</option>
        <option value="ភូមិ ១៤">ភូមិ ១៤</option>
        <option value="ភូមិ ១៥">ភូមិ ១៥</option>
        <option value="ភូមិ ១៦">ភូមិ ១៦</option>
        <option value="ភូមិ ១៧">ភូមិ ១៧</option>
        <option value="ភូមិ ១៨">ភូមិ ១៨</option>
        <option value="ភូមិ ១៩">ភូមិ ២០</option>
        <option value="ភូមិ ២១">ភូមិ ២១</option>
        <option value="ភូមិ ២២">ភូមិ ២២</option>
        <option value="ភូមិ ២៣">ភូមិ ២៣</option>
        <option value="ភូមិ ២៤">ភូមិ ២៤</option>

<!--             សង្កាត់ដង្កោ-->
        <option value="ថ្មី">ថ្មី</option>
        <option value="បាគូ">បាគូ</option>
        <option value="សំបួរ">សំបួរ</option>
        <option value="តាឡី">តាឡី</option>
        <option value="ម័ល">ម័ល</option>
        <option value="ខ្វា">ខ្វា</option>
<!--            សង្កាត់ពងទឹក-->
        <option value="ត្រពាំងទា">ត្រពាំងទា</option>
        <option value="ត្រពាំងគ">ត្រពាំងគ</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="វត្ដស្លែង">វត្ដស្លែង</option>
        <option value="ស្រែញ័រ">ស្រែញ័រ</option>
        <option value="ឃ្លះសណ្ដាយ">ឃ្លះសណ្ដាយ</option>
        <option value="ព្រៃកី ខ">ព្រៃកី ខ</option>
        <option value="ព្រៃកី ក">ព្រៃកី ក</option>
        <option value="ត្រាំងដោក">ត្រាំងដោក</option>
        <option value="ត្រពាំងសាលា">ត្រពាំងសាលា</option>
<!--            សង្កាត់ព្រៃវែង-->
        <option value="ព្រៃវែងខាងលិច">ព្រៃវែងខាងលិច</option>
        <option value="ព្រៃវែងខាងកើត">ព្រៃវែងខាងកើត</option>
        <option value="ត្រពាំងចក">ត្រពាំងចក</option>
        <option value="ត្រពាំងស្វាយ">ត្រពាំងស្វាយ</option>
        <option value="ទ័ពបោះ">ទ័ពបោះ</option>
        <option value="កំរៀង">កំរៀង</option>
        <option value="រោលជ្រូក">រោលជ្រូក</option>  
        <option value="សេរីដីដុះ">សេរីដីដុះ</option>
        <option value="ទួលសំបូរ">ទួលសំបូរ</option>
<!--            សង្កាត់ព្រៃស-->
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="ព្រៃសខាងកើត">ព្រៃសខាងកើត</option>
        <option value="ប្រការ">ប្រការ</option>
        <option value="ព្រៃសខាងលិច">ព្រៃសខាងលិច</option>
        <option value="ព្រៃទីទុយ">ព្រៃទីទុយ</option>
        <option value="អន្លង់គង">អន្លង់គង</option>
        <option value="គោកបន្ទាយ">គោកបន្ទាយ</option>
        <option value="ធម្មត្រ័យ">ធម្មត្រ័យ</option>
        <option value="ពាម">ពាម</option>
        <option value="ម្ភៃបួន">ម្ភៃបួន</option>
        <option value="អន្លង់គងថ្មី">អន្លង់គងថ្មី</option>
        <option value="ទួលរកាកុះ">ទួលរកាកុះ</option>
<!--            សង្កាត់ក្រាំងពង្រ-->
        <option value="ក្រាំងស្វាយ">ក្រាំងស្វាយ</option>
        <option value="ក្រាំងពង្រ">ក្រាំងពង្រ</option>
        <option value="ទឹកថ្លា">ទឹកថ្លា</option>
        <option value="ព្រៃសំព័រ">ព្រៃសំព័រ</option>
<!--            សង្កាត់ប្រទះឡាង-->
        <option value="ប្រទះឡាង"></option>
        <option value="ភា"></option>
        <option value="អង្គ"></option>
        <option value="តាំងរនាម"></option>
        <option value="គោកខ្សាច់"></option>
        <option value="គោកមាស"></option>
<!--            សង្កាត់សាក់សំពៅ-->
        <option value="ពោធី៍រលំ">ពោធី៍រលំ</option>
        <option value="ខ្វិត">ខ្វិត</option>
        <option value="ក្រាំងតាផូ">ក្រាំងតាផូ</option>
        <option value="កំរ៉ែង">កំរ៉ែង</option>
        <option value="សំបួរ">សំបួរ</option>
        <option value="សាក់សំពៅ">សាក់សំពៅ</option>
        <option value="ពារាម">ពារាម</option>
<!--            សង្កាត់ជើងឯក-->
        <option value="ជើងឯក">ជើងឯក</option>
        <option value="រលួស">រលួស</option>
        <option value="ស្រុកចេក">ស្រុកចេក</option>
        <option value="រែកប្រណាក">រែកប្រណាក</option>
        <option value="រែកថ្លឹង">រែកថ្លឹង</option>
        <option value="បូរីកម្មករ">បូរីកម្មករ</option>
<!--            សង្កាត់គងនយ-->
        <option value="គងនយ">គងនយ</option>
        <option value="វាលថ្លាន់">វាលថ្លាន់</option>
        <option value="សេរីសម្បត្ដិ">សេរីសម្បត្ដិ</option>
        <option value="ត្រពាំងសំរិត">ត្រពាំងសំរិត</option>
<!--            សង្កាត់ព្រែកកំពឹស-->
        <option value="រែកកំពឹស">រែកកំពឹស</option>
        <option value="រែករទាំង">រែករទាំង</option>
        <option value="ព្រែកថ្លឹង">ព្រែកថ្លឹង</option>
        <option value="ដំណាក់សង្កែ">ដំណាក់សង្កែ</option>
        <option value="ស្រីស្នំ">ស្រីស្នំ</option>
        <option value="ក្រាំងស្វាយ">ក្រាំងស្វាយ</option>
<!--            សង្កាត់រលួស-->
        <option value="ក្រពើទ្រោម">ក្រពើទ្រោម</option>
        <option value="ព្រះធាតុ">ព្រះធាតុ</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
<!--            សង្កាត់ស្ពានថ្ម-->
        <option value="អញ្ចាញ">អញ្ចាញ</option>
        <option value="គោកឪឡឹក">គោកឪឡឹក</option>
        <option value="ម៉ឺន្ដ្រា">ម៉ឺន្ដ្រា</option>
        <option value="ស្ពានថ្ម">ស្ពានថ្ម</option>
        <option value="ស្វាយមានល័ក្ខណ៍">ស្វាយមានល័ក្ខណ៍</option>
        <option value="ភូមិហា">ភូមិហា</option>
        <option value="ដូង">ដូង</option>
        <option value="ព្រែកជ្រៃ">ព្រែកជ្រៃ</option>
<!--            សង្កាត់ទៀន-->
        <option value="ក្រាំងក្រូច">ក្រាំងក្រូច</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ថ្ម">ថ្ម</option>
        <option value="កន្ទុយទឹក">កន្ទុយទឹក</option>
        <option value="សាលា">សាលា</option>
        <option value="ក្រាំង">ក្រាំង</option>
<!--        ខណ្ឌមានជ័យ-->
<!--            សង្កាត់ស្ទឹងមានជ័យ-->
        <option value="ឫស្សី">ឫស្សី</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ទ្រា">ទ្រា</option>
        <option value="មានជ័យ">មានជ័យ</option>
        <option value="ភ្នាត">ភ្នាត</option>
        <option value="ព្រែកទាល់">ព្រែកទាល់</option>
        <option value="ដំណាក់ធំ">ដំណាក់ធំ</option>
        <option value="ឫស្សី១">ឫស្សី១</option>
        <option value="ទ្រា១">ទ្រា១</option>
        <option value="ទ្រា២">ទ្រា២</option>
        <option value="ទ្រា៣">ទ្រា៣</option>
        <option value="ទ្រា៤">ទ្រា៤</option>
        <option value="មានជ័យ១">មានជ័យ១</option>
        <option value="ដំណាក់ធំ១">ដំណាក់ធំ១</option>
        <option value="ដំណាក់ធំ២">ដំណាក់ធំ២</option>
        <option value="ដំណាក់ធំ៣">ដំណាក់ធំ៣</option>
<!--            សង្កាត់បឹងទំពុន-->
        <option value="ចំរើនផល">ចំរើនផល</option>
        <option value="សន្សំកុសល ១">សន្សំកុសល ១</option>
        <option value="សន្សំកុសល ២">សន្សំកុសល ២</option>
        <option value="ក្បាលទំនប់">ក្បាលទំនប់</option>
        <option value="ត្នោតជ្រុំ">ត្នោតជ្រុំ</option>
        <option value="សន្សំកុសល ៣">សន្សំកុសល ៣</option>
        <option value="សន្សំកុសល ៤">សន្សំកុសល ៤</option>
        <option value="សន្សំកុសល ៥">សន្សំកុសល ៥</option>
        <option value="ចំរើនផល១">ចំរើនផល១</option>
        <option value="ក្បាលទំនប់១">ក្បាលទំនប់១</option>
        <option value="ក្បាលទំនប់២">ក្បាលទំនប់២</option>
        <option value="ត្នោតជ្រុំ១">ត្នោតជ្រុំ១</option>
        <option value="ត្នោតជ្រុំ២">ត្នោតជ្រុំ២</option>
        <option value="ត្នោតជ្រុំ៣">ត្នោតជ្រុំ៣</option>
        <option value="ត្នោតជ្រុំ៤">ត្នោតជ្រុំ៤</option>
<!--            សង្កាត់ចាក់អង្រែលើ-->
        <option value="ព្រែកតាគង់">ព្រែកតាគង់</option>
        <option value="ព្រែកតានូ">ព្រែកតានូ</option>
        <option value="ព្រែកតាគង់១">ព្រែកតាគង់១</option>
        <option value="ព្រែកតាគង់២">ព្រែកតាគង់២</option>
        <option value="ព្រែកតាគង់៣">ព្រែកតាគង់៣</option>
        <option value="ព្រែកតានូ១">ព្រែកតានូ១</option>
        <option value="ព្រែកតានូ២">ព្រែកតានូ២</option>
<!--            សង្កាត់ចាក់អង្រែក្រោម-->
        <option value="ទួលរកា">ទួលរកា</option>
        <option value="ព្រែកតាឡុង">ព្រែកតាឡុង</option>
        <option value="ទួលរកា១">ទួលរកា១</option>
        <option value="ទួលរកា២">ទួលរកា២</option>
        <option value="ទួលរកា៣">ទួលរកា៣</option>
        <option value="ព្រែកតាឡុង១">ព្រែកតាឡុង១</option>
        <option value="ព្រែកតាឡុង២">ព្រែកតាឡុង២</option>
        <option value="ព្រែកតាឡុង៣">ព្រែកតាឡុង៣</option>
<!--        ខណ្ឌឫស្សីកែវ-->
<!--            សង្កាត់ទួលសង្កែ-->
        <option value="ផ្សារតូច">ផ្សារតូច</option>
        <option value="ទួលសង្កែ">ទួលសង្កែ</option>
        <option value="ទួលគោក">ទួលគោក</option>
<!--            សង្កាត់ស្វាយប៉ាក-->
        <option value="ឡកំបោរ">ឡកំបោរ</option>
        <option value="លូ">លូ</option>
        <option value="ស្វាយប៉ាក">ស្វាយប៉ាក</option>
<!--            សង្កាត់គីឡូម៉ែត្រលេខ៦-->
        <option value="ក្រោលគោ">ក្រោលគោ</option>
        <option value="ស្ពានខ្ពស់">ស្ពានខ្ពស់</option>
        <option value="បឹងឈូក">បឹងឈូក</option>
<!--            សង្កាត់ឫស្សីកែវ-->
        <option value="មិត្ដភាព">មិត្ដភាព</option>
        <option value="សាមគ្គី">សាមគ្គី</option>
        <option value="ឃ្លាំងសាំង">ឃ្លាំងសាំង</option>
        <option value="បឹងសាឡាង">បឹងសាឡាង</option>
<!--            សង្កាត់ច្រាំងចំរេះទី ១-->
      <!--  <option value="ភូមិ ១">ភូមិ ១</option>
        <option value="ភូមិ ២">ភូមិ ២</option>
        <option value="ភូមិ ៣">ភូមិ ៣</option>
        <option value="ភូមិ ៤">ភូមិ ៤</option> -->
<!--            សង្កាត់ច្រាំងចំរេះទី ២-->
        <option value="ភូមិ ក">ភូមិ ក</option>
        <option value="ភូមិ ខ១">ភូមិ ខ១</option>
        <option value="ភូមិ ខ២">ភូមិ ខ២</option>
        <option value="ភូមិ ឃ">ភូមិ ឃ</option> 
<!--        ខណ្ឌសែនសុខ -->
<!--            សង្កាត់ភ្នំពេញថ្មី-->
        <option value="ច្រេស">ច្រេស</option>
        <option value="ពោងពាយ">ពោងពាយ</option>
        <option value="ដំណាក់">ដំណាក់</option>
        <option value="បាយ៉ាប">បាយ៉ាប</option>
        <option value="ភ្នំពេញថ្មី">ភ្នំពេញថ្មី</option>
        <option value="រោងចក្រ">រោងចក្រ</option>
        <option value="ទំនប់">ទំនប់</option>
        <option value="ដីថ្មី">ដីថ្មី</option>
        <option value="គោកឃ្លាង">គោកឃ្លាង</option>
        <option value="ត្រពាំងស្វាយ">ត្រពាំងស្វាយ</option>
<!--            សង្កាត់ទឹកថ្លា-->
        <option value="អូរបែកក្អម">អូរបែកក្អម</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ស្លែងរលើង">ស្លែងរលើង</option>
        <option value="ផ្សារទឹកថ្លា">ផ្សារទឹកថ្លា</option>
        <option value="សេប៉េសេ">សេប៉េសេ</option>
        <option value="បូរី១០០ខ្នង">បូរី១០០ខ្នង</option>
        <option value="ចុងថ្នល់ខាងកើត">ចុងថ្នល់ខាងកើត</option>
        <option value="ចុងថ្នល់ខាងលិច">ចុងថ្នល់ខាងលិច</option>
        <option value="ទឹកថ្លា">ទឹកថ្លា</option>
<!--            សង្កាត់ឃ្មួញ-->
        <option value="ឃ្មួញ">ឃ្មួញ</option>
        <option value="បន្លាស្អិត">បន្លាស្អិត</option>
        <option value="សំរោង">សំរោង</option>
        <option value="អន្លង់ក្ងាន">អន្លង់ក្ងាន</option>
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
        <option value="សែនសុខទី១">សែនសុខទី១</option>
        <option value="សែនសុខទី២">សែនសុខទី២</option>
        <option value="សែនសុខទី៣">សែនសុខទី៣</option>
        <option value="សែនសុខទី៤">សែនសុខទី៤</option>
        <option value="សែនសុខទី៥">សែនសុខទី៥</option>
        <option value="សែនសុខទី៦">សែនសុខទី៦</option>
        <option value="សែនសុខទី៧">សែនសុខទី៧</option>
        <option value="ត្រពាំងរាំងថ្មី">ត្រពាំងរាំងថ្មី</option>
<!--            សង្កាត់ក្រាំងធ្នង់-->
        <option value="ក្រាំងអង្ក្រង">ក្រាំងអង្ក្រង</option>
        <option value="ត្រពាំងមាន">ត្រពាំងមាន</option>
        <option value="ជាងទង">ជាងទង</option>
        <option value="ត្រពាំងជើងស្រុក">ត្រពាំងជើងស្រុក</option>
        <option value="ព្រៃខ្លា">ព្រៃខ្លា</option>
        <option value="សំរោងទាវ">សំរោងទាវ</option>
        <option value="ព្រៃមូល">ព្រៃមូល</option>
        <option value="វិមានទ្រង់">វិមានទ្រង់</option>
<!--        ខណ្ឌពោធិ៍សែនជ័យ-->
<!--            សង្កាត់ត្រពាំងក្រសាំង-->
        <option value="អក្សរ">អក្សរ</option>
        <option value="ក្រាំង">ក្រាំង</option>
        <option value="ត្រពាំងអណ្ដូង">ត្រពាំងអណ្ដូង</option>
        <option value="ព្រៃដូនអុក">ព្រៃដូនអុក</option>
        <option value="ត្រពាំងទា">ត្រពាំងទា</option>
        <option value="ត្រពាំងអញ្ចាញ">ត្រពាំងអញ្ចាញ</option>
        <option value="ជង្រុក">ជង្រុក</option>
        <option value="វាល">វាល</option>
        <option value="ខ្វែ">ខ្វែ</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="សាមគ្គី ១">សាមគ្គី ១</option>
        <option value="សាមគ្គី ២">សាមគ្គី ២</option>
        <option value="សាមគ្គី ៣">សាមគ្គី ៣</option>
<!--            សង្កាត់ភ្លើងឆេះរទេះ-->
        <option value="ទួលកី">ទួលកី</option>
        <option value="កប់ភ្លុក">កប់ភ្លុក</option>
        <option value="ព្រៃរងៀង">ព្រៃរងៀង</option>
        <option value="ត្រាញ់តាបាញ់">ត្រាញ់តាបាញ់</option>
        <option value="ភ្លើងឆេះរទេះលិច">ភ្លើងឆេះរទេះលិច</option>
        <option value="ភ្លើងឆេះរទេះកើត">ភ្លើងឆេះរទេះកើត</option>
        <option value="គោកខ្សាច់">គោកខ្សាច់</option>
        <option value="ព្រៃកី">ព្រៃកី</option>
<!--            សង្កាត់ចោមចៅ-->
        <option value="ត្រពាំងពោធិ៍">ត្រពាំងពោធិ៍</option>
        <option value="ដំណាក់ត្រយឹង">ដំណាក់ត្រយឹង</option>
        <option value="កប់គង">កប់គង</option>
        <option value="ព្រៃកំបុត">ព្រៃកំបុត</option>
        <option value="ព្រៃស្ពឺ">ព្រៃស្ពឺ</option>
        <option value="ថ្មគោល">ថ្មគោល</option>
        <option value="ព្រៃល្ង">ព្រៃល្ង</option>
        <option value="គោកចំបក់">គោកចំបក់</option>
        <option value="ព្រៃព្រីងខាងត្បូង">ព្រៃព្រីងខាងត្បូង</option>
        <option value="ព្រៃព្រីងខាងជើង">ព្រៃព្រីងខាងជើង</option>
        <option value="ទួលពង្រ">ទួលពង្រ</option>
        <option value="ចោមចៅ">ចោមចៅ</option>
        <option value="ថ្នល់បំបែក">ថ្នល់បំបែក</option>
        <option value="ព្រៃទា">ព្រៃទា</option>
        <option value="ត្រពាំងរំចេក">ត្រពាំងរំចេក</option>
        <option value="ក្រាំងដូនទៃ">ក្រាំងដូនទៃ</option>
        <option value="ស្រែជំរៅ">ស្រែជំរៅ</option>
        <option value="ជ្រៃកោង">ជ្រៃកោង</option>
        <option value="ព្រៃជីសាក់">ព្រៃជីសាក់</option>
        <option value="ជម្ពូវ័ន">ជម្ពូវ័ន</option>
        <option value="ត្រពាំងថ្លឹង">ត្រពាំងថ្លឹង</option>
        <option value="ល្វា">ល្វា</option>
        <option value="ព្រៃសណ្ដែក">ព្រៃសណ្ដែក</option>
        <option value="អង្គ">អង្គ</option>
        <option value="អូរ ដឹម">អូរ ដឹម</option>
        <option value="អង្គកែវ">អង្គកែវ</option>
        <option value="ព្រៃស្វាយ">ព្រៃស្វាយ</option>
<!--            សង្កាត់កាកាប-->
        <option value="ក្បាលដំរី">ក្បាលដំរី</option>
        <option value="ព្រៃសាលា">ព្រៃសាលា</option>
        <option value="កាកាប">កាកាប</option>
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="ចំការឪឡឹក">ចំការឪឡឹក</option>
        <option value="ត្រពាំងល្វា">ត្រពាំងល្វា</option>
        <option value="តាងួន">តាងួន</option>
        <option value="ប៉ប្រកខាងត្បូង">ប៉ប្រកខាងត្បូង</option>
        <option value="ប៉ប្រកខាងជើង">ប៉ប្រកខាងជើង</option>
<!--            សង្កាត់សំរោងក្រោម-->
        <option value="ចំការស្បែង">ចំការស្បែង</option>
        <option value="ត្រពាំងធ្នង់">ត្រពាំងធ្នង់</option>
        <option value="គោកព្រេជ">គោកព្រេជ</option>
        <option value="តិកូបញ្ញេ">តិកូបញ្ញេ</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ចែងម៉ែង">ចែងម៉ែង</option>
        <option value="ចាក់ជ្រូក">ចាក់ជ្រូក</option>
        <option value="អករំដួល">អករំដួល</option>
        <option value="ស្រែរាជ្ជៈ">ស្រែរាជ្ជៈ</option>
        <option value="អណ្ដូងតាអន">អណ្ដូងតាអន</option>
        <option value="ព្រៃពពេល">ព្រៃពពេល</option>
<!--            សង្កាត់បឹងធំ-->
        <option value="ត្មាតពង">ត្មាតពង</option>
        <option value="ព្រៃរមាសទី២">ព្រៃរមាសទី២</option>
        <option value="ព្រៃរមាសទី១">ព្រៃរមាសទី១</option>
        <option value="តាជេត">តាជេត</option>
        <option value="បឹងធំទី១">បឹងធំទី១</option>
        <option value="បឹងធំទី២">បឹងធំទី២</option>
        <option value="បឹងធំទី៣">បឹងធំទី៣</option>
        <option value="ដូនកុក">ដូនកុក</option>
        <option value="ខ្ចៅ">ខ្ចៅ</option>
        <option value="ក្ងោក">ក្ងោក</option>
        <option value="ទួលស្វាយ">ទួលស្វាយ</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ចំបក់ពណ្ណរាយ">ចំបក់ពណ្ណរាយ</option>
        <option value="ត្រពាំងត្រប់">ត្រពាំងត្រប់</option>
        <option value="អន្លុងស្វាយ">អន្លុងស្វាយ</option>
        <option value="ព្រៃតាកែ">ព្រៃតាកែ</option>
<!--             សង្កាត់កំបូល-->
        <option value="ផ្សារកំបូល">ផ្សារកំបូល</option>
        <option value="តាកេរិ៍្ដ">តាកេរិ៍្ដ</option>
        <option value="ត្រពាំងទួល">ត្រពាំងទួល</option>
        <option value="កំបូល">កំបូល</option>
        <option value="ទួលតាឡាត់">ទួលតាឡាត់</option>
        <option value="តាភេម">តាភេម</option>
        <option value="ព្រៃកុដិ">ព្រៃកុដិ</option>
        <option value="តាសេក">តាសេក</option>
        <option value="ទួលសាម៉">ទួលសាម៉</option>
        <option value="តានួន">តានួន</option>
        <option value="កប់អំបិល">កប់អំបិល</option>
        <option value="ស្នួលខ្ពស់">ស្នួលខ្ពស់</option>
        <option value="អំពិល">អំពិល</option>
        <option value="ត្រិប">ត្រិប</option>
        <option value="សាឡូង">សាឡូង</option>
        <option value="ជំទាវម៉ៅ">ជំទាវម៉ៅ</option>
        <option value="អង្គបឹងចក">អង្គបឹងចក</option>
        <option value="លិចវត្ដ">លិចវត្ដ</option>
<!--            សង្កាត់កន្ទោក-->
        <option value="ថ្មដា">ថ្មដា</option>
        <option value="ត្រពាំងគល់">ត្រពាំងគល់</option>
        <option value="ស្ងួនពេជ្រ">ស្ងួនពេជ្រ</option>
        <option value="កន្ទោកត្បូង">កន្ទោកត្បូង</option>
        <option value="គល់">គល់</option>
        <option value="អង្គត្រគៀត">អង្គត្រគៀត</option>
        <option value="កន្ទោកជើង">កន្ទោកជើង</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ស្រែក្នុង">ស្រែក្នុង</option>
        <option value="ព្រៃបឹង">ព្រៃបឹង</option>
        <option value="កំរៀង">កំរៀង</option>
        <option value="ដំបូកខ្ពស់">ដំបូកខ្ពស់</option>
        <option value="ត្រាំស្លឹក">ត្រាំស្លឹក</option>
<!--            សង្កាត់ឪឡោក-->
        <option value="ព្រៃតាប៉ុក">ព្រៃតាប៉ុក</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="អង្គស្លែង">អង្គស្លែង</option>
        <option value="ដូនរ័ត្ន">ដូនរ័ត្ន</option>
        <option value="ត្រពាំងអារ័ក្ស">ត្រពាំងអារ័ក្ស</option>
        <option value="ត្រពាំងពោធិ៍">ត្រពាំងពោធិ៍</option>
        <option value="បឹងគ្រើល">បឹងគ្រើល</option>
        <option value="ជ័យជំនះ">ជ័យជំនះ</option>
        <option value="បឹងកំបោរ">បឹងកំបោរ</option>
        <option value="ត្បែងមានជ័យ">ត្បែងមានជ័យ</option>
        <option value="ត្រង់នំ">ត្រង់នំ</option>
        <option value="ពង្រ">ពង្រ</option>
        <option value="ថ្ម">ថ្ម</option>
        <option value="សាមគ្គីប្រយុទ្ធ">សាមគ្គីប្រយុទ្ធ</option>
<!--            សង្កាត់ស្នោរ-->
        <option value="សាក់ប្រយុទ្ធ">សាក់ប្រយុទ្ធ</option>
        <option value="អង្គរជ័យ">អង្គរជ័យ</option>  
        <option value="ស្រែអំពិល">ស្រែអំពិល</option>
        <option value="តាអិន">តាអិន</option>
        <option value="កុលក្រស្នារ">កុលក្រស្នារ</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="ស្វាយ">ស្វាយ</option>
        <option value="ស្នោរកើត">ស្នោរកើត</option>
        <option value="តាពូង">តាពូង</option>
        <option value="ផ្លាំង">ផ្លាំង</option>
        <option value="ស្នោរលិច">ស្នោរលិច</option>
        <option value="ពង្រ">ពង្រ</option>
        <option value="ព្រៃរឹង">ព្រៃរឹង</option>
        <option value="រំដួលជើង">រំដួលជើង</option>
        <option value="រំដួលត្បូង">រំដួលត្បូង</option>
        <option value="ទួលលៀប">ទួលលៀប</option>
        <option value="ព្រៃក្រាយ">ព្រៃក្រាយ</option>  
        <option value="ព្រៃឈូក">ព្រៃឈូក</option>
<!--        ខណ្ឌជ្រោយចង្វារ-->
<!--            សង្កាត់ជ្រោយចង្វារ-->
        <option value="ភូមិ ១">ភូមិ ១</option>
        <option value="ភូមិ ២">ភូមិ ២</option>
        <option value="ភូមិ ៣">ភូមិ ៣</option>
        <option value="ដើមគរ">ដើមគរ</option>
        <option value="គៀនឃ្លាំង">គៀនឃ្លាំង</option>
<!--            សង្កាត់ព្រែកលៀប-->
        <option value="គៀនឃ្លាំង">គៀនឃ្លាំង</option>
        <option value="ព្រែកលៀប">ព្រែកលៀប</option>
        <option value="បាក់ខែង">បាក់ខែង</option>
        <option value="ខ្ទរ">ខ្ទរ</option>
<!--            សង្កាត់ព្រែកតាសេក-->
        <option value="ព្រែកតារ័ត្ន">ព្រែកតារ័ត្ន</option>
        <option value="ព្រែកតាគង់">ព្រែកតាគង់</option>
        <option value="ព្រែករាំង">ព្រែករាំង</option>
        <option value="ព្រែកតាសេក">ព្រែកតាសេក</option>
        <option value="ដើមគរ">ដើមគរ</option>
<!--            សង្កាត់កោះដាច់-->
        <option value="ចុងកោះ">ចុងកោះ</option>
        <option value="ល្វា">ល្វា</option>
        <option value="ក្បាលកោះ">ក្បាលកោះ</option>
        <option value="កោះដាច់">កោះដាច់</option>
        <option value="រនះ">រនះ</option>
<!--            សង្កាត់បាក់ខែង-->
        <option value="បាក់ខែងលើ">បាក់ខែងលើ</option>
        <option value="ក្ដីចាស់">ក្ដីចាស់</option>
        <option value="ចំបក់មាស">ចំបក់មាស</option>
<!--        ខណ្ឌព្រែកព្នៅ-->
<!--             សង្កាត់ព្រែកព្នៅ-->
        <option value="ដួង">ដួង</option>
        <option value="ពោធិ៍មង្គល">ពោធិ៍មង្គល</option>
        <option value="ព្រែកព្នៅ">ព្រែកព្នៅ</option>
        <option value="ផ្សារលិច">ផ្សារលិច</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
<!--            សង្កាត់ពញាពន់-->
        <option value="ធំត្បូង">ធំត្បូង</option>
        <option value="ធំជើង">ធំជើង</option>
        <option value="ត្រពាំងស្នោរ">ត្រពាំងស្នោរ</option>
        <option value="បឹង">បឹង</option>
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="ស្រែដូនតូច">ស្រែដូនតូច</option>
        <option value="ព្រៃពង្រ">ព្រៃពង្រ</option>
        <option value="វែង">វែង</option>
<!--            សង្កាត់សំរោង-->
        <option value="គ្រួស">គ្រួស</option>
        <option value="សំរោងជើង">សំរោងជើង</option>
        <option value="សំរោងកណ្ដាល">សំរោងកណ្ដាល</option>
        <option value="សំរោងត្បូង">សំរោងត្បូង</option>
<!--            សង្កាត់គោករកា-->
        <option value="កប់ស្រូវតូច">កប់ស្រូវតូច</option>
        <option value="កប់ស្រូវធំ">កប់ស្រូវធំ</option>
        <option value="ស្វាយចេក">ស្វាយចេក</option>
        <option value="បែកបក">បែកបក</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ត្រពាំងពោធិ៍">ត្រពាំងពោធិ៍</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="ពុទ្រា">ពុទ្រា</option>
        <option value="ជ្រេស">ជ្រេស</option>
        <option value="ខ្មែរលើ">ខ្មែរលើ</option>
        <option value="គោករកា">គោករកា</option>
        <option value="ធ្លក">ធ្លក</option>
        <option value="ជំរៅ">ជំរៅ</option>
        <option value="អណ្ដូង">អណ្ដូង</option>
        <option value="ត្រពាំងសំព័រ">ត្រពាំងសំព័រ</option>
        <option value="អង្គតាកូវ">អង្គតាកូវ</option>
        <option value="ភ្លូផ្អែម">ភ្លូផ្អែម</option>
        <option value="ទួលសំពៅ">ទួលសំពៅ</option>
<!--            សង្កាត់ពន្សាំង-->
        <option value="បួនមុខ">បួនមុខ</option>
        <option value="ត្រពាំងអំពិល">ត្រពាំងអំពិល</option>
        <option value="ស្វាយឧត្ដម">ស្វាយឧត្ដម</option>
        <option value="ថ្នល់បន្ទាយ">ថ្នល់បន្ទាយ</option>
        <option value="ត្រពាំងទទឹង">ត្រពាំងទទឹង</option>
        <option value="កាន់ទ្រង់">កាន់ទ្រង់</option>
        <option value="ត្រពាំងរនាម">ត្រពាំងរនាម</option>
        <option value="ដើមចាន់">ដើមចាន់</option>
        <option value="បឹងប៉ិច">បឹងប៉ិច</option>
        <option value="ឧសភា">ឧសភា</option>
        <option value="តាគល់">តាគល់</option>
        <option value="ព្រៃស្វាយ">ព្រៃស្វាយ</option>
        <option value="កោះរងៀង">កោះរងៀង</option>
        <option value="ទួល">ទួល</option>
        <option value="ត្នោតខ្ពស់">ត្នោតខ្ពស់</option>
        <option value="បឹងខ្នំ">បឹងខ្នំ</option>
        <option value="ចុងថ្នល់">ចុងថ្នល់</option>
        <option value="កន្លែងគល់">កន្លែងគល់</option>
        <option value="ចំបក់ធំ">ចំបក់ធំ</option>
        <option value="ត្រពាំងថ្លាន់">ត្រពាំងថ្លាន់</option>
        <option value="ត្រពាំងស្គន់">ត្រពាំងស្គន់</option>
        <option value="ព្រៃស្នួល">ព្រៃស្នួល</option>
        <option value="តាស្គរ">តាស្គរ</option>
        <option value="ទួលពន្សាំង">ទួលពន្សាំង</option>
<!--       ខណ្ឌច្បារអំពៅ -->
<!--            សង្កាត់ច្បារអំពៅទី ១-->
        <option value="ព្រែក">ព្រែក</option>
        <option value="ដើមម៉ាក់ក្លឿ">ដើមម៉ាក់ក្លឿ</option>
        <option value="ដើមអំពិល">ដើមអំពិល</option>
<!--            សង្កាត់ច្បារអំពៅទី ២-->
        <option value="ដើមចាន់">ដើមចាន់</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ដើមស្លែង">ដើមស្លែង</option>
        <option value="ដើមចាន់១">ដើមចាន់១</option>
        <option value="កណ្ដាល១">កណ្ដាល១</option>
        <option value="ដើមស្លែង១">ដើមស្លែង១</option>
<!--            សង្កាត់និរោធ-->
        <option value="តាងៅ">តាងៅ</option>
        <option value="បឹងឈូក">បឹងឈូក</option>
        <option value="ឫស្សីស្រស់">ឫស្សីស្រស់</option>
        <option value="កោះនរា">កោះនរា</option>
        <option value="តាងៅកណ្ដាល">តាងៅកណ្ដាល</option>
        <option value="តាងៅក្រោម">តាងៅក្រោម</option>
<!--            សង្កាត់ព្រែកប្រា-->
        <option value="ជ្រោយបាសាក់">ជ្រោយបាសាក់</option>
        <option value="ព្រះពន្លា">ព្រះពន្លា</option>
        <option value="ព្រែកតាពៅ">ព្រែកតាពៅ</option>
        <option value="អូរអណ្ដូង">អូរអណ្ដូង</option>
        <option value="ជ្រោយបាសាក់១">ជ្រោយបាសាក់១</option>
        <option value="អូរអណ្ដូង១">អូរអណ្ដូង១</option>
<!--            សង្កាត់វាលស្បូវ-->
        <option value="ក្ដីតាកុយ">ក្ដីតាកុយ</option>
        <option value="វាលស្បូវ">វាលស្បូវ</option>
        <option value="ស្វាយតាអុក">ស្វាយតាអុក</option>
        <option value="ព្រែកជាងព្រុំ">ព្រែកជាងព្រុំ</option>
        <option value="ស្វាយតាអុក ១">ស្វាយតាអុក ១</option>
<!--            សង្កាត់ព្រែកឯង-->
        <option value="ក្បាលជ្រោយ">ក្បាលជ្រោយ</option>
        <option value="តាព្រហ្ម">តាព្រហ្ម</option>
        <option value="មិត្ដភាព">មិត្ដភាព</option>
        <option value="ទួលតាចាន់">ទួលតាចាន់</option>
        <option value="ចុងព្រែក">ចុងព្រែក</option>
        <option value="របោះអង្កាញ់">របោះអង្កាញ់</option>
        <option value="មិត្ដភាព ១">មិត្ដភាព ១</option>
        <option value="ចុងព្រែក ១">ចុងព្រែក ១</option>
        <option value="របោះអង្កាញ់ ១">របោះអង្កាញ់ ១</option>
<!--            សង្កាត់ក្បាលកោះ-->
        <option value="ជ្រោយអំពិល">ជ្រោយអំពិល</option>
        <option value="យកបាត្រ">យកបាត្រ</option>
        <option value="ព្រែកធំ">ព្រែកធំ</option>
        <option value="ជ្រោយអំពិល ១">ជ្រោយអំពិល ១</option>
        <option value="ជ្រោយអំពិល ២">ជ្រោយអំពិល ២</option>
        <option value="យកបាត្រ ១">យកបាត្រ ១</option>
        <option value="ព្រែកធំ ១">ព្រែកធំ ១</option>
        <option value="ព្រែកធំ ២">ព្រែកធំ ២</option>
<!--            សង្កាត់ព្រែកថ្មី-->
        <option value="ចំពុះក្អែក">ចំពុះក្អែក</option>
        <option value="កោះក្របី">កោះក្របី</option>
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ចំពុះក្អែក ១">ចំពុះក្អែក ១</option>
        <option value="កោះក្របី ១">កោះក្របី ១</option>
        <option value="ព្រែកថ្មី ១">ព្រែកថ្មី ១</option>
<!--    ខេត្តកណ្ដាល-->
<!--        ស្រុកកណ្ដាលស្ទឹង-->
<!--            ឃុំអំពៅព្រៃ-->
        <option value="អំពៅព្រៃទី១">អំពៅព្រៃទី១</option>
        <option value="អំពៅព្រៃទី២">អំពៅព្រៃទី២</option>
        <option value="អំពៅព្រៃទី៣">អំពៅព្រៃទី៣</option>
        <option value="តាដោលទី១">តាដោលទី១</option>
        <option value="តាដោលទី២">តាដោលទី២</option>
        <option value="តាដោលទី៣">តាដោលទី៣</option>
        <option value="ជើងព្រៃទី១">ជើងព្រៃទី១</option>
        <option value="ជើងព្រៃទី២">ជើងព្រៃទី២</option>
        <option value="ជើងព្រៃទី៣">ជើងព្រៃទី៣</option>
<!--            ឃុំអន្លង់រមៀត-->
        <option value="អន្លង់រមៀតខាងជើង">អន្លង់រមៀតខាងជើង</option>
        <option value="អន្លង់រមៀតខាងត្បូង">អន្លង់រមៀតខាងត្បូង</option>
        <option value="អន្លង់រមៀតខាងលិច">អន្លង់រមៀតខាងលិច</option>
        <option value="ស្រែគោក">ស្រែគោក</option>
        <option value="ដើមត្រាំង">ដើមត្រាំង</option>
        <option value="កំពង់ទួល">កំពង់ទួល</option>
<!--            ឃុំបារគូ-->
        <option value="បារគូ">បារគូ</option>
        <option value="ឃ្មុត">ឃ្មុត</option>
        <option value="វាលកណ្ដាល">វាលកណ្ដាល</option>
        <option value="ពោធិ៍ដុះ">ពោធិ៍ដុះ</option>
        <option value="ត្បូងក្ដី">ត្បូងក្ដី</option>
        <option value="ស្វាយមីង">ស្វាយមីង</option>
        <option value="អូរអណ្ដូង">អូរអណ្ដូង</option>
<!--             ឃុំបឹងខ្យាង-->
        <option value="បឹងខ្យាង">បឹងខ្យាង</option>
        <option value="ព្រៃតាតូច">ព្រៃតាតូច</option>
        <option value="ស្រុកចេក">ស្រុកចេក</option>
        <option value="កំពង់តាឡុង">កំពង់តាឡុង</option>
        <option value="ប្រឡាយ">ប្រឡាយ</option>
        <option value="តាព្រហ្ម">តាព្រហ្ម</option>
<!--            ឃុំជើងកើប-->
        <option value="ក្រសាំង">ក្រសាំង</option>
        <option value="ស្រុកធំ">ស្រុកធំ</option>
        <option value="អំបឺស">អំបឺស</option>
        <option value="ពោធិ៍ស្មាត">ពោធិ៍ស្មាត</option>
        <option value="ឆ្មាពួន">ឆ្មាពួន</option>
        <option value="ប្រជុំអង្គ">ប្រជុំអង្គ</option>
<!--            ឃុំដើមឫស-->
        <option value="តាកុច">តាកុច</option>
        <option value="ស្លែងគង់">ស្លែងគង់</option>
        <option value="ដើមឫស">ដើមឫស</option>
        <option value="ទន្សាយកៀច">ទន្សាយកៀច</option>
        <option value="ក្រាំងចេក">ក្រាំងចេក</option>
        <option value="រលួស">រលួស</option>
        <option value="សាម៉រ">សាម៉រ</option>
        <option value="អន្លង់ព្រីង">អន្លង់ព្រីង</option>
        <option value="ទែន">ទែន</option>
        <option value="ក្រូចសើច">ក្រូចសើច</option>
        <option value="បទល្វា">បទល្វា</option>
        <option value="បាង">បាង</option>
        <option value="ព្រៃប្រាសាទ">ព្រៃប្រាសាទ</option>
        <option value="ម្កាក់">ម្កាក់</option>
<!--            ឃុំកណ្ដោក-->
        <option value="កណ្ដោក">កណ្ដោក</option>
        <option value="ទឹកនឹម">ទឹកនឹម</option>
        <option value="ស្វាយព្រៃ">ស្វាយព្រៃ</option>
        <option value="ជ្រៃរយោង">ជ្រៃរយោង</option>
        <option value="គោករមៀត">គោករមៀត</option>
        <option value="តោតម៉ា">តោតម៉ា</option>
        <option value="អំពៅព្រៃ">អំពៅព្រៃ</option>
<!--             ឃុំថ្មី-->
        <option value="ថ្មី">ថ្មី</option>
        <option value="ត្រពាំងចក">ត្រពាំងចក</option>
        <option value="ទួលកំរៀង">ទួលកំរៀង</option>
        <option value="ក្រាំងតី">ក្រាំងតី</option>
        <option value="ទន្លា">ទន្លា</option>
<!--            ឃុំគោកត្រប់-->
        <option value="ក្បាលសេះ">ក្បាលសេះ</option>
        <option value="ក្រាំងធ្មៃ">ក្រាំងធ្មៃ</option>
        <option value="ចារ">ចារ</option>
        <option value="គោកព្រីង">គោកព្រីង</option>
        <option value="គោកត្រប់">គោកត្រប់</option>
        <option value="លៀក">លៀក</option>
        <option value="ឈើនាង">ឈើនាង</option>
        <option value="ស្វាយកើត">ស្វាយកើត</option>
        <option value="ស្វាយលិច">ស្វាយលិច</option>
<!--            ឃុំព្រះពុទ្ធ-->
        <option value="ក្រាំងទ្រា">ក្រាំងទ្រា</option>
        <option value="បិនបោរ">បិនបោរ</option>
        <option value="ព្រះពុទ្ធ">ព្រះពុទ្ធ</option>
        <option value="ក្រាំងស្បូវ">ក្រាំងស្បូវ</option>
        <option value="បុណ្ណា">បុណ្ណា</option>
<!--             ឃុំព្រែករកា-->
        <option value="ចំបក់ត្រប់">ចំបក់ត្រប់</option>
        <option value="បឹងក្អែក">បឹងក្អែក</option>
        <option value="កោះខ្នុរ">កោះខ្នុរ</option>
        <option value="ព្រែករកា">ព្រែករកា</option>
<!--            ឃុំព្រែកស្លែង-->
        <option value="ព្រែកស្លែង">ព្រែកស្លែង</option>
        <option value="ពន់ចាន">ពន់ចាន</option>
        <option value="ព្រៃតាថុក">ព្រៃតាថុក</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
<!--             ឃុំរកា-->
        <option value="បឹង">បឹង</option>
        <option value="រកា">រកា</option>
        <option value="ស្វាយ">ស្វាយ</option>
        <option value="ចេក">ចេក</option>
        <option value="រុន">រុន</option>
        <option value="ក្រូច">ក្រូច</option>
        <option value="ត្រាញ់">ត្រាញ់</option>
<!--            ឃុំរលាំងកែន-->
        <option value="ភីរី">ភីរី</option>
        <option value="ចំការតាង៉ែត">ចំការតាង៉ែត</option>
        <option value="តាប៉េង">តាប៉េង</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ក្រាំងរលួស">ក្រាំងរលួស</option>
        <option value="ច្រឡាំង">ច្រឡាំង</option>
        <option value="អន្លង់បារាំង">អន្លង់បារាំង</option>
        <option value="អង្គ្រង">អង្គ្រង</option>
        <option value="ស្ទឹង">ស្ទឹង</option>
        <option value="ព្រៃកន្ទ្រា">ព្រៃកន្ទ្រា</option>
        <option value="រលាំងកែន">រលាំងកែន</option>
<!--            ឃុំសៀមរាប-->
        <option value="សៀមរាប">សៀមរាប</option>
        <option value="ចំបក់">ចំបក់</option>
        <option value="ព្រែកអង្គុញ">ព្រែកអង្គុញ</option>
        <option value="ជ័យជំនះ">ជ័យជំនះ</option>
        <option value="រានថ្ម">រានថ្ម</option>
        <option value="រាយដប">រាយដប</option>
<!--            ឃុំត្បែង-->
        <option value="អង្គក្លើ">អង្គក្លើ</option>
        <option value="កុកទិល">កុកទិល</option>
        <option value="ជ្រលង">ជ្រលង</option>
        <option value="ជីមៅ">ជីមៅ</option>
        <option value="ក្រាំងគាំ">ក្រាំងគាំ</option>
        <option value="ក្រាំងឈើនាង">ក្រាំងឈើនាង</option>
        <option value="កំណប់">កំណប់</option>
<!--            ឃុំត្រពាំងវែង-->
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="តំណាក់ត្របែក">តំណាក់ត្របែក</option>
        <option value="ត្រពាំងបារគូ">ត្រពាំងបារគូ</option>
        <option value="ស្លែង">ស្លែង</option>
        <option value="តាឡឹក">តាឡឹក</option>
<!--            ឃុំទ្រា-->
        <option value="ត្រស់">ត្រស់</option>
        <option value="ទ្រា">ទ្រា</option>
        <option value="រោងគោ">រោងគោ</option>
        <option value="មាត់បឹង">មាត់បឹង</option>
        <option value="កាប់លាវ">កាប់លាវ</option>
        <option value="ដូនវង្ស">ដូនវង្ស</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="ត្រពាំងស្វា">ត្រពាំងស្វា</option>
        <option value="ដំរីស្លាប់">ដំរីស្លាប់</option>
<!--        ស្រុកកៀនស្វាយ-->
<!--            ឃុំបន្ទាយដែក-->
        <option value="ខ្សុំ">ខ្សុំ</option>
        <option value="កណ្ដាលលើ">កណ្ដាលលើ</option>
        <option value="កណ្ដាលក្រោម">កណ្ដាលក្រោម</option>
        <option value="អង្គរជ័យ">អង្គរជ័យ</option>
        <option value="កណ្តាល">កណ្តាល</option>
        <option value="ព្រែកប៉ុល">ព្រែកប៉ុល</option>
<!--            ឃុំឈើទាល-->
        <option value="ឫស្សីស្រុក">ឫស្សីស្រុក</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="ព្រែកស្វាយ">ព្រែកស្វាយ</option>
        <option value="ស្រែអំពិល">ស្រែអំពិល</option>
        <option value="ឫស្សីស្រុក ២">ឫស្សីស្រុក ២</option>
        <option value="ឈើទាល ២">ឈើទាល ២</option>
        <option value="ព្រែកស្វាយ ២">ព្រែកស្វាយ ២</option>
        <option value="ស្រែអំពិល ២">ស្រែអំពិល ២</option>
<!--             ឃុំដីឥដ្ឋ-->
        <option value="ពពាលខែ">ពពាលខែ</option>
        <option value="ដីឥដ្ឋកោះផុស">ដីឥដ្ឋកោះផុស</option>
        <option value="ស្ដៅកន្លែង">ស្ដៅកន្លែង</option>
        <option value="ពពាលខែ ២">ពពាលខែ ២</option>
        <option value="ដីឥដ្ឋកោះផុស ២">ដីឥដ្ឋកោះផុស ២</option>
        <option value="ដីឥដ្ឋកោះផុស ៣">ដីឥដ្ឋកោះផុស ៣</option>
        <option value="តៅកន្លែង ២">តៅកន្លែង ២</option>
        <option value="តៅកន្លែង ៣">តៅកន្លែង ៣</option>
        <option value="តៅកន្លែង ៤">តៅកន្លែង ៤</option>
        <option value="តៅកន្លែង ៥">តៅកន្លែង ៥</option>
<!--            ឃុំកំពង់ស្វាយ-->
        <option value="ព្រែកដូង">ព្រែកដូង</option>
        <option value="កំពង់ស្វាយ">កំពង់ស្វាយ</option>
        <option value="ព្រែកតានប់">ព្រែកតានប់</option>
        <option value="កំពង់ស្វាយ ២">កំពង់ស្វាយ ២</option>
        <option value="ព្រែកដូង ២">ព្រែកដូង ២</option>
        <option value="ព្រែកតានប់ ២">ព្រែកតានប់ ២</option>
<!--             ឃុំគគីរ-->
        <option value="ទួលត្នោត">ទួលត្នោត</option>
        <option value="តារាបដូនស">តារាបដូនស</option>
        <option value="ស្លាបតាអោន">ស្លាបតាអោន</option>
        <option value="ចន្លក់">ចន្លក់</option>
        <option value="ទួលក្របៅ">ទួលក្របៅ</option>
        <option value="គគីរ">គគីរ</option>
        <option value="ដូនស">ដូនស</option>
        <option value="ទួលដូង">ទួលដូង</option>
        <option value="កោះបៀ">កោះបៀ</option>
        <option value="ចិនកោះ">ចិនកោះ</option>
        <option value="ជ្រោយតាប៉ា">ជ្រោយតាប៉ា</option>
        <option value="ចន្លក់ក្រៅ">ចន្លក់ក្រៅ</option>
<!--            ឃុំគគីរធំ-->
        <option value="ពោធិ៍មៀវ">ពោធិ៍មៀវ</option>
        <option value="គគីរធំ">គគីរធំ</option>
        <option value="រាំងដេក">រាំងដេក</option>
        <option value="កោះតេជោ">កោះតេជោ</option>
        <option value="គគីរធំ ២">គគីរធំ ២</option>
        <option value="រាំងដេក ២">រាំងដេក ២</option>
        <option value="កោះតេជោ ២">កោះតេជោ ២</option>
<!--            ឃុំភូមិធំ-->
        <option value="កោះប្រាក់">កោះប្រាក់</option>
        <option value="ភូមិធំ">ភូមិធំ</option>
        <option value="រទាំង">រទាំង</option>
        <option value="កោះប្រាក់ចាស់">កោះប្រាក់ចាស់</option>
        <option value="រទាំងត្បូង">រទាំងត្បូង</option>
<!--            ឃុំសំរោងធំ-->
        <option value="ជ័យឧត្ដម">ជ័យឧត្ដម</option>
        <option value="ព្រែកតាកែវ">ព្រែកតាកែវ</option>
        <option value="ជ្រោយដង">ជ្រោយដង</option>
        <option value="ស្ទឹង">ស្ទឹង</option>
        <option value="ព្រែកត្រែង">ព្រែកត្រែង</option>
        <option value="សំរោងក្អែរ">សំរោងក្អែរ</option>
        <option value="ជ័យឧត្ដម ២">ជ័យឧត្ដម ២</option>
        <option value="ព្រែកតាកែវ ២">ព្រែកតាកែវ ២</option>
        <option value="ជ្រោយដង ២">ជ្រោយដង ២</option>
        <option value="ស្ទឹង ២">ស្ទឹង ២</option>
        <option value="ព្រែកត្រែង ២">ព្រែកត្រែង ២</option>
        <option value="សំរោងក្អែរ ២">សំរោងក្អែរ ២</option>
        <option value="សំរោងក្អែរ ៣">សំរោងក្អែរ ៣</option>
<!--        ស្រុកខ្សាច់កណ្ដាល-->
<!--            ឃុំបាក់ដាវ-->
        <option value="ជ្រោយខ្សាច់">ជ្រោយខ្សាច់</option>
        <option value="បាក់ដាវលើ">បាក់ដាវលើ</option>
        <option value="បាក់ដាវក្រោម">បាក់ដាវក្រោម</option>
        <option value="ព្រែកជ្រូក">ព្រែកជ្រូក</option>
<!--            ឃុំជ័យធំ-->
        <option value="ជ័យធំ">ជ័យធំ</option>
        <option value="ជ័យតូច">ជ័យតូច</option>
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ជ្រៃលាស់">ជ្រៃលាស់</option>
        <option value="តាគាត់លិច">តាគាត់លិច</option>
        <option value="តាគាត់កើត">តាគាត់កើត</option>
<!--            ឃុំកំពង់ចំលង-->
        <option value="កំពង់ចំលង">កំពង់ចំលង</option>
        <option value="ត្បូងដំរី">ត្បូងដំរី</option>
        <option value="ព្រែកដំបង">ព្រែកដំបង</option>
<!--            ឃុំកោះចូរ៉ាម-->
        <option value="ត្បូង">ត្បូង</option>
        <option value="ក្រោម">ក្រោម</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="លើ">លើ</option>
<!--            ឃុំកោះឧកញ៉ាតី-->
        <option value="កោះតូច">កោះតូច</option>
        <option value="ក្បាលកោះ">ក្បាលកោះ</option>
        <option value="កណ្ដាលកោះ">កណ្ដាលកោះ</option>
        <option value="ចុងកោះ">ចុងកោះ</option>  
        <option value="ខ្ពប">ខ្ពប</option>
<!--            ឃុំព្រះប្រសប់-->
        <option value="ព្រះប្រសប់">ព្រះប្រសប់</option>
        <option value="ព្រែកតាបែន">ព្រែកតាបែន</option>
        <option value="ព្រែកតាទន់">ព្រែកតាទន់</option>
        <option value="ទេពមន្ដ្រី">ទេពមន្ដ្រី</option>
<!--             ឃុំព្រែកអំពិល-->
        <option value="ឃ្លាំងមឿងជើង">ឃ្លាំងមឿងជើង</option>
        <option value="ឃ្លាំងមឿងត្បូង">ឃ្លាំងមឿងត្បូង</option>
        <option value="ព្រែកក្របៅ ទី១">ព្រែកក្របៅ ទី១</option>
        <option value="ព្រែកក្របៅ ទី២">ព្រែកក្របៅ ទី២</option>
        <option value="ព្រែកក្របៅ ទី៣">ព្រែកក្របៅ ទី៣</option>
        <option value="ព្រែកដូនហែម">ព្រែកដូនហែម</option>
        <option value="ព្រែកអំពិល">ព្រែកអំពិល</option>
<!--             ឃុំព្រែកលួង-->
        <option value="ក្ដីចាស់">ក្ដីចាស់</option>
        <option value="ព្រែកតាទេព">ព្រែកតាទេព</option>
        <option value="ព្រែកលួង">ព្រែកលួង</option>
        <option value="ព្រែកថោង">ព្រែកថោង</option>
<!--            ឃុំព្រែកតាកូវ-->
        <option value="ព្រែកតាកូវ">ព្រែកតាកូវ</option>
        <option value="ព្រែកល្វា">ព្រែកល្វា</option>
        <option value="ព្រែកបង្កង">ព្រែកបង្កង</option>
<!--            ឃុំព្រែកតាមាក់-->
        <option value="ព្រែកតាគង់">ព្រែកតាគង់</option>
        <option value="កំពង់ដំរី">កំពង់ដំរី</option>
        <option value="ក្នុង">ក្នុង</option>
        <option value="ព្រែកតាមាក់">ព្រែកតាមាក់</option>
        <option value="ស្វាយអាត់លើ">ស្វាយអាត់លើ</option>
        <option value="ស្វាយអាត់កណ្ដាល">ស្វាយអាត់កណ្ដាល</option>
        <option value="ស្វាយអាត់ក្រោម">ស្វាយអាត់ក្រោម</option>
        <option value="បឹងក្រចាប់ជើង">បឹងក្រចាប់ជើង</option>
        <option value="បឹងក្រចាប់ត្បូង">បឹងក្រចាប់ត្បូង</option>
        <option value="អន្លង់">អន្លង់</option>
<!--            ឃុំពុកឫស្សី-->
        <option value="ក្រូចសើច">ក្រូចសើច</option>
        <option value="អញ្ជែងលើ">អញ្ជែងលើ</option>
        <option value="អញ្ជែងក្រោម">អញ្ជែងក្រោម</option>
        <option value="ពុកឫស្សីលើ">ពុកឫស្សីលើ</option>
        <option value="ពុកឫស្សីកណ្ដាល">ពុកឫស្សីកណ្ដាល</option>
        <option value="ពុកឫស្សីក្រោម">ពុកឫស្សីក្រោម</option>
<!--            ឃុំរកាជន្លឹង-->
        <option value="ជន្លឹង">ជន្លឹង</option>
        <option value="រកា ទី២">រកា ទី២</option>
        <option value="តាំងឫស្សី">តាំងឫស្សី</option>
        <option value="ទ្រា">ទ្រា</option>
        <option value="រកា ទី១">រកា ទី១</option>
<!--            ឃុំសន្លុង-->
        <option value="សន្លុង">សន្លុង</option>
        <option value="ឈូក">ឈូក</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="ដុល">ដុល</option>
<!--            ឃុំស៊ីធរ-->
        <option value="ព្រៃបាំង">ព្រៃបាំង</option>
        <option value="ស៊ីធរលិច">ស៊ីធរលិច</option>
        <option value="ស៊ីធរកើត">ស៊ីធរកើត</option>
        <option value="ទួលពង្រ">ទួលពង្រ</option>
        <option value="ម៉ែបាន">ម៉ែបាន</option>
        <option value="កំពង់ល្វា">កំពង់ល្វា</option>
<!--            ឃុំស្វាយជ្រំ-->
        <option value="លើ">លើ</option>
        <option value="ស្វាយជ្រុំ">ស្វាយជ្រុំ</option>
        <option value="បារាជ">បារាជ</option>
<!--            ឃុំស្វាយរមៀត-->
        <option value="ព្រែកតាបែន">ព្រែកតាបែន</option>
        <option value="ស្លា">ស្លា</option>
        <option value="ស្វាយរមៀត">ស្វាយរមៀត</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ស្វាយដំណាក់">ស្វាយដំណាក់</option>
<!--            ឃុំតាឯក-->
        <option value="តាឯក">តាឯក</option>
        <option value="ទ្រាំងក្រោម">ទ្រាំងក្រោម</option>
        <option value="ទ្រាំងលើ">ទ្រាំងលើ</option>
<!--            ឃុំវិហារសួគ៌-->
        <option value="ព្រែកចាស់">ព្រែកចាស់</option>
        <option value="សេដា">សេដា</option>
        <option value="វិហារសួគ៌ជើង">វិហារសួគ៌ជើង</option>
        <option value="វិហារសួគ៌ត្បូង">វិហារសួគ៌ត្បូង</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="តាកែវទី២">តាកែវទី២</option>
        <option value="តាកែវទី១">តាកែវទី១</option>
        <option value="ស្វាយមាស">ស្វាយមាស</option>
<!--         ស្រុកកោះធំ -->
<!--             ឃុំឈើខ្មៅ-->
        <option value="ក្បាលកោះ">ក្បាលកោះ</option>
        <option value="ឈើខ្មៅ">ឈើខ្មៅ</option>
        <option value="ចុងកោះ">ចុងកោះ</option>
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="ត្រើយកោះ">ត្រើយកោះ</option>
        <option value="ក្បាលជ្រោយ">ក្បាលជ្រោយ</option>
        <option value="កោះតូច">កោះតូច</option>
        <option value="ចុងខ្សាច់">ចុងខ្សាច់</option>
        <option value="ធំ">ធំ</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ទួលស្វាយ">ទួលស្វាយ</option>
<!--            ឃុំជ្រោយតាកែវ-->
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ព្រែកត្រឡាច">ព្រែកត្រឡាច</option>
        <option value="កំពង់ដ">កំពង់ដ</option>
        <option value="ជ្រោយតាកែវ">ជ្រោយតាកែវ</option>
        <option value="ព្រែកតាឃិន">ព្រែកតាឃិន</option>
        <option value="ព្រែកតាទៀង">ព្រែកតាទៀង</option>
        <option value="ព្រែកតាសេក">ព្រែកតាសេក</option>
        <option value="ដើមពោធិ៍">ដើមពោធិ៍</option>
        <option value="ព្រែកភូមិ">ព្រែកភូមិ</option>
<!--            ឃុំកំពង់កុង-->
        <option value="ក្បាលដំរីលើ">ក្បាលដំរីលើ</option>
        <option value="ក្បាលដំរីក្រោម">ក្បាលដំរីក្រោម</option>
        <option value="ព្រែកផ្អាវ">ព្រែកផ្អាវ</option>
        <option value="កំពង់កុង">កំពង់កុង</option>
        <option value="ជ្រុងរមាស">ជ្រុងរមាស</option>
        <option value="ព្រែកហង់">ព្រែកហង់</option>
        <option value="ទួលសង្កែ">ទួលសង្កែ</option>
        <option value="ត្របែកពក">ត្របែកពក</option>
        <option value="ព្រែកឫស្សី">ព្រែកឫស្សី</option>
        <option value="ល្វាទោង">ល្វាទោង</option>
        <option value="ទួលដូនគាំ">ទួលដូនគាំ</option>
        <option value="ទួលស្វាយ">ទួលស្វាយ</option>
<!--            ឃុំកោះធំ ‹ក›-->
        <option value="ក្បាលកោះធំ">ក្បាលកោះធំ</option>
        <option value="កណ្ដាលកោះធំ">កណ្ដាលកោះធំ</option>
        <option value="ចុងកោះធំ">ចុងកោះធំ</option>
        <option value="ពោធិ៍ទន្លេ">ពោធិ៍ទន្លេ</option>
        <option value="ក្បាលកោះថ្មី">ក្បាលកោះថ្មី</option>
        <option value="ចុងកោះថ្មី">ចុងកោះថ្មី</option>
        <option value="ទួលកន្ទួត">ទួលកន្ទួត</option>
<!--            ឃុំកោះធំ ‹ខ›-->
        <option value="សំប៉ាន">សំប៉ាន</option>
        <option value="ព្រែកតាកេរ">ព្រែកតាកេរ</option>
        <option value="ព្រែកសំរោង">ព្រែកសំរោង</option>
        <option value="ព្រែកបិ">ព្រែកបិ</option>
        <option value="ស្វាយតាមេឃ">ស្វាយតាមេឃ</option>
<!--            ឃុំលើកដែក-->
        <option value="ចំការដូង">ចំការដូង</option>
        <option value="លើកដែក">លើកដែក</option>
        <option value="ព្រែកអណ្ដូង">ព្រែកអណ្ដូង</option>
        <option value="ពោធិមិត្ដ">ពោធិមិត្ដ</option>
        <option value="អន្លង់ស្លាត">អន្លង់ស្លាត</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ទួលស្លែង">ទួលស្លែង</option>
        <option value="ពាមផ្ទោលកើត">ពាមផ្ទោលកើត</option>
        <option value="ពាមផ្ទោលលិច">ពាមផ្ទោលលិច</option>
        <option value="ឃ្លាំងកើត">ឃ្លាំងកើត</option>
        <option value="ឃ្លាំងលិច">ឃ្លាំងលិច</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ទួលចាន់">ទួលចាន់</option>
        <option value="ត្រឡោកបែក">ត្រឡោកបែក</option>
        <option value="ព្រែកខ្នាច">ព្រែកខ្នាច</option>
<!--            ឃុំពោធិ៍បាន-->
        <option value="ព្រែកអញ្ចាញ">ព្រែកអញ្ចាញ</option>
        <option value="ខ្វែងអណ្ដូង">ខ្វែងអណ្ដូង</option>
        <option value="ក្បាលជ្រោយ">ក្បាលជ្រោយ</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ព្រែកតាអ៊ិន">ព្រែកតាអ៊ិន</option>
        <option value="កំពង់គរ">កំពង់គរ</option>
        <option value="ព្រែកតាដុល">ព្រែកតាដុល</option>
        <option value="ព្រែកតារ័ត្ន">ព្រែកតារ័ត្ន</option>
        <option value="ពោធិបាន">ពោធិបាន</option>
<!--            ឃុំព្រែកជ្រៃ-->
        <option value="ជ្រោយស្នោ">ជ្រោយស្នោ</option>
        <option value="ព្រែកជ្រៃ">ព្រែកជ្រៃ</option>
        <option value="ប៉ាកណាម">ប៉ាកណាម</option>
        <option value="ខ្នារតាំងយូ">ខ្នារតាំងយូ</option>
        <option value="ព្រែកជ្រៃក្រៅ">ព្រែកជ្រៃក្រៅ</option>
        <option value="ខ្នាតាំងយូរត្បូង">ខ្នាតាំងយូរត្បូង</option>
        <option value="ព្រែកទន្លា">ព្រែកទន្លា</option>
<!--            ឃុំព្រែកស្ដី-->
        <option value="ព្រែកមេស្រុក">ព្រែកមេស្រុក</option>
        <option value="ព្រែកលោក">ព្រែកលោក</option>
        <option value="ព្រែកប៉ុក">ព្រែកប៉ុក</option>
        <option value="អន្លង់សាន្ដ">អន្លង់សាន្ដ</option>
        <option value="ចុងព្រែក">ចុងព្រែក</option>
        <option value="ព្រែកតាមេម">ព្រែកតាមេម</option>
        <option value="កោះចាស់">កោះចាស់</option>
        <option value="ពោធិ៍រាម្មា">ពោធិ៍រាម្មា</option>
        <option value="ប្រធាតុ">ប្រធាតុ</option>
        <option value="ព្រែកស្តី">ព្រែកស្តី</option>
        <option value="ចាស់">ចាស់</option>
        <option value="ឡក្បឿង">ឡក្បឿង</option>
        <option value="ពោធិ៍រាម្មា ក">ពោធិ៍រាម្មា ក</option>
        <option value="ពោធិ៍អណ្តែត">ពោធិ៍អណ្តែត</option>
        <option value="ប្រធាតុ ក">ប្រធាតុ ក</option>
<!--            ឃុំព្រែកថ្មី-->
        <option value="ព្រែកតាដួង">ព្រែកតាដួង</option>
        <option value="ព្រែកយាយហាយ">ព្រែកយាយហាយ</option>
        <option value="កំពង់សំបួរលើ">កំពង់សំបួរលើ</option>
        <option value="កំពង់សំបួរក្រោម">កំពង់សំបួរក្រោម</option>
        <option value="កំពង់ស្វាយលើ">កំពង់ស្វាយលើ</option>
        <option value="កំពង់ស្វាយកណ្ដាល">កំពង់ស្វាយកណ្ដាល</option>
        <option value="កំពង់ស្វាយក្រោម">កំពង់ស្វាយក្រោម</option>
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ចាមលើ">ចាមលើ</option>
        <option value="ព្រែកតាហ៊ីង">ព្រែកតាហ៊ីង</option>
        <option value="ព្រេកធន់">ព្រេកធន់</option>
        <option value="ចាមក្រោម"></option>
<!--            ឃុំសំពៅពូន-->
        <option value="ក្បាលកោះទៀវ">ក្បាលកោះទៀវ</option>
        <option value="កោះទៀវ ក">កោះទៀវ ក</option>
        <option value="កោះទៀវ ខ">កោះទៀវ ខ</option>
        <option value="ខ្ពប">ខ្ពប</option>
        <option value="កំពង់ថ្កុល">កំពង់ថ្កុល</option>
        <option value="កប៉ាល់គឿង">កប៉ាល់គឿង</option>
        <option value="ព្រែកស៊ឹង">ព្រែកស៊ឹង</option>
        <option value="ជ្រៃធំ">ជ្រៃធំ</option>
        <option value="ព្រែកស្បូវ">ព្រែកស្បូវ</option>
        <option value="ព្រែកគង់">ព្រែកគង់</option>
        <option value="ព្រែកសែម">ព្រែកសែម</option>
<!--         ស្រុកលើកដែក-->
<!--             ឃុំកំពង់ភ្នំ-->
        <option value="ក្បាលជ្រោយ">ក្បាលជ្រោយ</option>
        <option value="កំពង់ពោធិ៍">កំពង់ពោធិ៍</option>
        <option value="អំពិលទឹក">អំពិលទឹក</option>
        <option value="កោះចំរើន">កោះចំរើន</option>
<!--            ឃុំក្អមសំណរ-->
        <option value="ក្អមសំណក្រោម">ក្អមសំណក្រោម</option>
        <option value="ក្អមសំណលើ">ក្អមសំណលើ</option>
        <option value="រាំងជួរ">រាំងជួរ</option>
<!--            ឃុំខ្ពបអាទាវ-->
        <option value="បឹងកណ្ដាល">បឹងកណ្ដាល</option>
        <option value="បឹងក្រោម">បឹងក្រោម</option>
        <option value="បឹងលើ">បឹងលើ</option>
<!--            ឃុំពាមរាំង-->
        <option value="ពាមរាំងក្រោម">ពាមរាំងក្រោម</option>
        <option value="ពាមរាំងលើ">ពាមរាំងលើ</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ព្រែកតាទួន">ព្រែកតាទួន</option>
<!--            ឃុំព្រែកដាច់-->
        <option value="កោះកន្ធាយ">កោះកន្ធាយ</option>
        <option value="ព្រែកដាច់">ព្រែកដាច់</option>
        <option value="ព្រែកតូច">ព្រែកតូច</option>
        <option value="តាហ៊ីង">តាហ៊ីង</option>
<!--            ឃុំព្រែកទន្លាប់-->
        <option value="កំពង់ចំលង">កំពង់ចំលង</option>
        <option value="ព្រែកបាក់">ព្រែកបាក់</option>
        <option value="ព្រែកតូច">ព្រែកតូច</option>
        <option value="ស្ពានដែក">ស្ពានដែក</option>
<!--             ឃុំសណ្ដារ-->
        <option value="ចុងកោះ">ចុងកោះ</option>
        <option value="ដងក្ដោង">ដងក្ដោង</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
<!--        ស្រុកល្វាឯម-->
<!--            ឃុំអរិយក្សត្រ-->
        <option value="អរិយក្សត្រ">អរិយក្សត្រ</option>
        <option value="ខ្សាច់">ខ្សាច់</option>
        <option value="ពោធិ៍ធំ">ពោធិ៍ធំ</option>
        <option value="ទួលមាស">ទួលមាស</option>
<!--            ឃុំបារុង-->
        <option value="ខ្នោការ">ខ្នោការ</option>
        <option value="បារុង">បារុង</option>
<!--             ឃុំបឹងគ្រំ-->
        <option value="បឹងគ្រំលើ">បឹងគ្រំលើ</option>
        <option value="បឹងគ្រំក្រោម">បឹងគ្រំក្រោម</option>
<!--            ឃុំកោះកែវ-->
        <option value="កោះកែវលើ">កោះកែវលើ</option>
        <option value="កោះកែវក្រោម">កោះកែវក្រោម</option>
<!--            ឃុំកោះរះ-->
        <option value="កោះរះលើ">កោះរះលើ</option>
        <option value="កោះរះក្រោម">កោះរះក្រោម</option>
<!--            ឃុំល្វាសរ-->
        <option value="ល្វាសរលើ">ល្វាសរលើ</option>
        <option value="ល្វាសរកណ្ដាល">ល្វាសរកណ្ដាល</option>
        <option value="ល្វាសរក្រោម">ល្វាសរក្រោម</option>
<!--            ឃុំពាមឧកញ៉ាអុង-->
        <option value="ពាមតាឯក">ពាមតាឯក</option>
        <option value="ព្រែកតាអុងទី១">ព្រែកតាអុងទី១</option>
        <option value="ព្រែកតាអុងទី២">ព្រែកតាអុងទី២</option>
        <option value="ព្រែកតាអុងទី៣">ព្រែកតាអុងទី៣</option>
        <option value="វាលធំ">វាលធំ</option>
<!--            ឃុំភូមិធំ-->
        <option value="ព្រែកតាប្រាំង">ព្រែកតាប្រាំង</option>
        <option value="ព្រែកក្រូច">ព្រែកក្រូច</option>
<!--            ឃុំព្រែកក្មេង-->
        <option value="ព្រែកក្មេង">ព្រែកក្មេង</option>
        <option value="ទួលទ្រា">ទួលទ្រា</option>
<!--            ឃុំព្រែករៃ-->
        <option value="ព្រែករៃ">ព្រែករៃ</option>
        <option value="ព្រែកឈ្មោះ">ព្រែកឈ្មោះ</option>
        <option value="ព្រែកគង់រាជ">ព្រែកគង់រាជ</option>
<!--            ឃុំព្រែកឫស្សី-->
        <option value="ព្រែកប្រា">ព្រែកប្រា</option>
        <option value="ព្រែកជ្រៃ">ព្រែកជ្រៃ</option>
        <option value="ព្រែកឫស្សី">ព្រែកឫស្សី</option>
        <option value="អន្លង់ទ្រា">អន្លង់ទ្រា</option>
        <option value="ពាមស្ដី">ពាមស្ដី</option>
<!--            ឃុំសំបួរ-->
        <option value="សំបួរ">សំបួរ</option>
        <option value="ព្រែកចារ">ព្រែកចារ</option>
        <option value="ជ្រោយជ្រែ">ជ្រោយជ្រែ</option>
<!--            ឃុំសារិកាកែវ-->
        <option value="តាជោ">តាជោ</option>
        <option value="ក្ដីកណ្ដាល">ក្ដីកណ្ដាល</option>
        <option value="តាស្គរ">តាស្គរ</option>
<!--             ឃុំថ្មគរ-->
        <option value="ថ្មគរ">ថ្មគរ</option>
        <option value="ផ្លូវត្រី">ផ្លូវត្រី</option>
<!--            ឃុំទឹកឃ្លាំង-->
        <option value="ទឹកឃ្លាំង">ទឹកឃ្លាំង</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ជ្រោយពិសី">ជ្រោយពិសី</option>
<!--         ស្រុកមុខកំពូល-->
<!--            ឃុំព្រែកអញ្ចាញ-->
        <option value="ព្រែកតាបែន">ព្រែកតាបែន</option>  
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ក្រោម">ក្រោម</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="លើ">លើ</option>
        <option value="កោះរកា">កោះរកា</option>
        <option value="ឈើទាល">ឈើទាល</option>
<!--            ឃុំព្រែកដំបង-->
        <option value="ស្វាយជ្រុំ">ស្វាយជ្រុំ</option>
        <option value="ឡឥដ្ឋ">ឡឥដ្ឋ</option>
        <option value="វត្ដចាស់">វត្ដចាស់</option>
        <option value="វត្ដថ្មី">វត្ដថ្មី</option>
        <option value="សាមគ្គី">សាមគ្គី</option>
<!--            ឃុំរកាកោង ទី ១-->
        <option value="រកាកោង">រកាកោង</option>
        <option value="ពាម">ពាម</option>
        <option value="ព្រែកផ្ដៅ">ព្រែកផ្ដៅ</option>
        <option value="គោហ៊េ">គោហ៊េ</option>
<!--            ឃុំរកាកោង ទី ២-->
        <option value="ដើមជ្រៃ">ដើមជ្រៃ</option>
        <option value="ពោធិ៍រលំ">ពោធិ៍រលំ</option>
        <option value="ស្ពានថ្មី">ស្ពានថ្មី</option>
        <option value="ព្រែកជ្រៅ">ព្រែកជ្រៅ</option>
<!--            ឃុំឫស្សីជ្រោយ-->
        <option value="ព្រែកយាយហ៊ិន១">ព្រែកយាយហ៊ិន១</option>
        <option value="ព្រែកយាយហ៊ិន២">ព្រែកយាយហ៊ិន២</option>
        <option value="ព្រែកតាសោម">ព្រែកតាសោម</option>
        <option value="ឫស្សីជ្រោយ">ឫស្សីជ្រោយ</option>
        <option value="បឹងជន្លេន">បឹងជន្លេន</option>
        <option value="ជ្រោយមេត្រីក្រោម">ជ្រោយមេត្រីក្រោម</option>
        <option value="ជ្រោយមេត្រីលើ">ជ្រោយមេត្រីលើ</option>
<!--            ឃុំសំបួរមាស-->
        <option value="អន្លុងស្លែង">អន្លុងស្លែង</option>
        <option value="ជ្រៃមួយរយ">ជ្រៃមួយរយ</option>
        <option value="អំពិលទឹក">អំពិលទឹក</option>
        <option value="ពោង">ពោង</option>
        <option value="ព្រែកឫស្សី">ព្រែកឫស្សី</option>
        <option value="ក្រោលគោ">ក្រោលគោ</option>
        <option value="ពាម">ពាម</option>
<!--            ឃុំស្វាយអំពារ-->
        <option value="ថ្មី">ថ្មី</option>
        <option value="ស្វាយអំពារ">ស្វាយអំពារ</option>
        <option value="ឈើទាលភ្លោះ">ឈើទាលភ្លោះ</option>
        <option value="កំពង់ប្រាសាទ">កំពង់ប្រាសាទ</option>
        <option value="ក្រងស្វាយ">ក្រងស្វាយ</option>
<!--        ស្រុកអង្គស្នួល -->
<!--            ឃុំបែកចាន-->
        <option value="បែកចាន">បែកចាន</option>
        <option value="មនោរម្យ">មនោរម្យ</option>
        <option value="ចុងបង្គោល">ចុងបង្គោល</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ត្រពាំងទ្រា">ត្រពាំងទ្រា</option>
        <option value="ព្រៃសំរោង">ព្រៃសំរោង</option>
        <option value="ព្រៃទន្លាប់">ព្រៃទន្លាប់</option>
        <option value="ត្រាចទោល">ត្រាចទោល</option>
        <option value="បូរីកម្មករ">បូរីកម្មករ</option>
        <option value="កៅ">កៅ</option>
        <option value="ត្រពាំងសុក្រំ">ត្រពាំងសុក្រំ</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ត្រពាំងប្រុយ">ត្រពាំងប្រុយ</option>
        <option value="រកាធំ">រកាធំ</option>
        <option value="កម្ពោតស្បូវ">កម្ពោតស្បូវ</option>
        <option value="ព្រៃបឹង">ព្រៃបឹង</option>
        <option value="ត្នោតម្ដើម">ត្នោតម្ដើម</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="អណ្ដូង">អណ្ដូង</option>
        <option value="ចក">ចក</option>
        <option value="ស្វាយជ្រុំ">ស្វាយជ្រុំ</option>
<!--            ឃុំឆក់ឈើនាង-->  
        <option value="ព្រៃរការ">ព្រៃរការ</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ត្រពាំងជូន">ត្រពាំងជូន</option>
        <option value="សាំងដំរី">សាំងដំរី</option>
        <option value="ឆក់ឈើនាង">ឆក់ឈើនាង</option>
        <option value="ត្រពាំងខ្នារ">ត្រពាំងខ្នារ</option>
        <option value="ឈូកធំ">ឈូកធំ</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ត្រពាំងសុព័រ">ត្រពាំងសុព័រ</option>
        <option value="ក្រឡឹងដុំ">ក្រឡឹងដុំ</option>
<!--            ឃុំដំណាក់អំពិល-->
        <option value="ថ្នល់ទទឹង">ថ្នល់ទទឹង</option>
        <option value="ទន្លាប់ខ្ពស់ត្បូង">ទន្លាប់ខ្ពស់ត្បូង</option>
        <option value="ទន្លាប់ខ្ពស់ជើង">ទន្លាប់ខ្ពស់ជើង</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ព្រៃសំរោង">ព្រៃសំរោង</option>
        <option value="ដំណាក់អំពិល">ដំណាក់អំពិល</option>
        <option value="ក្ដាន់រយ">ក្ដាន់រយ</option>
        <option value="ត្រពាំងត្រាច">ត្រពាំងត្រាច</option>
        <option value="សេរីសុខា">សេរីសុខា</option>
        <option value="ជ្រៃមង្គ្គល">ជ្រៃមង្គ្គល</option>
<!--            ឃុំក្រាំងម្កាក់-->
        <option value="ត្រពាំងក្រពើ">ត្រពាំងក្រពើ</option>
        <option value="ត្រពាំងខ្ទឹម">ត្រពាំងខ្ទឹម</option>
        <option value="ក្រាំងម្កាក់">ក្រាំងម្កាក់</option>
        <option value="ត្រពាំងស្វាយ">ត្រពាំងស្វាយ</option>
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="ត្រពាំងនប់">ត្រពាំងនប់</option>
        <option value="ព្រៃពពេល">ព្រៃពពេល</option>
        <option value="ព្រៃខ្លា">ព្រៃខ្លា</option>
        <option value="ត្រពាំងផ្លុង">ត្រពាំងផ្លុង</option>
        <option value="អង្គពពាយ">អង្គពពាយ</option>
        <option value="ស រមាំង">ស រមាំង</option>
        <option value="ប្រាំបីមុម">ប្រាំបីមុម</option>
        <option value="ត្រពាំងជ្រៅ">ត្រពាំងជ្រៅ</option>
        <option value="ត្រពាំងរការ">ត្រពាំងរការ</option>
        <option value="ស្រែខ្សាច់">ស្រែខ្សាច់</option>
<!--            ឃុំលំហាច-->
        <option value="គោកពពេល">គោកពពេល</option>
        <option value="កន្ដោលតុ">កន្ដោលតុ</option>
        <option value="យសមេត្រី">យសមេត្រី</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="លំហាច">លំហាច</option>
        <option value="កណ្ដោស">កណ្ដោស</option>
        <option value="ត្រពាំងនំ">ត្រពាំងនំ</option>
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="ព្រៃក្រឡាញ់">ព្រៃក្រឡាញ់</option>
        <option value="លាក់កូប">លាក់កូប</option>
        <option value="អណ្ដូងទឹក">អណ្ដូងទឹក</option>
        <option value="ទំនប់សាប">ទំនប់សាប</option>
        <option value="ព្រៃចំការ">ព្រៃចំការ</option>
        <option value="រៃគោ">រៃគោ</option>
        <option value="ជើងអក">ជើងអក</option>
        <option value="សេកពង">សេកពង</option>
        <option value="ពន្លឺ">ពន្លឺ</option>
        <option value="តារ័ត្ន">តារ័ត្ន</option>
        <option value="សុខាភិរម្យ">សុខាភិរម្យ</option>
        <option value="ទូករទេះ">ទូករទេះ</option>
<!--            ឃុំម្កាក់-->
        <option value="ប្រសិទ្ធិ">ប្រសិទ្ធិ</option>
        <option value="ជោទ្រាច">ជោទ្រាច</option>
        <option value="ត្រពាំងធ្នង់">ត្រពាំងធ្នង់</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ត្រពាំងទៃ">ត្រពាំងទៃ</option>
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
        <option value="តាប្រាប">តាប្រាប</option>
        <option value="ត្រពាំងស្មាច់">ត្រពាំងស្មាច់</option>
        <option value="ស្រែកណ្ដោល">ស្រែកណ្ដោល</option>
        <option value="ត្រពាំងស្នោរ">ត្រពាំងស្នោរ</option>
        <option value="អូរ">អូរ</option>
        <option value="ត្រើង">ត្រើង</option>
        <option value="ជង្រុក">ជង្រុក</option>
        <option value="ត្រពាំងអណ្ដែង">ត្រពាំងអណ្ដែង</option>
        <option value="ត្រពាំងត្នោត">ត្រពាំងត្នោត</option>
        <option value="អង្គ រមាស">អង្គ រមាស</option>
        <option value="រំដេញ">រំដេញ</option>
        <option value="ស្ដុកវែង">ស្ដុកវែង</option>
        <option value="អន្ទុងក្រវៀន">អន្ទុងក្រវៀន</option>
        <option value="បេង">បេង</option>
        <option value="លំហាច">លំហាច</option>
        <option value="បឹងថ្នល់">បឹងថ្នល់</option>
        <option value="វែង">វែង</option>
        <option value="ពោធិ៍បួន">ពោធិ៍បួន</option>
        <option value="ចុងបឹង">ចុងបឹង</option>
        <option value="ចំការចិន">ចំការចិន</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="ឈើបួន">ឈើបួន</option>
        <option value="ពង្រ">ពង្រ</option>
<!--             ឃុំពើក-->
        <option value="ព្រៃពពេល">ព្រៃពពេល</option>
        <option value="ចំការជោ">ចំការជោ</option>
        <option value="ត្រពាំងពើក">ត្រពាំងពើក</option>
        <option value="ទឹកជា">ទឹកជា</option>
        <option value="ត្រពាំងឈើនាង">ត្រពាំងឈើនាង</option>
        <option value="ថ្លើក">ថ្លើក</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ត្រយឹង">ត្រយឹង</option>
        <option value="ត្រពាំងភូមិ">ត្រពាំងភូមិ</option>
        <option value="ស្រះសង្គម">ស្រះសង្គម</option>
        <option value="ទួលស្រម៉">ទួលស្រម៉</option>
        <option value="ទួលត្នោត">ទួលត្នោត</option>
        <option value="អង្គសំណាង">អង្គសំណាង</option>
        <option value="ចំការត្រាច">ចំការត្រាច</option>
        <option value="ខ្លាកូន">ខ្លាកូន</option>
        <option value="ព្រៃទំពូង">ព្រៃទំពូង</option>
        <option value="អង្គស្នួលទី១">អង្គស្នួលទី១</option>
        <option value="អង្គស្នួលទី២">អង្គស្នួលទី២</option>
        <option value="អង្គស្នួលទី៣">អង្គស្នួលទី៣</option>
<!--            ឃុំព្រៃពួច-->
        <option value="ពាម">ពាម</option>
        <option value="ស្វាយ">ស្វាយ</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="អូរកំបុត្រ">អូរកំបុត្រ</option>
        <option value="អង្គរស្មី">អង្គរស្មី</option>
        <option value="ទ្រា">ទ្រា</option>
        <option value="ព្រៃពួច">ព្រៃពួច</option>
        <option value="ប្របវត្ដ">ប្របវត្ដ</option>
        <option value="ត្រើយបឹង">ត្រើយបឹង</option>
        <option value="ទួលលាប">ទួលលាប</option>
        <option value="សល់ដី">សល់ដី</option>
        <option value="ទន្លាប់">ទន្លាប់</option>
        <option value="ព្រៃភក្ដី">ព្រៃភក្ដី</option>
        <option value="ព្រៃក្រាយ">ព្រៃក្រាយ</option>
        <option value="ចំការគួយ">ចំការគួយ</option>
        <option value="ចំការស្លែង">ចំការស្លែង</option>
        <option value="ចំបក់ទន្សាយ">ចំបក់ទន្សាយ</option>
        <option value="ក្រាំងលាវ">ក្រាំងលាវ</option>
        <option value="ប្រមរ">ប្រមរ</option>
        <option value="ព្រៃមាន">ព្រៃមាន</option>
        <option value="កប់ចន្លុះ">កប់ចន្លុះ</option>
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
        <option value="ទួលសាលា">ទួលសាលា</option>
<!--             ឃុំសំរោងលើ-->
        <option value="គីរីថ្មី">គីរីថ្មី</option>
        <option value="បឹង">បឹង</option>
        <option value="តាប៉ាង">តាប៉ាង</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="ត្រពាំងត្បែង">ត្រពាំងត្បែង</option>
        <option value="ល្វី">ល្វី</option>
        <option value="ទីទុយពង">ទីទុយពង</option>
        <option value="អង្គឃ្វាន">អង្គឃ្វាន</option>
        <option value="ត្រពាំងថ្ម">ត្រពាំងថ្ម</option>
        <option value="ស្រែអំព្រុំ">ស្រែអំព្រុំ</option>
        <option value="មង្គលបូរី">មង្គលបូរី</option>
        <option value="កោះគ្របបាយ">កោះគ្របបាយ</option>
        <option value="ដំណាក់កកោះ">ដំណាក់កកោះ</option>
        <option value="ត្រពាំងក្រឡាញ់">ត្រពាំងក្រឡាញ់</option>
        <option value="ពណ្ណរាយ">ពណ្ណរាយ</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ព្រៃផ្ចឹកទី១">ព្រៃផ្ចឹកទី១</option>
        <option value="ព្រៃផ្ចឹកទី២">ព្រៃផ្ចឹកទី២</option>
        <option value="ភ្នំដី">ភ្នំដី</option>
        <option value="ត្រពាំងអណ្ដូង">ត្រពាំងអណ្ដូង</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="ព្រៃលួង">ព្រៃលួង</option>
        <option value="ត្រពាំងទួល">ត្រពាំងទួល</option>
        <option value="ត្រពាំងទន្លាប់">ត្រពាំងទន្លាប់</option>
        <option value="ដូនម៉ាន់">ដូនម៉ាន់</option>
        <option value="ចំការដូង">ចំការដូង</option>
        <option value="ស្វាយឧត្ដម">ស្វាយឧត្ដម</option>
        <option value="ត្រពាំងជ្រៅ">ត្រពាំងជ្រៅ</option>
<!--            ឃុំទួលព្រេជ-->
        <option value="ចំបក់កោង">ចំបក់កោង</option>
        <option value="បឹងអញ្ចាញ">បឹងអញ្ចាញ</option>
        <option value="ខ្លុង">ខ្លុង</option>
        <option value="អង្គស្រែពោធិ៍">អង្គស្រែពោធិ៍</option>
        <option value="រំលេច">រំលេច</option>
        <option value="ទួលខ្លុង">ទួលខ្លុង</option>
        <option value="ពពេលរលំ">ពពេលរលំ</option>
        <option value="គោល">គោល</option>
        <option value="ទួលសេរី">ទួលសេរី</option>
        <option value="ក្រាំងក្រូច">ក្រាំងក្រូច</option>
        <option value="ច្រកក្រសាំង">ច្រកក្រសាំង</option>
        <option value="ត្រពាំងកំភ្លាញ">ត្រពាំងកំភ្លាញ</option>
        <option value="បឹងរមាស់">បឹងរមាស់</option>
        <option value="ម៉ឺនរៀម">ម៉ឺនរៀម</option>
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="ចន្ទ្រាថ្មី">ចន្ទ្រាថ្មី</option>
        <option value="ព្រៃរំដួល">ព្រៃរំដួល</option>
        <option value="អង្គតាសិត">អង្គតាសិត</option>
        <option value="បឹងទ្រា">បឹងទ្រា</option>
        <option value="ស្រែកាច់">ស្រែកាច់</option>
        <option value="បឹងខ្នារ">បឹងខ្នារ</option>
        <option value="ខ្នារ">ខ្នារ</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ព្រៃសំរោង">ព្រៃសំរោង</option>
        <option value="ទន្លាប់">ទន្លាប់</option>
<!--        ស្រុកពញាឮ-->
<!--            ឃុំឈ្វាំង-->
        <option value="តាំងស្ដុក">តាំងស្ដុក</option>
        <option value="ស្ដុកតាចាន់">ស្ដុកតាចាន់</option>
        <option value="ស្ដុកប្រនួត">ស្ដុកប្រនួត</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="ឈ្វាំង">ឈ្វាំង</option>
        <option value="ស្លែងមានជ័យ">ស្លែងមានជ័យ</option>
        <option value="តាគោក">តាគោក</option>
        <option value="ស្វាយ">ស្វាយ</option>
        <option value="ព្រៃគោល">ព្រៃគោល</option>
        <option value="ទស្សាស្វិត">ទស្សាស្វិត</option>
        <option value="កាកាប">កាកាប</option>
        <option value="តាពេជ">តាពេជ</option>
        <option value="ស្រែអំពិល">ស្រែអំពិល</option>
        <option value="ព្រៃផ្ចឹក">ព្រៃផ្ចឹក</option>
        <option value="តាអោក">តាអោក</option>
        <option value="ពង្រ">ពង្រ</option>
<!--            ឃុំជ្រៃលាស់-->
        <option value="ស្លែង">ស្លែង</option>
        <option value="តាពេទ្យ">តាពេទ្យ</option>
        <option value="ត្រពាំងរបង">ត្រពាំងរបង</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ត្រពាំងវាល">ត្រពាំងវាល</option>
        <option value="ព្រៃសុពណ៌">ព្រៃសុពណ៌</option>
        <option value="តាគោ">តាគោ</option>
        <option value="ពពែ">ពពែ</option>
        <option value="ត្រពាំងសង្កែ">ត្រពាំងសង្កែ</option>
        <option value="ត្បូងវត្ដ">ត្បូងវត្ដ</option>
        <option value="ត្រពាំងព្រលិត">ត្រពាំងព្រលិត</option>
        <option value="តាតូច">តាតូច</option>
        <option value="ជ្រៃលាស់">ជ្រៃលាស់</option>
        <option value="តាជៃ">តាជៃ</option>
        <option value="ទួលព្រិច">ទួលព្រិច</option>
<!--            ឃុំកំពង់ហ្លួង-->
        <option value="ឃ្លាំងស្បែក">ឃ្លាំងស្បែក</option>
        <option value="ទួលងោក">ទួលងោក</option>
        <option value="ពោធិ៍តូច">ពោធិ៍តូច</option>
        <option value="សង្វរ">សង្វរ</option>
        <option value="ពាមជំនីក">ពាមជំនីក</option>
        <option value="ពាមល្វែក">ពាមល្វែក</option>
        <option value="ជ្រោយឫស្សី">ជ្រោយឫស្សី</option>
        <option value="ខ្លាត្រាំ">ខ្លាត្រាំ</option>
<!--            ឃុំកំពង់អុស-->
        <option value="ដងគោម">ដងគោម</option>
        <option value="ព្រែកតាមែ">ព្រែកតាមែ</option>
        <option value="កំពុងអុស">កំពុងអុស</option>
        <option value="បឹងក្ដុល">បឹងក្ដុល</option>
        <option value="ព្រែកតាព្រហ្ម">ព្រែកតាព្រហ្ម</option>
<!--            ឃុំកោះចិន-->
        <option value="ដងគោម">ដងគោម</option>
        <option value="ព្រែកជីក">ព្រែកជីក</option>
        <option value="ព្រែកក្ដាម២">ព្រែកក្ដាម២</option>
        <option value="ព្រែកក្ដាម១">ព្រែកក្ដាម១</option>
        <option value="កោះចិន">កោះចិន</option>
        <option value="សសី">សសី</option>
        <option value="ថ្នល់បត់">ថ្នល់បត់</option>
        <option value="ចុងកោះ">ចុងកោះ</option>
        <option value="កំពង់រទេះ">កំពង់រទេះ</option>
        <option value="ជ្រោយសំបួរ">ជ្រោយសំបួរ</option>
        <option value="តាពៅ">តាពៅ</option>
<!--            ឃុំភ្នំបាត-->
        <option value="ពោធិ៍រ៉ាល">ពោធិ៍រ៉ាល</option>
        <option value="ស្រះពោធិ៍">ស្រះពោធិ៍</option>
        <option value="ត្រពាំងស្លែង">ត្រពាំងស្លែង</option>
        <option value="វាលថ្មី">វាលថ្មី</option>
        <option value="ដើមពោធិ៍">ដើមពោធិ៍</option>
        <option value="ភ្នំបាត">ភ្នំបាត</option>
        <option value="ធម្មស្រះ">ធម្មស្រះ</option>
        <option value="ក្បាលស្ពាន">ក្បាលស្ពាន</option>
        <option value="អូរស្លាត">អូរស្លាត</option>
        <option value="ថ្មស">ថ្មស</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="កំចាត់ព្រាយ">កំចាត់ព្រាយ</option>
        <option value="កំពង់ក្រសាំង">កំពង់ក្រសាំង</option>
        <option value="អង្គសេរី">អង្គសេរី</option>
        <option value="ស្ដុក">ស្ដុក</option>
        <option value="ឆ្កែលុង">ឆ្កែលុង</option>
        <option value="ក្រោលខៀវ">ក្រោលខៀវ</option>
        <option value="ត្រពាំងរកា">ត្រពាំងរកា</option>
        <option value="ចំបក់ភ្លោះ">ចំបក់ភ្លោះ</option>
        <option value="ថ្លុកអង្ក្រង">ថ្លុកអង្ក្រង</option>
        <option value="ថ្លុកត្របែក">ថ្លុកត្របែក</option>
        <option value="បន្ទាយតូច">បន្ទាយតូច</option>
<!--            ឃុំពញាឮ-->
        <option value="ទួលអំពិល">ទួលអំពិល</option>
        <option value="ពញាឮ">ពញាឮ</option>
        <option value="ព្រែកល្វា">ព្រែកល្វា</option>
<!--            ឃុំព្រែកតាទែន-->
        <option value="ពោធិ៍មង្គល">ពោធិ៍មង្គល</option>
        <option value="ស្លែងដីដុះ">ស្លែងដីដុះ</option>
        <option value="ព្រែកក្ដី">ព្រែកក្ដី</option>
<!--            ឃុំផ្សារដែក-->
        <option value="ធម្មត្រ័យ">ធម្មត្រ័យ</option>
        <option value="ម្លូម៉ឺន">ម្លូម៉ឺន</option>
        <option value="ផ្សារដែកលើ">ផ្សារដែកលើ</option>
        <option value="ផ្សារដែកក្រោម">ផ្សារដែកក្រោម</option>
        <option value="បដិមាករ">បដិមាករ</option>
        <option value="ចំបក់មាស">ចំបក់មាស</option>
        <option value="ទួលអង្គុញ">ទួលអង្គុញ</option>
        <option value="ភ្នំ">ភ្នំ</option>
        <option value="ជ័យឧត្ដម">ជ័យឧត្ដម</option>
        <option value="ស្រះពោធិ៍">ស្រះពោធិ៍</option>
<!--            ឃុំទំនប់ធំ-->
        <option value="ល្វា">ល្វា</option>
        <option value="ស្រែតាមែង">ស្រែតាមែង</option>
        <option value="ស្ពានទំលាប់">ស្ពានទំលាប់</option>
        <option value="ស្ដុកឈូក">ស្ដុកឈូក</option>
        <option value="ស្រែតាសេក">ស្រែតាសេក</option>
        <option value="ស្វាយលាប">ស្វាយលាប</option>
        <option value="ត្រពាំងជ្រៅ">ត្រពាំងជ្រៅ</option>
        <option value="អញ្ចាញ">អញ្ចាញ</option>
        <option value="ត្រពាំងឫស្សី">ត្រពាំងឫស្សី</option>
        <option value="កំណប់">កំណប់</option>
        <option value="បែកថ្លាង">បែកថ្លាង</option>
        <option value="ដំបូកមានល័ក្ខណ៍">ដំបូកមានល័ក្ខណ៍</option>
        <option value="ត្រពាំងអណ្ដូង">ត្រពាំងអណ្ដូង</option>
        <option value="ត្រពាំងធ្នង់">ត្រពាំងធ្នង់</option>
        <option value="ដំណាក់ព្រីង">ដំណាក់ព្រីង</option>
        <option value="ត្រពាំងព្រៃភូមិ">ត្រពាំងព្រៃភូមិ</option>
        <option value="ស្រែរាំង">ស្រែរាំង</option>
        <option value="អំពិលរូង">អំពិលរូង</option>
        <option value="ត្រពាំងពុទ្រា">ត្រពាំងពុទ្រា</option>
<!--            ឃុំវិហារហ្លួង-->
        <option value="ប្រក់ក្ដារ">ប្រក់ក្ដារ</option>
        <option value="ទេពប្រណម្យ">ទេពប្រណម្យ</option>
        <option value="កំពង់ចុះវារ">កំពង់ចុះវារ</option>
        <option value="ចេតីយ៍ទ្រេត">ចេតីយ៍ទ្រេត</option>
        <option value="អំពិលដាំទឹក">អំពិលដាំទឹក</option>
        <option value="សាលាកាត់សក់">សាលាកាត់សក់</option>
        <option value="ធ្យូង">ធ្យូង</option>
        <option value="ចេតីយ៍ថ្មី">ចេតីយ៍ថ្មី</option>
        <option value="ពោធិ៍កំបោរ">ពោធិ៍កំបោរ</option>
        <option value="អំពិលផ្អែម">អំពិលផ្អែម</option>
        <option value="ថ្នល់">ថ្នល់</option>  
        <option value="ចតុទិស">ចតុទិស</option>
<!--         ស្រុកស្អាង-->
<!--            ឃុំខ្ពប-->
        <option value="ឫស្សីស្រុក">ឫស្សីស្រុក</option>
        <option value="ខ្ពបលើ">ខ្ពបលើ</option>
        <option value="ខ្ពបក្រោម">ខ្ពបក្រោម</option>
        <option value="រកាលើ">រកាលើ</option>
        <option value="រកាក្រោម">រកាក្រោម</option>
        <option value="កោះថ្មី">កោះថ្មី</option>
        <option value="បឹងខ្ពប">បឹងខ្ពប</option>
        <option value="ដំរីឆ្លង">ដំរីឆ្លង</option>
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="ត្នោតញី">ត្នោតញី</option>
<!--            ឃុំកោះអន្លង់ចិន-->
        <option value="ក្បាលកោះ">ក្បាលកោះ</option>
        <option value="កណ្ដាលកោះ">កណ្ដាលកោះ</option>
        <option value="ស្វាយពងអង្ក្រង">ស្វាយពងអង្ក្រង</option>
        <option value="ចុងកោះ">ចុងកោះ</option>
<!--            ឃុំកោះខែល-->
        <option value="ស្វាយជួរ">ស្វាយជួរ</option>
        <option value="ទេពអរជូន">ទេពអរជូន</option>
        <option value="ព្រែកកែវ">ព្រែកកែវ</option>
        <option value="កោះខែល">កោះខែល</option>
        <option value="ព្រែកប៉ាង">ព្រែកប៉ាង</option>
        <option value="ដើមព្រីង">ដើមព្រីង</option>
<!--            ឃុំកោះខ្សាច់ទន្លា-->
        <option value="ក្បាលកោះកើត">ក្បាលកោះកើត</option>
        <option value="ក្បាលកោះលិច">ក្បាលកោះលិច</option>
        <option value="កណ្ដាលកោះ">កណ្ដាលកោះ</option>
        <option value="ចុងកោះកើត">ចុងកោះកើត</option>
        <option value="ចុងកោះលិច">ចុងកោះលិច</option>
        <option value="ក្បាលកោះខាងត្បូង">ក្បាលកោះខាងត្បូង</option>
        <option value="បទុមសាគរ">បទុមសាគរ</option>
        <option value="ព្រែកយាយម៉ី">ព្រែកយាយម៉ី</option>
<!--            ឃុំក្រាំងយ៉ូវ-->
        <option value="កំពង់ពោធិ៍">កំពង់ពោធិ៍</option>
        <option value="ទួលក្រាំង">ទួលក្រាំង</option>
        <option value="សំរោង">សំរោង</option>
        <option value="អណ្ដូង">អណ្ដូង</option>
        <option value="រកា">រកា</option>
        <option value="វិហារ">វិហារ</option>
        <option value="ពីងពង់">ពីងពង់</option>
        <option value="អំពិល">អំពិល</option>
        <option value="តាគល់">តាគល់</option>
        <option value="ធំ">ធំ</option>
        <option value="គរ">គរ</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="តាពេជ">តាពេជ</option>
        <option value="ចេក">ចេក</option>
        <option value="អង្គ">អង្គ</option>
        <option value="កំពង់ពោធិ៍ត្បូង">កំពង់ពោធិ៍ត្បូង</option>
        <option value="ទួលក្រួច">ទួលក្រួច</option>
        <option value="អម្ពិលលាស់">អម្ពិលលាស់</option>
        <option value="ស្វាយដំណាក់">ស្វាយដំណាក់</option>
<!--             ឃុំប្រាសាទ-->
        <option value="លេខ១">លេខ១</option>
        <option value="លេខ២">លេខ២</option>
        <option value="លេខ៣">លេខ៣</option>
        <option value="លេខ៤">លេខ៤</option>
        <option value="លេខ៥">លេខ៥</option>
<!--             ឃុំព្រែកអំបិល-->
        <option value="ត្រើយត្រឹង្ស">ត្រើយត្រឹង្ស</option>
        <option value="ព្រែកតាឡៃ">ព្រែកតាឡៃ</option>
        <option value="សំប៉ានលើ">សំប៉ានលើ</option>
        <option value="សំប៉ានក្រោម">សំប៉ានក្រោម</option>
        <option value="អន្លង់តាសេកលើ">អន្លង់តាសេកលើ</option>
        <option value="អន្លង់តាសេកក្រោម">អន្លង់តាសេកក្រោម</option>
        <option value="កូនជ្រែ">កូនជ្រែ</option>
        <option value="ព្រែកក្រាញ់">ព្រែកក្រាញ់</option>
        <option value="ពាមប្រជុំ">ពាមប្រជុំ</option>
        <option value="ព្រែកគ្រួស">ព្រែកគ្រួស</option>
        <option value="ព្រែកអំបិល">ព្រែកអំបិល</option>
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ឫស្សីដាច់">ឫស្សីដាច់</option>
        <option value="ព្រែកតាទាវ">ព្រែកតាទាវ</option>
        <option value="អន្លង់ធំ">អន្លង់ធំ</option>
        <option value="អន្លង់ឈើខ្លាង">អន្លង់ឈើខ្លាង</option>
<!--            ឃុំព្រែកគយ-->
        <option value="ក្នុងព្រែក">ក្នុងព្រែក</option>
        <option value="ព្រែករុន">ព្រែករុន</option>
        <option value="ព្រែកស្នង">ព្រែកស្នង</option>
        <option value="ព្រែកស្នាយ">ព្រែកស្នាយ</option>
        <option value="ស្វាយតានី">ស្វាយតានី</option>
        <option value="ព្រែកតាជ្រូក">ព្រែកតាជ្រូក</option>
        <option value="ទួលសូភី">ទួលសូភី</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ក្បាលជ្រោយ">ក្បាលជ្រោយ</option>
        <option value="ព្រែករុនក្រោម">ព្រែករុនក្រោម</option>
<!--            ឃុំរកាខ្ពស់-->
        <option value="កោះគរ">កោះគរ</option>
        <option value="ព្រែកថី">ព្រែកថី</option>
        <option value="ព្រែកសំរោង">ព្រែកសំរោង</option>
        <option value="ព្រែកខ្សេវ">ព្រែកខ្សេវ</option>
        <option value="ទួលក្រសាំង">ទួលក្រសាំង</option>
        <option value="កោះរំដួល">កោះរំដួល</option>
        <option value="ព្រែកឡុង">ព្រែកឡុង</option>
<!--            ឃុំស្អាងភ្នំ-->
        <option value="ព្រែកស្លែង">ព្រែកស្លែង</option>
        <option value="ទួលសាលា">ទួលសាលា</option>
        <option value="ព្រែកខ្មែរ">ព្រែកខ្មែរ</option>
        <option value="កំពង់ទ្រា">កំពង់ទ្រា</option>
        <option value="គោកអណ្ដែត">គោកអណ្ដែត</option>
        <option value="ដំរីឆ្លង">ដំរីឆ្លង</option>
        <option value="ពាមសាលា">ពាមសាលា</option>
        <option value="វាល">វាល</option>
        <option value="តានូ">តានូ</option>
<!--            ឃុំសិត្បូ-->
        <option value="ព្រែកតាព្រីង">ព្រែកតាព្រីង</option>
        <option value="សិត្បូ">សិត្បូ</option>
        <option value="កំពង់ព្រីង">កំពង់ព្រីង</option>
        <option value="ព្រែកត្រែង">ព្រែកត្រែង</option>
<!--            ឃុំស្វាយប្រទាល-->
        <option value="ព្រែកតាតិន">ព្រែកតាតិន</option>
        <option value="ឫស្សីជ្រោយ">ឫស្សីជ្រោយ</option>
        <option value="ព្រែកតាជ័រ">ព្រែកតាជ័រ</option>
        <option value="ព្រែកតាសៅ">ព្រែកតាសៅ</option>
        <option value="ចុងកោះគរ">ចុងកោះគរ</option>
        <option value="ប៉ារែនក្រោម">ប៉ារែនក្រោម</option>
        <option value="ប៉ារែនលើ">ប៉ារែនលើ</option>
        <option value="អូររំចេក">អូររំចេក</option>
        <option value="ពោធិ៍តាប៉ាង">ពោធិ៍តាប៉ាង</option>
<!--            ឃុំស្វាយរលំ-->
        <option value="លេខ១">លេខ១</option>
        <option value="លេខ២">លេខ២</option>
        <option value="លេខ៣">លេខ៣</option>
        <option value="លេខ៤">លេខ៤</option>
        <option value="លេខ៥">លេខ៥</option>
        <option value="លេខ១ ក">លេខ១ ក</option>
<!--            ឃុំតាលន់-->
        <option value="ព្រែកតាប្រាក់">ព្រែកតាប្រាក់</option>
        <option value="តាលន់">តាលន់</option>
        <option value="ចុងកោះតូច">ចុងកោះតូច</option>
        <option value="កណ្ដាលកោះតូច">កណ្ដាលកោះតូច</option>
        <option value="ក្បាលកោះតូច">ក្បាលកោះតូច</option>
        <option value="ទួលស្ពឺ">ទួលស្ពឺ</option>
        <option value="ព្រែកស្លែង">ព្រែកស្លែង</option>
        <option value="ព្រែកតាឯក">ព្រែកតាឯក</option>
        <option value="វាលត្រែង">វាលត្រែង</option>
<!--            ឃុំត្រើយស្លា-->
        <option value="ពោធិ៍លើ">ពោធិ៍លើ</option>
        <option value="ពោធិ៍កណ្ដាល">ពោធិ៍កណ្ដាល</option>
        <option value="ពោធិ៍ក្រោម">ពោធិ៍ក្រោម</option>
        <option value="ព្រែកតាឯក">ព្រែកតាឯក</option>
        <option value="ព្រែក">ព្រែក</option>
        <option value="ព្រែកប៉ាន">ព្រែកប៉ាន</option>
        <option value="ព្រែកបាឡាត់ឆឹង">ព្រែកបាឡាត់ឆឹង</option>
        <option value="ថ្កុល">ថ្កុល</option>
        <option value="ទួលក្ដី">ទួលក្ដី</option>
<!--            ឃុំទឹកវិល-->
        <option value="ព្រែកថ្មី">ព្រែកថ្មី</option>
        <option value="ព្រែកតាប៉ឹម">ព្រែកតាប៉ឹម</option>
        <option value="ព្រែកតាវ៉ា">ព្រែកតាវ៉ា</option>
        <option value="ព្រែកអុងប៉ាង">ព្រែកអុងប៉ាង</option>
        <option value="វត្ដកណ្ដាល">វត្ដកណ្ដាល</option>
        <option value="ផ្លូវបំបែក">ផ្លូវបំបែក</option>
        <option value="ព្រែកពោធិ៍">ព្រែកពោធិ៍</option>
        <option value="ព្រែករាំង">ព្រែករាំង</option>
        <option value="ព្រែករាំង ក">ព្រែករាំង ក</option>
        <option value="ទួលខ្មួញ">ទួលខ្មួញ</option>
<!--        ក្រុងតាខ្មៅ-->
<!--            សង្កាត់តាក្ដុល-->
        <option value="តាក្ដុល">តាក្ដុល</option>
        <option value="ព្រែកកាត់">ព្រែកកាត់</option>
        <option value="ព្រែកឡុង">ព្រែកឡុង</option>
        <option value="តាក្ដុលត្បូង">តាក្ដុលត្បូង</option>
<!--            សង្កាត់ព្រែកឫស្សី-->
        <option value="ក្រពើហា">ក្រពើហា</option>
        <option value="ព្រែកឫស្សី">ព្រែកឫស្សី</option>
        <option value="ព្រែកអញ្ចាញ">ព្រែកអញ្ចាញ</option>
        <option value="ព្រែកឫស្សីលិច">ព្រែកឫស្សីលិច</option>
        <option value="ក្រពើហាកើត">ក្រពើហាកើត</option>
<!--            សង្កាត់ដើមមៀន-->
        <option value="ដើមមៀន">ដើមមៀន</option>
        <option value="ដើមគរ">ដើមគរ</option>
        <option value="ព្រែកតាពៅ">ព្រែកតាពៅ</option>
        <option value="ស្ទឹងជ្រៅ">ស្ទឹងជ្រៅ</option>
        <option value="ដើមមៀន ១">ដើមមៀន ១</option>
        <option value="ព្រែកតាពៅ ១">ព្រែកតាពៅ ១</option>
<!--            សង្កាត់តាខ្មៅ-->
        <option value="តាខ្មៅ">តាខ្មៅ</option>
        <option value="ព្រែកសំរោង">ព្រែកសំរោង</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="តាខ្មៅ ១">តាខ្មៅ ១</option>
        <option value="តាខ្មៅ ២">តាខ្មៅ ២</option>
        <option value="តាខ្មៅ ៣">តាខ្មៅ ៣</option>
        <option value="ថ្មី ១">ថ្មី ១</option>
        <option value="ថ្មី ២">ថ្មី ២</option>
        <option value="ព្រែកសំរោង ១">ព្រែកសំរោង ១</option>
        <option value="ព្រែកសំរោង ២">ព្រែកសំរោង ២</option>
        <option value="ព្រែកសំរោង ៣">ព្រែកសំរោង ៣</option>
<!--            សង្កាត់ព្រែកហូរ-->
        <option value="ព្រែកហូរកើត">ព្រែកហូរកើត</option>
        <option value="ព្រែកហូរលិច">ព្រែកហូរលិច</option>
        <option value="បត្ដាជី">បត្ដាជី</option>
        <option value="ព្រែកហូរលិច ១">ព្រែកហូរលិច ១</option>
        <option value="ព្រែកហូរកើត ១">ព្រែកហូរកើត ១</option>
        <option value="បត្ដាជី ១">បត្ដាជី ១</option>
<!--            សង្កាត់កំពង់សំណាញ់-->
        <option value="កំពង់សំណាញ់">កំពង់សំណាញ់</option>
        <option value="ខ្ពបវែង">ខ្ពបវែង</option>
        <option value="ព្រែករាំង">ព្រែករាំង</option>
        <option value="អាចម៍កុក">អាចម៍កុក</option>
        <option value="កំពង់សំណាញ់ ១">កំពង់សំណាញ់ ១</option>
        <option value="ព្រែករាំង ១">ព្រែករាំង ១</option>
<!--    ខេត្តតាកែវ-->
<!--        ស្រុកអង្គរបូរី-->
<!--            ឃុំអង្គរបូរី-->
        <option value="កំពង់ហ្លួង">កំពង់ហ្លួង</option>
        <option value="ស្ទឹងកំបុត">ស្ទឹងកំបុត</option>
        <option value="ព្រៃសំបួរ">ព្រៃសំបួរ</option>
        <option value="ទួលសាំង ក">ទួលសាំង ក</option>
        <option value="ទួលសាំង ខ">ទួលសាំង ខ</option>
        <option value="សាមគ្គី">សាមគ្គី</option>
<!--            ឃុំបាស្រែ-->
        <option value="ស្វាយខាងត្បូង">ស្វាយខាងត្បូង</option>
        <option value="ស្វាយខាងជើង">ស្វាយខាងជើង</option>
        <option value="បាស្រែ">បាស្រែ</option>
        <option value="រំលក">រំលក</option>
        <option value="ព្រៃបាសឹង">ព្រៃបាសឹង</option>
        <option value="តាអី">តាអី</option>
        <option value="រកា">រកា</option>
        <option value="ពួនកក">ពួនកក</option>
<!--            ឃុំគោកធ្លក-->
        <option value="ព្រែកតាផ">ព្រែកតាផ</option>
        <option value="ព្រែកដា">ព្រែកដា</option>
        <option value="ទួលពុទ្រា">ទួលពុទ្រា</option>
        <option value="បាក់ដៃ">បាក់ដៃ</option>  
<!--            ឃុំពន្លៃ-->
        <option value="អំពិល">អំពិល</option>
        <option value="ស្រម៉ុក">ស្រម៉ុក</option>
        <option value="ពន្លៃខាងជើង">ពន្លៃខាងជើង</option>
        <option value="ពន្លៃខាងត្បូង">ពន្លៃខាងត្បូង</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ធ្លកយុល">ធ្លកយុល</option>
<!--             ឃុំព្រែកផ្ទោល-->
        <option value="អង្គរ">អង្គរ</option>
        <option value="កំពង់ពោធិ៍">កំពង់ពោធិ៍</option>
        <option value="ភ្នំបូរី">ភ្នំបូរី</option>
        <option value="ភ្នំបាទេព">ភ្នំបាទេព</option>
<!--            ឃុំព្រៃផ្គាំ-->
        <option value="ព្រៃផ្គាំ ក">ព្រៃផ្គាំ ក</option>
        <option value="ព្រៃផ្គាំ ខ">ព្រៃផ្គាំ ខ</option>
        <option value="ព្រៃផ្គាំ គ">ព្រៃផ្គាំ គ</option>
        <option value="ទ្រង់់ភូមិ">ទ្រង់់ភូមិ</option>
        <option value="យាផ្អើ">យាផ្អើ</option>
        <option value="តាមូង">តាមូង</option>
<!--         ស្រុកបាទី -->
<!--            ឃុំចំបក់-->
        <option value="តានប់">តានប់</option>
        <option value="បចាម">បចាម</option>
        <option value="ម្រះព្រៅ">ម្រះព្រៅ</option>
        <option value="ត្រពាំងលាន">ត្រពាំងលាន</option>
        <option value="រុន">រុន</option>
        <option value="ស្ដៅឯម">ស្ដៅឯម</option>
        <option value="កន្លែងខ្លា">កន្លែងខ្លា</option>
        <option value="សីហា">សីហា</option>
        <option value="បឹងលាច">បឹងលាច</option>
        <option value="វាលប្រីយ៍">វាលប្រីយ៍</option>
        <option value="ស្រមោចហែ">ស្រមោចហែ</option>      
        <option value="ត្រពាំងត្រយឹង">ត្រពាំងត្រយឹង</option>
<!--            ឃុំចំប៉ី-->
        <option value="ដើមដូង">ដើមដូង</option>
        <option value="ម្កាក់">ម្កាក់</option>
        <option value="ត្រកៀត">ត្រកៀត</option>
        <option value="ព្រែក">ព្រែក</option>
        <option value="មឿងប្រចិន">មឿងប្រចិន</option>
        <option value="ព្រៃមូល">ព្រៃមូល</option>
        <option value="ជើងលោង">ជើងលោង</option>
<!--            ឃុំដូង-->
        <option value="ប្រមូលសុខ">ប្រមូលសុខ</option>
        <option value="ក្រាំងប្រទាល">ក្រាំងប្រទាល</option>
        <option value="ស្វាយខម">ស្វាយខម</option>
        <option value="យុថ្កា">យុថ្កា</option>
        <option value="ចេក">ចេក</option>
        <option value="ដូង">ដូង</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="តានន">តានន</option>
<!--             ឃុំកណ្ដឹង-->
        <option value="ក្រសាំង">ក្រសាំង</option>
        <option value="ក្រាំងអំពិល">ក្រាំងអំពិល</option>
        <option value="ឱភាសាំង">ឱភាសាំង</option>
        <option value="ព្រះម្លប់">ព្រះម្លប់</option>
        <option value="ត្រពាំងលើក">ត្រពាំងលើក</option>
        <option value="កណ្ដឹងធំ">កណ្ដឹងធំ</option>
        <option value="ហនុមាន">ហនុមាន</option>
        <option value="កណ្ដឹងតូច">កណ្ដឹងតូច</option>
<!--            ឃុំកុមាររាជា-->
        <option value="ព្រៃខ្លា">ព្រៃខ្លា</option>
        <option value="ត្រពាំងផ្លុង">ត្រពាំងផ្លុង</option>
        <option value="សិរីមានជោគ">សិរីមានជោគ</option>
        <option value="ត្រមូងជ្រុំ">ត្រមូងជ្រុំ</option>
        <option value="ក្រាំងពង្រ">ក្រាំងពង្រ</option>
        <option value="រយ៉ក">រយ៉ក</option>
        <option value="ស្ដុក">ស្ដុក</option>
        <option value="ច្បារមន">ច្បារមន</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ខ្នារទង់">ខ្នារទង់</option>
        <option value="ព្រៃស្លែង">ព្រៃស្លែង</option>
        <option value="កញ្ចាង">កញ្ចាង</option>
        <option value="កញ្ជុំ">កញ្ជុំ</option>
<!--            ឃុំក្រាំងលាវ-->
        <option value="ត្រពាំងពោធិ">ត្រពាំងពោធិ</option>
        <option value="ស្រីគ្រងរាជ្យ">ស្រីគ្រងរាជ្យ</option>
        <option value="ព្រៃត្រាច">ព្រៃត្រាច</option>
        <option value="សាមគ្គី">សាមគ្គី</option>
        <option value="តាប៉ែន">តាប៉ែន</option>
        <option value="មានជ័យ">មានជ័យ</option>
        <option value="មានជោគ">មានជោគ</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="ត្រាចកុះ">ត្រាចកុះ</option>
        <option value="ត្រាំក្បាល់">ត្រាំក្បាល់</option>
        <option value="រូង">រូង</option>
        <option value="ត្រពាំងអង្គ">ត្រពាំងអង្គ</option>
        <option value="ដើមស្វាយ">ដើមស្វាយ</option>
        <option value="ព្រហូត">ព្រហូត</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ទួលស្លែង">ទួលស្លែង</option>
        <option value="ឈើទាលជ្រុំ">ឈើទាលជ្រុំ</option>
        <option value="ក្រាំងលាវ">ក្រាំងលាវ</option>
        <option value="សៀមដេក">សៀមដេក</option>
        <option value="ព្រៃជន្លួញ">ព្រៃជន្លួញ</option>
        <option value="ត្រាំសសរ">ត្រាំសសរ</option>
        <option value="ថ្នល់ដាច់">ថ្នល់ដាច់</option>
        <option value="បាញ់ខ្លា">បាញ់ខ្លា</option>
<!--            ឃុំក្រាំងធ្នង់-->
        <option value="ហនុមាន">ហនុមាន</option>
        <option value="ត្បែង">ត្បែង</option>
        <option value="ខ្នារ">ខ្នារ</option>
        <option value="ថ្នល់ទក្សិណ">ថ្នល់ទក្សិណ</option>
        <option value="ជ្រោងស្ដៅ">ជ្រោងស្ដៅ</option>
        <option value="ត្បូងដំរី">ត្បូងដំរី</option>
        <option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>
        <option value="ទន្លេបាទី">ទន្លេបាទី</option>
<!--            ឃុំលំពង់-->
        <option value="ក្រាំងធំ">ក្រាំងធំ</option>
        <option value="ត្រយឹងខ្ពស់">ត្រយឹងខ្ពស់</option>
        <option value="បាក់រនាស់">បាក់រនាស់</option>
        <option value="ត្រពាំងក្រឡាញ់">ត្រពាំងក្រឡាញ់</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ពូនភ្នំ">ពូនភ្នំ</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ពានមាសខាងកើត">ពានមាសខាងកើត</option>
        <option value="ថ្មស">ថ្មស</option>
        <option value="ត្រពាំងគ្រួស">ត្រពាំងគ្រួស</option>
        <option value="ត្រពាំងក្រឡោង">ត្រពាំងក្រឡោង</option>
        <option value="ពានមាសខាងលិច">ពានមាសខាងលិច</option>
<!--            ឃុំពារាម-->
        <option value="តាំងញាតិ">តាំងញាតិ</option>
        <option value="ពារាម">ពារាម</option>
        <option value="ខ្នារធំ">ខ្នារធំ</option>
        <option value="ខ្នាររោង">ខ្នាររោង</option>
        <option value="ត្រពាំងយោង">ត្រពាំងយោង</option>
        <option value="ជំរៅ">ជំរៅ</option>
        <option value="ក្រាំងក្រចាង">ក្រាំងក្រចាង</option>
        <option value="គ្រួស">គ្រួស</option>
<!--            ឃុំពត់សរ-->
        <option value="ព្រៃស្វា">ព្រៃស្វា</option>
        <option value="ក្រូច">ក្រូច</option>
        <option value="ក្រាំងពោធិ៍">ក្រាំងពោធិ៍</option>
        <option value="ត្រពាំងត្រាវ">ត្រពាំងត្រាវ</option>
        <option value="ចំបក់">ចំបក់</option>
        <option value="ពត់សរ">ពត់សរ</option>
        <option value="តាំងឫស្សី">តាំងឫស្សី</option>
        <option value="ឃ្លាំងសម្បត្ដិ">ឃ្លាំងសម្បត្ដិ</option>
        <option value="កណ្ដោល">កណ្ដោល</option>
        <option value="ខ្វាន់មាស">ខ្វាន់មាស</option>
        <option value="ខ្លាកូន">ខ្លាកូន</option>
<!--            ឃុំសូរភី-->
        <option value="ចំប៉ា">ចំប៉ា</option>
        <option value="ត្រាំខ្នារ">ត្រាំខ្នារ</option>
        <option value="ត្រពាំងឫស្សី">ត្រពាំងឫស្សី</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ព្រហែក">ព្រហែក</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ជើងកូត">ជើងកូត</option>
        <option value="ភ្នំតូច">ភ្នំតូច</option>
<!--            ឃុំតាំងដូង-->
        <option value="ព្រៃក្ដី">ព្រៃក្ដី</option>
        <option value="អង្គក្រឡាញ់">អង្គក្រឡាញ់</option>
        <option value="អំពែទុំ">អំពែទុំ</option>
        <option value="រវៀង">រវៀង</option>
        <option value="ព្រៃក្រាយ">ព្រៃក្រាយ</option>
        <option value="ផ្សារគុម្ពឫស្សី">ផ្សារគុម្ពឫស្សី</option>
        <option value="សំរោងជ្រៃ">សំរោងជ្រៃ</option>
        <option value="ទួលល្ហុង">ទួលល្ហុង</option>
        <option value="នាល">នាល</option>
        <option value="តាំងដូង">តាំងដូង</option>
<!--            ឃុំត្នោត-->
        <option value="ត្នោត">ត្នោត</option>
        <option value="រំចេក">រំចេក</option>
        <option value="តានប់">តានប់</option>
        <option value="តានន">តានន</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ពៃ">ពៃ</option>
        <option value="ព្រៃជប់">ព្រៃជប់</option>
        <option value="ឈើទាល">ឈើទាល</option>
<!--            ឃុំត្រពាំងក្រសាំង-->
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ធ្លក">ធ្លក</option>
        <option value="រលាំង">រលាំង</option>
        <option value="ត្រពាំងព្រិច">ត្រពាំងព្រិច</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ត្រពាំងគៀប">ត្រពាំងគៀប</option>
        <option value="ត្នោតជ្រុំ">ត្នោតជ្រុំ</option>
        <option value="គោដួល">គោដួល</option>
        <option value="រំដួល">រំដួល</option>
        <option value="ម្រះព្រៅ">ម្រះព្រៅ</option>
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
        <option value="យាមខៅ">យាមខៅ</option>
        <option value="ទឹកថ្លា">ទឹកថ្លា</option>
        <option value="រកាពក">រកាពក</option>
        <option value="ត្រាំសសរ">ត្រាំសសរ</option>
        <option value="បឹងពញ្ញាគុក">បឹងពញ្ញាគុក</option>
        <option value="ត្រពាំងចំការ">ត្រពាំងចំការ</option>
<!--            ឃុំត្រពាំងសាប-->
        <option value="ពូនភ្នំ">ពូនភ្នំ</option>
        <option value="ត្រពាំងទឹម">ត្រពាំងទឹម</option>
        <option value="រកាខ្ពស់">រកាខ្ពស់</option>
        <option value="ខ្សាច់លប់">ខ្សាច់លប់</option>
        <option value="រលាំងគ្រើល">រលាំងគ្រើល</option>
        <option value="ចក">ចក</option>
        <option value="ត្រពាំងសាប">ត្រពាំងសាប</option>
        <option value="តាស៊ូ">តាស៊ូ</option>
        <option value="សង្កែ">សង្កែ</option>
        <option value="ស្មៅខ្ញី">ស្មៅខ្ញី</option>
        <option value="អាជាំង">អាជាំង</option>
        <option value="ព្រេច">ព្រេច</option>
        <option value="ស្ដុកប្រីយ៍">ស្ដុកប្រីយ៍</option>
        <option value="ដើមក្រាយ">ដើមក្រាយ</option>
        <option value="ត្រកៀត">ត្រកៀត</option>
<!--             ឃុំបូរីជលសារ-->
        <option value="អង្គក្រូច">អង្គក្រូច</option>
        <option value="ស្នាយដួច">ស្នាយដួច</option>
        <option value="កំពង់អំពិល">កំពង់អំពិល</option>
        <option value="អន្លង់នៀន">អន្លង់នៀន</option>
        <option value="ក្អែកយំ">ក្អែកយំ</option>
        <option value="ដើមគរ">ដើមគរ</option>
        <option value="ព្រែកខ្សាច់">ព្រែកខ្សាច់</option>
<!--            ឃុំជ័យជោគ-->
        <option value="សង្កែជួរ">សង្កែជួរ</option>
        <option value="ជ័យជោគ">ជ័យជោគ</option>
        <option value="គោកបញ្ចា">គោកបញ្ចា</option>
        <option value="អញ្ចាញ">អញ្ចាញ</option>
        <option value="ដីលើក">ដីលើក</option>
        <option value="តារាគមន៍">តារាគមន៍</option>
        <option value="បន្ទាយស្លឹក">បន្ទាយស្លឹក</option>
<!--            ឃុំដូងខ្ពស់-->
        <option value="តាសៃ">តាសៃ</option>
        <option value="សូភី">សូភី</option>
        <option value="រទេះភ្លូក">រទេះភ្លូក</option>
        <option value="ត្រពាំងទន្លេ">ត្រពាំងទន្លេ</option>
        <option value="អាចម៍ទន្សាយ">អាចម៍ទន្សាយ</option>
        <option value="ដូងខ្ពស់">ដូងខ្ពស់</option>
        <option value="តារស់">តារស់</option>
        <option value="តាយឹង">តាយឹង</option>
        <option value="ត្រើយឃ្លោក">ត្រើយឃ្លោក</option>
        <option value="អង្កាញ់">អង្កាញ់</option>
        <option value="ជ្រៃងោក">ជ្រៃងោក</option>
        <option value="ព្រៃម្លប់">ព្រៃម្លប់</option>
<!--            ឃុំកំពង់ក្រសាំង-->
        <option value="បូរីជលសារ">បូរីជលសារ</option>
        <option value="កំពង់ក្រសាំង">កំពង់ក្រសាំង</option>
        <option value="ក្ដុលជ្រុំ">ក្ដុលជ្រុំ</option>
        <option value="សង្គមមានជ័យ">សង្គមមានជ័យ</option>
        <option value="ថ្មបីដុំ">ថ្មបីដុំ</option>
<!--            ឃុំគោកពោធិ៍-->
        <option value="កណ្ដោល">កណ្ដោល</option>  
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ជ្រៃគោកពោធិ៍">ជ្រៃគោកពោធិ៍</option>
        <option value="ព្រៃមូល">ព្រៃមូល</option>
        <option value="ព្រៃហៀវ">ព្រៃហៀវ</option>
        <option value="អ្នកតាត្បាល់">អ្នកតាត្បាល់</option>
        <option value="កំពង់យោល">កំពង់យោល</option>
        <option value="ថ្មស">ថ្មស</option>
<!--        ស្រុកគីរីវង់-->
<!--            ឃុំអង្គប្រាសាទ-->
        <option value="ពោធិ៍តាម៉ុក">ពោធិ៍តាម៉ុក</option>
        <option value="រនាមត្នោត">រនាមត្នោត</option>
        <option value="ប្រជ្រាយ">ប្រជ្រាយ</option>
        <option value="អង្គប្រាសាទ">អង្គប្រាសាទ</option>
        <option value="ភ្ងាស">ភ្ងាស</option>
        <option value="វត្ដស្វាយ">វត្ដស្វាយ</option>
        <option value="ភ្នំលន្ទះ">ភ្នំលន្ទះ</option>
        <option value="ទួលស្វាយ">ទួលស្វាយ</option>
        <option value="ស្វាយធំ">ស្វាយធំ</option>
        <option value="ក្របាក់">ក្របាក់</option>
<!--            ឃុំព្រះបាទជាន់ជុំ-->
        <option value="ពោធិ៍ខ្វិត">ពោធិ៍ខ្វិត</option>
        <option value="កំពង់">កំពង់</option>
        <option value="ត្រពាំងស្រង់">ត្រពាំងស្រង់</option>
        <option value="ត្រើយទន្លាប់">ត្រើយទន្លាប់</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="ពោធិ៍រោង">ពោធិ៍រោង</option>
        <option value="ជ្រោយ">ជ្រោយ</option>
        <option value="ពោធិ៍">ពោធិ៍</option>
        <option value="កំពង់ថ្មី">កំពង់ថ្មី</option>
<!--            ឃុំកំណប់-->
        <option value="កំណប់">កំណប់</option>
        <option value="ក្រងុល">ក្រងុល</option>
        <option value="ដើមស្លែង">ដើមស្លែង</option>
        <option value="ខ្មល់">ខ្មល់</option>
        <option value="ពោធិ៍សង្កែ">ពោធិ៍សង្កែ</option>
        <option value="ចំការទៀប">ចំការទៀប</option>
<!--            ឃុំកំពែង-->
        <option value="តាពៅ">តាពៅ</option>
        <option value="ធំ">ធំ</option>
        <option value="វត្ដភ្នំ">វត្ដភ្នំ</option>
        <option value="អាងខ្ចៅ">អាងខ្ចៅ</option>
        <option value="ជីម្រាក់">ជីម្រាក់</option>
        <option value="ហាន់ទា">ហាន់ទា</option>
        <option value="អណ្ដូងជ្រុង">អណ្ដូងជ្រុង</option>
        <option value="ពន្លៃ">ពន្លៃ</option>
        <option value="តាមែង">តាមែង</option>
        <option value="ស្វាយវល្លិ">ស្វាយវល្លិ</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="ពោធិ៍បាយ">ពោធិ៍បាយ</option>
        <option value="ច្រកជើគាំ">ច្រកជើគាំ</option>
<!--            ឃុំគីរីចុងកោះ-->
        <option value="ចំបក់ទឹម">ចំបក់ទឹម</option>
        <option value="ព្រៃតាម៉ៅ">ព្រៃតាម៉ៅ</option>
        <option value="ដើមបេង">ដើមបេង</option>
        <option value="ព្រាល">ព្រាល</option>
        <option value="ចេក">ចេក</option>
        <option value="ជ្រោយស្លែង">ជ្រោយស្លែង</option>
<!--             ឃុំគោកព្រេច-->
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="ក្បាលដំរី">ក្បាលដំរី</option>
        <option value="គោកព្រេច">គោកព្រេច</option>
        <option value="ជីឃ្មល់">ជីឃ្មល់</option>
        <option value="ឈើនៀងខ្ពស់">ឈើនៀងខ្ពស់</option>
        <option value="ស្លែង">ស្លែង</option>
        <option value="សំរោងខាងកើត">សំរោងខាងកើត</option>
        <option value="សំរោងខាងលិច">សំរោងខាងលិច</option>
        <option value="ចំបក់">ចំបក់</option>
        <option value="អណ្ដូងធំ">អណ្ដូងធំ</option>
        <option value="គោកគ្រួស">គោកគ្រួស</option>
        <option value="ព្រៃជើង">ព្រៃជើង</option>
        <option value="បាម">បាម</option>
<!--            ឃុំភ្នំដិន-->
        <option value="ផ្សារ">ផ្សារ</option>
        <option value="តាឡឹង">តាឡឹង</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="តារុង">តារុង</option>
        <option value="ជ្វា">ជ្វា</option>
        <option value="អណ្ដូងគៀន">អណ្ដូងគៀន</option>
        <option value="ធំ">ធំ</option>
        <option value="ទទឹង">ទទឹង</option>
        <option value="ថ្មី">ថ្មី</option>
<!--            ឃុំព្រៃអំពក-->
        <option value="ព្រៃលៀប">ព្រៃលៀប</option>
        <option value="ខ្វាវ">ខ្វាវ</option>
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="អំរ៉ែ">អំរ៉ែ</option>
        <option value="សុបិណ">សុបិណ</option>
        <option value="ព្រៃអំពក">ព្រៃអំពក</option>
        <option value="ថ្នល់លោក">ថ្នល់លោក</option>
        <option value="ពុម្ពអិដ្ឋ">ពុម្ពអិដ្ឋ</option>
        <option value="ឈើទាលភ្លោះ">ឈើទាលភ្លោះ</option>
<!--            ឃុំព្រៃរំដេង-->
        <option value="ភ្នំក្រពើ">ភ្នំក្រពើ</option>
        <option value="បួរ">បួរ</option>
        <option value="ចំរេះ">ចំរេះ</option>
        <option value="ដំណាក់ថ្ងាន់">ដំណាក់ថ្ងាន់</option>
        <option value="បពល">បពល</option>
        <option value="ត្រពាំងជ័យ">ត្រពាំងជ័យ</option>
        <option value="ដីក្រហម">ដីក្រហម</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ត្រពាំងពិដោរ">ត្រពាំងពិដោរ</option>
        <option value="ព្រៃរំដេង">ព្រៃរំដេង</option>
        <option value="បឹងទំនប់">បឹងទំនប់</option>
<!--            ឃុំរាមអណ្ដើក-->
        <option value="ស្វាយស">ស្វាយស</option>
        <option value="ត្រយឹង">ត្រយឹង</option>
        <option value="ត្រពាំងរុន">ត្រពាំងរុន</option>
        <option value="រាមអណ្ដើក">រាមអណ្ដើក</option>
        <option value="ត្រពាំងខ្ចៅ">ត្រពាំងខ្ចៅ</option>
        <option value="កោះកុសល">កោះកុសល</option>
        <option value="គោករកា">គោករកា</option>
        <option value="ពង្រ">ពង្រ</option>
<!--            ឃុំសោម-->   
        <option value="ទួលពង្រ">ទួលពង្រ</option>
        <option value="ស្រែឃ្មួញ">ស្រែឃ្មួញ</option>
        <option value="ស្រែកែស">ស្រែកែស</option>
        <option value="ព្រាល">ព្រាល</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ពីងពង់">ពីងពង់</option>
        <option value="សោម">សោម</option>
        <option value="ដើមរំដែល">ដើមរំដែល</option>
        <option value="ដើមអង្កោល">ដើមអង្កោល</option>
        <option value="នាសារគិរី">នាសារគិរី</option>
        <option value="ទន្លាប់">ទន្លាប់</option>
        <option value="ត្រពាំងពងទឹក">ត្រពាំងពងទឹក</option>
<!--            ឃុំតាអូរ-->
        <option value="ក្រសាំងពុល">ក្រសាំងពុល</option>
        <option value="រលៀក">រលៀក</option>
        <option value="ស្លា">ស្លា</option>
        <option value="ដោម">ដោម</option>
        <option value="ជាវប្ដីខាងកើត">ជាវប្ដីខាងកើត</option>
        <option value="ជាវប្ដីខាងលិច">ជាវប្ដីខាងលិច</option>
        <option value="គ្រាំងទ្រមូង">គ្រាំងទ្រមូង</option>
        <option value="តាអូរខាងជើង">តាអូរខាងជើង</option>
        <option value="តាអូរខាងត្បូង">តាអូរខាងត្បូង</option>
<!--         ស្រុកកោះអណ្ដែត-->
<!--             ឃុំក្រពុំឈូក-->
        <option value="ដើមដូង">ដើមដូង</option>
        <option value="បេង">បេង</option>
        <option value="ក្រពុំឈូក">ក្រពុំឈូក</option>
        <option value="ព្រៃម្លូ">ព្រៃម្លូ</option>
        <option value="ពាន្នី">ពាន្នី</option>
        <option value="តាពរ">តាពរ</option>
        <option value="ត្រពាំងតាអ៊ុយ">ត្រពាំងតាអ៊ុយ</option>
        <option value="កោះមាន់">កោះមាន់</option>
        <option value="វត្ដស្លា">វត្ដស្លា</option>
        <option value="ជ្រោយពោន">ជ្រោយពោន</option>
        <option value="ខ្លាគ្រហឹម ក">ខ្លាគ្រហឹម ក</option>
        <option value="ខ្លាគ្រហឹម ខ">ខ្លាគ្រហឹម ខ</option>
        <option value="ត្រពាំងទន្លេ">ត្រពាំងទន្លេ</option>
<!--            ឃុំពេជសារ-->
        <option value="តាបួ">តាបួ</option>
        <option value="ជន្ទល់មេឃ">ជន្ទល់មេឃ</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="ដំណាក់">ដំណាក់</option>
        <option value="ព្រៃបាយ">ព្រៃបាយ</option>
        <option value="គោកដូង">គោកដូង</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="តាមោក">តាមោក</option>
        <option value="ពោធិ">ពោធិ</option>
        <option value="ពោន">ពោន</option>
        <option value="ពេជសារ">ពេជសារ</option>
        <option value="ស្លែង">ស្លែង</option>
        <option value="ស្ដៅ">ស្ដៅ</option>
        <option value="ចុងអង្ករ">ចុងអង្ករ</option>
        <option value="អង្គុញ">អង្គុញ</option>
        <option value="តាបស">តាបស</option>
        <option value="គោកខ្ពស់">គោកខ្ពស់</option>
<!--            ឃុំព្រៃខ្លា-->
        <option value="ស្រម៉">ស្រម៉</option>
        <option value=""></option>
        <option value="ស្រែបឹង">ស្រែបឹង</option>
        <option value="ព្រៃមេលងខាងត្បូង">ព្រៃមេលងខាងត្បូង</option>
        <option value="ព្រៃមេលងខាងជើង">ព្រៃមេលងខាងជើង</option>
        <option value="កន្សោមអក">កន្សោមអក</option>
        <option value="ចំបក់">ចំបក់</option>
        <option value="សំភ្លី">សំភ្លី</option>
        <option value="គរ">គរ</option>
        <option value="តាំងរាសី">តាំងរាសី</option>
        <option value="ព្រៃខ្លាខាងជើង">ព្រៃខ្លាខាងជើង</option>
        <option value="ព្រៃខ្លាខាងត្បូង">ព្រៃខ្លាខាងត្បូង</option>
        <option value="បន្ទាយធ្លាយ">បន្ទាយធ្លាយ</option>
        <option value="កែវកាំភ្លើង">កែវកាំភ្លើង</option>
        <option value="ទួលកណ្ដាល">ទួលកណ្ដាល</option>
        <option value="ជំរំ">ជំរំ</option>
<!--            ឃុំព្រៃយុថ្កា-->
        <option value="តាញឹម">តាញឹម</option>
        <option value="ព្រៃបាយ">ព្រៃបាយ</option>
        <option value="តាភិន">តាភិន</option>
        <option value="តាផាន់">តាផាន់</option>
        <option value="តាហៀន">តាហៀន</option>
        <option value="ពងអណ្ដើក">ពងអណ្ដើក</option>
<!--             ឃុំរមេញ-->
        <option value="រមេញខាងត្បូង">រមេញខាងត្បូង</option>
        <option value="រមេញខាងជើង">រមេញខាងជើង</option>
        <option value="ដើមចាន់">ដើមចាន់</option>
        <option value="មានាគ">មានាគ</option>
        <option value="ដើមក្រូច">ដើមក្រូច</option>
        <option value="ប្រឡាយមាស">ប្រឡាយមាស</option>
        <option value="ចំបក់ឯម">ចំបក់ឯម</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="ដើមពោធិ">ដើមពោធិ</option>
<!--            ឃុំធ្លាប្រជុំ-->
        <option value="រលួស">រលួស</option>
        <option value="លៀប">លៀប</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="អណ្ដូងសំរិទ្ធិ">អណ្ដូងសំរិទ្ធិ</option>
        <option value="រូង">រូង</option>
        <option value="ព្នៅ">ព្នៅ</option>
        <option value="ស៊ីស្លា">ស៊ីស្លា</option>
<!--        ស្រុកព្រៃកប្បាស -->
<!--            ឃុំអង្កាញ់-->
        <option value="ត្រពាំងរកា">ត្រពាំងរកា</option>
        <option value="បា នយ">បា នយ</option>
        <option value="ផ្សារជ្រែ">ផ្សារជ្រែ</option>
        <option value="ព្រីង">ព្រីង</option>
        <option value="ស្វាយពារ">ស្វាយពារ</option>
        <option value="អង្កាញ់">អង្កាញ់</option>
<!--            ឃុំបានកាម-->
        <option value="សំរោងម្រះ">សំរោងម្រះ</option>
        <option value="ដូងខ្ពស់">ដូងខ្ពស់</option>
        <option value="ថ្លុកដូនទុំ">ថ្លុកដូនទុំ</option>
        <option value="ថ្នល់បត់">ថ្នល់បត់</option>  
        <option value="សេដ្ឋី">សេដ្ឋី</option>
        <option value="ពន្ទង">ពន្ទង</option>
        <option value="តាវង់">តាវង់</option>
<!--            ឃុំចំប៉ា-->
        <option value="ពន្សាំង">ពន្សាំង</option>
        <option value="ឫស្សីថ្មី">ឫស្សីថ្មី</option>
        <option value="ជ្រយ">ជ្រយ</option>
        <option value="ជំពូព្រឹក្ស">ជំពូព្រឹក្ស</option>
        <option value="ចំប៉ា">ចំប៉ា</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ដង្ហឹត">ដង្ហឹត</option>
        <option value="ចេក">ចេក</option>
        <option value="រនាមពេជ្រ">រនាមពេជ្រ</option>
<!--            ឃុំចារ-->
        <option value="អំពិលលិច">អំពិលលិច</option>
        <option value="អំពិលកើត">អំពិលកើត</option>
        <option value="ស្រែពោធិ">ស្រែពោធិ</option>
        <option value="ចារ">ចារ</option>
        <option value="ច័ន្ទមង្គល">ច័ន្ទមង្គល</option>
        <option value="ស្លា">ស្លា</option>
        <option value="បាំងបាត់">បាំងបាត់</option>
        <option value="ស្វាយចាល់">ស្វាយចាល់</option>
        <option value="អង្គស្វាយចេក">អង្គស្វាយចេក</option>
<!--            ឃុំកំពែង-->
        <option value="តាមូង">តាមូង</option>
        <option value="ចង្កើប">ចង្កើប</option>
        <option value="តាឡូង">តាឡូង</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="កំពែងធំ">កំពែងធំ</option>
        <option value="កំពែងត្បូង">កំពែងត្បូង</option>
        <option value="អង្គជ្រូក">អង្គជ្រូក</option>
        <option value="ចំណោម">ចំណោម</option>
        <option value="ត្រពាំងអង្គ">ត្រពាំងអង្គ</option>
<!--            ឃុំកំពង់រាប-->
        <option value="ពាម">ពាម</option>
        <option value="ជំនីក">ជំនីក</option>
        <option value="កញ្ចិល">កញ្ចិល</option>
        <option value="កំពង់លាវ">កំពង់លាវ</option>
        <option value="កំពង់រាប">កំពង់រាប</option>
        <option value="កំពង់សាម៉">កំពង់សាម៉</option>
        <option value="ក្លែងគង់">ក្លែងគង់</option>
<!--            ឃុំក្ដាញ់-->
        <option value="ត្រពាំងស្វាយ">ត្រពាំងស្វាយ</option>
        <option value="សម្ដេចពាន់">សម្ដេចពាន់</option>
        <option value="ក្ដាញ់">ក្ដាញ់</option>
        <option value="ក្ប៉ម">ក្ប៉ម</option>
        <option value="វាល">វាល</option>
        <option value="ក្រាំងវិច">ក្រាំងវិច</option>
        <option value="អង្គ">អង្គ</option>
<!--            ឃុំពោធិ៍រំចាក-->
        <option value="ក្ដីតាហុក">ក្ដីតាហុក</option>
        <option value="ថ្នល់បត់">ថ្នល់បត់</option>
        <option value="ស្វាយសំរោង">ស្វាយសំរោង</option>
        <option value="អង្គសង្កែ">អង្គសង្កែ</option>
        <option value="ក្រសាំង">ក្រសាំង</option>
        <option value="ព្រៃតាពង">ព្រៃតាពង</option>
        <option value="គោកអង្គង់">គោកអង្គង់</option>
        <option value="សម៉លាវ">សម៉លាវ</option>
        <option value="សម៉ខ្មែរ">សម៉ខ្មែរ</option>
        <option value="គរ">គរ</option>
        <option value="គោកកញ្ចាប">គោកកញ្ចាប</option>
<!--            ឃុំព្រៃកប្បាស-->    
        <option value="ពេជចង្វា">ពេជចង្វា</option>
        <option value="ត្រពាំងក្រូច">ត្រពាំងក្រូច</option>
        <option value="ព្រៃកប្បាស ក">ព្រៃកប្បាស ក</option>
        <option value="ព្រៃកប្បាស ខ">ព្រៃកប្បាស ខ</option>
        <option value="រៃកប្បាស គ">រៃកប្បាស គ</option>
        <option value="សំចត">សំចត</option>
        <option value="ព្រៃព្រុំ">ព្រៃព្រុំ</option>
        <option value="ដើមពោធិ">ដើមពោធិ</option>
        <option value="អំពិលរៀង">អំពិលរៀង</option>
        <option value="អូរ">អូរ</option>
<!--            ឃុំព្រៃល្វា-->
        <option value="ព្រៃល្វាកើត">ព្រៃល្វាកើត</option>
        <option value="ព្រៃល្វាលិច">ព្រៃល្វាលិច</option>
        <option value="អន្លុងមាស">អន្លុងមាស</option>
        <option value="តាខុន">តាខុន</option>
        <option value="អង្គក្រសាំង">អង្គក្រសាំង</option>
        <option value="ល្វាត្នោត">ល្វាត្នោត</option>
<!--            ឃុំព្រៃផ្ដៅ-->
        <option value="ព្រៃរបង">ព្រៃរបង</option>
        <option value="គោកទ្រា">គោកទ្រា</option>
        <option value="ព្រៃផ្ដៅត្បូង">ព្រៃផ្ដៅត្បូង</option>
        <option value="ព្រៃផ្ដៅជើង">ព្រៃផ្ដៅជើង</option>
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
        <option value="ជុំរំ">ជុំរំ</option>
        <option value="សៃវ៉ា">សៃវ៉ា</option>
        <option value="ព្រៃភ្ញី">ព្រៃភ្ញី</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="ដុង">ដុង</option>
        <option value="ស្មន់មុនី">ស្មន់មុនី</option>
<!--            ឃុំស្នោ-->
        <option value="ក្រាំង">ក្រាំង</option>
        <option value="ទង្គែ">ទង្គែ</option>
        <option value="ស្នោ">ស្នោ</option>
        <option value="រកា">រកា</option>
        <option value="ធម្មវិន័យ">ធម្មវិន័យ</option>
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
<!--            ឃុំតាំងយ៉ាប-->
        <option value="ត្រពាំងទា">ត្រពាំងទា</option>
        <option value="ព្រៃចំបក់">ព្រៃចំបក់</option>
        <option value="ត្រពាំងស្ដុក">ត្រពាំងស្ដុក</option>
        <option value="ស្នូលខ្ពស់">ស្នូលខ្ពស់</option>
        <option value="ក្រាំងអំពិល">ក្រាំងអំពិល</option>
        <option value="ក្រាំងចំរើន">ក្រាំងចំរើន</option>
        <option value="ជំរៅ">ជំរៅ</option>
        <option value="កែវចំរើន">កែវចំរើន</option>
        <option value="រលួស">រលួស</option>
        <option value="ច្រនៀងខ្ពស់">ច្រនៀងខ្ពស់</option>
        <option value="អង្គ រវាយ">អង្គ រវាយ</option>
        <option value="សំបូរ">សំបូរ</option>
<!--        ស្រុកសំរោង -->
<!--            ឃុំបឹងត្រាញ់ខាងជើង-->
        <option value="ខ្នាចខាងជើង">ខ្នាចខាងជើង</option>
        <option value="ខ្នាចខាងត្បូង">ខ្នាចខាងត្បូង</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="ពេជឥន្ទ្រា">ពេជឥន្ទ្រា</option>
        <option value="រមន់">រមន់</option>
        <option value="បីពៃ">បីពៃ</option>
        <option value="ពេជចង្វា">ពេជចង្វា</option>
        <option value="អង្គរាំង">អង្គរាំង</option>
        <option value="កូនរមាស">កូនរមាស</option>
<!--            ឃុំបឹងត្រាញ់ខាងត្បូង-->
        <option value="ដក់ពរ">ដក់ពរ</option>
        <option value="ស្រីជ័យ">ស្រីជ័យ</option>
        <option value="ខ្នាររុង">ខ្នាររុង</option>
        <option value="បឹងត្រាញ់">បឹងត្រាញ់</option>
        <option value="តាសំ">តាសំ</option>
        <option value="មហារាជ">មហារាជ</option>
        <option value="ហង់ហេង">ហង់ហេង</option>
        <option value="កំប៉ោរ">កំប៉ោរ</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="តាដក់ពង">តាដក់ពង</option>
<!--            ឃុំជើងគួន-->
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="តាមៅ">តាមៅ</option>
        <option value="ត្រពាំងវិហារ">ត្រពាំងវិហារ</option>
        <option value="ត្បាច">ត្បាច</option>
        <option value="តាខូយ">តាខូយ</option>
        <option value="ជើងគួន">ជើងគួន</option>
        <option value="ពន្សាំង">ពន្សាំង</option>
        <option value="អញ្ចាញ">អញ្ចាញ</option>
        <option value="ថ្កូវ">ថ្កូវ</option>
        <option value="ក្រាំងឡង">ក្រាំងឡង</option>
        <option value="ស្វាយចេក">ស្វាយចេក</option>
<!--            ឃុំជំរះពេន-->
        <option value="ស្រែតាសុខ">ស្រែតាសុខ</option>
        <option value="ស្វាយរន្ធ">ស្វាយរន្ធ</option>
        <option value="ធ្លកដំណាក់ហ្លួង">ធ្លកដំណាក់ហ្លួង</option>
        <option value="ត្រមែង">ត្រមែង</option>
        <option value="ដំណាក់ត្រយឹង">ដំណាក់ត្រយឹង</option>
        <option value="តាយឹង">តាយឹង</option>
        <option value="ព្រៃខ្លា">ព្រៃខ្លា</option>
        <option value="ពន្លឺ">ពន្លឺ</option>
        <option value="កន្សោមក្លែង">កន្សោមក្លែង</option>
        <option value="ត្រពាំងខ្នារ">ត្រពាំងខ្នារ</option>
        <option value="អង្គរផ្ដៀក">អង្គរផ្ដៀក</option>
        <option value="ជំរះពេន">ជំរះពេន</option>
        <option value="ស្លែង">ស្លែង</option>
        <option value="ត្រពាំងរំដួល">ត្រពាំងរំដួល</option>
        <option value="បឹង">បឹង</option>
        <option value="ត្រពាំងចំបក់">ត្រពាំងចំបក់</option>
        <option value="ព្រៃនាងពួន">ព្រៃនាងពួន</option>
        <option value="ស្នែងរមាំង">ស្នែងរមាំង</option>
        <option value="ព្រៃស្នួល">ព្រៃស្នួល</option>
<!--            ឃុំខ្វាវ-->
        <option value="លាក់រទេះ">លាក់រទេះ</option>
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
        <option value="ព្រៃជ័រ">ព្រៃជ័រ</option>
        <option value="វះពោះ">វះពោះ</option>
        <option value="ត្រពាំងឃ្លោក">ត្រពាំងឃ្លោក</option>
        <option value="អង្គុញ">អង្គុញ</option>
        <option value="ត្រាំគល់">ត្រាំគល់</option>
        <option value="កាប់នឹម">កាប់នឹម</option>
        <option value="ពោធិ៍">ពោធិ៍</option>
        <option value="ខ្វាវ">ខ្វាវ</option>
        <option value="បឹង">បឹង</option>
        <option value="អូរ">អូរ</option>
        <option value="ត្រពាំងធ្នុង">ត្រពាំងធ្នុង</option>
        <option value="ត្រពាំងពួន">ត្រពាំងពួន</option>
        <option value="ព្រៃញឹក">ព្រៃញឹក</option>
        <option value="ស្វាយទង">ស្វាយទង</option>
        <option value="ទួលតាចិន">ទួលតាចិន</option>
        <option value="ត្រពាំងត្របែក">ត្រពាំងត្របែក</option>
<!--            ឃុំលំចង់-->
        <option value="លំចង់">លំចង់</option>
        <option value="ស្វាយព្រៃ">ស្វាយព្រៃ</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ក្ដុល">ក្ដុល</option>
        <option value="ធម្មន័យ">ធម្មន័យ</option>
        <option value="ទួលទ្រា">ទួលទ្រា</option>
        <option value="តាមូង">តាមូង</option>
        <option value="រូង">រូង</option>
        <option value="ខ្វាវ">ខ្វាវ</option>
        <option value="ប្រសៀត">ប្រសៀត</option>
<!--            ឃុំរវៀង-->
        <option value="ត្រពាំងខ្នារ">ត្រពាំងខ្នារ</option>
        <option value="កុកតារៀ">កុកតារៀ</option>
        <option value="ត្រពាំងទ្រា">ត្រពាំងទ្រា</option>
        <option value="ទឹកអំបិល">ទឹកអំបិល</option>
        <option value="ព្រៃខ្ជាយ">ព្រៃខ្ជាយ</option>
        <option value="តោល">តោល</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="រវៀង">រវៀង</option>
        <option value="ក្រាំងលាវ">ក្រាំងលាវ</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ត្រពាំងស្ទង">ត្រពាំងស្ទង</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ចន្លាត់ដៃ">ចន្លាត់ដៃ</option>
        <option value="ស្រមោចហែរ">ស្រមោចហែរ</option>
        <option value="ត្រពាំងស្ដុក">ត្រពាំងស្ដុក</option>
        <option value="គ្រួស">គ្រួស</option>
        <option value="ដក់ពរ">ដក់ពរ</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>
        <option value="ព្រៃស្នួល">ព្រៃស្នួល</option>
        <option value="វាយឈ្នះ">វាយឈ្នះ</option>
        <option value="ចារ">ចារ</option>
        <option value="ដើមធ្លក">ដើមធ្លក</option>
<!--            ឃុំសំរោង-->
        <option value="កន្សោមអក">កន្សោមអក</option>
        <option value="ដីក្រហម">ដីក្រហម</option>
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ទួលចារ">ទួលចារ</option>
        <option value="តាពោនខាងកើត">តាពោនខាងកើត</option>
        <option value="តាពោនខាងលិច">តាពោនខាងលិច</option>
        <option value="ក្រាំងរអូត">ក្រាំងរអូត</option>
        <option value="ស្វាយ">ស្វាយ</option>
<!--             ឃុំសឹង្ហ-->
        <option value="ឈូកសខាងជើង">ឈូកសខាងជើង</option>
        <option value="ឈូកសខាងត្បូង">ឈូកសខាងត្បូង</option>
        <option value="ក្ដី">ក្ដី</option>
        <option value="ព្រៃខ្លា">ព្រៃខ្លា</option>
        <option value="ត្រពាំងកី">ត្រពាំងកី</option>
        <option value="វែង">វែង</option>
        <option value="ទន្លាប់">ទន្លាប់</option>
        <option value="ប៉ាណា">ប៉ាណា</option>
        <option value="កាយបាំង">កាយបាំង</option>
        <option value="ក្រាំងត្រែង">ក្រាំងត្រែង</option>
        <option value="សឹង្ហ">សឹង្ហ</option>
        <option value="ត្រពាំងប្រីយ៍">ត្រពាំងប្រីយ៍</option>
        <option value="អង្គក្ដី">អង្គក្ដី</option>
        <option value="ក្រាំងស្ដៅ">ក្រាំងស្ដៅ</option>
<!--            ឃុំស្លា-->
        <option value="ស្លាខាងលិច">ស្លាខាងលិច</option>
        <option value="ស្លាខាងកើត">ស្លាខាងកើត</option>
        <option value="អង្គចង្អេរ">អង្គចង្អេរ</option>
        <option value="ត្រពាំងត្រាវ">ត្រពាំងត្រាវ</option>
        <option value="កន្ទ្រង់ព្រិច">កន្ទ្រង់ព្រិច</option>
        <option value="បឹងកន្ទ្រន់">បឹងកន្ទ្រន់</option>
        <option value="ត្រពាំងស្រង់">ត្រពាំងស្រង់</option>
        <option value="ពោធិ">ពោធិ</option>
        <option value="អំពិល">អំពិល</option>
        <option value="កញ្ចាង">កញ្ចាង</option>
        <option value="ស្រីបណ្ឌិត">ស្រីបណ្ឌិត</option>
        <option value="ស្រីប្រសើរ">ស្រីប្រសើរ</option>
        <option value="អារោង">អារោង</option>
<!--            ឃុំទ្រា-->
        <option value="វែង">វែង</option>
        <option value="ក្បាលសំរោង">ក្បាលសំរោង</option>
        <option value="ឫស្សីជុំ">ឫស្សីជុំ</option>
        <option value="សែនភាស">សែនភាស</option>
        <option value="ត្នោតទេរ">ត្នោតទេរ</option>
        <option value="ដូនតី">ដូនតី</option>
        <option value="សំបួរ">សំបួរ</option>
        <option value="ទ្រាលើ">ទ្រាលើ</option>
        <option value="កំពង់ទ្រា">កំពង់ទ្រា</option>
        <option value="ធ្មា">ធ្មា</option>
        <option value="ដូង">ដូង</option>
<!--        ក្រុងដូនកែវ-->
<!--            សង្កាត់បារាយណ៍-->
        <option value="ធន់មន់ខាងជើង">ធន់មន់ខាងជើង</option>
        <option value="ធន់មន់ខាងត្បូង">ធន់មន់ខាងត្បូង</option>
        <option value="ឫស្សី">ឫស្សី</option>
        <option value="ជ្រោយប្រឃរ">ជ្រោយប្រឃរ</option>
        <option value="ស្វាយជ្រុំ">ស្វាយជ្រុំ</option>
        <option value="ដូនពែង">ដូនពែង</option>
        <option value="ស្វាយឫស្សី">ស្វាយឫស្សី</option>
        <option value="ខាន់ខាវ">ខាន់ខាវ</option>
        <option value="ត្រពាំងឫស្សី">ត្រពាំងឫស្សី</option>
        <option value="ក្រចាប់">ក្រចាប់</option>
        <option value="ត្រពាំងលាក់">ត្រពាំងលាក់</option>
        <option value="ចុងថ្នល់">ចុងថ្នល់</option>
        <option value="ក្រាំងតាពូង">ក្រាំងតាពូង</option>
        <option value="ជ្រោយសំរោង">ជ្រោយសំរោង</option>
<!--            សង្កាត់រកាក្នុង-->
        <option value="ចក">ចក</option>
        <option value="ផ្សារតាកោ">ផ្សារតាកោ</option>
        <option value="សំបួរ">សំបួរ</option>
        <option value="ខ្សឹង">ខ្សឹង</option>
        <option value="អូរស្វាយចេក">អូរស្វាយចេក</option>
        <option value="ច្រេស">ច្រេស</option>
        <option value="ព្រៃព្រហ្ម">ព្រៃព្រហ្ម</option>
        <option value="ឡូរី">ឡូរី</option>
        <option value="ភូមិ១">ភូមិ១</option>
        <option value="ភូមិ២">ភូមិ២</option>
        <option value="ភូមិ៣">ភូមិ៣</option>
        <option value="ស្នោរ">ស្នោរ</option>
<!--            សង្កាត់រកាក្រៅ-->
        <option value="ទំនប់">ទំនប់</option>
        <option value="ត្រាំ">ត្រាំ</option>
        <option value="ព្រហូត">ព្រហូត</option>
        <option value="តាឌូ">តាឌូ</option>
        <option value="ថ្នល់បែក">ថ្នល់បែក</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ត្រពាំងសាលា">ត្រពាំងសាលា</option>
        <option value="បិនម៉ៅ">បិនម៉ៅ</option>
        <option value="សូរ្យច័ន្ទ">សូរ្យច័ន្ទ</option>
        <option value="ត្រពាំងផ្លុង">ត្រពាំងផ្លុង</option>
        <option value="ព្រេច">ព្រេច</option>
        <option value="តុំ">តុំ</option>
        <option value="ធ្នង់">ធ្នង់</option>
        <option value="ត្រពាំងអង្គ">ត្រពាំងអង្គ</option>
<!--        ស្រុកត្រាំកក់-->
<!--            ឃុំអង្គតាសោម-->
        <option value="ស្រុកចេក">ស្រុកចេក</option>
        <option value="ព្រៃដំរី">ព្រៃដំរី</option>
        <option value="ព្រៃស្រោង">ព្រៃស្រោង</option>
        <option value="ព្រៃឈើទាលលើ">ព្រៃឈើទាលលើ</option>
        <option value="ឈើទាលប្រគាប">ឈើទាលប្រគាប</option>
        <option value="ព្រៃឈើទាលក្រោម">ព្រៃឈើទាលក្រោម</option>
        <option value="អង្គតាសោម">អង្គតាសោម</option>
        <option value="ត្រពាំងខ្នារ">ត្រពាំងខ្នារ</option>
        <option value="សេកយា">សេកយា</option>
        <option value="អូរផុត">អូរផុត</option>
        <option value="ខ្នាទៃ">ខ្នាទៃ</option>
        <option value="ព្រៃរំដេង">ព្រៃរំដេង</option>
        <option value="ត្រពាំងចែង">ត្រពាំងចែង</option>
        <option value="អង្គត្នោតលិច">អង្គត្នោតលិច</option>
        <option value="ត្រពាំងស្រងែ">ត្រពាំងស្រងែ</option>
        <option value="អង្គត្នោតកើត">អង្គត្នោតកើត</option>
        <option value="ចំការដៀប">ចំការដៀប</option>
        <option value="ស្មន់ព្រាម">ស្មន់ព្រាម</option>
        <option value="ត្រពាំងត្របែក">ត្រពាំងត្របែក</option>
        <option value="ព្រៃព្រាយ">ព្រៃព្រាយ</option>
        <option value="ត្រពាំងខ្លូត">ត្រពាំងខ្លូត</option>
        <option value="អង្គរមឿង">អង្គរមឿង</option>
<!--            ឃុំជាងទង-->
        <option value="ស្រែខ្វាវ">ស្រែខ្វាវ</option>
        <option value="តារាប">តារាប</option>
        <option value="អង្គក្រឡាញ់">អង្គក្រឡាញ់</option>
        <option value="អង្គបក្សី">អង្គបក្សី</option>
        <option value="ទួលរកា">ទួលរកា</option>
        <option value="ត្រពាំងស្រង៉ែ">ត្រពាំងស្រង៉ែ</option>
        <option value="ទទឹងថ្ងៃ">ទទឹងថ្ងៃ</option>
        <option value="ត្រពាំងទឹក">ត្រពាំងទឹក</option>
        <option value="តាតឹម">តាតឹម</option>
        <option value="មឿងចារ">មឿងចារ</option>
        <option value="ត្រពាំងពោធិ៍">ត្រពាំងពោធិ៍</option>
        <option value="សណ្ដោ">សណ្ដោ</option>
        <option value="ទីប៉ាត់">ទីប៉ាត់</option>
        <option value="ស្រែគ្រួ">ស្រែគ្រួ</option>
        <option value="ទួលត្បែង">ទួលត្បែង</option>
        <option value="នមោ">នមោ</option>
<!--            ឃុំគុស-->
        <option value="គុសថ្មី">គុសថ្មី</option>
        <option value="ទឹកថ្លា">ទឹកថ្លា</option>
        <option value="ទំនប់ជ្រៃ">ទំនប់ជ្រៃ</option>
        <option value="ខ្នាចចោរ">ខ្នាចចោរ</option>
        <option value="អកពង">អកពង</option>
        <option value="មានជ័យ">មានជ័យ</option>
        <option value="អង្គក្រឡាញ់">អង្គក្រឡាញ់</option>
        <option value="អង្គតាង៉ិល">អង្គតាង៉ិល</option>
        <option value="ឈើទាលថ្គោល">ឈើទាលថ្គោល</option>
        <option value="ពងទឹកជើង">ពងទឹកជើង</option>
        <option value="ក្រាំងតាចាន់">ក្រាំងតាចាន់</option>
        <option value="នៀល">នៀល</option>
        <option value="ត្រពាំងលាន">ត្រពាំងលាន</option>
        <option value="ត្មាតពង">ត្មាតពង</option>
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="ត្រពាំងថ្ម">ត្រពាំងថ្ម</option>
        <option value="តាលាក់ខាងជើង">តាលាក់ខាងជើង</option>
        <option value="តាលាក់ខាងត្បូង">តាលាក់ខាងត្បូង</option>
        <option value="ត្រពាំងតាសុខ">ត្រពាំងតាសុខ</option>
        <option value="ត្រពាំងអំពិល">ត្រពាំងអំពិល</option>
        <option value="ចំការទៀង">ចំការទៀង</option>
        <option value="សែនអោក">សែនអោក</option>
        <option value="ទន្សោងរោទ៍">ទន្សោងរោទ៍</option>
        <option value="ត្រពាំងឈើទាល">ត្រពាំងឈើទាល</option>
        <option value="ព្រៃស្នួល">ព្រៃស្នួល</option>
        <option value="ចំការអង្គខាងជើង">ចំការអង្គខាងជើង</option>
        <option value="ចំការអង្គខាងត្បូង">ចំការអង្គខាងត្បូង</option>
        <option value="ពងទឹកខាងត្បូង">ពងទឹកខាងត្បូង</option>
        <option value="អណ្ដូងថ្ម">អណ្ដូងថ្ម</option>
        <option value="ព្រៃតាខាប">ព្រៃតាខាប</option>
<!--             ឃុំលាយបូរ-->
        <option value="ជ្រែ">ជ្រែ</option>
        <option value="ខ្នារ">ខ្នារ</option>
        <option value="ព្រៃធាតុ">ព្រៃធាតុ</option>
        <option value="ត្រពាំងគូរ">ត្រពាំងគូរ</option>
        <option value="ទួលត្បែង">ទួលត្បែង</option>
        <option value="អង្គគគីរ">អង្គគគីរ</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ព្រៃគុយ">ព្រៃគុយ</option>
        <option value="អំពិល">អំពិល</option>
        <option value="អង្គតាកុប">អង្គតាកុប</option>
        <option value="បឹង">បឹង</option>
        <option value="កាច់ត្រក">កាច់ត្រក</option>
        <option value="ត្រពាំងត្រាច">ត្រពាំងត្រាច</option>
        <option value="ធ្យា">ធ្យា</option>
        <option value="ធ្នង់រលើង">ធ្នង់រលើង</option>
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="អង្គតាចាន់">អង្គតាចាន់</option>
        <option value="ប្រាសាទ">ប្រាសាទ</option>
        <option value="អង្គតានូ">អង្គតានូ</option>
        <option value="បាក់កូត">បាក់កូត</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="អង្គនារាយណ៍">អង្គនារាយណ៍</option>
        <option value="ស្លា">ស្លា</option>
        <option value="សីមា">សីមា</option>
        <option value="សែនបន">សែនបន</option>
<!--            ឃុំញ៉ែងញ៉ង-->
        <option value="ដូនទួត">ដូនទួត</option>
        <option value="សូទៃ">សូទៃ</option>
        <option value="ក្រញូង">ក្រញូង</option>
        <option value="ត្រពាំងស្នោ">ត្រពាំងស្នោ</option>
        <option value="គុស">គុស</option>
        <option value="ស្លែងកោង">ស្លែងកោង</option>
        <option value="តាតៃ">តាតៃ</option>
        <option value="ឫស្សីស្រុក">ឫស្សីស្រុក</option>
        <option value="អង្គតាសោម">អង្គតាសោម</option>
        <option value="អូរស្ងើន">អូរស្ងើន</option>
        <option value="កំសី">កំសី</option>
<!--            ឃុំអូរសារាយ-->
        <option value="ត្រពាំងដងទឹក">ត្រពាំងដងទឹក</option>
        <option value="ទួលខ្លុង">ទួលខ្លុង</option>
        <option value="សុក្រំ">សុក្រំ</option>
        <option value="ត្រពាំងក្រសាំង">ត្រពាំងក្រសាំង</option>
        <option value="ត្រពាំងក្រឡាញ់">ត្រពាំងក្រឡាញ់</option>
        <option value="បឹងសាទង">បឹងសាទង</option>
        <option value="ត្រពាំងភ្លូ">ត្រពាំងភ្លូ</option>
        <option value="ត្រពាំងខ្ចៅ">ត្រពាំងខ្ចៅ</option>
        <option value="ដំណាក់ខ្លុង">ដំណាក់ខ្លុង</option>
        <option value="ស្ទឹង">ស្ទឹង</option>
        <option value="ឫស្សីមួយគុម្ព">ឫស្សីមួយគុម្ព</option>
        <option value="ត្នោតជុំ">ត្នោតជុំ</option>
<!--            ឃុំត្រពាំងក្រញូង-->
        <option value="ខ្ពបស្វាយ">ខ្ពបស្វាយ</option>
        <option value="ត្រពាំងចក">ត្រពាំងចក</option>
        <option value="ត្រពាំងស្គា">ត្រពាំងស្គា</option>
        <option value="ត្រពាំងរបង">ត្រពាំងរបង</option>
        <option value="ត្រពាំងក្រញូង">ត្រពាំងក្រញូង</option>
        <option value="ព្រៃក្ដួច">ព្រៃក្ដួច</option>
        <option value="ផ្លូវលោក">ផ្លូវលោក</option>
        <option value="ព្រៃតាឡូយ">ព្រៃតាឡូយ</option>
        <option value="បុស្សតាផង់">បុស្សតាផង់</option>
<!--            ឃុំឧត្ដមសុរិយា-->
        <option value="ត្រពាំងរុន">ត្រពាំងរុន</option>
        <option value="ចុងអាង">ចុងអាង</option>
        <option value="ត្រពាំងត្រកៀត">ត្រពាំងត្រកៀត</option>
        <option value="អង្គរំពាក់">អង្គរំពាក់</option>
        <option value="ដកវ៉ាន">ដកវ៉ាន</option>
        <option value="តាសូរ">តាសូរ</option>
        <option value="តាលឿ">តាលឿ</option>
        <option value="រំពាក់ពេន">រំពាក់ពេន</option>
        <option value="ត្រពាំងថ្លាន់">ត្រពាំងថ្លាន់</option>
        <option value="ប្របសៀម">ប្របសៀម</option>
        <option value="រំលេចស្វាយ">រំលេចស្វាយ</option>
        <option value="ស្រង៉ែ">ស្រង៉ែ</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
<!--            ឃុំពពេល-->
        <option value="អង្គគគីរ">អង្គគគីរ</option>
        <option value="ត្រាវអែម">ត្រាវអែម</option>
        <option value="តាសិត">តាសិត</option>
        <option value="ព្រៃជួរ">ព្រៃជួរ</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="ក្ងោកពង">ក្ងោកពង</option>
        <option value="ដំណាក់រវាង">ដំណាក់រវាង</option>
        <option value="ចមពល">ចមពល</option>
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="រមៀត">រមៀត</option>
        <option value="ព្រៃមាន">ព្រៃមាន</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
<!--            ឃុំសំរោង-->
        <option value="ត្រពាំងចែង">ត្រពាំងចែង</option>
        <option value="កោះញ៉ៃ">កោះញ៉ៃ</option>
        <option value="ក្របីព្រៃ">ក្របីព្រៃ</option>
        <option value="ចានទាប">ចានទាប</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ក្រាំងបន្ទាយ">ក្រាំងបន្ទាយ</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ប៉ែនមាស">ប៉ែនមាស</option>
        <option value="តាស្មន់">តាស្មន់</option>
        <option value="ព្រៃតាដុក">ព្រៃតាដុក</option>
        <option value="ប្រសូត្រថ្មី">ប្រសូត្រថ្មី</option>
        <option value="ត្រពាំងថ្ម">ត្រពាំងថ្ម</option>
        <option value="តាសោម">តាសោម</option>
        <option value="ព្រៃគគីរ">ព្រៃគគីរ</option>
        <option value="តាប៉ែន">តាប៉ែន</option>
        <option value="សំបួរ">សំបួរ</option>
<!--            ឃុំស្រែរនោង-->
        <option value="ប្រវន័យ">ប្រវន័យ</option>
        <option value="ត្រពាំងថ្នល់">ត្រពាំងថ្នល់</option>
        <option value="ជ្រៃវែង">ជ្រៃវែង</option>
        <option value="សាមគ្គី">សាមគ្គី</option>
        <option value="ត្រពាំងរនោង">ត្រពាំងរនោង</option>
        <option value="ត្រាច">ត្រាច</option>
        <option value="គោករវៀង">គោករវៀង</option>
        <option value="ធំ">ធំ</option>
        <option value="តាកែវ">តាកែវ</option>
        <option value="ក្រាំងស្វាយ">ក្រាំងស្វាយ</option>
        <option value="ត្រពាំងរបើម">ត្រពាំងរបើម</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ប្រជុំ">ប្រជុំ</option>
        <option value="ស្រែឈើនៀង">ស្រែឈើនៀង</option>
        <option value="ព្រៃមោក">ព្រៃមោក</option>
        <option value="ស្រែថ្លុក">ស្រែថ្លុក</option>
        <option value="ត្រពាំងទម្លាប់">ត្រពាំងទម្លាប់</option>
<!--            ឃុំតាភេម-->
        <option value="ម្រុំ">ម្រុំ</option>
        <option value="ត្រពាំងអំពិល">ត្រពាំងអំពិល</option>
        <option value="តាម៉ុច">តាម៉ុច</option>
        <option value="អង្គគគីរ">អង្គគគីរ</option>
        <option value="តាភេម">តាភេម</option>
        <option value="តាសូ">តាសូ</option>
        <option value="ខ្លាគ្រហឹម">ខ្លាគ្រហឹម</option>
        <option value="លីញ៉ា">លីញ៉ា</option>
        <option value="ប្រស៊ុង">ប្រស៊ុង</option>
        <option value="ត្បែងទទឹង">ត្បែងទទឹង</option>
        <option​ value="ណងស្រាយ">ណងស្រាយ</option>
        <option value="អូរផុត">អូរផុត</option>
        <option value="មហាសេនា">មហាសេនា</option>
        <option value="លាងស្រាយ">លាងស្រាយ</option>
        <option value="តាគាំ">តាគាំ</option>
        <option value="ពោធិ៍ព្រះសង្ឃ">ពោធិ៍ព្រះសង្ឃ</option>
        <option value="ត្រពាំងកប្បាស">ត្រពាំងកប្បាស</option>
        <option value="ត្រពាំងស្វាយ">ត្រពាំងស្វាយ</option>
        <option value="បាខុងខាងកើត">បាខុងខាងកើត</option>
        <option value="បាខុងខាងលិច">បាខុងខាងលិច</option>
        <option value="តាមុំ">តាមុំ</option>
        <option value="ប្រវង់">ប្រវង់</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="តាកឹម">តាកឹម</option>
<!--            ឃុំត្រាំកក់-->
        <option value="យាយឡ">យាយឡ</option>
        <option value="ជ្រៃត្នោត">ជ្រៃត្នោត</option>
        <option value="ត្រពាំងរំពាក់">ត្រពាំងរំពាក់</option>
        <option value="នៀល">នៀល</option>
        <option value="ត្រពាំងកែស">ត្រពាំងកែស</option>
        <option value="ត្រពាំងចក">ត្រពាំងចក</option>
        <option value="កល់គម">កល់គម</option>
        <option value="អង្គរនាប">អង្គរនាប</option>
        <option value="ត្រពាំងឫស្សី">ត្រពាំងឫស្សី</option>
        <option value="ត្រពាំងខ្លូត">ត្រពាំងខ្លូត</option>
        <option value="បឹងម្កាក់">បឹងម្កាក់</option>
        <option value="ក្រាំងគរ">ក្រាំងគរ</option>
        <option value="ថ្មកែវ">ថ្មកែវ</option>
<!--            ឃុំត្រពាំងធំខាងជើង-->
        <option value="ពាក់បង្អោង">ពាក់បង្អោង</option>
        <option value="ព្រៃខ្វាវ">ព្រៃខ្វាវ</option>
        <option value="ត្រពាំងស្វាយ">ត្រពាំងស្វាយ</option>
        <option value="តាសួន">តាសួន</option>
        <option value="ព្រៃក្ដួច">ព្រៃក្ដួច</option>
        <option value="ព្រៃតាឡី">ព្រៃតាឡី</option>
        <option value="សំរ៉ង">សំរ៉ង</option>
        <option value="អង្គត្រាវ">អង្គត្រាវ</option>
        <option value="ពោធិដុះ">ពោធិដុះ</option>
        <option value="ព្រៃស្បាត">ព្រៃស្បាត</option>
        <option value="ព្រៃដក់ពរ">ព្រៃដក់ពរ</option>
<!--            ឃុំត្រពាំងធំខាងត្បូង-->
        <option value="ត្រពាំងកោះ">ត្រពាំងកោះ</option>
        <option value="ព្រៃក្ដី">ព្រៃក្ដី</option>
        <option value="ព្រៃរំដួល">ព្រៃរំដួល</option>
        <option value="ប្រគៀប">ប្រគៀប</option>
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="សំរោង">សំរោង</option>
        <option value="គោចិនលែង">គោចិនលែង</option>
        <option value="ត្រពាំងតាសោម">ត្រពាំងតាសោម</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="ត្រពាំងត្នោត">ត្រពាំងត្នោត</option>
        <option value="ត្រពាំងប្រីយ៍">ត្រពាំងប្រីយ៍</option>
        <option value="ត្រពាំងខន">ត្រពាំងខន</option>
        <option value="ព្រៃព្រាល">ព្រៃព្រាល</option>
<!--        ស្រុកទ្រាំង-->
<!--            ឃុំអង្កាញ់-->
        <option value="ព្រៃទូក">ព្រៃទូក</option>
        <option value="រលួស">រលួស</option>
        <option value="ត្រពាំងខ្លូត">ត្រពាំងខ្លូត</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
        <option value="តារេនជោ">តារេនជោ</option>
        <option value="ក្រាំងឈើនៀង">ក្រាំងឈើនៀង</option>
        <option value="ដឹកម៉ៃ">ដឹកម៉ៃ</option>
        <option value="អង្គរការ">អង្គរការ</option>
<!--            ឃុំអង្គខ្នុរ-->
        <option value="ស្ពឺ">ស្ពឺ</option>
        <option value="គ">គ</option>
        <option value="ត្រពាំងប្រី">ត្រពាំងប្រី</option>
        <option value="ត្រពាំងរកា">ត្រពាំងរកា</option>
        <option value="ព្រៃមាស">ព្រៃមាស</option>
        <option value="ស្រែវង្ស">ស្រែវង្ស</option>
<!--            ឃុំជីខ្មា-->
        <option value="ថ្កូវ">ថ្កូវ</option>
        <option value="ជួស">ជួស</option>
        <option value="ឆ្កែស្លាប់">ឆ្កែស្លាប់</option>
        <option value="អណ្ដែងសាំងជ្វា">អណ្ដែងសាំងជ្វា</option>
        <option value="អណ្ដែងសាំងខ្មែរ">អណ្ដែងសាំងខ្មែរ</option>
        <option value="ជ្រែ">ជ្រែ</option>
        <option value="រកា">រកា</option>
        <option value="យុលចេក">យុលចេក</option>
        <option value="បន្ទាយ">បន្ទាយ</option>
        <option value="ជីខ្មា">ជីខ្មា</option>
<!--            ឃុំខ្វាវ-->
        <option value="ពង្រ">ពង្រ</option>
        <option value="តាសឹង">តាសឹង</option>
        <option value="ក្ដី្ករុន">ក្ដី្ករុន</option>
        <option value="កកោះ">កកោះ</option>
        <option value="ស្រម៉លើ">ស្រម៉លើ</option>
        <option value="ធម្មតា">ធម្មតា</option>
        <option value="ស្រម៉ក្រោម">ស្រម៉ក្រោម</option>
        <option value="តាស្រែន">តាស្រែន</option>
        <option value="ដូនភើ">ដូនភើ</option>
        <option value="ព្រឹសលើ">ព្រឹសលើ</option>
        <option value="ព្រឹសក្រោម">ព្រឹសក្រោម</option>
<!--            ឃុំប្រាំបីមុំ-->
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ដំណាក់រាជា">ដំណាក់រាជា</option>
        <option value="ដង្កោ">ដង្កោ</option>
        <option value="ឈើទាលភ្លោះ">ឈើទាលភ្លោះ</option>
        <option value="ប្រាំបីមុំ">ប្រាំបីមុំ</option>
        <option value="ក្រាំងកណ្ដាល">ក្រាំងកណ្ដាល</option>
        <option value="ត្រពាំងលើក">ត្រពាំងលើក</option>
        <option value="ក្រាំងស្បែក">ក្រាំងស្បែក</option>
        <option value="ពញ្ញាឮ">ពញ្ញាឮ</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="រវៀង">រវៀង</option>
        <option value="ល្វេថ្មី">ល្វេថ្មី</option>
<!--            ឃុំអង្គកែវ-->
        <option value="អូរក្រឡង់ដួល">អូរក្រឡង់ដួល</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="អង្គកែវ">អង្គកែវ</option>
        <option value="ព្រៃរំពាក់">ព្រៃរំពាក់</option>
        <option value="ធ្នោះ">ធ្នោះ</option>
        <option value="ត្រពាំងស្នោ">ត្រពាំងស្នោ</option>
        <option value="សេរីដួច">សេរីដួច</option>
        <option value="អូរតាសេក">អូរតាសេក</option>
<!--            ឃុំព្រៃស្លឹក-->
        <option value="ព្រៃសណ្ដែក">ព្រៃសណ្ដែក</option>
        <option value="ស្រែច្រក">ស្រែច្រក</option>
        <option value="គោកញ័រ">គោកញ័រ</option>
        <option value="នៀល">នៀល</option>
        <option value="ព្រៃមៀន">ព្រៃមៀន</option>
        <option value="ព្រៃស្លឹក">ព្រៃស្លឹក</option>
        <option value="ភ្នំថ្នក់">ភ្នំថ្នក់</option>
        <option value="ដើមផ្ដៀក">ដើមផ្ដៀក</option>
        <option value="ត្រពាំងតាមូង">ត្រពាំងតាមូង</option>
        <option value="បារាយណ៍">បារាយណ៍</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ស្វាយរំដេង">ស្វាយរំដេង</option>
        <option value="ភ្នំខ្លែង">ភ្នំខ្លែង</option>
        <option value="ត្រពាំងអង្គ">ត្រពាំងអង្គ</option>
        <option value="សង្ហា">សង្ហា</option>
        <option value="សំរោងមានជ័យ">សំរោងមានជ័យ</option>
        <option value="សូរ្យច័ន្ទ">សូរ្យច័ន្ទ</option>
<!--            សូរ្យច័ន្ទ-->
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="ត្រពាំងរំពាក់">ត្រពាំងរំពាក់</option>
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
        <option value="ធម្មតា">ធម្មតា</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="ព្រៃផ្អាវ">ព្រៃផ្អាវ</option>
        <option value="ខ្លោងទ្វារ">ខ្លោងទ្វារ</option>
        <option value="ត្រពាំងខ្នុរ">ត្រពាំងខ្នុរ</option>
        <option value="ព្រៃផ្អេរ">ព្រៃផ្អេរ</option>
        <option value="គោកឃ្មោង">គោកឃ្មោង</option>
        <option value="សូភី">សូភី</option>
<!--            ឃុំសំបួរ-->
        <option value="ព្រៃផ្ដៅ">ព្រៃផ្ដៅ</option>
        <option value="រវៀង">រវៀង</option>
        <option value="ព្រៃដកពរ">ព្រៃដកពរ</option>
        <option value="ព្រៃភ្លង">ព្រៃភ្លង</option>
        <option value="ពោធិ៍">ពោធិ៍</option>
        <option value="តាប្រឹម">តាប្រឹម</option>
        <option value="ត្រពាំងពន្លុះ">ត្រពាំងពន្លុះ</option>
        <option value="ក្បាលពោធិ៍">ក្បាលពោធិ៍</option>
        <option value="ត្នោតជុំ">ត្នោតជុំ</option>
        <option value="អូរពោធិ៍">អូរពោធិ៍</option>
<!--            ឃុំសន្លុង-->
        <option value="អង្គតាភោគ">អង្គតាភោគ</option>
        <option value="អង្គប្រាង្គរ">អង្គប្រាង្គរ</option>
        <option value="ជ្រៃ">ជ្រៃ</option>
        <option value="តាព្រះ">តាព្រះ</option>
        <option value="ក្រាំងត្នោត">ក្រាំងត្នោត</option>
        <option value="ក្រសាំង">ក្រសាំង</option>
        <option value="ចិន">ចិន</option>
        <option value="ពន្លៃ">ពន្លៃ</option>
        <option value="ស្រះតាកួន">ស្រះតាកួន</option>
        <option value="ជីច្រាប">ជីច្រាប</option>
        <option value="លោក">លោក</option>
        <option value="ក្រោម">ក្រោម</option>
<!--            ឃុំស្មោង-->
        <option value="ស្មោង">ស្មោង</option>
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="កំពង់ជ្រៃ">កំពង់ជ្រៃ</option>
        <option value="ស្គុល">ស្គុល</option>
        <option value="ត្រពាំងលើក">ត្រពាំងលើក</option>
<!--            ឃុំស្រង៉ែ-->
        <option value="ត្នោត">ត្នោត</option>
        <option value="ព្រៃនប់">ព្រៃនប់</option>
        <option value="ច្រាំង">ច្រាំង</option>
        <option value="ត្រពាំងបបុស្ស">ត្រពាំងបបុស្ស</option>
        <option value="ត្របែក">ត្របែក</option>
        <option value="ម៉ឺនទំរង់">ម៉ឺនទំរង់</option>
        <option value="គក">គក</option>
        <option value="ស្វាយអំពារ">ស្វាយអំពារ</option>
        <option value="កន្ទួត">កន្ទួត</option>
        <option value="ព្រៃរុន្ធ">ព្រៃរុន្ធ</option>
        <option value="ស្នួល">ស្នួល</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="ព្រៃចង្រៀក">ព្រៃចង្រៀក</option>
        <option value="គោកអំពៅ">គោកអំពៅ</option>
        <option value="ពុទ្ធសាំ">ពុទ្ធសាំ</option>
<!--            ឃុំធ្លក-->
        <option value="ត្រពាំងស្លា">ត្រពាំងស្លា</option>
        <option value="រវៀង">រវៀង</option>
        <option value="ស្វាយ">ស្វាយ</option>
        <option value="ក្រាំងធំ">ក្រាំងធំ</option>
        <option value="ក្រាំងដៃ">ក្រាំងដៃ</option>
        <option value="ចិន">ចិន</option>
        <option value="ព្រៃវែង">ព្រៃវែង</option>
        <option value="ឈើទាលបាក់">ឈើទាលបាក់</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="ក្រាំងតូច">ក្រាំងតូច</option>
        <option value="ក្ដីត្នោត">ក្ដីត្នោត</option>
        <option value="សំរោង">សំរោង</option>
        <option value="សំណខ្មៅ">សំណខ្មៅ</option>
        <option value="ក្រាំងធ្នង់">ក្រាំងធ្នង់</option>
        <option value="ក្រាំងរោង">ក្រាំងរោង</option>
        <option value="ពន្លៃ">ពន្លៃ</option>
        <option value="ល្វេ">ល្វេ</option>
        <option value="ជ្រុងគ្រីស">ជ្រុងគ្រីស</option>
<!--            ឃុំត្រឡាច-->
        <option value="ទន្លេ">ទន្លេ</option>
        <option value="ឆឹស">ឆឹស</option>
        <option value="ត្រឡាច">ត្រឡាច</option>
        <option value="សំរោង">សំរោង</option>
        <option value="ជីងោ">ជីងោ</option>
        <option value="អង្គុញ">អង្គុញ</option>
        <option value="កន្ទួតធំ">កន្ទួតធំ</option>
        <option value="កន្ទួតតូច">កន្ទួតតូច</option>
        <option value="ពោន">ពោន</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
<!--    ខេត្តកំពត-->
<!--        ស្រុកអង្គរជ័យ-->
<!--            ឃុំអង្គភ្នំតូច-->
        <option value="ត្រពាំងកំភ្លាញ">ត្រពាំងកំភ្លាញ</option>
        <option value="ត្រពាំងខ្យង">ត្រពាំងខ្យង</option>
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="ពែងធំ">ពែងធំ</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
<!--            ឃុំអង្គរជ័យ-->
        <option value="សាមគ្គី">សាមគ្គី</option>
        <option value="ត្រពាំងស្រែ">ត្រពាំងស្រែ</option>
        <option value="អង្គរជ័យលើ">អង្គរជ័យលើ</option>
        <option value="អង្គរជ័យក្រោម">អង្គរជ័យក្រោម</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="អង្គរំដួល">អង្គរំដួល</option>
<!--            ឃុំចំប៉ី-->
        <option value="ខ្វាវ">ខ្វាវ</option>
        <option value="ក្រសាំង">ក្រសាំង</option>
        <option value="ទន្លេនាម">ទន្លេនាម</option>
        <option value="ណងសាហេតុ">ណងសាហេតុ</option>
        <option value="ចំប៉ី">ចំប៉ី</option>
        <option value="ស្រែជា">ស្រែជា</option>
        <option value="អង្គជោតិ">អង្គជោតិ</option>
        <option value="ដើមពោធិ៍">ដើមពោធិ៍</option>
<!--            ឃុំដំបូកខ្ពស់-->
        <option value="ត្រពាំងត្រកៀត">ត្រពាំងត្រកៀត</option>
        <option value="ស្បូវអណ្ដែត">ស្បូវអណ្ដែត</option>
        <option value="ចាក់ជ្រុំ">ចាក់ជ្រុំ</option>
        <option value="តាកោ">តាកោ</option>
        <option value="ដងទង់">ដងទង់</option>
        <option value="ត្រពាំងត្នោត">ត្រពាំងត្នោត</option>
        <option value="របោះខ្ទុម">របោះខ្ទុម</option>
        <option value="ដំបូកខ្ពស់">ដំបូកខ្ពស់</option>
        <option value="ត្រពាំងកំណប់">ត្រពាំងកំណប់</option>
        <option value="ត្រពាំងរុន">ត្រពាំងរុន</option>
        <option value="អូរម្កាក់">អូរម្កាក់</option>
<!--            ឃុំដានគោម-->
        <option value="ពន្លៃ">ពន្លៃ</option>
        <option value="ដានគោមខាងជើង">ដានគោមខាងជើង</option>
        <option value="ដានគោមខាងត្ប្មូង">ដានគោមខាងត្ប្មូង</option>
        <option value="អង្គខ្ជាយខាងជើង">អង្គខ្ជាយខាងជើង</option>
        <option value="អង្គខ្ជាយខាងត្បូង">អង្គខ្ជាយខាងត្បូង</option>
        <option value="ព្រៃខ្ជាយ">ព្រៃខ្ជាយ</option>
<!--            ឃុំម្រោម-->
        <option value="ព្រះឱង្ការ">ព្រះឱង្ការ</option>
        <option value="ម្រោម">ម្រោម</option>
        <option value="ត្រពាំងផ្នែល">ត្រពាំងផ្នែល</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="គក">គក</option>
        <option value="ភ្នំឆ្មារ">ភ្នំឆ្មារ</option>
<!--             ឃុំភ្នំកុង-->
        <option value="ស្រែរោង">ស្រែរោង</option>
        <option value="ស្គរទូង">ស្គរទូង</option>
        <option value="ត្រពាំងចក">ត្រពាំងចក</option>
        <option value="ព្រៃផ្គាំ">ព្រៃផ្គាំ</option>
        <option value="ពោធិ៍">ពោធិ៍</option>
        <option value="ពេជចង្វា">ពេជចង្វា</option>
        <option value="ព្រៃផ្ដៅ">ព្រៃផ្ដៅ</option>
        <option value="ភ្នំត្រែល">ភ្នំត្រែល</option>
        <option value="ត្រពាំងខ្វា">ត្រពាំងខ្វា</option>
        <option value="សង្កែបង្វែ">សង្កែបង្វែ</option>
<!--            ឃុំប្រភ្នំ-->
        <option value="ប្រភ្នំ">ប្រភ្នំ</option>
        <option value="ត្រពាំងថ្ងាន់">ត្រពាំងថ្ងាន់</option>
        <option value="ព្រៃឈើទាល">ព្រៃឈើទាល</option>
        <option value="ព្រៃស្គរ">ព្រៃស្គរ</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ក្រាំងថ្កូវ">ក្រាំងថ្កូវ</option>
        <option value="ដំណាក់ក្រសាំង">ដំណាក់ក្រសាំង</option>
        <option value="ឫស្សីដុំ">ឫស្សីដុំ</option>
        <option value="ទួលខ្ពស់">ទួលខ្ពស់</option>
<!--            ឃុំសំឡាញ-->
        <option value="កសិករ">កសិករ</option>
        <option value="ស្ដុក">ស្ដុក</option>
        <option value="កម្មករ">កម្មករ</option>
        <option value="ត្រពាំងស្លា">ត្រពាំងស្លា</option>
        <option value="ប្រស្រែ">ប្រស្រែ</option>
        <option value="ស្មោង">ស្មោង</option>
        <option value="ត្របៀត">ត្របៀត</option>
<!--            ឃុំតានី-->
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
        <option value="តាព្រាម">តាព្រាម</option>
        <option value="កាន់ទ្រង់">កាន់ទ្រង់</option>
        <option value="តាកុក">តាកុក</option>
        <option value="ឫស្សី">ឫស្សី</option>
        <option value="ត្រពាំងរុន">ត្រពាំងរុន</option>
        <option value="ប្រាល">ប្រាល</option>
<!--        ស្រុកបន្ទាយមាស-->
<!--            ឃុំបន្ទាយមាសខាងកើត-->
        <option value="ពេជ្ជនា">ពេជ្ជនា</option>
        <option value="ក្រសាំងក្រោម">ក្រសាំងក្រោម</option>
        <option value="ក្រសាំងលើ">ក្រសាំងលើ</option>
        <option value="ត្រពាំងពោន">ត្រពាំងពោន</option>
        <option value="ព្រៃបន្ទាំ">ព្រៃបន្ទាំ</option>
        <option value="ស្រែថ្លុក">ស្រែថ្លុក</option>
        <option value="ពោធិ៍">ពោធិ៍</option>
        <option value="ធ្នង់">ធ្នង់</option>
<!--            ឃុំបន្ទាយមាសខាងលិច-->
        <option value="លំទំពូង">លំទំពូង</option>
        <option value="កណ្ដាញ់">កណ្ដាញ់</option>
        <option value="កន្សោមអក">កន្សោមអក</option>
        <option value="ទង់លាង">ទង់លាង</option>
        <option value="ត្បាល់កិន">ត្បាល់កិន</option>
        <option value="ព្រៃលើ">ព្រៃលើ</option>
        <option value="ហុងស៊ុយ">ហុងស៊ុយ</option>
        <option value="បន្ទាយមាស">បន្ទាយមាស</option>
        <option value="ក្រាំងបន្ទាយ">ក្រាំងបន្ទាយ</option>
<!--             ឃុំព្រៃទន្លេ-->
        <option value="ព្រៃទន្លេ">ព្រៃទន្លេ</option>
        <option value="ទួលក្រសាំង">ទួលក្រសាំង</option>
        <option value="ព្រែកឫស្សី">ព្រែកឫស្សី</option>
        <option value="សំរោងចិន">សំរោងចិន</option>
        <option value="ម៉ឺនដាំ">ម៉ឺនដាំ</option>
<!--            ឃុំសំរោងក្រោម-->
        <option value="ត្រពាំងមន្ដ្រី">ត្រពាំងមន្ដ្រី</option>
        <option value="តាអៀក">តាអៀក</option>
        <option value="សែនពន្លួង">សែនពន្លួង</option>
        <option value="សំរោង">សំរោង</option>
        <option value="តាទេន">តាទេន</option>
        <option value="ក្រាំងដូង">ក្រាំងដូង</option>
<!--            ឃុំសំរោងលើ-->
        <option value="បរិវាស">បរិវាស</option>
        <option value="ដំណាក់ចំបក់">ដំណាក់ចំបក់</option>
        <option value="ដំណាក់ត្រយឹង">ដំណាក់ត្រយឹង</option>
        <option value="ត្រាំសសរ">ត្រាំសសរ</option>
        <option value="ប្រផុង">ប្រផុង</option>
<!--            ឃុំស្ដេចគង់ខាងជើង-->
        <option value="ជ្រុងស្រឡៅ">ជ្រុងស្រឡៅ</option>
        <option value="កន្លង់">កន្លង់</option>
        <option value="ត្រែង">ត្រែង</option>
        <option value="ពងទឹក">ពងទឹក</option>
<!--            ឃុំស្ដេចគង់ខាងលិច-->
        <option value="ព្រៃពព្រិច">ព្រៃពព្រិច</option>
        <option value="រំពើន">រំពើន</option>
        <option value="ចម្លងជ្រៃ">ចម្លងជ្រៃ</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="ដើមចំរៀក">ដើមចំរៀក</option>
<!--            ឃុំស្ដេចគង់ខាងត្បូង-->
        <option value="សង្កែដួច">សង្កែដួច</option>
        <option value="ឫស្សីជួរ">ឫស្សីជួរ</option>
        <option value="ភ្នំតូច">ភ្នំតូច</option>
        <option value="ស្ដេចគង់">ស្ដេចគង់</option>
<!--            ឃុំត្នោតចុងស្រង់-->
        <option value="តាឡង់">តាឡង់</option>
        <option value="គោកវែង">គោកវែង</option>
        <option value="ត្នោត">ត្នោត</option>
        <option value="ដូនទាវ">ដូនទាវ</option>
        <option value="ឫស្សីជុំ">ឫស្សីជុំ</option>
        <option value="ខ្នាយ">ខ្នាយ</option>
        <option value="សូរិយា">សូរិយា</option>
        <option value="អង្គញ់">អង្គញ់</option>
<!--            ឃុំត្រពាំងសាលាខាងកើត-->
        <option value="ទ័ពស្ដេច">ទ័ពស្ដេច</option>
        <option value="តាងើត">តាងើត</option>
        <option value="ពោន">ពោន</option>
        <option value="លៀប">លៀប</option>
        <option value="ខ្យើម">ខ្យើម</option>
<!--            ឃុំត្រពាំងសាលាខាងលិច-->
        <option value="ត្រពាំងក្ដុល">ត្រពាំងក្ដុល</option>
        <option value="គាថាវង្សក្រោម">គាថាវង្សក្រោម</option>
        <option value="គាថាវង្សលើ">គាថាវង្សលើ</option>
        <option value="សំព័រ">សំព័រ</option>
        <option value="ព្រៃស្រឡៅ">ព្រៃស្រឡៅ</option>
<!--            ឃុំទូកមាសខាងកើត-->
        <option value="ត្រពាំងព្រីង">ត្រពាំងព្រីង</option>
        <option value="ស្រែកាន់ចិន">ស្រែកាន់ចិន</option>
        <option value="ច្រកស្ដៅ">ច្រកស្ដៅ</option>
        <option value="ស្រែកាន់ខាងកើត">ស្រែកាន់ខាងកើត</option>
        <option value="ស្រែកាន់ខាងលិច">ស្រែកាន់ខាងលិច</option>
        <option value="អង្គម្លី">អង្គម្លី</option>
<!--            ឃុំទូកមាសខាងលិច-->
        <option value="ទូកមាស">ទូកមាស</option>
        <option value="កោះទន្សែ">កោះទន្សែ</option>
        <option value="ច្រកឃ្លៃ">ច្រកឃ្លៃ</option>
        <option value="ចំលងជ្រៃ">ចំលងជ្រៃ</option>
        <option value="ព្រៃធំ">ព្រៃធំ</option>
        <option value="ព្រៃចេក">ព្រៃចេក</option>
        <option value="ព្រៃក្រឡាខាងកើត">ព្រៃក្រឡាខាងកើត</option>
        <option value="ព្រៃក្រឡាខាងលិច">ព្រៃក្រឡាខាងលិច</option>
<!--            ឃុំវត្ដអង្គខាងជើង-->
        <option value="ស្រែត្រែង">ស្រែត្រែង</option>
        <option value="ត្នោតរលើង">ត្នោតរលើង</option>
        <option value="ពញាអង្គរ">ពញាអង្គរ</option>
        <option value="ស្វាយផ្អែម">ស្វាយផ្អែម</option>
        <option value="សូប៉េង">សូប៉េង</option>
<!--            ឃុំវត្ដអង្គខាងត្បូង-->
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ខ្នាច">ខ្នាច</option>
        <option value="ធាយ">ធាយ</option>
        <option value="ទទឹម">ទទឹម</option>
        <option value="ស្រែព្រៃ">ស្រែព្រៃ</option>
<!--         ស្រុកឈូក-->
<!--            ឃុំបានៀវ-->
        <option value="តាឡង់">តាឡង់</option>
        <option value="ព្រៃឈើនៀង">ព្រៃឈើនៀង</option>
        <option value="តាមុំ">តាមុំ</option>
        <option value="ព្រៃភ្ជុំ">ព្រៃភ្ជុំ</option>
        <option value="ត្រពាំងតាសេក">ត្រពាំងតាសេក</option>
<!--            ឃុំតាកែន-->
        <option value="ចំការមន">ចំការមន</option>
        <option value="វាលក្រសាំង">វាលក្រសាំង</option>
        <option value="ជ័យតាស្វាយ">ជ័យតាស្វាយ</option>
        <option value="ដំណាក់ត្រយឹង">ដំណាក់ត្រយឹង</option>
        <option value="ត្រពាំងបី">ត្រពាំងបី</option>
        <option value="ស្រកានាគ">ស្រកានាគ</option>
        <option value="ខ្ពប">ខ្ពប</option>
        <option value="ជ័យសេនា">ជ័យសេនា</option>
        <option value="ត្រពាំងក្ដី">ត្រពាំងក្ដី</option>  
        <option value="ត្រពាំងរុន">ត្រពាំងរុន</option>
        <option value="ភ្នំព្រាល">ភ្នំព្រាល</option>
        <option value="មនោសុខ">មនោសុខ</option>
<!--            ឃុំបឹងនិមល-->
        <option value="ស្រង៉ែ">ស្រង៉ែ</option>
        <option value="ព្រេច">ព្រេច</option>
        <option value="ពោធិ៍ដុះ">ពោធិ៍ដុះ</option>
        <option value="បឹង">បឹង</option>
<!--            ឃុំឈូក-->
        <option value="ក្រសាំង">ក្រសាំង</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="ក្រហូង">ក្រហូង</option>
<!--            ឃុំដូនយ៉យ-->
        <option value="ព្រៃឃ្មុំ">ព្រៃឃ្មុំ</option>
        <option value="ក្រសាំងមានជ័យ">ក្រសាំងមានជ័យ</option>
        <option value="ត្រពាំងកកោះ">ត្រពាំងកកោះ</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ដូនយ៉យ">ដូនយ៉យ</option>
<!--            ឃុំក្រាំងស្បូវ-->
        <option value="ជ័រដុំ">ជ័រដុំ</option>
        <option value="ត្រពាំងលើក">ត្រពាំងលើក</option>
        <option value="ក្រាំងម្ដេង">ក្រាំងម្ដេង</option>
        <option value="ក្រាំងស្បូវ">ក្រាំងស្បូវ</option>
        <option value="មន">មន</option>
<!--            ឃុំក្រាំងស្នាយ-->
        <option value="តូច">តូច</option>
        <option value="តានាន់">តានាន់</option>
        <option value="ដំណាក់ទ្រព្យខាងជើង">ដំណាក់ទ្រព្យខាងជើង</option>
        <option value="ដំណាក់ទ្រព្យខាងត្បូង">ដំណាក់ទ្រព្យខាងត្បូង</option>
        <option value="ល្វេ">ល្វេ</option>
        <option value="ក្រាំងរលួស">ក្រាំងរលួស</option>
<!--            ឃុំល្បើក-->
        <option value="ស្រែជ្រៅ">ស្រែជ្រៅ</option>
        <option value="ទួល">ទួល</option>
        <option value="ត្រពាំងគុយ">ត្រពាំងគុយ</option>
        <option value="ត្រពាំងថ្ម">ត្រពាំងថ្ម</option>
        <option value="ត្រពាំងគគីរ">ត្រពាំងគគីរ</option>
<!--            ឃុំត្រពាំងភ្លាំង-->
        <option value="កោះស្លា">កោះស្លា</option>
        <option value="ដុំផ្ដៅ">ដុំផ្ដៅ</option>
        <option value="ព្រៃពាយ">ព្រៃពាយ</option>
        <option value="ទួលដូនតី">ទួលដូនតី</option>
        <option value="ស្រែលាវ">ស្រែលាវ</option>
        <option value="ត្រពាំងស្ដៅ">ត្រពាំងស្ដៅ</option>
<!--            ឃុំមានជ័យ-->
        <option value="អង្គស្វាយ">អង្គស្វាយ</option>
        <option value="តាព្រុំ">តាព្រុំ</option>
        <option value="ទុល">ទុល</option>
        <option value="ព្រៃត្រាង">ព្រៃត្រាង</option>
        <option value="វាលត្បាល់">វាលត្បាល់</option>
<!--            ឃុំនារាយណ៍-->
        <option value="នារាយណ៍">នារាយណ៍</option>
        <option value="ពស់ពង">ពស់ពង</option>
        <option value="ព្រៃស្បូវ">ព្រៃស្បូវ</option>
        <option value="ខ្នាចរមាស">ខ្នាចរមាស</option>
<!--            ឃុំសត្វពង-->
        <option value="សត្វពង">សត្វពង</option>
        <option value="ព្រៃបែន">ព្រៃបែន</option>
        <option value="ត្រពាំងអណ្ដូង">ត្រពាំងអណ្ដូង</option>
        <option value="ជ្រៃ">ជ្រៃ</option>
<!--            ឃុំត្រពាំងបី-->
        <option value="ដំណាក់ឈ្នួល">ដំណាក់ឈ្នួល</option>
        <option value="ត្រពាំងល្បើក">ត្រពាំងល្បើក</option>
        <option value="ត្រពាំងបី">ត្រពាំងបី</option>
        <option value="កោះឫស្សី">កោះឫស្សី</option>
<!--            ឃុំត្រមែង-->
        <option value="ឈើទាលជ្រុំ">ឈើទាលជ្រុំ</option>
        <option value="ត្រមែង">ត្រមែង</option>
        <option value="ប្រមូល">ប្រមូល</option>
        <option value="អង្គឃ្លៃ">អង្គឃ្លៃ</option>
        <option value="ត្រពាំងបឹង">ត្រពាំងបឹង</option>
<!--            ឃុំតេជោអភិវឌ្ឍន៍-->
        <option value="តេជោអភិវឌ្ឍន៌">តេជោអភិវឌ្ឍន៌</option>
        <option value="តេជោក្បាលដំរី">តេជោក្បាលដំរី</option>
        <option value="តេជោអង្កាញ់">តេជោអង្កាញ់</option>
        <option value="តេជោពង្រក">តេជោពង្រក</option>
        <option value="តេជោជ្រៃបាក់">តេជោជ្រៃបាក់</option>
        <option value="តេជោអន្លងក្មេងលេង">តេជោអន្លងក្មេងលេង</option>
<!--        ស្រុកជុំគិរី-->
<!--            ឃុំច្រេស-->
        <option value="តាទែន">តាទែន</option>
        <option value="ត្រពាំងឈើទាល">ត្រពាំងឈើទាល</option>
        <option value="ធ្មា">ធ្មា</option>
        <option value="ច្រេស">ច្រេស</option>
<!--            ឃុំជំពូវន្ដ-->
        <option value="ឃ្លៃ">ឃ្លៃ</option>
        <option value="មនោណុប">មនោណុប</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ឈើទាល">ឈើទាល</option>
        <option value="ចេក">ចេក</option>
        <option value="ដំរីកូន">ដំរីកូន</option>
        <option value="ថ្មី">ថ្មី</option>
<!--            ឃុំស្នាយអញ្ជិត-->
        <option value="ស្នាយអញ្ជិត">ស្នាយអញ្ជិត</option>
        <option value="ដូនឪ">ដូនឪ</option>
        <option value="តាភុល">តាភុល</option>
        <option value="ព្រៃឃ្ជាយ">ព្រៃឃ្ជាយ</option>
        <option value="ដំណាក់ឈើក្រំ">ដំណាក់ឈើក្រំ</option>
<!--            ឃុំស្រែចែង-->
        <option value="កំណប់">កំណប់</option>
        <option value="ខ្ពបរុន">ខ្ពបរុន</option>
        <option value="ព្រៃឃ្លៃ">ព្រៃឃ្លៃ</option>
        <option value="ពងទឹក">ពងទឹក</option>
        <option value="ស្រែចែង">ស្រែចែង</option>
<!--            ឃុំស្រែក្នុង-->
        <option value="ដូង">ដូង</option>
        <option value="ព្រៃវែង">ព្រៃវែង</option>
        <option value="ព្រៃខ្មៅ">ព្រៃខ្មៅ</option>
        <option value="ព្រៃយ៉ាវ">ព្រៃយ៉ាវ</option>
        <option value="ដំណាក់ឈ្នួល">ដំណាក់ឈ្នួល</option>
        <option value="ត្បែងពក">ត្បែងពក</option>
<!--            ឃុំស្រែសំរោង-->
        <option value="ធ្លកយុល">ធ្លកយុល</option>
        <option value="ស្រែសំរោង">ស្រែសំរោង</option>
        <option value="ត្រពាំងប្រី">ត្រពាំងប្រី</option>
        <option value="រកាថ្មី">រកាថ្មី</option>
        <option value="ពោធិ៍">ពោធិ៍</option>
<!--            ឃុំត្រពាំងរាំង-->
        <option value="ត្រពាំងវែង">ត្រពាំងវែង</option>
        <option value="រវៀង">រវៀង</option>
        <option value="តារាជ">តារាជ</option>
        <option value="ត្រពាំងស្គន់">ត្រពាំងស្គន់</option>
        <option value="អូរ">អូរ</option>
<!--        ស្រុកដងទង់-->
<!--            ឃុំដំណាក់សុក្រំ-->
        <option value="ត្រពាំងតាមាស">ត្រពាំងតាមាស</option>
        <option value="អង្គ រពាក់">អង្គ រពាក់</option>
        <option value="ឃ្ជាយខាងលិច">ឃ្ជាយខាងលិច</option>
        <option value="ត្រពាំងឫស្សី">ត្រពាំងឫស្សី</option>
        <option value="ក្រាំងអំពៅ">ក្រាំងអំពៅ</option>
<!--            ឃុំដងទង់-->
        <option value="ធំថ្មី">ធំថ្មី</option>
        <option value="ច្រកកែស">ច្រកកែស</option>
        <option value="ត្រពាំងម្នាស់">ត្រពាំងម្នាស់</option>
        <option value="ឃ្ជាយខាងជើង">ឃ្ជាយខាងជើង</option>
        <option value="ត្រពាំងវែងខាងកើត">ត្រពាំងវែងខាងកើត</option>
        <option value="ត្រពាំងវែងខាងលិច">ត្រពាំងវែងខាងលិច</option>
        <option value="ស្រូវលើ">ស្រូវលើ</option>
        <option value="ឃ្ជាយខាងត្បូង">ឃ្ជាយខាងត្បូង</option>
        <option value="ព្រៃពក">ព្រៃពក</option>
        <option value="ព្រៃគគីរ">ព្រៃគគីរ</option>
        <option value="ស្រូវក្រោម">ស្រូវក្រោម</option>
<!--            ឃុំឃ្ជាយខាងជើង-->
        <option value="ព្រៃឃ្មុំ">ព្រៃឃ្មុំ</option>
        <option value="ព្រៃសំណាងលើ">ព្រៃសំណាងលើ</option>
        <option value="ព្រៃសំណាងក្រោម">ព្រៃសំណាងក្រោម</option>
        <option value="កំរ៉ែងក្រសាំង">កំរ៉ែងក្រសាំង</option>
        <option value="ដំណាក់">ដំណាក់</option>
<!--            ឃុំខ្ជាយខាងត្បូង-->
        <option value="ដំណាក់ត្រយឹង">ដំណាក់ត្រយឹង</option>
        <option value="ឈ្លីតក្រោម">ឈ្លីតក្រោម</option>
        <option value="ឈ្លីតលើ">ឈ្លីតលើ</option>
        <option value="ត្រពាំងអណ្ដូង">ត្រពាំងអណ្ដូង</option>
<!--            ឃុំមានរិទ្ធិ-->
        <option value="ព្រៃក្រាំងខាងជើង">ព្រៃក្រាំងខាងជើង</option>
        <option value="ព្រៃក្រាំងខាងត្បូង">ព្រៃក្រាំងខាងត្បូង</option>
        <option value="ក្រាំងលាវ">ក្រាំងលាវ</option>
        <option value="ត្រពាំងឈូក">ត្រពាំងឈូក</option>
<!--            ឃុំស្រែជាខាងជើង-->
        <option value="សុភី">សុភី</option>
        <option value="ប្រីពីរ">ប្រីពីរ</option>
        <option value="ព្រានទុំ">ព្រានទុំ</option>
        <option value="ត្រពាំងច្រនៀង">ត្រពាំងច្រនៀង</option>
<!--            ឃុំស្រែជាខាងត្បូង-->
        <option value="ត្រពាំងក្ដារ">ត្រពាំងក្ដារ</option>
        <option value="ខ្ពស់">ខ្ពស់</option>
        <option value="អូរឫស្សី">អូរឫស្សី</option>
<!--            ឃុំទទុង-->
        <option value="ភ្នំតូច">ភ្នំតូច</option>
        <option value="ទួលខ្ពស់">ទួលខ្ពស់</option>
        <option value="ចង្កៀងខាងកើត">ចង្កៀងខាងកើត</option>
        <option value="ចង្កៀងខាងលិច">ចង្កៀងខាងលិច</option>
        <option value="ស្ដុកធ្លក">ស្ដុកធ្លក</option>
        <option value="ដំរីលេង">ដំរីលេង</option>
        <option value="ត្រពាំងនៀល">ត្រពាំងនៀល</option>
        <option value="អូរកណ្តោល">អូរកណ្តោល</option>
        <option value="ឃ្ជាយខាងកើត">ឃ្ជាយខាងកើត</option>
<!--            ឃុំអង្គ រមាស-->
        <option value="ត្រពាំងរាំង">ត្រពាំងរាំង</option>
        <option value="អន្ទងបែក">អន្ទងបែក</option>
        <option value="ស្នោតូច">ស្នោតូច</option>
        <option value="ពន្លៃ">ពន្លៃ</option>
        <option value="បរក្នុង">បរក្នុង</option>
<!--            ឃុំល្អាង-->
        <option value="ត្រពាំងសេះ">ត្រពាំងសេះ</option>
        <option value="បើន">បើន</option>
        <option value="ដំណាក់អំពិល">ដំណាក់អំពិល</option>
        <option value="ល្អាង">ល្អាង</option>
<!--         ស្រុកកំពង់ត្រាច-->
<!--            ឃុំបឹងសាលាខាងជើង-->
        <option value="កោះទ្រមូង">កោះទ្រមូង</option>
        <option value="ដាសស្គរ">ដាសស្គរ</option>
        <option value="កោះម៉ាក់ប្រាំង">កោះម៉ាក់ប្រាំង</option>
        <option value="ព្រៃទប់">ព្រៃទប់</option>
<!--            ឃុំបឹងសាលាខាងត្បូង-->
        <option value="កោះចំការ">កោះចំការ</option>
        <option value="ជ្រេស">ជ្រេស</option>
        <option value="ស្វាយផ្អែម">ស្វាយផ្អែម</option>
<!--            ឃុំដំណាក់កន្ទួតខាងជើង-->
        <option value="អូរស្លែង">អូរស្លែង</option>
        <option value="ព្រៃកែស">ព្រៃកែស</option>
        <option value="អូរពពូល">អូរពពូល</option>
        <option value="ភ្នំដំរី">ភ្នំដំរី</option>
<!--            ឃុំដំណាក់កន្ទួតខាងត្បូង-->
        <option value="ដំណាក់កន្ទួត">ដំណាក់កន្ទួត</option>
        <option value="អូររលួស">អូររលួស</option>
        <option value="អង្គរជ័យទី ១">អង្គរជ័យទី ១</option>
        <option value="អង្គរជ័យទី ២">អង្គរជ័យទី ២</option>
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
<!--            ឃុំកំពង់ត្រាចខាងកើត-->
        <option value="កំពង់ត្រាចទី ១">កំពង់ត្រាចទី ១</option>
        <option value="កោះខ្លូត">កោះខ្លូត</option>
        <option value="កោះតាចាន់">កោះតាចាន់</option>
        <option value="របងក្រាស">របងក្រាស</option>
<!--            ឃុំកំពង់ត្រាចខាងលិច-->
        <option value="កណ្តាលទួល">កណ្តាលទួល</option>
        <option value="អង្រ្គង">អង្រ្គង</option>
        <option value="ភ្នំសាឡី">ភ្នំសាឡី</option>
        <option value="ដើមចារ">ដើមចារ</option>
        <option value="កោះផ្ដៅ">កោះផ្ដៅ</option>
        <option value="អូរច្រនៀង">អូរច្រនៀង</option>
        <option value="កំពង់ត្រាចទី ២">កំពង់ត្រាចទី ២</option>
<!--            ឃុំប្រាសាទភ្នំខ្យង-->
        <option value="ព្រៃកឹក">ព្រៃកឹក</option>
        <option value="ភ្នំខ្យង">ភ្នំខ្យង</option>
        <option value="ជ្រោះតាសោម">ជ្រោះតាសោម</option>
<!--            ឃុំភ្នំប្រាសាទ-->
        <option value="ច្រនៀងទេ">ច្រនៀងទេ</option>
        <option value="បង់បក់">បង់បក់</option>
        <option value="ភ្នំប្រសាទ">ភ្នំប្រសាទ</option>
        <option value="ឃ្លាំង">ឃ្លាំង</option>
        <option value="ដំណាក់ក្រឡាញ់">ដំណាក់ក្រឡាញ់</option>
        <option value="ស្ពានធំ">ស្ពានធំ</option>
        <option value="ចុងសួង">ចុងសួង</option>
<!--            ឃុំអង្គសុរភី-->
        <option value="ដើមពោធិ៍">ដើមពោធិ៍</option>
        <option value="អង្គសុរភី">អង្គសុរភី</option>
        <option value="ព្រៃទទឹង">ព្រៃទទឹង</option>
        <option value="ស្នាមគូរ">ស្នាមគូរ</option>
        <option value="បឹងធំខាងលិច">បឹងធំខាងលិច</option>
        <option value="បឹងធំខាងកើត">បឹងធំខាងកើត</option>
<!--            ឃុំព្រែកក្រឹស-->
        <option value="ព្រែកក្រឹស">ព្រែកក្រឹស</option>
        <option value="ព្រៃព្រូស">ព្រៃព្រូស</option>
        <option value="លក្ខជា">លក្ខជា</option>
        <option value="ដើមស្នាយ">ដើមស្នាយ</option>
        <option value="កោះតាកូវ">កោះតាកូវ</option>
        <option value="ព្រះទ្រហ៊ឹង">ព្រះទ្រហ៊ឹង</option>
        <option value="កោះត្នោត">កោះត្នោត</option>
<!--            ឃុំឫស្សីស្រុកខាងកើត-->
        <option value="ឫស្សីស្រុក">ឫស្សីស្រុក</option>
        <option value="កោះស្នូក">កោះស្នូក</option>
        <option value="ធ្នង់">ធ្នង់</option>
        <option value="អន្លង់ថ្ងាន់">អន្លង់ថ្ងាន់</option>
<!--            ឃុំឫស្សីស្រុកខាងលិច-->
        <option value="ថ្កូវ">ថ្កូវ</option>
        <option value="លក">លក</option>
        <option value="កោះក្រឹស្នា">កោះក្រឹស្នា</option>
        <option value="ត្រពាំងនៀល">ត្រពាំងនៀល</option>
        <option value="ដំណាក់ត្របែក">ដំណាក់ត្របែក</option>
        <option value="កំពូលមាស">កំពូលមាស</option>
<!--            ឃុំស្វាយទងខាងជើង-->
        <option value="ដូង">ដូង</option>
        <option value="ឈើទាលរាំ">ឈើទាលរាំ</option>
        <option value="បាយទា">បាយទា</option>
        <option value="ភ្នំក្ងាប">ភ្នំក្ងាប</option>
<!--            ឃុំស្វាយទងខាងត្បូង-->
        <option value="កោះត្រាច">កោះត្រាច</option>
        <option value="កោះឈ្វាំង">កោះឈ្វាំង</option>
        <option value="តាខ្វាយ">តាខ្វាយ</option>
        <option value="ស្លាបតាអោន">ស្លាបតាអោន</option>
        <option value="ស្វាយទង">ស្វាយទង</option>
<!--        ស្រុកទឹកឈូ-->
<!--            ឃុំបឹងទូក-->
        <option value="រលួស">រលួស</option>
        <option value="កែបថ្មី">កែបថ្មី</option>
        <option value="ទទឹងថ្ងៃ">ទទឹងថ្ងៃ</option>
<!--            ឃុំជុំគ្រៀល-->
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
        <option value="ជុំគ្រៀល">ជុំគ្រៀល</option>
        <option value="សំរ៉ោង">សំរ៉ោង</option>
        <option value="កំពង់កណ្ដាល">កំពង់កណ្ដាល</option>
<!--            ឃុំកំពង់ក្រែង-->
        <option value="អណ្ដូងជីម៉ឺន">អណ្ដូងជីម៉ឺន</option>
        <option value="ព្រៃត្នោត">ព្រៃត្នោត</option>
        <option value="កំពង់គ្រែង">កំពង់គ្រែង</option>
        <option value="កំពង់ក្រុង">កំពង់ក្រុង</option>
        <option value="ម៉ាក់ប្រាង្គ">ម៉ាក់ប្រាង្គ</option>
<!--            ឃុំកំពង់សំរោង-->
        <option value="ត្រពាំងកញ្ឆែត">ត្រពាំងកញ្ឆែត</option>
        <option value="កំពង់សំរោងខាងជើង">កំពង់សំរោងខាងជើង</option>
        <option value="កំពង់សំរោងខាងត្បូង">កំពង់សំរោងខាងត្បូង</option>
<!--            ឃុំកណ្ដោល-->
        <option value="ភ្នំតូច">ភ្នំតូច</option>
        <option value="អន្លង់គគី">អន្លង់គគី</option>
        <option value="ទឹកក្រហម">ទឹកក្រហម</option>
        <option value="មានរិទ្ធ">មានរិទ្ធ</option>
        <option value="ដំណាក់ត្រាច">ដំណាក់ត្រាច</option>
<!--            ឃុំកោះតូច-->
        <option value="គីឡូ ១២">គីឡូ ១២</option>
        <option value="ព្រែកចេក">ព្រែកចេក</option>
        <option value="កណ្ដាល">កណ្ដាល</option>
        <option value="ព្រែកអំពិល">ព្រែកអំពិល</option>
<!--            ឃុំកូនសត្វ-->
        <option value="បុស្សញិញ">បុស្សញិញ</option>
        <option value="កូនសត្វ">កូនសត្វ</option>
        <option value="កំពង់នង់">កំពង់នង់</option>
        <option value="កំពង់ត្នោត">កំពង់ត្នោត</option>
<!--            ឃុំម៉ាក់ប្រាង្គ-->
        <option value="ស្នំប្រាំពីរ">ស្នំប្រាំពីរ</option>
        <option value="បត់ក្បាលដំរី">បត់ក្បាលដំរី</option>
        <option value="មាត់ពាម">មាត់ពាម</option>
<!--            ឃុំព្រែកត្នោត-->
        <option value="ត្រពាំងរពៅ">ត្រពាំងរពៅ</option>
        <option value="ព្រែកក្រែង">ព្រែកក្រែង</option>
        <option value="ព្រែកត្នោត">ព្រែកត្នោត</option>
        <option value="ចង្ហោន">ចង្ហោន</option>
<!--            ឃុំព្រៃឃ្មុំ-->
        <option value="ព្រៃតុម្ព">ព្រៃតុម្ព</option>
        <option value="បឹងតារោង">បឹងតារោង</option>
        <option value="ព្រៃឃ្មុំ">ព្រៃឃ្មុំ</option>
        <option value="វត្ដអង្គ">វត្ដអង្គ</option>
<!--            ឃុំព្រៃថ្នង-->
        <option value="ចក្រីទីង">ចក្រីទីង</option>
        <option value="ដំណាក់ហ្លួង">ដំណាក់ហ្លួង</option>
        <option value="ព្រៃថ្នង">ព្រៃថ្នង</option>
        <option value="ទ្វារថ្មី">ទ្វារថ្មី</option>
        <option value="ច្បារអំពៅ">ច្បារអំពៅ</option>
<!--            ឃុំស្ទឹងកែវ-->
        <option value="កំពង់ចិន">កំពង់ចិន</option>
        <option value="ត្រពាំងកក់">ត្រពាំងកក់</option>
        <option value="ដូង">ដូង</option>
        <option value="ម្លិចគល់">ម្លិចគល់</option>
        <option value="អន្លង់ម៉ាក់ប្រាង្គ">អន្លង់ម៉ាក់ប្រាង្គ</option>
<!--            ឃុំថ្មី-->
        <option value="ត្រសេកកោង">ត្រសេកកោង</option>
        <option value="ត្រពាំងច្រាប">ត្រពាំងច្រាប</option>
        <option value="ថ្មី">ថ្មី</option>
        <option value="វត្ដពោធិ៍">វត្ដពោធិ៍</option>
        <option value="ដូនស៊យ">ដូនស៊យ</option>
        <option value="គោចិនលែង">គោចិនលែង</option>
<!--            ឃុំត្រពាំងព្រីង-->
        <option value="ត្រពាំងព្រីងខាងជើង">ត្រពាំងព្រីងខាងជើង</option>
        <option value="ត្រពាំងព្រីងខាងត្បូង">ត្រពាំងព្រីងខាងត្បូង</option>
        <option value="បុះត្របែក">បុះត្របែក</option>
        <option value="អង្គ">អង្គ</option>
<!--            ឃុំត្រពាំងសង្កែ-->
        <option value="ត្រពាំងសង្កែ">ត្រពាំងសង្កែ</option>
        <option value="កំពង់កែស">កំពង់កែស</option>
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
<!--            ឃុំត្រពាំងធំ-->
        <option value="ត្រពាំងជ្រៃ">ត្រពាំងជ្រៃ</option>
        <option value="ក្រាំង">ក្រាំង</option>
        <option value="ត្រពាំងធំ">ត្រពាំងធំ</option>
        <option value="ស្វាយធំ">ស្វាយធំ</option>
<!--        ក្រុងកំពត-->
<!--            សង្កាត់កំពង់កណ្ដាល-->
        <option value="សុវណ្ណសាគរ">សុវណ្ណសាគរ</option>
        <option value="ភូមិ១ឧសភា">ភូមិ១ឧសភា</option>
<!--            សង្កាត់ក្រាំងអំពិល-->
        <option value="ក្រាំង">ក្រាំង</option>
        <option value="ស្វាយធំ">ស្វាយធំ</option>
<!--            សង្កាត់កំពង់បាយ-->
        <option value="កំពង់បាយខាងជើង">កំពង់បាយខាងជើង</option>
        <option value="កំពង់បាយខាងត្បូង">កំពង់បាយខាងត្បូង</option>
<!--            សង្កាត់អណ្ដូងខ្មែរ-->
        <option value="ទ្វីខាងជើង">ទ្វីខាងជើង</option>
        <option value="ទ្វីខាងត្បូង">ទ្វីខាងត្បូង</option>
        <option value="អូតូច">អូតូច</option>
        <option value="អណ្ដូងខ្មែរ">អណ្ដូងខ្មែរ</option>
        <option value="តាដិប">តាដិប</option>
<!--            សង្កាត់ត្រើយកោះ-->
        <option value="ដូនតោក">ដូនតោក</option>
        <option value="តាអង្គ">តាអង្គ</option>
        <option value="បឹងតាព្រាម">បឹងតាព្រាម</option>
        <option value="ស្រែ">ស្រែ</option>
        <option value="ផ្សេងៗ">ផ្សេងៗ</option>
</select>  
</p> 
<p id="other_village"></p>
</div>