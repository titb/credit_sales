@extends('credit_sale.layout.master')
@section('contend')	

<div class="container-fluid">
    <div class="row-fluid">
					
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                               
								<ul class="nav nav-tabs">

									<li class="active"><a href="{{URL::to('accounts/all_sub_client')}}">អ្នកធានាទាំងអស់</a></li>

									<!-- <li><a href="{{URL::to('accounts_group')}}">អតិថិជនជាលក្ខណៈក្រុម</a></li> -->
								
								</ul>

						    </div>
                            <div class="block-content collapse in change_content">
                            
                            	<!-- <form class="form-search" style="margin-bottom:2px;" action="{{URL::to('accounts')}}" method="GET"> -->
											<div class="span12" style="margin-left:0px;">
												<?php $name_kh = DB::table('cs_clients')->get(); ?>
												<input type="text" name="search_name" data-required="1" placeholder="ឈ្មោះ ខ្មែរ/អង់គ្លេស" class="span2 m-wrap s_name_kh_en" style="margin-top:5px;margin-left: 10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($name_kh as $row)"{{$row->kh_username}}","{{$row->en_username}}",@endforeach""]' autocomplete="off">
												
												<input type="text" name="search_phone" data-required="1" placeholder="លេខទូរសព្ទ" class="span2 m-wrap s_phone" style="margin-top:5px;margin-left: 10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($name_kh as $row)"{{$row->phone}}",@endforeach""]' autocomplete="off">
												
												<input type="text" name="search_id_card" data-required="1" placeholder="អត្តសញ្ញាណប័ណ្ណ" class="span2 m-wrap s_id_card" style="margin-top:5px;margin-left: 10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($name_kh as $row)"{{$row->identify_num}}",@endforeach""]'  autocomplete="off">

												<input type="text" name="search_dob" placeholder="ថ្ងៃខែឆ្នាំកំណើត" class="span2 m-wrap s_dob input-xlarge datepicker" value="{{old('search_dob')}}" style="margin-top:5px;margin-left: 10px;" id="date01"/>
												
												<select class="span2 m-wrap s_gender" name="search_gender" style="margin-top:5px;margin-left: 10px;">
													  <option value="">-- ជ្រើស ភេទ --</option>
													  <option value="M">ប្រុស</option>
													  <option value="F">ស្រី</option>
												</select>

												<?php $branch = DB::table('mfi_branch')->where('deleted','=',0)->get(); ?>
												<select class="span2 m-wrap s_branch" name="search_branch" style="margin-top:5px;margin-left: 10px;">
													  <option value="">-- ជ្រើស សាខា --</option>
													  @foreach($branch as $b)
													  	<option value="{{$b->id}}">{{$b->brand_name}}</option>
													  @endforeach
												</select>

												<div class="span1 pull-right" style="margin-left:0px;">
													<button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top:5px;" value="b_search">ស្វែងរក</button>
	                            				</div>
											</div>
                            	<!-- </form> -->
                            	@include('errors.error')	
	
                            	<div class="span12" style="margin-left: 0;margin-top: 10px;">

	                            	<center>
                                        <h3 class="cen_title" style="font-family: 'Moul', 'Times New Roman';"> ការគ្រប់គ្រងអ្នកធានា</h3>
	                            		<!-- <div class="muted span3 pull-right" style="margin-bottom:5px;">
	                            		@if(Auth::user()->groups->first()->hasPermission(['developer']))
											<a href="{{ url('accounts/trush') }}" class="btn btn-warning pull-right" style="margin-left: 5px;"><i class="icon-plus icon-white"></i>  &nbsp;Trush
	                            			</a>
										@endif
										
											<a href="{{ url('get_two_name_storn') }}" class="btn btn-info pull-right" target="_blank" style="margin-left: 5px;"> &nbsp;អតិថិជនស្ទួន</a> &nbsp;
	                            			<a href="{{ url('accounts/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី
	                            			</a>
											
	                            		</div> -->
	                            	</center>
	                            	<legend></legend>
									
	                            </div>
									
									    <table class="table table-bordered">
									      <thead style="background: rgb(251, 205, 205);">
									        <tr class="header">
									        	<th>ល.រ</th>
									            <!-- <th>លេខកូដអតិថិជន</th> -->
								                <th>គោត្តនាម</th>
								                <th>នាម</th>
								                <th>អាសយដ្ឋាន</th>
								                <th>ភេទ</th>
								                <th>លេខទូរសព្ទ</th>
								                <th>លេខសម្គាល់អត្តសញ្ញាណ</th>
								                <th>ថ្ងៃខែឆ្នាំកំណើត</th>
								                <th>ស្ថានភាព</th>
								                <th>សកម្មភាព</th>
									        </tr>
									      </thead>
									      <tbody class="client_list">

											  	
									        		
									      </tbody>
										  		<tr>
													<td colspan="9">
														<b class="pull-right">អ្នកធានាសរុប:</b>
													</td>
													<td>
														<b id="total_all"></b>
													</td>
									            </tr>
									    </table>
									    <!-- Pagination -->
								<div class="pagination text-right"></div>
									  

	                            
							</div>
						</div>
                     	<!-- /block -->
		    		</div>
	</div>
</div>
<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
	$(document).ready(function(){
//Input Code Date And Year 
		$('#add').click(function(event){
		event.preventDefault();
			var img = '<div class="controls" id="addimage">' +

					'<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

					'</div>';
			$('#addimage').append(img);
		});
		var age_get = 18;
		var d = new Date();
		var year_d = d.getFullYear() - age_get;
		var i;
		var year_dob ="";
		year_dob += "<option value=''></option>";
		for(i = 1940 ; i <= year_d ; i ++){
			year_dob += "<option value='"+i+"'>"+i+"</option>";
		}

		$(".get_year_dob").html(year_dob);	

		var lastday = function(y,m){
				return  new Date(y, m +1, 0).getDate();
		}
		var ldfm = lastday(d.getFullYear(),d.getMonth());
		var date_of = "";
		date_of += "<option value=''></option>";
		for (var m = 1 ; m <= ldfm ; m ++ ){
			date_of += "<option value='"+m+"'>"+m+"</option>";
		}
		$(".get_date_dob").html(date_of);	
// End Input Code Date And Year 

		var numpage = 1 ;
		var url_index1 = "{{url('all_sub_client')}}";
		get_page(url_index1,numpage);
		
		
		function get_page(url,n){
			$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
			var search_name = $(".s_name_kh_en").val();
			var search_phone = $(".s_phone").val();
			var search_id_card = $(".s_id_card").val();
			var search_dob = $(".s_dob").val();
			var search_gender = $(".s_gender").val();
			var brand_name = $(".s_branch").val();
			var submit_search = $(".b_search").text();
		
           var forData1 = {
				search_name: $(".s_name_kh_en").val(),
				search_phone: $(".s_phone").val(),
				search_id_card: $(".s_id_card").val(),
				search_dob: $(".s_dob").val(),
				search_gender: $(".s_gender").val(),
				brand_name: $(".s_branch").val(),
				submit_search: $(".b_search").val()
		   }
           var forData2 = {};
		   if(url === "b_search" || search_name !== "" || search_phone !== "" || search_id_card !== "" || search_dob !== "" || search_gender !== "" || brand_name !== ""){
			   var forData = forData1;
				var url_index = url_index1+"?search_name="+search_name+"&search_phone="+search_phone+"&search_id_card="+search_id_card+"&search_dob="+search_dob+"&search_gender="+search_gender+"&brand_name="+brand_name+"&submit_search=Search&page="+n;
					//    }else if(search_name !== "" || search_phone !== "" || search_id_card !== "" || search_dob !== "" || search_gender !== "" || brand_name !== ""){
					// 		var forData = forData1;
					// 	   var url_index = url_index1+"?search_name="+search_name+"&search_phone="+search_phone+"&search_id_card="+search_id_card+"&search_dob="+search_dob+"&search_gender="+search_gender+"&brand_name="+brand_name+"&submit_search=Search&page="+n;
		   }else{
			   var forData = forData2;
			   var url_index = url+"?page="+n;
		   }	
			// search data
			// search
			var client;
			var out = "";
			$.ajax({
					type: "GET",
					url: url_index, 
					dataType: "json",
					data: forData,
					success: function(result){
						// console.log(forData);
						console.log(result);
						$("#total_all").text(result.length);
					$.each(result, function(i, field){
							var il = i + 1;
							var address , hn ,gn ,st, vl , cm , dis,pro ,gd ,sts , dob;
							if(field.client_house_num !== null){
								hn = field.client_house_num +",";
							}else{
								hn = "";
							}
							if(field.client_group_num !== null){
								gn = field.client_group_num +",";
							}else{
								gn = "   ";
							}
							if(field.client_st_num !== null){
								st = field.client_st_num +",";
							}else{
								st = "  ";
							}
							if(field.client_village !== null){
								vl = field.client_village +",";
							}else{
								vl = "";
							}
							if(field.client_commune !== null){
								cm = field.client_commune +",";
							}else{
								cm = "";
							}
							if(field.client_district !== null){
								dis = field.client_district +",";
							}else{
								dis = "";
							}
							if(field.client_province !== null){
								pro = field.client_province +",";
							}else{
								pro = "";
							}
							 
							address = hn +"  "+  gn +" "+  st +"  "+  vl  +"  "+  cm +"  "+  dis +"  "+  pro ; 

							if(field.client_gender !== "F"){
								gd = "ប្រុស";
							}else{
								gd = "ស្រី";
							}

						if(field.client_dob !== null){	
							 dob = new Date(field.client_dob);
							var dd = dob.getDate();
							var mm = dob.getMonth()+1; //January is 0!

							var yyyy = dob.getFullYear();
							if(dd<10){
								dd='0'+dd;
							} 
							if(mm<10){
								mm='0'+mm;
							} 
							dob = dd+'-'+mm+'-'+yyyy;
						}else{
							dob = "not yet input";
						}   
							if(sts !== 1 ){
								sts = "<span class='label label-success'>Active</span>";
							}else{
								sts = "<span class='label label-important'>Inactive</span>";
							}	
						var url = "{{ url('accounts/sub_client') }}";
                         var show_url = url + "/"+field.id+"/sub_client?client_id=0";
						 
							client += "<tr>";
								client += "<td>"+ il +"</td>";
								// client += "<td>"+ field.id +"</td>";
								client += "<td>"+ field.client_first_name_kh +"</td>";
								client += "<td>"+ field.client_second_name_kh +"</td>";
								client += "<td>"+ address +"</td>";
								client += "<td>"+ gd +"</td>";
								client += "<td>"+ field.client_phone +"</td>";	
								client += "<td>"+ field.client_idcard_no +"</td>";
								client += "<td>"+ dob +"</td>";
								client += "<td>"+ sts +"</td>";
								client += "<td>";

								client += "<a href='"+show_url+"' class='btn btn-info btn_show'>លម្អិត</a>  ";
								// client += "<button  class='btn btn-primary btn_edit' id='btn_edit'  value='"+ field.id +"'>Edit</button>  ";
								// client += "<button  class='btn btn-danger btn_deleted' ' value='"+ field.id +"'>Delete</button>";
								client += "</td>";

							client += "</tr>";
				    });
						$(".client_list").html(client);	

						var page = "";
						if(result.prev_page_url === null){
							var pr_url = result.current_page;
						}else{
							var pr_url = result.current_page -1;
						}
						page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
						for(var x = 1; x <= result.last_page; x ++  ) {
							if(result.current_page === x){
								page += "<a class='pag active' >"+x+"</a>";
							}else{
								page += "<a class='pag' >"+x+"</a>";
							}


						}
						if(result.next_page_url === null){
							var ne_url = result.current_page;
						}else{
							var ne_url = result.current_page +1;
						}
						page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
						$(".pagination").html(page );

				}, 
				complete: function() {
					$(this).data('requestRunning', false);
					
				},
				error: function (result ,status, xhr) {
					console.log(result.responseText);
				}
			
			});

			
		}
    });	
</script>
@endsection