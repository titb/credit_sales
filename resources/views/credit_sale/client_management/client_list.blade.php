@extends('credit_sale.layout.master')
@section('contend')	

<div class="container-fluid">
    <div class="row-fluid">
					
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                               
								<ul class="nav nav-tabs">

									<li class="active"><a href="{{URL::to('accounts')}}">@lang('client.personal-client')</a></li>

									<!-- <li><a href="{{URL::to('accounts_group')}}">អតិថិជនជាលក្ខណៈក្រុម</a></li> -->
								
								</ul>

						    </div>
                            <div class="block-content collapse in change_content">
                            
                            	<!-- <form class="form-search" style="margin-bottom:2px;" action="{{URL::to('accounts')}}" method="GET"> -->
											<div class="span12" style="margin-left:0px;">
												<?php $name_kh = DB::table('cs_clients')->get(); ?>
												<input type="text" name="search_name" data-required="1" placeholder="@lang('client.personal-client')" class="span2 m-wrap s_name_kh_en" style="margin-top:5px;margin-left: 10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($name_kh as $row)"{{$row->kh_username}}","{{$row->en_username}}",@endforeach""]' autocomplete="off">
												
												<input type="text" name="search_phone" data-required="1" placeholder="@lang('client.search-phone')" class="span2 m-wrap s_phone" style="margin-top:5px;margin-left: 10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($name_kh as $row)"{{$row->phone}}",@endforeach""]' autocomplete="off">
												
												<input type="text" name="search_id_card" data-required="1" placeholder="@lang('client.search-id')" class="span2 m-wrap s_id_card" style="margin-top:5px;margin-left: 10px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($name_kh as $row)"{{$row->identify_num}}",@endforeach""]'  autocomplete="off">

												<input type="text" name="search_dob" placeholder="@lang('client.search-dob')" class="span2 m-wrap s_dob input-xlarge datepicker" value="{{old('search_dob')}}" style="margin-top:5px;margin-left: 10px;" id="date01"/>
												
												<select class="span2 m-wrap s_gender" name="search_gender" style="margin-top:5px;margin-left: 10px;">
													  <option value="">-- @lang('client.search-gender') --</option>
													  <option value="M">@lang('client.boy')</option>
													  <option value="F">@lang('client.girl')</option>
												</select>

												<?php $branch = DB::table('mfi_branch')->where('deleted','=',0)->get(); ?>
												<select class="span2 m-wrap s_branch" name="search_branch" style="margin-top:5px;margin-left: 10px;">
													  <option value="">-- @lang('client.search-branch') --</option>
													  @foreach($branch as $b)
													  	<option value="{{$b->id}}">{{$b->brand_name}}</option>
													  @endforeach
												</select>

												<div class="span1 pull-right" style="margin-left:0px;">
													<button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top:5px;" value="b_search">@lang('client.search')</button>
	                            				</div>
											</div>
                            	<!-- </form> -->
                            	@include('errors.error')	
	
                            	<div class="span12" style="margin-left: 0;margin-top: 10px;">

	                            	<center>
                                        <h3 class="cen_title" style="font-family: 'Moul', 'Times New Roman';"> @lang('client.per-client-mgm')</h3>
	                            		<div class="muted span3 pull-right" style="margin-bottom:5px;">
	                            		@if(Auth::user()->groups->first()->hasPermission(['developer']))
											<!-- <a href="{{ url('accounts/trush') }}" class="btn btn-warning pull-right" style="margin-left: 5px;"><i class="icon-plus icon-white"></i>  &nbsp;Trush
	                            			</a> -->
										@endif
										
											<!-- <a href="{{ url('get_two_name_storn') }}" class="btn btn-info pull-right" target="_blank" style="margin-left: 5px;"> &nbsp;អតិថិជនស្ទួន</a> &nbsp; -->
	                            			<a href="{{ url('accounts/create') }}?redirect=accounts&item=0" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> @lang('client.add-new')
	                            			</a>
											
	                            		</div>
	                            	</center>
	                            	<legend></legend>
									
	                            </div>
									
									    <table class="table table-bordered">
									      <thead style="background: rgb(251, 205, 205);">
									        <tr class="header">
									        	<th>@lang('client.no')</th>
									            <!-- <th>លេខកូដអតិថិជន</th> -->
								                <th>@lang('client.first-name')</th>
								                <th>@lang('client.last-name')</th>
								                <th>@lang('client.address')</th>
								                <th>@lang('client.gender')</th>
								                <th>@lang('client.phone')</th>
								                <th>@lang('client.id-number')</th>
								                <th>@lang('client.dob')</th>
								                <th>@lang('client.status')</th>
								                <th>@lang('client.action')</th>
									        </tr>
									      </thead>
									      <tbody class="client_list">

											  	
									        		
									      </tbody>
										  		<tr>
													<td colspan="9">
														<b class="pull-right">@lang('client.total-client')</b>
													</td>
													<td>
														<b id="total_all"></b>
													</td>
									            </tr>
									    </table>
									    <!-- Pagination -->
								<div class="pagination text-right"></div>
									  

	                            
							</div>
						<!-- Model Edit -->
							<div id="edit_me" class="modal hide">
								<div class="modal-header">
									<br/>
									<button class="close btn_close" type="button">&times;</button>
									<center><h3 class="cen_title">កែរប្រែរព័ត៌មានអតិថិជន</h3></center>
								</div>
								<div class="modal-body">
									<!-- <p>Lorem ipsum dolor sit amet...</p> -->
									<form role="form" id="form_insert_client" name="form_insert_client" method="POST"  enctype="multipart/form-data">
										<div class="span8"  style="margin: 0 auto !important;float: none;">

											{{ csrf_field() }}

												<div class="control-group">
														<div class="span12" style="margin-left:0;">
															<div class="span6">
																<label class="control-label" title="ប្រភេទកម្ចី">ប្រភេទអតិថិជន<span class="required" title="This place you must be select data">*</span></label>
																<select name="client_type" class="span12 m-wrap" id="client_type">
																<?php $client_type  = App\ClientType::get(); ?>
																@foreach($client_type as  $ct)
																	<option value="{{ $ct->id }}">{{ $ct->dispay_name }}</option>
																@endforeach 
																	<!-- <option value="2">ក្រុម</option> -->
																</select>
															</div>

															<div class="span6">
																<label class="control-label" title="ជ្រើសរើសសាខារបស់អតិថិជន ដែលគាត់មកខ្ចីប្រាក់, ចាំបាច់ត្រូវតែមាន">ជ្រើសរើសសាខា <span class="required" title="This place you must be select data">*</span></label>
																<?php $branch = DB::table('mfi_branch')->where('deleted','=',0)->get(); ?>

																<select name="branch_id" class="span12 m-wrap" id="branch_id">
																		<!--<option value="0">-- Please Select Brand --</option>-->

																	@foreach($branch as $b)

																		<option value="{{ $b->id }}">{{ $b->brand_name }}</option>

																	@endforeach

																</select>

															</div>
														</div>
													
													<div class="span12" style="margin-left:0;">

														<div class="span6">

															<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាម <span class="required" title="This place you must be put data">*</span></label>

															<div class="controls">

																<input type="text" id="client_first_name_kh" class="span12 m-wrap kh_name_first" name="client_first_name_kh" data-required="1" value="{{ old('client_first_name_kh') }}"/>

															</div>

															<p class="alert-danger">{{$errors->first('client_first_name_kh')}}</p>

														</div>

														<div class="span6">

															<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាម <span class="required" title="This place you must be put data">*</span></label>

															<div class="controls">

																<input type="text" id="client_second_name_kh" class="span12 m-wrap kh_name_last" name="client_second_name_kh" data-required="1" value="{{ old('client_second_name_kh') }}"/>

															</div>

															<p class="alert-danger">{{$errors->first('client_second_name_kh')}}</p>

														</div>

													</div>



													<div class="span12" style="margin-left:0;">

														<div class="span6">

															<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">គោត្តនាមជាអក្សរឡាតាំង (First Name)</label>

															<div class="controls">

																<input type="text" autocapitalize="word" id="client_first_name_en" name="client_first_name_en" data-required="1" value="{{ old('client_first_name_en') }}" class="span12 m-wrap en_name_first"/>

															</div>

														</div>

														<div class="span6">

															<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">នាមជាអក្សរឡាតាំង (Last Name)</label>

															<div class="controls">

																<input type="text" autocapitalize="word" id="client_second_name_en" class="span12 m-wrap en_name_last" name="client_second_name_en" data-required="1" value="{{ old('client_second_name_en') }}"/>

															</div>
															
														</div>
															<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ភេទ <span class="required" title="This place you must be put data">*</span></label>

															<div class="controls">

																<select id="selectError"​ class="span12 m-wrap client_gender" name="client_gender">

																<option value="M">ប្រុស</option>

																<option value="F">ស្រី</option>

																</select>

															</div>

															<p class="alert-danger">{{$errors->first('client_gender')}}</p>

													</div>
													<div class="span12" style="margin-left:0px;">
														<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ថ្ងៃខែឆ្នាំកំណើត <span class="required" title="This place you must be put data">*</span></label>
														<div class="controls">
															<div class="span4">
																<label>ថ្ងៃ</label>
																<select name="day" id="select01" class="chzn-select get_date_dob span12 m-wrap"></select>
																	<p class="alert-danger">{{$errors->first('day')}}</p>
															</div>
															<div class="span4">
																<label>ខែ</label>
																<select name="month" id="select01" class="chzn-select get_month_dob span12 m-wrap">
																<option value=""></option>
																<option value="01">មករា</option>
																<option value="02">កុម្ភៈ</option>
																<option value="03">មីនា</option>
																<option value="04">មេសា</option>
																<option value="05">ឧសភា</option>
																<option value="06">មិថុនា</option>
																<option value="07">កក្កដា</option>
																<option value="08">សីហា</option>
																<option value="09">កញ្ញា</option>
																<option value="10">តុលា</option>
																<option value="11">វិចិ្ឆកា</option>
																<option value="12">ធ្នូ</option>
																</select>
															</div>
															<div class="span4">
																<label>ឆ្នាំ</label>
																<select name="year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
															</div>
														</div>
													</div>
												<div class="span12" style="margin-left:0px;">
													<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">សញ្ជាតិ <span class="required" title="This place you must be put data">*</span></label>

													<div class="controls">

														<label class="checkbox-inline" style="margin-top:10px;">

															<input type="radio" id="default" class="check_me" checked name="client_nationality1" value="1"> ខ្មែរ

																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" id="other" class="check_me" name="client_nationality1" value="2"> ផ្សេងៗ

														</label>

														<input type="text" id="show_other" name="client_nationality2" placeholder="Input other" data-required="1" value="{{ old('client_nationality') }}" class="span12 m-wrap"/>

													</div>

													<p class="alert-danger">{{$errors->first('client_nationality')}}</p>

													<script type="text/javascript">

													$(document).ready(function(){

														$('#show_other').hide();



														$('#default').click(function (){

															$('#show_other').hide();

														});

														$('#other').click(function (){

															$('#show_other').show();

														});

													});

													</script>

													<br/>
													<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប័ណ្ណសម្គាល់អត្តសញ្ញាណលេខ​ ( <b><span id="characters"><span></b> ) <span class="required" title="This place you must be put data">*</span></label>

													<div class="controls">

														<input type="text" id="count-number" name="identify_num" onkeypress="return forceNumber(event);" data-required="1" value="{{ old('identify_num') }}"  class="span12 m-wrap identify_num"/>

													</div>

													<p class="alert-danger">{{$errors->first('identify_num')}}</p>

													<script type="text/javascript">

														$('#count-number').keyup(updateCount);

														$('#count-number').keydown(updateCount);



														function updateCount() {

															var cs = $(this).val().length;

															$('#characters').text(cs);

														}
														//can type English only
														function forceNumber(e) {
															var keyCode = e.keyCode ? e.keyCode : e.which;
																if(keyCode == 32)
																	return true;
																if(48 <= keyCode && keyCode <= 57)
																	return true;
																if(65 <= keyCode && keyCode <= 90)
																	return true;
																if(97 <= keyCode && keyCode <= 122)
																	return true;
																return false;
														}

													</script>

													<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ប្រភេទប័ណ្ណ <span class="required" title="This place you must be put data">*</span></label>

													<div class="controls">

														<label class="checkbox-inline" style="margin-top:10px;">

															<input type="radio" class="default client_type_idcard1" checked name="client_type_idcard1" value="1"> អត្តសញ្ញាណប័ណ្ណ

															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" class="default client_type_idcard2" name="client_type_idcard1" value="2"> លិខិនឆ្លង់ដែន

															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" class="default client_type_idcard3" name="client_type_idcard1" value="3"> សំបុត្របញ្ជាក់កំណើត

															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" class="default client_type_idcard4" name="client_type_idcard1" value="4"> សៀវភៅសា្នក់នៅ

															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" class="default client_type_idcard5" name="client_type_idcard1" value="5"> សៀវភៅគ្រួសារ

															&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" id="other1" name="client_type_idcard1" value="6"> ផ្សេងៗ

														</label>

														<input type="text" id="show_other1" name="client_type_idcard2" placeholder="Input other" data-required="1" value="{{ old('client_type_idcard') }}" class="span12 m-wrap"/>

													</div>

													<p class="alert-danger">{{$errors->first('client_type_idcard')}}</p>

													<script type="text/javascript">

														$(document).ready(function(){

															$('#show_other1').hide();



															$('.default').click(function (){

																$('#show_other1').hide();

															});

															$('#other1').click(function (){

																$('#show_other1').show();

															});

														});

													</script>

													<br/>

													<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">ចេញដោយ</label>

													<div class="controls">

														<label class="checkbox-inline" style="margin-top:10px;">

															<input type="radio" id="default2" checked name="client_aprovel_idcard_by1" value="1"> ក្រសួងមហាផ្ទៃ

																&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

															<input type="radio" id="other2" name="client_aprovel_idcard_by1" value="2"> ផ្សេងៗ

														</label>

														<input type="text" id="show_other2" name="client_aprovel_idcard_by2" placeholder="Input other" data-required="1" value="{{ old('client_aprovel_idcard_by') }}" class="span12 m-wrap"/>

													</div>
													<br/>
													<script type="text/javascript">

													$(document).ready(function(){

														$('#show_other2').hide();



														$('#default2').click(function (){

															$('#show_other2').hide();

														});

														$('#other2').click(function (){

															$('#show_other2').show();

														});

													});

													</script>			  	

												</div>
												
											</div>			
													
												<div class="span12" style="margin-left:0px;">
												
													<div class="span4">
														<label class="control-label">អាសយដ្ឋានផ្ទះលេខ</label>

														<div class="controls">

															<input type="text" name="client_house_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap home_num"/>

														</div>
													</div>
													<div class="span4">
														<label class="control-label">ក្រុមទី</label>

														<div class="controls">

															<input type="text" name="client_group_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap group_num"/>

														</div>
													</div>
													<div class="span4">
														<label class="control-label">ផ្លូវលេខ</label>

														<div class="controls">

															<input type="text" name="client_st_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap street_num"/>

														</div>
													</div>
												</div>
												<div class="span12" style="margin-left:0px;">		
												
														@include('credit_sale.client_management.p_location')
												</div>	
													<br/>

													<label class="control-label">លេខទូរសព្ទទំនាក់ទំនង <span class="required" title="This place you must be put data">*</span></label>

													<div class="controls">

														<input type="text" name="phone" data-required="1" value="{{ old('phone') }}" class="span12 m-wrap telnumber phone" maxlength="12"/>

													</div>

													<p class="alert-danger">{{$errors->first('phone')}}</p>
													<script type="text/javascript">
														$('.telnumber').keyup(function() {
															foo = $(this).val().split("-").join(""); // remove hyphens
														
																foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");

																$(this).val(foo);
														
															});
													</script>



													<label class="control-label">មុខរបរ</label>

													<div class="controls">

														<input type="text" name="client_job" data-required="1" value="{{ old('client_job') }}" class="span12 m-wrap job"/>

													</div>

													<label class="control-label">ទីកន្លែងប្រកបមុខរបរ</label>

													<div class="controls">

														<input type="text" name="client_address_job" data-required="1" value="{{ old('client_address_job') }}" class="span12 m-wrap place_job"/>

													</div>

												
													<br/>
													<label>កូអរដោនេ</label>
													X: <input type="text" name="client_lutidued" class="span5 m-wrap client_lutidued" style="width: 14%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
													Y: <input type="text" name="client_longitidute" class="span5 m-wrap client_longitidute" style="width: 14%;">

													<br/>
													<label class="control-label">ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ <span class="required" title="This place you must be select data">*</span></label>

													<div class="controls">

														<input type="file" name="client_upload_image" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត" />

													</div>
													<p class="client_upload_image"></p>		
													<p class="alert-danger">{{$errors->first('client_upload_image')}}</p>

													<br/>

													<a href="#" id="add" class="btn btn-danger pull-right">+ បន្ថែមទៀត</a>

														<label class="control-label">ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ <span class="required" title="This place you must be select data">*</span></label>

														<div class="controls" id="addimage">

															<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវមានទំហំសមល្មម"/>

														</div>

														<p class="alert-danger">{{$errors->first('image_uploade_id_familly')}}</p>
														<p class="image_uploade_id_familly"></p>	
												</div>

											</div>

											

											<div class="span8"  style="margin: 0 auto !important;float: none;">

												<br/>

													<label class="control-label">កំណត់ចំណាំ</label>
													
													<div class="controls">
														<textarea name="client_note"  data-required="1" rows="5" class="span12 m-wrap client_note">{{ old('client_job_profit') }}</textarea>

											</div>

												<br/>

												<center>
														<button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="Edit">បញ្ចូល</button>
														<input type="hidden" id="client_id" name="client_id" value="0">
													<a href="{{url('accounts')}}" class="btn btn-danger">ត្រលប់</a>

												</center>

											</div>
										<br/><br/>
										</form>										


								</div>
								<!-- <div class="modal-footer">
									<a data-dismiss="modal" class="btn btn-primary" href="#">Confirm</a>
									<button data-dismiss="modal" class="btn btn-primary">Su</button>
									<a data-dismiss="modal" class="btn" href="#">Cancel</a>
								</div> -->
							</div>
						
						<!-- Edit Model Edit -->

						</div>
                     	<!-- /block -->
		    		</div>
	</div>
</div>
<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<!-- <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script> -->
<script>
	$(document).ready(function(){
//Input Code Date And Year 
		$('#add').click(function(event){
		event.preventDefault();
			var img = '<div class="controls" id="addimage">' +

					'<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

					'</div>';
			$('#addimage').append(img);
		});
		var age_get = 18;
		var d = new Date();
		var year_d = d.getFullYear() - age_get;
		var i;
		var year_dob ="";
		year_dob += "<option value=''></option>";
		for(i = 1940 ; i <= year_d ; i ++){
			year_dob += "<option value='"+i+"'>"+i+"</option>";
		}

		$(".get_year_dob").html(year_dob);	

		var lastday = function(y,m){
				return  new Date(y, m +1, 0).getDate();
		}
		var ldfm = lastday(d.getFullYear(),d.getMonth());
		var date_of = "";
		date_of += "<option value=''></option>";
		for (var m = 1 ; m <= ldfm ; m ++ ){
			date_of += "<option value='"+m+"'>"+m+"</option>";
		}
		$(".get_date_dob").html(date_of);	
// End Input Code Date And Year 

		var numpage = 1 ;
		var url_index1 = "{{route('accounts_get_js')}}";
		get_page(url_index1,numpage);
		
		
		function get_page(url,n){
			$.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
			var search_name = $(".s_name_kh_en").val();
			var search_phone = $(".s_phone").val();
			var search_id_card = $(".s_id_card").val();
			var search_dob = $(".s_dob").val();
			var search_gender = $(".s_gender").val();
			var brand_name = $(".s_branch").val();
			var submit_search = $(".b_search").text();
		
           var forData1 = {
				search_name: $(".s_name_kh_en").val(),
				search_phone: $(".s_phone").val(),
				search_id_card: $(".s_id_card").val(),
				search_dob: $(".s_dob").val(),
				search_gender: $(".s_gender").val(),
				brand_name: $(".s_branch").val(),
				submit_search: $(".b_search").val()
		   }
           var forData2 = {};
		   if(url === "b_search" || search_name !== "" || search_phone !== "" || search_id_card !== "" || search_dob !== "" || search_gender !== "" || brand_name !== ""){
			   var forData = forData1;
				var url_index = url_index1+"?search_name="+search_name+"&search_phone="+search_phone+"&search_id_card="+search_id_card+"&search_dob="+search_dob+"&search_gender="+search_gender+"&brand_name="+brand_name+"&submit_search=Search&page="+n;
					//    }else if(search_name !== "" || search_phone !== "" || search_id_card !== "" || search_dob !== "" || search_gender !== "" || brand_name !== ""){
					// 		var forData = forData1;
					// 	   var url_index = url_index1+"?search_name="+search_name+"&search_phone="+search_phone+"&search_id_card="+search_id_card+"&search_dob="+search_dob+"&search_gender="+search_gender+"&brand_name="+brand_name+"&submit_search=Search&page="+n;
		   }else{
			   var forData = forData2;
			   var url_index = url+"?page="+n;
		   }	
			// search data
			// search
			var client;
			var out = "";
			$.ajax({
					type: "GET",
					url: url_index, 
					dataType: "json",
					data: forData,
					success: function(result){
						$("#total_all").text(result.total);
					$.each(result.data, function(i, field){
							var il = i + 1;
							var address , hn ,gn ,st, vl , cm , dis,pro ,gd ,sts , dob;
							if(field.home_num !== null){
								hn = field.home_num +",";
							}else{
								hn = "";
							}
							if(field.group_num !== null){
								gn = field.group_num +",";
							}else{
								gn = "   ";
							}
							if(field.street_num !== null){
								st = field.street_num +",";
							}else{
								st = "  ";
							}
							if(field.vilige !== null){
								vl = field.vilige +",";
							}else{
								vl = "";
							}
							if(field.commune !== null){
								cm = field.commune +",";
							}else{
								cm = "";
							}
							if(field.district !== null){
								dis = field.district +",";
							}else{
								dis = "";
							}
							if(field.province !== null){
								pro = field.province +",";
							}else{
								pro = "";
							}
							 
							address = hn +"  "+  gn +" "+  st +"  "+  vl  +"  "+  cm +"  "+  dis +"  "+  pro ; 

							if(field.gender !== "F"){
								gd = "ប្រុស";
							}else{
								gd = "ស្រី";
							}

						if(field.dob !== null){	
							 dob = new Date(field.dob);
							var dd = dob.getDate();
							var mm = dob.getMonth()+1; //January is 0!

							var yyyy = dob.getFullYear();
							if(dd<10){
								dd='0'+dd;
							} 
							if(mm<10){
								mm='0'+mm;
							} 
							dob = dd+'-'+mm+'-'+yyyy;
						}else{
							dob = "not yet input";
						}   
							if(sts !== 1 ){
								sts = "<span class='label label-success'>Active</span>";
							}else{
								sts = "<span class='label label-important'>Inactive</span>";
							}	
						var url = "{{ url('accounts') }}";
                         var show_url = url + "/"+field.id+"/show";
						 var edit_url = url + "/"+field.id+"/edit";
						 
							client += "<tr>";
								client += "<td>"+ il +"</td>";
								// client += "<td>"+ field.id +"</td>";
								client += "<td>"+ field.kh_name_first +"</td>";
								client += "<td>"+ field.kh_name_last +"</td>";
								client += "<td>"+ address +"</td>";
								client += "<td>"+ gd +"</td>";
								client += "<td>"+ field.phone +"</td>";	
								client += "<td>"+ field.identify_num +"</td>";
								client += "<td>"+ dob +"</td>";
								client += "<td>"+ sts +"</td>";
								client += "<td>";

								client += "<a href='"+show_url+"' class='btn btn-info btn_show'>@lang('client.btn-show')</a>  ";
								client += "<a href='accounts/edit?client_id="+field.id+"' class='btn btn-primary btn_edit' id='btn_edit'  value='"+ field.id +"'>@lang('client.btn-edit')</a>  ";
								client += "<button  class='btn btn-danger btn_deleted' ' value='"+ field.id +"'>@lang('client.btn-delete')</button>";
								client += "</td>";

							client += "</tr>";
				    });
						$(".client_list").html(client);	

						var page = "";
						if(result.prev_page_url === null){
							var pr_url = result.current_page;
						}else{
							var pr_url = result.current_page -1;
						}
						page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
						for(var x = 1; x <= result.last_page; x ++  ) {
							if(result.current_page === x){
								page += "<a class='pag active' >"+x+"</a>";
							}else{
								page += "<a class='pag' >"+x+"</a>";
							}


						}
						if(result.next_page_url === null){
							var ne_url = result.current_page;
						}else{
							var ne_url = result.current_page +1;
						}
						page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
						$(".pagination").html(page );

				}, 
				complete: function() {
					$(this).data('requestRunning', false);
					
				},
				error: function (result ,status, xhr) {
					console.log(result.responseText);
				}
			
			});

			
		}
	$(document).ajaxComplete(function(){
			var body_leng = $('.client_list').find('tr').length;
			$(".b_search").click(function(){
				var submit_search = $(this).val();
				var n = 1;
				var url_index2 = submit_search; 		
				get_page(url_index2,numpage = n);
			});
			$(".pag").click(function(){
				var n = $(this).text();
				get_page(url_index1,numpage = n);
			});

			$(".pre").click(function(){
				var n = $(this).find(".pre_in").val();
				get_page(url_index1,numpage = n);
			});		
	});	
	
	// $(document).ajaxComplete(function(){	
		
	// 	//Edit Client  Show Form  Edit 
	// 	var url_e = "{{ route('accounts') }}";				
	// 		$(".btn_edit").click(function(){
	// 					$.ajaxSetup({
	// 						headers: {
	// 							'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
	// 						}
	// 					})
	// 					// alert("ok");
	// 					$("#edit_me").addClass("in");
	// 					$("#edit_me").css({'display':'block'});
	// 					$("#edit_me").attr("aria-hidden","false");
	// 					$("#model_in").html("<div class='modal-backdrop  in'></div>");
	// 					var c_id = $(this).val();
	// 					$("#client_id").val(c_id);
	// 					$(".tbn_add").val("update");
	// 					var url_edit = url_e+"/"+c_id+"/edit";
	// 					// alert(url_edit);
	// 					var client;
	// 					var forData = {};
	// 						var out = "";
	// 						$.ajax({
	// 								type: "GET",
	// 								url: url_edit, 
	// 								dataType: "json",
	// 								data: forData,
	// 								success: function(result ,xhr){
	// 									console.log(result);
	// 									$.each(result ,function(i, field){
											
	// 										if(i == "client"){
	// 											if(field.description !== null){
	// 												$(".client_note").val(field.description);
	// 											}
	// 											if(field.latitude !== null){
	// 												$(".client_lutidued").val(field.latitude);
	// 											}
	// 											if(field.longitude !== null){
	// 												$(".client_longitidute").val(field.longitude);
	// 											}
	// 											if(field.kh_name_first !== null){
	// 												$(".kh_name_first").val(field.kh_name_first);
	// 											}
	// 											if(field.kh_name_last !== null){
	// 												$(".kh_name_last").val(field.kh_name_last);
	// 											}
	// 											if(field.en_name_first !== null){
	// 												$(".en_name_first").val(field.en_name_first);
	// 											}
	// 											if(field.en_name_last !== null){
	// 												$(".en_name_last").val(field.en_name_last);
	// 											}
	// 											if(field.job !== null){
	// 												$(".job").val(field.job);
	// 											}
	// 											if(field.place_job !== null){
	// 												$(".place_job").val(field.place_job);
	// 											}
	// 											if(field.street_num !== null){
	// 												$(".street_num").val(field.street_num);
	// 											}
	// 											if(field.home_num !== null){
	// 												$(".home_num").val(field.home_num);
	// 											}
	// 											if(field.group_num !== null){
	// 												$(".group_num").val(field.group_num);
	// 											}		
	// 											if(field.identify_num !== null){
	// 												$(".identify_num").val(field.identify_num);
	// 												var idn = field.identify_num;
	// 												$("#characters").text(idn.length); 
	// 											}
	// 											if(field.phone !== null){
	// 												$(".phone").val(field.phone);
	// 											}


	// 									//conditional 		
											
	// 											$('#client_type').find("[value='"+field.client_type_id+"']").attr("selected","selected");
	// 											$('#branch_id').find("[value='"+field.branch_id+"']").attr("selected","selected");
	// 											$('.client_gender').find("[value='"+field.gender+"']").attr("selected","selected");
	// 								// DOB		
	// 									if(field.dob !== null){		
	// 											var dob1 = new Date(field.dob);
	// 											var day1 = dob1.getDate();
	// 											var month2 = dob1.getMonth() + 1;
	// 											if(month2 < 10){
	// 												var month1 = "0"+month2;
	// 											}else{
	// 												var month1 = month2;
	// 											}
	// 											var year1 = dob1.getFullYear();
												
	// 											$('.get_date_dob').find("[value='"+day1+"']").attr("selected","selected");
	// 											$('.get_month_dob').find("[value='"+month1+"']").attr("selected","selected");
	// 											$('.get_year_dob').find("[value='"+year1+"']").attr("selected","selected");
	// 											$('#client_province').find("[value='"+field.province+"']").attr("selected","selected");
	// 											$('#client_district').find("[value='"+field.district+"']").attr("selected","selected");
	// 											$('#client_commune').find("[value='"+field.commune+"']").attr("selected","selected");
	// 											$('#client_village').find("[value='"+field.vilige+"']").attr("selected","selected");
												
	// 											//$('#client_village').next().find("[data-option-array-index='"+day1+"']").addClass("result-selected");
	// 											// Day 	
	// 												$('.get_date_dob').next().find("span").text(day1);
	// 												$('.get_date_dob').next().find("a").addClass("chzn-single");
	// 												$('.get_date_dob').next().find("a").removeClass("chzn-default");
	// 											// Month 
	// 												var month3 = $('.get_month_dob').find("[value='"+month1+"']").text();
	// 												$('.get_month_dob').next().find("span").text(month3);
	// 												$('.get_month_dob').next().find("a").addClass("chzn-single");
	// 												$('.get_month_dob').next().find("a").removeClass("chzn-default");
	// 											// Year  
	// 												$('.get_year_dob').next().find("span").text(year1);
	// 												$('.get_year_dob').next().find("a").addClass("chzn-single");
	// 												$('.get_year_dob').next().find("a").removeClass("chzn-default");
	// 										}
										
	// 										// Province 
	// 											if(field.province !== null){
	// 												$("#client_province").next().find("span").text(field.province);
	// 												$('#client_province').next().find("a").addClass("chzn-single");
	// 												$('#client_province').next().find("a").removeClass("chzn-default");	
	// 											}
											
	// 										// District 
	// 											if(field.district !== null){
	// 												$("#client_district").next().find("span").text(field.district);
	// 												$('#client_district').next().find("a").addClass("chzn-single");
	// 												$('#client_district').next().find("a").removeClass("chzn-default");		
	// 											}
	// 										// Comnunce 
	// 											if(field.commune !== null){
	// 												$("#client_commune").next().find("span").text(field.commune);
	// 												$('#client_commune').next().find("a").addClass("chzn-single");
	// 												$('#client_commune').next().find("a").removeClass("chzn-default");
	// 											}
	// 										// Villeg 	
	// 											if(field.vilige !== null){
	// 												$("#client_village").next().find("span").text(field.vilige);
	// 												$('#client_village').next().find("a").addClass("chzn-single");
	// 												$('#client_village').next().find("a").removeClass("chzn-default");
	// 											}
	// 										//Check Nationality
	// 										    //$(".check_me").removeAttr("checked");
	// 											if(field.nationality ==1){
	// 												$("#default").attr("checked","checked");
	// 											}else{
	// 												$("#other").attr("checked","checked");
	// 													$("#show_other").css({"display":"block"});
	// 													$("#show_other").val(field.nationality);
	// 											}
	// 										//ប្រភេទប័ណ្ណ 	
	// 											if(field.identify_type ==="1"){
	// 												$('.client_type_idcard1').attr("checked","checked");
	// 											}else if(field.identify_type ==="2"){
	// 												$('.client_type_idcard2').attr("checked","checked");
	// 											}else if(field.identify_type ==="3"){
	// 												$('.client_type_idcard3').attr("checked","checked");
	// 											}else if(field.identify_type ==="4"){
	// 												$('.client_type_idcard4').attr("checked","checked");
	// 											}else if(field.identify_type ==="5"){
	// 												$('.client_type_idcard5').attr("checked","checked");
	// 											}else{
	// 												$("#other1").attr("checked","checked");
	// 													$("#show_other1").css({"display":"block"});
	// 													$("#show_other1").val(field.identify_type);
	// 											}

	// 										//	ប្រភេទប័ណ្ណចេញដោយ	
	// 											if(field.identify_by ==1){
	// 												$("#default2").attr("checked","checked");
	// 											}else{
	// 												$("#other2").attr("checked","checked");
	// 													$("#show_other2").css({"display":"block"});
	// 													$("#show_other2").val(field.nationality);
	// 											}
	// 										//ដាក់បញ្ចូលនូវរូបភាពនៃអាសយដ្ឋានដែលបានថតហើយ
	// 											var url_image = "{{url('/')}}";			
	// 													if(field.upload_relate_document !== null){
	// 														$(".client_upload_image").html("<br/><img class='client_upload_image_respon' > ");
	// 														$(".client_upload_image_respon").attr("src",url_image+"/Account/images/"+field.upload_relate_document);
	// 													}
	// 											}
											
	// 									});
	// 									//ដាក់បញ្ចូលនូវអត្តសញ្ញាណប័ណ្ណ ឬសៀវភៅគ្រួសារដែលបានថតចំលងហើយ
	// 									var l =	(result.relate_file).length;
	// 									if(l > 0){
	// 										var im_up_id = "";
	// 											$.each(result.relate_file, function(i, field){
	// 														var url_image = "{{url('/')}}";			
	// 														if(field.image_value !== null){
	// 															im_up_id += "<img class='client_upload_image_respon' id='image_uploade_id_familly"+i+"' src='"+url_image+"/Account/images/"+field.image_value+"'>"; 
	// 														}

	// 											});
	// 											$(".image_uploade_id_familly").html(im_up_id);
	// 									}
										
	// 								},
	// 								error: function (result ) {
	// 									console.log(result.stutus);
	// 								}

	// 						});
						
	// 		});	 
		
	// 	});
		
	$(document).ajaxComplete(function(){	
		// Deleted data Client
		$(".btn_deleted").click(function(e){
			$.ajaxSetup({
					headers: {
						'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
					}
				})
			e.preventDefault();
			var url_cr = "{{ route('accounts') }}";
			var client_id = $(this).val();
			var url_dell = url_cr+"/"+client_id+"/delete";
			
			$.ajax({
				type: "POST",
				url: url_dell,
				dataType:"json",
				data : {client_id:client_id} ,
				success: function(data){
					console.log(data);
				
					window.scrollTo(0, 0);  
					$('.msg_show').html(data.msg_show);
					var numpage = 1 ;
					var url_index1 = "{{route('accounts_get_js')}}";
					get_page(url_index1,numpage);
				}		
			});
		}); 

		
		
	});
   

  //Close Button
					$(".btn_close").click(function(){
						$("#edit_me").removeClass("in");
						$("#edit_me").css({'display':'none'});
						$("#edit_me").attr("aria-hidden","true");
						$("#model_in").html("");
						$(".tbn_add").val("");
						$("#client_id").val("0");
						$("#other").removeAttr("checked");
						$("#show_other").css({"display":"none"});
						$('.client_type_idcard2 ,.client_type_idcard3 ,.client_type_idcard4 ,.client_type_idcard5').removeAttr("checked");
						$("#other1").removeAttr("checked");
						$("#show_other1").css({"display":"none"});
						$("#other2").removeAttr("checked");
						$("#show_other2").css({"display":"none"});
						//$(".image_uploade_id_familly").
						$('#form_insert_client').trigger("reset");
						
						$("body").removeData("client");
					});
						
						
				// insert  eite client  to database   

				$("#btn-save").click(function(e){
							
							$.ajaxSetup({
									headers: {
										'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
									}
								})
							e.preventDefault();
							var url_cr = "{{ route('accounts') }}";
							var state = $('#btn-save').val();
							var client_id =  $('#client_id').val();
							// high importance!, here you need change "yourformname" with the name of your form
							var form = document.forms.namedItem("form_insert_client"); 
							var formData = new FormData(form); 
							var url_edit = url_cr+"/"+client_id+"/edit";
							
							$.ajax({
								type:'POST',
								url: url_edit,
								dataType: 'json',
								contentType: false,
								data: formData,
								processData: false,
								success: function (data,status, xhr) {
								console.log(xhr);
								window.scrollTo(0, 0);  
								$('.msg_show').html(data.msg_show);
								$("#edit_me").removeClass("in");
								$("#edit_me").css({'display':'none'});
								$("#edit_me").attr("aria-hidden","true");
								$("#model_in").html("");
								$(".tbn_add").val("");
								$("#client_id").val("0");
								$("#other").removeAttr("checked");
								$("#show_other").css({"display":"none"});
								$('.client_type_idcard2 ,.client_type_idcard3 ,.client_type_idcard4 ,.client_type_idcard5').removeAttr("checked");
								$("#other1").removeAttr("checked");
								$("#show_other1").css({"display":"none"});
								$("#other2").removeAttr("checked");
								$("#show_other2").css({"display":"none"});
								$(".filename").text("No file selected");
								$('#form_insert_client').trigger("reset");
								var numpage = 1 ;
								var url_index1 = "{{route('accounts_get_js')}}";
								get_page(url_index1,numpage);
								
							}, 
								complete: function() {
								$(this).data('requestRunning', false);
							},
								error: function (data ,status, xhr) {
									console.log(data.responseText);
								}
							});

						});
				//Edit Client  Show Form  Edit

		


});			
		
</script>
@endsection