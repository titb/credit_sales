@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
    <div class="row-fluid">
            <!-- validation -->
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                    <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span> {{$title}} </div>
                </div>  

                <div class="block-content collapse in">
                @include('credit_sale.client_management.menu_client')
                @include('errors.error')
                <div class="span8" style="margin: 0 auto !important;float: none;">
                    <center>
                        <h3 class="cen_title">  </h3>
                    </center>
                </div>
                    <table class="table table-bordered">

                        <thead style="background: rgb(251, 205, 205);">

                        <tr class="header">

                            <th>ល.រ</th>
                            <th>សំណើ​រ ល.រ</th>
                            <th>ចំនួនទំនិញ</th>
                            <th>ប្រាក់ទាំងអស់</th>
                            <th>កាប្រាក់កក់</th>
                            <th>ប្រាក់កក់</th>
                            <th>ប្រាក់ត្រូវនៅសល់</th>
                            <th>សកម្មភាព</th>

                        </tr>
                        </thead>
                        <tbody class="item_list">
                            
                        </tbody>
                        <tr>
                            <td colspan="7">
                                <b class="pull-right">សរុប:</b>
                            </td>
                            <td>
                                <b id="total_all"></b>
                                <input type="hidden" id="client_id" value="{{$client_id}}">
                            </td>
                        </tr>
                    </table>
                    <!-- Pagination -->
                    <div class="pagination text-right"></div>
                </div>
            </div>
            <!-- /block -->
        </div>
            <!-- /validation -->
    </div>

</div>

<meta name="_token" content="{{ csrf_token() }}" />

<script>
    var client_id = $("#client_id").val();
    var url = '{{ url("accounts/history_payment/sale_credit_json") }}' +"/"+ client_id;
    $.get( url, function( result ) {
        console.log(result);
        if(result != ''){
            var url_show = '{{url("accounts/history_payment/show_history_credit_sale")}}' + "?client_id={{$client_id}}";
            var data;
            $.each(result, function(k, v){
                data += '<tr>';
                data += '<td>'+(k+1)+'</td>';
                data += '<td>'+ v.cds_request_id +'</td>';
                var qty = 0;
                $.each(v.approval_item , function(sk, sv){
                    qty += sv.qty;
                });
                data += '<td>'+ qty +'</td>';
                data += '<td>'+ v.prices_total_num +'</td>';
                data += '<td>'+ v.deposit_precent +'</td>';
                data += '<td>'+ v.deposit_fixed +'</td>';
                data += '<td>'+ v.money_owne +'</td>';
                data += '<td><a href="'+ url_show + "&request_id=" + v.sale_id +'" class="btn btn-info btn_show">លម្អិត</a></td>';
                data += '<tr>';
                if(k == 0){
                    var cl_name = "ការបង្ហាញប្រវត្តិនៃការទិញបង់រំលស់ របស់អតិថិជន " + v.cs_sales.cs_client.kh_username;
                    $(".cen_title").html(cl_name);
                }
            });
            $(".item_list").append(data);
            $("#total_all").html(result.length);
            
        }else{
            $(".item_list").append("<tr><td>មិនមានទិន្នន័យ</td></tr>");
        }
        
    });
</script>            
@stop()
