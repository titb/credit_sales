@extends('credit_sale.layout.master')
@section('contend')
<style>
ul.typeahead.dropdown-menu {
    margin-top: -10px;
}
</style>
<div class="container-fluid">
    <div class="row-fluid">
            <!-- validation -->
        <div class="row-fluid">
                <!-- block -->
            <div class="block">
                <div class="navbar navbar-inner block-header">
                <div class="muted pull-left"><a href="{!! url('accounts') !!}"> គណនី</a> <span class="divider">|</span> កាលវិភាគសងប្រាក់អតិថិជន </div>
                    <center>
                        <h3 class="cen_title text-center khmer_Moul"></h3>
                    </center>
                </div>

                <div class="block-content collapse in">
                    <div class="span12" style="margin: 0 auto !important;float: none;">								
                        @include('credit_sale.client_management.menu_client')					
                    </div>

                    @include('errors.error')
                    <table class="table table-bordered">

                        <thead style="background: rgb(251, 205, 205);">

                            <tr class="header">
                                <th>ល.រ</th>
                                <th>លេខកូដកាលវិភាគ</th>
                                <th>សំណើ​រ ល.រ</th>
                                <th>ប្រាក់ដើមត្រូវបង់</th>
                                <th>ការប្រាក់ត្រូវបង់</th>
                                <th>ប្រាក់ត្រូវបង់</th>
                                <th>ការបង់ប្រាក់</th>
                                <th>ការបញ្ចប់</th>
                                <th>សកម្មភាព</th>
                            </tr>
                        </thead>
                        <tbody class="item_list">

                        </tbody>
                            <tr>
                                <td colspan="8">
                                    <b class="pull-right">សរុប:</b>
                                </td>
                                <td>
                                    <b id="total_all"></b>
                                </td>
                            </tr>
                        </table>
                        <!-- Pagination -->
                        <div class="pagination text-right"></div>
                    </div>
                </div>
                <!-- /block -->
            </div>
            <!-- /validation -->
        </div>
    </div>
</div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>
<script>
    var url_json = "{{ url('accounts/client/list_schedule_json') }}" + "?client_id=" + "{{$client_id}}";
    $.get(url_json, function(data){
        console.log(data);
        $.each(data, function(k, v){
            if(v.status_for_pay == 0){
                var stat_pay = "មិនទាន់បង់";
            }else if(v.status_for_pay == 1){
                var stat_pay = "បង់គ្រប់ចំនួន";
            }else{
                var stat_pay = "កំពុងបង់";
            }
            var url_show = "{{url('accounts/show_client_schedule')}}" + "?client_id={{$client_id}}&schedule_id=" + v.id;
            $('.item_list').append( '<tr>'
                                    + '<td>'+ (k+1) +'</td>'
                                    + '<td>'+ v.schedule_number +'</td>'
                                    + '<td>'+ v.approval_id +'</td>'
                                    + '<td>'+ v.money_owne_cost +'</td>'
                                    + '<td>'+ v.money_owne_interest +'</td>'
                                    + '<td>'+ v.money_owne_total_pay +'</td>'
                                    + '<td>'+ stat_pay +'</td>'
                                    + '<td>'+ (v.is_finish==1?"បានបញ្ចប់":"មិនទាន់បញ្ចប់") +'</td>'
                                    + '<td><a href="'+ url_show +'" class="btn btn-info btn_show">លម្អិត</a></td>'
                                    + '</tr>' );
        });
        $("#total_all").html(data.length);
    }).fail(function(error){
        console.log(error.responseText);
    });
    
    
</script>          
@stop()
