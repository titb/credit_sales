@extends('credit_sale.layout.master')
@section('contend')	
<div class="container-fluid">

    <div class="row-fluid">

			<!-- block -->

		<div class="block">

			<div class="navbar navbar-inner block-header">

				<div class="muted pull-left"><a href="{!! url('accounts') !!}"> @lang('client.account')</a> <span class="divider">|</span> @lang('client.add-account-new') </div>

			</div>

			<div class="block-content collapse in">
			
			@include('errors.error')

						<div class="span8"  style="margin: 0 auto !important;float: none;">

							<center>

								<h3 class="cen_title"> @lang('client.account') </h3>

							</center>
							<div class="errors"></div>
						</div>

					<form role="form" id="form_insert_client" name="form_insert_client" method="POST"  enctype="multipart/form-data">
						<div class="span8"  style="margin: 0 auto !important;float: none;">

							{{ csrf_field() }}

								<div class="control-group">
										<div class="span12" style="margin-left:0;">
											<div class="span6">
												<label class="control-label" title="ប្រភេទកម្ចី">@lang('client.client-type')<span class="required" title="This place you must be select data">*</span></label>
												<select name="client_type" class="span12 m-wrap">
												<?php $client_type  = App\ClientType::get(); ?>
												@foreach($client_type as  $ct)
													<option value="{{ $ct->id }}">{{ $ct->dispay_name }}</option>
												@endforeach 
													<!-- <option value="2">ក្រុម</option> -->
												</select>
											</div>

											<div class="span6">
												<label class="control-label" title="ជ្រើសរើសសាខារបស់អតិថិជន ដែលគាត់មកខ្ចីប្រាក់, ចាំបាច់ត្រូវតែមាន">@lang('client.choose-branch') <span class="required" title="This place you must be select data">*</span></label>
												<?php $branch = DB::table('mfi_branch')->where('deleted','=',0)->get(); ?>

												<select name="branch_id" class="span12 m-wrap">
														<!--<option value="0">-- Please Select Brand --</option>-->

													@foreach($branch as $b)

														<option value="{{ $b->id }}">{{ $b->brand_name }}</option>

													@endforeach

												</select>

											</div>
										</div>
									
									<div class="span12" style="margin-left:0;">

										<div class="span6">

											<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-first-name') <span class="required" title="This place you must be put data">*</span></label>

											<div class="controls">

												<input type="text" id="client_first_name_kh" class="span12 m-wrap" name="client_first_name_kh" data-required="1" value="{{ old('client_first_name_kh') }}"/>

											</div>

											<p class="alert-danger">{{$errors->first('client_first_name_kh')}}</p>

										</div>

										<div class="span6">

											<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-last-name') <span class="required" title="This place you must be put data">*</span></label>

											<div class="controls">

												<input type="text" id="client_second_name_kh" class="span12 m-wrap" name="client_second_name_kh" data-required="1" value="{{ old('client_second_name_kh') }}"/>

											</div>

											<p class="alert-danger">{{$errors->first('client_second_name_kh')}}</p>

										</div>

									</div>



									<div class="span12" style="margin-left:0;">

										<div class="span6">

											<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-first-name-en')</label>

											<div class="controls">

												<input type="text" autocapitalize="word" id="client_first_name_en" name="client_first_name_en" data-required="1" value="{{ old('client_first_name_en') }}" class="span12 m-wrap"/>

											</div>

										</div>

										<div class="span6">

											<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-last-name-en')</label>

											<div class="controls">

												<input type="text" autocapitalize="word" id="client_second_name_en" class="span12 m-wrap" name="client_second_name_en" data-required="1" value="{{ old('client_second_name_en') }}"/>

											</div>
											
										</div>
											<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-gender') <span class="required" title="This place you must be put data">*</span></label>

											<div class="controls">

												<select id="selectError"​ class="span12 m-wrap" name="client_gender">

												<option value="M">@lang('client.create-boy')</option>

												<option value="F">@lang('client.create-girl')</option>

												</select>

											</div>

											<p class="alert-danger">{{$errors->first('client_gender')}}</p>

									</div>
									<div class="span12" style="margin-left:0px;">
										<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-dob') <span class="required" title="This place you must be put data">*</span></label>
										<div class="controls">
											<div class="span4">
												<label>@lang('client.create-date')</label>
												<select name="day" id="select01" class="chzn-select get_date_dob span12 m-wrap"></select>
													<p class="alert-danger">{{$errors->first('day')}}</p>
											</div>
											<div class="span4">
												<label>@lang('client.create-month')</label>
												<select name="month" id="select01" class="chzn-select span12 m-wrap">
												<option value=""></option>
												<option value="01">@lang('client.jan')</option>
												<option value="02">@lang('client.feb')</option>
												<option value="03">@lang('client.mar')</option>
												<option value="04">@lang('client.apr')</option>
												<option value="05">@lang('client.may')</option>
												<option value="06">@lang('client.jun')</option>
												<option value="07">@lang('client.jul')</option>
												<option value="08">@lang('client.aug')</option>
												<option value="09">@lang('client.sep')</option>
												<option value="10">@lang('client.oct')</option>
												<option value="11">@lang('client.nov')</option>
												<option value="12">@lang('client.dec')</option>
												</select>
											</div>
											<div class="span4">
												<label>@lang('client.create-year')</label>
												<select name="year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
											</div>
										</div>
									</div>
								<div class="span12" style="margin-left:0px;">
									<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-nationality') <span class="required" title="This place you must be put data">*</span></label>

									<div class="controls">

										<label class="checkbox-inline" style="margin-top:10px;">

											<input type="radio" id="default" checked name="client_nationality1" value="1"> @lang('client.check-kh')

												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" id="other" name="client_nationality1" value="2"> @lang('client.check-other')

										</label>

										<input type="text" id="show_other" name="client_nationality2" placeholder="Input other" data-required="1" value="{{ old('client_nationality') }}" class="span12 m-wrap"/>

									</div>

									<p class="alert-danger">{{$errors->first('client_nationality')}}</p>

									<script type="text/javascript">

									$(document).ready(function(){

										$('#show_other').hide();



										$('#default').click(function (){

											$('#show_other').hide();

										});

										$('#other').click(function (){

											$('#show_other').show();

										});

									});

									</script>

									<br/>
									<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-id') ( <b><span id="characters"><span></b> ) <span class="required" title="This place you must be put data">*</span></label>

									<div class="controls">

										<input type="text" id="count-number" name="identify_num" onkeypress="return forceNumber(event);" data-required="1" value="{{ old('identify_num') }}"  class="span12 m-wrap"/>

									</div>

									<p class="alert-danger">{{$errors->first('identify_num')}}</p>

									<script type="text/javascript">

										$('#count-number').keyup(updateCount);

										$('#count-number').keydown(updateCount);



										function updateCount() {

											var cs = $(this).val().length;

											$('#characters').text(cs);

										}
										//can type English only
										function forceNumber(e) {
											var keyCode = e.keyCode ? e.keyCode : e.which;
												if(keyCode == 32)
													return true;
												if(48 <= keyCode && keyCode <= 57)
													return true;
												if(65 <= keyCode && keyCode <= 90)
													return true;
												if(97 <= keyCode && keyCode <= 122)
													return true;
												return false;
										}

									</script>

									<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.create-id-type') <span class="required" title="This place you must be put data">*</span></label>

									<div class="controls">

										<label class="checkbox-inline" style="margin-top:10px;">

											<input type="radio" class="default" checked name="client_type_idcard1" value="1"> @lang('client.id-type-id')

											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" class="default" name="client_type_idcard1" value="2"> @lang('client.id-type-passport')

											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" class="default" name="client_type_idcard1" value="3"> @lang('client.id-type-birth')

											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" class="default" name="client_type_idcard1" value="4"> @lang('client.id-type-stay')

											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" class="default" name="client_type_idcard1" value="5"> @lang('client.id-type-family')

											&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" id="other1" name="client_type_idcard1" value="6"> @lang('client.id-type-other')

										</label>

										<input type="text" id="show_other1" name="client_type_idcard2" placeholder="Input other" data-required="1" value="{{ old('client_type_idcard') }}" class="span12 m-wrap"/>

									</div>

									<p class="alert-danger">{{$errors->first('client_type_idcard')}}</p>

									<script type="text/javascript">

										$(document).ready(function(){

											$('#show_other1').hide();



											$('.default').click(function (){

												$('#show_other1').hide();

											});

											$('#other1').click(function (){

												$('#show_other1').show();

											});

										});

									</script>

									<br/>

									<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.id-approve-by')</label>

									<div class="controls">

										<label class="checkbox-inline" style="margin-top:10px;">

											<input type="radio" id="default2" checked name="client_aprovel_idcard_by1" value="1"> @lang('client.id-appr-interior')

												&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;

											<input type="radio" id="other2" name="client_aprovel_idcard_by1" value="2"> @lang('client.id-appr-other')

										</label>

										<input type="text" id="show_other2" name="client_aprovel_idcard_by2" placeholder="Input other" data-required="1" value="{{ old('client_aprovel_idcard_by') }}" class="span12 m-wrap"/>

									</div>
									<br/>
									<script type="text/javascript">

									$(document).ready(function(){

										$('#show_other2').hide();



										$('#default2').click(function (){

											$('#show_other2').hide();

										});

										$('#other2').click(function (){

											$('#show_other2').show();

										});

									});

									</script>			  	

								</div>
								
							</div>		


							<div class="span12" style="margin-left:0px;">
								<label class="control-label" title="ចាំបាច់ត្រូវតែមាន">@lang('client.id-appr-date') <!-- <span class="required" title="This place you must be put data">*</span> --></label>
								<div class="controls">
									<div class="span4">
										<label>@lang('client.create-date')</label>
										<select name="identify_day" id="select01" class="chzn-select get_date_dob span12 m-wrap"></select>
											<p class="alert-danger">{{$errors->first('day')}}</p>
									</div>
									<div class="span4">
										<label>@lang('client.create-month')</label>
										<select name="identify_month" id="identify_month" class="chzn-select span12 m-wrap">
										<option value=""></option>
										<option value="01">@lang('client.jan')</option>
										<option value="02">@lang('client.feb')</option>
										<option value="03">@lang('client.mar')</option>
										<option value="04">@lang('client.apr')</option>
										<option value="05">@lang('client.may')</option>
										<option value="06">@lang('client.jun')</option>
										<option value="07">@lang('client.jul')</option>
										<option value="08">@lang('client.aug')</option>
										<option value="09">@lang('client.sep')</option>
										<option value="10">@lang('client.oct')</option>
										<option value="11">@lang('client.nov')</option>
										<option value="12">@lang('client.dec')</option>
										</select>
									</div>
									<div class="span4">
										<label>@lang('client.create-year')</label>
										<select name="identify_year" id="select01" class="chzn-select get_year_dob span12 m-wrap"></select>
									</div>
								</div>
							</div>

							<div class="span12" style="margin-left:0;">

								<div class="span12">

									<label class="control-label" title="ចាំបាច់ត្រូវតែមាន"> @lang('client.id-appr-name')</label>

									<div class="controls">

										<input type="text" autocapitalize="word" id="approve_by_name" name="approve_by_name" data-required="1" value="{{ old('approve_by_name') }}" class="span12 m-wrap"/>

									</div>

								</div>
							</div>
									
								<div class="span12" style="margin-left:0px;">
								
									<div class="span4">
										<label class="control-label">@lang('client.home-num')</label>

										<div class="controls">

											<input type="text" name="client_house_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap"/>

										</div>
									</div>
									<div class="span4">
										<label class="control-label">@lang('client.group-num')</label>

										<div class="controls">

											<input type="text" name="client_group_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap"/>

										</div>
									</div>
									<div class="span4">
										<label class="control-label">@lang('client.street-mum')</label>

										<div class="controls">

											<input type="text" name="client_st_num" data-required="1" value="{{ old('client_num_st_ho_gr') }}" class="span12 m-wrap"/>

										</div>
									</div>
								</div>
								<div class="span12" style="margin-left:0px;">		
								
										@include('credit_sale.client_management.p_location')
								</div>	
									<br/>

									<label class="control-label">@lang('client.phone') <span class="required" title="This place you must be put data">*</span></label>

									<div class="controls">

										<input type="text" name="phones" data-required="1" value="{{ old('phone') }}" class="span12 m-wrap telnumber" maxlength="12"/>

									</div>

									<p class="alert-danger">{{$errors->first('phones')}}</p>
									<script type="text/javascript">
										$('.telnumber').keyup(function() {
											foo = $(this).val().split("-").join(""); // remove hyphens
										
												foo = foo.match(new RegExp('.{1,3}$|.{1,3}', 'g')).join("-");

												$(this).val(foo);
										
											});
									</script>



									<label class="control-label">@lang('client.job')</label>

									<div class="controls">

										<input type="text" name="client_job" data-required="1" value="{{ old('client_job') }}" class="span12 m-wrap"/>

									</div>

									<label class="control-label">@lang('client.work-place')</label>

									<div class="controls">

										<input type="text" name="client_address_job" data-required="1" value="{{ old('client_address_job') }}" class="span12 m-wrap"/>

									</div>

								
									<br/>
									<label>@lang('client.coordinate')</label>
									X: <input type="text" name="client_lutidued" class="span5 m-wrap" style="width: 14%;">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
									Y: <input type="text" name="client_longitidute" class="span5 m-wrap" style="width: 14%;">

									<br/>
									<label class="control-label">@lang('client.image-upload') <span class="required" title="This place you must be select data">*</span></label>

									<div class="controls">

										<input type="file" name="client_upload_image" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវតែជារូបភាពដែលថតចេញមកពីការបើកGPSថត"/>

									</div>

									<p class="alert-danger">{{$errors->first('client_upload_image')}}</p>

									<br/>

									<a href="#" id="add" class="btn btn-danger pull-right">+ Add more file scan</a>

										<label class="control-label">@lang('client.family-image') <span class="required" title="This place you must be select data">*</span></label>

										<div class="controls" id="addimage">

											<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" multiple title="រូបភាពត្រូវមានទំហំសមល្មម"/>

										</div>

										<p class="alert-danger">{{$errors->first('image_uploade_id_familly')}}</p>

								</div>

							</div>

							
							<?php
								if(isset($_REQUEST["redirect"]) ){
								
									echo ' <input type="hidden" name="redirect" id="redirect"  value="'.$_REQUEST["redirect"].'" />';
								}
								if(isset($_REQUEST["item"]) ){
									echo ' <input type="hidden" name="item" id="item" value="'.$_REQUEST["item"].'" />';
								}
							?>					
							<div class="span8"  style="margin: 0 auto !important;float: none;">

								<br/>

									<label class="control-label">@lang('client.note')</label>
									
									<div class="controls">
										<textarea name="client_note"  data-required="1" rows="5" class="span12 m-wrap">{{ old('client_job_profit') }}</textarea>

							</div>

								<br/>

								<center>
										<button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="add">@lang('client.submit')</button>
										<input type="hidden" id="client_id" name="client_id" value="0">
									<a href="{{url('accounts')}}" class="btn btn-danger">@lang('client.back')</a>

								</center>

							</div>
						<br/><br/>
						</form>
												
			</div>

		</div>

		<!-- /block -->
    </div>
		    	
	</div>

</div>
<meta name="_token" content="{{ csrf_token() }}" />
	
<script type="text/javascript">

	$(document).ready(function(){
		
		$('#add').click(function(event){
				event.preventDefault();
			var img = '<div class="controls" id="addimage">' +

				  	'<input type="file" name="image_uploade_id_familly[]" class="input-file uniform_on" id="fileInput" title="រូបភាពត្រូវមានទំហំសមល្មម"/>' +

				  	'</div>';
			$('#addimage').append(img);
		});
		var age_get = 18;
		var d = new Date();
		var year_d = d.getFullYear() - age_get;
		var i;
		var year_dob ="";
		for(i = 1940 ; i <= year_d ; i ++){
			year_dob += "<option value=''></option>";
			year_dob += "<option value='"+i+"'>"+i+"</option>";
		}

		$(".get_year_dob").html(year_dob);	

		var lastday = function(y,m){
				return  new Date(y, m +1, 0).getDate();
		}
		var ldfm = lastday(d.getFullYear(),d.getMonth());
        var date_of = "";
		for (var m = 1 ; m <= ldfm ; m ++ ){
			date_of += "<option value=''></option>";
			date_of += "<option value='"+m+"'>"+m+"</option>";
		}
		$(".get_date_dob").html(date_of);	
		  
   // insert  add client  to database   
	    @if(Request::is('accounts/create'))
		    var url = "{{ route('accounts/create') }}";
		@else
		    var url = "{{ url('accounts/'.$data->id.'/edit') }}";
		@endif  
      $("#btn-save").click(function(e){
      		  $.ajaxSetup({
			        headers: {
			            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
			        }
			    })
		     e.preventDefault();
      		var url_create = "{{ route('accounts/create') }}";
            var state = $('#btn-save').val();
		    var client_id =  $('#client_id').val();
		    // high importance!, here you need change "yourformname" with the name of your form
		    var form = document.forms.namedItem("form_insert_client"); 
		    var formData = new FormData(form); 

		    $.ajax({    
		        type:'POST',
		        url: url, 
		        dataType: 'json',
		       contentType: false,
		        data: formData,
		       processData: false,
		        success: function (data,status, xhr) {
		          console.log(data);
				  if(data.error){
                        var err = "";
                        $.each(data.error, function(key, val){
                            err += '<li>'+val+'</li>';
                        })
	                	$(".errors").append('<div class = "alert alert-danger"><ul>'+err+'</ul></div></br>');
                        window.scrollTo(0, 0);
                        $(".alert").delay(4000).slideUp(1000, function() {
                            $(this).alert('close');
                        });
	                }else{
						if(data.redirect == 'credit_sales'){
							var post_url = "{{url('pos_add_all_session_credit_sale')}}";
							var done = function() {
									window.location.href = "{{url('credit_sales')}}?redirect=credit_sales&sales="+data.item;
							}; 
							
							var formdata = {
									customer_id: data.datas.id,
									customer_name: data.datas.kh_username,    
								};  
							$.post(post_url, formdata , done);
						}else if(data.redirect == 'accounts'){
							window.scrollTo(0, 0);  
							$('.msg_show').html(data.msg_show);
							$('#form_insert_client').trigger("reset");
							$('#client_id').val("0");
							$('#btn-save').val("add");
							var client_id = data.datas.id;
							window.location = "{{ url('accounts/sub_client/create_sub_client') }}" + "?from="+data.datas.client_type_id+"&client_id="+ client_id +"&sub_client_id=0&item=create";
						}else{
							window.location="{{url('accounts')}}";
						}
					}
		       
		       },
		        error: function (data ,status, xhr) {
		            console.log(data.responseText);
		        }
		    });

		});
				
	});
</script>

@endsection

