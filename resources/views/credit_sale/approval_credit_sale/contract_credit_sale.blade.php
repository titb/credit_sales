<?php
  // Client Info
  function getDob($dob){
    $getdate = date_create($dob);
    $year = date_format($getdate,"Y");
    $month = date_format($getdate,"m");
    $day = date_format($getdate,"d");
    $date = $day." ".$month." ".$year;
    return $date;
  }
  $contract_code = "00000000".$appr_data->cds_request_id;
  $client = $appr_data->cs_client;
  $client_name = $client->kh_username;
  $client_sex = $client->gender=="M"?" ប្រុស ":" ស្រី ";
  $client_dob = getDob($client->dob);
  $client_nation = $client->nationality==1?" ខ្មែរ ":$client->nationality;
  $client_id = $client->identify_num;
  if($client->identify_type == 1){
    $client_idtype = " អត្តសញ្ញាណប័ណ្ណ ";
  }elseif($client->identify_type == 2){
    $client_idtype = " លិខិនឆ្លង់ដែន ";
  }elseif($client->identify_type == 3){
    $client_idtype = " សំបុត្របញ្ជាក់កំណើត ";
  }elseif($client->identify_type == 4){
    $client_idtype = " សៀវភៅសា្នក់នៅ ";
  }elseif($client->identify_type == 5){
    $client_idtype = " សៀវភៅគ្រួសារ ";
  }else{
    $client_idtype = $client->identify_type;
  }
  $client_iddate = $client->identify_date!=""?getDob($client->identify_date):" ................ ";
  if($client->identify_by == 1){
    $client_idby = " ក្រសួងមហាផ្ទៃ ";
  }else{
    $client_idby = $client->identify_by;
  }
  $client_home_num = $client->home_num;
  $client_street_num = $client->street_num;
  $client_group_num = $client->group_num;
  $client_village = $client->vilige;
  $client_commune = $client->commune;
  $client_district = $client->district;
  $client_province = $client->province;

  // Item
  function addComma($k, $length, $val){
    return $k==count($length)-1?$val:$val.", ";
  }
  $item_name = "";
  $item_qty = 0;
  $item_brand = "";
  $item_code = "";
  foreach($appr_data->approval_item as $k=>$apt){
    $item_name .= addComma($k, $appr_data->approval_item, $apt->item->name);
    $item_qty += $apt->qty;
    if($apt->item->cs_brand){
      $item_brand .= addComma($k, $appr_data->approval_item, $apt->item->cs_brand->name);
    }else{
      $item_brand = "";
    }
    $item_code .= addComma($k, $appr_data->approval_item, $apt->item->item_bacode);
  }
  $total_item_price = $appr_data->prices_total_num;
  $tipw = $appr_data->prices_totalword;
  $booking_percent = $appr_data->deposit_precent;
  $booking_price = $appr_data->deposit_fixed;
  $booking_word = $appr_data->deposit_fixed_word;
  $money_owe = $appr_data->money_owne;
  $money_owe_word = $appr_data->money_owne_word;
  $duration_pay = $appr_data->duration_pay_money;
  if($appr_data->duration_pay_money_type == "month"){
    $dpt = "ខែ";
    $c_month = "þ";
    $c_day = "¨";
    $c_2week = "¨";
    $c_week = "¨";
  }elseif($appr_data->duration_pay_money_type == "2week"){
    $dpt = "២សប្តាហ៍";
    $c_2week = "þ";
    $c_month = "¨";
    $c_week = "¨";
    $c_day = "¨";
  }elseif($appr_data->duration_pay_money_type == "week"){
    $dpt = "សប្តាហ៍";
    $c_week = "þ";
    $c_month = "¨";
    $c_2week = "¨";
    $c_day = "¨";
  }elseif($appr_data->duration_pay_money_type == "day"){
    $dpt = "ថ្ងៃ";
    $c_day = "þ";
    $c_month = "¨";
    $c_2week = "¨";
    $c_week = "¨";
  }else{
    $dpt = "";
    $c_month = "¨";
    $c_2week = "¨";
    $c_week = "¨";
    $c_day = "¨";
  }
  
  // Scedules
  function day($d){
    $d = date_create($d);
    return $d = date_format($d, 'd');
  }
  function month($m){
    $m = date_create($m);
    return $m = date_format($m, 'm');
  }
  function year($y){
    $y = date_create($y);
    return $y = date_format($y, 'Y');
  }
  $sched = $appr_data->schedule;
  $payment_last = $sched->cs_schedule_timesheet->last()->total_payment;
  $day_pay = day($appr_data->date_for_payments);
  $month_pay = month($appr_data->date_for_payments);
  $year_pay = year($appr_data->date_for_payments);
  $date_last = $sched->cs_schedule_timesheet->last()->date_payment;
  $finish_pay_day = day($date_last);
  $finish_pay_month = month($date_last);
  $finish_pay_year = year($date_last);

  // Guaranteer
  $guar = $appr_data->cs_client;
  if(count($guar->sub_clients) > 0){
  foreach($guar->sub_clients as $gk=>$vk){
    if($gk==0){
      $vk->client_name_kh!=null?$guar1_name=$vk->client_name_kh:$guar1_name="";
      $vk->client_gender=="M"?$guar1_sex="ប្រុស":$guar1_sex="ស្រី";
      $guar1_dob = date_format(date_create($vk->client_dob), "d m Y");
      $vk->client_nationality==1?$guar1_nat="ខ្មែរ":$guar1_nat="";
      $vk->client_idcard_no!=null?$guar1_id=$vk->client_idcard_no:$guar1_id="";
      if($vk->client_type_idcard == 1){
        $guar1_idt = " អត្តសញ្ញាណប័ណ្ណ ";
      }elseif($vk->client_type_idcard == 2){
        $guar1_idt = " លិខិនឆ្លង់ដែន ";
      }elseif($vk->client_type_idcard == 3){
        $guar1_idt = " សំបុត្របញ្ជាក់កំណើត ";
      }elseif($vk->client_type_idcard == 4){
        $guar1_idt = " សៀវភៅសា្នក់នៅ ";
      }elseif($vk->client_type_idcard == 5){
        $guar1_idt = " សៀវភៅគ្រួសារ ";
      }else{
        $guar1_idt = $vk->client_type_idcard;
      }
      $vk->client_identify_date!=null?$guar1_idd=$vk->client_identify_date:$guar1_idd="";
      $vk->client_aprovel_idcard_by==1?$guar1_idby="ក្រសួងមហាផ្ទៃ":$guar1_idby="";
      // Address
      $vk->client_house_num!=null?$guar1_home=$vk->client_house_num:$guar1_home="";
      $vk->client_group_num!=null?$guar1_group=$vk->client_group_num:$guar1_group="";
      $vk->client_st_num!=null?$guar1_street=$vk->client_st_num:$guar1_street="";
      $vk->client_village!=null?$guar1_village=$vk->client_village:$guar1_village="";
      $vk->client_commune!=null?$guar1_commune=$vk->client_commune:$guar1_commune="";
      $vk->client_district!=null?$guar1_district=$vk->client_district:$guar1_district="";
      $vk->client_province!=null?$guar1_province=$vk->client_province:$guar1_province="";

    }else{
      $guar1_name = "...............................";
      $guar1_sex = "...............................";
      $guar1_dob = "...............................";
      $guar1_nat = "...............................";
      $guar1_id = "...............................";
      $guar1_idt = "...............................";
      $guar1_idd = "...............................";
      $guar1_idby = "................................";

      $guar1_home = "........................";
      $guar1_group = ".......................";
      $guar1_street = ".........................";
      $guar1_village = ".........................";
      $guar1_commune = "..........................";
      $guar1_district = "..........................";
      $guar1_province = "...............................";
    }
    if($gk==1){
      $vk->client_name_kh!=null?$guar2_name=$vk->client_name_kh:$guar2_name="";
      $vk->client_gender=="M"?$guar2_sex="ប្រុស":$guar2_sex="ស្រី";
      $guar2_dob = date_format(date_create($vk->client_dob), "d m Y");
      $vk->client_nationality==1?$guar2_nat="ខ្មែរ":$guar2_nat="";
      $vk->client_idcard_no!=null?$guar2_id=$vk->client_idcard_no:$guar2_id="";
      if($vk->client_type_idcard == 1){
        $guar2_idt = " អត្តសញ្ញាណប័ណ្ណ ";
      }elseif($vk->client_type_idcard == 2){
        $guar2_idt = " លិខិនឆ្លង់ដែន ";
      }elseif($vk->client_type_idcard == 3){
        $guar2_idt = " សំបុត្របញ្ជាក់កំណើត ";
      }elseif($vk->client_type_idcard == 4){
        $guar2_idt = " សៀវភៅសា្នក់នៅ ";
      }elseif($vk->client_type_idcard == 5){
        $guar2_idt = " សៀវភៅគ្រួសារ ";
      }else{
        $guar2_idt = $vk->client_type_idcard;
      }
      $vk->client_identify_date!=1?$guar2_idd=$vk->client_identify_date:$guar2_idd="";
      $vk->client_aprovel_idcard_by==1?$guar2_idby="ក្រសួងមហាផ្ទៃ":$guar2_idby="";
    }else{
      // Guaranteer 1
      
      $guar2_name = "................................";
      $guar2_sex = "................";
      $guar2_dob = "....................................";
      $guar2_nat = "................................";
      $guar1_id = "................................";
      $guar2_idt = "...................................";
      $guar2_idd = "................................";
      $guar2_idby = "................................";

    }
  }
}else{
      $guar2_name = "................................";
      $guar2_sex = "................";
      $guar2_dob = "....................................";
      $guar2_nat = "................................";
      $guar1_id = "................................";
      $guar2_idt = "...................................";
      $guar2_idd = "................................";
      $guar2_idby = "................................";
      $guar1_name = "...............................";
      $guar1_sex = "...............................";
      $guar1_dob = "...............................";
      $guar1_nat = "...............................";
      $guar1_id = "...............................";
      $guar1_idt = "...............................";
      $guar1_idd = "...............................";
      $guar1_idby = "................................";

      $guar1_home = "........................";
      $guar1_group = ".......................";
      $guar1_street = ".........................";
      $guar1_village = ".........................";
      $guar1_commune = "..........................";
      $guar1_district = "..........................";
      $guar1_province = "...............................";
}
?>
<html><head>
    <title>កិច្ចសន្យាខ្ចីប្រាក់</title>
    <link href="https://fonts.googleapis.com/css?family=Khmer" rel="stylesheet">
  
    <link rel="stylesheet" type="text/css" href="http://sys.titbserver.com/mfi-en/public/bootstrap/css/form.css">
     <!-- Bootstrap -->
     <link href="http://sys.titbserver.com/mfi-en/public/bootstrap/css/bootstrap.min.css" rel="stylesheet" media="screen">
     <script>
      function myFunction() {
  
          window.print();
      }
    </script>
    <style type="text/css">
      @font-face{
        font-family: "Khmer Os Battambang";
        src:local("Khmer Os Battambang"), url("assets/fonts/KhmerOSbattambang.woff") format("woff"), url("assets/fonts/KhmerOSbattambang.woff") format("truetype");
      }
      @media  screen {
          header.onlyprint, footer.onlyprint{
              display: none; /* Hide from screen */
          }
          .container-fluid {
            padding-right: 200px;
            padding-left: 200px;
          }
          .content-main{ 
            margin: 50px;
          }
          body{
            font-family: khmer os;
          }
          ol{list-style: none;}
  
      }
  
      @media  print {
		.last_see2 {
			margin-top: 20px !important;
		}
        .span12 {
          width:100%;
        }
        .print_lo {
          margin-left:200px;
        }
        body{
            font-family: khmer os;
            counter-reset:page;
          }
        
          ol{list-style: none;}
        header.onlyprint:first-child{
              display:none;
          }
        header.onlyprint {
             position:fixed ; 
              bottom: -50px; 
              font-size: 12px;
          }
          .header_left {
            font-size: 14px;
          }
          footer.onlyprint {
              position: fixed;
              bottom: 25px; /* Because it's footer */
              font-size: 12px;
          }  
          .content-main{
            margin:30px 0px;
          }
          .page-break{ page-break-after:always; }
          footer.onlyprint span:after {
            margin-left: 580px;
          /*content: counter(page);
          counter-increment: page;*/
          
        }
        p{
          /* font-size: 16px; */
        }
        section:before {
          position:relative;
          font-size:12px;
          left:690px;
          top: 1045px;
          counter-increment: page;
          content: "ទំព័រទី ​​" counter(page) "  នៃ 3";	
        } 
        
      } 
         
      
    </style>
      </head>
  
  <body class="font_load-header">
    <div class="container-fluid">
        <div class="row-fluid ">
          <button id="printPageButton" onclick="myFunction()" class="btn btn-primary">Print</button>	
          <header class="onlyprint">
            <span></span>
            <span style="margin-left:700px;font-size:11px;">ទម្រង់ទី ០៣</span>
            <hr style="width:1000px;margin-top:0px;">
          </header>
      <section>
          <div class="content-main" style="margin-top: 0px;">
            <div class="span12 header main-title ">
            <center>
              <span class="print_lo">ព្រះរាជាណាចក្រកម្ពុជា</span><br>
              <span class="print_lo">	ជាតិ សាសនា ព្រះមហាក្សត្រ</span>
            </center>
          </div>
        <div class="span12 main-title header_left">
        ខេ.អេ.អេស.ភី.  កសិករ ពាណិជ្ជ ឯ.ក<br>
		លេខកិច្ចសន្យា: <span>{{$contract_code}}</span>
        </div>
        <div style="clear:both;"></div>
        <div class="main-title span12">
          <center style="margin-top:20px;">
          កិច្ចសន្យាទិញបង់រំលស់	
            <p>ធ្វើនៅ <span>.......... </span> ថ្ងៃទី <span>..........  </span> ឆ្នាំ <span>.......... </span></p>
                      រវាង				
                  </center>
        </div>
        <div class="span12 content">
          <ol>
            <li>ក្រុមហ៊ុន <span style="font-family: Khmer OS Muol Light !important;">ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក</span> ដែលមានលោក <span style="font-family: Khmer OS Muol Light !important;">ផុន ភារម្យ</span>  តួនាទីជា <span style="font-family: Khmer OS Muol Light !important;">ប្រធានសាខា</span> ដែលមានអាសយដ្ឋាន ផ្ទះលេខ៦២ ផ្លូវលេខ៦២​ R ភូមិទទួលគោក សង្កាត់ទួលសង្កែ ខណ្ឌឬស្សីកែវ រាជធានីភ្នំពេញ ជាតំណាងរបស់ក្រុម				
  
                  </span>តទៅនេះហៅកាត់ថាភាគី  (ក)</li>
          </ol>
          
        </div>
        <div class="span12 title_center main-title">
          <center>និង</center>
        </div>
        <div class="span12 content li_height">
          <ol>
            <li>
            ២. &nbsp;ឈ្មោះ <span> {{$client_name}} </span>
                          ភេទ <span>{{$client_sex}}</span>
                          ថ្ងៃខែឆ្នាំកំណើត <span>{{$client_dob}}</span>
                           សញ្ជាតិ <span>{{$client_nation}}</span><br>
                          កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span>{{$client_id}}</span>
                           ប្រភេទប័ណ្ណ <span>{{$client_idtype}}</span><br>
                          ចុះថ្ងៃទី <span>{{$client_iddate}}</span> ចេញដោយ <span>{{$client_idby}} </span>
                          និងឈ្មោះ <span>....... </span>
                          ភេទ <span>.......</span><br>
                          ថ្ងៃខែឆ្នាំកំណើត <span>.......</span>
                          សញ្ជាតិ <span>.......</span>
                          កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span>.......</span><br>
                          ប្រភេទប័ណ្ណ <span>.......</span>
                          ចុះថ្ងៃទី <span>.......</span>
                          ចេញដោយ <span> .......	</span> ។<br>
                          ថ្ងៃខែឆ្នាំកំណើត <span></span>
                          អាសយដ្ឋានផ្ទះលេខ <span>{{$client_home_num}}</span>
                          ក្រុមទី <span>{{$client_group_num}}</span>
                          ផ្លូវលេខ <span>{{$client_street_num}}</span>
                          ភូមិ <span>{{$client_village}}</span>
                          ឃុំ/សង្កាត់ <span>{{$client_commune}}</span><br>
                          ស្រុក/ខណ្ឌ <span>{{$client_district}}</span>
                          ខេត្ត/ក្រុង <span>{{$client_province}}</span>
                          តទៅនេះហៅកាត់ថាភាគី (ខ) ។<br>
            </li>
                  </ol>
                  
          <p style="margin-left: 23px;" class="paragrap_p">
              ភាគីទាំងពីរបានព្រមព្រៀងចុះកិច្ចសន្យា ដោយអនុវត្តតាមរាល់ប្រការដូចខាងក្រោម៖<br>
          </p>
          
        </div>
        <div class="main-title span12">ប្រការ១៖ អំពីលក្ខខណ្ឌរួម</div>
        <div class="span12 content">
          <p style="margin-left: 23px;" class="paragrap_p page-break">
		  	១.១ ភាគី (ខ) យល់ព្រមទិញ {{$item_name}} ចំនួន {{$item_qty}} គ្រឿង <br> 
			  ម៉ាក {{$item_brand}} លេខកូដ {{$item_code}} <br>
			  ក្នុងតម្លៃសរុបជាលេខ {{$total_item_price}} (US$) ជាអក្សរ {{$tipw}}<br>
			  ដោយបង់ប្រាក់កក់មុនចំនួន {{$booking_percent}} ភាគរយ ស្មើនឹងចំនួនសរុបជាលេខ {{$booking_price}} <br>
			  ជាអក្សរ {{$booking_word}}។<br>
	  		១.២ ភាគី (ខ) យល់ព្រមបង់រំលស់ប្រាក់ដែលនៅសល់រួមទាំងការប្រាក់ចំនួនជាលេខ {{$money_owe}} US$
			  ជាអក្សរ {{$money_owe_word}} ក្នុងរយៈពេល {{$duration_pay}} {{$dpt}} ទៅតាមដំណាក់កាលដូចខាងក្រោមៈ
		  </p>
          
        </div>
        </div></section>
        
        <section>
		<div class=" span12">
    <br>
			<ul type="square" style="margin-left:43px;">
      
      <li style="list-style:none;"><p class="MsoNoSpacing" style="text-align:justify"><span style="font-size:14px;
        font-family:Wingdings;mso-ascii-font-family:&quot;Khmer OS Battambang&quot;;mso-hansi-font-family:
        &quot;Khmer OS Battambang&quot;;mso-bidi-font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-language:
        KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings"><span style="mso-char-type:symbol;mso-symbol-font-family:Wingdings">{{$c_week}}</span></span><span lang="KHM" style="font-size:14px;font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-language:
        KHM"> សបា្តហ៍ <span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="font-size:14px;font-family:Wingdings;mso-ascii-font-family:&quot;Khmer OS Battambang&quot;;
        mso-hansi-font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-font-family:&quot;Khmer OS Battambang&quot;;
        mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings"><span style="mso-char-type:symbol;mso-symbol-font-family:Wingdings">{{$c_2week}}</span></span><span lang="KHM" style="font-size:14px;font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-language:
        KHM"> ពីរសបា្តហ៍<span style="mso-spacerun:yes">&nbsp;&nbsp; </span><span style="mso-tab-count:1">&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp; </span></span><span style="font-size:14px;
        font-family:Wingdings;mso-ascii-font-family:&quot;Khmer OS Battambang&quot;;mso-hansi-font-family:
        &quot;Khmer OS Battambang&quot;;mso-bidi-font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-language:
        KHM;mso-char-type:symbol;mso-symbol-font-family:Wingdings"><span style="mso-char-type:symbol;mso-symbol-font-family:Wingdings">{{$c_month}}</span></span><span lang="KHM" style="font-size:14px;font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-language:
        KHM"> ខែ</span><span style="font-size:14px;font-family:Wingdings;mso-ascii-font-family:
        &quot;Khmer OS Muol&quot;;mso-hansi-font-family:&quot;Khmer OS Muol&quot;;mso-bidi-font-family:
        &quot;Khmer OS Muol&quot;;mso-bidi-language:KHM;mso-char-type:symbol;mso-symbol-font-family:
        Wingdings"><span style="mso-char-type:symbol;mso-symbol-font-family:Wingdings"></span></span><span style="font-size:14px;font-family:&quot;Khmer OS Battambang&quot;;mso-bidi-language:
        KHM"><o:p></o:p></span></p></li>
				<li>ចំនួនជាលេខ US$ {{$payment_last}} x {{$duration_pay}} ដង <span></span></li>
				<li>ទូរទាត់ចាប់ពីថ្ងៃទី {{$day_pay}} ខែ {{$month_pay}} ឆ្នាំ {{$year_pay}} </li>
				<li class="" style="list-style:none;">  នឹងបញ្ចប់ថ្ងៃទី {{$finish_pay_day}} ខែ {{$finish_pay_month}} ឆ្នាំ {{$finish_pay_year}} (តារាងកាលវិភាគសងប្រាក់)។</li>
			</ul>
		</div>
    <div class="span12 content li_list">
      <p style="margin-left: 23px;" class="paragrap_p"> ១.៣ ភាគី (ខ) យល់ព្រមទូទាត់សំណងថ្លៃដឹកជញ្ជូន និងសេវារដ្ឋបាលផ្សេងៗ ដែលមានតម្លៃស្មើ១០% នៃតម្លៃទំ និញ ក្នុងករណីភាគី(ខ) មិនទទួលយកទំនិញដែលបានដឹកជញ្ជូនទីកន្លែង។</p>
      <p style="margin-left: 23px;" class="paragrap_p"> ១.៤ ភាគី(ក) ធានាលើលក្ខនៃទំនិញ គុណភាពផលិតផលល្អ១០០% និងមានប័ណ្ណធានារយៈពេលមួយឆ្នាំ។</p>
    </div>
    <div class="main-title span12" style="">
		
        ប្រការ២៖ អំពីលក្ខខណ្ធពិសេស			</div>
        <div class="span12 content li_list">
           <p style="margin-left: 23px;" class="paragrap_p">
           ២.១ ភាគី (ខ) មានកាតព្វកិច្ចបង់ប្រាក់ឲ្យបានទៀងទាត់ទៅតារាងកាលវិភាគសងប្រាក់ដែលបានកំណត់។ ក្នុងករណីភាគី (ខ) 
           មិនបានគោរពលក្ខខណ្ឌទាំងឡាយនៃកិច្ចសន្យានេះ ភាគី (ខ) យល់ព្រមឲ្យភាគី (ក) រឹបអូសសម្ភារៈដែលបានទិញពីក្រុមហ៊ុន 
           ឬសម្ភារៈផ្សេងៗដែលមានតម្លៃប្រហាក់ប្រហែលគ្នា ដើម្បីលក់ឡាយឡុងសម្រាប់ទូទាត់ប្រាក់បំណុលដែលនៅខ្វះ។					 </p>
           <p style="margin-left: 23px;" class="paragrap_p">
           ២.២ ក្នុងករណី ភាគី (ខ) ខកខានមិនបានសងប្រាក់គ្រប់ចំនួនតាមថ្ងៃកំណត់ដូចមានក្នុងតារាងកាលវិភាគសងប្រាក់នោះ ភាគី​ (ខ) 
           យល់ព្រមបង់ប្រាក់ពិន័យឲ្យ ភាគី (ក)។ ប្រាក់ពិន័យនេះត្រូវគណនាដោយយកចំនួនទឹកប្រាក់ដែលខកខានមិនបានសង គុណនឹងអត្រា ១០% 
           ទៅតាមចំនួនថ្ងៃដែលបានយឺត។				 </p>
           <p style="margin-left: 23px;" class="paragrap_p">
           ២.៣ ក្នុងករណីមិនអាចអនុវត្តកិច្ចសន្យាបាន មុនចប់អាណត្តិដូចជា បាត់បង់លទ្ធភាពការងារ ឬមរណៈភាព អ្នកធានារបស់ភា​គី​ (ខ) 
           ជាអ្នកទទួលខុសត្រូវទាំងស្រុងក្នុងការអនុវត្តកិច្ចសន្យាបន្ត។ </p>
		</div>
        <div class=" main-title span12">
        ប្រការ៣៖ កាតព្វកិច្ចអ្នកធានា			</div>
                       <div class="span12 content li_list">
           <p style="margin-left: 23px;" class="paragrap_p">
                    ៣.១ អ្នកធានាឈ្មោះ <span> {{$guar1_name}} </span>ភេទ <span> {{$guar1_sex}} </span> ថ្ងៃខែឆ្នាំកំណើត: <span> {{$guar1_dob}} </span>សញ្ជាតិ <span> {{$guar1_nat}} </span><br>
                    
                    កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ <span> {{$guar1_id}} </span>ប្រភេទប័ណ្ណ  <span> {{$guar1_idt}} </span><br>

                    ចុះថ្ងៃទី <span> {{$guar1_idd}} </span>ចេញដោយ <span> {{$guar1_idby}} </span>និងឈ្មោះ <span> {{$guar2_name}} </span> <br>

                    ភេទ<span> {{$guar2_sex}} </span>ថ្ងៃខែឆ្នាំកំណើត: <span> {{$guar2_dob}} </span>សញ្ជាតិ 	<span> {{$guar2_nat}} </span><br>      

                    កាន់ប័ណ្ណសំគាល់អត្តសញ្ញាណលេខ<span> {{$guar1_id}} </span>ប្រភេទប័ណ្ណអត្តសញ្ញាណប័ណ្ណ<span> {{$guar2_idt}} </span> <br>

                    ចុះថ្ងៃទី <span> {{$guar2_idd}} </span> ចេញដោយ {{$guar2_idby}} ។ 

                    អាសយដ្ឋានផ្ទះលេខ <span> {{$guar1_home}} </span>

                    ក្រុមទី <span> {{$guar1_group}}	</span>

                    ផ្លូវលេខ <span> {{$guar1_street}} </span><br> 

                    ភូមិ <span> {{$guar1_village}} </span>

                    ឃុំ/សង្កាត់ <span> {{$guar1_commune}} </span>

                    ស្រុក/ខណ្ឌ <span> {{$guar1_district}} </span>

                    ខេត្ត/ក្រុង <span> {{$guar1_province}} </span>
           </p>     
                <p style="margin-left: 23px;" class="paragrap_p page-break">
                ៣.២ ក្នុងករណីភាគី (ខ) គ្មានលទ្ធភាពសងប្រាក់ជូនភាគី (ក) ទេ នោះភាគី (គ) 
                នឹងមានកាតព្វកិច្ចសងប្រាក់ជំពាក់ទាំងអស់ ដែលមិនទាន់បានសងជំនួសភាគី (ខ) ឲ្យគ្រប់ចំនួន។				 </p>
           
        </div></section></div>

        <section>
        <div class=" main-title span12 content-main" style="margin-bottom: 10px;">ប្រការ៤៖ អវសានបញ្ញត្តិ</div>
        <div class="span12 content li_list ">
           <p style="margin-left: 23px;" class="paragrap_p ">
           ៤.១ ភាគី (ក) ភាគី (ខ) និងភាគី (គ) សន្យាគោរពយ៉ាងមឺងម៉ាត់តាមរាល់ប្រការនៃកិច្ចសន្យានេះ។ ក្នុងករណីមានការអនុវត្តផ្ទុយ ឬដោយរំលោភលើលក្ខខណ្ឌណាមួយនៃកិច្ចសន្យានេះ 
           ភាគីដែលល្មើសត្រូវទទួលខុសត្រូវចំពោះមុខច្បាប់ជាធរមាន។ រាល់សោហ៊ុយចំណាយទាក់ទងក្នុងការដោះស្រាយ វិវាទ ជាបន្ទុករបស់ភាគីដែលបង្ករការរំលោភបំពានលើកិច្ចសន្យា។				 </p>
           <p style="margin-left: 23px;" class="paragrap_p ">
           ៤.២​ ភាគីទាំងអស់បានអាន និងយល់យ៉ាងច្បាស់អំពីអត្ថន័យនៃកិច្ចសន្យា ហើយស្ម័គ្រចិត្តគោរពយ៉ាងម៉ឺងម៉ាត់រាល់ប្រការទាំងអស់ដែលមានចែងនៅក្នុងកិច្ចសន្យានេះដោយគ្មានកាបង្ខិតបង្ខំឡើយ។				 </p>
           <p style="margin-left: 23px;" class="paragrap_p">
           ៤.៣ កិច្ចសន្យា និងតារាងកាលវិភាគសងប្រាក់នេះមានសុពលភាពចាប់ពីថ្ងៃចុះហត្ថលេខា និងផ្តិតស្នាមមេដៃស្តាំតទៅ។				 </p>
           <p style="margin-left: 23px;" class="paragrap_p">
           ៤.៤ កិច្ចសន្យានេះត្រូវបានធ្វើឡើងជា ០៣ច្បាប់ជាភាសាខ្មែរ ១ច្បាប់ដើមរក្សាទុកភាគី (ក) ១ច្បាប់រក្សាទុកនៅភាគី (ខ) និង១ច្បាប់ទៀតរក្សានៅភាគី (គ)។ 				 </p>
        </div>
        </section>
        <div class="span12 font-title">
          <div class="span4" style="float:left; width:250px" id="column-left">
            <center>ស្នាមមេដៃស្តាំភាគី (ខ)</center>
            <center>
                <div class="span4 sub_col1_1" style="">
                    <p style="margin-left: 70px;"> ប្តី / ប្រពន្ធ</p>
                </div>
                <!-- <div class="span4" style="float: left;">
                    <p></p>
                </div> -->
            </center>
          </div>
          <div class="span4" style="float:left;width:220px" id="column-center">
            <center>ស្នាមមេដៃស្តាំអ្នកធានា</center>
            <center>
                <div class="span4 sub_col1_1" style="">
                    <p style="margin-left: 70px;"> ភាគី(គ)</p>
                </div>
            </center>
          </div>
          <div class="span4" style="float:right; width:200px;" id="column-right">
            <center>តំណាងឲ្យអ្នកខ្ចីប្រាក់ ភាគី(ក) </center>
            <div class="span4">
              <center><p>ហត្ថលេខា និងឈ្មោះ</p></center>
            </div>
          </div>
        </div>
        <div class="span12 br_line"></div>
        <div class="span12">
          <div class="span6" style="float:left;">
            <div class="span3" style="float:left;padding-right: 3px;margin-right: 60px;">
              <p>ឈ្មោះ ..........​</p>
            </div>
            <div class="span3" style="float:right;">
              <p>ឈ្មោះ ..........</p>
            </div>
            <div style="clear:both;"></div>
          </div>
          <div class="span6">
            <div class="span3" style="float:left;margin-left:60px;">
              <p>ឈ្មោះ ..........</p>
            </div>
            <div class="span3" style="float:right;margin-right:45px;">
              <p>ឈ្មោះ ..........</p>
            </div>
          </div>
          <div style="clear:both;"></div>
        </div>
		<div class="span12 font-title  ">
          <div class="span6   " style="float:left; margin:50px  90px 0 0px; height:189px;">
          <center >
            បានឃើញ និងបញ្ជាក់ថាសេចក្តីបញ្ជាក់របស់<br>
            មេភូមិ...............................................<br>
            <p>ថ្ងៃទី..................ខែ.....................ឆ្នាំ....................</p>
            មេឃុំ/ចៅសង្កាត់.....................................
            <br>ហត្ថលេខា និងត្រា
    
            </center>
          </div>
          <div class="span5 last_see2  " style="float:right;margin-top:20px;">
            <center>
            <b class="">បានឃើញ និងទទួលស្គាល់ថា</b><br>
          <p style=" width:310px; text-align:justify;" class=" ">កម្មសិទ្ធិស្របច្បាប់របស់ភាគី(ខ) ប្រាកដមែន ហើយទ្រព្យទាំងនេះពុំមាន  ពាក់ព័ន្ធនិងបញ្ហាអ្វីឡើយ ហើយភាគី(ខ) បានយល់ព្រម ដាក់បញ្ចាំដោយ ស្ម័គ្រចិត្តឲ្យ ក្រុមហ៊ុន ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក ពិតប្រាកដមែន។ <br>
          <p class=" ">ថ្ងៃទី..................ខែ.....................ឆ្នាំ....................</p>
          មេភូមិ................................................
          <br/>ហត្ថលេខា និងឈ្មោះ
          </p>  
            </center>
          </div>
          <span class="pag1"></span>
      </div>
          </div>
        <footer class="onlyprint">
          <hr style="width:1000px;margin-bottom:5px;">ខេ.អេ.អេស.ភី. កិច្ចសន្យាទិញបង់រំលស់
          <span></span>
        </footer>
  
        
    
  
  
  </body></html>