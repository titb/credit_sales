@extends('credit_sale.layout.master')
@section('contend')
<style >
    .mytab3 td{
        border-top: 0px solid #ddd !important;  
    }
    .mytab1 th ,.mytab1 td{
        border-top: 0px solid #ddd !important;  
    }
    /* .mytab1 td{
        border-top: 0px solid #ddd !important;  
    } */


</style>
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('aprove_credit_sales') !!}"> ការអនុម័តឥណទាន</a> <span class="divider">|</span> ការបង្ហាញការអនុម័តឥណទាន </div>					
                                    <button id="back_to_prev" class="btn btn-danger pull-right back_to_prev" style="font-family: 'Hanuman' !important;" onclick="back_to_prev()"><i class=" icon-arrow-left icon-white"></i> back </button>
                            </div>

                            <div class="block-content collapse in">
                            <button class="btn btn-success pull-left" id="print_hide" style=" margin-right: 10px; font-family: 'Hanuman' !important;" onclick="with_print()"><i class="icon-print icon-white"></i> ព្រីនកិច្ចសន្យាទិញបង់រំលស់ </button>   
                            <button id="print_generate_schedule" class="btn btn-info pull-left print_generate_schedule" style="font-family: 'Hanuman' !important;"  onclick="with_print_schedul('myprint')"  name="print_generate_schedule"><i class="icon-th-list icon-white"></i> ព្រីនកាលវិភាគសងប្រាក់អតិថិជន </button>

                            @include('errors.error')	
                            <div id="myprint" class="span8"  style="margin: 0 auto !important;float: none;"> 	
			                        <!-- <h3 class="cen_title text-center khmer_Moul ">{{$title}}</h3>
			                   
                            	<legend></legend> -->
                                <style>
                                        @media print {
                                            table {
                                                max-width: 100%;
                                                background-color: transparent;
                                                border-collapse: collapse;
                                                border-spacing: 0;
                                            }
                                            .cen_title {
                                                    color: #000;
                                            }
                                            .text-center {
                                                    text-align: center;
                                            }
                                            table.table {
                                                clear: both;
                                                margin-bottom: 6px !important;
                                                max-width: none !important;
                                            }
                                            .table-bordered {
                                                width:100%;
                                                border: 1px solid #353131;
                                                border-top: 0px solid #353131;
                                                border-collapse: separate;
                                                border-left: 0;
                                                /* -webkit-border-radius: 4px;
                                                -moz-border-radius: 4px;
                                                border-radius: 4px; */
                                            }
                                            .table-bordered th, .table-bordered td {
                                                padding: 8px;
                                                line-height: 10px;
                                                border-left: 1px solid #353131;
                                                border-top: 1px solid #353131;
                                            }
                                            .mytab1 {
                                                width:100%;
                                            }
                                            .mytab1 th{
                                                    padding: 8px;
                                                    text-align: left;
                                                    vertical-align: top; 
                                                    line-height: 10px;   
                                            }
                                          
                                            .table th {
                                                font-weight: 200;
                                            }
                                            p {
                                                margin: 0 0 10px;
                                                line-height: 15px;
                                                letter-spacing: 1px;
                                                font-weight: 100;
                                            }
                                            .mytab3 td{
                                                padding: 8px;
                                                    text-align: left;
                                                    vertical-align: top; 
                                                    line-height: 20px;   
                                            }
                                        }
                                </style>
                                 <h2 class="cen_title text-center khmer_Moul "​​ style="font-size: 25px !important;line-height: 30px !important;">ក្រុមហ៊ុន ខេ.អេ.អេស.ភី. កសិករ ពាណិជ្ជ ឯ.ក</h2>
                                 <h3 class="cen_title text-center khmer_Moul ">កាលវិភាគសងប្រាក់អតិថិជន</h3>   
                                 <div class="span12"  style="margin: 0 auto !important;float: none;">
                                    <div class="span12" style="margin-left:0;">
                                        <table class="table mytab1">
                                            <tr>
                                                <th>ឈ្មោះបុគ្គលិកលក់</th>
                                                <th> <span class="staff_sale_name"></span> </th>
                                                <th>  <span class="staff_sale_phone"></span> </th>
                                                <th  style="text-align: right;"  >លេខកូដអតិថិជន</th>
                                                <th>  <span class="schedule_id"></span> </th>
                                            </tr>
                                            <tr>
                                                <th>បុគ្គលិកប្រមូលប្រាក់</th>
                                                <th> <span class="staff_colect_name"></span> </th>
                                                <th> <span class="staff_colect_phone"></span> </th>
                                                <th  style="text-align: right;"  >ចំនួនទឹកប្រាក់</th>
                                                <th> <span class="total_num_of_own"></span> </th>
                                            </tr>
                                            <tr>
                                                <th>ឈ្មោះអតិថិជន</th>
                                                <th> <span class="cleint_name"></span> </th>
                                                <th> <span class="client_address"></span> </th>
                                                <th  style="text-align: right;"  >រយៈពេលសង</th>
                                                <th> <span class="duration_payment"></span> </th>
                                            </tr>
                                            <tr>
                                                <th>ឈ្មោះទំនិញ</th>
                                                <th> <span class="item_name"></span> </th>
                                                <th> <span class="item_qty"></span> </th>
                                                <th  style="text-align: right;"  >ថ្ងៃទទួលទំនិញ</th>
                                                <th> <span class="date_give_item"></span> </th>
                                            </tr>
                                            <tr>
                                                <th>លេខ​កូដ​ទំនិញ</th>
                                                <th colspan="2"> <span class="item_code"></span> </th>
                                                <!-- <th> <span class="item_name"></span> </th> -->
                                                <!-- <th  style="text-align: right;"  >ប្រាក់បង់តំបូង៣០%</th>
                                                <th> <span class="deposit_fixed"></span> </th> -->
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <!-- <th  style="text-align: right;"  >ប្រាក់ដែលនៅជំពាក់</th>
                                                <th> <span class="money_owne_cost"></span> </th> -->
                                               
                                            </tr>
                                            <tr>
                                                <th></th>
                                                <th></th>
                                                <th></th>
                                                <!-- <th style="text-align: right;" >អត្រាការប្រាក់</th>
                                                <th> <span class="interest_rate"></span> </th> -->
                                            </tr>
                                        </table>
                                        <br/>
                                 <br/>  
                                 </div> 
                                 
                                </div>
                            @if(Auth::user()->groups->first()->hasPermission(['developer'])) 
                                <div class="span12"  style="margin: 0 auto !important;float: none;">
                                        <div class="span12">
                                                    <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                                <th rowspan="2" colspan="2" style="text-align: center;vertical-align: middle;"> ថ្ងៃត្រូវបង់ប្រាក់ </th>
                                                                <th colspan="4" style="text-align:center">ចំនួនប្រាក់ត្រូវបង់ </th>
                                                                <th rowspan="2" style="text-align: center;vertical-align: middle;"> ថ្ងៃបានសង​​​​​ </th>
                                                                <th rowspan="2" style="text-align: center;vertical-align: middle;"> ចំនួនប្រាក់បានសង </th>
                                                                <th rowspan="2" style="text-align: center;vertical-align: middle;"> ហត្ថលេខាអ្នកទទួលប្រាក់ </th>
                                                            </tr>
                                                            <tr>
                                                                <th style="text-align: center;vertical-align: middle;">ប្រាក់ដើម</th>
                                                                <th style="text-align: center;vertical-align: middle;">ប្រាក់ការ</th>
                                                                <th style="text-align: center;vertical-align: middle;">ប្រាក់ដែលត្រូវបង់</th>
                                                                <th style="text-align: center;vertical-align: middle;">ប្រាក់ដែលនៅជំពាក់</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="body_time_list">
                                                           <tr>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                                <td></td>
                                                           </tr>
                                                        </tbody>
                                                                
                                                    </table>  
                                                        <br/>
                                                        <br/>
                                                        <br/>
                                                        <table class="table mytab3"> 
                                                                <tr>
                                                                    <td style="text-align: center;">ហត្ថលេខាប្រធានសាខា</td>
                                                                    <td></td>
                                                                    <td style="text-align: center;">ស្នាមមេដៃស្ដាំអ្នកទទួលប្រាក់ </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> </td>
                                                                    <td> </td>
                                                                    <td> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td> </td>
                                                                    <td> </td>
                                                                    <td> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td style="text-align: center;">________________________________________________</td>
                                                                    <td width="150px"></td>
                                                                    <td style="text-align: center;">_______________________________________________
                                                                        <br/>
                                                                        <span class="client_name_payment"> </span>
                                                                    </td>
                                                                </tr>
                                                               
                                                        </table> 
                                                <legend></legend> 
                                        </div>
                                 </div>
                                @else
                                    <div class="span12"  style="margin: 0 auto !important;float: none;">
                                            <div class="span12">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th rowspan="2" colspan="2" style="text-align: center;vertical-align: middle;"> ថ្ងៃត្រូវបង់ប្រាក់ </th>
                                                                    <th colspan="2" style="text-align:center">ចំនួនប្រាក់ត្រូវបង់ </th>
                                                                    <th rowspan="2" style="text-align: center;vertical-align: middle;"> ថ្ងៃបានសង​​​​​ </th>
                                                                    <th rowspan="2" style="text-align: center;vertical-align: middle;"> ចំនួនប្រាក់បានសង </th>
                                                                    <th rowspan="2" style="text-align: center;vertical-align: middle;"> ហត្ថលេខាអ្នកទទួលប្រាក់ </th>
                                                                </tr>
                                                                <tr>
                                                                    <th style="text-align: center;vertical-align: middle;">ប្រាក់ដែលត្រូវបង់</th>
                                                                    <th style="text-align: center;vertical-align: middle;">ប្រាក់ដែលនៅជំពាក់</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="body_time_list">
                                                            <tr>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                                    <td></td>
                                                            </tr>
                                                            </tbody>
                                                                    
                                                        </table>  
                                                            <br/>
                                                            <br/>
                                                            <br/>
                                                            <table class="table mytab3"> 
                                                                    <tr>
                                                                        <td style="text-align: center;">ហត្ថលេខាប្រធានសាខា</td>
                                                                        <td></td>
                                                                        <td style="text-align: center;">ស្នាមមេដៃស្ដាំអ្នកទទួលប្រាក់ </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> </td>
                                                                        <td> </td>
                                                                        <td> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td> </td>
                                                                        <td> </td>
                                                                        <td> </td>
                                                                    </tr>
                                                                    <tr>
                                                                        <td style="text-align: center;">________________________________________________</td>
                                                                        <td width="150px"></td>
                                                                        <td style="text-align: center;">_______________________________________________
                                                                            <br/>
                                                                            <span class="client_name_payment"> </span>
                                                                        </td>
                                                                    </tr>
                                                                
                                                            </table> 
                                                    <legend></legend> 
                                            </div>
                                    </div>    

                                @endif


			    			</div>
                        <!-- Edit Approval -->
                        </div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>
        <script>

           
           function with_print(){
                window.location= "{{ url('aprove_credit_sales/'.$data_id.'/print_contract') }}";
                
            }
            function with_print_schedul(print_generate_schedule){
                var printContents = document.getElementById(print_generate_schedule).innerHTML;
                var originalContents = document.body.innerHTML;
                document.body.innerHTML = printContents;
                window.print();
                document.body.innerHTML = originalContents;
            }
            function back_to_prev(){
                window.history.back();
                
            }
        </script>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
$(document).ready(function(){
    $(window).load(function(){
        get_sale_credit_sale_show();
    });
    function day_format_show(date_format){
                var d = new Date(date_format);

                var year_n = d.getFullYear();
                var month_n = d.getMonth() + 1;
                var day_n = d.getDate();
                if(month_n > 10){
                    month_n = month_n;
                }else{
                    month_n = "0"+month_n; 
                }
               
                if(day_n > 10){
                    day_n = day_n;
                }else{
                    day_n = "0"+day_n; 
                }

        return  day_n +"-"+month_n+"-"+year_n;
    }
    function day_format_show_kh(date_format){
                var d = new Date(date_format);
                
                var year_n = d.getFullYear();
                var month_n = d.getMonth() + 1;
                var day_n = d.getDate();

            //day kh 
                var days = ["អាទិត្យ","ចន្ទ័","អង្គារ","ពុធ","ព្រហស្បតិ៍","សុក្រ","សៅរ៍"]; 
                var day_n_letter = days[d.getDay()];

            //month kh
                var months = ['មករា', 'កុម្ភៈ', 'មីនា', 'មេសា', 'ឧសភា',  'មិថុនា', 'កក្កដា', 'សីហា', 'កញ្ញា', 'តុលា', 'វិច្ឆិកា', 'ធ្នូ'];
                var  month_n =  months[d.getMonth()]; 

                if(day_n > 10){
                    day_n = day_n;
                }else{
                    day_n = "0"+day_n; 
                }
                
        return  day_n_letter +"   "+ day_n +"-"+month_n+"-"+year_n;
    }



        function get_sale_credit_sale_show(){
            var url_json =  "{{ url('aprove_credit_sales/'.$data_id.'/schedule_show_json') }}";
                    $.ajax({
                        type: "GET",
                        url: url_json, 
                        dataType: "json",
                        success: function(result){
                            console.log(result);
                        var time_table = "";
                            $.each(result.cs_schedule_timesheet,function(i,fi){
                                if(fi.total_pay_interest !== 0){
                                    var indx = i+1;
                                    //var date_payment =day_format_show(fi.date_payment);
                                    var days = ["អាទិត្យ","ចន្ទ័","អង្គារ","ពុធ","ព្រហស្បតិ៍","សុក្រ","សៅរ៍"]; 
                                    time_table += "<tr>";
                                    time_table += "<td >"+indx+"</td>";
                                    time_table += "<td style='text-align: center;vertical-align: middle;'>"+ day_format_show_kh(fi.date_payment) +"</td>";
                                    time_table += "<td style='text-align: center;vertical-align: middle;'>"+ accounting.formatMoney(fi.total_pay_cost) +"</td>";
                                    time_table += "<td style='text-align: center;vertical-align: middle;'>"+ accounting.formatMoney(fi.total_pay_interest) +"</td>";
                                    time_table += "<td style='text-align: center;vertical-align: middle;'>"+ accounting.formatMoney(fi.total_payment) +"</td>";
                                    time_table += "<td style='text-align: center;vertical-align: middle;'>"+ accounting.formatMoney(fi.total_pay_cost_owe) +"</td>";
                                    time_table += "<td></td>";
                                    time_table += "<td></td>";
                                    time_table += "<td></td>";
                                    time_table += "</tr>";
                                }
                                
                            });  
                            $(".body_time_list").html(time_table);
                                  
                            $(".staff_sale_name").text(result.user.name_kh);
                            $(".staff_sale_phone").text(result.user.user_phone);
                            
                            $(".staff_colect_name").text(result.cs_staff.name_kh);
                            $(".staff_colect_phone").text(result.cs_staff.user_phone);
                            $(".cleint_name").text(result.cs_client.kh_username);
                            $(".schedule_id").text(result.id);
                            $(".total_num_of_own").text(accounting.formatMoney(result.cs_approvalcredit.prices_total_num));    
                            $(".client_name_payment").text(result.cs_client.kh_username);
                            var dpmt = result.cs_approvalcredit.duration_pay_money_type ;
                            if(dpmt == "month"){
                                dpmt = "ខែ";
                            }else if(dpmt == "2week"){
                                dpmt = "២ សប្តាហ៍";
                            }else if(dpmt == "week"){
                                dpmt = "សប្តាហ៍";
                            }else if(dpmt == "day"){
                                dpmt = "ថ្ងៃ";
                            }
                            var dura_pay = result.cs_approvalcredit.duration_pay_money +"   "+ dpmt;
                            $(".duration_payment").text(dura_pay);

                            $(".date_give_item").text(day_format_show(result.cs_approvalcredit.date_give_product));    
                            $(".deposit_fixed").text(accounting.formatMoney(result.cs_approvalcredit.deposit_fixed));    
                            $(".money_owne_cost").text(accounting.formatMoney(result.cs_approvalcredit.money_owne)); 

                            var interest = (result.cs_approvalcredit.interest_of_owne_precent  * 100).toFixed(2);

                            $(".interest_rate").text(interest+"%");    
                            
                           var item_late = " "; 
                           var item_code = " ";   
                           var item_qty = "";
                            $.each(result.cs_approvalcredit.approval_item ,function(x,fix){
                                item_late += "<p style='margin-bottom: 0px;'>"+fix.item.name+"</p>"; 
                                item_qty += "<p style='margin-bottom: 0px;'>"+fix.qty+"</p>"; 
                                item_code += "<p style='margin-bottom: 0px;'>"+fix.item.item_bacode+"</p>"; 
                            });   
                            
                            $(".item_name").html(item_late);
                            $(".item_qty").html(item_qty);
                            $(".item_code").html(item_code);
                            // Address
                                 var address , hn ,gn ,st, vl , cm , dis,pro ,gd ,sts , dob;
                                    // if(result.cs_client.home_num !== null){
                                    //     hn = result.cs_client.home_num +" , ";
                                    // }else{
                                    //     hn = "";
                                    // }
                                    // if(result.cs_client.group_num !== null){
                                    //     gn = result.cs_client.group_num +" , ";
                                    // }else{
                                    //     gn = "   ";
                                    // }
                                    // if(result.cs_client.street_num !== null){
                                    //     st = result.cs_client.street_num +"  ";
                                    // }else{
                                    //     st = "  ";
                                    // }
                                    if(result.cs_client.vilige !== null){
                                        vl = result.cs_client.vilige +"  ";
                                    }else{
                                        vl = "";
                                    }
                                    if(result.cs_client.commune !== null){
                                        cm = result.cs_client.commune +"  ";
                                    }else{
                                        cm = "";
                                    }
                                    if(result.cs_client.district !== null){
                                        dis = result.cs_client.district +"  ";
                                    }else{
                                        dis = "";
                                    }
                                    // if(result.cs_client.province !== null){
                                    //     pro = result.cs_client.province +"  ";
                                    // }else{
                                    //     pro = "";
                                    // }
                                    
                                   // address = hn +"  "+  gn +" "+  st +"  "+  vl  +"  "+  cm +"  "+  dis +"  "+  pro ; 
                                    address =  vl  +"  "+  cm +"  "+  dis ; 
                                    $('.client_address').html(address);


   
                            
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
        }

 });       
</script>            
@stop()
