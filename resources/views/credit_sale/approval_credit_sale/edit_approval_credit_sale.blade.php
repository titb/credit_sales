@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('aprove_credit_sales') !!}"> ការអនុម័តឥណទាន</a> <span class="divider">|</span> ការបង្កើតការអនុម័តឥណទាន </div>					
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center>
			                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
			                    </center>
                            	<legend></legend>
                                <form role="form" id="form_insert_aprove_credit_sales" name="form_insert_aprove_credit_sales" method="POST"  enctype="multipart/form-data">
                                        {{ csrf_field() }} 
                                            <div class="span8"  style="margin: 0 auto !important;float: none;">
                                                    <div class="span12" style="margin-left:0;">
                                                        <table class="table table-bordered">
                                                        <thead>
                                                            <tr>
                                                            <th>ឈ្មោះទំនិញ</th>
                                                            <th>លេខ​កូដ​</th>
                                                            <th>ម៉ាក</th>
                                                            <th>ចំនួន</th>
                                                            <th>តម្លៃ​ឯកតា</th>
                                                            <th>តម្លៃសរុប</th>
                                                            </tr>
                                                        </thead>
                                                        <tbody class="item_list">
                                                           
                                                        </tbody>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> តម្លៃសរុប </td>
                                                                    <td> <b class="prices_total_num_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រភេទនៃការទិញ </td>
                                                                    <td> <b class="method"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;"> ប្រាក់កក </td>
                                                                    <td> <b class="deposit_fixed_text"></b> </td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">  រយៈពេលបង់ប្រាក់</td>
                                                                    <td> <b class="duration_pay_money_text"></b>  / <b class="duration_pay_money_type_text"></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">  សុំបង់ថ្ងៃទី</td>
                                                                    <td> <b class="date_for_payments_text"></b></td>
                                                                </tr>
                                                                <tr>
                                                                    <td colspan="5" style="text-align: right;">ចំនួនទឹកប្រាក់</td>
                                                                    <td> <b class="money_owne_text"></b> </td>
                                                                </tr>
                                                            
                                                        </table>  
                                                        <br/>
                                                <legend></legend> 
                                                    </div>
                                                   
                                                   
                                                    <div class="span12" style="margin-left:0;">

                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ សេចក្តីបញ្ជាក់របស់គណៈកម្មការឥណទាន<span class="required" title="This place you must be put data">*</span></label>
                                                        <div class="controls">
                                                            <textarea name="comment_manager" data-required="1" rows="3" id="comment_manager" class="span12 m-wrap comment_manager"></textarea>
                                                        </div>
                                                        <label class="control-label">តម្លៃសរុបជាលេខ<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled prices_total_num number-format" name="prices_total_num" id="focusedInput" type="text" autocomplete="off">
                                                        </div>
                                                        <label class="control-label">តម្លៃសរុបជាអក្សរ<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled prices_totalword" name="prices_totalword" id="focusedInput" type="text" autocomplete="off">
                                                        </div>
                                                        <label class="control-label">ភាគរយនៃប្រាក់កក់ (%)<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled deposit_precent number-format" name="deposit_precent" id="focusedInput" type="text" value="30"  autocomplete="off">
                                                        </div> 
                                                        <label class="control-label">ចំនួនសរុបជាលេខនៃប្រាក់កក់<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled deposit_fixed number-format" name="deposit_fixed" id="focusedInput" type="text" >
                                                        </div> 
                                                        <label class="control-label">ចំនួនសរុបជាអក្សរនៃប្រាក់កក់<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled deposit_fixed_word number-format" name="deposit_fixed_word" id="focusedInput" type="text" autocomplete="off">
                                                        </div>
                                                        <label class="control-label">ចំនួនសរុបជាលេខនៃប្រាក់ដែលនៅជំពាក់<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled money_owne number-format" name="money_owne" id="focusedInput" type="text" autocomplete="off">
                                                        </div> 
                                                        <label class="control-label">ចំនួនសរុបជាអក្សរនៃប្រាក់ដែលនៅជំពាក់<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <input class="span12 m-wrap disabled money_owne_word number-format" name="money_owne_word" id="focusedInput" type="text" autocomplete="off">
                                                        </div>    
                                                        <label class="control-label"> ជ្រើសរើសការអនុម័តឥណទាន<span class="required">*</span></label>
                                                        <div class="controls">
                                                            <label class="uniform">
                                                                <input class="uniform_on is_agree" name="is_agree" type="radio" id="optionsCheckbox" value="1" >
                                                                <span class="label label-warning">អនុម័ត</span>
                                                            </label>
                                                            <label class="uniform">
                                                                <input class="uniform_on not_yet_agree" name="is_agree" type="radio" id="optionsCheckbox" value="2">
                                                                <span class="label label-warning">មិនទាន់អនុម័ត</span>
                                                            </label>
                                                            <label class="uniform">
                                                                <input class="uniform_on not_agree" name="is_agree" type="radio" id="optionsCheckbox" value="3">
                                                                <span class="label label-warning">មិនអនុម័ត</span>
                                                            </label>
                                                        </div>
                                                        <label class="control-label">រយៈពេល<span class="required">*</span></label>
                                                        <div class="controls">
                                                                <input class="span12 m-wrap duration_pay_money number-format" name="duration_pay_money" id="focusedInput" type="text" value="0">
                                                        </div>

                                                       <label class="control-label">គិតជា <span class="required">*</span></label>
                                                            <div class="controls" >
                                                                <select name="duration_pay_money_type" class="span12 m-wrap duration_pay_money_type">
                                                                        <option value="none">ជ្រើសរើស </option>
                                                                        <option value="month">ខែ</option>
                                                                        <option value="2week">២ សប្តាហ៍</option>
                                                                        <option value="week">សប្តាហ៍</option>
																		<option value="day">ថ្ងៃ</option>
                                                                </select>
                                                            </div>
                                                           <label class="control-label">សុំសងនៅថ្ងៃទី<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <input type="text" name="date_for_payments" class="span12 m-wrap input-xlarge datepicker date_for_payments"  id="date01"/>
                                                            </div>
                                                            <label class="control-label">សងនៅ<span class="required">*</span></label>
                                                            <div class="controls" >
                                                                <select name="place_for_pay" class="span12 m-wrap type_of_fomula">
                                                                    <option value="1">សាខា</option>
                                                                    <option value="2">ភូមិ</option>
                                                                </select>
                                                            </div>
                                                            <label class="control-label">ជ្រើសរើសអត្រាការប្រាក់<span class="required">*</span></label>
                                                            <div class="controls" >
                                                                <select name="interest_of_owne_precent" class="span12 m-wrap interest_of_owne_precent">
                                                                    @foreach($interest as $it)
                                                                        @if(!empty($it->module))
                                                                        <option value="{{$it->module_interest_id}}">{{$it->module->display_name_kh}}</option>
                                                                        @endif
                                                                    @endforeach
                                                                    <option value="special">អត្រាការប្រាក់ពិសេស</option>  
                                                                </select>
                                                            </div>
                                                            <div class="controls inter_specail" style="display:none;">
                                                                <input type="text" autocomplete="off" name="interest_of_owne_precent_special" class="span12 m-wrap  interest_of_owne_precent_special"  id="date01"/>
                                                            </div>
                                                            <label class="control-label">ផលិតផលឥណទាន (រូបមន្ត)<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <select name="type_of_fomula" class="span12 m-wrap type_of_fomula">
                                                                        <option value="1">ការប្រាក់</option>
                                                                        <option value="2">សេវា និងការប្រាក់</option>                                                                        
                                                                </select>
                                                            </div>

                                                            <label class="control-label">បង់រំលស់ជា<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <select name="type_of_payment" class="span12 m-wrap type_of_payment">
                                                                        <option value="2">រំលស់ថេរ</option>   
                                                                        <option value="1">រំលស់ថយ</option>                                                                     
                                                                </select>
                                                            </div>

                                                            <label class="control-label">កាលបរិច្ឆេទបើកផលិតផល<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <input type="text" name="date_give_product" class="span12 m-wrap input-xlarge datepicker date_give_product" value="{{ old('date_give_product') }}" id="date01"/>
                                                            </div>
                                                            <label class="control-label">កាលបរិច្ឆេទអនុម័តឥណទាន<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <input type="text" name="date_approval" class="span12 m-wrap input-xlarge datepicker date_approval" value="{{ old('date_approval') }}" id="date01"/>
                                                            </div>

                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <legend>ហត្ថលេខាគណៈកម្មការឥណទាន</legend>
                                                        <div class="span4" style="margin-left:0;">
                                                            <label class="control-label">ប្រធាន<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <select name="manager_id1" class="span12 m-wrap manager_id1" >
                                                                    @foreach($userdata as $user)
                                                                        <option value="{{ $user->id }}">{{ $user->name_kh }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="span4">
                                                            <label class="control-label">សមាជិក<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <select name="manager_id2" class="span12 m-wrap manager_id2 " >
                                                                    @foreach($userdata as $user)
                                                                        <option value="{{ $user->id }}">{{ $user->name_kh }}</option>
                                                                    @endforeach
                                                                </select>
                                                            </div>
                                                        </div>

                                                        <div class="span4">
                                                            <label class="control-label">សមាជិក<span class="required">*</span></label>
                                                            <div class="controls">
                                                                <select name="manager_id3" class="span12 m-wrap manager_id3" >

                                                                    @foreach($userdata as $user)
                                                                    
                                                                        <option value="{{ $user->id }}">{{ $user->name_kh }}</option>

                                                                    @endforeach

                                                                </select>
                                                            </div>
                                                        </div>
                                                    </div>

                                                    <div class="span12" style="margin-left:0;">
                                                        <label class="control-label" >​ សេចក្ដីពិពណ៌នា:</label>
                                                        <div class="controls">
                                                            <textarea name="description"  data-required="1" rows="5" class="span12 m-wrap description"></textarea>
                                                        </div>
                                                        
                                                    </div>
                                                    <div class="span12" style="margin-left:0;">
                                                        <br/>
                                                            <center>
                                                                    <button type="submit" class="btn btn-success tbn_add" id="btn-save" name="btn_save" value="edit">Submit</button>
                                                                    <input type="hidden" id="item_id" name="data_id" value="{{$data_id}}">
                                                                    <button  class="btn btn-danger get_back">Back</button>
                                                            </center>  
                                                        <br/>
                                                    
                                                    </div>
                                            </div>
                                        </form>
			    			</div>
                        <!-- Edit Supplier -->

						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">
$(document).ready(function(){
    $(window).load(function(){
        get_sale_credit_sale();
    });
    $(".interest_of_owne_precent").change(function(){
        var inter_specail = $(this).val();
        if(inter_specail == "special"){
            $(".inter_specail").removeAttr('style');
        }else{
            $(".inter_specail").css('display','none');
        } 
    });
    function day_format_show(date_format){
                var d = new Date(date_format);
                var year_n = d.getFullYear();
                var month_n = d.getMonth() + 1;
                var day_n = d.getDate();
                if(month_n >= 10){
                    month_n = month_n;
                }else{
                    month_n = "0"+month_n; 
                }
                if(day_n >= 10){
                    day_n = day_n;
                }else{
                    day_n = "0"+day_n; 
                }
        return  day_n +"-"+month_n+"-"+year_n;
    }
        function get_sale_credit_sale(){
            var url_json =  "{{ url('aprove_credit_sales/'.$data_id.'/show_json') }}";
                    $.ajax({
                        type: "GET",
                        url: url_json, 
                        dataType: "json",
                        success: function(result){
                            console.log(result);
                            var text = "";
                            $.each(result.approval_item,function(i,da){
                                  if(da.item.categorys){
                                   var cat =  da.item.categorys.name;
                                    
                                }else{
                                  var cat =  "";  
                                }
                                text += "<tr> " ;
                                // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+cat+" </td>"
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                                text += "<input type='hidden' name='item_id[]' value='"+da.product_id+"'> ";
                                text += "<input type='hidden' name='sale_item_id[]' value='"+da.sell_item_id+"'>";
                                text += "<input type='hidden' name='qty[]' value='"+da.qty+"'>";
                                text += "<input type='hidden' name='discount[]' value='"+da.discount+"'>";
                                text += "<input type='hidden' name='other_price[]' value='"+da.other_price+"'>";
                                text += "<input type='hidden' name='total_tax[]' value='"+da.total_tax+"'>";
                                text += "<input type='hidden' name='total_price_payment[]' value='"+da.total_price_payment+"'>";
                                text += "<input type='hidden' name='total_price[]' value='"+da.total_price+"'>";
                                text += "<input type='hidden' name='sell_price[]' value='"+da.sell_price+"'>";
                                text += "<input type='hidden' name='cost_price[]' value='"+da.cost_price+"'>";
                                text += "<input type='hidden' name='commission[]' value='"+da.commission+"'>";

                            });

                            $(".item_list").html(text);
                            if(result.method == "sale_by_credit"){
                                var mothod = "បង់រំលស់";
                            }else{
                                var mothod = "បង់ជាសាច់ប្រាក់";   
                            }

                        if(result.date_for_payments !== null){  
                           var  dpm = result.date_for_payments;
                           var  date_for_payments = day_format_show(dpm);
                        }else{
                            var  date_for_payments = "";
                        }

                        if(result.date_give_product !== null){
                           var  date_give_product = day_format_show(result.date_give_product);
                        }else{
                            var  date_give_product = "";
                        }

                        if(result.date_approval !== null){
                           var  date_approval = day_format_show(result.date_approval); 
                        }else{
                            var  date_approval = "";
                        }
                           var dpmt = result.duration_pay_money_type;
                            if(dpmt == "month"){
                                dpmt = "ខែ";
                            }else if(dpmt == "2week"){
                                dpmt = "២ សប្តាហ៍";
                            }else if(dpmt == "week"){
                                dpmt = "សប្តាហ៍";
                            }else if(dpmt == "day"){
                                dpmt = "ថ្ងៃ";
                            }
                            if(result.is_agree == 1){
                                $(".is_agree").attr("checked","checked");
                                $(".is_agree").parent().addClass("checked"); 
                            }else if(result.is_agree == 3){
                                $(".not_agree").attr("checked","checked"); 
                                $(".not_agree").parent().addClass("checked");      
                            }else if(result.is_agree == 2){
                                $(".not_yet_agree").attr("checked","checked");
                                $(".not_yet_agree").parent().addClass("checked");     
                            }
                            $(".interest_of_owne_precent").find("[value='"+result.interest_type+"']").attr("selected","selected"); 
                            if(result.interest_type == 'special'){
                                $(".inter_specail").removeAttr('style');
                                $(".interest_of_owne_precent_special").val(result.interest_of_owne_precent*100);
                            }
                            $(".manager_id1").find("[value='"+result.manager_id1+"']").attr("selected","selected"); 
                            $(".manager_id2").find("[value='"+result.manager_id2+"']").attr("selected","selected"); 
                            $(".manager_id3").find("[value='"+result.manager_id3+"']").attr("selected","selected"); 
                            $(".comment_manager").val(result.comment_manager);
                            $(".description").val(result.description);  
                            $(".date_give_product").val(date_give_product); 
                            $(".date_approval").val(date_approval); 
                            $(".duration_pay_money").val(result.duration_pay_money); 
                            $(".duration_pay_money_type").find("[value='"+result.duration_pay_money_type+"']").attr("selected","selected");
                            $(".place_for_pay").find("[value='"+result.place_for_pay+"']").attr("selected","selected"); 
                            $(".type_of_fomula").find("[value='"+result.type_of_fomula+"']").attr("selected","selected");
                            $(".type_of_payment").find("[value='"+result.type_of_payment+"']").attr("selected","selected");    
                            $(".date_for_payments").val(date_for_payments );
                            $(".method").text(mothod);
                            $(".prices_total_num").val(accounting.formatMoney(result.prices_total_num));
                            $(".prices_totalword").val(result.prices_totalword);
                            $(".deposit_fixed").val(accounting.formatMoney(result.deposit_fixed));
                            $(".deposit_fixed_word").val(result.deposit_fixed_word);
                            $(".money_owne").val(accounting.formatMoney(result.money_owne)); 
                            $(".money_owne_word").val(result.money_owne_word); 
                            $(".duration_pay_money").val(result.duration_pay_money);     
                            $(".prices_total_num_text").text(accounting.formatMoney(result.prices_total_num));
                            $(".deposit_fixed_text").text(accounting.formatMoney(result.deposit_fixed));
                            $(".duration_pay_money_text").text(result.duration_pay_money);
                            $(".duration_pay_money_type_text").text(dpmt);
                            $(".date_for_payments_text").text(date_for_payments );
                            $(".money_owne_text").text(accounting.formatMoney(result.money_owne));    
                            
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
        }

        $('#btn-save').click(function(e){
                $.ajaxSetup({
                        headers: {
                            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                        }
                    }) 
                    e.preventDefault();
                    var btn_id = $(this).val();
                       var item_id = $('#item_id').val();
                       var url = "{{ url('aprove_credit_sales') }}";
                        var url_index = url+"/"+item_id+"/edit";
                  
                var comment_manager = $(".comment_manager").val();
                var duration_pay_money = $(".duration_pay_money").val(); 
                var duration_pay_money_type = $(".duration_pay_money_type").val();
                var date_for_payments = $(".date_for_payments").val();   
                var place_for_pay = $(".place_for_pay").val();   
                var type_of_fomula = $(".type_of_fomula").val();   
                var date_give_product = $(".date_give_product").val();   
                var date_approval = $(".date_approval").val();         
                
                if(comment_manager.length <= 0){
                    alert("​ សេចក្តីបញ្ជាក់របស់គណៈកម្មការឥណទាន Is required");
                    return false;
                } 
                if(duration_pay_money <= 0){
                    alert("​ រយៈពេលបង់ប្រាក់ Is required");
                    return false;
                }
                if(duration_pay_money_type == "none"){
                    alert("​ រយៈពេលបង់ប្រាក់គិតជា  Is required");
                    return false;
                } 
                if(date_for_payments.length <= 0){
                    alert("​ សុំសងនៅថ្ងៃទី  Is required");
                    return false;
                } 
                // if(date_give_product.length <= 0){
                //     alert("​ កាលបរិច្ឆេទបើកផលិតផល  Is required");
                //     return false;
                // }
                // if(date_approval.length <= 0){
                //     alert("​ កាលបរិច្ឆេទអនុម័តឥណទាន  Is required");
                //     return false;
                // } 
               
                    var form = document.forms.namedItem("form_insert_aprove_credit_sales"); 
                    var formData = new FormData(form); 
                 
                    $.ajax({
                        type:'POST',
                        url: url_index,
                        dataType: 'json',
                        contentType: false,
                        data: formData,
                        processData: false,
                        success: function(result){
                            console.log(result);

                                window.location="{{url('aprove_credit_sales')}}";   
                           
                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }                   
                    });
            }); 


            $(".deposit_precent").keyup(function(){
                var prices_total_num = accounting.unformat($(".prices_total_num").val());
                var depos = $(this).val();
                if(depos < 0 ){
                    depos = prices_total_num;
                }else{
                    depos = prices_total_num * (depos/100);
                }
                var total_depos = depos;
                $(".deposit_fixed").val(accounting.formatMoney(total_depos));
                var total_own = prices_total_num - depos;
                $(".money_owne").val(accounting.formatMoney(total_own));

            }); 

               
 });       
</script>            
@stop()
