@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
    <div class="row-fluid">
        <!-- validation -->
		<div class="row-fluid">
			<!-- block -->
			<div class="block">
				<div class="navbar navbar-inner block-header">
					<div class="muted pull-left"><a href="{!! url('penaty_date') !!}">ការកំណត់ពិន័យ</a> </div>
						<!-- <div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('penaty_date/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> New</a></div>
 -->                </div>
                <div class="block-content collapse in">
                	@if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif
					<form action="{{ url('penaty_date/create') }}" method="post" enctype="multipart/form-data">
						{{ csrf_field() }}
						<div class="span12" style="padding-left: 50px;">
							<label class="control-label">ឈ្មោះ<span class="required">*</span></label>
							<div class="controls">
								<input type="text" value="{{ old('title') }}" placeholder="ឈ្មោះ" name="title" data-required="1" class="span9 m-wrap"/>
							</div>
							
							<label class="control-label">ថ្ងៃចាប់ផ្តើម<span class="required">*</span></label>
								<div class="controls">
									<input type="number" value="{{ old('start_date') }}" placeholder="ថ្ងៃចាប់ផ្តើម" name="start_date" data-required="1" class="span9 m-wrap input-xlarge " id="start_date"/>
								</div>
							<label class="control-label">ថ្ងៃបញ្ចប់<span class="required">*</span></label>
								<div class="controls">
									<input type="number" value="{{ old('end_date') }}" name="end_date" placeholder="ថ្ងៃបញ្ចប់" data-required="1" class="span9 m-wrap input-xlarge " />
								</div>
							<label class="control-label">ចំនួនពិន័យជាភាគរយ (%)<span class="required">*</span></label>
							<div class="controls">
								<input type="text" value="{{ old('percent_of_payment') }}" name="percent_of_payment" placeholder="ចំនួនពិន័យជាភាគរយ"  class="span9 m-wrap input-xlarge " />
							</div>
							<div class="form-group">
			                    <label>ពណ័:</label>
			                    <div class="input-group my-colorpicker2">
			                    	<div class="input-group-addon" style="position: absolute;margin-top: 6px;">
			                        	<i></i>
			                      	</div>
			                      	<input type="text" name="color" value="{{ old('color') }}" class="span8 m-wrap" style="margin-left: 25px">
			                      
			                    </div><!-- /.input group -->
			                  </div><!-- /.form group -->
							<label class="control-label">ការកណត់សំគាល់</label>
								<div class="controls">
									<textarea name="discription" data-required="1" class="span9 m-wrap" placeholder="មតិយោបល់"></textarea>
								</div>
						</div>
						<div class="span9">
							<center>
								<button type="submit" class="btn btn-success">បញ្ចូល</button>
								<a class="btn btn-info" href="{{ URL::TO('penaty_date') }}">បោះបង់</a>
							</center>
						</div>
					</form>
                </div>
            </div>
        </div>
    </div>
</div>


@stop()