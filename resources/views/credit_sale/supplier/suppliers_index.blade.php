@extends('credit_sale.layout.master')
@section('contend')

<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <?php $items = DB::table('cs_suppliers')->where('deleted','=',0)->get();?>
                                    <input type="text" name="item_search" placeholder="Name" data-required="1" class="span3 m-wrap item_search" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source='[@foreach($items as $row)"{{$row->name}}",@endforeach""]' autocomplete="off">
                                    <button type="submit" class="btn btn-primary b_search" id="b_search" name="submit_search" style="margin-top: -5px;" value="b_search">ស្វែងរក</button>
											
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center>
			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រង់អ្នកផ្គត់ផ្គង់ផលិតផល</h3>

			                    	<div class="muted span3 pull-right" style="margin-bottom:5px;"><a href="{{ url('suppliers/create?redirect=create') }}" class="btn btn-success btn_edit pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
			                    </center>
                            	<legend></legend>
                           
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>

							            <th>ឈ្មេាះក្រុមហ៊ុន</th>

							            <th>ឈ្មេាះ</th>

							            <th>លេខទូរសព្ទ</th>

                                        <th>អ៊ីម៉ែល</th>

                                        <th>សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="item_list">
                                       
                                  </tbody>
                                    <tr>
                                        <td colspan="5">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right"></div>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />
<p id="model_in"></p>

<script>
$(document).ready(function(){
        var numpage = 1;
		var url_index1 = "{{route('suppliers_get_js')}}";
		get_page(url_index1,numpage);
            $(document).ajaxComplete(function(){    
                    $(".b_search").click(function(){
                        var submit_search = $(this).val();
                        var n = 1;
                        
                        var url_index2 = submit_search; 		
                        get_page(url_index2,numpage = n);
                    });
                    
                    $(".pag").click(function(){
                        var numpage = $(this).text();   
                        get_page(url_index1,numpage);
                    });

                    $(".pre").click(function(){
                        var numpage = $(this).find(".pre_in").val();
                        get_page(url_index1,numpage);
                    });

                    $(".btn_deleted").click(function(e){
                        
                        $.ajaxSetup({
                            headers: {
                                'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                            }
                        })
                            var item_id = $(this).val();
                            var url1 = "{{ route('suppliers') }}";
                            var url_index = url1+"/"+item_id+"/deleted";
                            e.preventDefault();
                            $.ajax({
                                type: "POST",
                                url: url_index, 
                                dataType: "json",
                                success: function(result){
                                    $('.msg_show').html(result.msg_show); 
                                            var numpage = 1 ;
                                            var url_index1 = "{{ route('suppliers_get_js') }}";
                                            get_page(url_index1,numpage);
                                },
                                error: function (result ,status, xhr) {
                                    console.log(result.responseText);
                                }                   
                            });
                    });

                });
        function get_page(url,n){
                $.ajaxSetup({
                    headers: {
                        'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                    }
                })
                var item_search = $(".item_search").val(); 
                //alert(item_search);
                if(url === "b_search" || item_search !== ""){
                    var url_i = "{{route('suppliers_get_js')}}";
                    var forData = {
                                    item_search: $(".item_search").val(),
                                    submit_search: $(".b_search").val()
                                }                
                    var url_index = url_i+"?item_search="+item_search+"&submit_search=b_search&page="+n;     
                }else{
                    var forData = {};
                    var url_index = url+"?page="+n;
                }
			    var client;
                    $.ajax({
                            type: "GET",
                            url: url_index, 
                            dataType: "json",
                            data: forData,
                            // async: false,
                            success: function(result){
                                console.log(result);
                                $("#total_all").text(result.total);
                                        $.each(result.data, function(i, field){
                                                var il = result.from  + i;  
                                                var url_edit = "{{url('suppliers')}}/"+field.id+"/edit"; 
                                                //var                                      
                                                client += "<tr>";
                                                    client += "<td>"+ il +"</td>";
                                                    client += "<td>"+ field.company_name +"</td>";
                                                    client += "<td>"+ field.name +"</td>";
                                                    client += "<td> "+ field.phone +"</td>";
                                                    client += "<td> "+ field.email +"</td>";
                                                    client += "<td>";
                                                    client += "<a href='"+url_edit+"' class='btn btn-primary btn_edit' id='btn_edit'>កែប្រែ</a>  ";
                                                    client += "<button  class='btn btn-danger btn_deleted'  value='"+ field.id +"'>លុប</button>";
                                                    client += "</td>";
                                                client += "</tr>";
                                        });
                                    $(".item_list").html(client);	
                                    var page = "";
                                    if(result.prev_page_url === null){
                                        var pr_url = result.current_page;
                                    }else{
                                        var pr_url = result.current_page -1;
                                    }
                                    page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
                                    for(var x = 1; x <= result.last_page; x ++  ) {
                                        if(result.current_page === x){
                                            page += "<a class='pag active' >"+x+"</a>";
                                        }else{
                                            page += "<a class='pag' >"+x+"</a>";
                                        }
                                    }
                                    if(result.next_page_url === null){
                                        var ne_url = result.current_page;
                                    }else{
                                        var ne_url = result.current_page +1;
                                    }
                                    page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
                                    $(".pagination").html(page );

                        },
                        error: function (result ,status, xhr) {
                            console.log(result.responseText);
                        }
                    
                    });
                   
		}    
        
 
 });       
</script>            
@stop()
