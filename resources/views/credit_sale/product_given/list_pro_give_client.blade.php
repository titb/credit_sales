@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
.table th, .table td{
    vertical-align: middle;
}
</style>
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('given_products/client') !!}"> ការបើកផលិតផលអោយអតិថិជន</a> </div>					
                            </div>
                            <div class="block-content collapse in">      
                                 <center>
			                        <h3 class="cen_title text-center khmer_Moul">ការបើកផលិតផលអោយអតិថិជន </h3>
			                    </center>
                            	<legend></legend>
                                <div class="span12" style="margin-left:0px;" >
                                            <div class="span3">
                                                <lable>&nbsp;</lable>
                                                <input type="text" placeholder="បញ្ចូលឈ្មោះអតិថិជន" class="span12 m-wrap search_client_id" autocomplete="off">
                                                <input type="hidden" name="search_client_hidden_id" class="search_client_hidden_id" >
                                            </div>
                                            <div class="span3">
                                                <lable>&nbsp;</lable>
                                                <input type="text"  placeholder="បញ្ចូលលេខកូដអតិថិជន"  class="span12 m-wrap search_schedule_id"  autocomplete="off">
                                                <input type="hidden" name="search_schedule_hidden_id" class="search_schedule_hidden_id" >
                                            </div>
                                            <div class="span2">
                                                <lable>&nbsp;</lable><br/>
                                                <button type="button" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search">ស្វែងរក</button>
                                            </div>
                                            <div class="muted span3 pull-right" style="margin-top: 18px;"><a href="{{ url('given_products/create?item_id=0&redirect=give_product_to_client') }}" class="btn btn-success btn_add pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
                                 </div>  
                            
                            @include('errors.error')	
                             	
                           
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>
							            <th>ឈ្មោះអតិថិជន</th>
                                        <th>លេខតារាង</th>
                                        <th>លេខបណ្ណ</th>
                                        <th>ផលិតផល</th>
                                        <th>ស្ថានភាព</th>
                                        <th>ផ្ដល់ដោយ</th>
                                        <th>ថ្ងៃអោយផលិតផលទៅអតិថិជន</th>
                                        <th style="width: 185px;">សកម្មភាព</th>

							        </tr>
							      </thead>
                                  
							      <tbody class="item_list">
                                @if(count($datas) > 0)
                                        @foreach($datas as $key => $da)
                                            <tr>    
                                                <td>{{$key+1}}</td>
                                                <td>{{$da->client->kh_username}}</td>
                                                <td>{{$da->schedule_id}}</td>
                                                <td>{{$da->num_give_to_client}}</td>
                                                <td>
                                                @foreach($da->schedule->cs_approvalcredit->approval_item as $it)
                                                    <p>@if($it->item){{$it->item->name}}@endif   &nbsp; &nbsp; &nbsp; X  &nbsp; &nbsp; &nbsp;  {{$it->qty}}</p>           
                                                @endforeach
                                                </td>
                                                <td>
                                                    @if($da->status == 1)
                                                        <p style='color:#008000;'>បានយក</p>
                                                    @elseif($da->status == 2)
                                                        <p style='color:#ff6800;'>មិនទាន់យកអោយ</p>
                                                    @elseif($da->status == 4)
                                                        <p style='color:#ff0000;'>មិនយក</p>
                                                    @elseif($da->status == 3)
                                                        <p style='color:#ff6800;'>មិនទាន់យក</p>
                                                    @endif
                                                </td>
                                                <td>
                                                @if($da->staff_give_id)
                                                    {{$da->staff_give->name_kh}}
                                                @endif
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y',strtotime($da->date_give_to_client))}}
                                                </td>
                                                <td>
                                              
                                                    <a href="{{ url('given_products/create?item_id='.$da->id.'&redirect=give_product_to_client') }}" class="btn btn-primary">កែប្រែ</a>
                                                    <a href="{{ url('given_products/show?item_id='.$da->id.'&redirect=give_product_to_client') }}" class="btn btn-info">លម្អិត</a>
                                                  
                                                    <button type="buttom"  class="btn btn-danger btn_delete" value="{{$da->id}}">លុប</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                   @endif
                                  </tbody>
                                    <!-- <tr>
                                        <td colspan="7">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr> -->
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">{{$datas->links()}}</div>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />




<script>

// Schedule Id 
$(".search_schedule_id").autocomplete({

    source : '{{ URL::route('schedule_search_all') }}',

    minlenght :1,

    autoFocuse : true,

    select:function(e,ui){
        $(".search_schedule_hidden_id").val(ui.item.id);
    }
});
// Client Id 
$(".search_client_id").autocomplete({

        source : '{{ URL::route('search_Client_ajax') }}',

        minlenght :1,

        autoFocuse : true,

        select:function(e,ui){
            $(".search_client_hidden_id").val(ui.item.id);
        }
});

$(".btn_delete").click(function(){
    if (confirm("Are you sure you wish to delete  this Row?")) {
        var item_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var url_now = "{{url('delete_data_give_to_client')}}/"+item_id;
                $.ajax({
                    
                    type: "POST",
                    url: url_now,
                    dataType: 'json',
                    // async: false,
                    //data: formData1,
                    success: function(result){
                            location.href= "{{url('given_products/client')}}";
                    }

                });
    }else{
        return false;
    }
});

// $(document).ready(function(){
         
//  });   
 
</script>            
@stop()
