@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<style>
.table th, .table td{
    vertical-align: middle;
}
</style>
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{!! url('given_products') !!}"> ការបើកផលិតផលអោយបុគ្គលិក</a></div>					
                            </div>
                            <div class="block-content collapse in">      
                                 <center>
			                        <h3 class="cen_title text-center khmer_Moul">ការបើកផលិតផលអោយបុគ្គលិក </h3>
			                    </center>
                            	<legend></legend>
                                <div class="span12" style="margin-left:0px;" >
                                            <div class="span3">
                                                <lable>&nbsp;</lable>
                                                <input type="text" placeholder="បញ្ចូលឈ្មោះអតិថិជន" class="span12 m-wrap search_client_id" autocomplete="off">
                                                <input type="hidden" name="search_client_hidden_id" class="search_client_hidden_id" >
                                            </div>
                                            <div class="span3">
                                                <lable>&nbsp;</lable>
                                                <input type="text"  placeholder="បញ្ចូលលេខកូដអតិថិជន"  class="span12 m-wrap search_schedule_id"  autocomplete="off">
                                                <input type="hidden" name="search_schedule_hidden_id" class="search_schedule_hidden_id" >
                                            </div>
                                            <div class="span2">
                                                <lable>&nbsp;</lable><br/>
                                                <button type="button" class="btn btn-primary b_search" id="b_search" name="submit_search" value="b_search">ស្វែងរក</button>
                                            </div>
                                            <div class="muted span3 pull-right" style="margin-top: 18px;"><a href="{{ url('given_products/create?item_id=0&redirect=give_product_to_co') }}" class="btn btn-success btn_add pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>
                                 </div>  
                            
                            @include('errors.error')	
                             	
                           
							    <table class="table table-bordered">

							      <thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>ល.រ</th>
							            <th>ឈ្មោះអតិថិជន</th>
                                        <th>លេខតារាង</th>
                                        <th>ផលិតផល</th>
                                        <th>ស្ថានភាព</th>
                                        <th>ផ្ដល់ដោយ</th>
                                        <th>ថ្ងៃអោយផលិតផលទៅមន្ត្រីឥណទាន</th>
                                        <th style="width: 185px;">សកម្មភាព</th>

							        </tr>
							      </thead>
							      <tbody class="item_list">
                                        @foreach($datas as $key => $da)
                                            <tr>    
                                                <td>{{$key+1}}</td>
                                                <td>{{$da->client->kh_username}}</td>
                                                <td>{{$da->schedule_id}}</td>
                                                <td>
                                                @foreach($da->schedule->cs_approvalcredit->approval_item as $it)

                                                <p>@if($it->item){{$it->item->name}}@endif   &nbsp; &nbsp; &nbsp; X  &nbsp; &nbsp; &nbsp;  {{$it->qty}}</p>     
                                                          
                                                @endforeach
                                                </td>
                                                <td>
                                                     @if($da->status == 1)
                                                        <p style='color:#008000;'>បានយក</p>
                                                    @elseif($da->status == 2)
                                                        <p style='color:#ff6800;'>មិនទាន់យកអោយ</p>
                                                    @elseif($da->status == 3)
                                                        <p style='color:#ff0000;'>មិនយក</p>
                                                    @endif
                                                </td>
                                                <td>
                                                    {{$da->staff->name_kh}}
                                                </td>
                                                <td>
                                                    {{ date('d-m-Y',strtotime($da->date_give))}}
                                                </td>
                                                <td>
                                                    @if($da->status == 2)
                                                        <button type="buttom"  class="btn btn-warning generate_schedule" value="{{$da->schedule_id}}" style="margin: 5px 0 5px 0;">បង្កើតកាលវិភាគម្តងទៀត</button>
                                                    @endif
                                                    <a href="{{ url('given_products/create?item_id='.$da->id.'&redirect=give_product_to_co') }}" class="btn btn-primary">កែប្រែ</a>
                                                    <a href="{{ url('given_products/show?item_id='.$da->id.'&redirect=give_product_to_co') }}" class="btn btn-info">លម្អិត</a>
                                                    <button type="buttom"  class="btn btn-danger btn_delete" value="{{$da->id}}">លុប</button>
                                                </td>
                                            </tr>
                                        @endforeach
                                  </tbody>
                                    <tr>
                                        <td colspan="7">
                                            <b class="pull-right">សរុប:</b>
                                        </td>
                                        <td>
                                            <b id="total_all"></b>
                                        </td>
                                    </tr>
							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">{{$datas->links()}}</div>
			    			</div>
						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />




<script>

// Schedule Id 
$(".search_schedule_id").autocomplete({

    source : '{{ URL::route('schedule_search_all') }}',

    minlenght :1,

    autoFocuse : true,

    select:function(e,ui){
        $(".search_schedule_hidden_id").val(ui.item.id);
    }
});
// Client Id 
$(".search_client_id").autocomplete({

        source : '{{ URL::route('search_Client_ajax') }}',

        minlenght :1,

        autoFocuse : true,

        select:function(e,ui){
            $(".search_client_hidden_id").val(ui.item.id);
        }
});

$(".btn_delete").click(function(){
    if (confirm("Are you sure you wish to delete this Row?")) {
        var item_id = $(this).val();
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        var url_now = "{{url('deleted_from_database_give_product_to_co')}}/"+item_id;
                $.ajax({
                    
                    type: "POST",
                    url: url_now,
                    dataType: 'json',
                    // async: false,
                    //data: formData1,
                    success: function(result){
                            location.href= "{{url('given_products')}}";
                    }

                });
    }else{
        return false;
    }
});

$(".generate_schedule").one("click", function(e){ 
        var sche_id = $(this).val();
            $.ajaxSetup({
                headers: {
                    'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
                }
            });
            e.preventDefault();
            var url_sc_gen = "{{ url('aprove_credit_sales') }}/"+sche_id+"/generate_credit_sale"  ;    
            $.ajax({
                type: "POST",
                url: url_sc_gen, 
                dataType: "json",
                success: function(data){
                    console.log(data);
                    $('.msg_show').html(data.msg_show); 
                    window.location="{{ url('aprove_credit_sales') }}/"+sche_id+"/generate_credit_sale";        
                },
                error: function (data ,status, xhr) {
                    console.log(data.responseText);
                }                   
            });
    });        
</script>            
@stop()
