@extends('credit_sale.layout.master')
@section('contend')
@include('credit_sale.autocompleted_script')
<div class="container-fluid">
            <div class="row-fluid">
                     <!-- validation -->
                    <div class="row-fluid">
                         <!-- block -->
                        <div class="block">
                            <div class="navbar navbar-inner block-header">
                                    <div class="muted pull-left"><a href="{{ url('given_products/create?item_id=0&redirect=give_product_to_co') }}"> ទម្រង់បើកប្រាក់ទៅបុគ្គលិក</a></div>					
                            </div>

                            <div class="block-content collapse in">
                            @include('errors.error')	
                             	<center> 
			                        <h3 class="cen_title text-center khmer_Moul">{{$title}}</h3>
			                    </center>
                            	<!-- <legend></legend> -->
                                <form role="form" id="form_insert_give_product" name="form_insert_give_product" method="POST"  enctype="multipart/form-data">
                                        {{ csrf_field() }}  
                                            <div class="span12"  style="margin: 0 auto !important;float: none;">
                                                <div class="controls">
                                                    <div class="span6">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ យោងតាមកិច្ចសន្យាលេខ <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <input type="text" id="search_item" class="span12 m-wrap search_item" name="schedule_id" data-required="1" autocomplete="off"/>
                                                        </div>
                                                    </div>   
                                                    <div class="span6">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ កាលបរិច្ឆេទធ្វើកិច្ចសន្យា <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <input type="text" id="date" class="span12 m-wrap date_approval" name="date_approval" data-required="1" autocomplete="off" readonly/>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="controls item_open_list">
                                                    <div class="span12" style="margin-left:0; margin-bottom: 20px;margin-top: 11px;    padding-bottom: 10px;">
                                                        <table class="table table-bordered">
                                                            <thead>
                                                                <tr>
                                                                    <th>ឈ្មោះទំនិញ</th>
                                                                    <th>លេខ​កូដ​</th>
                                                                    <th>ម៉ាក</th>
                                                                    <th>ចំនួន</th>
                                                                    <th>តម្លៃ​ឯកតា</th>
                                                                    <th>តម្លៃសរុប</th>
                                                                </tr>
                                                            </thead>
                                                            <tbody class="item_list">

                                                            </tbody>
                                                        </table>    
                                                    </div>
                                                </div> 
                                                <div class="controls">
                                                    <div class="span6"> 
                                                        <div class="span9">
                                                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ចំនួនទឹកប្រាក់សរុបជាលេខ <span style="color: red;">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text" id="prices_total_num" class="span12 m-wrap number_amount prices_total_num"  data-required="1" autocomplete="off" readonly/>
                                                            </div>
                                                        </div>
                                                        <div class="span3" style="margin-left: 0.12766%; margin-top: 25px;">
                                                            <div class="controls">
                                                            <input type="hidden" id="client_id" class="client_id" name="client_id">
                                                                <input type="hidden" id="currency_id" class="currency_id" name="currency_id">
                                                                <input type="text" id="currency" class="span12 m-wrap currency" data-required="1" autocomplete="off" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>   
                                                    <div class="span6">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ចំនួនទឹកប្រាក់សរុបជាអក្សរ <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <input type="text" id="prices_totalword" class="span12 m-wrap prices_totalword" data-required="1" autocomplete="off" readonly/>
                                                        </div>
                                                    </div>
                                                </div>  
                                                <div class="controls">
                                                    <div class="span6">
                                                        <div class="span12">
                                                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ចំនួនទឹកប្រាក់ដែលបានកក់សរុបជាលេខ <span style="color: red;">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text" id="deposit_fixed" class="span12 m-wrap number_amount deposit_fixed"  data-required="1" autocomplete="off" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>   
                                                    <div class="span6">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ចំនួនទឹកប្រាក់ដែលបានកក់សរុបជាអក្សរ <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <input type="text" id="deposit_fixed_word" class="span12 m-wrap deposit_fixed_word" data-required="1" autocomplete="off" readonly/>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="controls">
                                                    <div class="span6">
                                                        <div class="span12">
                                                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ចំនួនទឹកប្រាក់ដែលនៅជំពាក់សរុបជាលេខ <span style="color: red;">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text" id="money_owne" class="span12 m-wrap number_amount money_owne"  data-required="1" autocomplete="off" readonly/>
                                                            </div>
                                                        </div>
                                                    </div>   
                                                    <div class="span6">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ចំនួនទឹកប្រាក់ដែលនៅជំពាក់សរុបជាអក្សរ <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <input type="text" id="money_owne_word" class="span12 m-wrap money_owne_word"  data-required="1" autocomplete="off" readonly/>
                                                        </div>
                                                    </div>
                                                </div> 
                                                <div class="controls">
                                                    <div class="span12" style="margin-left:0;">
                                                        <div class="span6">
                                                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ថ្ងៃខែបើកផលិតផល <span style="color: red;">*</span> </label>
                                                            <div class="controls">
                                                                <input type="text" name="date_give" class="span12 m-wrap input-xlarge datepicker date_give"  id="date01" autocomplete="off"/>
                                                            </div>
                                                        </div> 
                                                        <div class="span6">
                                                            <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ស្ថានភាពបើកប្រាក់ <span style="color: red;">*</span> </label>
                                                            <select class="span12 m-wrap status" name="status" id="status">
                                                                <option value="2">មិនទាន់យក</option>
                                                                <option value="3">មិនយក</option>    
                                                            </select>
                                                        </div> 
                                                    </div>
                                                </div>
                                                <div class="controls">
                                                    <div class="span4">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ត្រួតពិនិត្យដោយ<br/> មន្រ្តីហិរញ្ញវត្ថុ/ប្រធានសាខា <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <select class="span12 m-wrap approve_by" name="approve_by" id="select01">
                                                                <option value="">ជ្រើសរើស</option>
                                                                    @foreach($user as $us)
                                                                        <option value="{{$us->id}}">{{$us->name_kh}}</option>
                                                                    @endforeach
                                                            </select>
                                                        </div>
                                                    </div>   
                                                    <div class="span4">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ទទួលផលិតដោយ <br/> បុគ្គលិក/មន្រ្តីឥទាន <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <select class="span12 m-wrap staff_id" name="staff_id" id="select02">
                                                                <option value="">ជ្រើសរើស</option>
                                                                    @foreach($user as $us)
                                                                        <option value="{{$us->id}}">{{$us->name_kh}}</option>
                                                                    @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div class="span4">
                                                        <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ ប្រគល់ផលិតដោយ <br/> គណនេយ្យករ/បេឡាករ <span style="color: red;">*</span> </label>
                                                        <div class="controls">
                                                            <select class="span12 m-wrap give_by" name="give_by" id="select03">
                                                           
                                                                <option value="">ជ្រើសរើស</option>
                                                                    @foreach($user as $us)
                                                                        <option value="{{$us->id}}" <?php if($us->id == $user_login_id){ echo "selected"; } ?> >{{$us->name_kh}}</option>
                                                                    @endforeach
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="controls">
                                                    <label class="control-label" title="ចាំបាច់ត្រូវតែមាន">​ បរិយាយ <span style="color: red;">*</span> </label>
                                                    <textarea name="comment"  data-required="1" rows="5" class="span12 m-wrap comment"></textarea>
                                                </div>
                                                <div class="span12" style="margin-left:0;">
                                                    <br/>
                                                        <center>
                                                            <button type="button" class="btn btn-success btn-save" id="btn-save"  value="{{$item_id}}">បញ្ចូល</button>
                                                        </center>  
                                                    <br/>
                                                
                                                </div>
                                            </div>
                                        </form>
			    			</div>
                        <!-- Edit Supplier -->

						</div>
                     	<!-- /block -->
		    		</div>
                     <!-- /validation -->
                </div>

            </div>

<meta name="_token" content="{{ csrf_token() }}" />

<script type="text/javascript">

// Schedule Id 
$("#search_item").autocomplete({

    source : '{{ URL::route('search_schedule_ajax_search_co') }}',

    minlenght :1,

    autoFocuse : true,
    
    select:function(e,ui){
        get_data_schedule(ui.item.id);
    }
});
//show data when select to item
function get_data_schedule(schedule_id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var url_now = "{{url('get_data_function')}}/"+schedule_id;
            $.ajax({
                
                type: "POST",
                url: url_now,
                dataType: 'json',
                // async: false,
                //data: formData1,
                success: function(result){
                    console.log(result);
                    if(result.cs_approvalcredit.date_approval){
                        $(".date_approval").val(day_format_show(result.cs_approvalcredit.date_approval));
                    }
                    if(result.cs_approvalcredit.prices_total_num){
                        $(".prices_total_num").val(accounting.formatMoney(result.cs_approvalcredit.prices_total_num));
                    }
                    if(result.cs_approvalcredit.prices_totalword){
                        $(".prices_totalword").val(result.cs_approvalcredit.prices_totalword);
                    }

                    $(".money_owne").val(accounting.formatMoney(result.cs_approvalcredit.money_owne));
                    $(".money_owne_word").val(result.cs_approvalcredit.money_owne_word);

                    $(".deposit_fixed").val(accounting.formatMoney(result.cs_approvalcredit.deposit_fixed));
                    $(".deposit_fixed_word").val(result.cs_approvalcredit.deposit_fixed_word);
                    $(".client_id").val(result.client_id);
                    if(result.currency){
                        $(".currency").val(result.currency.name);
                        $(".currency_id").val(result.currency.id);
                       
                    }
                    if(result.cs_approvalcredit.approval_item){
                        var text = "";
                            $.each(result.cs_approvalcredit.approval_item,function(i,da){
                                text += "<tr> " ;
                                // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+(da.item.categorys!==null?da.item.categorys.name:'')+" </td>"
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                               
                            });

                            $(".item_list").html(text); 
                    }
                    
                },
                error: function(){} 	          
            });
}

var item_id = "{{$item_id}}";
if(item_id != 0){
    show_data(item_id);
}
function show_data(item_id){
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var url_now = "{{url('show_give_product_to_co')}}/"+item_id;
            $.ajax({
                
                type: "POST",
                url: url_now,
                dataType: 'json',
                // async: false,
                //data: formData1,
                success: function(result){
                    console.log(result);
                    if(result.schedule.cs_approvalcredit.date_approval){
                        $(".search_item").val(result.schedule_id);
                        $(".search_item").attr('readonly','readonly')
                    }
                    if(result.schedule.cs_approvalcredit.date_approval){
                        $(".date_approval").val(day_format_show(result.schedule.cs_approvalcredit.date_approval));
                    }
                    if(result.schedule.cs_approvalcredit.prices_total_num){
                        $(".prices_total_num").val(accounting.formatMoney(result.schedule.cs_approvalcredit.prices_total_num));
                    }
                    if(result.schedule.cs_approvalcredit.prices_totalword){
                        $(".prices_totalword").val(result.schedule.cs_approvalcredit.prices_totalword);
                    }
                    
                    if(result.schedule.cs_approvalcredit.date_give_product){
                        $(".date_give").val(day_format_show(result.schedule.cs_approvalcredit.date_give_product));
                    }

                    $(".money_owne").val(accounting.formatMoney(result.schedule.cs_approvalcredit.money_owne));
                    $(".money_owne_word").val(result.schedule.cs_approvalcredit.money_owne_word);

                    $(".deposit_fixed").val(accounting.formatMoney(result.schedule.cs_approvalcredit.deposit_fixed));
                    $(".deposit_fixed_word").val(result.schedule.cs_approvalcredit.deposit_fixed_word);
                    $(".client_id").val(result.client_id);
                    if(result.schedule.currency){
                        $(".currency").val(result.schedule.currency.name);
                        $(".currency_id").val(result.schedule.currency.id);
                       
                    }
                    if(result.approve_by.id){
                        $(".approve_by").find("[value='"+result.approve_by.id+"']").attr("selected","selected"); 
                    }
                    if(result.staff.id){
                        $(".staff_id").find("[value='"+result.staff.id+"']").attr("selected","selected"); 
                    }
                    if(result.give_by.id){
                        $(".give_by").find("[value='"+result.give_by.id+"']").attr("selected","selected"); 
                    }
                    if(result.status){
                        $(".status").find("[value='"+result.status+"']").attr("selected","selected"); 
                    }
                    if(result.comment){
                        $(".comment").val(result.comment);
                    }

                    if(result.schedule.cs_approvalcredit.approval_item){
                        var text = "";
                            $.each(result.schedule.cs_approvalcredit.approval_item,function(i,da){
                                text += "<tr> " ;
                                // text += "<td>  <button class='btn btn-danger btn-mini remove_item' value='"+da.product_id+"'><i class='icon-remove icon-white ' style='padding-right: 0px;'></i></button>   "+da.item.name+" </td>";
                                text += "<td> "+da.item.name+" </td>";
                                text +=  "<td> "+da.item.item_bacode+"  </td>";
                                text +=  "<td> "+da.item.categorys.name+" </td>";
                                text +=  "<td> "+da.qty+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.sell_price)+" </td>";
                                text +=  "<td>  "+accounting.formatMoney(da.total_price_payment)+" </td>";
                                text += "</tr> "; 
                               
                            });

                            $(".item_list").html(text); 
                    }
                    
                },
                error: function(){} 	          
            });

}
// insert and update  

$("#btn-save").click(function(){
    var url = "{!! url()->full() !!}";
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="_token"]').attr('content')
        }

    });
    var item_id = $(this).val();
        var form = document.forms.namedItem("form_insert_give_product");
        var formDatas = new FormData(form);   
        $.ajax({
            type:'POST', 
            url: url,
            dataType: 'json',
            contentType: false,
            data: formDatas, 
            processData: false,
            success: function (result) {
                console.log(result);
                    location.href= "{{url('given_products')}}";
            },
            error: function (result) {
                console.log('Error:', result);
            }
        });


});

// $(document).ajaxComplete(function(){
//     $(".search_item").click(function(){
//             $.ajax({
//                 type: "GET",
//                 url: url_index,
//                 dataType: "json",
//                 data: forData,
//                 success: function(result ){
//                     console.log(result);

//                 },
//             });
//     });
// });




</script>            
@stop()
