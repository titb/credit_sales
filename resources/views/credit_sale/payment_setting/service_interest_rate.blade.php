@extends('master.master')

@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

               

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                            	<div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('new_interest_rate') }}" class="btn btn-danger pull-right">Back</a></div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                            <div class="span12">

                            	
                            <center>
                            	<h3 class="cen_title khmer_Moul"> អត្រាសេវាផ្សេងៗ </h3>
                            	<legend></legend>
                            </center>

                            </div>

                      <style type="text/css">

                      	.table th, .table td {

						    padding: 8px;

						    line-height: 20px;

						    text-align: center;

						    vertical-align: middle !important;

						    border-top: 1px solid #ddd;

						}

                      </style>

                      <?php $i = 6; ?>

                        @foreach($groups as $key => $interest)

                        	@if($key == "table_1")

                        		<legend>តារាងទី ១៖ កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល</legend>

                        	@elseif($key == "table_2")

                        	<br/>

                        		<legend>តារាងទី ២៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល</legend>

                        	@elseif($key == "table_3")

                        		<legend>តារាងទី ៣៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល</legend>

                        	@elseif($key == "table_4")

                        		<legend>តារាងទី ៤៖ កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ</legend>

                        	@elseif($key == "table_5")

                        		<legend>តារាងទី ៥៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ</legend>

                        	@elseif($key == "table_6")

                        		<legend>តារាងទី ៦៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ</legend>

                        	@elseif($key == "table_7")

                        		<legend>តារាងទី ៧៖ កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល</legend>

                        	@elseif($key == "table_8")

                        		<legend>តារាងទី ៨៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល</legend>

                        	@elseif($key == "table_9")

                        		<legend>តារាងទី ៩៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល</legend>

                        	@elseif($key == "table_10")

                        		<legend>តារាងទី ១០៖ កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ</legend>

                        	@elseif($key == "table_11")

                        		<legend>តារាងទី ១១៖ កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ</legend>

                        	@elseif($key == "table_12")

                        		<legend>តារាងទី ១២៖ កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ</legend>

                        	@endif



                            	<table class="table table-bordered" style="text-align:center">

                            	     <tbody>

						                <tr>

						                  <th rowspan="2">ប្រភេទកម្ចី</th>

						                  <th rowspan="4">ទំហំកម្ចី(រៀល)</th>

						                  <th colspan="8">អត្រាការប្រាក់ប្រចាំ</th>

						                </tr>

						                <tr>

						                	

						                	<td colspan="2">ថ្ងៃ</td>

						                	<td colspan="2">សប្តាហ៍</td>

						                	<td colspan="2">២សប្តាហ៍</td>

						                	<td colspan="2">ខែ</td>

						                </tr>

						               

						            

						         

						               <tr>



						                @if($key == "table_1")

						                	<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីប្រើប្រាស់សម្រាប់ជំនួញ និងផលិតកម្មផ្សេងៗ</td>

						               	@elseif($key == "table_2")

						                	<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល</td>

						               	@elseif($key == "table_3")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល</td>	

						               	@elseif($key == "table_4")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ</td>

						               	@elseif($key == "table_5")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ</td>

						               	@elseif($key == "table_6")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ</td>

						               	@elseif($key == "table_7")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់ការប្រើប្រាស់ផ្សេងៗ ជាប្រាក់រៀល កម្ចីក្រុមធានា</td>

										@elseif($key == "table_8")

											<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់រៀល កម្ចីក្រុមធានា</td>

						               	@elseif($key == "table_9")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់រៀល កម្ចីក្រុមធានា</td>

						               	@elseif($key == "table_10")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីប្រើប្រាស់ផ្សេងៗ ជាប្រាក់ដុល្លារ កម្ចីក្រុមធានា</td>

						               	@elseif($key == "table_11")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់កសិកម្ម ជាប្រាក់ដុល្លារ កម្ចីក្រុមធានា</td>

						               	@elseif($key == "table_12")

						               		<td rowspan="<?php echo $i+count($key); ?>" width="100">កម្ចីសម្រាប់ពាណិជ្ជកម្ម សេវាកម្ម ឬផលិតកម្មផ្សេងៗ ជាប្រាក់ដុល្លារ កម្ចីក្រុមធានា</td>

						               	@endif

						                	<td colspan="8">សងនៅ</td>

						                </tr>

						                <tr>

						                	<td>ភូមិ</td>

						                	<td>សាខា</td>

						                	<td>ភូមិ</td>

						                	<td>សាខា</td>

						                	<td>ភូមិ</td>

						                	<td>សាខា</td>

						                	<td>ភូមិ</td>

						                	<td>សាខា</td>

						                	

						                </tr>



						        @foreach($interest as $ky => $d ) 

						               <tr>

						               @if($ky == 0)

						               	<td >{{ $d->size_money_from }} ≤ {{ $d->size_money_to }} </td>

						               	@else

						               	<td > > {{ $d->size_money_from }} ≤ {{ $d->size_money_to }} </td>

						               	@endif

						               	<td>
						               	@if($d->day_rate_villige == 0)
						               		<?php echo round($d->day_rate_villige,2); ?> (%)
						               	@else
						               		<?php echo round($d->day_rate_villige-(($data1->villige)/30 ),2); ?> (%)
						               	@endif
						               	</td>

						               	<td >
						               	@if($d->day_rate_brand == 0)
						               		{{ round($d->day_rate_brand,2) }} (%)
						               	@else
						               		{{ round($d->day_rate_brand-(($data1->branch)/30),2) }} (%)
						               	@endif

						               	</td>

						               	<td >
						               	@if($d->weekly_rate_villige == 0)
						               		{{ round($d->weekly_rate_villige,2) }} (%)
						               	@else
						               		{{ round($d->weekly_rate_villige-(($data1->villige)/4),2) }} (%)
						               	@endif

						               	</td>

						               	<td >
						               	@if($d->weekly_rate_brand == 0)
						               		{{ round($d->weekly_rate_brand,2) }} (%) 
						               	@else
						               		{{ round($d->weekly_rate_brand-(($data1->branch)/4),2) }} (%) 
						               	@endif

						               	</td>

						               	<td >
						               	@if($d->two_weekly_rate_villige == 0)
						               		{{ round($d->two_weekly_rate_villige,2) }} (%)
						               	@else
						               		{{ round($d->two_weekly_rate_villige-(($data1->villige)/2),2) }} (%)
						               	@endif 

						               	</td>

						               	<td >
						               	@if($d->two_weekly_brand == 0)
						               		{{ round($d->two_weekly_brand,2) }} (%)
						               	@else
						               		{{ round($d->two_weekly_brand-(($data1->branch)/2),2) }} (%)
						               	@endif

						               	</td>

						               	<td >
						               	@if($d->monthly_rate_villige == 0)
						               		{{ round($d->monthly_rate_villige,2) }} (%)
						               	@else
						               		{{ round($d->monthly_rate_villige-($data1->villige),2) }} (%)
						               	@endif

						               	</td>

						               	<td >
						               	@if($d->monthly_rate_brand == 0)
						               		{{ round($d->monthly_rate_brand,2) }} (%)
						               	@else
						               		{{ round($d->monthly_rate_brand-($data1->branch),2) }} (%)
						               	@endif

						               	</td>

						               </tr>



						        @endforeach  

						        		<tr>

						        			<td colspan="11"> {{ $interest->first()->repayment_disction }}</td>

						        		</tr>      

						              </tbody>

						            </table>

						@endforeach

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	



	

@stop()