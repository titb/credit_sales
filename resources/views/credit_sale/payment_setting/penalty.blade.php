@extends('credit_sale.layout.master')
@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

               

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('public_holiday') !!}">ការកណត់ថ្ងៃឈប់សំរាក</a> </div>

                            	<div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('public_holiday/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> New</a></div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                            <div class="span12">

                            	

                             	<h3 class="cen_title"> ការកណត់ថ្ងៃឈប់សំរាក</h3>

                            	<legend></legend>

                            </div>

                            	<table class="table table-bordered" style="text-align:center">

                            	     <thead>

                            	     	<tr>

                            	     		<th>#</th>

                            	     		<th>ឈ្មោះ</th>

                            	     		<th>រយៈពេល</th>

                            	     		<th>ថ្ងៃចាប់ផ្តើម</th>

                            	     		<th>ថ្ងៃបញ្ចប់</th>

                            	     		<th>សកម្មភាព</th>

                            	     	</tr>

                            	     </thead>

                            	     <tbody>

                            	@foreach($data as $key=>$da)

						                <tr>

						                  <td>{{ $key+1 }} </td>

						                  <td>{{ $da->title }}</td>

						                  <td>{{ $da->duration }}</td>

						                  <td><?php  echo date('l',strtotime($da->start_date))."&nbsp;&nbsp;".date('d-m-Y',strtotime($da->start_date)) ?></td>

						                  <td>

						                 	 <?php  echo date('l',strtotime($da->end_date))."&nbsp;&nbsp;".date('d-m-Y',strtotime($da->end_date)) ?>

						                  </td>

						                  <td width="140px"><a href="{{ url('public_holiday/'.$da->id.'/edit') }}" class="btn btn-info">Edit</a> &nbsp;&nbsp;<a href="{{ url('public_holiday/'.$da->id.'/deleted') }}" class="btn btn-danger">Delete</a> </td>

						                </tr>

						        @endforeach       

						              </tbody>

						            </table>

							{{  $data->links() }}

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	


      
@stop()