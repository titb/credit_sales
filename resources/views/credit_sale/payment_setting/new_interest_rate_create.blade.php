@extends('credit_sale.layout.master')

@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

                		

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">
                                <div class="muted pull-left"><a href="{!! url('new_interest_rate') !!}">ការកំណត់អត្រាការប្រាក់ថ្មី</a> <span class="divider">/</span> បង្កើតអ្នកប្រើប្រាស់ថ្មី</div>
                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="span12">

                            		<h3 class="cen_title"> បង្កើតការកំណត់អត្រាការប្រាក់ថ្មី </h3>

                            	<legend></legend>

                            </div>

                            	<form action="{{ url('new_interest_rate/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">

	             

										  <div class="control-group">

										  	<label class="control-label">របៀបសង</label>
			  								<div class="controls">
			  									<select name="type_dura" class="span9 m-wrap">
												  <option value="1">ខែ</option>
												</select>
			  								</div>

			  								<label class="control-label">អត្រាការប្រាក់ ភូមិ<span class="required">*</span></label>
			  								<div class="controls">
			  									<input type="text" name="villige" data-required="1" class="span9 m-wrap"/>
			  								</div>

			  								<label class="control-label">អត្រាការប្រាក់ សាខា<span class="required">*</span></label>
			  								<div class="controls">
			  									<input type="text" name="branch" data-required="1" class="span9 m-wrap"/>
			  								</div>

	  									  </div>
									</div>



									<div class="span12">

										<center>

											<button type="submit" class="btn btn-success">Submit</button>

											<a href="{{ url('new_interest_rate') }}" class="btn btn-danger">Back</a>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	





	

@stop()