@extends('credit_sale.layout.master')
@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

                		

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <div class="muted pull-left"><a href="{!! url('public_holiday') !!}">ការកំណត់ថ្ងៃឈប់សម្រាក</a> <span class="divider">/</span>ថ្ងៃឈប់សម្រាក</div>

                            	<!--<div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('public_holiday/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> New</a></div>-->

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                     @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

                            <div class="span12">

                            		<h3 class="cen_title"> បង្កើតថ្ងៃឈប់សម្រាក</h3>

                            	<legend></legend>

                            </div>

                            	<form action="{{ url('public_holiday/create') }}" method="post" enctype="multipart/form-data">

	                                {{ csrf_field() }}

	                                <div class="span12" style="padding-left: 50px;">

											<label class="control-label">ឈ្មោះ<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" placeholder="ឈ្មោះ" name="title" data-required="1" class="span9 m-wrap"/>

			  								</div>

											<label class="control-label">រយៈពេល<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" placeholder="រយៈពេល" name="duration" data-required="1" class="span9 m-wrap"/>

			  								</div>

			  								<label class="control-label">កាលបរិច្ឆេទចាប់ផ្តើម<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" placeholder="ថ្ងៃចាប់ផ្តើម" name="start_date" data-required="1" class="span9 m-wrap input-xlarge datepicker" id="start_date"/>

			  								</div>

											<label class="control-label">កាលបរិច្ឆេទបញ្ចប់<span class="required">*</span></label>

			  								<div class="controls">

			  									<input type="text" name="end_date" placeholder="ថ្ងៃបញ្ចប់" data-required="1" class="span9 m-wrap input-xlarge datepicker" />

			  								</div>

											

											<label class="control-label">ការកំណត់សម្គាល់</label>

			  								<div class="controls">
			  									<textarea name="discription" data-required="1" class="span9 m-wrap" placeholder="មតិយោបល់"></textarea>			  								
			  								</div>

									</div>



									<div class="span9">

										<center>

											<button type="submit" class="btn btn-success">បញ្ចូល</button>
											<a href="{{url('public_holiday')}}" class="btn btn-danger">ត្រលប់</a>

										</center>

									</div>

								</form>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	

<script type="text/javascript">



</script>



	

@stop()