@extends('credit_sale.layout.master')

@section('contend')

	

<div class="container-fluid">

            <div class="row-fluid">

               

                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <!-- <div class="muted pull-left"><a href="{!! url('public_holiday') !!}">ការកំណត់ថ្ងៃឈប់សំរាក</a> </div> -->

                            	<div class="muted pull-right" style="padding-top: 0px;"><a href="{{ url('public_holiday/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើត</a></div>

                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                            <div class="span12">

                            <center>
                            	<h3 class="cen_title khmer_Moul"> ការកំណត់ថ្ងៃឈប់សម្រាក</h3>
                            	<legend></legend>
                            </center>

                            </div>

                            	<table class="table table-bordered" style="text-align:center">

                            	     <thead style="background: rgb(251, 205, 205);">

                            	     	<tr>

                            	     		<th>#</th>
                            	     		<th>ឈ្មោះ</th>
                            	     		<th>រយៈពេល</th>
                            	     		<th>កាលបរិចេ្ឆទចាប់ផ្តើម</th>
                            	     		<th>កាលបរិច្ឆេទបញ្ចប់</th>
                            	     		<th>ការកំណត់សម្គាល់</th>
                            	     		<th>សកម្មភាព</th>
                            	     		

                            	     	</tr>

                            	     </thead>

                            	     <tbody>

                            	@foreach($list as $key=>$l)

						                <tr>

						                  <td>{{ $key+1 }} </td>

						                  <td>{{ $l->title }}</td>

						                  <td>{{ $l->duration }}</td>

						                  <td><?php  echo date('l',strtotime($l->start_date))."&nbsp;&nbsp;".date('d-m-Y',strtotime($l->start_date)) ?></td>

						                  <td>

						                 	 <?php  echo date('l',strtotime($l->end_date))."&nbsp;&nbsp;".date('d-m-Y',strtotime($l->end_date)) ?>

						                  </td>
						                  <td>{{ $l->discription }}</td>

						                  <td width="140px"><a href="{{ url('public_holiday/'.$l->id.'/edit') }}" class="btn btn-info">កែប្រែ</a> &nbsp;&nbsp;<a href="{{ url('public_holiday/'.$l->id.'/deleted') }}" class="btn btn-danger">លុប</a> </td>

						                </tr>

						        @endforeach       

						              </tbody>

						            </table>

							{{  $data->links() }}

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>

            </div>	



	

@stop()