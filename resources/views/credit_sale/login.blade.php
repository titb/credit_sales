<!DOCTYPE html>

<html>

  <head>

    <title>Admin Login</title>

    <!-- Bootstrap -->

    <meta name="csrf-token" content="{{ csrf_token() }}">

    <link href="{!! url('bootstrap/css/bootstrap.min.css') !!}" rel="stylesheet" media="screen">

    <link href="{!! url('bootstrap/css/bootstrap-responsive.min.css') !!}" rel="stylesheet" media="screen">

    <link href="{!! url('assets/styles.css') !!}" rel="stylesheet" media="screen">

     <!-- HTML5 shim, for IE6-8 support of HTML5 elements -->

    <!--[if lt IE 9]>

      <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>

    <![endif]-->

    <!-- <script src="{!! url('js/vendor/modernizr-2.6.2-respond-1.1.0.min.js') !!}"></script> -->

  </head>

  <body id="login">

    <div class="container">

      @if (count($errors) > 0)

          <div class="alert alert-danger">

            <strong>Whoops!</strong> There were some problems with your input.<br><br>

            <ul>

              @foreach ($errors->all() as $error)

                <li>{{ $error }}</li>

              @endforeach

            </ul>

          </div>

      @endif

      <form class="form-signin" action="{!! url('login') !!}" method="post">

       {{ csrf_field() }}



        <h2 class="form-signin-heading">Please sign in</h2>

        <label>UserName</label>

        <input type="text" class="input-block-level" name="username" placeholder="Username" autofocus>

         @if ($errors->has('username'))

            <span class="help-block">

                <strong>{{ $errors->first('username') }}</strong>

            </span>

        @endif

        <label>Password</label>

        <input type="password" class="input-block-level" name="password" placeholder="Password">

         @if ($errors->has('password'))

            <span class="help-block">

                <strong>{{ $errors->first('password') }}</strong>

            </span>

        @endif

        <!-- <label class="checkbox">

          <input type="checkbox" value="remember-me"> Remember me

        </label> -->

        <button class="btn btn-large btn-primary" type="submit">Sign in</button>

      </form>



    </div> <!-- /container -->

    <script src="{!! url('vendors/jquery-1.9.1.min.js') !!}"></script>

    <script src="{!! url('bootstrap/js/bootstrap.min.js') !!}"></script>

  </body>

</html>
