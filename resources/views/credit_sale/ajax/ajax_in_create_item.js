var submitting = false;

function hashDiff(h1, h2) {
  var d = {};
  for (k in h2) {
    if (h1[k] !== h2[k]) d[k] = h2[k];
  }
  return d;
}

function convertSerializedArrayToHash(a) { 
  var r = {}; 
  for (var i = 0;i<a.length;i++) {
		if(a[i].name !== 'submitf')
		{
			r[a[i].name] += a[i].value;
		}
  }
  return r;
}

var $form = $('#item_form, #item_kit_form').eq(0);

var startItems;

$(document).ready(function() {		
	if(!startItems)
	{
		startItems = convertSerializedArrayToHash($form.serializeArray());
	}
});

function doItemSubmit(form, args)
{
  var currentItems = convertSerializedArrayToHash($form.serializeArray());
	
	var startItems_length = $.map(startItems, function(n, i) { return i; }).length;
	var currentItems_length = $.map(currentItems, function(n, i) { return i; }).length;
	
  var itemsToSubmit = hashDiff(startItems, currentItems);
		
	if($.isEmptyObject(itemsToSubmit) && startItems_length == currentItems_length)
	{
		show_feedback('warning',"warning","There was no updated information to save.");
		
		return;
	}
	
	if (submitting) return;
	submitting = true;
	
	$('#grid-loader').show();
	
	$(form).ajaxSubmit({
	success:function(response)
	{
		$('#grid-loader').hide();
			startItems = convertSerializedArrayToHash($form.serializeArray());
			submitting = false;		
									
						
			if(response.progression && response.redirect=='sales/index/1' && response.success)
			{ 
				item_save_confirm_dialog({
					reload: response.reload ? true : false,
					id: response.item_id,
					confirm: {
						label: "Add Item to Sale",
						post_url: "https:\/\/demo.phppointofsale.com\/index.php\/sales\/add",
						redirect: "https:\/\/demo.phppointofsale.com\/index.php\/sales\/index\/1"					},
					cancel: {
						label: args.next.label,
						redirect: args.next.url
					}
				});
			}
			else if(response.progression && response.redirect=='receivings/' && response.success)
			{
				item_save_confirm_dialog({
					reload: response.reload ? true : false,
					id: response.item_id,
					confirm: {
						label: "Add Item to Receiving",
						post_url: "https:\/\/demo.phppointofsale.com\/index.php\/receivings\/add",
						redirect: "https:\/\/demo.phppointofsale.com\/index.php\/receivings"					},
					cancel: {
						label: args.next.label,
						redirect: args.next.url
					}
				});
			}
			else if(response.progression && response.redirect == 'items' && response.success)
			{
				if(args.next.redirect)
				{
					item_save_confirm_dialog({
						reload: response.reload ? true : false,
						id: response.item_id,
						confirm: {
							label: "Return to Items",
							post_url: null,
							redirect: "https:\/\/demo.phppointofsale.com\/index.php\/items"						},
						cancel: {
							label: "Continue Editing",
							redirect: null
						}
					});
				}
				else
				{
					if(response.reload)
					{
						window.location.reload();
					}
					else
					{
						window.location.href = args.next.url.replace(/-1/g, response.item_id);
					}	
				}
			}
			else if(response.progression && response.redirect == 'item_kits' && response.success)
			{
				if(args.next.redirect)
				{
					item_save_confirm_dialog({
						reload: response.reload ? true : false,
						id: response.item_kit_id,
						confirm: {
							label: "Return to Item Kits",
							post_url: null,
							redirect: "https:\/\/demo.phppointofsale.com\/index.php\/item_kits"						},
						cancel: {
							label: "Continue Editing",
							redirect: null
						}
					});
				}
				else
				{
					window.location.href = args.next.url.replace(/-1/g, response.item_kit_id);
				}
			}
			else if(response.redirect == 'items' && response.success)
			{
				if(response.quick_edit)
				{
					window.location.href = "https:\/\/demo.phppointofsale.com\/index.php\/items";
				}
				else
				{
						item_save_confirm_dialog({
							reload: response.reload ? true : false,
							id: response.item_id,
							confirm: {
								label: "Return to Items",
								post_url: null,
								redirect: "https:\/\/demo.phppointofsale.com\/index.php\/items"							},
							cancel: {
								label: "Continue Editing",
								redirect: null
							}
						});
					
				}
			}
			else if(response.redirect == 'item_kits' && response.success)
			{
				if(response.quick_edit)
				{
					window.location.href = "https:\/\/demo.phppointofsale.com\/index.php\/item_kits";
				}
				else
				{
					item_save_confirm_dialog({
						reload: response.reload ? true : false,
						id: response.item_id,
						confirm: {
							label: "Return to Item Kits",
							post_url: null,
							redirect: "https:\/\/demo.phppointofsale.com\/index.php\/item_kits"						},
						cancel: {
							label: "Continue Editing",
							redirect: null
						}
					});
				}
			}
	},
	dataType:'json'
	});
}

$('a.outbound_link').click(function(e)
{
	var $that = $(this);
	
  var currentItems = convertSerializedArrayToHash($form.serializeArray());
  var itemsToSubmit = hashDiff(startItems, currentItems);
		
	$form.validate();
		
	if(!$form.valid())
	{
		e.preventDefault();
		bootbox.alert("You must fill out the required fields before making other changes.");
		return;
	}
		
	if($.isEmptyObject(itemsToSubmit))
	{
		return;
	}
		
	e.preventDefault();
		
	$('#grid-loader').show();
	
	$form.ajaxSubmit({
		success: function(response,status)
		{
			var id = response.item_id ? response.item_id : response.item_kit_id;

			var updated_href = $that.attr('href').replace(/-1/g, id);
			$(e.target).attr('href', updated_href);
			$('#grid-loader').hide();
			window.location = updated_href;

		},
		dataType:'json'
	});
	
});

function item_save_confirm_dialog(args) {
	
	bootbox.confirm({
	    message: "What would you like to do next?",
	    buttons: {
	        confirm: {
	            label: args.confirm.label,
	            className: 'btn-primary'
	        },
	        cancel: {
	            label: args.cancel.label,
							className: 'btn-default'
	            
	        }
	    },
	    callback: function (result) {
				if(result)
				{
					
					var done = function() {
							window.location.href = args.confirm.redirect;
					};
					
					if(args.confirm.post_url)
					{
						$.post(args.confirm.post_url, {item: args.id}, done);
					} else {
						done();
					}
					
				} else {
					if(args.cancel.redirect)
					{
						window.location.href = args.cancel.redirect.replace(/-1/g, args.id);
					}
					
					if(args.reload)
					{
						window.location.reload();
					}
				}
	    }
	});
}	
function check_service_inputs()
{
	var $reorder_inputs = $(".is-service-toggle");
	
	if ($('#is_service').prop('checked'))
	{
		$reorder_inputs.addClass('hidden');
	}
	else
	{
		$reorder_inputs.removeClass('hidden');
	}
}


$(document).ready(function()
{		
	$("#is_serialized").change(function()
	{
		if ($(this).prop('checked'))
		{
			$("#serial_container").removeClass('hidden');
		}
		else
		{
			$("#serial_container").addClass('hidden');			
		}
	});
	
	$(".delete_serial_number").click(function()
	{
		$(this).parent().parent().remove();
	});
	
	$("#add_serial_number").click(function()
	{
		$("#serial_numbers tbody").append('<tr><td><input type="text" class="form-control form-inps" size="40" name="serial_numbers[]" value="" /></td><td><input type="text" class="form-control form-inps" size="20" name="serial_number_prices[]" value="" /></td><td>&nbsp;</td></tr>');
	});
	
	$(".delete_addtional_item_number").click(function()
	{
		$(this).parent().parent().remove();
	});
	
	$("#add_addtional_item_number").click(function()
	{
		$("#additional_item_numbers tbody").append('<tr><td><input type="text" class="form-control form-inps" size="40" name="additional_item_numbers[]" value="" /></td><td>&nbsp;</td></tr>');
	});	
	
	$('#supplier_id').selectize();
		
	$('#category_id').selectize({
		create: true,
		render: {
	    item: function(item, escape) {
				var item = '<div class="item">'+ escape($('<div>').html(item.text).text()) +'</div>';
				return item;
	    },
	    option: function(item, escape) {
				var option = '<div class="option">'+ escape($('<div>').html(item.text).text()) +'</div>';
				return option;
	    },
      option_create: function(data, escape) {
			var add_new = "New Category";
        return '<div class="create">'+escape(add_new)+' <strong>' + escape(data.input) + '</strong></div>';
      }
		}
	});
	
	$("#cancel").click(cancelItemAddingFromSaleOrRecv);
	
  setTimeout(function(){$(":input:visible:first","#item_form").focus();},100);
	
	$(document).on('change', '#is_service', check_service_inputs);

	$('#tags').selectize({
		delimiter: ',',
		loadThrottle : 215,
		persist: false,
		valueField: 'value',
		labelField: 'label',
		searchField: 'label',
		create: true,
		render: {
	      option_create: function(data, escape) {
				var add_new = "Add new tag";
	        return '<div class="create">'+escape(add_new)+' <strong>' + escape(data.input) + '</strong></div>';
	      }
		},
		load: function(query, callback) {
			if (!query.length) return callback();
			$.ajax({
				url:'https://demo.phppointofsale.com/index.php/items/tags'+'?term='+encodeURIComponent(query),
				type: 'GET',
				error: function() {
					callback();
				},
				success: function(res) {
					res = $.parseJSON(res);
					callback(res);
				}
			});
		}
	});
	
	$('#item_form').validate({
		ignore: ':hidden:not([class~=selectized]),:hidden > .selectized, .selectize-control .selectize-input input',
		submitHandler:function(form)
		{			
			var args = {
				next: {
					label: "Edit Variations",
					url: "https:\/\/demo.phppointofsale.com\/index.php\/items\/variations\/-1?redirect=items&progression=1",
				}
			};
			
			$.post('https://demo.phppointofsale.com/index.php/items/check_duplicate', {term: $('#name').val()},function(data) {
							if(data.duplicate)
				{
					bootbox.confirm("A similar item name already exists. Do you want to continue?", function(result)
					{
						if(result)
						{
							doItemSubmit(form, args);
						}
					});
				}
				else
				{
					doItemSubmit(form, args);
				}
								} , "json");
		},
		errorClass: "text-danger",
		errorElement: "span",
		highlight:function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-success').addClass('has-error');
		},
		unhighlight: function(element, errorClass, validClass) {
			$(element).parents('.form-group').removeClass('has-error').addClass('has-success');
		},
		rules:
		{
					item_number:
			{
				remote: 
		    { 
					url: "https://demo.phppointofsale.com/index.php/items/item_number_exists", 
					type: "post"
		    } 
			},
			product_id:
			{
				remote: 
		    { 
					url: "https://demo.phppointofsale.com/index.php/items/product_id_exists", 
					type: "post"
		    } 
			},
					name:"required",
			category_id:"required",
			reorder_level:
			{
				number:true
			}
		},
		messages:
		{			
						item_number:
			{
				remote: "UPC\/EAN\/ISBN already exists"				   
			},
			product_id:
			{
				remote: "Product Id already exists"				   
			},
						
						
							"locations[1][quantity]":
				{
					number: "This field must be a number"				},
				"locations[1][reorder_level]":
				{
					number: "This field must be a number"				},
				"locations[1][cost_price]":
				{
					number: "This field must be a number"				},
				"locations[1][unit_price]":
				{
					number: "This field must be a number"				},			
				"locations[1][promo_price]":
				{
					number: "This field must be a number"				},			
								
							"locations[2][quantity]":
				{
					number: "This field must be a number"				},
				"locations[2][reorder_level]":
				{
					number: "This field must be a number"				},
				"locations[2][cost_price]":
				{
					number: "This field must be a number"				},
				"locations[2][unit_price]":
				{
					number: "This field must be a number"				},			
				"locations[2][promo_price]":
				{
					number: "This field must be a number"				},			
								
							"locations[4][quantity]":
				{
					number: "This field must be a number"				},
				"locations[4][reorder_level]":
				{
					number: "This field must be a number"				},
				"locations[4][cost_price]":
				{
					number: "This field must be a number"				},
				"locations[4][unit_price]":
				{
					number: "This field must be a number"				},			
				"locations[4][promo_price]":
				{
					number: "This field must be a number"				},			
								
						
			name:"Item Name is a required field",
			category_id:"Category is a required field",
			cost_price:
			{
				required:"Cost Price is a required field",
				number:"Cost price must be a number"			},
			unit_price:
			{
				required:"Selling Price is a required field",
				number:"Unit price must be a number"			},
			promo_price:
			{
				number: "This field must be a number"			}
		}
	});
});

function cancelItemAddingFromSaleOrRecv()
{
	bootbox.confirm("Are you sure you wish to cancel adding this item?", function(result)
	{
		if (result)
		{
							window.location = "https:\/\/demo.phppointofsale.com\/index.php\/receivings";
					}
	});
}

$("#verify_age").click(function()
{
	if ($('#verify_age').prop('checked'))
	{
		$("#required_age_container").removeClass('hidden');	
	}
	else
	{
		$("#required_age_container").addClass('hidden');
	}
	
});

$(document).on('click', "#add_category",function()
{
	$("#categoryModalDialogTitle").html("Add Category");
	var parent_id = $("#category_id").val();
	
	$parent_id_select = $('#parent_id');
	$parent_id_select[0].selectize.setValue(parent_id, false);
	
	$("#categories_form").attr('action',SITE_URL+'/items/save_category');
	
	//Clear form
	$(":file").filestyle('clear');
	$("#categories_form").find('#category_name').val("");
	$("#categories_form").find('#category_color').val("");
	$('#category_color').colorpicker('setValue', '');
	$("#categories_form").find('#category_image').val("");
	$("#categories_form").find('#image-preview').attr('src','');
	$('#del_image').prop('checked',false);
	$('#preview-section').hide();
	
	//show
	$("#category-input-data").modal('show');
});

$("#categories_form").submit(function(event)
{
	event.preventDefault();

	$(this).ajaxSubmit({ 
		success: function(response, statusText, xhr, $form){
			show_feedback(response.success ? 'success' : 'error', response.message, response.success ? "Success" : "Error");
			if(response.success)
			{
				$("#category-input-data").modal('hide');
				
				var category_id_selectize = $("#category_id")[0].selectize
				category_id_selectize.clearOptions();
				category_id_selectize.addOption(response.categories);		
				category_id_selectize.addItem(response.selected, true);			
			}		
		},
		dataType:'json',
	});
});