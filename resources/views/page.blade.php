@extends('credit_sale.layout.master')

@section('contend')
<style>
.pagination a {
    color: black;
    cursor: pointer;
    padding: 8px 16px;
    text-decoration: none;
    transition: background-color .3s;
}

.pagination a.active {
    background-color: dodgerblue;
    color: white;
}

.pagination a:hover:not(.active) {background-color: #ddd;}
</style>
	<div class="container-fluid">

        <div class="row-fluid">
                     <!-- validation -->

                    <div class="row-fluid">

                         <!-- block -->

                        <div class="block">

                            <div class="navbar navbar-inner block-header">

                                <!-- <form class="form-search" style="margin-bottom:2px;" action="{{URL::to('users')}}" method="GET">


                                        <input type="text" name="search" placeholder="Name" data-required="1" class="span3 m-wrap" style="margin-top:5px;" id="typeahead" data-provide="typeahead" data-source=''>

                                        <button type="submit" class="btn btn-success"><i class="icon-search icon-white"></i> Search</button>
								</form> -->
								<div class="muted span3" style=""><a href="{{ url('users') }}" class="pull-left">ការគ្រប់គ្រង់អ្នកប្រើប្រាស់</a></div>

								<div class="muted span3 pull-right" style="margin-top:-8px; margin-bottom:5px;"><a href="{{ url('users/create') }}" class="btn btn-success pull-right"><i class="icon-plus icon-white"></i> បង្កើតថ្មី</a></div>


                            </div>

                            <div class="block-content collapse in">

                            @if (count($errors) > 0)

						          <div class="alert alert-danger">

						            <strong>Whoops!</strong> There were some problems with your input.<br><br>

						            <ul>

						              @foreach ($errors->all() as $error)

						                <li>{{ $error }}</li>

						              @endforeach

						            </ul>

						          </div>

						    @endif

                            @if ($message = Session::get('success'))

		                        <div class="alert alert-success">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif

		                    @if($message = Session::get('keyerror'))

		                        <div class="alert alert-danger">

		                            <p>{{ $message }}</p>

		                        </div>

		                    @endif



                             	<center>

			                        <h3 class="cen_title text-center khmer_Moul">ការគ្រប់គ្រង់អ្នកប្រើប្រាស់</h3>


			                    </center>

                            	<legend></legend>

							   <!-- <table cellpadding="0" cellspacing="0" border="0" class="table table-striped table-bordered" id="example">
							   -->
							   <table class="table table-striped table-bordered">

								<thead style="background: rgb(251, 205, 205);">

							        <tr class="header">

							            <th>លេខសម្គាល់</th>

							            <th>ឈ្មោះអ្នកប្រើប្រាស់</th>

							            <th>ឈ្មោះជាភាសាខ្មែរ</th>

							            <th>សារអេឡិចត្រូនិច </th>

							            <th>លេខទូរសព្ទ</th>

							            <th>ភេទ</th>

							            <th>មុខតំណែង</th>

							            <th>សិទ្ធិប្រើប្រាស់</th>

							            <th>ស្ថានភាព</th>

										<th>ប្តូរលេខសម្ងាត់</th>

							            <th>សកម្មភាព</th>

							        </tr>

							      </thead>

							     <tbody id="user_tr">
							      </tbody>

							    </table>
                            	<!-- Pagination -->
								<div class="pagination text-right">

								</div>

			    			</div>

						</div>

                     	<!-- /block -->

		    		</div>

                     <!-- /validation -->

                </div>
                <input type="hidden"  name="token" id="token0">

            </div>

<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
	 $(document).ready(function(){
        var numpage = 1 ;

		get_page(numpage);

		function get_page(n){

			 var login =  $.ajax({

	              type: "POST",
	              url: "http://localhost/kasp/mapi1/public/api/login",
	              dataType: "json",
	              data:{'username':'socheat','password':'mfi!@#123','user_status':1},
	              success: function(data){
	                  $("#token0").val(data.result); 
	                 
	              }
	        });	

				// var url = "http://localhost/credit-sale/mapi1/public/api/users?page="+n;
				// var txt = ""; var out = "";
         login.done(function() {
         var token1 =  $("#token0").val();
              if(token1 !== ""){
                  var url = "http://localhost/kasp/mapi1/public/api/users?page="+n;
                  var txt = ""; var out = "";
                     var forData = {
                                  'token': $("#token0").val(),
                       }
                    $.getJSON(url, forData, function(result){
					// $.getJSON(url, function(result){

						$.each(result.data, function(i, field){
							if(field.user_status === 1){
								var tive = "Active";
								var st = "<span class='badge badge-success'>" + tive + "</span>";
							}else{
								var tive = "Inactive";
								var st = "<span class='badge badge-important'>" + tive + "</span>";
							}
							txt += "<tr id='user_lr" +i+ "'>";
							txt += "<td>" + field.id + "</td>";
							txt += "<td>" + field.username + "</td>";
							txt += "<td>" + field.name_kh + "</td>";
							txt += "<td>" + field.user_email + "</td>";
							txt += "<td>" + field.user_phone + "</td>";
							txt += "<td>" + field.user_gender + "</td>";
							txt += "<td>" + field.mp_name+ "</td>";
							txt += "<td>" + field.gdisplay_name+ "</td>";
							txt += "<td>"+ st +"</td>";
							txt += "<td><a href='#' id='change_pass" +field.id+ "'>Change Password</a></td>";
							txt += "<td>";
							txt += " <button class='btn btn-info' id='show" + field.id+ "' >Show</button> ";
							txt += "  <button class='btn btn-primary' id='edit" + field.id+ "'>Edit</button> ";
							txt += " <button class='btn btn-danger' id='deleted" + field.id+ "'>Delete</button> ";
							txt += "</td>";
							txt += "</tr>";

						});
						$("#user_tr").html(txt );
						var page = "";
						if(result.prev_page_url === null){
							var pr_url = result.current_page;
						}else{
							var pr_url = result.current_page -1;
						}
						page += "<a class='pre'>&laquo;<input type='hidden' class='pre_in' value='"+pr_url+"' ></a>";
						for(var x = 1; x <= result.last_page; x ++  ) {
							if(result.current_page === x){
								page += "<a class='pag active' >"+x+"</a>";
							}else{
								page += "<a class='pag' >"+x+"</a>";
							}


						}
						if(result.next_page_url === null){
							var ne_url = result.current_page;
						}else{
							var ne_url = result.current_page +1;
						}
						page += "<a class='pre'>&raquo;<input type='hidden' class='pre_in' value='"+ne_url+"' ></a>";
						$(".pagination").html(page );

					})
				} 
            
		});
  }
		$(document).ajaxComplete(function(){
					$(".pag").click(function(){
						var n = $(this).text();
						get_page(numpage = n);
					});

					$(".pre").click(function(){
						var n = $(this).find(".pre_in").val();
						get_page(numpage = n);
					});
		});
    });
</script>



@stop()
