<div class="span12 mess_direct"  style="margin-left: 0;margin-top: 0px; min-height: 0px;">
                            @if (count($errors) > 0)
						          <div class="alert alert-danger">
						            <strong>Whoops!</strong> There were some problems with your input.<br><br>
										<ul>
										@foreach ($errors->all() as $error)
											<li>{{ $error }}</li>
										@endforeach
										</ul>
						          </div>
						    @endif
						    
						    @if ($message = Session::get('error_search'))
		                        <div class="alert alert-success">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
                            @if ($message = Session::get('success'))
		                        <div class="alert alert-success">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
		                    @if ($message = Session::get('keyerror'))
		                        <div class="alert alert-danger">
		                            <p>{{ $message }}</p>
		                        </div>
		                    @endif
		

</div>
<div class="span12 " id="msg_show1" style="margin-left: 0;margin-top: 0px; min-height: 0px;">
		<p class="msg_show"></p>
</div>
<script>
		setTimeout(function(){
			 $('.mess_direct').empty();
			 $('#msg_show1').empty();
			 $('#msg_show1').html("<p class='msg_show'></p>");

		}, 10000);
		
</script>
